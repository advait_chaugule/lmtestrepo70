/*****
 * Controller for Question Authoring
 * Author - Atin Roy*
 */

'use strict';
app.controller('assessmentQuestion', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'assessmentService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', function ($scope, $http, sessionService, $location, objectService, assessmentService, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.project_id = $routeParams.project_id;
        $scope.assessment_id = $routeParams.assessment_id;
        $scope.question_type_id = $routeParams.question_type_id;
        $scope.question_id = $routeParams.question_id;
        $scope.project_type_id = 4;
        $scope.createQuesButtonEnable = true;
        $scope.enableAdd = false;
        $scope.enableEdit = false;
        $scope.ext_res_block = false;
        $scope.ext_ico_block = false;
        $scope.questionSelection = false;
        $scope.selected_data = [];
        $scope.err_message = '';
        $scope.textwith_blanks = '';
        $scope.location = $location;
        if (angular.isDefined($routeParams.question_type_id)) {
            $scope.createQuesButtonEnable = false;
            $scope.question_type_folder = ($routeParams.question_type_id == "MCQ") ? "mcss" : $scope.question_type_id;
            $scope.question_type_folder = $scope.question_type_folder.toLowerCase();
            $scope.template_folder = 'templates/assessment/' + $scope.question_type_folder + '/' + $scope.question_type_folder + '.html';
            $scope.script_path = 'templates/assessment/' + $scope.question_type_folder + '/js/getSetContent.js';
            if (!angular.isDefined($routeParams.question_id)) {
                $scope.enableAdd = true;
            }

        }
        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        //Get Project Detail
        $scope.project_name = '';
        $scope.getProject = function () {
            if ($scope.project_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    $scope.project_name = data.name;
                    $scope.project = data;
                    $scope.applyPermission = 1;
                    $scope.project.project_permissions = data.project_permissions;
                });
            }
        }
        $scope.getProject();
        //Fetch Questions
        $scope.pageNumber = 1;
        $scope.itemsPerPage = 10;
        $scope.currentPage = 1;
        $scope.listQuestions = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 10;
            assessmentService.getQuestions($scope).then(function (data) {
                $scope.questionList = data.data.question_list;
                $scope.questionTotal = data.data.total;
            });
        };
        $scope.listQuestions(1);

        //Upload Flow
        var scopecounter = 0;
        $scope.MsgCounter = 1;
        $scope.SuccessMsgCounter = 0;
        $scope.fileUploadMessage = '';
        $scope.$watch('$flow.progress()', function (val) {
            if ($filter('number')(val * 100, 0) == 100) {

            }
        });
        $scope.$on('flow::fileAdded', function (event, $flow, flowFile) {
            $flow.opts.query = {project_id: $scope.project_id, assessment_id: $scope.assessment_id, temp_guid: guid('')};
        });

        $scope.uploader = {
            controllerFn: function ($flow, $file, $message) {
                console.log($message);
                $file.msg = $message; // Just display message for a convenience
                if ($file.size == 0) {
                    $scope.fileUploadMessage += '<p> File <span style="word-wrap: break-word;">' + $file.name + '<span> should have some content' + '</p>';
                    $scope.MsgCounter = $scope.MsgCounter + 1;
                } else {
                    if ($file.msg.split(':')[0] == 'Error') {
                        $scope.fileUploadMessage += '<p>' + $file.msg.substring($file.msg.indexOf(':') + 1) + ' </p>';
                        $scope.MsgCounter = $scope.MsgCounter + 1;
                    } else {
                        $scope.SuccessMsgCounter = $scope.SuccessMsgCounter + 1;
                    }
                }
                scopecounter = scopecounter + 1;
                if ($scope.$flow.files.length === scopecounter) {
                    if ($scope.SuccessMsgCounter > 0) {
                        if ($scope.MsgCounter === 1) {
                            $scope.HtmlSuccess = '<p>';
                        } else {
                            $scope.HtmlSuccess = '<p style="text-align:left">' + $scope.MsgCounter + '. ';
                        }
                        console.log($file.msg.split(':')[0]);
                        if ($file.msg.split(':')[0] == 'Success') {
                            $scope.fileUploadMessage += $file.msg.split(':')[1] + '</p>';
                        } else {
                            $scope.fileUploadMessage += $file.msg.split(':')[1] + ' </p>';
                        }
                    }
                    SweetAlert.swal({html: $scope.fileUploadMessage, width: 540, type: "warning"});
                    $scope.$flow.files.length = 0;
                    scopecounter = 0;
                    $scope.fileUploadMessage = '';
                    $scope.MsgCounter = 1;
                    $scope.SuccessMsgCounter = 0;
                }
            }
        };



        $scope.getQuestionDetails = function (assessment_id, question_id) {
            $scope.question_id = question_id;
            assessmentService.getQuestionDetails($scope).then(function (data) {
                if (data.status == 200) {
                    $scope.fetched_data = data.data.question_data.QuestionData;
                    if ($scope.question_type_id === 'MCQ') {
                        mcq.renderQuestionData(data.data.question_data.QuestionData);
                    } else if ($scope.question_type_id === 'FIB') {
                        angular.element('.textWithBlankError').css('display', 'none');
                        fib.renderQuestionData(data.data.question_data.QuestionData);
                    } else if ($scope.question_type_id === 'FIB-DRAGDROP') {
                        angular.element('.textWithBlankError').css('display', 'none');
                        fibdrag.renderQuestionData(data.data.question_data.QuestionData);
                    } else if ($scope.question_type_id === 'FIB-DROPDOWN') {
                        angular.element('.textWithBlankError').css('display', 'none');
                        fib.renderQuestionData(data.data.question_data.QuestionData);
                    } else if ($scope.question_type_id === 'MULTI-PART') {
                        multipart.renderQuestionData(data.data.question_data.QuestionData);
                        $scope.selected_data = data.data.question_data.QuestionData.subquestions;
                        $scope.fetched_data = data.data.question_data.QuestionData;
                        for (var i = 0; i < $scope.selected_data.length; i++) {
                            $scope.fetched_data.subquestions[i] = JSON.parse($scope.fetched_data.subquestions[i]).question_id;
                        }
                    } else if ($scope.question_type_id === 'IN-CONTEXT') {
                        incontext.renderQuestionData(data.data.question_data.QuestionData);
                    } else {
                        openended.renderQuestionData(data.data.question_data.QuestionData);
                    }
                    // bind all events after render data
                    common_assessment_utility.ckeditorBindEvent();

                    var dataset = data.data.question_data.QuestionData;
                    if (angular.isDefined(dataset.media)) {
                        if (dataset.media.type != 'no-media') {
                            $scope.media = {
                                type: "media"
                            };
                            $scope.ext_res_url = dataset.media.url;
                            if (dataset.media.type === 'brightcove') {
                                $scope.ext_res_title = '<b>Video ID:</b> <br>' + dataset.media.brightcove_id;
                                $scope.ext_res_title_id = dataset.media.brightcove_id;
                                $scope.ext_res_width = parseInt(dataset.media.width);
                                $scope.ext_res_height = parseInt(dataset.media.height);
                                angular.element('#ext_res_title').val($scope.ext_res_title);
                                angular.element('#ext_res_title_id').val($scope.ext_res_title_id);
                                angular.element('#media_type').val('brightcove');
                                angular.element('#ext_res_width').val($scope.ext_res_width);
                                angular.element('#ext_res_height').val($scope.ext_res_height);
                                angular.element('#brightcove_button').text('Edit Brightcove Video');
                                $scope.ext_res_type = dataset.media.type;
                            } else {
                                $scope.ext_res_title = dataset.media.title;
                                $scope.ext_res_type = dataset.media.asset_type;
                                angular.element('#ext_res_title').val($scope.ext_res_title);
                                angular.element('#ext_res_type').val($scope.ext_res_type);
                                angular.element('#media_type').val('assets');
                                if ($scope.ext_res_type == 'image' || $scope.ext_res_type == 'Image') {
                                    $scope.ext_ico_block = true;
                                } else {
                                    $scope.ext_ico_block = false;
                                }
                                angular.element('#brightcove_button').text('Add Brightcove Video');
                            }
                            //$scope.ext_res_type = dataset.media.type;
                            angular.element('#ext_res_url').val($scope.ext_res_url);
                        }
                    }

                } else {
                    SweetAlert.swal("Oops...", "Something went wrong!", "error");
                }

            });
        };
        //For Page Change from paging
        $scope.pageChangeHandlerQuestion = function (newPage) {
            //$scope.assessment_id = '';
            $scope.listQuestions(newPage);
        };
        $scope.question_types = [];
        assessmentService.getQuestionTypes($scope).then(function (data) {
            var question_types = data.data.question_types;
            angular.forEach(question_types, function (value, key) {
                $scope.question_types.push({
                    'id': key,
                    'name': value
                });
            });
        });
        $scope.backToAssessment = function (assessment_id) {
            $location.path('assessment/' + assessment_id);
        }
        $scope.backToGlobalRepo = function (assessment_id) {
            $location.path('global_repo/ASSESSMENTS');
        }
        $scope.backToQuestion = function (project_id, assessment_id) {
            if ($scope.question_type_id === 'MCQ') {
                $scope.compare_data_json = mcq.generateQuestionData();
            } else if ($scope.question_type_id === 'FIB') {
                $scope.compare_data_json = fib.generateQuestionData();
            } else if ($scope.question_type_id === 'FIB-DRAGDROP') {
                $scope.compare_data_json = fibdrag.generateQuestionData();
            } else if ($scope.question_type_id === 'FIB-DROPDOWN') {
                $scope.compare_data_json = fib.generateQuestionData();
            } else if ($scope.question_type_id === 'MULTI-PART') {
                $scope.compare_data_json = multipart.generateQuestionData();
            } else if ($scope.question_type_id === 'IN-CONTEXT') {
                $scope.compare_data_json = incontext.generateQuestionData();
            } else {
                $scope.compare_data_json = openended.generateQuestionData();
            }
            var i = 0;
            angular.forEach(angular.element(".requires"), function (value, key) {
                var a = angular.element(value);
                if (a.hasClass('ckeditor')) { // for all ckeditor
                    var field_value = common_assessment_utility.getFieldValue(a.prop('id'), true);
                    if (field_value.trim().length === 0) {
                        a.parent().addClass('has-error');
                        i++;
                    } else { // for all input box
                        a.parent().removeClass('has-error');
                    }
                } else {
                    if (a.val().trim().length === 0) {
                        a.parent().addClass('has-error');
                        i++;
                    } else {
                        a.parent().removeClass('has-error');
                    }
                }
            });
            var compare = JSON.stringify($scope.fetched_data) === JSON.stringify($scope.compare_data_json);
            if (!compare) {
                SweetAlert.swal({
                    title: "Are you sure?",
                    text: "You are leaving the authoring page",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Save it",
                    cancelButtonText: "No, Don't save",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        if ($scope.enableAdd) {
                            if (i === 0) {
                                $scope.addQuestions();
                            }
                            //$location.path('assessment_question/' + assessment_id + '/' + project_id);
                        } else {
                            if (i === 0) {
                                $scope.editQuestion(true);
                            }
                        }
                    } else {
                        $location.path('assessment_question/' + assessment_id + '/' + project_id);
                    }
                });
            } else {
                $location.path('assessment_question/' + assessment_id + '/' + project_id);
            }
        };
        $scope.backToQuestionGlobal = function (project_id, assessment_id) {
            if ($scope.question_type_id === 'MCQ') {
                $scope.compare_data_json = mcq.generateQuestionData();
            } else if ($scope.question_type_id === 'FIB') {
                $scope.compare_data_json = fib.generateQuestionData();
            } else if ($scope.question_type_id === 'FIB-DRAGDROP') {
                $scope.compare_data_json = fibdrag.generateQuestionData();
            } else if ($scope.question_type_id === 'FIB-DROPDOWN') {
                $scope.compare_data_json = fib.generateQuestionData();
            } else if ($scope.question_type_id === 'MULTI-PART') {
                $scope.compare_data_json = multipart.generateQuestionData();
            } else if ($scope.question_type_id === 'IN-CONTEXT') {
                $scope.compare_data_json = incontext.generateQuestionData();
            } else {
                $scope.compare_data_json = openended.generateQuestionData();
            }
            var i = 0;
            angular.forEach(angular.element(".requires"), function (value, key) {
                var a = angular.element(value);
                if (a.hasClass('ckeditor')) { // for all ckeditor
                    var field_value = common_assessment_utility.getFieldValue(a.prop('id'), true);
                    if (field_value.trim().length === 0) {
                        a.parent().addClass('has-error');
                        i++;
                    } else { // for all input box
                        a.parent().removeClass('has-error');
                    }
                } else {
                    if (a.val().trim().length === 0) {
                        a.parent().addClass('has-error');
                        i++;
                    } else {
                        a.parent().removeClass('has-error');
                    }
                }
            });
            var compare = JSON.stringify($scope.fetched_data) === JSON.stringify($scope.compare_data_json);
            if (!compare) {
                SweetAlert.swal({
                    title: "Are you sure?",
                    text: "You are leaving the authoring page",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Save it",
                    cancelButtonText: "No, Don't save",
                    closeOnConfirm: true,
                    closeOnCancel: true
                }, function (isConfirm) {
                    if (isConfirm) {
                        if ($scope.enableAdd) {
                            if (i === 0) {
                                $scope.addQuestions();
                            }
                            //$location.path('assessment_question/' + assessment_id + '/' + project_id);
                        } else {
                            if (i === 0) {
                                $scope.editQuestion(true);
                            }
                        }
                    } else {
                        $location.path('assessment_question/' + assessment_id + '/' + project_id + '/global_repo');
                    }
                });
            } else {
                $location.path('assessment_question/' + assessment_id + '/' + project_id + '/global_repo');
            }
        };
        $scope.addForm = function (size) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/assessment/questionTypeModal.html',
                controller: 'ModalInstanceQuestionCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormText: function () {
                        return "";
                    },
                    modalFormTile: function () {
                        return "Choose question type";
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    assessment_id: function () {
                        return $scope.assessment_id;
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    question_types: function () {
                        return $scope.question_types;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                $scope.assessment_id = modalCloseParameter.assessment_id;
                if (modalCloseParameter.global === '1') {
                    $location.path('assessment_question/' + modalCloseParameter.assessment_id + '/' + modalCloseParameter.project_id + '/' + modalCloseParameter.question_type_id + '/global_repo');
                } else {
                    $location.path('assessment_question/' + modalCloseParameter.assessment_id + '/' + modalCloseParameter.project_id + '/' + modalCloseParameter.question_type_id);
                }
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        $scope.addQuestions = function () {
            $scope.title = angular.element('#question_title').val();
            if (angular.element('#question_title').val().length < 256) {
                //$scope.stem = angular.element('#question_stem').val();
                $scope.stem = common_assessment_utility.getFieldValue('question_stem', true);
                var i = 0;
                var error = 0;
                angular.forEach(angular.element(".requires"), function (value, key) {
                    var a = angular.element(value);
                    var field_value = '';
                    if (a.hasClass('ckeditor')) {
                        field_value = common_assessment_utility.getFieldValue(a.context.id, true);
                    } else {
                        field_value = a.val();
                    }

                    //if (a.val().length === 0)
                    if (field_value.trim().length === 0) {
                        a.parent().addClass('has-error');
                        if (a.context.id.indexOf('Distractor') != -1) {
                            var id = a.context.id;
                            var idnum = id.replace(/Distractor/g, '');
                            if ($scope.question_type_id == 'FIB-DROPDOWN') {
                                fib.toggleOption(idnum);
                            }
                        }
                        i++;
                    } else {
                        a.parent().removeClass('has-error');
                    }
                });
                if (i === 0) {
                    if ($scope.question_type_id === 'MCQ') {
                        $scope.data_json = mcq.generateQuestionData();
                    } else if ($scope.question_type_id === 'FIB') {
                        var questionData = fib.generateQuestionData();
                        if (questionData == false) {
                            //                            if($('#textWithBlankErrorVisibility').val()=="true"){
                            //                                angular.element('.textWithBlankError').css('display', 'block');
                            //                            }
                            SweetAlert.swal("Error", 'Question can\'t be saved as it contains error', "error");
                            error = 1;
                        } else {
                            $scope.data_json = questionData;
                        }
                    } else if ($scope.question_type_id === 'FIB-DRAGDROP') {
                        var questionData = fibdrag.generateQuestionData();
                        if (questionData == false) {
                            //                             if($('#textWithBlankErrorVisibility').val()=="true"){
                            //                                angular.element('.textWithBlankError').css('display', 'block');
                            //                             }
                            SweetAlert.swal("Error", 'Question can\'t be saved as it contains error', "error");
                            error = 1;
                        } else {
                            $scope.data_json = questionData;
                        }
                    } else if ($scope.question_type_id === 'FIB-DROPDOWN') {
                        var questionData = fib.generateQuestionData();
                        if (questionData == false) {
                            //                             if($('#textWithBlankErrorVisibility').val()=="true"){
                            //                                angular.element('.textWithBlankError').css('display', 'block');
                            //                             }
                            SweetAlert.swal("Error", 'Question can\'t be saved as it contains error', "error");
                            error = 1;
                        } else {
                            $scope.data_json = questionData;
                        }
                    } else if ($scope.question_type_id === 'MULTI-PART') {
                        $scope.data_json = multipart.generateQuestionData();
                    } else if ($scope.question_type_id === 'IN-CONTEXT') {
                        $scope.data_json = incontext.generateQuestionData();
                    } else {
                        $scope.data_json = openended.generateQuestionData();
                    }

                    $scope.type = $scope.question_type_id;
                    if (!error) {
                        $scope.disableAdd = true;
                        assessmentService.addQuestion($scope, $rootScope).then(function (data) {
                            if (data.status === 200) {
                                SweetAlert.swal("Success", "Question has been successfully added", "success");
                                $scope.disableAdd = false;
                                if ($scope.location.$$path.indexOf('global_repo') !== -1) {
                                    $location.path('assessment_question/' + $scope.assessment_id + '/' + $scope.project_id + '/global_repo');
                                } else {
                                    $location.path('assessment_question/' + $scope.assessment_id + '/' + $scope.project_id);
                                }
                            } else {
                                SweetAlert.swal("Oops...", "Something went wrong!", "error");
                            }

                        });
                    }
                } else {
                    //SweetAlert.swal("Oops...", "Please Enter data into the blank fileds!", "error");
                }
            } else {
                SweetAlert.swal("Oops...", "Question title charecter should be not more than 255", "error");
            }

        };
        $scope.editQuestion = function (fromGoBack) {
            $scope.title = angular.element('#question_title').val();
            var error = 0;
            if (angular.element('#question_title').val().length < 256) {
                if ($scope.question_type_id === 'MCQ') {
                    $scope.data_json = mcq.generateQuestionData();
                } else if ($scope.question_type_id === 'FIB') {
                    var questionData = fib.generateQuestionData();
                    if (questionData == false) {
                        //                        if($('#textWithBlankErrorVisibility').val()=="true"){
                        //                            angular.element('.textWithBlankError').css('display', 'block');
                        //                        }
                        SweetAlert.swal("Error", 'Question can\'t be saved as it contains error', "error");
                        error = 1;
                    } else {
                        $scope.data_json = questionData;
                    }
                } else if ($scope.question_type_id === 'FIB-DRAGDROP') {
                    var questionData = fibdrag.generateQuestionData();
                    if (questionData == false) {
                        //                             if($('#textWithBlankErrorVisibility').val()=="true"){
                        //                                angular.element('.textWithBlankError').css('display', 'block');
                        //                             }
                        SweetAlert.swal("Error", 'Question can\'t be saved as it contains error', "error");
                        error = 1;
                    } else {
                        $scope.data_json = questionData;
                    }
                } else if ($scope.question_type_id === 'FIB-DROPDOWN') {
                    var questionData = fib.generateQuestionData();
                    if (questionData == false) {
                        //                             if($('#textWithBlankErrorVisibility').val()=="true"){
                        //                                angular.element('.textWithBlankError').css('display', 'block');
                        //                             }
                        SweetAlert.swal("Error", 'Question can\'t be saved as it contains error', "error");
                        error = 1;
                    } else {
                        $scope.data_json = questionData;
                    }
                } else if ($scope.question_type_id === 'MULTI-PART') {
                    $scope.data_json = multipart.generateQuestionData();
                } else if ($scope.question_type_id === 'IN-CONTEXT') {
                    $scope.data_json = incontext.generateQuestionData();
                } else {
                    $scope.data_json = openended.generateQuestionData();
                }

                $scope.fetched_data = $scope.data_json;
                $scope.type = $scope.question_type_id;
                var i = 0;
                angular.forEach(angular.element(".requires"), function (value, key) {
                    var a = angular.element(value);
                    var field_value = '';
                    if (a.hasClass('ckeditor')) {
                        field_value = common_assessment_utility.getFieldValue(a.context.id, true);
                    } else {
                        field_value = a.val();
                    }
                    if (field_value.trim().length === 0) {
                        a.parent().addClass('has-error');
                        i++;
                    } else {
                        a.parent().removeClass('has-error');
                    }
                });
                if (i == 0) {
                    if (!error) {
                        $scope.disableEdit = true;
                        assessmentService.editQuestion($scope, $rootScope).then(function (data) {
                            if (data.status == 200) {
                                $scope.disableEdit = false;
                                if ($('.textWithBlankError').parent().hasClass('has-error')) {
                                    $('.textWithBlankError').parent().removeClass('has-error')
                                }
                                SweetAlert.swal("Success", "Question has been successfully edited", "success");
                                if (angular.element("input[name='response_type']:checked").val() == 'with-response') {
                                    angular.element('#answer').val('');
                                    if (angular.element("input[name='grade_type']:checked").val() == 'with-grading') {
                                        angular.element('#sample_answer').val('');
                                        angular.element('#feedback').val('');
                                    } else {
                                        angular.element('#feedback_grade').val('');
                                    }
                                } else {
                                    angular.element('#sample_answer').val('');
                                    angular.element('#feedback').val('');
                                    angular.element('#feedback_grade').val('');
                                }
                                if (fromGoBack) {
                                    if ($scope.location.$$path.indexOf('global_repo') !== -1) {
                                        $location.path('assessment_question/' + $scope.assessment_id + '/' + $scope.project_id + '/global_repo');
                                    } else {
                                        $location.path('assessment_question/' + $scope.assessment_id + '/' + $scope.project_id);
                                    }
                                }
                            } else {
                                SweetAlert.swal("Oops...", "Something went wrong!", "error");
                            }
                        });
                    }
                }
            } else {
                SweetAlert.swal("Oops...", "Question title should be not more than 255 characters", "error");
            }
        };
        $scope.delete = function (id) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Question!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    assessmentService.delQuestion(id).then(function (data) {
                        if (data.status == 200) {
                            $scope.listQuestions(1);
                            SweetAlert.swal("Success", "Question has been deleted", "success");
                        } else {
                            SweetAlert.swal({
                                title: "Cancelled",
                                html: data.message,
                                type: "error",
                            });
                        }

                    });
                } else {
                    SweetAlert.swal("Cancelled", "Question is safe :)", "error");
                }
            });
        };
        $scope.media = {
            type: "no-media"
        };
        $scope.$watch('media.type', function (val) {
            if (val == 'no-media') {
                $scope.ext_res_block = false;
                angular.element('#ext_res_url').val('');
                angular.element('#ext_res_title').val('');
                angular.element('#ext_res_width').val('');
                angular.element('#ext_res_height').val('');
                $scope.ext_res_title = '';
                $scope.ext_res_title_id = '';
                $scope.ext_res_width = '';
                $scope.ext_res_height = '';
                $scope.ext_res_url = '';
                angular.element('#brightcove_button').text('Add Brightcove Video');
            } else {
                $scope.ext_res_block = true;
            }
        });
        $scope.add_media_button = false;
        $scope.addAssestModal = function (flag) {
            $scope.currentCkInstance = CKEDITOR.currentInstance;
            $scope.add_media_button = true;
            var size = 'lg';
            var modal_form_title = 'Asset Library';
            var modalInstance = $modal.open({
                templateUrl: 'templates/assessment/externalResourceModal.html',
                controller: 'ModalAssetsCtrl',
                size: size,
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return modal_form_title;
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    ext_res_url: function () {
                        return $scope.ext_res_url;
                    },
                    ext_res_title: function () {
                        return angular.element('#ext_res_title').val();
                    },
                    ext_res_type: function () {
                        return angular.element('#ext_res_type').val();
                    },
                    flag: function () {
                        return flag;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                // modalCloseParameter
                $scope.add_media_button = false;
                if (modalCloseParameter != null) {
                    $scope.ext_res_url = modalCloseParameter.ext_res_location;
                    $scope.ext_res_title = modalCloseParameter.ext_res_title;
                    $scope.ext_res_type = modalCloseParameter.ext_res_type;
                    angular.element('#ext_res_url').val($scope.ext_res_url);
                    angular.element('#ext_res_title').val($scope.ext_res_title);
                    angular.element('#ext_res_type').val($scope.ext_res_type);
                    angular.element('#ext_res_width').val('');
                    angular.element('#ext_res_height').val('');
                    $scope.ext_res_title_id = '';
                    $scope.ext_res_width = '';
                    $scope.ext_res_height = '';
                    angular.element('#media_type').val('assets');
                    if ($scope.ext_res_type == 'image' || $scope.ext_res_type == 'Image') {
                        $scope.ext_ico_block = true;
                    } else {
                        $scope.ext_ico_block = false;
                    }
                    $scope.add_media_button = false;
                }
            }, function () {
                $scope.add_media_button = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        $scope.addBrightCoveModal = function () {
            $scope.add_media_button = true;
            var size = 'md';
            var modal_form_title = 'Brightcove Configuration';
            var modalInstance = $modal.open({
                templateUrl: 'templates/assessment/brightcoveModal.html',
                controller: 'ModalBrightcoveCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return modal_form_title;
                    },
                    brightCoveId: function () {
                        return $scope.ext_res_title_id;
                    },
                    brightCoveWidth: function () {
                        return parseInt($scope.ext_res_width);
                    },
                    brightCoveHeight: function () {
                        return parseInt($scope.ext_res_height);
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                $scope.ext_res_url = modalCloseParameter.ext_res_location;
                $scope.ext_res_title = '<b>Video ID:</b> <br>' + modalCloseParameter.ext_res_title;
                $scope.ext_res_title_id = modalCloseParameter.ext_res_title;
                $scope.ext_res_width = parseInt(modalCloseParameter.ext_res_width);
                $scope.ext_res_height = parseInt(modalCloseParameter.ext_res_height);
                $scope.ext_res_type = modalCloseParameter.ext_res_type;
                angular.element('#ext_res_title').val($scope.ext_res_title);
                angular.element('#ext_res_url').val($scope.ext_res_url);
                angular.element('#ext_res_title_id').val($scope.ext_res_title_id);
                angular.element('#media_type').val('brightcove');
                angular.element('#ext_res_width').val($scope.ext_res_width);
                angular.element('#ext_res_height').val($scope.ext_res_height);
                angular.element('#brightcove_button').text('Edit Brightcove Video');
                $scope.ext_ico_block = false;
                $scope.add_media_button = false;
            }, function () {
                $scope.add_media_button = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        $scope.questionSelect = function () {
            var size = 'lg';
            var modal_form_title = 'Select Questions';
            var modalInstance = $modal.open({
                templateUrl: 'templates/assessment/questionModal.html',
                controller: 'ModalQuestionSelectCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return modal_form_title;
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    project_type_id: function () {
                        return $scope.project_type_id;
                    },
                    selected_questions: function () {
                        var subquestions = [];
                        var total_subquestions = $('.question-container').length;
                        for (var i = 1; i <= total_subquestions; i++) {
                            subquestions.push($('#selected_question_' + i).val());
                        }
                        return subquestions;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                $scope.selected_data = modalCloseParameter.selected_data;
                multipart.renderSubquestion(modalCloseParameter.selected_data);
            });
        };
        $scope.parseJson = function (json) {
            return JSON.parse(json);
        };
        $scope.deleteResource = function () {
            $scope.ext_res_url = '';
            $scope.ext_res_title = null;
            $scope.ext_res_type = '';
            $scope.ext_res_width = '';
            $scope.ext_res_height = '';
            $scope.ext_res_title_id = '';
            angular.element('#ext_res_title').val('');
            angular.element('#ext_res_url').val('');
            angular.element('#media_type').val('');
            angular.element('#ext_res_width').val('');
            angular.element('#ext_res_height').val('');
            angular.element('#brightcove_button').text('Add Brightcove Video');
        };
        //Preview Questions
        $scope.preview_button_disabled = false;
        $scope.preview_question = function (search_name) {
            $scope.preview_button_disabled = true;
            var size = 'lg';
            var modal_form_title = 'Question Preview';
            var modalInstance = $modal.open({
                templateUrl: 'templates/assessment/previewModal.html',
                controller: 'ModalPreviewCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return modal_form_title;
                    },
                    shortcode: function () {
                        return search_name;
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    project_type_id: function () {
                        return $scope.project_type_id;
                    },
                    entity_type_id: function () {
                        return 3;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
            }, function () {
                $scope.preview_button_disabled = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        // when authoring content is loaded including javascript
        $scope.contentLoaded = function () {
            if (angular.isDefined($routeParams.question_id)) { // edit questing
                $scope.enableEdit = true;
                setTimeout(function () {
                    $scope.getQuestionDetails($scope.assessment_id, $scope.question_id);
                }, 1800);
            }
        };
        $scope.checkType = function (question_type_id) {
            for (var i = 0; i < $scope.question_types.length; i++) {
                if ($scope.question_types[i].id == question_type_id) {
                    return $scope.question_types[i].name;
                }
            }
        };
        $scope.bindEvent = function (elem) {
            $scope.elem = elem;
            var modalInstance = $modal.open({
                template: '<div class="modal-header" style="background:white"><h4 class="modal-title" id="myModalLabel">Configure Image</h4></div><div class="modal-body"  style="background:white"><form class=""><div class="row"><div class="col-sm-5"><div class="form-group"><label class="control-label" for="width">Width:</label><input type="number" ng-model="imgWidth" ng-change="adjustAspectRatio(this)" class="form-control" id="width" placeholder="Enter width"></div></div><div class="col-sm-5"><div class="form-group"><label class="control-label" for="height">Height:</label><input type="number" ng-change="adjustAspectRatio(this)" class="form-control" ng-model="imgHeight" id="height" placeholder="Enter height"></div></div><div class="col-sm-2"><div class="form-group"><label class="control-label"></label><div style="margin-top: 5px;"><button ng-click="resetImageSize()" class="btn btn-default"><i class="fa fa-refresh" aria-hidden="true"></i></button></div></div></div><div class="col-sm-12"><div class="form-group"><label class="control-label" for="height">Alt Text:</label><input type="text"  class="form-control" ng-model="altText" id="height" placeholder="Enter alternative text"></div><div class="col-sm-12"><div class="form-group"><div><label class="control-label" for="height">Alignment:</label></div><label class="radio-inline"><input type="radio" name="inlineRadioOptions" alignImg="none" class="configRad"   id="inlineRadio1" checked value="option1">None</label><label class="radio-inline"><input type="radio" name="inlineRadioOptions" alignImg="left" class="configRad" id="inlineRadio2" value="option2">Left </label><label class="radio-inline"><input type="radio" name="inlineRadioOptions" alignImg="center" class="configRad"  id="inlineRadio3" value="option3">Center </label><label class="radio-inline"><input type="radio" name="inlineRadioOptions" alignImg="right" class="configRad"  id="inlineRadio4" value="option4">Right </label></div></div></div></form></div><div class="modal-footer"><div class="row"><div class="col-sm-4 text-left"><button ng-click="deleteImg()" class="btn btn-default">Delete image</button></div><div class="col-sm-8 text-right"><button type="button" class="btn btn-default" ng-click="dismissModal()" data-dismiss="modal">Cancel</button><button ng-click="ConfigureImg()" class="btn btn-primary">Submit</button></div></div></div>',
                backdrop: 'static',
                controller: 'ckImgController',
                scope: $scope,
                keyboard: false,
                resolve: {
                    elem: function () {
                        return $scope.elem;
                    }
                }
            });
            modalInstance.result.then(function (params) {
                var height = params.height;
                var width = params.width;
                var altText = params.altText;
                var radChecked = params.checked;
                $(params.elem.currentTarget).attr('height', height).attr('width', width).attr('alt', altText).attr('radChecked', radChecked);
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.jsRegisterer = function (elem) {
            $(elem).off().on('dblclick click', $scope.bindEvent.bind(this));
        };
        /*
         *@description : check the sentence format for FIB type,whether for example,[[1]] exists atleast one in the sentence
         *@author : Meghamala Mukherjee
         *@created on 21st August,2017
         */
        $scope.checkTextWithBlankFormat = function () {
            var queTextwithblanks = this.textwith_blanks;
            if (!queTextwithblanks.match(/(\[\[[1]]])/gi)) {
                angular.element('.textWithBlankError').css('display', 'block');
                this.textwith_blanks = '';
            } else {
                angular.element('.textWithBlankError').css('display', 'none');
            }
        };
    }]);
app.controller('ckImgController', function ($scope, $modalInstance, elem) {
    $scope.imgWidth = elem.currentTarget.clientWidth;
    $scope.imgHeight = elem.currentTarget.clientHeight;
    $scope.altText = elem.currentTarget.getAttribute('alt');
    window.setTimeout(function () {
        angular.element('#' + elem.currentTarget.getAttribute('radChecked')).prop('checked', true);
    }, 200);

    $scope.ConfigureImg = function () {
        var params = $scope.resizeImg();
        params.checked = angular.element('.configRad:checked').attr('id');
        $scope.imgAlignment(angular.element('.configRad:checked')[0]);

        $modalInstance.close(params);
    }
    $scope.dismissModal = function () {
        $modalInstance.dismiss('cancel');
    }
    $scope.adjustAspectRatio = function () {
        var trueWidth = $scope.getDimension().trueWidth;
        var trueHeight = $scope.getDimension().trueHeight;
        var htRatio = (trueHeight / (trueHeight + trueWidth));
        var widRatio = 1 - htRatio;
        if (event.target.getAttribute('id') === 'height') {
            var currHt = $scope.imgHeight;
            var newWidth = parseInt(currHt * widRatio / htRatio);
            $scope.imgWidth = newWidth;
        }
        if (event.target.getAttribute('id') === 'width') {
            var currWd = $scope.imgWidth;
            var newHeight = parseInt(currWd * htRatio / widRatio);
            $scope.imgHeight = newHeight;
        }

    }
    $scope.getDimension = function () {
        var dimension = {
            "trueWidth": elem.currentTarget.clientWidth,
            "trueHeight": elem.currentTarget.clientHeight
        }
        return dimension;
    }
    $scope.imgAlignment = function (element) {
        if (angular.element(elem.currentTarget).parents('p').length === 0) {
            angular.element(elem.target).wrap('<p></p>');
        }
        if (element.getAttribute('alignImg') === 'center') {
            angular.element(elem.currentTarget).parents('p')[0].style.textAlign = 'center';
            elem.currentTarget.removeAttribute('style');
        } else {
            elem.currentTarget.style.float = element.getAttribute('alignImg');
            angular.element(elem.currentTarget).parents('p')[0].removeAttribute('style');
        }
    }
    $scope.resetImageSize = function () {
        $scope.imgWidth = elem.currentTarget.naturalWidth;
        $scope.imgHeight = elem.currentTarget.naturalHeight;
    }
    $scope.deleteImg = function () {
        elem.currentTarget.remove();
        $modalInstance.dismiss('cancel');
    }
    $scope.resizeImg = function () {
        var params = {
            height: this.imgHeight,
            width: this.imgWidth,
            altText: this.altText,
            elem: elem
        }
        return params;

    }
});
app.controller('ModalPreviewCtrl', function ($scope, assessmentService, sessionService, $modalInstance, modalFormTitle, shortcode, project_id, project_type_id, entity_type_id) {
    $scope.modalFormTitle = modalFormTitle;
    $scope.repo_id = shortcode;
    $scope.project_id = project_id;
    $scope.project_type_id = project_type_id;
    $scope.entity_type_id = entity_type_id;
    assessmentService.getQuestionPreview($scope).then(function (data) {
        if (data.status === 200) {
            angular.element('#parentFrame').attr('src', data.widget_launch_file);
            angular.element('#parentFrame').attr('data-path', data.json_file_path);
            var lastPos;
            $(window).on("message", function (data) {
                //mock implementation of parents message listener
                if (data.originalEvent.data.height) {
                    if (data.originalEvent.data.type == 'adjustIframe') {
                        lastPos = $(window).scrollTop();
                    }


                    angular.element('#parentFrame').css({'height': data.originalEvent.data.height, overflow: data.originalEvent.data.overflow});
                    var scrollTo = angular.element('#parentFrame').offset().top;
                    if (data.originalEvent.data.scroll) {

                        $(window).scrollTop(scrollTo)
                    }
                    if (data.originalEvent.data.type == 'close') {
                        $(window).scrollTop(lastPos)
                    }

                }
                var iframeWindow = angular.element('#parentFrame')[0].contentWindow;
                var dataPath = angular.element('#parentFrame').attr("data-path");
                iframeWindow.postMessage({
                    "data_path": dataPath
                }, "*");
            });
        } else {

        }

    });
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
app.controller('ModalInstanceQuestionCtrl', function ($scope, $location, assessmentService, sessionService, $modalInstance, modalFormText, modalFormTile, project_id, assessment_id, project_permissions, question_types) {
    $scope.showedit = 0;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.project_id = project_id;
    $scope.assessment_id = assessment_id;
    $scope.question_types = question_types;
    $scope.location = $location;
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    $scope.project = {
        'project_permissions': project_permissions
    };
    $scope.add = function () {
        $scope.formError = {
            'question_type': ''
        };
        if (angular.isDefined($scope.question_type) && $scope.question_type != '') {
            var modalCloseParameter = {
                question_type_id: $scope.question_type.id,
                project_id: $scope.project_id,
                assessment_id: $scope.assessment_id
            };
            $modalInstance.close(modalCloseParameter);
        } else {
            $scope.formError = {
                'question_type': 'Choose a type'
            };
        }

    };
    $scope.addGlobal = function () {
        $scope.formError = {
            'question_type': ''
        };
        if (angular.isDefined($scope.question_type) && $scope.question_type != '') {
            var modalCloseParameter = {
                question_type_id: $scope.question_type.id,
                project_id: $scope.project_id,
                assessment_id: $scope.assessment_id,
                global: '1'
            };
            $modalInstance.close(modalCloseParameter);
        } else {
            $scope.formError = {
                'question_type': 'Choose a type'
            };
        }

    };
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
app.controller('ModalAssetsCtrl', function ($scope, $modalInstance, assetService, project_id, modalFormTitle, ext_res_url, ext_res_title, ext_res_type, objectService, SweetAlert, flag, keyValueService, tagService, taxonomyService, userService, ivhTreeviewMgr, $compile, $filter) {
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.ext_res_location = ext_res_url;
    $scope.modalFormTitle = modalFormTitle;
    $scope.project_id = project_id;
    $scope.taxonomy_id = 0;
    $scope.searchModel = {
        "search_text": ''
    };
    $scope.advanced_search = {
        "active": null
    };
    if (flag) {
        $scope.asset_filter = {
            "images": true,
            "videos": false,
            "gadgets": false
        };
    } else {
        $scope.asset_filter = {
            "images": true,
            "videos": true,
            "audio": true,
            "gadgets": false
        };
    }

    $scope.asset_local_global = {
        "local": true,
        "global": true
    };
    $scope.pageNumber = 1;
    $scope.itemsPerPage = 8;
    $scope.ext_res_location = ext_res_url;
    $scope.ext_res_title = ext_res_title;
    $scope.ext_res_type = ext_res_type;
    $scope.getAssets = function (pageNumber) {
        $scope.removeErrors = 1;
        $scope.showCheckAll = 0;
        $scope.showMakeGlobal = 0;
        $scope.pageNumber = pageNumber;
        $scope.itemsPerPage = 8;
        assetService.getMimes().then(function (mimes) {
            angular.element('#submit_resource').attr('disabled', 'disabled');
            $scope.mimes = mimes.data;
            assetService.getAssets($scope).then(function (data) {
                $scope.assets = data.data;
                angular.forEach($scope.assets, function (value, key) {
                    if (value.asset_location == $scope.ext_res_location) {
                        value.Selected = $scope.ext_res_location;
                        value.Style = "opacity:1 !important;";
                        angular.element('#check-box-label-' + $scope.asset_id).attr('style', 'opacity:1 !important;');
                        angular.element('#submit_resource').attr('disabled', false);
                        angular.element('#submit_resource').focus();
                    }
                    //                    else if ($scope.ext_res_location == '') {
                    //                        angular.element('#submit_resource').attr('disabled', 'disabled');
                    //                    } else {
                    //                        angular.element('#submit_resource').attr('disabled', false);
                    //                    }
                });
                $scope.totalAssets = data.totalAssets;
                $scope.removeErrors = angular.isDefined(data.totalAssets) ? data.totalAssets : 0;
            });
        });
    };
    //$scope.getAssets(1);

    //Get Project Detail
    $scope.getProject = function () {
        $scope.project_name = '';
        if ($scope.object_id != '') {
            objectService.getProjectDetails($scope).then(function (data) {
                $scope.project_name = data.name;
                $scope.project_type_id = data.project_type_id;
                $scope.toc_creation_status = data.toc_creation_status;
                $scope.project = data;
                $scope.actual_permission = data.project_permissions['asset.use_global'].grant;
            });
        }
    };
    $scope.getProject();
    //For Pagination Of Dashboard
    $scope.pageChangeHandlerAsset = function (newPage) {
        $scope.getAssets(newPage);
        if (angular.isDefined($scope.ext_res_location)) {
            angular.element('#submit_resource').removeAttr('disabled');
            angular.element('#submit_resource').focus();
        }

    };
    $scope.$watch('search_text', function (val) {
        $scope.advanced_search.active = false;
        $scope.advanced_search.enableSearch = false;
        $scope.searchModel = {
            "search_text": val
        };
        $scope.getAssets(1);
    });
    $scope.selectOption = function (asset_id, asset_location, title, type, is_global) {
        if (is_global === 1 && $scope.actual_permission === false) {
            angular.element('.label-checkbox').removeAttr('style');
            angular.element('#submit_resource').attr('disabled', 'disabled');
            SweetAlert.swal("Cancelled", "Permission Denied!", "error");
        } else {
            angular.element('.label-checkbox').removeAttr('style');
            angular.element('#check-box-label-' + asset_id).attr('style', 'opacity:1 !important;');
            $scope.ext_res_location = asset_location;
            $scope.ext_res_title = title;
            $scope.ext_res_type = type;
            $scope.asset_id = asset_id;
            angular.element('#submit_resource').attr('disabled', false);
            angular.element('#submit_resource').focus();
        }
    };
    $scope.submitResource = function (callby) {
        if ($scope.currentCkInstance) {
            var oEditor = $scope.currentCkInstance;
            var html = "<img class='ckImg' alt='' width='' height= '' src='" + $scope.ext_res_location + "'/>";

            var newElement = CKEDITOR.dom.element.createFromHtml(html, oEditor.document);
            oEditor.insertElement(newElement);
            var elem = oEditor.document.find('.ckImg').$;
            //oEditor.on('setData', $scope.jsRegisterer(oEditor.document.find('.ckImg').$));
            $(elem).off().on('click', $scope.bindEvent.bind(this));
            $modalInstance.close(null);
        } else {
            var modalCloseParameter = {
                ext_res_location: $scope.ext_res_location,
                ext_res_title: $scope.ext_res_title,
                ext_res_type: $scope.ext_res_type
            };
            $modalInstance.close(modalCloseParameter);
        }
    };

    //=========================================//
    //Advance Search For Metadata = Starts Here
    //=========================================//
    $scope.node_id = 0;
    $scope.advanced_search = {};
    $scope.advanced_search.enableSearch = false;
    $scope.advanced_search.title = '';
    $scope.advanced_search.active = false;
    $scope.advanced_search.isCollapsed = false;
    $scope.advanced_search.heading = 'Advanced Search Result:';

    $scope.advanced_search.key_data = {};
    $scope.advanced_search.value_data = {};
    $scope.advanced_search.totalKeyArea = 0;
    $scope.advanced_search.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';

    // get taxonomy
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200) {
            if (data.data != null && angular.isDefined(data.data.children)) {
                $scope.advanced_search.taxonomy = data.data.children;
                // expand nodes
                ivhTreeviewMgr.expandRecursive($scope.advanced_search.taxonomy, $scope.advanced_search.taxonomy);
            }
        }

    });

    // get tags
    var tags = [];
    var object_tags = [];


    $scope.advanced_search.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200) {
            if (data.data != null && angular.isDefined(data.data.tags)) {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.advanced_search.tag.data = tags;
                if (angular.isDefined(data.data.object_tags)) {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.advanced_search.tagValue = object_tags;
                }
            }
        }

    });

    // get owners
    var owners = [];
    var object_owners = [];

    $scope.advanced_search.owner = {
        multiple: true,
        data: owners
    };

    userService.getUsers($scope).then(function (data) {
        if (data.status == 200) {
            if (data.data != null) {
                angular.forEach(data.data, function (value, key) {
                    owners[key] = {id: value.id, text: value.username};
                });
                $scope.advanced_search.owner.data = owners;
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
        //        if (data.projectPermissionGranted == 401) {
        //            SweetAlert.swal("Cancelled", "Access prohibited!", "error");
        //            $location.path("dashboard");
        //        }
        if (data.status == 200) {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values)) {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + $filter('strLimit')(value.key_name, 20) + '</option>';
                    }
                });
                setTimeout(function () {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }, 500);
            }

            if (data.count == 0) {
                setTimeout(function () {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }, 500);
            }
        }
    });


    $scope.addKeyValue = function () {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.advanced_search.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId) {
                if (value.key_type == 'select_list') {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="advanced_search.value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + valueObj.value + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    //$scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="advanced_search.value_data.key_' + key + '" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                } else if (value.key_type == 'time_entry') {
                    //$scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" data-time-type="string" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="advanced_search.value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    //$scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.advanced_search.key_data['key_' + keyArray[1]];
        delete $scope.advanced_search.value_data['key_' + keyArray[1]];
        if (angular.element(".advanced-search-key-values-area").length == 0) {
            $scope.advanced_search.totalKeyArea = 1;
            $scope.advanced_search.key_data = {};
            $scope.advanced_search.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
            angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="row advanced-search-key-values-area" id="advancedSearchKeyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="advanced_search.key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group adv-input-group" id="advancedSearchKeyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
        return keySelect;
    };

    $scope.applyAdvancedSearch = function () {
        $scope.advanced_search.heading = 'Advanced Search Result:';
        $scope.advanced_search.enableSearch = true;
        $scope.advanced_search.active = true;
        $scope.advanced_search.isCollapsed = false;
        $scope.getAssets(1);
    };

    $scope.clearSearch = function () {
        $scope.advanced_search.active = false;
        $scope.search_text = '';
        $scope.advanced_search.enableSearch = false;
        $scope.getAssets(1);
    };

    $scope.toggleAdvanceSearch = function () {
        if ($scope.advanced_search.isCollapsed) {
            $scope.advanced_search.isCollapsed = false;
            if ($scope.advanced_search.active) {
                $scope.advanced_search.enableSearch = true;
            }
        } else {
            $scope.advanced_search.isCollapsed = true;
            $scope.advanced_search.enableSearch = false;
        }
    };

    $scope.applyAdvancedClear = function () {
        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
        angular.element("#advancedSearchKeyValuesAreaId").html($compile(newKeyAreaHtml)($scope));
        $scope.advanced_search.tagValue = [];
        $scope.advanced_search.ownerValue = [];
        $scope.advanced_search.title = '';
        $scope.advanced_search.key_data = {};
        $scope.advanced_search.value_data = {};
        ivhTreeviewMgr.deselectAll($scope.advanced_search.taxonomy);
    };

    $scope.generateAdvanceSearchTerm = function () {
        var searchTerm = [];
        var tagList = $scope.advanced_search.tagValue;
        if (tagList.length > 0) {
            angular.forEach(tagList, function (tag) {
                searchTerm.push(tag.text);
            });
        }
        var ownerList = $scope.advanced_search.ownerValue;
        if (ownerList.length > 0) {
            angular.forEach(ownerList, function (owner) {
                searchTerm.push(owner.text);
            });
        }
        var taxonomyList = $scope.selectedTaxonomy;
        if (taxonomyList.length > 0) {
            angular.forEach(taxonomyList, function (taxonomy) {
                searchTerm.push(taxonomy);
            });
        }

        if ($scope.advanced_search.title != '') {
            searchTerm.push($scope.advanced_search.title);
        }

    };

    $scope.selectedTaxonomy = [];
    $scope.selectedTaxonomyIds = [];

    $scope.$watch('advanced_search.taxonomy', function (newValues) {
        $scope.selectedTaxonomy.length = 0;
        $scope.selectedTaxonomyIds.length = 0;
        angular.forEach(newValues, function (item) {
            if (item.selected == true) {
                $scope.selectedTaxonomy.push(item.title);
                $scope.selectedTaxonomyIds.push(item.id);
            }
        });
    }, true);







});
app.controller('ModalBrightcoveCtrl', function ($scope, $modalInstance, modalFormTitle, brightCoveId, brightCoveWidth, brightCoveHeight) {
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
    $scope.modalFormTitle = modalFormTitle;
    $scope.brightcove_id = brightCoveId;
    $scope.brightcove_video_width = brightCoveWidth;
    $scope.brightcove_video_height = brightCoveHeight;
    $scope.submitResource = function () {
        var i = 0;
        angular.forEach(angular.element(".requires-modal"), function (value, key) {
            var a = angular.element(value);
            if (a.val().length === 0) {
                a.parent().addClass('has-error');
                i++;
            } else {
                a.parent().removeClass('has-error');
            }
        });
        if (i == 0) {
            //var brightcove_url = 'https://players.brightcove.net/2402232200001/default_default/index.html?videoId=' + $scope.brightcove_id;
            var brightcove_url = bright_cove_url + $scope.brightcove_id;
            var modalCloseParameter = {
                ext_res_location: brightcove_url,
                ext_res_title: $scope.brightcove_id,
                ext_res_type: 'brightcove',
                ext_res_width: $scope.brightcove_video_width,
                ext_res_height: $scope.brightcove_video_height
            };
            $modalInstance.close(modalCloseParameter);
        }
    };
});
app.controller('ModalQuestionSelectCtrl', ['$scope', 'assessmentService', 'sessionService', '$modalInstance', 'modalFormTitle', 'project_id', 'project_type_id', 'selected_questions', function ($scope, assessmentService, sessionService, $modalInstance, modalFormTitle, project_id, project_type_id, selected_questions) {
        $scope.modalFormTitle = modalFormTitle;
        $scope.isNodeAddDisabled = true;
        $scope.project_id = project_id;
        $scope.project_type_id = project_type_id;
        $scope.selected_questions = [];
        $scope.selected_data = selected_questions;
        var currentLocation = window.location;
        (window.location.href).indexOf('global_repo') >= 0 ? $scope.assessment_local_global = {local: false, global: true} : $scope.assessment_local_global = {local: true, global: false};
        // $scope.assessment_local_global = { local: true, global: false };
        $scope.listAssessmentsData = function () {
            $scope.itemsPerPage = -1;
            assessmentService.getAssessments($scope, $scope.project_id).then(function (data) {
                $scope.assessmentList = data.data.assessment_list;
                $scope.assessmentsTotal = data.data.total;
                angular.forEach($scope.assessmentList, function (value, key) {
                    $scope.assessment_id = value.id;
                    $scope.listQuestionsChecked();
                });
            });
        };
        $scope.$watch('assessment', function (val) {
            if (angular.isDefined(val)) {
                $scope.questionTotal = 0;
                $scope.assessment_id = val;
                $scope.listQuestions();
            }
        });
        //Fetch Questions
        $scope.questionTotal = 0;
        $scope.pageNumber = 1;
        $scope.itemsPerPage = 10;
        $scope.currentPage = 1;

        $scope.listQuestions = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 5;
            $scope.excludeType = 'MULTI-PART';
            assessmentService.getQuestions($scope).then(function (data) {
                $scope.questionList = data.data.question_list;
                $scope.questionTotal = data.data.total;
                angular.forEach($scope.questionList, function (value, key) {
                    angular.forEach($scope.selected_data, function (value1, key1) {
                        if (parseInt(value1) === value.question_id) {
                            $scope.questionList[key].checked = true;
                        }
                    });
                });
            });
        };
        $scope.listQuestionsChecked = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 5;
            assessmentService.getQuestions($scope).then(function (data) {
                angular.forEach(data.data.question_list, function (value, key) {
                    angular.forEach(selected_questions, function (value1, key1) {
                        if (parseInt(value1) === value.question_id) {
                            //$scope.IfCheck(data.data.question_list[key].data, true, value.question_id);
                            var dataJson = JSON.parse(data.data.question_list[key].data);
                            dataJson.question_id = parseInt(value.question_id);
                            var dataJson = JSON.stringify(dataJson);
                            $scope.selected_questions.push(dataJson);
                        }
                    });
                });
                $scope.isNodeAddDisabled = true;
            });
        };
        $scope.IfCheck = function (data, check, question_id) {
            $scope.isNodeAddDisabled = false;
            var dataJson = JSON.parse(data);
            dataJson.question_id = parseInt(question_id);
            var dataJson = JSON.stringify(dataJson);
            if (check) {
                $scope.selected_questions.push(dataJson);
                $scope.selected_data.push(JSON.stringify(question_id));
            } else {
                var index = $scope.selected_questions.indexOf(dataJson);
                $scope.selected_questions.splice(index, 1);
                var index1 = $scope.selected_data.indexOf(JSON.stringify(question_id));
                $scope.selected_data.splice(index1, 1);
            }
        };
        $scope.selected_questions_array = [];
        var j = 0;
        $scope.reOrder = function () {
            selected_questions.forEach(function (key) {
                for (var i = 0; i < $scope.selected_questions.length; i++) {
                    if (JSON.parse($scope.selected_questions[i]).question_id == parseInt(key)) {
                        $scope.selected_questions_array[j] = $scope.selected_questions[i];
                        j++;
                    }
                }
            });
            $scope.selected_questions = $scope.selected_questions_array;
        };

        setTimeout(function () {
            $scope.reOrder();
        }, 2000);
        $scope.submitResource = function () {
            var modalCloseParameter = {
                selected_data: $scope.selected_questions
            };
            $modalInstance.close(modalCloseParameter);
        };
        $scope.arrayObjectIndexOf = function (myArray, searchTerm, property) {
            for (var i = 0, len = myArray.length; i < len; i++) {
                if (myArray[i][property] === searchTerm)
                    return i;
            }
            return -1;
        };
        //For Page Change from paging
        $scope.pageChangeHandlerQuestion = function (newPage) {
            //$scope.assessment_id = '';
            $scope.listQuestions(newPage);
        };
        $scope.listAssessmentsData();
        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
var common_assessment_utility = {};
common_assessment_utility.getFieldValue = function (field_key, isEditor) {
    var value = '';
    if (isEditor) {
        value = CKEDITOR.instances[field_key].getData();
        value = value.replace('class="math_edit"', '');
        value = value.replace(' >', '>');
        var regex = /<br\s*[\/]?>/gi;
        value = value.replace(regex, "");
        var replace_value = value;
        value = value.replace(/\s|&nbsp;/g, '');
        value = value.replace(/\s|<p><\/p>/g, '');
        if (value.length != 0) {
            value = replace_value;
        } else {
            value = '';
        }
    } else {
        value = jQuery('#' + field_key).val();
    }
    return value;
}

common_assessment_utility.setFieldValue = function (field_key, value, isEditor) {
    if (isEditor) {
        //var pp =CKEDITOR.instances[field_key].getData();
        value = CKEDITOR.instances[field_key].setData(value);
        //var pp = CKEDITOR.instances[field_key].getData();
    } else {
        value = jQuery('#' + field_key).val(value);
    }
}

// initialize ckeditor
common_assessment_utility.initializeCkeditor = function () {
    $('.ckeditor').each(function (e) {
        if (!CKEDITOR.instances[this.id]) {
            CKEDITOR.replace(this.id);
        }
    });
}

common_assessment_utility.ckeditorBindEvent = function () {
    for (name in CKEDITOR.instances) {
        var instance = CKEDITOR.instances[name];
        instance.on('change', function (e) {
            if (this.getData().trim().length > 0) {
                $('#' + this.name).parent().removeClass('has-error');
            } else {
                $('#' + this.name).parent().addClass('has-error');
                $('.textWithBlankError').parent().removeClass('has-error');
            }
        });

        instance.on('mode', function () {
            if (this.mode == 'wysiwyg') {
                angular.element(document.getElementById('questionController')).scope().jsRegisterer(this.document.find('.ckImg').$);
            }
        });

        if (typeof instance.document !== "undefined") {
            angular.element(document.getElementById('questionController')).scope().jsRegisterer(instance.document.find('.ckImg').$);
        }
    }
}