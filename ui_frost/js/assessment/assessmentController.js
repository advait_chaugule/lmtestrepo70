/*****
 * Controller for project
 * Author - Arikh Akher * 
 */

'use strict';
app.controller('assessment', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'assessmentService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', function ($scope, $http, sessionService, $location, objectService, assessmentService, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter) {

        $scope.access_token = sessionService.get('access_token');
        $scope.addAssessment = false;
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.project_id = $routeParams.project_id;
        $scope.project_type_id = 4;
        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
        $scope.show_make_global = false;
        $scope.project_name = '';
        $scope.getProject = function () {
            if ($scope.project_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    if (data == null) {
                        $location.path("edit_toc/" + $scope.project_id);
                    } else if (angular.isObject(data)) {
                        $scope.project_name = data.name;
                        $scope.project = data;
                        $scope.applyPermission = 1;
                        $scope.project.project_permissions = data.project_permissions;
                    }
                });
            }
        }
        $scope.getProject();

        $scope.assessment_local_global = {};
        $scope.assessment_local_global.local = true;
        $scope.assessment_local_global.global = false;
        
        $scope.pageNumber = 1;
        $scope.itemsPerPage = 10;
        $scope.currentPage = 1;
        $scope.listAssessments = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 10;
            assessmentService.getAssessments($scope, $scope.project_id).then(function (data) {
                $scope.assessmentList = data.data.assessment_list;
                $scope.assessmentsTotal = data.data.total;
            });
        };
        $scope.listAssessments(1);
        //For Pagination Of Dashboard
        $scope.pageChangeHandlerAssessment = function (newPage) {
            $scope.assessment_id = '';
            $scope.listAssessments(newPage);
        };

        $scope.addForm = function (size) {
            //$scope.child = child;
            $scope.addAssessment = true;
            $scope.show_make_global = false;
            var modalInstance = $modal.open({
                templateUrl: 'templates/assessment/myModalContent.html',
                controller: 'ModalInstanceAssessmentCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    assessmentRec: function () {
                        return '';
                    },
                    modalFormText: function () {
                        return "You are trying to add a new assessment";
                    },
                    modalFormTile: function () {
                        return "Add Assessment";
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_theme: function () {
                        return $scope.project.theme;
                    },
                    show_make_global: function () {
                        return $scope.show_make_global;
                    }
                }
            });
            modalInstance.result.then(function () {
                //Saving TOC    
                $scope.addAssessment = false;
                $scope.assessment_id = '';
                assessmentService.getAssessments($scope, $scope.project_id).then(function (data) {
                    $scope.assessmentList = data.data.assessment_list;
                    $scope.assessmentsTotal = data.data.total;
                });
            }, function () {
                $scope.addAssessment = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.edit = function (size, id) {
            var assessmentRec = {};
            $scope.assessment_id = id;
            $scope.show_make_global = true;
            angular.element('#editAssessment' + id).attr('disabled', 'disabled');
            assessmentService.getAssessments($scope, $scope.project_id).then(function (data) {
                assessmentRec = data.data.assessment_list;
                var assessmentTitle = assessmentRec[0].title;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/assessment/myModalContent.html',
                    controller: 'ModalInstanceAssessmentCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        assessmentRec: function () {
                            return assessmentRec;
                        },
                        modalFormText: function () {
                            return "You are trying to edit assessment [" + $filter('strLimit')(assessmentTitle, 20) + "]";
                        },
                        modalFormTile: function () {
                            return "Edit Assessment";
                        },
                        project_id: function () {
                            return $scope.project_id;
                        },
                        project_permissions: function () {
                            return $scope.project.project_permissions;
                        },
                        project_theme: function () {
                            return $scope.project.theme;
                        },
                        show_make_global: function () {
                            return $scope.show_make_global;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    $scope.assessment_id = '';
                    assessmentService.getAssessments($scope, $scope.project_id).then(function (data) {
                        $scope.assessmentList = data.data.assessment_list;
                        $scope.assessmentsTotal = data.data.total;
                    });
                }, function () {
                    angular.element('#editAssessment' + id).removeAttr('disabled');
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };

        $scope.delete = function (id, project_id) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this assessment!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    assessmentService.delAssessment(id, project_id).then(function (data) {
                        if (data.status == 200) {
                            $scope.listAssessments(1);
                            SweetAlert.swal("Success", "Assessment has been deleted", "success");
                        } else {
                            SweetAlert.swal({
                                title: "Cancelled",
                                html: data.message,
                                type: "error",
                            });
                        }
                    });

                } else {
                    SweetAlert.swal("Cancelled", "Assessment is safe :)", "error");
                }
            });
        };

        $scope.preview_button_disabled = false;
        $scope.preview_question = function (search_name) {
            $scope.preview_button_disabled = true;
            var size = 'lg';
            var modal_form_title = 'Assessment Preview';
            var modalInstance = $modal.open({
                templateUrl: 'templates/assessment/previewModal.html',
                controller: 'ModalPreviewCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return modal_form_title;
                    },
                    shortcode: function () {
                        return search_name;
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    project_type_id: function () {
                        return $scope.project_type_id;
                    },
                    entity_type_id: function () {
                        return 2;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                $scope.preview_button_disabled = false;
            }, function () {
                $scope.preview_button_disabled = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

    }]);

app.controller('ModalInstanceAssessmentCtrl', function ($scope, $location, assessmentService, sessionService, SweetAlert, $modalInstance, modalFormText, modalFormTile, assessmentRec, project_id, project_permissions, project_theme, show_make_global, $filter, tagService, keyValueService, taxonomyService, $compile, ivhTreeviewMgr) {
    $scope.showedit = 0;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.project_id = project_id;
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    $scope.project = {'project_permissions': project_permissions};
    $scope.pattern_classes = [{value: ''}];
    $scope.project_theme = project_theme;
    $scope.make_global = 0;
    $scope.show_make_global = show_make_global;
    $scope.disable_global_checkbox = false;
    $scope.key_data = {};
    $scope.value_data = {};
    $scope.totalKeyArea = 0;
    $scope.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';
    $scope.metadata = {};
    if (project_permissions['tag.apply'].grant) {
        $scope.applyMetadata = 1;
    } else {
        $scope.applyMetadata = 0;
    }
    if (assessmentRec.length) {
        if (assessmentRec[0].is_global == 1) {
            $scope.disable_global_checkbox = true;
        }
        $scope.assessment_id = assessmentRec[0].id;
        $scope.title = assessmentRec[0].title;
        $scope.showedit = assessmentRec.length;
    } else {
        $scope.showedit = 0;
    }


    // taxonomy 
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.assessment_id).then(function (data) {
        if (data.status == 200) {
            if (data.data != null && angular.isDefined(data.data.children)) {
                $scope.taxonomy = data.data.children;
                $scope.findSelectedTaxonomy({children: $scope.taxonomy});
            }
        }
        if (data.count === 0) {
            setTimeout(function () {
                angular.element('#taxonomyStatus').html('No active taxonomy available in the management');
            }, 500);
        }
    });
    //This method is used to select parent indeterminately
    $scope.findSelectedTaxonomy = function (taxonomy_arr) {
        if (taxonomy_arr.children.length > 0) {
            for (var i = 0; i < taxonomy_arr.children.length; i += 1) {
                var currentChild = taxonomy_arr.children[i];
                $scope.findSelectedTaxonomy(currentChild);
                if (currentChild.selected == true) {
                    //console.log(currentChild);
                    ivhTreeviewMgr.select($scope.taxonomy, currentChild);
                }
            }
        }
    };
    // get tags 
    var tags = [];
    var object_tags = [];
    $scope.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.assessment_id).then(function (data) {
        if (data.status == 200) {
            if (data.data != null && angular.isDefined(data.data.tags)) {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.tag.data = tags;
                if (angular.isDefined(data.data.object_tags))
                {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.metadata.tagValue = object_tags;
                }
            }
        }
    });
    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.assessment_id).then(function (data) {
        if (data.status == 200) {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + $filter('strLimit')(value.key_name, 20) + '</option>';
                    }
                });
                /*setTimeout(function () {
                 $scope.totalKeyArea = $scope.totalKeyArea + 1;
                 var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                 angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                 }, 500);*/
            }

            /*if (data.count == 0) {
             setTimeout(function () {
             $scope.totalKeyArea = $scope.totalKeyArea + 1;
             var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
             angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
             }, 500);
             }*/
            setTimeout(function () {
                // populate old key values
                if (Object.keys(data.data.object_key_values).length)
                {
                    angular.forEach(data.data.object_key_values, function (object_key_value) {
                        $scope.totalKeyArea = $scope.totalKeyArea + 1;
                        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                        $scope.key_data['key_' + $scope.totalKeyArea] = object_key_value.key_id;
                        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                        $scope.onChangeKeyUpdateValue($scope.totalKeyArea);
                        if (object_key_value.key_type == 'select_list')
                        {
                            $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value_id;
                        } else {
                            if (object_key_value.key_type == 'number_entry') {
                                $scope.value_data['key_' + $scope.totalKeyArea] = parseFloat(object_key_value.value);
                            } else {
                                $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value;
                            }
                        }

                    });
                } else {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }
            }, 500);
        }
    });
    $scope.addKeyValue = function () {
        $scope.totalKeyArea = $scope.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };
    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + $filter('strLimit')(valueObj.value, 20) + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-keypress="keyPressedEvent($event)" ng-model="value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="date2" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'time_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" ng-keypress="keyPressedEvent($event)" data-time-type="string" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" step="any" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.keyPressedEvent = function (keyEvent) {
        keyEvent.preventDefault();
    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.key_data['key_' + keyArray[1]];
        delete $scope.value_data['key_' + keyArray[1]];
        if (angular.element(".key-values-area").length == 0)
        {
            $scope.totalKeyArea = 1;
            $scope.key_data = {};
            $scope.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="key-values-area row" id="keyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group" id="keyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
        return keySelect;
    };







    $scope.add = function () {
        $scope.formError = {'title': ''};
        if ($scope.applyMetadata) {
            $scope.tags = $scope.metadata.tagValue;
        } else {
            $scope.tags = [];
            $scope.key_data = {};
            $scope.value_data = {};
            $scope.taxonomy = [];
        }
        assessmentService.saveAssessment($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
//                assessmentService.getAssessments($scope).then(function (data) {
//                    $scope.assessmentList = data.data.assessment_list;
//                    $scope.assessmentsTotal = data.data.total;
                $modalInstance.close();
//                });
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['title'] = savedResponse.error.title['0'];
                }
            }

        });
        //}
    };

    $scope.edit = function () {
        $scope.formError = {'title': ''};
        if ($scope.applyMetadata) {
            $scope.tags = $scope.metadata.tagValue;
        } else {
            $scope.tags = [];
            $scope.key_data = {};
            $scope.value_data = {};
            $scope.taxonomy = [];
        }
        assessmentService.editAssessment($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
//                assessmentService.getAssessments($scope).then(function (data) {
//                    $scope.assessmentList = data.data.assessment_list;
                $modalInstance.close();
//                });
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['title'] = savedResponse.error.title['0'];
                }
            }

        });
    };

    $scope.cancel = function () {
        if ($scope.disable_global_checkbox == true && $scope.make_global == 1) {
            $modalInstance.close();
        } else {
            $modalInstance.dismiss('cancel');
        }
    };

    $scope.makeGlobal = function (object_id) {
        if (!$scope.make_global) {
            $scope.make_global = 1;
        } else {
            $scope.make_global = 0;
            return false;
        }
        SweetAlert.swal({
            title: "Are you sure?",
            text: _lang['content_global_confirm_message'],
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, change it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var ids = [];
                ids.push(object_id);
                ids = JSON.stringify(ids);
                assessmentService.makeGlobal(ids, $scope).then(function (data) {
                    //console.log(data);
                    if (data.status === 200) {
                        //$scope.show_make_global = false;
                        $scope.disable_global_checkbox = true;
//                        assessmentService.getAssessments($scope).then(function (data) {
//                            $scope.assessmentList = data.data.assessment_list;
//                            //$modalInstance.close();
//                            //$location.path("assessment/"+$scope.project_id);
//                        });
                    }
                });
                SweetAlert.swal("Success", "Assessment access is set to global", "success");
            } else {
                $scope.make_global = 0;
                $('#global_check').attr('checked', false);
                $scope.$apply();
                SweetAlert.swal("Cancelled", "Assessment is safe :)", "error");
            }
        });
    }

});

app.controller('ModalPreviewCtrl', function ($scope, assessmentService, sessionService, $modalInstance, modalFormTitle, shortcode, project_id, project_type_id, entity_type_id) {

    $scope.modalFormTitle = modalFormTitle;
    $scope.repo_id = shortcode;
    $scope.project_id = project_id;
    $scope.project_type_id = project_type_id;
    $scope.entity_type_id = entity_type_id;

    assessmentService.getQuestionPreview($scope).then(function (data) {
        if (data.status === 200) {
            angular.element('#parentFrame').attr('src', data.widget_launch_file);
            angular.element('#parentFrame').attr('data-path', data.json_file_path);
            var lastPos;
            $(window).on("message", function (data) {
                if (data.originalEvent.data.height) {
                    if (data.originalEvent.data.type == 'adjustIframe') {
                        lastPos = $(window).scrollTop();
                    }


                    angular.element('#parentFrame').css({'height': data.originalEvent.data.height, overflow: data.originalEvent.data.overflow});
                    var scrollTo = angular.element('#parentFrame').offset().top;
                    if (data.originalEvent.data.scroll) {

                        $(window).scrollTop(scrollTo)
                    }
                    if (data.originalEvent.data.type == 'close') {
                        $(window).scrollTop(lastPos)
                    }

                }
                var iframeWindow = angular.element('#parentFrame')[0].contentWindow;
                var dataPath = angular.element('#parentFrame').attr("data-path");
                iframeWindow.postMessage({
                    "data_path": dataPath
                }, "*");
            });

        } else {

        }

    });

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
/*
 * @author Meghamala Mukherjee
 * @description assessment for global repository
 * @dated 8-9-2017
 */
app.controller('global_assessment', ['$scope', '$http', 'sessionService', '$location', 'objectService', 'assessmentService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', '$filter', '$window', function ($scope, $http, sessionService, $location, objectService, assessmentService, $rootScope, $modal, $log, SweetAlert, $routeParams, $filter, $window) {
        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.user_id = userinfo.id;

        $scope.project_type_id = 5;
        objectService.getGlobalProjectId($scope).then(function (data) {
            if (data == null) {
                $location.path("edit_toc/" + $scope.project_id);
            } else if (angular.isObject(data)) {
                $scope.project_id = data.global_project_id;
                objectService.getProjectDetails($scope).then(function (data) {
                    if (data == null) {
                        $location.path("edit_toc/" + $scope.project_id);
                    } else if (angular.isObject(data)) {
                        $scope.project_name = data.name;
                        $scope.project = data;
                        $scope.applyPermission = 1;
                        $scope.project.project_permissions = data.project_permissions;
                    }
                });
            }
        });


        $scope.currentOrderByField = 'title';
        $scope.reverseSort = false;


        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }

        $scope.template_local_global = {};
        $scope.template_local_global.local = false;
        $scope.template_local_global.global = false;
        $scope.is_global = 0;
        if ($scope.user_permissions['global_template.show.all'].grant) {
            $scope.template_local_global.global = true;
        }

        $scope.pageNumber = 1;
        $scope.currentPage = 1;
        $scope.itemsPerPage = 10;
        $scope.addGlobalAssesmentForm = function (size) {
            //$scope.child = child;
            $scope.addAssessment = true;
            $scope.show_make_global = false;
            var modalInstance = $modal.open({
                templateUrl: 'templates/assessment/myModalContent.html',
                controller: 'ModalInstanceGlobalAssessmentCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    assessmentRec: function () {
                        return '';
                    },
                    modalFormText: function () {
                        return "You are trying to add a new assessment";
                    },
                    modalFormTile: function () {
                        return "Add Assessment";
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    project_permissions: function () {
                        return $scope.project.project_permissions;
                    },
                    project_theme: function () {
                        return $scope.project.theme;
                    },
                    show_make_global: function () {
                        return $scope.show_make_global;
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.addAssessment = false;
                $scope.assessment_id = '';
                $scope.listAssessments(1);
            }, function () {
                $scope.addAssessment = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
        $scope.listAssessments = function (pageNumber) {
            $scope.currentPage = pageNumber;
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 10;
            $scope.is_global = 1;
            assessmentService.getGlobalAssessments($scope).then(function (data) {
                $scope.templateList = data.data.assessment_list;
                $scope.totalTemplates = data.data.total;
            });
        };
        $scope.listAssessments(1);

        //For Pagination Of Dashboard
        $scope.pageChangeHandlerAssessment = function (newPage) {
            $scope.listAssessments(newPage);
        };

        $scope.changeStatus = function (id) {
            assessmentService.changeStatus(id, $scope).then(function () {
                $scope.id = '';
            });
        };

        $scope.applySorting = function (field) {
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.listAssessments(1);
        };

        $scope.preview_button_disabled = false;
        $scope.preview_question = function (search_name) {
            $scope.preview_button_disabled = true;
            var size = 'lg';
            var modal_form_title = 'Assessment Preview';
            var modalInstance = $modal.open({
                templateUrl: 'templates/assessment/previewModal.html',
                controller: 'GlobalModalPreviewCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return modal_form_title;
                    },
                    shortcode: function () {
                        return search_name;
                    },
                    entity_type_id: function () {
                        return 2;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                $scope.preview_button_disabled = false;
            }, function () {
                $scope.preview_button_disabled = false;
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.edit = function (size, id) {
            var assessmentRec = {};
            $scope.assessment_id = id;
            $scope.show_make_global = false;
            angular.element('#editAssessment' + id).attr('disabled', 'disabled');
            assessmentService.getAssessments($scope, $scope.project_id).then(function (data) {
                assessmentRec = data.data.assessment_list;
                var assessmentTitle = assessmentRec[0].title;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/assessment/myModalContent.html',
                    controller: 'ModalInstanceAssessmentCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        assessmentRec: function () {
                            return assessmentRec;
                        },
                        modalFormText: function () {
                            return "You are trying to edit assessment [" + $filter('strLimit')(assessmentTitle, 20) + "]";
                        },
                        modalFormTile: function () {
                            return "Edit Assessment";
                        },
                        project_id: function () {
                            return $scope.project_id;
                        },
                        project_permissions: function () {
                            return $scope.project.project_permissions;
                        },
                        project_theme: function () {
                            return $scope.project.theme;
                        },
                        show_make_global: function () {
                            return $scope.show_make_global;
                        }
                    }
                });
                modalInstance.result.then(function () {
                    $scope.assessment_id = '';
                    assessmentService.getGlobalAssessments($scope).then(function (data) {
                        $scope.templateList = data.data.assessment_list;
                        $scope.totalTemplates = data.data.total;
                    });
                }, function () {
                    angular.element('#editAssessment' + id).removeAttr('disabled');
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };

        $scope.delete = function (id, project_id) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this assessment!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    assessmentService.delAssessment(id, project_id).then(function (data) {
                        if (data.status == 200) {
                            $scope.listAssessments(1);
                            SweetAlert.swal("Success", "Assessment has been deleted", "success");
                        } else {
                            SweetAlert.swal({
                                title: "Cancelled",
                                html: data.message,
                                type: "error",
                            });
                        }
                    });

                } else {
                    SweetAlert.swal("Cancelled", "Assessment is safe :)", "error");
                }
            });
        };
        $scope.$on('apply-search', function (event, args) {
            $scope.listAssessments(1);
        });

        $scope.$on('apply-advaced-search', function (event, args) {
            $scope.listAssessments(1);
        });

        $scope.$on('clear-advaced-search', function (event, args) {
            $scope.listAssessments(1);
        });


    }]);


app.controller('ModalInstanceGlobalAssessmentCtrl', function ($scope, $location, assessmentService, sessionService, SweetAlert, $modalInstance, modalFormText, modalFormTile, assessmentRec, project_id, project_permissions, project_theme, show_make_global, $filter, tagService, keyValueService, taxonomyService, $compile, ivhTreeviewMgr) {
    $scope.showedit = 0;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.project_id = project_id;
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    $scope.project = {'project_permissions': project_permissions};
    $scope.pattern_classes = [{value: ''}];
    $scope.project_theme = project_theme;
    $scope.make_global = 0;
    $scope.is_global = 1;
    $scope.show_make_global = show_make_global;
    $scope.disable_global_checkbox = false;
    $scope.key_data = {};
    $scope.value_data = {};
    $scope.totalKeyArea = 0;
    $scope.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';
    $scope.metadata = {};
    if (project_permissions['tag.apply'].grant) {
        $scope.applyMetadata = 1;
    } else {
        $scope.applyMetadata = 0;
    }
    if (assessmentRec.length) {
        if (assessmentRec[0].is_global == 1) {
            $scope.disable_global_checkbox = true;
        }
        $scope.assessment_id = assessmentRec[0].id;
        $scope.title = assessmentRec[0].title;
        $scope.showedit = assessmentRec.length;
    } else {
        $scope.showedit = 0;
    }


    // taxonomy 
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.assessment_id).then(function (data) {
        if (data.status == 200) {
            if (data.data != null && angular.isDefined(data.data.children)) {
                $scope.taxonomy = data.data.children;
                $scope.findSelectedTaxonomy({children: $scope.taxonomy});
            }
        }
        if (data.count === 0) {
            setTimeout(function () {
                angular.element('#taxonomyStatus').html('No active taxonomy available in the management');
            }, 500);
        }
    });
    //This method is used to select parent indeterminately
    $scope.findSelectedTaxonomy = function (taxonomy_arr) {
        if (taxonomy_arr.children.length > 0) {
            for (var i = 0; i < taxonomy_arr.children.length; i += 1) {
                var currentChild = taxonomy_arr.children[i];
                $scope.findSelectedTaxonomy(currentChild);
                if (currentChild.selected == true) {
                    //console.log(currentChild);
                    ivhTreeviewMgr.select($scope.taxonomy, currentChild);
                }
            }
        }
    };
    // get tags 
    var tags = [];
    var object_tags = [];
    $scope.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.assessment_id).then(function (data) {
        if (data.status == 200) {
            if (data.data != null && angular.isDefined(data.data.tags)) {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.tag.data = tags;
                if (angular.isDefined(data.data.object_tags))
                {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.metadata.tagValue = object_tags;
                }
            }
        }
    });
    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.assessment_id).then(function (data) {
        if (data.status == 200) {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    if (value.key_name != 'License Type' && value.key_name != 'License Valid Till') {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + $filter('strLimit')(value.key_name, 20) + '</option>';
                    }
                });
                /*setTimeout(function () {
                 $scope.totalKeyArea = $scope.totalKeyArea + 1;
                 var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                 angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                 }, 500);*/
            }

            /*if (data.count == 0) {
             setTimeout(function () {
             $scope.totalKeyArea = $scope.totalKeyArea + 1;
             var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
             angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
             }, 500);
             }*/
            setTimeout(function () {
                // populate old key values
                if (Object.keys(data.data.object_key_values).length)
                {
                    angular.forEach(data.data.object_key_values, function (object_key_value) {
                        $scope.totalKeyArea = $scope.totalKeyArea + 1;
                        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                        $scope.key_data['key_' + $scope.totalKeyArea] = object_key_value.key_id;
                        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                        $scope.onChangeKeyUpdateValue($scope.totalKeyArea);
                        if (object_key_value.key_type == 'select_list')
                        {
                            $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value_id;
                        } else {
                            if (object_key_value.key_type == 'number_entry') {
                                $scope.value_data['key_' + $scope.totalKeyArea] = parseFloat(object_key_value.value);
                            } else {
                                $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value;
                            }
                        }

                    });
                } else {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }
            }, 500);
        }
    });
    $scope.addKeyValue = function () {
        $scope.totalKeyArea = $scope.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };
    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + $filter('strLimit')(valueObj.value, 20) + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-keypress="keyPressedEvent($event)" ng-model="value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="date2" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'time_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" ng-keypress="keyPressedEvent($event)" data-time-type="string" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" step="any" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };

    $scope.keyPressedEvent = function (keyEvent) {
        keyEvent.preventDefault();
    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.key_data['key_' + keyArray[1]];
        delete $scope.value_data['key_' + keyArray[1]];
        if (angular.element(".key-values-area").length == 0)
        {
            $scope.totalKeyArea = 1;
            $scope.key_data = {};
            $scope.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="key-values-area row" id="keyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group" id="keyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
        return keySelect;
    };







    $scope.add = function () {
        $scope.formError = {'title': ''};
        if ($scope.applyMetadata) {
            $scope.tags = $scope.metadata.tagValue;
        } else {
            $scope.tags = [];
            $scope.key_data = {};
            $scope.value_data = {};
            $scope.taxonomy = [];
        }
        assessmentService.saveAssessment($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
//                assessmentService.getAssessments($scope).then(function (data) {
//                    $scope.assessmentList = data.data.assessment_list;
//                    $scope.assessmentsTotal = data.data.total;
                $modalInstance.close();
//                });
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['title'] = savedResponse.error.title['0'];
                }
            }

        });
        //}
    };

    $scope.edit = function () {
        $scope.formError = {'title': ''};
        if ($scope.applyMetadata) {
            $scope.tags = $scope.metadata.tagValue;
        } else {
            $scope.tags = [];
            $scope.key_data = {};
            $scope.value_data = {};
            $scope.taxonomy = [];
        }
        assessmentService.editAssessment($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
//                assessmentService.getAssessments($scope).then(function (data) {
//                    $scope.assessmentList = data.data.assessment_list;
                $modalInstance.close();
//                });
            } else {
                if (angular.isDefined(savedResponse.error.title)) {
                    $scope.formError['title'] = savedResponse.error.title['0'];
                }
            }

        });
    };

    $scope.cancel = function () {
        if ($scope.disable_global_checkbox == true && $scope.make_global == 1) {
            $modalInstance.close();
        } else {
            $modalInstance.dismiss('cancel');
        }
    };

    $scope.makeGlobal = function (object_id) {
        if (!$scope.make_global) {
            $scope.make_global = 1;
        } else {
            $scope.make_global = 0;
            return false;
        }
        SweetAlert.swal({
            title: "Are you sure?",
            text: _lang['content_global_confirm_message'],
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, change it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                var ids = [];
                ids.push(object_id);
                ids = JSON.stringify(ids);
                assessmentService.makeGlobal(ids, $scope).then(function (data) {
                    //console.log(data);
                    if (data.status === 200) {
                        //$scope.show_make_global = false;
                        $scope.disable_global_checkbox = true;
//                        assessmentService.getAssessments($scope).then(function (data) {
//                            $scope.assessmentList = data.data.assessment_list;
//                            //$modalInstance.close();
//                            //$location.path("assessment/"+$scope.project_id);
//                        });
                    }
                });
                SweetAlert.swal("Success", "Assessment access is set to global", "success");
            } else {
                $scope.make_global = 0;
                $('#global_check').attr('checked', false);
                $scope.$apply();
                SweetAlert.swal("Cancelled", "Assessment is safe :)", "error");
            }
        });
    }

});

/*
 * @author Meghamala Mukherjee
 * @description global assess preview modal
 * @dated 13/9/2017
 */
app.controller('GlobalModalPreviewCtrl', function ($scope, assessmentService, sessionService, $modalInstance, modalFormTitle, shortcode, entity_type_id) {

    $scope.modalFormTitle = modalFormTitle;
    $scope.repo_id = shortcode;
    $scope.entity_type_id = entity_type_id;

    assessmentService.getGlobalQuestionPreview($scope).then(function (data) {
        if (data.status === 200) {
            angular.element('#parentFrame').attr('src', data.widget_launch_file);
            angular.element('#parentFrame').attr('data-path', data.json_file_path);
            var lastPos;
            $(window).on("message", function (data) {
                if (data.originalEvent.data.height) {
                    if (data.originalEvent.data.type == 'adjustIframe') {
                        lastPos = $(window).scrollTop();
                    }


                    angular.element('#parentFrame').css({'height': data.originalEvent.data.height, overflow: data.originalEvent.data.overflow});
                    var scrollTo = angular.element('#parentFrame').offset().top;
                    if (data.originalEvent.data.scroll) {

                        $(window).scrollTop(scrollTo)
                    }
                    if (data.originalEvent.data.type == 'close') {
                        $(window).scrollTop(lastPos)
                    }

                }
                var iframeWindow = angular.element('#parentFrame')[0].contentWindow;
                var dataPath = angular.element('#parentFrame').attr("data-path");
                iframeWindow.postMessage({
                    "data_path": dataPath
                }, "*");
            });

        } else {

        }

    });

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});
