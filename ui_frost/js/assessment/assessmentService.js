/*****
 * Specific services Assessments 
 */
'use strict';
app.service('assessmentService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        /*
         * Service to get assessments         
         */
        this.getAssessments = function ($scope, project_id) {
            var deferred = $q.defer();

            var params = {};

            params.access_token = sessionService.get('access_token');
            params.assessment_local_global = JSON.stringify($scope.assessment_local_global);
            if ($scope.search_text) {
                params.search_text = window.btoa($scope.search_text);
            }

            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                params.pageNumber = $scope.pageNumber;
                params.itemsPerPage = $scope.itemsPerPage;
            }

            if (angular.isDefined($scope.assessment_id) && $scope.assessment_id != '') {
                params.assessment_id = $scope.assessment_id;
            }

            // for advance search
            if (angular.isDefined($scope.advanced_search) && angular.isDefined($scope.advanced_search.active) && $scope.advanced_search.active) {
                params.advanced_search_enable = $scope.advanced_search.active;
                params.tags = JSON.stringify($scope.advanced_search.tagValue);
                params.owners = JSON.stringify($scope.advanced_search.ownerValue);
                params.key_data = JSON.stringify($scope.advanced_search.key_data);
                params.value_data = JSON.stringify($scope.advanced_search.value_data);
                params.taxonomy = JSON.stringify($scope.advanced_search.taxonomy);
                params.advanced_search_title = window.btoa($scope.advanced_search.title);
                var deferred = $q.defer();
                $http.post(serviceEndPoint + 'api/v1/assessment-search/project_id/' + project_id, params)
                        .success(function (data, status, headers, config) {
                            //console.log(data);
                            deferred.resolve(data);
                        }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            } else {
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: serviceEndPoint + 'api/v1/assessment/project_id/' + project_id,
                    params: params
                }).success(function (data, status, headers, config) {
                    //console.log(data);
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            }

            return deferred.promise;
        };
        //End - Method to get Assessment
        /*
         * Service to create Assessment         
         */
        this.saveAssessment = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/assessment/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), {
                project_id: $scope.project_id,
                title: $scope.title,
                is_global: $scope.is_global,
                tags: (angular.isUndefined($scope.tags) ? '' : JSON.stringify($scope.tags)),
                key_data: (angular.isUndefined($scope.key_data) ? '' : JSON.stringify($scope.key_data)),
                value_data: (angular.isUndefined($scope.value_data) ? '' : JSON.stringify($scope.value_data)),
                taxonomy: (angular.isUndefined($scope.taxonomy) ? '' : JSON.stringify($scope.taxonomy))
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }
        /*
         * Service to create Assessment         
         */
        this.editAssessment = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/assessment/assessment_id/' + $scope.assessment_id + '?access_token=' + sessionService.get('access_token'), {
                title: $scope.title,
                _method: 'PUT',
                project_id: $scope.project_id,
                tags: (angular.isUndefined($scope.tags) ? '' : JSON.stringify($scope.tags)),
                key_data: (angular.isUndefined($scope.key_data) ? '' : JSON.stringify($scope.key_data)),
                value_data: (angular.isUndefined($scope.value_data) ? '' : JSON.stringify($scope.value_data)),
                taxonomy: (angular.isUndefined($scope.taxonomy) ? '' : JSON.stringify($scope.taxonomy))
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /*
         * Delete Assessment         
         */
        this.delAssessment = function (assessment_id, project_id) {
            var deferred = $q.defer();
            $http.delete(serviceEndPoint + 'api/v1/assessment/assessment_id/' + assessment_id + '?access_token=' + sessionService.get('access_token'), {
                project_id: project_id,
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        /*
         * Service to get Questions         
         */
        this.getQuestions = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }

            if (angular.isDefined($scope.excludeType)) {
                queryString += '&excludeType=' + $scope.excludeType;
            }

            $http.get(serviceEndPoint + 'api/v1/assessment-question-list/assessment_id/' + $scope.assessment_id + '/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the question list             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        /*
         * Service to get Questions Types       
         */
        this.getQuestionTypes = function ($scope) {
            var deferred = $q.defer();
            $http.get(serviceEndPoint + 'api/v1/question-type?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return question type       
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        /*
         * Service to get Questions Detailss       
         */
        this.getQuestionDetails = function ($scope) {
            var deferred = $q.defer();
            $http.get(serviceEndPoint + 'api/v1/question-details/assessment_id/' + $scope.assessment_id + '/question_id/' + $scope.question_id + '?access_token=' + sessionService.get('access_token'), {
                project_id: $scope.project_id
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the Question Details             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /*
         * Delete Questions         
         */
        this.delQuestion = function (question_id) {
            var deferred = $q.defer();
            $http.delete(serviceEndPoint + 'api/v1/assessment-question/question_id/' + question_id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.addQuestion = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/assessment-question/assessment_id/' + $scope.assessment_id + '?access_token=' + sessionService.get('access_token'), {
                data_json: JSON.stringify($scope.data_json),
                title: $scope.title,
                type: $scope.type,
                project_id: $scope.project_id
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        };

        this.editQuestion = function ($scope, $rootScope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/question-details/assessment_id/' + $scope.assessment_id + '/question_id/' + $scope.question_id + '?access_token=' + sessionService.get('access_token'), {
                data_json: JSON.stringify($scope.data_json),
                title: $scope.title,
                type: $scope.type,
                project_id: $scope.project_id,
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /*
         * Service to get Questions Preview       
         */
        this.getQuestionPreview = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&project_id=' + $scope.project_id + '&project_type_id=' + $scope.project_type_id + '&repo_id=' + $scope.repo_id + '&entity_type_id=' + $scope.entity_type_id;
            $http.get(serviceEndPoint + 'api/v1/assessment-object-preview?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the Question Details             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };
        this.getGlobalAssessments = function ($scope) {
            var params = {};
            var deferred = $q.defer();

            if (angular.isDefined($scope.currentOrderByField)) {
                params.orderField = $scope.currentOrderByField;
            }

            if (angular.isDefined($scope.searchModel.search_text)) {
                params.search_text = window.btoa($scope.searchModel.search_text);
            }

            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                params.orderDirection = dir;
            }
            params.access_token = sessionService.get('access_token');
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                params.pageNumber = $scope.pageNumber;
                params.itemsPerPage = $scope.itemsPerPage;
            }

            var deferred = $q.defer();
            // for advance search
            if (angular.isDefined($scope.advanced_search.active) && $scope.advanced_search.active) {
                params.advanced_search_enable = $scope.advanced_search.active;
                params.tags = JSON.stringify($scope.advanced_search.tagValue);
                params.owners = JSON.stringify($scope.advanced_search.ownerValue);
                params.key_data = JSON.stringify($scope.advanced_search.key_data);
                params.value_data = JSON.stringify($scope.advanced_search.value_data);
                params.taxonomy = JSON.stringify($scope.advanced_search.taxonomy);
                params.advanced_search_title = window.btoa($scope.advanced_search.title);
                params.access_token = sessionService.get('access_token');

                $http.post(serviceEndPoint + 'api/v1/global-assessment-search', params)
                        .success(function (data, status, headers, config) {
                            deferred.resolve(data);
                        }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            } else {
                $http({
                    method: 'GET',
                    url: serviceEndPoint + 'api/v1/global-assessment',
                    params: params
                }).success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            }
            return deferred.promise;
        };
        /*
         * Change status to active or inactive
         */
        this.changeStatus = function (assessment_id, $scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/assessment/change_status/' + assessment_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        /*
         * Service to get Questions Preview global      
         */
        this.getGlobalQuestionPreview = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&repo_id=' + $scope.repo_id + '&entity_type_id=' + $scope.entity_type_id;
            $http.get(serviceEndPoint + 'api/v1/assessment-global-preview?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the Question Details             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };
        /*
         * service to make global the assessment
         */
        this.makeGlobal = function (ids, $scope) {
            var deferred = $q.defer();

            $http.post(serviceEndPoint + 'api/v1/change-assessment-access?access_token=' + sessionService.get('access_token') + '&assess_id=' + ids + '&is_global=' + $scope.make_global, {
            }).success(function (data, status, headers, config) {
//                if (data.projectPermissionGranted == 401) {
//                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted);
//                }
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
    }]);

