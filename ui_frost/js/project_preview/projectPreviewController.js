/*****
 * Controller for TOC
 * Author - Arikh Akher *
 */

'use strict';
var access_token = '';
var returnPatterns = '';
var toc_type = '';
var tooltipDesc = '';
var indexValue = '';
var parentIndex = "";
app.controller('project_preview', ['$scope', 'sessionService', 'tocService', '$location', '$routeParams', '$route', '$rootScope', 'objectService', 'SweetAlert', '$sce', '$window', function($scope, sessionService, tocService, $location, $routeParams, $route, $rootScope, objectService, SweetAlert, $sce, $window) {

    $scope.access_token = sessionService.get('access_token');
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
    // passing access_token to simple editor
    access_token = $scope.access_token;
    $scope.user_id = sessionService.get('uid');
    $scope.toc_index = [];
    $scope.file_toc_index = [];
    if (!$scope.access_token && !$scope.user_id) {
        $location.path('logout');
    } else {

        $scope.project_id = $routeParams.project_id;
        // Fetch Object Details
        objectService.getProjectDetails($scope).then(function(data) {
            $scope.project_details = data;
            $scope.toc_creation_status = data.toc_creation_status;
            $scope.data = JSON.parse(data.toc);
            $scope.iterateToc($scope.data.children);
            setTimeout(function() {
                angular.element('.link-to-preview').triggerHandler('click');
                //$(".link-to-preview").eq(0).trigger("click").addClass("active");
            }, 1000);

        });

        $scope.toggleMinimized = function(child) {
            child.minimized = !child.minimized;
        };

        $scope.iterateToc = function(toc) {
            angular.forEach(toc, function(value) {
                // console.log(value);
                if (value.file != "") {
                    $scope.file_toc_index.push(value);
                    $scope.toc_index.push(value.file);
                }
                if (angular.isDefined(value.children) && value.children.length > 0) {
                    $scope.iterateToc(value.children);
                }
            });
        };

        $scope.errorMessage = '';
        $scope.content = '';
        $scope.object_details = {};
        $scope.file_name = '';
        $scope.disableback = false;
        $scope.disablenext = false;
        $scope.contentPreview = function(file_name, node_type, linkClass) {
            if (file_name.indexOf("/widgets/") != -1 && file_name.indexOf("/launch.html") == -1) {
                file_name += '/launch.html';
            }
            if (node_type == 213) {
                file_name = file_name;
            } else {
                if (file_name.lastIndexOf("/") >= 0) {
                    var slashIndex = file_name.lastIndexOf("/") + 1;
                    file_name = file_name.substr(slashIndex);
                }
            }
            setTimeout(function() {
                var t = $(".header-container").outerHeight(),
                    e = $(window).innerHeight(),
                    n = $(".page-title").outerHeight(),
                    i = e - t - n - 55,
                    tocHeight = i - parseInt(jQuery(".navigator").css("height")) - 27;
                $(".toccontent-container").css("height", tocHeight);
            }, 500);
            $scope.reference = '';
            if (file_name.indexOf("#") >= 0) {
                $scope.reference = file_name.split('#')[1];
                $scope.file_name = file_name.substring(0, file_name.indexOf("#"));
                indexValue = ($scope.toc_index.indexOf(file_name));
                // $scope.page_number = $scope.renderHTML($scope.page_number_index[indexValue]);
                $scope.errorColor = '';
                for (var index = 0; index < $scope.toc_index.length; ++index) {
                    var value = $scope.toc_index[index];
                    if (value.substring(0, file_name.length) === file_name) {
                        var checkValue = index;
                        break;
                    }
                }
            } else {
                $scope.file_name = file_name;
                indexValue = ($scope.toc_index.indexOf($scope.file_name));
                // $scope.page_number = $scope.renderHTML($scope.page_number_index[indexValue]);
                $scope.errorColor = '';
                for (var index = 0; index < $scope.toc_index.length; ++index) {
                    var value = $scope.toc_index[index];
                    if (value == null) {
                        index++;
                    } else if (value.substring(0, $scope.file_name.length) === $scope.file_name) {
                        var checkValue = index;
                        break;
                    }
                }
            }

            if (node_type == 213 || linkClass === 'ulink') {
                objectService.getObjectDetailsByFilename($scope, $scope.project_id, $scope.file_name).then(function(data) {

                    if (data.status === 400 && data.message == "External links cannot be previewed.") {
                        var a = $('.link-to-preview');
                        var url = file_name;
                        a.each(function(key, item) {
                            var linkUrl = $(item).attr('data-link');
                            if ((typeof linkUrl) !== 'undefined' && String(url.toLowerCase()) === String(linkUrl.toLowerCase())) {
                                $(".link-to-preview").removeClass("active");
                                $(item).addClass("active");
                            }
                        });
                        $window.open(file_name, '_blank');
                    }
                });
            }

            if (checkValue) {
                indexValue = checkValue;
            }

            if ($scope.toc_index.length >= 1) {
                if (indexValue == -1) {
                    if ($scope.toc_index.indexOf($scope.file_name) > 0) {
                        var key = $scope.toc_index.indexOf($scope.file_name);

                        // $(".copyright-text").attr("title", $scope.node_title_index[key]);
                        // $(".copyright-text").html($scope.node_title_index[key]);
                        // $scope.page_number = $scope.renderHTML($scope.page_number_index[key]);
                        $scope.errorColor = '';
                        indexValue = key;
                    } else {
                        var search_term = $scope.file_name; // search term
                        var arr = jQuery.grep($scope.toc_index, function(value) {
                            value = value.split("#");
                            return value[0].match(search_term) ? value : null;
                        });

                        var key = $scope.toc_index.indexOf(arr[0]);

                        // $(".copyright-text").attr("title", $scope.node_title_index[key]);
                        // $(".copyright-text").html($scope.node_title_index[key]);
                        // $scope.page_number = $scope.renderHTML($scope.page_number_index[key]);
                        $scope.errorColor = '';
                        indexValue = key;
                    }
                }
                if (indexValue == 0) {
                    $scope.disableback = true;
                    jQuery("#backBtn").css("opacity", '0.5');
                } else {
                    $scope.disableback = false;
                    jQuery("#backBtn").css("opacity", '1');
                    sessionService.set('oldIndexValue', indexValue);
                }
                if (indexValue == (($scope.toc_index.length) - 1)) {
                    $scope.disablenext = true;
                    jQuery("#nextBtn").css("opacity", '0.5');
                } else {
                    $scope.disablenext = false;
                    jQuery("#nextBtn").css("opacity", '1');
                    sessionService.set('oldIndexValue', indexValue);
                }
            } else {
                $scope.disableback = true;
                $scope.disablenext = true;
                jQuery("#nextBtn").css("opacity", '0.5');
                jQuery("#backBtn").css("opacity", '0.5');
            }
            objectService.getObjectDetailsByFilename($scope, $scope.project_id, $scope.file_name).then(function(data) {
                $scope.content = '';
                if (angular.isDefined(data) && data.status == '200') {
                    $scope.object_details = data.data;
                    $scope.content = data.data.objects.description.replace("<![CDATA[<![CDATA[", "").replace("]]]]>", "").replace("<![CDATA[>]]>", "");
                    $scope.theme_location = data.data.theme_location;
                    $scope.js_location = data.data.js_location;
                    //$scope.file_path = data.data.s3_file_path + '#' + $scope.reference;// + $scope.ts_hash; //ts_hash path is removed for redirection
                    $scope.file_path = '';
                    $scope.frameHgt = '';
                    $scope.frameWidth = '';
                    $scope.file_path = $sce.trustAsResourceUrl($scope.file_path);
                    $scope.getActualPath = file_name; // + $scope.ts_hash;
                    $scope.errorMessage = '';

                    if ($scope.reference) {
                        var url = file_name;
                    } else {
                        var url = data.data.s3_file_path.split("/").pop(-1);
                    }
                    $scope.ts_hash = '?_ts=' + new Date().getTime();
                    $scope.s3filePath = data.data.s3_file_path.split('html')[0];
                    $scope.file_path = data.data.s3_file_path + '#' + $scope.reference + $scope.ts_hash;
                    $scope.glossary_link_path = data.data.glossary_js_link + $scope.ts_hash;
                    $scope.file_path = $sce.trustAsResourceUrl($scope.file_path);
                    $scope.glossary_link_path = $sce.trustAsResourceUrl(data.data.glossary_js_link);

                    // setTimeout(function() {
                    //     setContentIntoIframe();
                    // }, 500);
                    setTimeout(function() {
                        var anchorNodes = $('.link-to-preview');
                        for (var i = 0; i < anchorNodes.length; i++) {
                            if ($(anchorNodes[i]).hasClass('active')) {
                                $(anchorNodes[i]).removeClass('active');
                            }
                        }
                        $("#" + data.data.objects.id).addClass('active');
                        var scrollToPos = $(".link-to-preview.active").offset().top + $(".toccontent-container").scrollTop() - $('.toccontent-container').offset().top - 20;
                        $('.toccontent-container').animate({ scrollTop: scrollToPos }, 0);
                    }, 600);
                } else {
                    $scope.errorMessage = data.message;
                    if (angular.isDefined($scope.errorMessage)) {
                        //SweetAlert.swal("Error", $scope.errorMessage, "info");
                        angular.element("#content_preview_iframe").contents().find("body").html("<div style='color: #AD0505; padding: 22%;'>" + $scope.errorMessage + "</div>");
                    }
                }
            });
        };



        $scope.next = function() {
            if (jQuery(".link-to-preview.active").length > 0) {
                var file_name = jQuery(".link-to-preview.active").attr("data-link");
                if (file_name.indexOf("#") >= 0) {
                    file_name.substring(0, file_name.indexOf("#"));
                }
                indexValue = $scope.toc_index.indexOf(file_name);
            }
            if (parentIndex != "") {
                if ($scope.file_toc_index[parentIndex].minimized) {
                    $scope.toggleMinimized($scope.file_toc_index[parentIndex]);
                }
            }
            indexValue++;
            $scope.contentPreview($scope.toc_index[indexValue]);
        };
        $scope.prev = function() {
            if (jQuery(".link-to-preview.active").length > 0) {
                var file_name = jQuery(".link-to-preview.active").attr("data-link");
                if (file_name.indexOf("#") >= 0) {
                    file_name.substring(0, file_name.indexOf("#"));
                }
                indexValue = $scope.toc_index.indexOf(file_name);
            }
            if (indexValue == -1) {
                indexValue = sessionService.get('oldIndexValue');
            } else {
                indexValue--;
            }
            // $scope.contentPreview($scope.toc_index[indexValue], $scope.type[indexValue], $scope.node_title_index[indexValue]);
            $scope.contentPreview($scope.toc_index[indexValue]);
            for (var i = 0; i < $scope.file_toc_index.length; i++) {
                if ($scope.file_toc_index[i].children.length > 0) {
                    if ($scope.file_toc_index[i].minimized) {
                        $scope.toggleMinimized($scope.file_toc_index[i]);
                    }
                }
            }

        };

        $scope.previewProject = function() {
            $scope.errorMessage = '';
            $scope.content = '';
            $scope.object_details = {};
        };

        $scope.previews = [{ "id": "normal", "title": "Normal View" }, { "id": "shell1", "title": "Shell 1 View" }, { "id": "shell2", "title": "Shell 2 View" }];

        $scope.change_shell_view = function(opt) {
            $scope.toc_html = "";
            if ($scope.data) {
                $scope.create_toc($scope.data.children);
            }
            $scope.toc_html_final = "<ul style='list-style-type: none;' id='toc'>" + $scope.toc_html + "</ul>";

            $scope.show_view_pane = opt;
            if (opt != 'normal') {
                $scope.frameSrc = s3_url + "/shell/" + opt + "/index.html?pid=" + $scope.project_id;
            }
            setTimeout(function() {
                angular.element($('.link-to-preview')[0]).triggerHandler('click');
                //$(".link-to-preview").eq(0).trigger("click").addClass("active");
            }, 1000);
            // angular.element($('.link-to-preview')[0]).triggerHandler('click');
        };
        $(window).on("message", function(data) {
            var pathObj = {
                path: $scope.s3filePath,
                identifier: 'shell',
                toc_html: $scope.toc_html_final
            }
            if (data.originalEvent.data === 'shell') {
                $(shell_preview_frame)[0].contentWindow.postMessage(pathObj, '*');
            }
        });


        $scope.create_toc = function(toc, className) {
            angular.forEach(toc, function(value, key) {
                var ext_res = value.ext_res_type;
                if (ext_res) {
                    $scope.toc_html += "<li style='list-style-type: none;'><a class='" + className + "' target='_blank' rel='" + parseInt(key + 1) + "' id='" + value.id + "' href='" + value.ext_res_link + "';>" + value.title + "</a>";
                    if (value.children.length > 0) {
                        $scope.toc_html += "<ul style='list-style-type: none;' class='leftSelectionPanelToc'>";
                        $scope.create_toc(value.children, 'tocItem');
                        $scope.toc_html += "</ul>";
                    }
                    $scope.toc_html += "</li>";
                } else {
                    $scope.toc_html += "<li style='list-style-type: none;'><a class='" + className + "' rel='" + parseInt(key + 1) + "' id='" + value.id + "' href='javascript:void(0);' file='" + value.file + "' onClick=get_page(\"" + value.id + "\",\"" + value.file + "\");>" + value.title + "</a>";
                    if (value.children.length > 0) {
                        $scope.toc_html += "<ul style='list-style-type: none;' class='leftSelectionPanelToc'>";
                        $scope.create_toc(value.children, 'tocItem');
                        $scope.toc_html += "</ul>";
                    }
                    $scope.toc_html += "</li>";
                }


            });
        };

    }
}]);