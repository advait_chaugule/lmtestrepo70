/*****
 * Controller for TOC
 * Author - Arikh Akher * 
 */

'use strict';
var access_token = '';
var returnPatterns = '';
var toc_type = '';
var tooltipDesc = '';
app.controller('search_replace', ['$scope', 'sessionService', 'tocService', '$location', '$routeParams', '$route', '$rootScope', 'objectService', 'SweetAlert', '$sce', '$filter', '$window', '$modal', function ($scope, sessionService, tocService, $location, $routeParams, $route, $rootScope, objectService, SweetAlert, $sce, $filter, $window, $modal) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        // passing access_token to simple editor
        access_token = $scope.access_token;
        $scope.user_id = sessionService.get('uid');

        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
        } else {

            $scope.project_id = $routeParams.project_id;
            $scope.search_text = $routeParams.search_text;
            $scope.search_replace_text = $routeParams.search_text;
            // Fetch Object Details

            tocService.searchText($scope).then(function (result) {
                $scope.data = result.data;
                $scope.showErrorMessage = 0;

                objectService.getProjectDetails($scope).then(function (data) {
                    $scope.project_details = data;
                    angular.forEach(result.search_found, function (v, k) {
                        if (v) {
                            $scope.showErrorMessage = 1;
                        }
                    });
                    if ($scope.showErrorMessage == 0) {
                        $scope.errorMessage = 'No result found with the search text "' + $scope.search_text + '"';
                    }
                    //console.info($scope.errorMessage);
                    setTimeout(function () {
                        if ($(".link-to-preview").eq(0)) {
                            $(".link-to-preview").eq(0).trigger("click").addClass("active");
                        }
                    }, 1000);
                });
            });

            $scope.data = {
                children: [{
                        id: guid('-'),
                        title: "Loading...",
                        type: '2',
                        root_type: '2',
                        children: []
                    }]
            };

            $scope.toggleMinimized = function (child) {
                child.minimized = !child.minimized;
            };

            $scope.errorMessage = '';
            $scope.content = '';
            $scope.object_details = {};
            $scope.contentPreview = function (file_name) {
                $scope.nextPrevCount = 0;
                $scope.currentIndex = 0;
                $('#content_preview_iframe').contents().find('.testContainer').html('');
                $scope.file_name = file_name;
                objectService.getObjectDetailsByFilename($scope, $scope.project_id, $scope.file_name).then(function (data) {
                    $scope.content = '';
                    if (data.status == '200') {
                        //console.log(data.data.objects.description);
                        var content = data.data.objects.description.replace("<![CDATA[<![CDATA[", "").replace("]]]]>", "").replace("<![CDATA[>]]>", "");
                        //console.info(content);
                        $scope.object_details = data.data;
                        $scope.content = $filter('highlight_txt')(content, $scope.search_text);
                        $scope.theme_location = data.data.theme_location;
                        $scope.file_path = data.data.file_path;
                        $scope.file_path = $sce.trustAsResourceUrl($scope.file_path);
                        $scope.getActualPath = file_name;
                        $scope.errorMessage = '';
                        setTimeout(function () {
                            setContentIntoIframe();
                        }, 500);
                    } else {
                        $scope.errorMessage = data.message;
                        if (angular.isDefined($scope.errorMessage)) {
                            angular.element("#content_preview_iframe").contents().find("body").html("<div style='color: #AD0505; padding: 22%;'>" + $scope.errorMessage + "</div>");
                        }
                    }
                    //console.log($scope.content);
                });
            };

            $scope.previewProject = function () {
                $scope.errorMessage = '';
                $scope.content = '';
                $scope.object_details = {};
            };

            $scope.search = function () {
                if (angular.isDefined($scope.project_id) && angular.isDefined($scope.search_replace_text)) {
                    $window.location.href = "/#/search_replace/" + $scope.project_id + "/" + $scope.search_replace_text;
                } else {
                    SweetAlert.swal({html: 'Please enter a search text'});
                }
            };

            $scope.replace = function () {
                if (angular.isDefined($scope.project_id) && angular.isDefined($scope.search_replace_text)) {
                    if ($scope.currentIndex) {
                        var scroll = angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq($scope.currentIndex).offset().top;
                        angular.element("#content_preview_iframe").contents().find("body").scrollTop(scroll);
                        angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq($scope.currentIndex).replaceWith($scope.search_replace_text);                        
                    } else {
                        var scroll = angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq(0).offset().top;
                        angular.element("#content_preview_iframe").contents().find("body").scrollTop(scroll);
                        angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq(0).replaceWith($scope.search_replace_text);
                    }
                } else {
                    SweetAlert.swal({html: 'Please enter a text to replace'});
                }
            };

            $scope.replaceAll = function () {
                if (angular.isDefined($scope.project_id) && angular.isDefined($scope.search_replace_text)) {
                    angular.forEach(angular.element("#content_preview_iframe").contents().find(".highlightTextSearch"), function (v, k) {
                        //console.log(k);
                        var scroll = angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq(k).offset().top;
                        angular.element("#content_preview_iframe").contents().find("body").scrollTop(scroll);
                        angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq(k).replaceWith($scope.search_replace_text);
                    });
                    //angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq(0).replaceWith($scope.search_replace_text);
                } else {
                    SweetAlert.swal({html: 'Please enter a text to replace'});
                }
            };

            $scope.next = function () {
                if (angular.isDefined($scope.project_id) && angular.isDefined($scope.search_replace_text)) {
                    if ($scope.nextPrevCount < angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").length) {
                        var scroll = angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq($scope.nextPrevCount).offset().top;
                        angular.element("#content_preview_iframe").contents().find("body").scrollTop(scroll);
                        angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq($scope.nextPrevCount).css("background-color", "#96FFDC");                        
                        $scope.currentIndex = $scope.nextPrevCount;
                        if ($scope.nextPrevCount > 0) {
                            var c = $scope.nextPrevCount - 1;
                            angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq(c).css("background-color", "yellow");
                        }
                        //console.log("current: " + $scope.currentIndex);
                        $scope.nextPrevCount++;
                        if ($scope.nextPrevCount == angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").length) {
                            $scope.nextPrevCount = angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").length - 1;
                        }
                    }
                } else {
                    SweetAlert.swal({html: 'Please enter a text to replace'});
                }
                //console.info("next: " + $scope.nextPrevCount);
            };

            $scope.previous = function () {
                if (angular.isDefined($scope.project_id) && angular.isDefined($scope.search_replace_text)) {
                    $scope.nextPrevCount = $scope.currentIndex;
                    if ($scope.nextPrevCount > 0) {
                        angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq($scope.nextPrevCount).css("background-color", "yellow");
                        $scope.nextPrevCount--;
                        var scroll = angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq($scope.nextPrevCount).offset().top;
                        angular.element("#content_preview_iframe").contents().find("body").scrollTop(scroll);
                        angular.element("#content_preview_iframe").contents().find(".highlightTextSearch").eq($scope.nextPrevCount).css("background-color", "#96FFDC");                        
                        $scope.currentIndex = $scope.nextPrevCount;
                    }
                } else {
                    SweetAlert.swal({html: 'Please enter a text to replace'});
                }
                //console.log("current: " + $scope.currentIndex);
                //console.info("previous: " + $scope.nextPrevCount);
            };
        }
    }]);



