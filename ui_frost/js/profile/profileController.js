/*****
 * Controller for HEADER
 * Author - Indranil Ghosh * 
 */

'use strict';

app.controller('profile', ['$scope', 'sessionService', '$location', '$routeParams', '$rootScope', '$log', 'SweetAlert', '$modal', 'profileService', function ($scope, sessionService, $location, $routeParams, $rootScope, $log, SweetAlert, $modal, profileService) {

        $scope.access_token = sessionService.get('access_token');
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        $scope.formData = {};

        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
            return true;
        }

        //console.info(userinfo);

        $scope.formData = userinfo;


        $scope.resetPasswd = function (formData) {
            $scope.access_token = sessionService.get('access_token');
            var userinfo = JSON.parse(sessionService.get('userinfo'));
            $scope.userId = userinfo.id;
            if (!$scope.access_token || userinfo == null) {
                $location.path('logout');
            }
            if (formData.$valid) {
                if ($scope.formData.new_password1.$modelValue != $scope.formData.new_password2.$modelValue) {
                    $scope.status = 400;
                    $scope.error_msg = 'New passwords are not same.';
                } else {
                    profileService.resetPassword($scope).then(function (data) {
                        $scope.error_msg = data.message;
                        $scope.status = data.status;
                    });
                }
//                profileService.resetPassword($scope).then(function (data) {
//                    $scope.error_msg = data.message;
//                    $scope.status = data.status;
//                });

            } else {
                if ($scope.formData.old_password.$error.required || $scope.formData.new_password1.$error.required || $scope.formData.new_password2.$error.required) {
                    $scope.status = 400;
                    $scope.error_msg = 'All fields are required.';
                }
            }
        };

        //$injector.invoke(login, this, {$scope: $scope});
//        profileService.getUser($scope).then(function(data){
//            console.info(data);
//        });


        $scope.showMsg = 0;
        $scope.update_profile = function () {
            profileService.updateUser($scope).then(function (data) {
                $scope.showMsg = 1;
                $scope.message = data.message;
                $scope.status = data.status;
                sessionService.set('userinfo', JSON.stringify(data.data));
            });
        }

        $scope.showChangePasswordMsg = 0;
        $scope.old_password = '';
        $scope.change_password = function () {
            profileService.changePassword($scope).then(function (data) {
                $scope.showChangePasswordMsg = 1;
                $scope.changePasswordMsg = data.message;
                $scope.changePasswordstatus = data.status;
                $scope.old_password = '';

                $scope.error = [];
                if (data.status == '400') {
                    if (angular.isDefined(data.error.oldpassword)) {
                        $scope.error.oldpassword = data.error.oldpassword[0];
                    }
                    if (angular.isDefined(data.error.password)) {
                        $scope.error.password = data.error.password[0];
                    }
                    if (angular.isDefined(data.error.password_confirmation)) {
                        $scope.error.password_confirmation = data.error.password_confirmation[0];
                    }

                }
                $scope.oldpassword = '';
                $scope.password = '';
                $scope.password_confirmation = '';
            });
        }

        $scope.showEmailFrequencyMsg = 0;
        $scope.saveEmailFrequency = function () {
            var userinfo = JSON.parse(sessionService.get('userinfo'));
            $scope.userId = userinfo.id;
            profileService.saveEmailFrequency($scope).then(function (data) {
                $scope.showEmailFrequencyMsg = 1;
                $scope.showEmailFrequencyMsg = data.message;
                $scope.saveEmailFrequencyStatus = data.status;
            });
        }

        $scope.getEmailFrequency = function () {
            $scope.userId = userinfo.id;
            profileService.getEmailFrequency($scope).then(function (data) {
                $scope.email_frequency = data.data['frequency'];
            });
        }
        $scope.getEmailFrequency();



    }]);
