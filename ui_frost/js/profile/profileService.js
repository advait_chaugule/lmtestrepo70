/*****
 * Services for Login
 * Author - Arikh Akher
 */

'use strict';

//Start of loginService
app.service('profileService', ['$http', '$q', '$location', 'tokenService', 'sessionService', '$rootScope', function ($http, $q, $location, tokenService, sessionService, $rootScope) {

        this.getUser = function ($scope) {
            var deferred = $q.defer();
            var params = {};
            $http.post(serviceEndPoint + 'api/v1/getuser?access_token=' + sessionService.get('access_token'), {
                email: $scope.useremail
            }).success(function (data, status, headers, config) {
                $scope.status = data.status;
                $scope.message = data.message;
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        };

        this.resetPassword = function ($scope) {
            var deferred = $q.defer();
            var params = {};
            params.access_token = $scope.access_token;
            params.old_password = $scope.old_password;
            params.new_password = $scope.new_password1;
            params.user_id = $scope.userId;
            $http({
                method: 'POST',
                url: serviceEndPoint + 'api/v1/resetpassword',
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);//Return the roles list
            }).error(function (data, status, headers, config) {

            });

            return deferred.promise;
        };

        //Update User Profile
        this.updateUser = function ($scope) {
            var deferred = $q.defer();
            var params = {};
            $http.post(serviceEndPoint + 'api/v1/users-profile?access_token=' + sessionService.get('access_token'), {
                first_name: $scope.formData.first_name,
                last_name: $scope.formData.last_name,
                _method: 'put'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        //Change Password
        this.changePassword = function ($scope) {
            var deferred = $q.defer();
            var params = {};
            $http.post(serviceEndPoint + 'api/v1/change-password?access_token=' + sessionService.get('access_token'), {
                oldpassword: $scope.oldpassword,
                password: $scope.password,
                password_confirmation: $scope.password_confirmation,
                _method: 'put'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        //Save Email Frequency 
        this.saveEmailFrequency = function ($scope) {
            var deferred = $q.defer();
            var params = {};
            //api/v1/email-notification/user_id/{user_id}/frequency/{frequency}
            $http.post(serviceEndPoint + 'api/v1/email-notification/user_id/' + $scope.userId + '/frequency/' + $scope.email_frequency + '?access_token=' + sessionService.get('access_token'), {
                _method: 'put'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        
        //Save Email Frequency 
        this.getEmailFrequency = function ($scope) {
            var deferred = $q.defer();
            var params = {};
            //api/v1/email-notification/user_id/{user_id}/frequency/{frequency}
            $http.get(serviceEndPoint + 'api/v1/email-notification/user_id/' + $scope.userId + '?access_token=' + sessionService.get('access_token'), {
                
            }).success(function (data, status, headers, config) {                
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

    }]);
