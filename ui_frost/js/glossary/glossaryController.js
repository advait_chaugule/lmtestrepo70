/*****
 * Controller for project
 * Author - Arikh Akher * 
 */

'use strict';
app.controller('glossary', ['$scope', '$http', 'sessionService', '$location', 'glossaryService', '$rootScope', '$modal', '$log', 'SweetAlert', '$routeParams', 'objectService', function ($scope, $http, sessionService, $location, glossaryService, $rootScope, $modal, $log, SweetAlert, $routeParams, objectService) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));

        $scope.project_id = $routeParams.project_id;
        $scope.nodeTitle = $routeParams.title;
        if (!$scope.access_token || userinfo == null) {
            $location.path('logout');
            return true;
        }
        objectService.getProjectDetails($scope).then(function (data) {
            //console.log(data);
            $scope.object_details = data;
            $scope.project = data;
            $scope.project.project_permissions = data.project_permissions//{'project_permissions': data.project_permissions};

        });

        //For pagination
        $scope.currentPage = 1;
        $scope.pageNumber = '';
        $scope.itemsPerPage = '';
        //
        $scope.getList = function (char_code, pageNumber) {
            $scope.pageNumber = pageNumber;
            $scope.itemsPerPage = 10;
            $scope.glossary_id = "";
            $scope.char_code = char_code;
            glossaryService.getGlossary($scope).then(function (data) {
                $scope.glossaryList = data.data;
                $scope.totalGlossary = data.totalGlossary;

                $scope.charList = [];
                for (var i = 65; i <= 90; i++) {
                    var obj = {};
                    obj.code = i;
                    obj.char = '&#' + i + ';';
                    $scope.charList.push(obj);
                }
                //jQuery('.editor-loader').remove();
                //jQuery('body').css('overflow', '');
            });
        };

        $scope.getList('', 1);

        $scope.getListByChar = function (char_code) {
            $scope.pageChangeHandlerGlossary(char_code, 1);
        }

        //For Pagination Of Dashboard
        $scope.pageChangeHandlerGlossary = function (char_code, newPage) {
            $scope.getList(char_code, newPage);
        };

        $scope.addGlossaryForm = function (size, term) {

            //$scope.child = child;
            var modalInstance = $modal.open({
                templateUrl: 'templates/glossary/myModalContent.html',
                controller: 'ModalInstanceGlossaryCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    glossaryRec: function () {
                        return '';
                    },
                    modalFormText: function () {
                        return "You are trying to add a new glossary";
                    },
                    modalFormTile: function () {
                        return "Add Glossary";
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    term: function () {
                        return term;
                    }
                }
            });
            modalInstance.result.then(function (data) {
                glossaryService.getGlossary($scope).then(function (data) {
                    $scope.glossaryList = data.data;
                    $scope.totalGlossary = data.totalGlossary;
                });

                if (typeof (htmlEditor) != "undefined" && htmlEditor.toAddClass == 1) {
                    htmlEditor.highlightGlossayText(data.description);
                }
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.editGlossary = function (size, id) {
            var glossaryRec = {};
            $scope.glossary_id = id;
            glossaryService.getGlossary($scope).then(function (data) {
                glossaryRec = data.data;
                $scope.glossaryRecTerm = data.data[0].term;
                var modalInstance = $modal.open({
                    templateUrl: 'templates/glossary/myModalContent.html',
                    controller: 'ModalInstanceGlossaryCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        glossaryRec: function () {
                            return glossaryRec;
                        },
                        modalFormText: function () {
                            return "You are trying to edit glossary term [" + $scope.glossaryRecTerm + "]";
                        },
                        modalFormTile: function () {
                            return "Edit Glossary";
                        },
                        project_id: function () {
                            return $scope.project_id;
                        },
                        term: function () {
                            return '';
                        }
                    }
                });

                modalInstance.result.then(function (data) {
                    $scope.glossary_id = '';
                    glossaryService.getGlossary($scope).then(function (data) {
                        $scope.glossaryList = data.data;
                    });
                    if (typeof (htmlEditor) != "undefined" && htmlEditor.toAddClass == 1) {
                        htmlEditor.highlightGlossayText(data.description);
                    }
                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };

        $scope.viewGlossary = function (size, id) {
            var glossaryRec = {};
            $scope.glossary_id = id;
            glossaryService.getGlossary($scope).then(function (data) {
                glossaryRec = data.data;

                var modalInstance = $modal.open({
                    templateUrl: 'templates/glossary/myModalContentView.html',
                    controller: 'ModalInstanceGlossaryCtrl',
                    size: size,
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        glossaryRec: function () {
                            return glossaryRec;
                        },
                        modalFormText: function () {
                            return "You are trying to view a glossary";
                        },
                        modalFormTile: function () {
                            return "View Glossary";
                        },
                        project_id: function () {
                            return $scope.project_id;
                        },
                        term: function () {
                            return '';
                        }
                    }
                });

                modalInstance.result.then(function () {

                }, function () {
                    $log.info('Modal dismissed at: ' + new Date());
                });
            });
        };


        $scope.deleteGlossary = function (id, project_id) {

            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this glossary!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    glossaryService.delGlossary(id, project_id).then(function () {
                        $scope.glossary_id = '';
                        glossaryService.getGlossary($scope).then(function (data) {
                            $scope.glossaryList = data.data;
                            $scope.totalGlossary = data.totalGlossary;
                        });
                    });
                    SweetAlert.swal("Success", "Glossary has been deleted", "success");
                } else {
                    SweetAlert.swal("Cancelled", "Glossary is safe :)", "error");
                }
            });

        };

        $scope.redirectPage = function () {
            $location.path($scope.redirpage);
        }


        $scope.downloadCSV = function () {
            $scope.itemsPerPage = 0;
            glossaryService.getGlossary($scope).then(function (data) {
                $scope.glossaryAllList = data.data;
                $scope.totalGlossary = data.totalGlossary;
                //console.info($scope.glossaryAllList);
                var glossaryCsvArr = [];

                angular.forEach($scope.glossaryAllList, function (value, key) {
                    var glossaryCsv = {};
                    glossaryCsv.Term = value.term;
                    glossaryCsv.Description = value.description.trim();
                    glossaryCsvArr.push(glossaryCsv);
                });

                $scope.JSONToCSVConvertor(glossaryCsvArr, "Glossary of " + $scope.object_details.name, true);
            });
        }

        $scope.JSONToCSVConvertor = function (JSONData, ReportTitle, ShowLabel) {
            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

            var CSV = '';
            //Set Report title in first row or line

            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                alert("Invalid data");
                return;
            }

            //Generate a file name
            var fileName = "";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

            // Now the little tricky part.
            // you can use either>> window.open(uri);
            // but this will not work in some browsers
            // or you will not get the correct file extension    

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = uri;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

    }]);


app.controller('ModalInstanceGlossaryCtrl', function ($scope, glossaryService, sessionService, $modalInstance, modalFormText, modalFormTile, glossaryRec, project_id, term) {
    $scope.showedit = 0;
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.project_id = project_id;
    $scope.term = term;
    $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));

    if (glossaryRec.length) {
        $scope.glossary_id = glossaryRec[0].id;
        $scope.term = glossaryRec[0].term;
        $scope.description = glossaryRec[0].description;
        $scope.showedit = glossaryRec.length;
    } else {
        $scope.showedit = 0;
    }

    $scope.add = function () {
        $scope.formError = {'term': '', 'description': ''};
        //if (angular.isUndefined($scope.term) == false && angular.isUndefined($scope.description) == false) {
        glossaryService.saveGlossary($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                $modalInstance.close($scope);
            } else {
                if (angular.isDefined(savedResponse.message) && savedResponse.message.split(':')[0] == 'Error') {
                    $scope.formError['description'] = savedResponse.message.substring(savedResponse.message.indexOf(':') + 1)
                } else {
                    if (angular.isDefined(savedResponse.error.term)) {
                        $scope.formError['term'] = savedResponse.error.term['0'];
                    }
                    if (angular.isDefined(savedResponse.error.description)) {
                        $scope.formError['description'] = savedResponse.error.description['0'];
                    }
                }
            }

        });
        //}
    };

    $scope.edit = function () {
        $scope.formError = {'term': '', 'description': ''};
        //if (angular.isUndefined($scope.term) == false && angular.isUndefined($scope.description) == false) {
        glossaryService.editGlossary($scope).then(function (savedResponse) {
            if (savedResponse.status == '200') {
                $modalInstance.close($scope);
            } else {
                if (angular.isDefined(savedResponse.message) && savedResponse.message.split(':')[0] == 'Error') {
                    $scope.formError['description'] = savedResponse.message.substring(savedResponse.message.indexOf(':') + 1)
                } else {
                    if (angular.isDefined(savedResponse.error.term)) {
                        $scope.formError['term'] = savedResponse.error.term['0'];
                    }
                    if (angular.isDefined(savedResponse.error.description)) {
                        $scope.formError['description'] = savedResponse.error.description['0'];
                    }
                }
            }

        });
        //}
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});