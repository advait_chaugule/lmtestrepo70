/*****
 * Specific services object type =  project
 * Author - Arikh Akher  
 */
'use strict';

app.service('glossaryService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        //Start - Method to get glossary
        this.getGlossary = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.glossary_id)) {
                queryString += "&id=" + $scope.glossary_id;
            }
            if (angular.isDefined($scope.project_id)) {
                queryString += '&project_id=' + $scope.project_id;
            }
            if (angular.isDefined($scope.char_code)) {
                queryString += '&char_code=' + $scope.char_code;
            }            
            if (angular.isDefined($scope.term_text)) {
                queryString += '&term_text=' + $scope.term_text;
            }
            //For Pagination
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber+'&itemsPerPage=' + $scope.itemsPerPage;
            }

            $http.get(serviceEndPoint + 'api/v1/glossary?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                
                var glossary = {'data':data.data.glossary_detail,'totalGlossary':data.totalGlossary};
                //console.log(glossary);
                
                deferred.resolve(glossary);//Return the project list             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;

        };
        //End - Method to get glossary


        this.saveGlossary = function ($scope, $rootScope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/glossary/project_id/'+$scope.project_id+'?access_token=' + sessionService.get('access_token'), {
                project_id: $scope.project_id,
                term: $scope.term,
                description: $scope.description
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }

        this.editGlossary = function ($scope, $rootScope) {
            //console.info($scope);
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/glossary/project_id/'+$scope.project_id+'/glossary_id/' + $scope.glossary_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT',
                project_id: $scope.project_id,
                term: $scope.term,
                description: $scope.description
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                //$rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }

        this.delGlossary = function (glossary_id,project_id) {
            var deferred = $q.defer();
            $http.delete(serviceEndPoint + 'api/v1/glossary/project_id/'+project_id+'/glossary_id/' + glossary_id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }

    }]);

