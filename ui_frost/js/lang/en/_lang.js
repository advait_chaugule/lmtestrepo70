var _lang = {
    "1": {
        "uplaod_project_1": "Now that you have created a Project you can continue by either creating your TOC or by uploading a PXE Master. Haven't decided yet? Use links on right to configure your project.",
        "uplaod_project_2": "Upload a PXE package"
    },
    "5": {
        "uplaod_project_1": "Your COURSE is created and you will be redirect to the TOC page shortly..",
        "uplaod_project_2": "Upload a COURSE package"
    },
    "4": {
        "uplaod_project_1": "Now that you have created a Project you can continue by either creating your TOC or by uploading an EDUPUB package. Haven't decided yet? Use links on right to configure your project.",
        "uplaod_project_2": "Upload an EDUPUB package"
    },
    "content_global_confirm_message": "Please confirm. Are you sure you want to make this global. By doing so you are allowing other user to reuse it on different projects. Please make sure as this action cannot be reverted.",
    "NO_RECORD": "No Records."
};