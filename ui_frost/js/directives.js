'use strict';

app.directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function () {
                    scope.$apply(function () {
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }]);


app.directive("fileread", [function () {
        return {
            restrict: 'AEC',
            scope: {
                fileread: "="
            },
            link: function (scope, element, attributes) {
                element.bind("change", function (changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.fileread = loadEvent.target.result;
                        });
                    }
                    reader.readAsDataURL(changeEvent.target.files[0]);
                });
            }
        }
    }]);

app.directive('ckEditor', [function () {
        return {
            require: '?ngModel',
            restrict: 'C',
            link: function (scope, elm, attr, model) {
                var isReady = false;
                var data = [];
                var ck = CKEDITOR.replace(elm[0]);

                function setData() {
                    if (!data.length) {
                        return;
                    }

                    var d = data.splice(0, 1);
                    ck.setData(d[0] || '<span></span>', function () {
                        setData();
                        isReady = true;
                    });
                }

                ck.on('instanceReady', function (e) {
                    if (model) {
                        setData();
                    }
                });

                elm.bind('$destroy', function () {
                    ck.destroy(false);
                });

                if (model) {
                    ck.on('change', function () {
                        scope.$apply(function () {
                            var data = ck.getData();
                            if (data == '<span></span>') {
                                data = null;
                            }
                            model.$setViewValue(data);
                        });
                    });

                    model.$render = function (value) {
                        if (model.$viewValue === undefined) {
                            model.$setViewValue(null);
                            model.$viewValue = null;
                        }

                        data.push(model.$viewValue);

                        if (isReady) {
                            isReady = false;
                            setData();
                        }
                    };
                }
            }
        };
    }]);

// TOC tree dragable starts
app.directive('yaTree', function () {

    return {
        restrict: 'A',
        transclude: 'element',
        priority: 1000,
        terminal: true,
        compile: function (tElement, tAttrs, transclude) {

            var repeatExpr, childExpr, rootExpr, childrenExpr, branchExpr;

            repeatExpr = tAttrs.yaTree.match(/^(.*) in ((?:.*\.)?(.*)) at (.*)$/);
            childExpr = repeatExpr[1];
            rootExpr = repeatExpr[2];
            childrenExpr = repeatExpr[3];
            branchExpr = repeatExpr[4];

            return function link(scope, element, attrs) {

                var rootElement = element[0].parentNode,
                        cache = [];

                // Reverse lookup object to avoid re-rendering elements
                function lookup(child) {
                    var i = cache.length;
                    while (i--) {
                        if (cache[i].scope[childExpr] === child) {
                            return cache.splice(i, 1)[0];
                        }
                    }
                }

                scope.$watch(rootExpr, function (root) {

                    var currentCache = [];

                    // Recurse the data structure
                    (function walk(children, parentNode, parentScope, depth) {
                        if (angular.isDefined(children)) {
                            var i = 0,
                                    n = children.length,
                                    last = n - 1,
                                    cursor,
                                    child,
                                    cached,
                                    childScope,
                                    grandchildren;

                            // Iterate the children at the current level
                            for (; i < n; ++i) {

                                // We will compare the cached element to the element in 
                                // at the destination index. If it does not match, then 
                                // the cached element is being moved into this position.
                                cursor = parentNode.childNodes[i];

                                child = children[i];

                                // See if this child has been previously rendered
                                // using a reverse lookup by object reference
                                cached = lookup(child);

                                // If the parentScope no longer matches, we've moved.
                                // We'll have to transclude again so that scopes 
                                // and controllers are properly inherited
                                if (cached && cached.parentScope !== parentScope) {
                                    cache.push(cached);
                                    cached = null;
                                }

                                // If it has not, render a new element and prepare its scope
                                // We also cache a reference to its branch node which will
                                // be used as the parentNode in the next level of recursion
                                if (!cached) {
                                    transclude(parentScope.$new(), function (clone, childScope) {

                                        childScope[childExpr] = child;

                                        cached = {
                                            scope: childScope,
                                            parentScope: parentScope,
                                            element: clone[0],
                                            branch: clone.find(branchExpr)[0]
                                        };

                                        // This had to happen during transclusion so inherited 
                                        // controllers, among other things, work properly
                                        if (!cursor)
                                            parentNode.appendChild(cached.element);
                                        else
                                            parentNode.insertBefore(cached.element, cursor);


                                    });
                                } else if (cached.element !== cursor) {
                                    if (!cursor)
                                        parentNode.appendChild(cached.element);
                                    else
                                        parentNode.insertBefore(cached.element, cursor);

                                }

                                // Lets's set some scope values
                                childScope = cached.scope;

                                // Store the current depth on the scope in case you want 
                                // to use it (for good or evil, no judgment).
                                childScope.$depth = depth;

                                // Emulate some ng-repeat values
                                childScope.$index = i;
                                childScope.$first = (i === 0);
                                childScope.$last = (i === last);
                                childScope.$middle = !(childScope.$first || childScope.$last);

                                // Push the object onto the new cache which will replace
                                // the old cache at the end of the walk.
                                currentCache.push(cached);

                                // If the child has children of its own, recurse 'em.             
                                grandchildren = child[childrenExpr];
                                if (grandchildren && grandchildren.length) {
                                    walk(grandchildren, cached.branch, childScope, depth + 1);
                                }
                            }
                        }

                    })(root, rootElement, scope, 0);

                    // Cleanup objects which have been removed.
                    // Remove DOM elements and destroy scopes to prevent memory leaks.
                    var i = cache.length;

                    while (i--) {
                        var cached = cache[i];
                        if (cached.scope) {
                            cached.scope.$destroy();
                        }
                        if (cached.element) {
                            cached.element.parentNode.removeChild(cached.element);
                        }
                    }

                    // Replace previous cache.
                    cache = currentCache;

                }, true);
            };
        }
    };
});

// app.directive('uiNestedSortable', ['$parse', function ($parse) {

//         'use strict';

//         var eventTypes = 'Create Start Sort Change BeforeStop Stop Update Receive Remove Over Out Activate Deactivate'.split(' ');

//         return {
//             restrict: 'A',
//             link: function (scope, element, attrs) {

//                 var options = attrs.uiNestedSortable ? $parse(attrs.uiNestedSortable)() : {};

//                 angular.forEach(eventTypes, function (eventType) {

//                     var attr = attrs['uiNestedSortable' + eventType], callback;

//                     if (attr) {
//                         callback = $parse(attr);
//                         options[eventType.charAt(0).toLowerCase() + eventType.substr(1)] = function (event, ui) {
//                             scope.$apply(function () {

//                                 callback(scope, {
//                                     $event: event,
//                                     $ui: ui
//                                 });
//                             });
//                         };
//                     }

//                 });

//                 //note the item="{{child}}" attribute on line 17
//                 options.isAllowed = function (item, parent) {
//                     if (!parent) {
//                         parent = scope.data;
//                     } else {
//                         var attrs = parent.context.attributes;
//                         parent = attrs.getNamedItem('item');
//                     }
//                     attrs = item.context.attributes;
//                     item = attrs.getNamedItem('item');
//                     //console.log(item, parent);
//                     //if ( ... ) return false;
//                     return true;
//                 };

//                 element.nestedSortable(options);

//             }
//         };
//     }]);
// TOC tree dragable ends

// Window resize starts
app.directive('resize', function ($window) {
    return function (scope, element) {
        var w = angular.element($window);
        scope.getWindowDimensions = function () {
            //console.log(angular.element(angular.element('.page-title')).height());

            var page_title_padding_top = angular.isDefined(angular.element(angular.element('.page-title')).css('padding-top')) ? Number(angular.element(angular.element('.page-title')).css('padding-top').replace('px', '')) : 0;
            var page_title_padding_bottom = angular.isDefined(angular.element(angular.element('.page-title')).css('padding-bottom')) ? Number(angular.element(angular.element('.page-title')).css('padding-bottom').replace('px', '')) : 0;
            return {
                'windowHeight': w.height(),
                'windowWidth': w.width(),
                'headerContainerHeight': angular.element(angular.element('.header-container')).height(),
                'footerContainerHeight': angular.element(angular.element('.footer-container')).height(),
                'headerHeight': angular.element(angular.element('.header')).height(),
                'footerHeight': angular.element(angular.element('.footer')).height(),
                'rightPanelTitleHeight': angular.element(angular.element('.right-panel-navpanel .page-title')).height(),
                'rightPanelFooterHeight': angular.element(angular.element('.right-panel-navpanel .info-footer-option')).height(),
                'assetListContainerHeight': angular.element(angular.element('.listing-view-content .view-panel')).outerHeight(),
                'pageTitleHeight': angular.isDefined(angular.element('.page-title')) ? (angular.element(angular.element('.page-title')).height() + page_title_padding_top + page_title_padding_bottom) : 0

            };
        };

        scope.$watch(scope.getWindowDimensions, function (newValue, oldValue) {
            scope.windowHeight = newValue.windowHeight;
            scope.windowWidth = newValue.windowWidth;
            scope.headerHeight = newValue.headerHeight;
            scope.footerHeight = newValue.footerHeight;
            scope.headerContainerHeight = newValue.headerContainerHeight;
            scope.footerContainerHeight = newValue.footerContainerHeight;
            scope.pageTitleHeight = newValue.pageTitleHeight;
            scope.rightPanelTitleHeight = newValue.rightPanelTitleHeight;
            scope.rightPanelFooterHeight = newValue.rightPanelFooterHeight;
            scope.assetListContainerHeight = newValue.assetListContainerHeight;
            //scope.actualHeight =  (scope.windowHeight - scope.headerHeight - scope.footerHeight);
            //scope.actualHeight = (scope.windowHeight - scope.headerHeight - scope.footerContainerHeight - scope.pageTitleHeight);
            scope.actualHeight = (scope.windowHeight - scope.headerHeight - scope.pageTitleHeight - 1);
            scope.actualAssetListHeight = (scope.windowHeight - scope.headerHeight - scope.pageTitleHeight - scope.assetListContainerHeight - 20);
            scope.assetSidepanelHeight = (scope.windowHeight - scope.headerHeight - scope.pageTitleHeight - scope.rightPanelTitleHeight - scope.rightPanelFooterHeight + 5);
            scope.actualGlossaryHeight = (scope.windowHeight - scope.headerHeight - 55);
            scope.actualAdvanceSearchHeight = (scope.windowHeight - scope.headerContainerHeight);
            scope.actualAdvanceSearchInnerHeight = (scope.windowHeight - scope.headerContainerHeight - scope.pageTitleHeight);

        }, true);

        w.bind('resize', function () {
            scope.$apply();
        });
    }
});

app.directive('customEditor', function () {
    'use strict';
    return {
        restrict: 'AEC',
        scope: true,
        link: function (scope, element, attr) {
            attr.$observe("projectType", function (projectType) {
                if (projectType == 1) {
                    if (attr.nodeType == 12) {
                        scope.editorUrl = 'simple_editor/glossary_view.html';
                    } else {
                        scope.editorUrl = 'simple_editor/index.html';
                    }
                }
                if (projectType == 2) {
                    scope.editorUrl = 'EDITOR/express.html';
                }
                if (projectType == 3) {
                    scope.editorUrl = 'bootstrap_editor/index.html';
                }
            });
        },
        template: '<div ng-include="editorUrl"></div>'
    }
});

app.directive('autocomplete', function () {
    return {
        restrict: 'E',
        scope: {
            ngModel: '=',
            options: '=',
            createdBy: '=',
            userId: '=',
            editFlag: '=',
            editUsername: '=',
            responseStatus: '='
        },
        controller: function ($scope, $element, $attrs, $location) {
            if ($scope.ngModel) {
                $scope.ngModelName = $scope.ngModel.username;
            }

            $scope.onSelect = function ($item, $model, $label) {
                angular.extend($scope.ngModel, $item);
                if ($item.username != 'No User Found') {
                    $scope.ngModelName = $item.username;
                } else {
                    $scope.ngModelName = '';
                }
            };
            $scope.setText = function (index, $item) {
                angular.extend($scope.ngModel, $item);
                $scope.ngModel.username = $scope.ngModelName;
                setTimeout(function () {
                    if ($scope.responseStatus == '400') {
                        //$scope.ngModelName = $scope.ngModelName.slice(0,-1);
                        $scope.ngModelName = '';
                    }
                }, 500);
                //$scope.ngModel = [{username:$scope.ngModelName}]
                /*console.log($scope.ngModel);
                 //console.log('userText= '+userText);
                 console.log($scope.ngModelName);
                 var username = $scope.ngModelName;
                 angular.extend($scope.ngModel, $item);
                 $scope.ngModelName = username;*/
            };

        },
        template: '<input type="text" ng-model="ngModelName" \n\
        typeahead="o.username for o in options($viewValue) | filter:{username:$viewValue}" typeahead-on-select="onSelect($item, $model, $label)" ng-change="setText($index,$item)" class="form-control" ng-disabled="userId == createdBy || editUsername == 1"/>'

    };
});


app.directive('showtab',
        function () {
            return {
                link: function (scope, element, attrs) {
                    element.click(function (e) {
                        e.preventDefault();
                        $(element).tab('show');
                    });
                }
            };
        });


app.directive('tocNodeStatusCount', function ($filter) {
    return {
        restrict: 'E',
        scope: {
            objectId: '=',
            ticketStatusCount: '='
        },
        controller: function ($scope, $element, $attrs, $location) {
            //console.log($scope.objectId);
            //console.log($scope.ticketTypeCount[$scope.objectId]);
            $scope.status_count_string = '';
            var open = '-';
            var need_clarification = '-';
            var fixed = '-';
            var verified = '-';
            var ignored = '';
            var validation_string = '<div class="action-link error pull-left"></div>';

            if (angular.isDefined($scope.ticketStatusCount[$scope.objectId])) {
                $scope.status_count_string = '';

                open = $scope.ticketStatusCount[$scope.objectId]['status']['OPEN'] ? $scope.ticketStatusCount[$scope.objectId]['status']['OPEN'] : '-';
                need_clarification = $scope.ticketStatusCount[$scope.objectId]['status']['NEED CLARIFICATION'] ? $scope.ticketStatusCount[$scope.objectId]['status']['NEED CLARIFICATION'] : '-';
                fixed = $scope.ticketStatusCount[$scope.objectId]['status']['FIXED'] ? $scope.ticketStatusCount[$scope.objectId]['status']['FIXED'] : '-';
                verified = $scope.ticketStatusCount[$scope.objectId]['status']['VERIFIED'] ? $scope.ticketStatusCount[$scope.objectId]['status']['VERIFIED'] : '-';
                ignored = $scope.ticketStatusCount[$scope.objectId]['status']['IGNORED'] ? $scope.ticketStatusCount[$scope.objectId]['status']['IGNORED'] : '-';


                //var validation_error = $scope.ticketStatusCount[$scope.objectId]['validation_response'];
                if ($scope.ticketStatusCount[$scope.objectId]['validation_response']) {
                    var validation_response = JSON.parse($scope.ticketStatusCount[$scope.objectId]['validation_response']);
                    if (angular.isDefined(validation_response.data) && angular.isDefined(validation_response.data.errors) && validation_response.data.errors > 0) {
                        //console.log(validation_response.data.errors);
                        validation_string = '<div class="action-link error pull-left"><span class="glyphicon glyphicon-exclamation-sign"></span>' + validation_response.data.errors + '</div>';
                    }
                }
                $scope.status_count_string += validation_string;
            }
            $scope.status_count_string += '<label title="open" class="action-link status open-status fst">' + open + '</label>';
            $scope.status_count_string += '<label title="need clarification" class="action-link need-clarification-status status">' + need_clarification + '</label>';
            $scope.status_count_string += '<label title="fixed" class="action-link fixed-status status">' + fixed + '</label>';
            $scope.status_count_string += '<label title="verified" class="action-link verified-status status">' + verified + '</label>';
            $scope.status_count_string += '<label title="ignored" class="action-link ignored-status status">' + ignored + '</label>';

        },
        template: '<div ng-bind-html="status_count_string" class="toc-inn-Btn" ></div>'

    };
});

// ngEnter preventDefault
app.directive('ngEnterPreventDefault', function ($document) {
    return {
        scope: {
            ngEnterPreventDefault: "&"
        },
        link: function (scope, element, attrs) {
            var enterWatcher = function (event) {
                if (event.which === 13) {
                    event.preventDefault();
                }
            };
            $document.bind("keydown keypress", enterWatcher);
        }
    }
});

app.directive('patternFrame', function ($compile) {
    return {
        restrict: 'AEC',
        scope: {
            body: '=',
            theme: '='
        },
        template: "<iframe id='patternview' height='300' width='100%' frameborder='0'></iframe>",
        link: function (scope, elm, attrs) {
            setTimeout(function () {
                //simple_editor\css\redserif
                var object = elm.find('iframe#patternview').contents();
                //object.find('head').append('<link rel="stylesheet" href="' + scope.theme + '"/><link rel="stylesheet" href="simple_editor/css/iframe.css"/>');
                var theme_location = '';
                angular.forEach(scope.theme, function (value, key) {
                    theme_location += '<link rel="stylesheet" href="' + value + '"/>';
                });
                object.find('head').append(theme_location);

                object.find('body').html(scope.body);
                var videoObj = object.find('video');
                var gadgetObj = object.find('iframe');
                if (typeof videoObj.find('source').attr('src') !== 'undefined') {
                    var videoSrc = videoObj.find('source').attr('src').length > 0 ? videoObj.find('source').attr('src') : 'Path not defined';
                    object.find('video').replaceWith('<div class="converted ' + videoObj.attr('class') + '"contenteditable="false" src="' + videoSrc + '" style="height: ' + videoObj.height() + 'px;"><div class="lm-rochak-out"><div class="lm-rochak"><div><div class="fa fa-file-video-o"> </div></div><strong>Video</strong><p class="mode-text">(Can be viewed in the test mode)</p><span class="url-link">Video URL: ' + videoSrc + '</span></div><div></div></div></div>');
                }

                if (typeof gadgetObj.attr('src') !== 'undefined') {
                    if (gadgetObj.attr('class') == 'gadget') {
                        //For Brightcove Gadget
                        var gadgetObjSrc = gadgetObj.attr('src').length > 0 ? gadgetObj.attr('src') : 'Path not defined';
                        object.find('iframe').replaceWith('<div class="converted ' + gadgetObj.attr('class') + '"contenteditable="false" src="' + gadgetObjSrc + '" scrolling=' + gadgetObj.attr('scrolling') + ' style="height: ' + gadgetObj.height() + 'px;"><div class="lm-rochak-out"><div class="lm-rochak"><div><div class="fa fa-file-code-o"> </div></div><strong>Gadget</strong><p class="mode-text">(Can be viewed in the test mode)</p><span class="url-link">Gadget URL: ' + gadgetObjSrc + '</span></div></div></div>');
                    } else if (gadgetObj.attr('class') == 'multipleVideoPopup') {
                        //For Brightcove video
                        var gadgetObjSrc = gadgetObj.attr('src').length > 0 ? gadgetObj.attr('src') : 'Id not defined';
                        object.find('iframe').replaceWith('<div class="converted ' + gadgetObj.attr('class') + '"contenteditable="false" src="' + gadgetObjSrc + '" scrolling=' + gadgetObj.attr('scrolling') + ' style="height: ' + gadgetObj.height() + 'px;"><div class="lm-rochak-out"><div class="lm-rochak"><div><div class="fa fa-file-code-o"> </div></div><strong>Multi Video</strong><p class="mode-text">(Can be viewed in the test mode)</p><span class="url-link">Multi Video: ' + gadgetObjSrc + '</span></div></div></div>');
                    } else {
                        //For Brightcove video
                        var gadgetObjSrc = gadgetObj.attr('src').length > 0 ? gadgetObj.attr('src') : 'Id not defined';
                        object.find('iframe').replaceWith('<div class="converted ' + gadgetObj.attr('class') + '"contenteditable="false" src="' + gadgetObjSrc + '" scrolling=' + gadgetObj.attr('scrolling') + ' style="height: ' + gadgetObj.height() + 'px;"><div class="lm-rochak-out"><div class="lm-rochak"><div><div class="fa fa-file-code-o"> </div></div><strong>Brightcove Video</strong><p class="mode-text">(Can be viewed in the test mode)</p><span class="url-link">Brightcove Video: ' + gadgetObjSrc + '</span></div></div></div>');
                    }


                }
            }, 500);
        }
    };
});


app.directive('iFrame', function ($compile) {
    return {
        restrict: 'AEC',
        scope: {
            body: '='
        },
        template: "<iframe id='iframeId' height='400' width='100%' frameborder='0'></iframe>",
        link: function (scope, elm, attrs) {
            setTimeout(function () {
                //simple_editor\css\redserif
                var object = elm.find('iframe#iframeId').contents();
                object.find('head').append('<link rel="stylesheet" href="simple_editor/css/redserif/default.css"/> <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600" rel="stylesheet" type="text/css">');
                object.find('head').append('<style type="text/css">  .diff td{ /*padding:0 0.667em; vertical-align:top; white-space:pre; white-space:pre-wrap; font-family:Consolas,"Courier New",Courier,monospace; font-size:0.75em; line-height:1.333; */ border: 0px;}  .diff span{ /*display:block; min-height:1.333em; margin-top:-1px; padding:0 3px; */}   html .diff span{ height:1.333em; }  .diff img{ height:auto; max-width: 300px !important; }  .diff span:first-child{ margin-top:0; }  .diffDeleted , del p, .diffdel, del.diffmod{ /*border:1px solid rgb(255,192,192);*/ background:rgb(255,224,224); }  .diffInserted , ins p, .diffins, ins.diffmod{ /*border:1px solid rgb(192,255,192);*/ background:rgb(224,255,224); }  .diffUnmodified  { /*border:1px solid rgb(192,192,255);*/ /*background:rgb(224,224,255);*/ }  #toStringOutput{ margin:0 2em 2em; }  #outputDiv {width:100%;padding: 0;} #outputDiv table.diff tr{background: transparent none repeat scroll 0 0;}</style>');
                object.find('body').html('<section class="col-md-12" id="outputDiv"></section>');
            }, 500);

            setTimeout(function () {
                var win = document.getElementById("iframeId").contentWindow;
                win.postMessage(true, "*");
            }, 1000);
        }
    };
});
/**
 * Widget Preview
 */
app.directive('widgetPreviewFrame', function ($compile, $filter) {
    return {
        restrict: 'AEC',
        scope: {
            objectId: '=',
            projectId: '='
        },
        controller: function ($scope, $element, $attrs, $location, assetService) {
            //console.log($scope.objectId);
            //console.log($scope.projectId);
            //get widget manifest file to launch the file
            assetService.getWidgetManifestDetails($scope).then(function (data) {
                var object = $element.find('iframe#widgetPreview');
                if (data.status == '200') {
                    var launch_file = data.data.manifest.launch_file;
                    object.attr('src', launch_file);
                } else {
                    object.contents().find("body").html("<div style='color: #AD0505; padding: 22%;text-align:center;'>" + data.message + "</div>");
                }
            });

        },
        template: "<iframe id='widgetPreview' height='300' width='100%' frameborder='0' src='' data-path=''></iframe>",
        link: function ($scope, elm, attrs) {
            //console.info($scope.projectId);
        }
    };
});

app.directive('onErrorSrc', function () {
    return {
        link: function (scope, element, attrs) {
            element.bind('error', function () {
                if (attrs.src != attrs.onErrorSrc) {
                    attrs.$set('src', attrs.onErrorSrc);
                }
            });
        }
    }
});
/**
 * Used to create ticket status count
 */
app.directive('ticketStatusCount', function () {
    return {
        restrict: 'E',
        scope: {
            projectId: '='
        },
        controller: function ($scope, $element, $attrs, $location, tocService) {
            $scope.project_id = $scope.projectId;
            $scope.status_count_string = '';
            tocService.getTicketStatusCount($scope).then(function (data) {
                $scope.status_count_string += '<label class="action-link status open-status fst">Open <span>' + data.TicketTotalCountsByStatus['OPEN'] + '</span></label>';
                $scope.status_count_string += '<label class="action-link status need-clarification-status">Need Clarification <span>' + (angular.isDefined(data.TicketTotalCountsByStatus['NEED CLARIFICATION']) ? data.TicketTotalCountsByStatus['NEED CLARIFICATION'] : '0') + '</span></label>';
                $scope.status_count_string += '<label class="action-link status fixed-status">Fixed <span>' + (angular.isDefined(data.TicketTotalCountsByStatus['FIXED']) ? data.TicketTotalCountsByStatus['FIXED'] : '0') + '</span></label>';
                $scope.status_count_string += '<label class="action-link status verified-status">Verified <span>' + (angular.isDefined(data.TicketTotalCountsByStatus['VERIFIED']) ? data.TicketTotalCountsByStatus['VERIFIED'] : '0') + '</span></label>';
                $scope.status_count_string += '<label class="action-link status ignored-status">Ignored <span>' + (angular.isDefined(data.TicketTotalCountsByStatus['IGNORED']) ? data.TicketTotalCountsByStatus['IGNORED'] : '0') + '</span></label>';
            });
        },
        template: '<div ng-bind-html="status_count_string" ></div>'

    };
});
/**
 * Used to create export button
 */
app.directive('projectExportButton', function () {
    return {
        restrict: 'E',
        scope: {
            project: '='
        },
        controller: function ($scope, $element, $attrs, $location, $modal, tocService, SweetAlert, objectService, $log, $rootScope) {
            $scope.status_count_string = '';
            $scope.project_id = $scope.project.id;
            $scope.data = JSON.parse($scope.project.toc);
            $scope.cfi_domain_name = '';
            $scope.cfi_book_id = '';

            $(document).on('keyup', '#cfi_domain_name', function () {
                $scope.showHideError();
            });

            $(document).on('keyup', '#cfi_book_id', function () {
                $scope.showHideError();
            });

            $(document).on('change', '#course_export_options', function () {
                $scope.course_export_option = $('#course_export_options').val();
                if ($scope.course_export_option != 'html') {
                    // html export options
                    $('#course_export_shell_div').hide();
                } else {
                    //$('#course_export_shell_div').show();
                    $('#course_export_shell_div').hide();
                }
            });

            $scope.showHideError = function () {
                var cfi_book_id = $("#cfi_book_id").val().trim();
                var cfi_domain_name = $("#cfi_domain_name").val().trim();
                if ((!cfi_book_id && cfi_domain_name) || (!cfi_domain_name && cfi_book_id)) {
                    $('.show-error').show();
                } else {
                    $('.show-error').hide();
                }
            };

            $scope.ticket_response = '';
            $("#export_btn_opener").removeAttr("disabled");
            $scope.exportProject = function (exportType) {
                $("#export_btn_opener").attr("disabled", true);
                $scope.exportType = exportType;
                if ($scope.exportType == 'pxe') {
                    var modalInstance = $modal.open({
                        templateUrl: 'templates/toc/export_option.html',
                        controller: 'ExportOption',
                        size: 'md',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                        }
                    });
                    modalInstance.result.then(function () {
                        $scope.project.project_export_count = 1;
                        //$rootScope.project_export_count = 1;
                    }, function () {
                        $log.info('Modal dismissed at: ' + new Date());
                    });
                } else {
                    tocService.getTicketStatusCount($scope).then(function (data) {
                        if (parseInt(data.TicketTotalCountsByStatus['OPEN']) > 0 || parseInt(data.TicketTotalCountsByStatus['IN PROGRESS']) > 0) {
                            $scope.ticket_response += "--------------------------------------------------------";
                            $scope.ticket_response += "<div>Following are issues available in your package:</div>";
                        }
                        if (parseInt(data.TicketTotalCountsByStatus['OPEN'])) {
                            $scope.ticket_response += '<div>OPEN=>' + parseInt(data.TicketTotalCountsByStatus['OPEN']) + '</div>';
                        }
                        if (parseInt(data.TicketTotalCountsByStatus['IN PROGRESS'])) {
                            $scope.ticket_response += '<div> IN PROGRESS=>' + parseInt(data.TicketTotalCountsByStatus['IN PROGRESS']) + '</div>';
                        }
                        $scope.cfi_html = '';
                        if ($scope.exportType == 'course') {
                            $scope.course_opt = "<br><br><div class=\"row\"><div class=\"form-group col-sm-10\"><label for=\"exampleInputEmail1\">Export Options:</label> \n\
                                                <select name='course_export_options' id='course_export_options'>\n\
                                                    <option value='html'>HTML</option>\n\
                                                    <option value='sc12'>SCORM 1.2</option>\n\
                                                    <option value='sc2004_3'>SCORM 2004 3rd Ed</option>\n\
                                                    <option value='imscc12'>IMSCC 1.2</option>\n\\n\
                                                    <option value='canvas'>CANVAS EXPORT</option>\n\\n\
                                                    <option value='imscc12pdf'>IMSCC 1.2 From PDF</option>\n\\n\
                                                    <option value='lti'>CANVAS IMSCC LTI</option>\n\
                                                </select></div>  <br/> \n\
                                                <div class=\"form-group col-sm-10\" id='course_export_shell_div' style=\"display:none;\"><label for=\"exampleInputEmail1\">Prefered Shell:</label>\n\
                                                    <select ng-if='' id='course_export_shell_options' name='course_export_shell_options' >\n\
                                                    <option value='NO_SHELL'>-No Shell-</option>\n\
                                                    <option value='SHELL1'>Shell 1</option>\n\
                                                    <option value='SHELL2'>Shell 2</option>\n\
                                                </select></div> </div>";
                        }
                        else {
                            $scope.course_opt = "";
                        }
                        if ($scope.exportType == 'edupub') {
                            $scope.export_opt = "<br><br>Export Type: <span><input id='export_typ1' name='export_typ' type='radio' value='full' checked>Full </span><span><input id='export_typ2' name='export_typ' type='radio' value='partial'>Partial </span>";
                            $scope.cfi_html = "<br><br><div style='border: 1px solid #eee;background: #f6f6f6;padding: 4px 10px 0;border-radius: 5px;margin: 0 0 7px 0;' class='cfibox'><div style='font-size: 13px;display: block;padding: 2px 0;text-align: left;margin-bottom: 5px;'>Add info in the textboxes as suggested to generate absolute CFI URLs</div><div class='form-group'><label class='control-label pull-left' style='font-size: 13px;font-weight: 600 !important;' for='cfi_domain_name'>CFI Domain Name</label><input type='text' class='form-control input-sm' placeholder='https://bookshelf.vitalsource.com/?#/books/' name='cfi_domain_name' id='cfi_domain_name'></div><div class='form-group'><label for='cfi_domain_name' style='font-size: 13px;font-weight: 600 !important;' class='control-label pull-left'>CFI BookId</label><input type='text' class='form-control input-sm' placeholder='9781319018016' name='cfi_book_id' id='cfi_book_id' maxlength='50'><p style='text-align: left;margin-top: 5px;font-size: 13px;'>Note: A-head and B-head will not be generated in partial export.</p></div><div style='display:none;font-size:14px;' class='alert alert-danger show-error' role='alert'>Data should be input either in both fields or in none.</div></div>";
                        } else {
                            $scope.export_opt = "";
                        }

                        $scope.getExportOptions = function () {

                            $scope.course_export_option = $('#course_export_options').val();
                        }


                        SweetAlert.swal({
                            title: "Are you sure?",
                            html: $scope.ticket_response + "<br>Do you really want to export this project now?" + $scope.course_opt + $scope.export_opt + $scope.cfi_html,
                            type: "warning",
                            showCancelButton: true,
                            customClass: 'export-project',
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, export it!",
                            cancelButtonText: "No, cancel!",
                            closeOnConfirm: false,
                            closeOnCancel: true,
                        }, function (isConfirm) {

                            //swal.disableButtons();
                            $scope.ticket_response = '';
                            if (isConfirm) {
                                if ($scope.cfi_html != '') {
                                    $scope.cfi_domain_name = $("#cfi_domain_name").val().trim();
                                    $scope.cfi_book_id = $("#cfi_book_id").val().trim();
                                    // check validation one filed should not be filled up
                                    if ((!$scope.cfi_domain_name && $scope.cfi_book_id) || ($scope.cfi_domain_name && !$scope.cfi_book_id)) {
                                        $('.show-error').show();
                                        return false;
                                    }
                                }

                                $("#export_btn").attr("disabled", true);
                                var export_option = $("input[type=radio]:checked").val();
                                if (angular.isUndefined(export_option)) {
                                    export_option = 'full';
                                }
                                if ($scope.exportType == 'course') {
                                    $scope.course_export_option = $('#course_export_options').val();
                                    if ($scope.course_export_option == 'html') {
                                        $scope.course_export_shell_options = $('#course_export_shell_options').val();
                                    } else {
                                        delete $scope.course_export_shell_options;
                                    }
                                } else {
                                    $scope.course_export_option = '';
                                }
                                // close export type popup
                                window.sweetAlert.closeModal();
                                if (export_option == 'full') {
                                    tocService.initiateExport($scope).then(function (data) {
                                        if (data.status == 200) {
                                            var error_response = '';
                                            tocService.getTicketStatusCount($scope).then(function (data) {
                                                if (parseInt(data.TicketTotalCountsByStatus['OPEN']) > 0 || parseInt(data.TicketTotalCountsByStatus['IN PROGRESS']) > 0) {
                                                    error_response += "--------------------------------------------------------";
                                                    error_response += "<div>Following are issues available in your package:</div>";
                                                }
                                                if (parseInt(data.TicketTotalCountsByStatus['OPEN'])) {
                                                    error_response += '<div>OPEN=>' + parseInt(data.TicketTotalCountsByStatus['OPEN']) + '</div>';
                                                }
                                                if (parseInt(data.TicketTotalCountsByStatus['IN PROGRESS'])) {
                                                    error_response += '<div> IN PROGRESS=>' + parseInt(data.TicketTotalCountsByStatus['IN PROGRESS']) + '</div>';
                                                }
                                            });
                                            setTimeout(function () {
                                                SweetAlert.swal(
                                                        {
                                                            title: 'Success',
                                                            allowEscapeKey: true,
                                                            closeOnConfirm: true,
                                                            html: data.message + "<div style='width:100%;overflow-y:auto;max-height:250px;'>" + error_response + "</div>"
                                                        },
                                                function (isConfirm) {
                                                    if (isConfirm) {
                                                        $("#export_btn_opener").removeAttr("disabled");
                                                    } else {
                                                        $("#export_btn_opener").removeAttr("disabled");
                                                    }
                                                }
                                                );
                                            }, 500);
                                            $scope.project.project_export_count = 1;
                                            //$rootScope.project_export_count = 1;
                                        }
                                        $("#export_btn").removeAttr("disabled");
                                    });
                                } else {
                                    objectService.getProjectDetails($scope).then(function (data) {
                                        if (data == null) {
                                            SweetAlert.swal("Cancelled", "Access prohibited!", "error");
                                            $location.path("dashboard");
                                        }
                                        $scope.data = JSON.parse(data.toc);
                                        var modalInstance = $modal.open({
                                            templateUrl: 'templates/toc/partial_toc.html',
                                            controller: 'PartialExport',
                                            size: 'md',
                                            backdrop: 'static',
                                            keyboard: false,
                                            resolve: {
                                                project_id: function () {
                                                    return $scope.project_id;
                                                },
                                                modalFormTile: function () {
                                                    return 'Partial Export';
                                                },
                                                modalFormText: function () {
                                                    return 'Please select nodes to export'
                                                },
                                                cfi_domain_name: function () {
                                                    return  $scope.cfi_domain_name;
                                                },
                                                cfi_book_id: function () {
                                                    return $scope.cfi_book_id;
                                                },
                                                data: function () {
                                                    return $scope.data;
                                                }
                                            }
                                        });
                                        modalInstance.result.then(function (modal_param) {
                                            $scope.project.project_export_count = 1;
                                            //$rootScope.project_export_count = 1;
                                            //console.info(modal_param);
                                            //$rootScope.$broadcast('SOME_TAG', $rootScope.project_export_count);
                                            //console.log($rootScope.project_export_count);
                                        }, function () {
                                            $log.info('Modal dismissed at: ' + new Date());
                                        });
                                        /*$scope.$watch('project.project_export_count', function (val) {                                        
                                         //$scope.project.project_export_count=val;
                                         $rootScope.project_export_count = 1;
                                         });*/
                                        $("#export_btn_opener").removeAttr("disabled");
                                        jQuery("#export_btn").removeAttr("disabled");
                                        /*$scope.$on("SOME_TAG", function(event, project_export_count) {
                                         $rootScope.project_export_count = project_export_count;
                                         console.log('project_export_count =>'+project_export_count);
                                         });*/
                                    });
                                }
                            } else {
                                //SweetAlert.swal("Cancelled", "May be later..", "error");
                                $("#export_btn_opener").removeAttr("disabled");
                                $("#export_btn").removeAttr("disabled");
                            }
                        });
                    });
                }
            };

        },
        template: '<button type="button" id="export_btn" class="btn btn-primary" ng-click="exportProject(\'pxe\');" ng-show="project.project_permissions[\'project.export\'].grant && project.project_type_id == 1" >Export Project</button>\n\
                    <button type="button" id="export_btn" class="btn btn-primary" ng-click="exportProject(\'imscc\');" ng-show="project.project_permissions[\'project.export\'].grant && project.project_type_id == 3" >Export IMS CC</button>\n\
                    <button type="button" id="export_btn" class="btn btn-primary" ng-click="exportProject(\'html\');" ng-show="project.project_permissions[\'project.export\'].grant && project.project_type_id == 3" >Export HTML</button>\n\
                    <button type="button" id="export_btn_opener" class="btn btn-primary" ng-click="exportProject(\'edupub\');" ng-show="project.project_permissions[\'project.export\'].grant && project.project_type_id == 4" >Export EDUPUB</button>\n\
                    <button type="button" id="export_btn" class="btn btn-primary" ng-click="exportProject(\'course\');" ng-show="project.project_permissions[\'project.export\'].grant && project.project_type_id == 5" >Export Course</button>'
    };
});

/**
 * Used to create Project Preview button
 */
app.directive('projectPreviewButton', function () {
    return {
        restrict: 'E',
        scope: {
            projectId: '='
        },
        controller: function ($scope, $element, $attrs, $location, tocService) {
            $scope.project_id = $scope.projectId;

            $scope.previewProject = function (project_id) {
                $location.url("project_preview/" + project_id);
            };
        },
        template: '<button type="button" class="btn btn-primary" ng-click="previewProject(project_id);">Project Preview</button>'
    };
});

/**
 * Used to create Back to dashboard button
 */
app.directive('backToDashboard', function () {
    return {
        restrict: 'E',
        scope: {
            project: '='
        },
        controller: function ($scope, $element, $attrs, $location, tocService) {
            $scope.backToDashBoard = function (toc_creation_status, project_id) {
                if (toc_creation_status == '3000') {
                    $scope.show_toc = 0;
                    $location.path('project_import/' + project_id);
                } else {
                    $scope.show_toc = 1;
                    $location.path('edit_toc/' + project_id);
                }
            }
        },
        template: '<button type="button" class="btn btn-default" ng-if="project.id" ng-click="backToDashBoard(project.toc_creation_status, project.id);"><i class="fa fa-chevron-left"></i>Back to Dashboard</button> '
    };
});

/***
 * Asset Usage count 
 */
app.directive('assetUsageCount', function ($filter) {
    return {
        restrict: 'E',
        scope: {
            assetId: '=',
            templateMode: '=',
            action: '='
        },
        controller: function ($scope, $element, $attrs, assetService, $modal) {
            $scope.asset_id = $scope.assetId;
            $scope.action = $scope.action;
            $scope.usage_count = 0;
            if ($scope.templateMode == '1') {
                $scope.template_string = 'Usage: ' + $scope.usage_count;
            }
            if ($scope.templateMode == '2') {
                $scope.template_string = $scope.usage_count;
            }


            assetService.getAssetUsageCount($scope).then(function (data) {
                if (data.status == '200') {
                    $scope.usage_count = parseInt(data.data.usage_count);
                    if ($scope.usage_count > 0) {
                        if ($scope.templateMode == '1') {
                            $scope.template_string = '<a href="javascript:void(0);">Usage: ' + $scope.usage_count + '</a>';
                        }
                        if ($scope.templateMode == '2') {
                            $scope.template_string = '<a href="javascript:void(0);">' + $scope.usage_count + '</a>';
                        }
                    }

                }
            });

            $scope.openModal = function (entity_id, action) {
                if (action == 'ASSET') {
                    $scope.openAssetUsageModal(entity_id);
                } else if (action == 'CONTENT_OBJECT') {
                    $scope.openContentObjectUsageModal(entity_id);
                }
            }

            $scope.openAssetUsageModal = function (asset_id) {
                var modalInstance = $modal.open({
                    templateUrl: 'templates/asset/assetUsagModalContent.html',
                    controller: 'ModalAssetUsagCtrl',
                    size: 'md',
                    keyboard: false,
                    resolve: {
                        asset_id: function () {
                            return asset_id;
                        }
                    }
                });
                modalInstance.result.then(function () {

                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            $scope.openContentObjectUsageModal = function (asset_id) {
                var modalInstance = $modal.open({
                    templateUrl: 'templates/toc/contentObjectUsagModalContent.html',
                    controller: 'ModalContentObjectUsagCtrl',
                    size: 'md',
                    keyboard: false,
                    resolve: {
                        asset_id: function () {
                            return asset_id;
                        }
                    }
                });
                modalInstance.result.then(function () {

                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };



        },
        templateUrl: 'templates/asset//usage.html'

    };
});

/* license validity for assets */
app.directive('licenseValidity', function ($filter) {
    return {
        restrict: 'E',
        scope: {
            assetId: '=',
            entityType: '='
        },
        controller: function ($scope, $element, $attrs, assetService, $modal, SweetAlert) {
            $scope.asset_id = $scope.assetId;
            $scope.entity_type = $scope.entityType;
            $scope.validity_expired = 0;
            assetService.isAssetLicenseExpired($scope).then(function (data) {
                if (data.status == '200') {
                    $scope.validity_expired = data.data.licenseValidityExpired;
                }
            });

            $scope.openPopup = function (entity_id) {
                SweetAlert.swal({title: "Notification", text: "Asset has passed the License period.Please update asset/license information.", showCancelButton: false});
            }
        },
        template: '<div ng-show="validity_expired" ng-click="openPopup();" style="cursor:pointer;padding-top:8px;" class="pull-left" title="license has expired"><i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:red"></i></div>'
    };
});

/* license validity for assets under assessment */
app.directive('otherLicenseValidity', function ($filter) {
    return {
        restrict: 'E',
        scope: {
            assetId: '=', //assetId refers assessmentID
            entityType: '='
        },
        controller: function ($scope, $element, $attrs, assetService, $modal, SweetAlert) {
            $scope.asset_id = $scope.assetId;//assetId refers assessmentID
            $scope.entity_type = $scope.entityType;
            $scope.validity_expired = 0;
            assetService.isAssetLicenseExpired($scope).then(function (data) {
                if (data.status == '200') {
                    $scope.validity_expired = data.data.licenseValidityExpired;
                }
            });
            $scope.openPopup = function () {
                SweetAlert.swal({title: "Notification", text: "Asset used on the content has passed the License period.Please go to asset management and update asset/license information.", showCancelButton: false});
            }
        },
        template: '<span ng-show="validity_expired" ng-click="openPopup();" style="cursor:pointer;" title="license has expired"><i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:red"></i></span>'
    };
});
/* update availability notification for replicated content objects */
app.directive('updateAvailable', function ($filter) {
    return {
        restrict: 'E',
        scope: {
            objectId: '=',
            projectId: '='
        },
        controller: function ($scope, $element, $attrs, tocService, assetService, $modal, $log, SweetAlert) {
            $scope.object_id = $scope.objectId;
            $scope.update_available = 0;
            tocService.isUpdateAvailable($scope).then(function (data) {
                if (data.status == '200') {
                    $scope.update_available = data.data.updateAvailable;
                }
            });
            $scope.openUpdateModal = function () {



                swal({
                    title: "The content for this file is updated in Global repository",
                    type: "warning",
                    html: "<div style=\"height:100px;\"><center><h4>Do you want to bring in the updated copy?</h4></center></br></br></br></br>\n\
                        <button style=\"background: #673AB7 !important; border: 1px solid #673AB7 !important; border-radius: 3px; padding: 5px 20px; text-transform: uppercase; font-family: 'Roboto Condensed', sans-serif;\" type=\"button\" id=\"btnA\" >Yes</button> " +
                            "<button style=\"background: #fff !important; color: #999999 !important; border: 1px solid #999999 !important; border-radius: 3px !important; padding: 5px 20px !important; text-transform: uppercase !important; font-family: 'Roboto Condensed', sans-serif;\" type=\"button\" id=\"btnB\" >No</button>" +
                            "<button style=\"background: #fff !important; color: #999999 !important; border: 1px solid #999999 !important; border-radius: 3px !important; padding: 5px 20px !important; text-transform: uppercase !important; font-family: 'Roboto Condensed', sans-serif;\" type=\"button\" id=\"btnC\" >Preview updates</button>" +
                            "<button style=\"background: #fff !important; color: #999999 !important; border: 1px solid #999999 !important; border-radius: 3px !important; padding: 5px 20px !important; text-transform: uppercase !important; font-family: 'Roboto Condensed', sans-serif;\"type=\"button\" id=\"btnD\" >Decide Later</button></div>",
                    showConfirmButton: false,
                    closeOnConfirm: false,
                });
                jQuery(document).on('click', "#btnA", function () {
                    SweetAlert.swal({
                        title: "Alert Message",
                        text: "Please confirm to update?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Confirm",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: true},
                    function (isConfirm) {
                        if (isConfirm) {
                            //api call to update the document
                            tocService.updateContent($scope).then(function (data) {
                                if (data.status == '200') {
                                    SweetAlert.swal({
                                        title: "Updated!",
                                        text: "The content has been updated.",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55", confirmButtonText: "Ok",
                                        closeOnConfirm: false,
                                    }, function () {
                                        location.reload();
                                    });
                                }
                            });
                        } else {
                            SweetAlert.swal("Cancelled", "No changes are being made to the content", "error");
                        }
                    }
                    );
                });
                jQuery(document).on('click', "#btnB", function () {
                    tocService.cancelContentUpdate($scope).then(function (data) {
                        if (data.status == '200') {
                            location.reload();
                        }
                    });
                });
                jQuery(document).on('click', "#btnD", function () {
                    //SweetAlert.swal("Cancelled", "No changes are being made to the content", "error");
                });

                jQuery(document).off().on('click', "#btnC", function (event) {                                     
                    tocService.getSourceContentObjectId($scope).then(function (data) {
                        if (data.status == '200') {
                            $scope.source_objectid = data.data.source_object_id;
                            $scope.previewContent('lg', $scope.source_objectid);
                        }
                    });                    
                    event.stopPropagation();
                });
            };


            $scope.previewContent = function (size, id) {
                var contentRec = {};
                $scope.id = id;
                assetService.getGlobalContentDetails($scope).then(function (data) {
                    contentRec = data.data;
                    var modalInstance = $modal.open({
                        templateUrl: 'templates/toc/globalModalContentView.html',
                        controller: 'ModalInstanceContentCtrl',
                        size: size,
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            contentRec: function () {
                                return contentRec;
                            },
                            object_id: function () {
                                return id;
                            },
                            modalFormText: function () {
                                return "You are trying to view a content";
                            },
                            modalFormTile: function () {
                                return "Preview Content";
                            }
                        }
                    });
                    modalInstance.result.then(function () {

                    }, function () {
                        $log.info('Modal dismissed at: ' + new Date());
                    });
                });
            };
        },
        template: '<span ng-show="update_available" ng-click="openUpdateModal();" style="cursor:pointer;" title="update available"><i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:green"></i></span>'
    };
});

/* 
 * Check license validity and update availability fort TOC   
 */
app.directive('tocLicenseValidityAndUpdateAvailability', function ($filter) {
    return {
        restrict: 'E',
        scope: {
            //projectId: '=', 
            objectId: '=',
            ticketStatusCount: '='
        },
        controller: function ($scope, $element, $attrs, tocService, assetService, $modal, $log, SweetAlert) {
            $scope.project_id = $scope.projectId;
            $scope.object_id = $scope.objectId;
            $scope.ticket_status_count = $scope.ticketStatusCount;
            /*tocService.tocLicenseAndUpdateAvailabilityCheck($scope).then(function (data) {               
            });*/
            $scope.validity_expired = angular.isDefined($scope.ticket_status_count[$scope.object_id]) ? $scope.ticket_status_count[$scope.object_id]['is_license_validity_expired'] : '0';
            //console.log($scope.object_id+'=='+$scope.validity_expired);
            
            $scope.openPopup = function () {
                SweetAlert.swal({title: "Notification", text: "Asset used on the content has passed the License period.Please go to asset management and update asset/license information.", showCancelButton: false});
            }
            
            $scope.update_available = angular.isDefined($scope.ticket_status_count[$scope.object_id]) ? $scope.ticket_status_count[$scope.object_id]['updateAvailable'] : '0';
            $scope.openUpdateModal = function () {
                swal({
                    title: "The content for this file is updated in Global repository",
                    type: "warning",
                    html: "<div style=\"height:100px;\"><center><h4>Do you want to bring in the updated copy?</h4></center></br></br></br></br>\n\
                        <button style=\"background: #673AB7 !important; border: 1px solid #673AB7 !important; border-radius: 3px; padding: 5px 20px; text-transform: uppercase; font-family: 'Roboto Condensed', sans-serif;\" type=\"button\" id=\"btnA\" >Yes</button> " +
                            "<button style=\"background: #fff !important; color: #999999 !important; border: 1px solid #999999 !important; border-radius: 3px !important; padding: 5px 20px !important; text-transform: uppercase !important; font-family: 'Roboto Condensed', sans-serif;\" type=\"button\" id=\"btnB\" >No</button>" +
                            "<button style=\"background: #fff !important; color: #999999 !important; border: 1px solid #999999 !important; border-radius: 3px !important; padding: 5px 20px !important; text-transform: uppercase !important; font-family: 'Roboto Condensed', sans-serif;\" type=\"button\" id=\"btnC\" >Preview updates</button>" +
                            "<button style=\"background: #fff !important; color: #999999 !important; border: 1px solid #999999 !important; border-radius: 3px !important; padding: 5px 20px !important; text-transform: uppercase !important; font-family: 'Roboto Condensed', sans-serif;\"type=\"button\" id=\"btnD\" >Decide Later</button></div>",
                    showConfirmButton: false,
                    closeOnConfirm: false,
                });
                
                                
                //jQuery(document).off().on('click', "#btnA", function () {
                jQuery('#btnA').off().on('click', function () {                    
                    SweetAlert.swal({
                        title: "Alert Message",
                        text: "Please confirm to update?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55", confirmButtonText: "Confirm",
                        cancelButtonText: "No",
                        closeOnConfirm: false,
                        closeOnCancel: true},
                    function (isConfirm) {
                        if (isConfirm) {
                            //api call to update the document
                            tocService.updateContent($scope).then(function (data) {
                                if (data.status == '200') {
                                    SweetAlert.swal({
                                        title: "Updated!",
                                        text: "The content has been updated.",
                                        type: "success",
                                        showCancelButton: false,
                                        confirmButtonColor: "#DD6B55", confirmButtonText: "Ok",
                                        closeOnConfirm: false,
                                    }, function () {
                                        location.reload();
                                    });
                                }
                            });
                        } else {
                            SweetAlert.swal("Cancelled", "No changes are being made to the content", "error");
                        }
                    }
                    );
                });
                jQuery('#btnB').on('click', function () {
                    tocService.cancelContentUpdate($scope).then(function (data) {
                        if (data.status == '200') {
                            location.reload();
                        }
                    });
                });
                jQuery('#btnD').on('click', function () {
                    SweetAlert.swal("Cancelled", "No changes are being made to the content", "error");
                });

                jQuery('#btnC').off().on('click', function (event) {                                     
                    tocService.getSourceContentObjectId($scope).then(function (data) {
                        if (data.status == '200') {
                            $scope.source_objectid = data.data.source_object_id;
                            $scope.previewContent('lg', $scope.source_objectid);
                        }
                    });                    
                    event.stopPropagation();
                });
            };


            $scope.previewContent = function (size, id) {
                var contentRec = {};
                $scope.id = id;
                assetService.getGlobalContentDetails($scope).then(function (data) {
                    contentRec = data.data;
                    var modalInstance = $modal.open({
                        templateUrl: 'templates/toc/globalModalContentView.html',
                        controller: 'ModalInstanceContentCtrl',
                        size: size,
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            contentRec: function () {
                                return contentRec;
                            },
                            object_id: function () {
                                return id;
                            },
                            modalFormText: function () {
                                return "You are trying to view a content";
                            },
                            modalFormTile: function () {
                                return "Preview Content";
                            }
                        }
                    });
                    modalInstance.result.then(function () {

                    }, function () {
                        $log.info('Modal dismissed at: ' + new Date());
                    });
                });
            };
            
            
            
        },
        template: '<span ng-show="validity_expired" ng-click="openPopup();" style="cursor:pointer;" title="license has expired"><i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:red"></i></span> \n\
                  <span ng-show="update_available" ng-click="openUpdateModal();" style="cursor:pointer;" title="update available"><i class="fa fa-exclamation-triangle" aria-hidden="true" style="color:green"></i></span>'
    };
});