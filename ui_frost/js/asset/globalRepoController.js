/*****
 * Controller for ASSET
 * Author - Alamgir Hossain Sk * 
 */

'use strict';

app.controller('global_repo', ['$scope', 'sessionService', '$location', '$routeParams', '$rootScope', '$log', 'SweetAlert', 'assetService', '$modal', 'objectService', '$filter', '$controller', 'taxonomyService', 'keyValueService', 'tagService', 'ivhTreeviewMgr', '$compile', 'userService', '$timeout', function ($scope, sessionService, $location, $routeParams, $rootScope, $log, SweetAlert, assetService, $modal, objectService, $filter, $controller, taxonomyService, keyValueService, tagService, ivhTreeviewMgr, $compile, userService, $timeout) {

        $scope.access_token = sessionService.get('access_token');
        $scope.user_id = sessionService.get('uid');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        $scope.project_id = angular.isUndefined($routeParams.project_id) ? '' : $routeParams.project_id;
        //$scope.object_id = '';
        $scope.showAsset = false;
        $scope.showTemplate = false;
        $scope.showPattern = false;
        $scope.showContent = false;
        $scope.showAccess = false;
        $scope.showAssessment = true;




        if ($scope.user_permissions['global_asset.show.all'].grant) {
            $scope.showAsset = true;
        }
        if ($scope.user_permissions['global_content.show.all'].grant) {
            $scope.showContent = true;
        }
        if ($scope.user_permissions['global_pattern.show.all'].grant) {
            $scope.showPattern = true;
        }
        if ($scope.user_permissions['global_template.show.all'].grant) {
            $scope.showTemplate = true;
        }

        /*Make active specific tab after returning from editor */
        $scope.global_tab = angular.isUndefined($routeParams.global_tab) ? '' : $routeParams.global_tab;
        if ($scope.global_tab == 'ContentObject') {
            $scope.showContent = true;
            $('.nav-tabs a[data-target="#ContentObject"]').tab('show');
        }
        if ($scope.global_tab == 'TEMPLATES') {
            $scope.showTemplate = true;
            $('.nav-tabs a[data-target="#TEMPLATES"]').tab('show');
        }
        if ($scope.global_tab == 'ASSESSMENTS') {
            $scope.showAssessment = true;
            $('.nav-tabs a[data-target="#ASSESSMENTS"]').tab('show');
        }
        /*End*/


        if ($scope.user_permissions['global_asset.show.all'].grant === false && $scope.user_permissions['global_content.show.all'].grant === false && $scope.user_permissions['global_pattern.show.all'].grant === false && $scope.user_permissions['global_template.show.all'].grant === false) {
            angular.element("#ACCESS").show();
            $scope.showAccess = true;
        }

        if ($scope.showAsset === false && $scope.showContent === true) {
            $('.nav-tabs a[data-target="#ContentObject"]').tab('show');
        }
        if ($scope.showAsset === false && $scope.showContent === false && $scope.showPattern === true) {
            $('.nav-tabs a[data-target="#PATTERNS"]').tab('show');
        }

        if ($scope.showAsset === false && $scope.showContent === false && $scope.showPattern === false && $scope.showTemplate === true) {
            $('.nav-tabs a[data-target="#TEMPLATES"]').tab('show');
        }

        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
            return true;
        }

        $scope.node_id = 0;

        $scope.showSearchBox = true;
        $scope.searchModel = {};
        $scope.searchModel.search_text = '';

        $scope.advanced_search = {};
        $scope.advanced_search.enableSearch = false;
        $scope.advanced_search.title = '';
        $scope.advanced_search.active = false;
        $scope.advanced_search.isCollapsed = false;
        $scope.advanced_search.heading = 'Advanced Search Result:';

        $scope.advanced_search.key_data = {};
        $scope.advanced_search.value_data = {};
        $scope.advanced_search.totalKeyArea = 0;
        $scope.advanced_search.taxonomy = [];
        $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
        $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
        $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';
        // get taxonomy
        $scope.taxonomyStatus = 1001;
        taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
            if (data.status == 200)
            {
                if (data.data != null && angular.isDefined(data.data.children))
                {
                    $scope.advanced_search.taxonomy = data.data.children;
                    // expand nodes
                    ivhTreeviewMgr.expandRecursive($scope.advanced_search.taxonomy, $scope.advanced_search.taxonomy);
                }
            }

        });

        // get tags 
        var tags = [];
        var object_tags = [];


        $scope.advanced_search.tag = {
            multiple: true,
            data: tags
        };
        tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
            if (data.status == 200)
            {
                if (data.data != null && angular.isDefined(data.data.tags))
                {
                    angular.forEach(data.data.tags, function (value, key) {
                        tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.advanced_search.tag.data = tags;
                    if (angular.isDefined(data.data.object_tags))
                    {
                        angular.forEach(data.data.object_tags, function (value, key) {
                            object_tags[key] = {id: value.id, text: value.text};
                        });
                        $scope.advanced_search.tagValue = object_tags;
                    }
                }
            }

        });

        // get owners 
        var owners = [];
        var object_owners = [];

        $scope.advanced_search.owner = {
            multiple: true,
            data: owners
        };

        userService.getUsers($scope).then(function (data) {
            if (data.status == 200)
            {
                if (data.data != null)
                {
                    angular.forEach(data.data, function (value, key) {
                        owners[key] = {id: value.id, text: value.username};
                    });
                    $scope.advanced_search.owner.data = owners;
                }
            }

        });

        $scope.keyOptions = '<option value=""></option>';
        $scope.keyValueData = '';
        // get key values
        keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
            if (data.status == 200)
            {
                $scope.keyValueData = data.data;
                if (data.data != null && angular.isDefined(data.data.key_values))
                {
                    angular.forEach(data.data.key_values, function (value) {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + value.key_name + '</option>';
                    });

                    setTimeout(function () {
                        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
                        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
                        angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));

                    }, 500);
                }
            }
        });


        $scope.addKeyValue = function () {
            $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
            var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
            angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        };


        $scope.onChangeKeyUpdateValue = function (key) {
            var keyId = $scope.advanced_search.key_data['key_' + key];
            angular.forEach($scope.keyValueData.key_values, function (value) {
                if (value.id == keyId)
                {
                    if (value.key_type == 'select_list')
                    {
                        var valueSelect = '<select style="width:100%" ui-select2 ng-model="advanced_search.value_data.key_' + key + '" data-placeholder="Select a Value">';
                        valueSelect += '<option value=""></option>';
                        angular.forEach(value.values, function (valueObj) {
                            valueSelect += '<option value="' + valueObj.value_id + '">' + valueObj.value + '</option>';
                        });
                        valueSelect += '</select>';
                        valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                        angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                    } else if (value.key_type == 'date_entry') {
                        var valueInput = '<input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="advanced_search.value_data.key_' + key + '" bs-datepicker>';
                        valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                        angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                    } else if (value.key_type == 'time_entry') {
                        var valueInput = '<input type="text" data-time-type="string" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="advanced_search.value_data.key_' + key + '"  name="advanced_search.value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                        valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                        angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                    } else if (value.key_type == 'number_entry') {
                        var valueInput = '<input type="number" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                        valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                        angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                    } else {
                        var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                        valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                        angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                    }
                }
            });

        };

        $scope.removeKeyValue = function (keyValueArea) {
            angular.element("#" + keyValueArea).remove();
            var keyArray = keyValueArea.split('_');
            delete $scope.advanced_search.key_data['key_' + keyArray[1]];
            delete $scope.advanced_search.value_data['key_' + keyArray[1]];
            if (angular.element(".advanced-search-key-values-area").length == 0)
            {
                $scope.advanced_search.totalKeyArea = 1;
                $scope.advanced_search.key_data = {};
                $scope.advanced_search.value_data = {};
                var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
                angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
            }

        };

        $scope.generateKeyValue = function (key) {
            var keySelect = '<div class="form-group"><div class="row advanced-search-key-values-area" id="advancedSearchKeyValuesArea_' + key + '">';
            keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="advanced_search.key_data.key_' + key + '" data-placeholder="Select a Key">';
            keySelect += $scope.keyOptions + '</select></div>';
            keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group adv-input-group" id="advancedSearchKeyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
            return keySelect;
        };

        $scope.applyAdvancedSearch = function () {
            // check selected tabs are pattern or template
            var args = {};
            args.selectedTab = angular.element('.nav-tabs .active').text();
            if (args.selectedTab == 'Patterns' || args.selectedTab == 'Templates') {
                $('.nav-tabs a[data-target="#ASSETS"]').tab('show');
            }

            $scope.showTemplate = false;
            $scope.showPattern = false;
            $scope.advanced_search.heading = 'Advanced Search Result:';
            $scope.advanced_search.enableSearch = true;
            $scope.advanced_search.active = true;
            $scope.advanced_search.isCollapsed = false;
            $scope.searchModel.search_text = '';

            $rootScope.$broadcast('apply-advaced-search', args);
            //$scope.generateAdvanceSearchTerm();
        };

        $scope.clearSearch = function () {
            $scope.showTemplate = true;
            $scope.showPattern = true;
            $scope.advanced_search.active = false;
            $scope.searchModel.search_text = '';
            $scope.advanced_search.enableSearch = false;
            var args = {};
            args.selectedTab = angular.element('.nav-tabs .active').text();
            $rootScope.$broadcast('clear-advaced-search', args);
        };

        $scope.toggleAdvanceSearch = function () {
            if ($scope.advanced_search.isCollapsed) {
                $scope.advanced_search.isCollapsed = false;
                if ($scope.advanced_search.active) {
                    $scope.advanced_search.enableSearch = true;
                }
            } else {
                $scope.advanced_search.enableSearch = false;
                $scope.advanced_search.isCollapsed = true;
            }

        };

        $scope.applyAdvancedClear = function () {
            $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
            var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
            angular.element("#advancedSearchKeyValuesAreaId").html($compile(newKeyAreaHtml)($scope));
            $scope.advanced_search.tagValue = [];
            $scope.advanced_search.ownerValue = [];
            $scope.advanced_search.title = '';
            $scope.advanced_search.key_data = {};
            $scope.advanced_search.value_data = {};
            ivhTreeviewMgr.deselectAll($scope.advanced_search.taxonomy);
        };

        $scope.generateAdvanceSearchTerm = function () {
            var searchTerm = [];
            var tagList = $scope.advanced_search.tagValue;
            if (tagList.length > 0) {
                angular.forEach(tagList, function (tag) {
                    searchTerm.push(tag.text);
                });
            }
            var ownerList = $scope.advanced_search.ownerValue;
            if (ownerList.length > 0) {
                angular.forEach(ownerList, function (owner) {
                    searchTerm.push(owner.text);
                });
            }
            var taxonomyList = $scope.selectedTaxonomy;
            if (taxonomyList.length > 0) {
                angular.forEach(taxonomyList, function (taxonomy) {
                    searchTerm.push(taxonomy);
                });
            }

            if ($scope.advanced_search.title != '') {
                searchTerm.push($scope.advanced_search.title);
            }

        };

        $scope.selectedTaxonomy = [];
        $scope.selectedTaxonomyIds = [];

        $scope.$watch('advanced_search.taxonomy', function (newValues) {
            $scope.selectedTaxonomy.length = 0;
            $scope.selectedTaxonomyIds.length = 0;
            angular.forEach(newValues, function (item) {
                if (item.selected == true) {
                    $scope.selectedTaxonomy.push(item.title);
                    $scope.selectedTaxonomyIds.push(item.id);
                }
            });
        }, true);

        $scope.licenseNotification = function () {
            SweetAlert.swal({title: "Notification", text: "Asset used on the content has passed the License period.Please go to asset management and update asset/license information.", showCancelButton: false});
        };
    }]);

