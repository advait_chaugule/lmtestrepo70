/*****
 * Controller for ASSET
 * Author - Koushik Samanta * 
 */

'use strict';

app.controller('asset', ['$scope', 'sessionService', '$location', '$routeParams', '$rootScope', '$log', 'SweetAlert', 'assetService', '$modal', 'objectService', '$filter', '$controller', 'taxonomyService', 'keyValueService', 'tagService', 'ivhTreeviewMgr', '$compile', 'userService', '$timeout', function ($scope, sessionService, $location, $routeParams, $rootScope, $log, SweetAlert, assetService, $modal, objectService, $filter, $controller, taxonomyService, keyValueService, tagService, ivhTreeviewMgr, $compile, userService, $timeout) {
        $scope.access_token = sessionService.get('access_token');
        $scope.user_id = sessionService.get('uid');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        $scope.project_id = angular.isUndefined($routeParams.project_id) ? '' : $routeParams.project_id;
        //$scope.object_id = '';
        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
            return true;
        }
        $scope.node_id = 0;
        $rootScope.showSearchBox = true;
        $scope.searchModel = {};
        $scope.searchModel.search_text = '';
        $scope.advanced_search = {};
        $scope.advanced_search.enableSearch = false;
        $scope.advanced_search.title = '';
        $scope.advanced_search.active = false;
        $scope.advanced_search.isCollapsed = false;
        $scope.advanced_search.heading = 'Advanced Search Result:';
        $scope.local_global = 0;
        $scope.showCheckAll = 0;
        $scope.showMakeGlobal = 0;
        $scope.is_local_global_list = 'local';
        $scope.asset_filter = {};
        $scope.asset_local_global = {};
        $scope.asset_filter.images = true;
        $scope.asset_filter.videos = true;
        $scope.asset_filter.audio = true;
        $scope.asset_filter.gadgets = true;
        $scope.asset_filter.other = true;
        $scope.asset_local_global.local = true;
        $scope.asset_local_global.global = false;
        $scope.totalAssets = 0;
        $scope.disable_upload = true;
        //$scope.search_text = '';
        /*
         * Change View
         */
        $scope.viewMode = sessionService.get('viewMode') == null ? 1 : sessionService.get('viewMode');
        $scope.changeView = function (mode) {
            $scope.viewMode = mode;
            //sessionService.set('viewMode', mode);
        };

        $scope.$flow.defaults.flowRepoDir = 'asset';
        var scopecounter = 0;
        $scope.MsgCounter = 1;
        $scope.SuccessMsgCounter = 0;
        $scope.fileUploadMessage = '';
        $scope.$watch('$flow.progress()', function (val) {
            if ($filter('number')(val * 100, 0) == 100) {

            }
        });


        $scope.uploader = {
            controllerFn: function ($flow, $file, $message) {
                $file.msg = $message; // Just display message for a convenience
                if ($file.size == 0) {
                    $scope.fileUploadMessage += '<p style="text-align:left">' + $scope.MsgCounter + '. File <span style="word-wrap: break-word;">' + $file.name + '<span> should have some content' + '</p>';
                    $scope.MsgCounter = $scope.MsgCounter + 1;
                } else {
                    if ($file.msg.split(':')[0] == 'Error') {
                        $scope.fileUploadMessage += '<p style="text-align:left">' + $scope.MsgCounter + '. ' + $file.msg.substring($file.msg.indexOf(':') + 1) + ' for <span style="word-wrap: break-word;">' + $file.name + '<span></p>';
                        $scope.MsgCounter = $scope.MsgCounter + 1;
                    } else {
                        $scope.SuccessMsgCounter = $scope.SuccessMsgCounter + 1;
                    }
                }
                scopecounter = scopecounter + 1;
                if ($scope.$flow.files.length === scopecounter) {
                    if ($scope.SuccessMsgCounter > 0) {
                        if ($scope.MsgCounter === 1) {
                            $scope.HtmlSuccess = '<p>';
                        } else {
                            $scope.HtmlSuccess = '<p style="text-align:left">' + $scope.MsgCounter + '. ';
                        }
                        $scope.fileUploadMessage += $scope.HtmlSuccess + $scope.SuccessMsgCounter + ' file(s) uploaded successfully</p>';
                    }
                    SweetAlert.swal({html: $scope.fileUploadMessage, width: 540, type: "warning"});
                    $scope.$flow.files.length = 0;
                    scopecounter = 0;
                    $scope.fileUploadMessage = '';
                    $scope.MsgCounter = 1;
                    $scope.SuccessMsgCounter = 0;
                }
            }
        };

        $scope.assets = {};
        //Call Assets
        $scope.currentOrderByField = '';
        $scope.reverseSort = '';
        $scope.currentPage = 1;
        $scope.pageNumber = 1;
        $scope.itemsPerPage = 12;
        $scope.removeErrors = 1;
        $scope.getAssets = function (pageNumber) {
            $scope.ts_hash = '?_ts=' + new Date().getTime()
            $scope.removeErrors = 1;
            $scope.showCheckAll = 0;
            $scope.showMakeGlobal = 0;
            $scope.pageNumber = pageNumber;
            $scope.currentPage = pageNumber;
            $scope.itemsPerPage = 12;
            assetService.getMimes().then(function (mimes) {
                $scope.mimes = mimes.data;
                $scope.show_directive = false;
                assetService.getAssets($scope).then(function (data) {
                    $scope.assets = data.data;
                    angular.forEach($scope.assets, function (value, key) {
                        if (!value.is_global) {
                            $scope.showCheckAll = 1;
                        }
                        if (value.id == $scope.updateAssetId) {
                            value.asset_location += '?_ts=' + new Date().getTime();
                        }
                    });

                    $scope.show_directive = true;
                    $scope.totalAssets = data.totalAssets;
                    $scope.removeErrors = angular.isDefined(data.totalAssets) ? data.totalAssets : 0;
                });

            });
        };
        $scope.getAssets($scope.currentPage);
        //For Pagination Of Dashboard
        $scope.pageChangeHandlerAsset = function (newPage) {
            $scope.selectedAll = false;
            $scope.getAssets(newPage);
        };
        $scope.taxonomy_id = '0';
        //$scope.$flow.opts.query = {taxonomy_id: $scope.taxonomy_id, project_id: $scope.project_id, project_type_id: $scope.project_type_id};
        $scope.setTaxonmonyTitle = function (taxonomy_id, taxonomy_title) {
            $scope.taxonomy_title = '>' + taxonomy_title;
            $scope.taxonomy_id = taxonomy_id;
            $scope.getAssets($scope.currentPage);

            $scope.$flow.opts.query = {taxonomy_id: taxonomy_id, project_id: $scope.project_id, project_type_id: $scope.project_type_id, temp_guid: guid('')};
        }

        //Used to minimize and maximize to Taxonomy
        $scope.toggleMinimized = function (child) {
            child.minimized = !child.minimized;
        };
        //

        $scope.openTaxonomyModal = function (size) {
            $scope.getTaxonomy();
            var modalInstance = $modal.open({
                templateUrl: 'templates/asset/taxonomyModalContent.html',
                controller: 'ModalInstanceTaxonomyCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    patternRec: function () {
                        return '';
                    },
                    modalFormText: function () {
                        return "You are trying to add a Taxonomy. Please rollover a taxonomy and click parent or child.";
                    },
                    modalFormTile: function () {
                        return "Add Taxonomy";
                    },
                    taxonomy: function () {
                        return $scope.taxonomy;
                    },
                    project_id: function () {
                        return $scope.project_id;
                    }
                }
            });
            modalInstance.result.then(function () {
                //Saving TOC    
                /*assetService.getPattern().then(function (data) {
                 $scope.patternList = data.data;
                 });*/
                $scope.getTaxonomy();
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };


        $scope.deleteAsset = function (id, project_id, curr) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this asset!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    var viewMode = $scope.viewMode;
                    //jQuery('#asset_' + id + viewMode).hide();                    
                    assetService.delAsset(id, project_id).then(function (data) {
                        if (curr != 'undefined') {
                            $scope.getAssets(curr);
                        } else {
                            $scope.getAssets($scope.currentPage);
                        }
                        if (data.status == 200) {
                            SweetAlert.swal("Success", "Asset has been deleted", "success");
                        } else {
                            SweetAlert.swal("Cancelled", data.message, "error");
                        }
                    });
                } else {
                    SweetAlert.swal("Cancelled", "Asset is safe :)", "error");
                }
            });
        };



        $scope.deleteGlobalAsset = function (id, project_id, newNumber) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this asset!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    var viewMode = $scope.viewMode;
                    //jQuery('#asset_' + id + viewMode).hide();
                    assetService.delGlobalAsset(id, project_id).then(function () {
                        $scope.getAssets(newNumber);
                    });
                    SweetAlert.swal("Success", "Asset has been deleted", "success");
                } else {
                    SweetAlert.swal("Cancelled", "Asset is safe :)", "error");
                }
            });
        };
        $scope.editbox = 0;
        $scope.viewbox = 0;
        $scope.filterbox = 0;
        /*$scope.editAsset = function (id) {
         $scope.editbox = 1;
         $scope.viewbox = 0;
         $scope.filterbox = 0;
         $('.right-panel-navpanel').show();
         $('.listing-view-content').removeClass('listing-view-content col-md-9').addClass('listing-view-content col-md-9');
         //$scope.project = {'project_permissions':data.project_permissions};            
         assetService.getAssetById(id).then(function (data) {
         //console.info(data);
         $scope.formData = {};
         var metadata = (data.data.metadata == "") ? '' : JSON.parse(data.data.metadata);
         $scope.formData.asset_org_name = data.data.original_name ? data.data.original_name : '';
         $scope.formData.asset_type = data.data.type ? data.data.type : '';
         if (metadata) {
         $scope.formData.asset_height = metadata.height ? metadata.height : '';
         $scope.formData.asset_width = metadata.width ? metadata.width : '';
         $scope.formData.asset_size = metadata.size ? metadata.size : '';
         }
         
         $scope.formData.asset_location = data.data.asset_location ? data.data.asset_location : '';
         $scope.formData.name = data.data.name ? data.data.name : '';
         $scope.formData.id = data.data.object_id ? data.data.object_id : '';
         });
         };*/

        /*$scope.viewAsset = function (id) {
         $scope.viewbox = 1;
         $scope.editbox = 0;
         $scope.filterbox = 0;
         $('.right-panel-navpanel').show();
         $('.listing-view-content').removeClass('listing-view-content col-md-12').addClass('listing-view-content col-md-9');
         assetService.getAssetById(id).then(function (data) {
         $scope.formData = {};
         var metadata = (data.data.metadata == "") ? '' : JSON.parse(data.data.metadata);
         $scope.formData.asset_org_name = data.data.original_name ? data.data.original_name : '';
         $scope.formData.asset_type = data.data.type ? data.data.type : '';
         $scope.formData.object_type_name = data.data.object_type_name ? data.data.object_type_name : '';
         if (metadata) {
         $scope.formData.asset_height = metadata.height ? metadata.height : '';
         $scope.formData.asset_width = metadata.width ? metadata.width : '';
         $scope.formData.asset_size = metadata.size ? metadata.size : '';
         }
         $scope.formData.asset_location = data.data.asset_location ? data.data.asset_location : '';
         $scope.formData.created_date = new Date(data.data.created_at) ? new Date(data.data.created_at) : '';
         $scope.formData.modified_date = new Date(data.data.updated_at).getTime() ? new Date(data.data.updated_at).getTime() : '';
         
         $scope.formData.name = data.data.name ? data.data.name : '';
         $scope.formData.id = data.data.id ? data.data.id : '';
         });
         };*/

        /*$scope.filterAsset = function () {
         $scope.viewbox = 0;
         $scope.editbox = 0;
         $scope.filterbox = 1;
         $('.right-panel-navpanel').show();
         $('.listing-view-content').removeClass('listing-view-content col-md-9').addClass('listing-view-content col-md-6');
         };*/

        $scope.cancel = function () {
            $('.right-panel-navpanel').hide();
            $('.listing-view-content').removeClass('listing-view-content col-md-9').addClass('listing-view-content col-md-12');
        };

        $scope.edit = function (id, asset_form) {
            $scope.formdata = {};
            $scope.formdata.name = asset_form.asset_org_name.$modelValue;
            $scope.formdata.type = asset_form.asset_type.$modelValue;
            $scope.formdata.height = asset_form.asset_height.$modelValue;
            $scope.formdata.width = asset_form.asset_width.$modelValue;
            $scope.formdata.size = asset_form.asset_size.$modelValue;
            //console.log(asset_form.asset_org_name.$modelValue);
            //console.log(asset_form);
            assetService.editAssetById(id, $scope).then(function (data) {
                $scope.getAssets();
                $scope.cancel();
            });
        };
        $scope.filterData = {};
        $scope.filter = function (formData) {
            assetService.filterAssets(formData).then(function (data) {
                $scope.assets = data;
                $scope.totalAssets = data.totalAssets;
            });
        }
        $scope.formData = {};
        $scope.reset = function () {
            $scope.formData = {};
            //console.info($scope.formData);
        }

        // Used to search asset
        /*$scope.assetSearch = function (search_text, pageNumber) {
         $scope.search_text = search_text;
         if ($scope.search_text) {
         $scope.pageNumber = pageNumber;
         $scope.itemsPerPage = 12;
         assetService.getSearchedAssets($scope).then(function (data) {
         $scope.assets = data.data;
         $scope.totalAssets = data.totalAssets;
         });
         } else {
         // If search field is empty                
         $scope.getAssets(1);
         }
         }*/

        $scope.getProject = function () {
            $scope.project_name = '';
            if ($scope.object_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    if (data == null) {
                        $location.path("edit_toc/" + $scope.project_id);
                    } else if (angular.isObject(data)) {
                        $scope.project_name = data.name;
                        $scope.project_type_id = data.project_type_id;
                        $scope.toc_creation_status = data.toc_creation_status;
                        $scope.project = data;
                        $scope.$flow.opts.query = {taxonomy_id: $scope.taxonomy_id, project_id: $scope.project_id, project_type_id: $scope.project_type_id, temp_guid: guid('')};
                        $scope.actual_permission = data.project_permissions['asset.upload'].grant;
                        angular.extend(dst, $controller('asset_preview', {$scope: $scope}));
                    }
                });
            }
        };
        $scope.getProject();

        var dst = {};
        $scope.changeAccess = function (id, check_all) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You want to make this asset global. By doing so you are allowing other user to reuse it on different projects. Please make sure as this action cannot be reverted.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "YES MAKE IT GLOBAL",
                cancelButtonText: "CANCEL",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    var assetArray = [];
                    if (!check_all) {
                        assetArray.push(id);
                    } else {
                        angular.forEach($scope.assets, function (item) {
                            if (item.Selected && !item.is_global) {
                                assetArray.push(item.id);
                            }

                        });
                    }

                    var ids = JSON.stringify(assetArray);

                    assetService.makeGlobal(ids, $scope).then(function (data) {
                        $scope.selectedAll = false;
                        angular.forEach($scope.assets, function (item) {
                            item.Selected = $scope.selectedAll;
                        });
                        $scope.getAssets(0);
                    });
                    SweetAlert.swal("Success", "", "success");
                } else {
                    SweetAlert.swal("Cancelled", "", "error");
                }
            });

        };

        $scope.selectedAll = false;
        $scope.checkAll = function () {
            if (!$scope.selectedAll) {
                $scope.selectedAll = true;
                $scope.showMakeGlobal = true;
            } else {
                $scope.selectedAll = false;
                $scope.showMakeGlobal = false;
            }
            angular.forEach($scope.assets, function (item) {
                item.Selected = $scope.selectedAll;
            });
        };

        $scope.selectOption = function (id) {
            var all_check = true;
            var slected_items = 0;
            angular.forEach($scope.assets, function (item) {
                if (!item.Selected) {
                    all_check = false;
                    $scope.selectedAll = false;
                } else {
                    slected_items = 1;
                }
            });

            if (all_check) {
                $scope.selectedAll = true;
            }
            $scope.showMakeGlobal = slected_items;
        };

        $scope.apply_filter = function () {
            angular.element('.asset-filter-types.active').length;
            $scope.asset_filter.images = false;
            $scope.asset_filter.videos = false;
            $scope.asset_filter.audio = false;
            $scope.asset_filter.gadgets = false;
            $scope.asset_filter.other = false;
            angular.forEach(document.querySelectorAll('.multiselect li.active'), function (value, key) {
                value.innerText;
                if (value.innerText.toLowerCase() == 'all type') {
                    $scope.asset_filter.images = true;
                    $scope.asset_filter.videos = true;
                    $scope.asset_filter.audio = true;
                    $scope.asset_filter.gadgets = true;
                    $scope.asset_filter.other = true;
                }
                else if (value.innerText.toLowerCase() == 'image') {
                    $scope.asset_filter.images = true;
                }
                else if (value.innerText.toLowerCase() == 'audio') {
                    $scope.asset_filter.audio = true;
                }
                else if (value.innerText.toLowerCase() == 'video') {
                    $scope.asset_filter.videos = true;
                }
                else if (value.innerText.toLowerCase() == 'gadget') {
                    $scope.asset_filter.gadgets = true;
                }
                else if (value.innerText.toLowerCase() == 'other') {
                    $scope.asset_filter.other = true;
                }
            });

            $scope.getAssets($scope.currentPage);

            // if all filter are deselected
            /*setTimeout(function () {
             if (angular.element('.asset-filter-types.active').length == 0) {
             $scope.asset_filter.all = true;
             $scope.asset_filter.images = true;
             $scope.asset_filter.videos = true;
             $scope.asset_filter.audio = true;
             $scope.asset_filter.gadgets = true;
             }
             
             if (angular.element('.asset-filter-access.active').length == 0) {
             $scope.asset_local_global.local = true;
             $scope.asset_local_global.global = true;
             }
             $scope.getAssets($scope.currentPage);
             }, 200)*/

        };

        $scope.advanced_search.key_data = {};
        $scope.advanced_search.value_data = {};
        $scope.advanced_search.totalKeyArea = 0;
        $scope.advanced_search.taxonomy = [];
        $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
        $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
        $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';

        // get taxonomy
        $scope.taxonomyStatus = 1001;
        taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
            if (data.status == 200) {
                if (data.data != null && angular.isDefined(data.data.children)) {
                    $scope.advanced_search.taxonomy = data.data.children;
                    // expand nodes
                    ivhTreeviewMgr.expandRecursive($scope.advanced_search.taxonomy, $scope.advanced_search.taxonomy);
                }
            }

        });

        // get tags 
        var tags = [];
        var object_tags = [];


        $scope.advanced_search.tag = {
            multiple: true,
            data: tags
        };
        tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
            if (data.status == 200) {
                if (data.data != null && angular.isDefined(data.data.tags)) {
                    angular.forEach(data.data.tags, function (value, key) {
                        tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.advanced_search.tag.data = tags;
                    if (angular.isDefined(data.data.object_tags)) {
                        angular.forEach(data.data.object_tags, function (value, key) {
                            object_tags[key] = {id: value.id, text: value.text};
                        });
                        $scope.advanced_search.tagValue = object_tags;
                    }
                }
            }

        });

        // get owners 
        var owners = [];
        var object_owners = [];

        $scope.advanced_search.owner = {
            multiple: true,
            data: owners
        };

        userService.getUsers($scope).then(function (data) {
            if (data.status == 200) {
                if (data.data != null) {
                    angular.forEach(data.data, function (value, key) {
                        owners[key] = {id: value.id, text: value.username};
                    });
                    $scope.advanced_search.owner.data = owners;
                }
            }

        });

        $scope.keyOptions = '<option value=""></option>';
        $scope.keyValueData = '';
        // get key values
        keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
            if (data.status == 200) {
                $scope.keyValueData = data.data;
                if (data.data != null && angular.isDefined(data.data.key_values)) {
                    angular.forEach(data.data.key_values, function (value) {
                        $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + value.key_name + '</option>';
                    });

                    setTimeout(function () {
                        $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
                        var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
                        angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));

                    }, 500);
                }
            }
        });


        $scope.addKeyValue = function () {
            $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
            var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
            angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        };


        $scope.onChangeKeyUpdateValue = function (key) {
            var keyId = $scope.advanced_search.key_data['key_' + key];
            angular.forEach($scope.keyValueData.key_values, function (value) {
                if (value.id == keyId) {
                    if (value.key_type == 'select_list') {
                        var valueSelect = '<select style="width:100%" ui-select2 ng-model="advanced_search.value_data.key_' + key + '" data-placeholder="Select a Value">';
                        valueSelect += '<option value=""></option>';
                        angular.forEach(value.values, function (valueObj) {
                            valueSelect += '<option value="' + valueObj.value_id + '">' + valueObj.value + '</option>';
                        });
                        valueSelect += '</select>';
                        valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                        angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                    } else if (value.key_type == 'date_entry') {
                        //$scope.value_data['key_' + key] = null;
                        var valueInput = '<input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="advanced_search.value_data.key_' + key + '" bs-datepicker>';
                        valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                        angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                    } else if (value.key_type == 'time_entry') {
                        var valueInput = '<input type="text" data-time-type="string" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="advanced_search.value_data.key_' + key + '"  name="advanced_search.value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                        valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                        angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                    } else if (value.key_type == 'number_entry') {
                        var valueInput = '<input type="number" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                        valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                        angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));

                    } else {
                        var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="advanced_search.value_data.key_' + key + '">';
                        valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                        angular.element("#advancedSearchKeyValueAreaValue_" + key).html($compile(valueInput)($scope));
                    }
                }
            });

        };

        $scope.removeKeyValue = function (keyValueArea) {
            angular.element("#" + keyValueArea).remove();
            var keyArray = keyValueArea.split('_');
            delete $scope.advanced_search.key_data['key_' + keyArray[1]];
            delete $scope.advanced_search.value_data['key_' + keyArray[1]];
            if (angular.element(".advanced-search-key-values-area").length == 0) {
                $scope.advanced_search.totalKeyArea = 1;
                $scope.advanced_search.key_data = {};
                $scope.advanced_search.value_data = {};
                var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
                angular.element("#advancedSearchKeyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
            }

        };

        $scope.generateKeyValue = function (key) {
            var keySelect = '<div class="form-group"><div class="row advanced-search-key-values-area" id="advancedSearchKeyValuesArea_' + key + '">';
            keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="advanced_search.key_data.key_' + key + '" data-placeholder="Select a Key">';
            keySelect += $scope.keyOptions + '</select></div>';
            keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group adv-input-group" id="advancedSearchKeyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="advanced_search.value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'advancedSearchKeyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
            return keySelect;
        };

        $scope.applyAdvancedSearch = function () {
            //$scope.generateAdvanceSearchTerm();
            $scope.searchModel.search_text = '';
            $scope.advanced_search.heading = 'Advanced Search Result:';
            $scope.advanced_search.enableSearch = true;
            $scope.advanced_search.active = true;
            $scope.advanced_search.isCollapsed = false;
            $scope.getAssets($scope.currentPage);
        };

        $scope.clearSearch = function () {
            $scope.advanced_search.active = false;
            $scope.searchModel.search_text = '';
            $scope.advanced_search.enableSearch = false;
            $scope.getAssets($scope.currentPage);
        };

        $scope.applySorting = function (field) {
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.getAssets($scope.currentPage);
        };

        $scope.assetSorting = function (field) {
            // debugger
            if(field=="objects.updated_at"){
                $scope.reverseSort=true;
                $scope.currentOrderByField = field;
                $scope.getAssets($scope.currentPage);
            }
            else{
                $scope.reverseSort = false;
                $scope.currentOrderByField = field;
                $scope.getAssets($scope.currentPage);
            }
            
        };



        $scope.toggleAdvanceSearch = function () {
            if ($scope.advanced_search.isCollapsed) {
                $scope.advanced_search.isCollapsed = false;
                $scope.disable_upload = true;
                if ($scope.advanced_search.active) {
                    $scope.advanced_search.enableSearch = true;
                }
            } else {
                $scope.advanced_search.isCollapsed = true;
                $scope.advanced_search.enableSearch = false;
                $scope.disable_upload = false;
            }

        };

        $scope.applyAdvancedClear = function () {
            $scope.advanced_search.totalKeyArea = $scope.advanced_search.totalKeyArea + 1;
            var newKeyAreaHtml = $scope.generateKeyValue($scope.advanced_search.totalKeyArea);
            angular.element("#advancedSearchKeyValuesAreaId").html($compile(newKeyAreaHtml)($scope));
            $scope.advanced_search.tagValue = [];
            $scope.advanced_search.ownerValue = [];
            $scope.advanced_search.title = '';
            $scope.advanced_search.key_data = {};
            $scope.advanced_search.value_data = {};
            ivhTreeviewMgr.deselectAll($scope.advanced_search.taxonomy);
        };

        $scope.generateAdvanceSearchTerm = function () {
            var searchTerm = [];
            var tagList = $scope.advanced_search.tagValue;
            if (tagList.length > 0) {
                angular.forEach(tagList, function (tag) {
                    searchTerm.push(tag.text);
                });
            }
            var ownerList = $scope.advanced_search.ownerValue;
            if (ownerList.length > 0) {
                angular.forEach(ownerList, function (owner) {
                    searchTerm.push(owner.text);
                });
            }
            var taxonomyList = $scope.selectedTaxonomy;
            if (taxonomyList.length > 0) {
                angular.forEach(taxonomyList, function (taxonomy) {
                    searchTerm.push(taxonomy);
                });
            }

            if ($scope.advanced_search.title != '') {
                searchTerm.push($scope.advanced_search.title);
            }

            //console.log($scope.advanced_search.key_data.key_1);
            // params.advanced_search.key_data = JSON.stringify($scope.advanced_search.key_data);
            // params.advanced_search.value_data = JSON.stringify($scope.advanced_search.value_data);

        };

        $scope.selectedTaxonomy = [];
        $scope.selectedTaxonomyIds = [];

        $scope.$watch('advanced_search.taxonomy', function (newValues) {
            $scope.selectedTaxonomy.length = 0;
            $scope.selectedTaxonomyIds.length = 0;
            angular.forEach(newValues, function (item) {
                if (item.selected == true) {
                    $scope.selectedTaxonomy.push(item.title);
                    $scope.selectedTaxonomyIds.push(item.id);
                }
            });
        }, true);

    }]);



/*
 * Asset Usage Modal window Controller
 */
app.controller('ModalAssetUsagCtrl', function ($scope, $location, $modalInstance, $window, SweetAlert, assetService, sessionService, objectService, asset_id, tocService) {
    var userinfo = JSON.parse(sessionService.get('userinfo'));
    $scope.user_id = userinfo.id;
    $scope.asset_id = asset_id;
    $scope.asset_usage_list = [];
    //For pagination
    $scope.currentPage = 1;
    $scope.itemsPerPage = 10;
    //
    $scope.asset_usage_total = 0;
    $scope.getAssetUsageList = function (pageNumber) {
        $scope.pageNumber = pageNumber;
        assetService.getAssetUsageList($scope).then(function (data) {
            if (data.status == '200') {
                $scope.asset_usage_list = data.data.list;
                $scope.asset_usage_total = data.data.total;
            }
        });
    }
    $scope.getAssetUsageList(1);

    $scope.pageChangeHandlerObject = function (newPageNumber) {
        $scope.getAssetUsageList(newPageNumber);
    }

    $scope.goToProjectDashboard = function (list) {
        $scope.project_id = list.project_id;
        objectService.getProjectDetails($scope).then(function (data) {
            if (data != null) {
                $location.path("edit_toc/" + list.project_id);
            } else {
                SweetAlert.swal({html: 'Either the project does not exist or you are no longer allocated to this project.', width: 540, type: "warning"});
            }
        });
    }
    $scope.goToEditor = function (list) {
        $scope.project_id = list.project_id;
        objectService.getProjectDetails($scope).then(function (data) {
            if (data != null) {
                var content_from = data.is_global_project == true ? 'global_repo_content' : '';
                $scope.object_id = list.used_in_object_id;
                tocService.checkObjectLock($scope).then(function (data) {
                    if (Number(data.current_lock)) {
                        SweetAlert.swal("Cancelled", "You cannot access this content right now, as it is locked. Please try again later!", "error");
                        return false;
                    } else {
                        if (list.used_in_project_type_id == 4) {
                            //Edupub
                            $window.location.href = "edupub_editor/index.html?access_token=" + sessionService.get('access_token') + "&project_id=" + list.project_id + '&node_id=' + list.used_in_object_id + '&node_title=' + encodeURIComponent(list.used_in_object_name) + '&node_type_name=&node_type_id=&project_type_id=' + list.used_in_project_type_id + '&section_id=&user_id=' + $scope.user_id + '&content_from=' + content_from;
                        } else if (list.used_in_project_type_id == 5) {
                            //Course
                            $window.location.href = "course_editor/index.html?access_token=" + sessionService.get('access_token') + "&project_id=" + list.project_id + '&node_id=' + list.used_in_object_id + '&node_title=' + encodeURIComponent(list.used_in_object_name) + '&node_type_name=&node_type_id=&project_type_id=' + list.used_in_project_type_id + '&section_id=&user_id=' + $scope.user_id + '&content_from=' + content_from;
                        }
                    }
                });



            } else {
                SweetAlert.swal({html: 'Either the project does not exist or you are no longer allocated to this project.', width: 540, type: "warning"});
            }
        });
    }

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

//
/** End of Asset Controller **/
