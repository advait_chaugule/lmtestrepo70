/*****
 * Services for Asset
 * Author - Koushik Samanta 
 *  
 */
'use strict';

//Start of assetService
app.service('assetService', ['$http', '$q', 'sessionService', '$rootScope', function ($http, $q, sessionService, $rootScope) {
        /**
         * Get Assets
         */
        this.getAssets = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            if ($scope.project_id) {
                params.project_id = $scope.project_id;
            }

            if ($scope.taxonomy_id) {
                params.taxonomy_id = $scope.taxonomy_id;
            }
            params.local_global = $scope.local_global;
            params.asset_filter = JSON.stringify($scope.asset_filter);
            params.asset_local_global = JSON.stringify($scope.asset_local_global);
            if (angular.isDefined($scope.searchModel) && angular.isDefined($scope.searchModel.search_text)) {
                params.search_text = window.btoa($scope.searchModel.search_text);
            }

            if (angular.isDefined($scope.currentOrderByField)) {
                params.orderField = $scope.currentOrderByField;
            }

            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                params.orderDirection = dir;
            }

            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                params.pageNumber = $scope.pageNumber;
                params.itemsPerPage = $scope.itemsPerPage;
                //queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }


            // for advance search
            if (angular.isDefined($scope.advanced_search) && angular.isDefined($scope.advanced_search.active) && $scope.advanced_search.active) {
                params.advanced_search_enable = $scope.advanced_search.active;
                params.tags = JSON.stringify($scope.advanced_search.tagValue);
                params.owners = JSON.stringify($scope.advanced_search.ownerValue);
                params.key_data = JSON.stringify($scope.advanced_search.key_data);
                params.value_data = JSON.stringify($scope.advanced_search.value_data);
                params.taxonomy = JSON.stringify($scope.advanced_search.taxonomy);
                params.advanced_search_title = window.btoa($scope.advanced_search.title);
                var deferred = $q.defer();
                $http.post(serviceEndPoint + 'api/v1/asset-search', params)
                        .success(function (data, status, headers, config) {
                            //console.log(data);
                            deferred.resolve(data);
                        }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            } else {
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: serviceEndPoint + 'api/v1/assets',
                    params: params
                }).success(function (data, status, headers, config) {
                    //console.log(data);
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            }

            //params.local_global = $scope.local_global;
            //params.object_types = '102_103_104_111'; // Fetcch only image, audio, video, widget            
            return deferred.promise;
        };
        /**
         * Get Mime Types
         */
        this.getMimes = function () {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/asset-mime',
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.delAsset = function (id, project_id) {
            var deferred = $q.defer();
            $http.delete(serviceEndPoint + 'api/v1/asset/object_id/' + id + '/project_id/' + project_id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }
        
        this.delGlobalAsset = function (id, project_id) {
            var deferred = $q.defer();
            $http.delete(serviceEndPoint + 'api/v1/asset/global_object_id/' + id + '/project_id/' + project_id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }
        this.getAssetById = function (id) {
            var deferred = $q.defer();
            $http.get(serviceEndPoint + 'api/v1/asset/' + id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }

        this.editAssetById = function (id, $scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/asset/asset_id/' + id + '/project_id/' + $scope.project_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT',
                original_name: $scope.formdata.name,
                type: $scope.formdata.type,
                height: $scope.formdata.height,
                width: $scope.formdata.width,
                size: $scope.formdata.size
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        }

        this.filterAssets = function (formData) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            if (formData && formData.asset_org_name) {
                params.org_name = formData.asset_org_name;
            }
            if (formData && formData.asset_type) {
                params.type = formData.asset_type;
            }
            if (formData && formData.asset_size) {
                params.size = formData.asset_size;
            }
            //console.info(formData);
            var deferred = $q.defer();

            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/filter-asset',
                params: params
            }).success(function (data, status, headers, config) {
                //console.log(data);
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        /**
         * Get Searched result of Assets
         */
        this.getSearchedAssets = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/asset-search?access_token=' + sessionService.get('access_token'), {
                object_id: $scope.object_id,
                search_text: $scope.search_text,
                asset_filter: JSON.stringify($scope.asset_filter),
                asset_local_global: JSON.stringify($scope.asset_local_global),
                project_id: $scope.project_id,
                pageNumber: angular.isDefined($scope.pageNumber) ? $scope.pageNumber : '',
                itemsPerPage: angular.isDefined($scope.itemsPerPage) ? $scope.itemsPerPage : '',
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.makeGlobal = function (ids, $scope) {
            var deferred = $q.defer();
            var isLocal = 1;
            if ($scope.local_global) {
                isLocal = 0;
            }
            $http.post(serviceEndPoint + 'api/v1/change-access?access_token=' + sessionService.get('access_token') + '&object_id=' + ids + '&project_id=' + $scope.project_id + '&is_global=' + isLocal,             {
                assessment_source: angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '',
                assessment_id: (angular.isDefined($scope.assessment_id) ? $scope.assessment_id : ''),
            }).success(function (data, status, headers, config) {
                /*if (data.projectPermissionGranted == 401) {
                 $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted);
                 }*/
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.getWidgetManifestDetails = function ($scope) {
            var deferred = $q.defer();
            $http.get(serviceEndPoint + 'api/v1/widget-manifest/object_id/' + $scope.objectId + '?project_id=' + $scope.projectId + '&access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                deferred.resolve(data);
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.updateMetaData = function ($scope) {
            var deferred = $q.defer();
            var fd = new FormData();
            if (angular.isDefined($scope.tags)) {
                fd.append('tags', JSON.stringify($scope.tags));
            }
            if (angular.isDefined($scope.key_data)) {
                fd.append('key_data', JSON.stringify($scope.key_data));
            }
            if (angular.isDefined($scope.value_data)) {
                fd.append('value_data', JSON.stringify($scope.value_data));
            }
            if (angular.isDefined($scope.taxonomy)) {
                fd.append('taxonomy', JSON.stringify($scope.taxonomy));

            }
            if (angular.isDefined($scope.project_id)) {
                fd.append('project_id', $scope.project_id);
            }
            fd.append('title', $scope.title);
            fd.append('asset_file', $scope.asset_file);
            $http({
                method: 'POST',
                url: serviceEndPoint + 'api/v1/asset-update-metadata/' + $scope.object_id + '?access_token=' + sessionService.get('access_token'),
                data: fd,
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the project list
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /**
         * Get Assets
         */
        this.getGlobalAssets = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            if ($scope.project_id) {
                params.project_id = $scope.project_id;
            }
            if ($scope.taxonomy_id) {
                params.taxonomy_id = $scope.taxonomy_id;
            }

            /**if (angular.isDefined($scope.globalContent)) {
             params.globalContent = $scope.globalContent;
             }**/
            //params.local_global = $scope.local_global;
            params.asset_filter = JSON.stringify($scope.asset_filter);
            params.asset_local_global = JSON.stringify($scope.asset_local_global);
            if (angular.isDefined($scope.searchModel.search_text)) {
                params.search_text = window.btoa($scope.searchModel.search_text);
            }
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                params.pageNumber = $scope.pageNumber;
                params.itemsPerPage = $scope.itemsPerPage;
                //queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }

            if (angular.isDefined($scope.currentOrderByField)) {
                params.orderField = $scope.currentOrderByField;
            }

            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                params.orderDirection = dir;
            }

            //var method =  'GET';
            // for advance search
            if (angular.isDefined($scope.advanced_search.active) && $scope.advanced_search.active) {
                params.advanced_search_enable = $scope.advanced_search.active;
                params.tags = JSON.stringify($scope.advanced_search.tagValue);
                params.owners = JSON.stringify($scope.advanced_search.ownerValue);
                params.key_data = JSON.stringify($scope.advanced_search.key_data);
                params.value_data = JSON.stringify($scope.advanced_search.value_data);
                params.taxonomy = JSON.stringify($scope.advanced_search.taxonomy);
                params.advanced_search_title = window.btoa($scope.advanced_search.title);
                params.access_token = sessionService.get('access_token');
                //var method =  'POST';
                //console.log(params);  
                var deferred = $q.defer();
                $http.post(serviceEndPoint + 'api/v1/global-assets-search', params)
                        .success(function (data, status, headers, config) {
                            deferred.resolve(data);
                        }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            } else {
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: serviceEndPoint + 'api/v1/global-assets?access_token=' + sessionService.get('access_token'),
                    params: params
                }).success(function (data, status, headers, config) {
                    //console.log(data);
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            }


            //params.object_types = '102_103_104_111'; // Fetcch only image, audio, video, widget

            /*var deferred = $q.defer();
             $http.post(serviceEndPoint + 'api/v1/global-repo?access_token=' + sessionService.get('access_token'), params)
             .success(function (data, status, headers, config) {
             deferred.resolve(data);
             }).error(function (data, status, headers, config) {
             $rootScope.unauthorised_redirection(data.status);
             });
             
             var deferred = $q.defer();
             $http({
             method: method,//'GET',
             url: serviceEndPoint + 'api/v1/global-repo',
             params: params
             }).success(function (data, status, headers, config) {
             //console.log(data);
             deferred.resolve(data);
             }).error(function (data, status, headers, config) {
             $rootScope.unauthorised_redirection(data.status);
             });*/

            return deferred.promise;
        };


        /**
         * Get Global content
         */
        this.getGlobalContents = function ($scope) {
            var params = {};
            if (angular.isDefined($scope.currentOrderByField)) {
                params.orderField = $scope.currentOrderByField;
            }

            if (angular.isDefined($scope.searchModel.search_text)) {
                params.search_text = window.btoa($scope.searchModel.search_text);
            }

            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort)
                {
                    dir = 'DESC';
                }
                params.orderDirection = dir;
            }
            params.access_token = sessionService.get('access_token');
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                params.pageNumber = $scope.pageNumber;
                params.itemsPerPage = $scope.itemsPerPage;
            }

            // for advance search
            if (angular.isDefined($scope.advanced_search.active) && $scope.advanced_search.active) {
                params.advanced_search_enable = $scope.advanced_search.active;
                params.tags = JSON.stringify($scope.advanced_search.tagValue);
                params.owners = JSON.stringify($scope.advanced_search.ownerValue);
                params.key_data = JSON.stringify($scope.advanced_search.key_data);
                params.value_data = JSON.stringify($scope.advanced_search.value_data);
                params.taxonomy = JSON.stringify($scope.advanced_search.taxonomy);
                params.advanced_search_title = window.btoa($scope.advanced_search.title);
                params.access_token = sessionService.get('access_token');

                var deferred = $q.defer();
                $http.post(serviceEndPoint + 'api/v1/global-content-search', params)
                        .success(function (data, status, headers, config) {
                            deferred.resolve(data);
                        }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            } else {
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: serviceEndPoint + 'api/v1/global-content',
                    params: params
                }).success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
            }


            return deferred.promise;
        };

        /**
         * Get Global content details
         */
        this.getGlobalContentDetails = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/global-content/' + $scope.id,
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        /**
         * Get Global content details for assessment
         */
        this.getGlobalContentDetails_1 = function ($scope, object_id) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/global-content/' + object_id,
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        
        /*
         * 
         * @param {type} $scope
         * @returns {$q@call;defer.promise}
         */
        this.getLOContents = function ($scope) {
            var params = {};
            params.object_id=$scope.object_id;
            params.project_type_id=$scope.project_type;
            params.project_id='';
            params.is_global=$scope.is_global;
            if (angular.isDefined($scope.currentOrderByField)) {
                params.orderField = $scope.currentOrderByField;
            }            
            if (angular.isDefined($scope.reverseSort)) {
                var dir = 'ASC';
                if ($scope.reverseSort){
                    dir = 'DESC';
                }
                params.orderDirection = dir;
            }
            params.access_token = sessionService.get('access_token');
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                params.pageNumber = $scope.pageNumber;
                params.itemsPerPage = $scope.itemsPerPage;
            }

            // for advance search
                var deferred = $q.defer();
                $http({
                    method: 'GET',
                    url: serviceEndPoint + 'api/v1/global-lo-content/'+$scope.object_id,
                    params: params
                }).success(function (data, status, headers, config) {
                    deferred.resolve(data);
                }).error(function (data, status, headers, config) {
                    $rootScope.unauthorised_redirection(data.status);
                });
          


            return deferred.promise;
        };
        
        
        /**
         * 
         * @param {type} $scope
         * @returns {$q@call;defer.promise}
         */
        this.getAssetUsageCount = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/asset-usage-count/' + $scope.asset_id + '?access_token=' + sessionService.get('access_token'),
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }

        this.getAssetUsageList = function ($scope) {
            var params = {};
            params.access_token = sessionService.get('access_token');
            var queryString = '';
            //For Pagination
            if (angular.isDefined($scope.pageNumber) && angular.isDefined($scope.itemsPerPage)) {
                queryString += '&pageNumber=' + $scope.pageNumber + '&itemsPerPage=' + $scope.itemsPerPage;
            }
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/asset-usage-list/' + $scope.asset_id + '?access_token=' + sessionService.get('access_token') + queryString,
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /**
         * Add Global Content object
         * @param {type} $scope
         * @returns {$q@call;defer.promise}
         */
        this.addGlobalContentObject = function ($scope, param) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/global-content-object?access_token=' + sessionService.get('access_token'), {                
                object_name: (angular.isUndefined(param.object_name) ? '' : param.object_name),
                templete_id: (angular.isUndefined(param.templete_id) ? '' : param.templete_id),
                project_type: (angular.isUndefined(param.project_type) ? '' : param.project_type),
                section_page: (angular.isUndefined(param.section_page) ? '' : param.section_page),
                parent_learning_object_id: (angular.isUndefined(param.parent_learning_object_id) ? '' : param.parent_learning_object_id),
                assessment_source: angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '',
                assessment_id: (angular.isUndefined($scope.assessment_id) ? '' : $scope.assessment_id),
                ext_res_link: (angular.isUndefined($scope.ext_res_link) ? '' : $scope.ext_res_link),
                ext_res_url: (angular.isUndefined($scope.ext_res_url) ? '' : $scope.ext_res_url),
                ext_res_type: (angular.isUndefined($scope.ext_res_type) ? '' : $scope.ext_res_type),
                asset_id: (angular.isUndefined($scope.asset_id) ? '' : $scope.asset_id)
            }).success(function (data, status, headers, config) {
                console.log(data);
                $scope.checkChildNode = data;
                deferred.resolve($scope.checkChildNode);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        /**
         * Add Global Content object
         * @param {type} $scope
         * @returns {$q@call;defer.promise}
         */
        this.editGlobalContentObject = function ($scope, param) {
            console.log(param);
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/global-content-object/'+param.object_id+'?access_token=' + sessionService.get('access_token'), { 
                _method: 'PUT',
                object_name: (angular.isUndefined(param.object_name) ? '' : param.object_name),
                project_id: param.project_id,
                tags: (angular.isUndefined($scope.tags) ? '' : JSON.stringify($scope.tags)),
                key_data: (angular.isUndefined($scope.key_data) ? '' : JSON.stringify($scope.key_data)),
                value_data: (angular.isUndefined($scope.value_data) ? '' : JSON.stringify($scope.value_data)),
                taxonomy: (angular.isUndefined($scope.taxonomy) ? '' : JSON.stringify($scope.taxonomy)),
                section_page: (angular.isUndefined($scope.section_page) ? '' : $scope.section_page),
                assessment_source: angular.isDefined($scope.assessment_source) ? $scope.assessment_source : '',
                assessment_id: (angular.isUndefined($scope.assessment_id) ? '' : $scope.assessment_id),
                ext_res_link: (angular.isUndefined($scope.ext_res_link) ? '' : $scope.ext_res_link),
                ext_res_url: (angular.isUndefined($scope.ext_res_url) ? '' : $scope.ext_res_url),
                ext_res_type: (angular.isUndefined($scope.ext_res_type) ? '' : $scope.ext_res_type),
                asset_id: (angular.isUndefined($scope.asset_id) ? '' : $scope.asset_id)
            }).success(function (data, status, headers, config) {
                console.log(data);
                $scope.checkChildNode = data;
                deferred.resolve($scope.checkChildNode);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        this.isAssetLicenseExpired=function($scope){
            var params = {};
            params.access_token = sessionService.get('access_token');
            var deferred = $q.defer();
            $http({
                method: 'GET',
                url: serviceEndPoint + 'api/v1/check-license-validity/entity_type/'+$scope.entity_type+'/asset_id/'+ $scope.asset_id + '?access_token=' + sessionService.get('access_token'),
                params: params
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };        
        
        this.delContent = function (param) {
           var deferred = $q.defer();
           $http({
                method: 'DELETE',
                url: serviceEndPoint + 'api/v1/asset/content_id/' + param.id + '?access_token=' + sessionService.get('access_token'),
                params:param
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        }
        
    }]);