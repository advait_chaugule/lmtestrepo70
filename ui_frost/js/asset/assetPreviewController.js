'use strict';

app.controller('asset_preview', ['$scope', 'sessionService', '$routeParams', '$rootScope', '$modal', '$log', 'assetService', 'SweetAlert', function ($scope, sessionService, $routeParams, $rootScope, $modal, $log, assetService, SweetAlert) {
        $scope.project_permissions = {};
        setTimeout(function () {
            if (angular.isDefined($scope.project_id) && $scope.project_id != '') {
                $scope.project_permissions = $scope.project.project_permissions;
                $scope.actual_permission = $scope.project_permissions['asset.upload'].grant;
            }
        }, 2000);
        $scope.previewAsset = function (size, object_id, modal_form_title, display_option) {
            var modalInstance = $modal.open({
                templateUrl: 'templates/asset/assetModalContent.html',
                controller: 'ModalAssetCtrl',
                size: size,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormTitle: function () {
                        return modal_form_title;
                    },
                    object_id: function () {
                        return object_id;
                    },
                    project_id: function () {
                        return $scope.project_id;
                    },
                    displayOption: function () {
                        return display_option;
                    },
                    user_permissions: function () {
                        return $scope.user_permissions;
                    },
                    project_permissions: function () {
                        return $scope.project_permissions;
                    }
                }
            });
            modalInstance.result.then(function (modalCloseParameter) {
                jQuery(".metadata_save_button").attr("disabled", true);
                $scope.object_id = modalCloseParameter.object_id;
                $scope.applyMetadata = modalCloseParameter.applyMetadata;
                if ($scope.applyMetadata) {
                    $scope.tags = modalCloseParameter.tags;
                    $scope.key_data = modalCloseParameter.key_data;
                    $scope.value_data = modalCloseParameter.value_data;
                    $scope.taxonomy = modalCloseParameter.taxonomy;
                }
                $scope.title = modalCloseParameter.title;
                $scope.asset_file = modalCloseParameter.asset_file;
                $scope.project_id = modalCloseParameter.project_id;
                //Saving metadata
                assetService.updateMetaData($scope).then(function (data) {
                    if (data.status == 400) {
                        SweetAlert.swal("Failed", data.message, "error");
                    }
                    $scope.updateAssetId = data.data;
                    $scope.getAssets($scope.pageNumber);
                    jQuery(".metadata_save_button").attr("disabled", false);
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    }]);


app.controller('ModalAssetCtrl', function ($scope, $filter, $modalInstance, $http, sessionService, object_id, project_id, tagService, keyValueService, taxonomyService, $compile, ivhTreeviewMgr, assetService, modalFormTitle, displayOption, project_permissions, user_permissions) {

    $scope.displayOption = displayOption;
    $scope.applyMetadata = 0;
    $scope.user_permissions = user_permissions;
    $scope.project_permissions = project_permissions;
    $scope.modalFormTitle = modalFormTitle;
    $scope.object_id = object_id;
    $scope.project_id = project_id;
    $scope.showDetails = 0;
    $scope.node_id = object_id;
    $scope.key_data = {};
    $scope.value_data = {};
    $scope.totalKeyArea = 0;
    $scope.taxonomy = [];
    $scope.tplFolderOpen = '<span class="twistie glyphicon glyphicon-folder-open"></span>&nbsp;&nbsp;';
    $scope.tplFolderClose = '<span class="twistie glyphicon glyphicon-folder-close"></span>&nbsp;&nbsp;';
    $scope.tplLeaf = '<span class="twistie glyphicon glyphicon-book"></span>&nbsp;&nbsp;';
    $scope.update_form = {};
    // get asset details
    assetService.getAssetById(object_id).then(function (data) {
        $scope.formdata = {};
        $scope.showDetails = 1;
        var metadata = (data.data.metadata == "") ? '' : JSON.parse(data.data.metadata);
        $scope.asset_org_name = data.data.original_name ? data.data.original_name : '';
        $scope.asset_type = data.data.type ? data.data.type : '';
        $scope.update_form.title = data.data.title ? data.data.title : '';
        $scope.formdata.asset_file = data.data.file_name ? data.data.file_name : '';
        $scope.object_type_name = data.data.object_type_name ? data.data.object_type_name : '';
        $scope.mimetype = data.data.mimetype ? data.data.mimetype : '';
        if (metadata) {
            $scope.asset_height = metadata.height ? metadata.height : '';
            $scope.asset_width = metadata.width ? metadata.width : '';
            $scope.asset_size = metadata.size ? metadata.size : '';
        }
        $scope.ts_hash = '?_ts=' + new Date().getTime();
        $scope.asset_location = data.data.asset_location ? data.data.asset_location : '';
        $scope.created_date = new Date(data.data.created_at) ? new Date(data.data.created_at) : '';
        $scope.modified_date = new Date(data.data.updated_at).getTime() ? new Date(data.data.updated_at).getTime() : '';
        $scope.name = data.data.name ? data.data.name : '';
        $scope.id = data.data.id ? data.data.id : '';
        $scope.is_global = data.data.is_global;
        // check apply metadata permission
        if ($scope.is_global) {
            if (angular.isDefined($scope.user_permissions['tag.apply']) && $scope.user_permissions['tag.apply']) {
                $scope.applyMetadata = $scope.user_permissions['tag.apply'].grant;
            }
        } else {
            if (angular.isDefined($scope.project_permissions['tag.apply']) && $scope.project_permissions['tag.apply']) {
                $scope.applyMetadata = $scope.project_permissions['tag.apply'].grant;
            }

        }

    });

    // get taxonomy 
    $scope.taxonomyStatus = 1001;
    taxonomyService.getTaxonomy($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.children))
            {
                $scope.taxonomy = data.data.children;
                $scope.findSelectedTaxonomy({children:$scope.taxonomy});
                // expand nodes
                ivhTreeviewMgr.expandRecursive($scope.taxonomy, $scope.taxonomy);
            }
            if (data.count === 0) {
                setTimeout(function () {
                    angular.element('#taxonomyStatus').html('No active taxonomy available in the management');
                }, 500);
            }
        }

    });
    //This method is used to select parent indeterminately
    $scope.findSelectedTaxonomy = function(taxonomy_arr){
        if (taxonomy_arr.children.length>0) {
            for (var i = 0; i < taxonomy_arr.children.length; i += 1) {
                var currentChild = taxonomy_arr.children[i];
                $scope.findSelectedTaxonomy(currentChild);
                if(currentChild.selected==true){
                    ivhTreeviewMgr.select($scope.taxonomy, currentChild);
                }
            }
        }        
    };

    // get tags 
    var tags = [];
    var object_tags = [];

    $scope.tag = {
        multiple: true,
        data: tags
    };
    tagService.getTagsByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            if (data.data != null && angular.isDefined(data.data.tags))
            {
                angular.forEach(data.data.tags, function (value, key) {
                    tags[key] = {id: value.id, text: value.text};
                });
                $scope.tag.data = tags;
                if (angular.isDefined(data.data.object_tags))
                {
                    angular.forEach(data.data.object_tags, function (value, key) {
                        object_tags[key] = {id: value.id, text: value.text};
                    });
                    $scope.update_form.tagValue = object_tags;
                }
            }
        }

    });

    $scope.keyOptions = '<option value=""></option>';
    $scope.keyValueData = '';
    // get key values
    keyValueService.getKeyValuesByObject($scope, $scope.node_id).then(function (data) {
        if (data.status == 200)
        {
            $scope.keyValueData = data.data;
            if (data.data != null && angular.isDefined(data.data.key_values))
            {
                angular.forEach(data.data.key_values, function (value) {
                    $scope.keyOptions = $scope.keyOptions + '<option value="' + value.id + '">' + value.key_name + '</option>';
                });

                setTimeout(function () {
                    // populate old key values
                    if (Object.keys(data.data.object_key_values).length)
                    {
                        angular.forEach(data.data.object_key_values, function (object_key_value) {
                            $scope.totalKeyArea = $scope.totalKeyArea + 1;
                            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                            $scope.key_data['key_' + $scope.totalKeyArea] = object_key_value.key_id;
                            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                            $scope.onChangeKeyUpdateValue($scope.totalKeyArea);
                            if (object_key_value.key_type == 'select_list')
                            {
                                $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value_id;
                            } else {
                                if(object_key_value.key_type == 'number_entry'){
                                    $scope.value_data['key_' + $scope.totalKeyArea] = parseFloat(object_key_value.value);
                                }else{
                                   $scope.value_data['key_' + $scope.totalKeyArea] = object_key_value.value;
                                }
                                
                            }

                        });
                    } else {
                        $scope.totalKeyArea = $scope.totalKeyArea + 1;
                        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                    }
                }, 500);
            }

            if (data.count == 0) {
                setTimeout(function () {
                    $scope.totalKeyArea = $scope.totalKeyArea + 1;
                    var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
                    angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
                }, 500);
            }
        }
    });


    $scope.addKeyValue = function () {
        $scope.totalKeyArea = $scope.totalKeyArea + 1;
        var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
        angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
    };


    $scope.onChangeKeyUpdateValue = function (key) {
        var keyId = $scope.key_data['key_' + key];
        angular.forEach($scope.keyValueData.key_values, function (value) {
            if (value.id == keyId)
            {
                if (value.key_type == 'select_list')
                {
                    var valueSelect = '<select style="width:100%" ui-select2 ng-model="value_data.key_' + key + '" data-placeholder="Select a Value">';
                    valueSelect += '<option value=""></option>';
                    angular.forEach(value.values, function (valueObj) {
                        valueSelect += '<option value="' + valueObj.value_id + '">' + $filter('strLimit')(valueObj.value, 20) + '</option>';
                    });
                    valueSelect += '</select>';
                    valueSelect += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueSelect)($scope));
                } else if (value.key_type == 'date_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" class="form-control" ng-keypress="keyPressedEvent($event)" ng-model="value_data.key_' + key + '" data-date-format="MM-dd-yyyy" data-date-type="string" data-min-date="02/10/86" data-autoclose="1" name="date2" bs-datepicker>';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'time_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text" data-time-type="string" ng-keypress="keyPressedEvent($event)" data-time-format="HH:mm" data-length="1" data-minute-step="1" class="form-control" ng-model="value_data.key_' + key + '"  name="value_data.key_' + key + '" bs-timepicker data-arrow-behavior="picker" data-autoclose="1">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else if (value.key_type == 'number_entry') {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="number" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));

                } else {
                    $scope.value_data['key_' + key] = null;
                    var valueInput = '<input type="text"  placeholder="Enter value" class="form-control" ng-model="value_data.key_' + key + '">';
                    valueInput += '<div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div>';
                    angular.element("#keyValueAreaValue_" + key).html($compile(valueInput)($scope));
                }
            }
        });

    };
    
    $scope.keyPressedEvent = function (keyEvent) {
        keyEvent.preventDefault();
    };

    $scope.removeKeyValue = function (keyValueArea) {
        angular.element("#" + keyValueArea).remove();
        var keyArray = keyValueArea.split('_');
        delete $scope.key_data['key_' + keyArray[1]];
        delete $scope.value_data['key_' + keyArray[1]];
        if (angular.element(".key-values-area").length == 0)
        {
            $scope.totalKeyArea = 1;
            $scope.key_data = {};
            $scope.value_data = {};
            var newKeyAreaHtml = $scope.generateKeyValue($scope.totalKeyArea);
            angular.element("#keyValuesAreaId").append($compile(newKeyAreaHtml)($scope));
        }

    };

    $scope.generateKeyValue = function (key) {
        var keySelect = '<div class="form-group"><div class="row key-values-area" id="keyValuesArea_' + key + '">';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Key</label><select style="width:100%" ng-change="onChangeKeyUpdateValue(\'' + key + '\')" ui-select2 ng-model="key_data.key_' + key + '" data-placeholder="Select a Key">';
        keySelect += $scope.keyOptions + '</select></div>';
        keySelect += '<div class="col-sm-6"><label for="metadataValue">Value</label><div class="input-group" id="keyValueAreaValue_' + key + '"><input type="text" class="form-control" ng-model="value_data.key_' + key + '" placeholder="Enter value"><div class="input-group-btn"><button type="button" class="btn btn-success" ng-click="addKeyValue()"><i class="fa fa-plus"></i></button><button type="button" class="btn btn-danger" ng-click="removeKeyValue(\'keyValuesArea_' + key + '\')"><i class="fa fa-trash"></i></button></div></div></div>';
        return keySelect;
    };
    

    /**
     * 
     * @returns {save matadata by object id}
     */
    $scope.saveMetadata = function () {
        var new_array = [];
        angular.forEach($scope.key_data, function (value, key) {
            new_array.push(value);
        });
        var counts = {}, i, value;
        for (i = 0; i < new_array.length; i++) {
            value = new_array[i];
            if (typeof counts[value] === "undefined") {
                counts[value] = 1;
            } else {
                counts[value]++;
            }
        }
        $scope.flag = 0;
        angular.forEach(counts, function (value, key) {
            if (value > 1) {
                $scope.flag = 1;
            }
        });
        if ($scope.flag === 0) {
            var modalCloseParameter = {'project_id': $scope.project_id, 'object_id': $scope.object_id, 'title': $scope.update_form.title, 'asset_file': $scope.formdata.asset_file, 'applyMetadata': $scope.applyMetadata};
            if ($scope.applyMetadata) {
                modalCloseParameter = {'project_id': $scope.project_id, 'object_id': $scope.object_id, 'tags': $scope.update_form.tagValue, 'key_data': $scope.key_data, 'value_data': $scope.value_data, 'taxonomy': $scope.taxonomy, 'title': $scope.update_form.title, 'asset_file': $scope.formdata.asset_file, 'applyMetadata': $scope.applyMetadata};
            }
            $modalInstance.close(modalCloseParameter);
        } else {
            $scope.formError = {
                duplicate_value: 'Duplicate value not allowed'
            };
        }
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

