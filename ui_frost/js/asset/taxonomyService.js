/*****
 * Services for ACL
 * Author - Koushik Samanta 
 *  
 */
'use strict';

//Start of aclService
app.service('taxonomyService', ['$http', '$q', 'sessionService', '$rootScope', function($http, $q, sessionService, $rootScope) {

        /*
         * Service to add Taxonomy
         */
        this.addTaxonomy = function($scope) {            
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/taxonomy?access_token=' + sessionService.get('access_token'), {
                project_id: $scope.project_id,
                taxonomy_text: $scope.taxonomy_text,
                parent_id: $scope.parent_id
            }).success(function(data, status, headers, config) {                
                deferred.resolve(data);                                
            }).error(function(data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);              
            });
            return deferred.promise;
        }
        /*
         * Service to asset taxonomy mapping
         */
        /*this.taxonomyObjectMapping = function($scope) {            
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/taxonomy-object?access_token=' + sessionService.get('access_token'), {                
                taxonomy_id: $scope.taxonomy_id,
                asset_ids: $scope.asset_ids
            }).success(function(data, status, headers, config) {                
                deferred.resolve(data);                                
            }).error(function(data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);              
            });
            return deferred.promise;
        }*/
        

    }]);