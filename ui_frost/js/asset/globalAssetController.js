/*****
 * Controller for ASSET
 * Author - Alamgir Hossain Sk *
 */

'use strict';

app.controller('global_asset', ['$scope', 'sessionService', '$location', '$routeParams', '$rootScope', '$log', 'SweetAlert', 'assetService', '$modal', 'objectService', '$filter', '$controller', function ($scope, sessionService, $location, $routeParams, $rootScope, $log, SweetAlert, assetService, $modal, objectService, $filter, $controller) {
        $scope.access_token = sessionService.get('access_token');
        $scope.user_id = sessionService.get('uid');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        // console.info($scope.user_permissions);
        $scope.project_id = angular.isUndefined($routeParams.project_id) ? '' : $routeParams.project_id;
        //$scope.object_id = '';

        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
            return true;
        }
        $scope.project_type_id = 5;
        objectService.getGlobalProjectId($scope).then(function (data) {
            $scope.project_id = data.global_project_id;
        });
        //$rootScope.showSearchBox = false;
        $scope.local_global = 0;
        $scope.showCheckAll = 0;
        $scope.showMakeGlobal = 0;
        $scope.is_local_global_list = 'local';
        $scope.asset_filter = {};
        $scope.asset_local_global = {};
        $scope.asset_filter.images = true;
        $scope.asset_filter.videos = true;
        $scope.asset_filter.audio = true;
        $scope.asset_filter.gadgets = true;
        $scope.asset_filter.other = true;
        $scope.asset_local_global.local = false;
        $scope.asset_local_global.global = true;
        $scope.totalAssets = 0;
        //$scope.search_text = '';
        /*
         * Change View
         */
        $scope.viewMode = sessionService.get('viewMode') == null ? 1 : sessionService.get('viewMode');
        $scope.changeView = function (mode) {
            $scope.viewMode = mode;
            //sessionService.set('viewMode', mode);
        };

        $scope.$flow.defaults.flowRepoDir = 'asset';

        var scopecounter = 0;
        $scope.MsgCounter = 1;
        $scope.SuccessMsgCounter = 0;
        $scope.fileUploadMessage = '';
        $scope.$watch('$flow.progress()', function (val) {
            if ($filter('number')(val * 100, 0) == 100) {

            }
        });

        $scope.uploader = {
            controllerFn: function ($flow, $file, $message) {
                $file.msg = $message; // Just display message for a convenience
                if ($file.size == 0) {
                    $scope.fileUploadMessage += '<p style="text-align:left">' + $scope.MsgCounter + '. File <span style="word-wrap: break-word;">' + $file.name + '<span> should have some content' + '</p>';
                    $scope.MsgCounter = $scope.MsgCounter + 1;
                } else {
                    if ($file.msg.split(':')[0] == 'Error') {
                        $scope.fileUploadMessage += '<p style="text-align:left">' + $scope.MsgCounter + '. ' + $file.msg.substring($file.msg.indexOf(':') + 1) + ' for <span style="word-wrap: break-word;">' + $file.name + '<span></p>';
                        $scope.MsgCounter = $scope.MsgCounter + 1;
                    } else {
                        $scope.SuccessMsgCounter = $scope.SuccessMsgCounter + 1;
                    }
                }
                scopecounter = scopecounter + 1;
                if ($scope.$flow.files.length === scopecounter) {
                    if ($scope.SuccessMsgCounter > 0) {
                        if ($scope.MsgCounter === 1) {
                            $scope.HtmlSuccess = '<p>';
                        } else {
                            $scope.HtmlSuccess = '<p style="text-align:left">' + $scope.MsgCounter + '. ';
                        }
                        $scope.fileUploadMessage += $scope.HtmlSuccess + $scope.SuccessMsgCounter + ' file(s) uploaded successfully</p>';
                    }
                    SweetAlert.swal({html: $scope.fileUploadMessage, width: 600, type: "warning"});
                    $scope.$flow.files.length = 0;
                    scopecounter = 0;
                    $scope.fileUploadMessage = '';
                    $scope.MsgCounter = 1;
                    $scope.SuccessMsgCounter = 0;
                }
            }
        };

        $scope.assets = {};
        //Call Assets
        $scope.currentOrderByField = '';
        $scope.reverseSort = '';
        $scope.currentPage = 1;
        $scope.pageNumber = 1;
        $scope.itemsPerPage = '';
        $scope.local_global = 1;
        $scope.getAssets = function (pageNumber) {
            $scope.show_directive = false;
            $scope.ts_hash = '?_ts=' + new Date().getTime();
            $scope.orderDirection = '';
            $scope.removeErrors = 1;
            $scope.showCheckAll = 0;
            $scope.showMakeGlobal = 0;
            $scope.pageNumber = pageNumber;
            $scope.currentPage = pageNumber;
            $scope.itemsPerPage = 12;
            assetService.getMimes().then(function (mimes) {
                $scope.mimes = mimes.data;
                assetService.getGlobalAssets($scope).then(function (data) {
                    if ($scope.user_permissions['global_asset.show.all'].grant) {
                        $scope.assets = data.data;

                        angular.forEach($scope.assets, function (value, key) {
                            if (!value.is_global)
                            {
                                $scope.showCheckAll = 1;
                            }
                            if (value.id == $scope.updateAssetId) {
                                value.asset_location += '?_ts=' + new Date().getTime();
                            }

                        });
                        $scope.totalAssets = data.totalAssets;
                        $scope.removeErrors = data.totalAssets;
                    }
                });
            });
            $scope.show_directive = true;
        };
        $scope.getAssets($scope.currentPage);
        //For Pagination Of Dashboard
        $scope.pageChangeHandlerAsset = function (newPage) {
            $scope.getAssets(newPage);
        };

        $scope.applySorting = function (field) {
            if ($scope.currentOrderByField != field) {
                $scope.reverseSort = false;
            } else {
                if ($scope.reverseSort) {
                    $scope.reverseSort = false;
                } else {
                    $scope.reverseSort = true;
                }
            }
            $scope.currentOrderByField = field;
            $scope.getAssets($scope.currentPage);
        };

        $scope.assetSorting = function (field) {
            if (field == "objects.updated_at") {
                $scope.reverseSort = true;
                $scope.currentOrderByField = field;
                $scope.getAssets($scope.currentPage);
            }
            else {
                $scope.reverseSort = false;
                $scope.currentOrderByField = field;
                $scope.getAssets($scope.currentPage);
            }

        };

        /*$scope.show_local_assets = function (swtchtab) {
         $scope.assets = {};
         $scope.totalAssets = 0;
         $scope.local_global = 0;
         $scope.removeErrors = 1;
         $scope.showCheckAll = 0;
         $scope.is_local_global_list = 'local';
         if (swtchtab)
         {
         $scope.pageNumber = '';
         }
         $scope.getAssets($scope.pageNumber);
         };*/

        /*$scope.show_global_assets = function (swtchtab) {
         $scope.assets = {};
         $scope.totalAssets = 0;
         $scope.local_global = 1;
         $scope.removeErrors = 1;
         $scope.showCheckAll = 0;
         $scope.is_local_global_list = 'global';
         if (swtchtab)
         {
         $scope.pageNumber = '';
         }
         $scope.getAssets($scope.pageNumber);
         };*/
        //Call Taxonomy
        /*$scope.taxonomy = {'children': []};
         $scope.getTaxonomy = function () {
         assetService.getTaxonomy($scope).then(function (data) {
         if (data.data == null || data.data == '') {
         $scope.taxonomy = {
         children: []
         };
         } else {
         $scope.taxonomy = data.data;
         }
         
         });
         };
         $scope.getTaxonomy();*/


        /*$scope.taxonomy_id = '0';
         //$scope.$flow.opts.query = {taxonomy_id: $scope.taxonomy_id, project_id: $scope.project_id, project_type_id: $scope.project_type_id};
         $scope.setTaxonmonyTitle = function (taxonomy_id, taxonomy_title) {
         $scope.taxonomy_title = '>' + taxonomy_title;
         $scope.taxonomy_id = taxonomy_id;
         $scope.getAssets(1);
         
         //$scope.$flow.opts.query = {taxonomy_id: taxonomy_id, project_id: $scope.project_id, project_type_id: $scope.project_type_id};
         }*/
        /*console.log($scope.$flow.onDrop);
         $scope.$on('flow::fileAdded', function(event, $flow, flowFile) {
         //console.log($scope.$flow);
         event.preventDefault();//prevent file from uploading
         });*/

        //Used to minimize and maximize to Taxonomy
        /*$scope.toggleMinimized = function (child) {
         child.minimized = !child.minimized;
         };
         //
         
         $scope.openTaxonomyModal = function (size) {
         $scope.getTaxonomy();
         var modalInstance = $modal.open({
         templateUrl: 'templates/asset/taxonomyModalContent.html',
         controller: 'ModalInstanceTaxonomyCtrl',
         size: size,
         resolve: {
         patternRec: function () {
         return '';
         },
         modalFormText: function () {
         return "You are trying to add a Taxonomy. Please rollover a taxonomy and click parent or child.";
         },
         modalFormTile: function () {
         return "Add Taxonomy";
         },
         taxonomy: function () {
         return $scope.taxonomy;
         },
         project_id: function () {
         return $scope.project_id;
         }
         }
         });
         modalInstance.result.then(function () {
         //Saving TOC    
         assetService.getPattern().then(function (data) {
         $scope.patternList = data.data;
         });
         $scope.getTaxonomy();
         }, function () {
         //$log.info('Modal dismissed at: ' + new Date());
         });
         };*/


        $scope.deleteAsset = function (id, project_id, newNumber) {
            SweetAlert.swal({
                title: "Are you sure?",
                text: "You will not be able to recover this asset!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    var viewMode = $scope.viewMode;
                    jQuery('#asset_' + id + viewMode).hide();
                    assetService.delGlobalAsset(id, project_id).then(function () {
                        $scope.getAssets(newNumber);
                    });
                    SweetAlert.swal("Success", "Asset has been deleted", "success");
                } else {
                    SweetAlert.swal("Cancelled", "Asset is safe :)", "error");
                }
            });
        };

        /*$scope.editbox = 0;
         $scope.viewbox = 0;
         $scope.filterbox = 0;
         $scope.editAsset = function (id) {
         $scope.editbox = 1;
         $scope.viewbox = 0;
         $scope.filterbox = 0;
         $('.right-panel-navpanel').show();
         $('.listing-view-content').removeClass('listing-view-content col-md-9').addClass('listing-view-content col-md-9');
         //$scope.project = {'project_permissions':data.project_permissions};            
         assetService.getAssetById(id).then(function (data) {
         //console.info(data);
         $scope.formData = {};
         var metadata = (data.data.metadata == "") ? '' : JSON.parse(data.data.metadata);
         $scope.formData.asset_org_name = data.data.original_name ? data.data.original_name : '';
         $scope.formData.asset_type = data.data.type ? data.data.type : '';
         if (metadata) {
         $scope.formData.asset_height = metadata.height ? metadata.height : '';
         $scope.formData.asset_width = metadata.width ? metadata.width : '';
         $scope.formData.asset_size = metadata.size ? metadata.size : '';
         }
         
         $scope.formData.asset_location = data.data.asset_location ? data.data.asset_location : '';
         $scope.formData.name = data.data.name ? data.data.name : '';
         $scope.formData.id = data.data.object_id ? data.data.object_id : '';
         });
         };*/

        /*$scope.viewAsset = function (id) {
         $scope.viewbox = 1;
         $scope.editbox = 0;
         $scope.filterbox = 0;
         $('.right-panel-navpanel').show();
         $('.listing-view-content').removeClass('listing-view-content col-md-12').addClass('listing-view-content col-md-9');
         assetService.getAssetById(id).then(function (data) {
         $scope.formData = {};
         var metadata = (data.data.metadata == "") ? '' : JSON.parse(data.data.metadata);
         $scope.formData.asset_org_name = data.data.original_name ? data.data.original_name : '';
         $scope.formData.asset_type = data.data.type ? data.data.type : '';
         $scope.formData.object_type_name = data.data.object_type_name ? data.data.object_type_name : '';
         if (metadata) {
         $scope.formData.asset_height = metadata.height ? metadata.height : '';
         $scope.formData.asset_width = metadata.width ? metadata.width : '';
         $scope.formData.asset_size = metadata.size ? metadata.size : '';
         }
         $scope.formData.asset_location = data.data.asset_location ? data.data.asset_location : '';
         $scope.formData.created_date = new Date(data.data.created_at) ? new Date(data.data.created_at) : '';
         $scope.formData.modified_date = new Date(data.data.updated_at).getTime() ? new Date(data.data.updated_at).getTime() : '';
         
         $scope.formData.name = data.data.name ? data.data.name : '';
         $scope.formData.id = data.data.id ? data.data.id : '';
         });
         };*/

        /*$scope.filterAsset = function () {
         $scope.viewbox = 0;
         $scope.editbox = 0;
         $scope.filterbox = 1;
         $('.right-panel-navpanel').show();
         $('.listing-view-content').removeClass('listing-view-content col-md-9').addClass('listing-view-content col-md-6');
         };*/

        /*$scope.cancel = function () {
         $('.right-panel-navpanel').hide();
         $('.listing-view-content').removeClass('listing-view-content col-md-9').addClass('listing-view-content col-md-12');
         };
         
         $scope.edit = function (id, asset_form) {
         $scope.formdata = {};
         $scope.formdata.name = asset_form.asset_org_name.$modelValue;
         $scope.formdata.type = asset_form.asset_type.$modelValue;
         $scope.formdata.height = asset_form.asset_height.$modelValue;
         $scope.formdata.width = asset_form.asset_width.$modelValue;
         $scope.formdata.size = asset_form.asset_size.$modelValue;
         //console.log(asset_form.asset_org_name.$modelValue);
         //console.log(asset_form);
         assetService.editAssetById(id, $scope).then(function (data) {
         $scope.getAssets();
         $scope.cancel();
         });
         };
         $scope.filterData = {};
         $scope.filter = function (formData) {
         assetService.filterAssets(formData).then(function (data) {
         $scope.assets = data;
         $scope.totalAssets = data.totalAssets;
         });
         }
         $scope.formData = {};
         $scope.reset = function () {
         $scope.formData = {};
         //console.info($scope.formData);
         }*/

        // Used to search asset
        /*$scope.assetSearch = function (search_text, pageNumber) {
         $scope.search_text = search_text;
         if ($scope.search_text) {
         $scope.pageNumber = pageNumber;
         $scope.itemsPerPage = 12;
         assetService.getSearchedAssets($scope).then(function (data) {
         $scope.assets = data.data;
         $scope.totalAssets = data.totalAssets;
         });
         } else {
         // If search field is empty                
         $scope.getAssets(1);
         }
         }*/

        $scope.getProject = function () {
            $scope.project_name = '';
            if ($scope.object_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    $scope.project_name = data.name;
                    $scope.project_type_id = data.project_type_id;
                    $scope.toc_creation_status = data.toc_creation_status;
                    $scope.project = {'project_permissions': data.project_permissions};
                    $scope.$flow.opts.query = {taxonomy_id: $scope.taxonomy_id, project_id: $scope.project_id, project_type_id: $scope.project_type_id, temp_guid: guid('')};
                });
            }
        }
        //$scope.getProject();

        /**
         * Open Asset Taxonomy Assignment ModalContainer
         */
        /*$scope.openAssignTaxonomyModal = function (size, asset_ids) {
         $scope.getTaxonomy();
         $scope.asset_ids = asset_ids;
         var modalInstance = $modal.open({
         templateUrl: 'templates/asset/assignTaxonomyModalContent.html',
         controller: 'ModalInstanceAssignTaxonomyCtrl',
         size: size,
         resolve: {
         modalFormText: function () {
         return "Choose a taxonomy and confirm";
         },
         modalFormTile: function () {
         return "Assign Taxonomy";
         },
         taxonomy: function () {
         return $scope.taxonomy;
         },
         asset_ids: function () {
         return $scope.asset_ids;
         }
         }
         });
         modalInstance.result.then(function () {
         $scope.getAssets();
         }, function () {
         //$log.info('Modal dismissed at: ' + new Date());
         });
         }*/
        /*
         * Used to reset taxonomy and fetch asset accordingly
         */
        /*$scope.resetTaxonomy = function () {
         $scope.taxonomy_title = '';
         $scope.taxonomy_id = '0';
         $scope.getAssets();
         }*/

        var dst = {};
        angular.extend(dst, $controller('asset_preview', {$scope: $scope}));

        $scope.selectedAll = false;
        $scope.checkAll = function () {
            if (!$scope.selectedAll) {
                $scope.selectedAll = true;
                $scope.showMakeGlobal = true;
            } else {
                $scope.selectedAll = false;
                $scope.showMakeGlobal = false
            }
            angular.forEach($scope.assets, function (item) {
                item.Selected = $scope.selectedAll;
            });
        };

        $scope.selectOption = function (id) {
            var all_check = true;
            var slected_items = 0;
            angular.forEach($scope.assets, function (item) {
                if (!item.Selected) {
                    all_check = false;
                    $scope.selectedAll = false;
                } else {
                    slected_items = 1;
                }
            });

            if (all_check) {
                $scope.selectedAll = true;
            }
            $scope.showMakeGlobal = slected_items;
        };

        $scope.apply_filter = function () {

            angular.element('.asset-filter-types.active').length;
            $scope.asset_filter.images = false;
            $scope.asset_filter.videos = false;
            $scope.asset_filter.audio = false;
            $scope.asset_filter.gadgets = false;
            $scope.asset_filter.other = false;
            angular.forEach(document.querySelectorAll('.multiselect li.active'), function (value, key) {
                value.innerText;
                if (value.innerText.toLowerCase() == 'all type') {
                    $scope.asset_filter.images = true;
                    $scope.asset_filter.videos = true;
                    $scope.asset_filter.audio = true;
                    $scope.asset_filter.gadgets = true;
                    $scope.asset_filter.other = true;
                }
                else if (value.innerText.toLowerCase() == 'image') {
                    $scope.asset_filter.images = true;
                }
                else if (value.innerText.toLowerCase() == 'audio') {
                    $scope.asset_filter.audio = true;
                }
                else if (value.innerText.toLowerCase() == 'video') {
                    $scope.asset_filter.videos = true;
                }
                else if (value.innerText.toLowerCase() == 'gadget') {
                    $scope.asset_filter.gadgets = true;
                }
                else if (value.innerText.toLowerCase() == 'other') {
                    $scope.asset_filter.other = true;
                }
            });

            $scope.getAssets($scope.currentPage);
            // if all filter are deselected
            // setTimeout(function () {
            //     if (angular.element('.asset-filter-types.active').length == 0) {
            //         $scope.asset_filter.images = true;
            //         $scope.asset_filter.videos = true;
            //         $scope.asset_filter.audio = true;
            //         $scope.asset_filter.gadgets = true;
            //     }

            // }, 200);
        };

        $scope.$on('apply-advaced-search', function (event, args) {
            $scope.getAssets($scope.currentPage);
        });

        $scope.$on('clear-advaced-search', function (event, args) {
            $scope.getAssets($scope.currentPage);
        });

        $scope.$on('apply-search', function (event, args) {
            $scope.getAssets($scope.currentPage);
        });

    }]);

app.controller('ModalInstanceContentCtrl', function ($scope, assetService, objectService, sessionService, $modalInstance, modalFormText, modalFormTile, object_id, $window, $sce) {
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.id = object_id;
    $scope.title = '';
    $scope.content = '';
    $scope.project_theme = '';
    var contentRec = {};
    assetService.getGlobalContentDetails($scope).then(function (data) {
        contentRec = data.data;
        if (contentRec.object.object_type_id == 101 || contentRec.object.object_type_id == 116 || contentRec.object.object_type_id == 117) {
            $scope.object_details = contentRec;
            $scope.id = contentRec.object.id;
            $scope.title = contentRec.object.name;
            $scope.content = contentRec.object.description;
            $scope.theme_location = contentRec.object.theme_location;
            $scope.file_path = contentRec.object.file_path;
            $scope.s3_file_path = contentRec.s3_file_path;
            $scope.file_path = $sce.trustAsResourceUrl($scope.file_path);
            $scope.getActualPath = contentRec.object.file_name;
            $scope.errorMessage = '';
            if(contentRec.object.total_question==0){
                $('#ModalInstanceContentCtrl form .inner-modal-content').html('<div style="height:400px;padding:15px;"><p class="alert alert-danger">This assessment does not have any question.</p></div>')
            }
            setTimeout(function () {
                setContentIntoIframe();
            }, 500);
        }
    });

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});

//Added by Chandan Choudhary on 02/01/2018
app.controller('LODataController', function ($scope, assetService, objectService, sessionService, $modalInstance, modalFormText, modalFormTile, content, $window, $sce, project_id) {
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.project_id = project_id;
    var indexValue = '';
    var parentIndex = '';
    var lo_data = {};
    var assessmentData = {};
    objectService.getObjectDetailsById(content.id).then(function (data) {
        setTimeout(function () {
            angular.element($('.lo_content_preview li.lo_title')[0]).triggerHandler('click');
        }, 500);
        var LO_Data = data.data;
        $scope.lo_toc = LO_Data.learning_object.lo_toc;
        $scope.id = LO_Data.id;
        $scope.LO_Data=data.data;
        $scope.lo_toc_index = [];
        $scope.lo_file_toc_index = [];
        $scope.disableback = false;
        $scope.disablenext = false;
        $scope.ts_hash = '?_ts=' + new Date().getTime();
        for (var i = 0; i < $scope.lo_toc.length; i++) {
            var file = $scope.lo_toc[i].file;
            if(file==''){
                var file=$scope.lo_toc[i].metadata.ext_res_url;
                 $scope.lo_toc_index.push(file);
            }
            else{
                $scope.lo_toc_index.push(file);
            }
        }
    });
    $scope.toggleMinimized = function() {
           if( $('.expand_icon i').hasClass('fa fa-sort-desc')){
               $('.expand_icon i').toggleClass('toggleMinimized');
           }
        };
    $scope.LOChildPreview = function (lo_child,url,node_type,event) {
        $scope.node_type = node_type;        
        $scope.url=url;
         if (node_type == 114 || node_type == 115 || url ) {
             $scope.s3_file_path='';
             var url = url;
             var a = $('.lo_title');
             a.each(function (key, item) {
             var linkUrl = $(item).attr('data-link');
             if ((typeof linkUrl) !== 'undefined' && String(url.toLowerCase()) === String(linkUrl.toLowerCase())) {
                $(".lo_title").removeClass("active");
                $(item).addClass("active");
                    }
                });
                $window.open(url, '_blank');           
                   
            }
        if (node_type) {
             if (node_type == 114 || node_type == 115){
                 $scope.file_name = url;
             }
              else if (node_type == 101 || node_type == 116 || node_type == 117){
                 $scope.file_name = lo_child;
             }
            indexValue = ($scope.lo_toc_index.indexOf($scope.file_name));
            for (var index = 0; index < $scope.lo_toc_index.length; ++index) {
                var value = $scope.lo_toc_index[index];
                if (value == null) {
                    index++;
                } else if (value.substring(0, $scope.file_name.length) === $scope.file_name) {
                    var checkValue = index;
                    break;
                }
            }
        }   
        
        if (checkValue) {
            indexValue = checkValue;
        }

        objectService.getObjectDetailsByFilename($scope, $scope.project_id, lo_child).then(function (data) {
            if(data.data!=null){
               lo_data = data.data;               
               var s3_file_path = data.data.s3_file_path;
                var a = $('.lo_title');
                var url = lo_child;
                a.each(function (key, item) {
                    var linkUrl = $(item).attr('data-link');
                    if ((typeof linkUrl) !== 'undefined' && String(url.toLowerCase()) === String(linkUrl.toLowerCase())) {
                        $(".lo_title").removeClass("active");
                        $(item).addClass("active");
                    }
                });

                //check for assessment with zero question//
                if(node_type == 117){
                    var id_value=$(".lo_content_preview li.active").attr('data-id');
                    assetService.getGlobalContentDetails_1($scope,id_value).then(function (data) {
                        if(data.data.object.total_question==0){   
                            $scope.s3_file_path=''; 
                        }else{
                            $scope.s3_file_path = $sce.trustAsResourceUrl( s3_file_path+ $scope.ts_hash);
                        }

                    })
                }else{
                    $scope.s3_file_path = $sce.trustAsResourceUrl( s3_file_path+ $scope.ts_hash);
                }

            }            
        });
        if (indexValue == 0) {
            $scope.disableback = true;
            jQuery("#backBtn").css({"opacity": '0.5', "pointer-events": "none"});
        } else {
            $scope.disableback = false;
            jQuery("#backBtn").css({"opacity": '1', "pointer-events": "all"});
        }
        if (indexValue == (($scope.lo_toc_index.length) - 1)) {
            $scope.disablenext = true;
            jQuery("#nextBtn").css({"opacity": '0.5', "pointer-events": "none"});
        } else {
            $scope.disablenext = false;
            jQuery("#nextBtn").css({"opacity": '1', "pointer-events": "all"});
        }
    }
    $scope.cancel = function (file_name,url,node_type) {
        $modalInstance.dismiss('cancel');
    };

    $scope.next = function () {
        if (jQuery(".lo_content_preview li.active").length > 0) {
            var file_name = jQuery(".lo_content_preview li.active").attr("data-link");
            indexValue = $scope.lo_toc_index.indexOf(file_name);
        }
        indexValue++;
        if($scope.lo_toc[indexValue].metadata.ext_res_url){
            $scope.LOChildPreview('',$scope.lo_toc[indexValue].metadata.ext_res_url);
        }
        else{
            $scope.LOChildPreview($scope.lo_toc_index[indexValue],'',$scope.lo_toc[indexValue].object_type_id);
        }        
    };
    $scope.prev = function () {
        if (jQuery(".lo_content_preview li.active").length > 0) {
            var file_name = jQuery(".lo_content_preview li.active").attr("data-link");
            indexValue = $scope.lo_toc_index.indexOf(file_name);
        }
        indexValue--;
        if($scope.lo_toc[indexValue].metadata.ext_res_url){
             $scope.LOChildPreview('',$scope.lo_toc[indexValue].metadata.ext_res_url);
        }
        else{
            $scope.LOChildPreview($scope.lo_toc_index[indexValue],'',$scope.lo_toc[indexValue].object_type_id);
        } 
    };

});
