/*****
 * Controller for Comopare version files
 * Author - Indranil Ghosh
 ***/

'use strict';

app.controller('compare', ['$scope', 'sessionService', 'compareService', '$location', '$routeParams', '$rootScope', '$modal', '$log', 'objectService', 'SweetAlert', '$window', '$filter', '$timeout', '$controller', '$interval', 'assetService', function ($scope, sessionService, compareService, $location, $routeParams, $rootScope, $modal, $log, objectService, SweetAlert, $window, $filter, $timeout, $controller, $interval, assetService) {
        $scope.access_token = sessionService.get('access_token');
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.user_id = userinfo.id;

        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
            return true;
        }
        $scope.project_id = $routeParams.project_id;
        $scope.object_id = $routeParams.object_id;
        $scope.object = {};

        $scope.getProject = function () {
            $scope.project_name = '';
            if ($scope.project_id != '') {
                objectService.getProjectDetails($scope).then(function (data) {
                    $scope.project_name = data.name;
                    $scope.toc_creation_status = data.toc_creation_status;
                    $scope.project = {};
                    $scope.project.project_permissions = data.project_permissions;
                });
            }
        }
        $scope.getProject();

        objectService.getObjectDetailsById($scope.object_id).then(function (data) {
            $scope.object.title = data.data.name;
        });
        $scope.pid = $scope.project_id;
        $scope.oid = $scope.object_id;
        objectService.get_version_list($scope).then(function (data) {
            $scope.versionList = data.data;
        });
        $scope.file1 = {};
        $scope.file2 = {};
        $scope.compareFiles = function (file1, file2) {
            $scope.file1 = file1;
            $scope.file2 = file2;
            compareService.getFileCompare($scope).then(function (data) {
                var d = dmp.diff_main(data.file1_content, data.file2_content);
                dmp.diff_cleanupSemantic(d);
                var ds = dmp.diff_prettyHtml(d);

                //$scope.compare_result = data.data;                
                //console.info(data.data);
                
                /*
                var htmlContent = data.data.replace(/..\/images\/default-img.png/g, 'images/default-img.png');
                htmlContent = htmlContent.replace(/..\/images\/default-video.jpg/g, 'images/default-video.jpg');
                jQuery('iframe#iframeId').contents().find('#outputDiv').html(htmlContent);
                */
                
            });
        };
    }]);