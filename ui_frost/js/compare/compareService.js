/*****
 * Services for TOC
 * Author - Koushik Samanta
 *  
 */
'use strict';

//Start of aclService
var returnVal = '';
var returnTree = '';
var node_id = '';
var project_id = '';
app.service('compareService', ['$http', '$q', 'sessionService', '$location', '$rootScope', function ($http, $q, sessionService, $location, $rootScope) {

        this.getFileCompare = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/compare-files?access_token=' + sessionService.get('access_token'), {
                file1_versionid: $scope.file1 ? $scope.file1.version_id : '',
                file2_versionid: $scope.file2 ? $scope.file2.version_id : '',
                project_id: $scope.project_id ? $scope.project_id : 0,
                object_id: $scope.object_id ? $scope.object_id : ''
            }).success(function (data, status, headers, config) {                
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        };


    }]);