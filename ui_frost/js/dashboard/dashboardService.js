/*****
 * Services for Dashboard
 * Author - Arikh Akher 
 *  
 */
'use strict';

//Start of dashboardServices
app.service('dashboardService', ['projectService', '$http', '$q', 'sessionService', function (projectService, $http, $q, sessionService) {

        this.dashboardObjects = function ($scope) {

            var projectDataPromise = projectService.getProjects();
            projectDataPromise.then(function (data) {

                return $scope.responsedata = data;
                //console.log($scope.responsedata);

            });
            //console.log($scope.responsedata);
        };

        this.makeFavorite = function ($scope) {
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined($scope.pid)) {
                queryString += "&project_id=" + $scope.pid;
            }
            if (angular.isDefined($scope.uid)) {
                queryString += "&user_id=" + $scope.uid;
            }
            if (angular.isDefined($scope.favoriteAction)) {
                queryString += "&favorite_action=" + $scope.favoriteAction;
            }
            $http.get(serviceEndPoint + 'api/v1/project-favorite?access_token=' + sessionService.get('access_token') + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the project list             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });

            return deferred.promise;
        };
    }]);