'use strict';

app.controller('dashboard', ['$scope', '$location', 'sessionService', 'objectService', 'cfpLoadingBar', '$rootScope', '$interval', '$http', '$window', 'SweetAlert', 'dashboardService', function ($scope, $location, sessionService, objectService, cfpLoadingBar, $rootScope, $interval, $http, $window, SweetAlert, dashboardService) {
        $rootScope.chkAccessTokenInt = 1000;
        $scope.access_token = sessionService.get('access_token');
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.loggedid_user = userinfo.user_id;
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        $scope.searchModel = {};
        $scope.searchModel.search_text = '';
		

        /*
         * Change View
         */
        $scope.viewMode = sessionService.get('viewMode') == null ? 1 : sessionService.get('viewMode');
        $scope.changeView = function (mode) {
            $scope.viewMode = mode;
            sessionService.set('viewMode', mode);
        };


        $scope.object_types = {};

        if (!$scope.access_token || userinfo == null) {
            //If login session is expired or null
            $location.path('logout');
            return true;
        } else {

            //Get Object Types
            $scope.projectlist = {};

            //Call object types for passing object-type = PROJECT for project listing 
            /*$scope.pagination = {
             current: 1
             };*/
            $scope.pageNumber = 1;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 12;
    
            $scope.listProjects = function (pageNumber, search_text) {
                $scope.ts_hash = '?_ts=' + new Date().getTime(); // for cover image refresh
                $scope.search_text = search_text;                
                $scope.pageNumber = pageNumber;
                $scope.currentPage = pageNumber;
                $scope.itemsPerPage = 12;
                objectService.list($scope).then(function (data) {
                    $scope.projectlist = data;
                    $scope.totalProjects = data.totalProjects;
                    // update project tickets and ticket comments count
                    $scope.project_ids = [];
                    angular.forEach($scope.projectlist.projects, function (value, key) {
                        value.total_tickets = '';
                        value.total_ticket_comments = '';
                        $scope.project_ids.push(value.project_id);
                    });
                });
            };

            //Call when page load
            $scope.listProjects(1);

            //For Pagination Of Dashboard
            $scope.pageChangeHandlerDashboard = function (newPage) {
                $scope.listProjects(newPage, $scope.searchModel.search_text);
            };
			
			$scope.redirect_taxonomy= function(screen,id){
				/*if (screen == 'taxo'){
					$location.path('manage-taxonomy/'+id);
				}
				else{
					$location.path('project-import/'+id);
                }*/
                $location.path('taxonomy-authoring/'+id);
					
			}
				

            $scope.createProject = function () {
                $location.path('createproject');
            };

            $scope.deleteProject = function (project_id, curr) {
                SweetAlert.swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this project!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function (isConfirm) {
                    if (isConfirm) {
                        $http({
                            method: 'DELETE',
                            url: serviceEndPoint + 'api/v1/project/delete/' + project_id,
                            headers: {
                                'Authorization': sessionService.get('access_token'),
                                'Accept': 'application/json',
                                'Content-Type': 'application/x-www-form-urlencoded'
                            }
                        }).success(function (data, status, headers, config) {
                            $rootScope.token_expiry = data;
                            if (data.status == 200) {
                                if (curr != 'undefined') {
                                    $scope.listProjects(curr);
                                } else {
                                    $scope.listProjects(1);
                                }
                                
                                SweetAlert.swal("Deleted!", "Project has been deleted.", "success");
                            } else {
                                SweetAlert.swal("Cancelled", "Some error occurred)", "error");
                            }
                        }).error(function (data, status, headers, config) {
                            SweetAlert.swal("Cancelled", "Some error occurred, request not submitted", "error");
                        });
                    } else {
                        SweetAlert.swal("Cancelled", "Project is safe :)", "error");
                    }
                });
            }

        }

    }]);

