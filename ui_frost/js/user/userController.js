/*****
 * Controller for HEADER
 * Author - Indranil Ghosh * 
 */

'use strict';

app.controller('user', ['$scope', 'sessionService', '$location', '$routeParams', '$rootScope', '$log', 'SweetAlert', '$modal', 'userService', 'aclService', 'loginService', function ($scope, sessionService, $location, $routeParams, $rootScope, $log, SweetAlert, $modal, userService, aclService, loginService) {

        $scope.access_token = sessionService.get('access_token');
        var userinfo = JSON.parse(sessionService.get('userinfo'));
        $scope.loggedinuser_role = userinfo.roles[0].role_id;
        console.log(userinfo.roles[0].role_id);
        $scope.loggedinUserid = userinfo.id;
        $scope.user_permissions = JSON.parse(sessionService.get('user_permissions'));
        $scope.formData = {};

        if (!$scope.access_token && !$scope.user_id) {
            $location.path('logout');
            return true;
        }

        var page = $location.path();
        page = page.slice(1, page.length);

        if (page == 'settings') {
            $scope.userList = [];
            userService.getUsers($scope).then(function (data) {
                $scope.userList = data.data;
            });
        }

        //  aclService.roles().then(function(data) {
        //  $scope.roles = data.data;
         
        //  // Select Default Role as user
        //  $scope.role_id = $scope.roles[1];
         
        //  });

        $scope.is_admin = false;
        $scope.submitted = false;
        $scope.createNewUser = function (size) {
            $scope.message = '';
            var modalInstance = $modal.open({
                templateUrl: 'templates/user/create_user_modal.html',
                controller: 'ModalUserAddCtrl',
                size: size,
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    modalFormText: function () {
                        return "You are trying to add a new User";
                    },
                    modalFormTile: function () {
                        return "Create New User";
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.userList = [];
                userService.getUsers($scope).then(function (data) {
                    $scope.userList = data.data;
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.editUser = function (user_id, size) {
            $scope.message = '';
            var modalInstance = $modal.open({
                templateUrl: 'templates/user/edit_user_modal.html',
                controller: 'ModalUserEditCtrl',
                size: size,
                scope: $scope,
                backdrop: 'static',
                keyboard: false,
                resolve: {
                    formData: function () {
                        return {first_name: '', last_name: '', username: '', email: '', is_admin: false};
                    },
                    user_id: function () {
                        return user_id;
                    },
                    modalFormText: function () {
                        return "You are trying to Edit a User";
                    },
                    modalFormTile: function () {
                        return "Edit User";
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.userList = [];
                userService.getUsers($scope).then(function (data) {
                    $scope.userList = data.data;
                });
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        $scope.gotToUserAclPage = function (user_id) {
            $location.path('/allocate_acl_user/' + user_id);
        };
        $scope.showMsg = 0;
        $scope.deleteUser = function (user_id) {
            $scope.message = '';
            $scope.user_id = user_id;
            SweetAlert.swal({
                title: "Are you sure?",
                text: "This user will be deactivated from the system!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, deactivate it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    userService.delUsers($scope).then(function (data) {
                        console.log(data);
                        //System Permission Disallow
                        //Do logout from the system
                        if (angular.isDefined(data.systemPermissionGranted) && data.systemPermissionGranted == '401') {
                            loginService.logout();
                        } else {
                            $scope.userList = [];
                            $scope.message = data.message;
                            $scope.status = data.status;
                            userService.getUsers($scope).then(function (data) {
                                $scope.userList = data.data;
                            });
                        }

                    });
                    //SweetAlert.swal("Success", "User has been deleted", "success");
                } else {
                    SweetAlert.swal("Cancelled", "User is safe :)", "error");
                }
            });
        };

        $scope.activateUser = function (user_id) {
            $scope.message = '';
            $scope.user_id = user_id;
            SweetAlert.swal({
                title: "Are you sure?",
                text: "Do you want to activate this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, activate it!",
                cancelButtonText: "No, cancel!",
                closeOnConfirm: true,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    userService.activateUser($scope).then(function (data) {
                        console.info(data);
                        if (angular.isDefined(data.systemPermissionGranted) && data.systemPermissionGranted == '401') {
                            loginService.logout();
                        } else {
                            $scope.userList = [];
                            $scope.message = data.message;
                            $scope.status = data.status;
                            userService.getUsers($scope).then(function (data) {
                                $scope.userList = data.data;
                            });
                        }

                    });
                    //SweetAlert.swal("Success", "User has been acivate", "success");
                } else {
                    SweetAlert.swal("Cancelled", "User remains inactive :)", "error");
                }
            });
        };

    }]);


app.controller('ModalUserAddCtrl', function ($scope, $modalInstance, modalFormText, modalFormTile, userService, loginService) {
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;

    $scope.createUser = function (formData) {
        $scope.submitted = true;
        userService.createUser($scope, formData).then(function (data) {
            if (data.status == '200') {
                $modalInstance.close();
            } else if (data.status == '400') {
                $scope.formData.error = data.error;
            }

            //System Permission Disallow
            //Do logout from the system
            if (angular.isDefined(data.systemPermissionGranted) && data.systemPermissionGranted == '401') {
                $modalInstance.close();
                loginService.logout();
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});

app.controller('ModalUserEditCtrl', function ($scope, $modalInstance, modalFormText, modalFormTile, userService, user_id, formData, loginService) {
    $scope.modalFormText = modalFormText;
    $scope.modalFormTile = modalFormTile;
    $scope.user_id = user_id;
    $scope.formData = formData;
    
    $scope.loading = true;
    userService.getUserDetail($scope).then(function (userDetail) {
        $scope.formData = userDetail.data;
        $scope.loading = false;
        $scope.formData.is_admin = false;
        if ($scope.formData.user_roles[0].id == '2') {
            $scope.formData.is_admin = true;
        }
    });

    $scope.updateUser = function (formData) {
        $scope.submitted = true;
        userService.updateUser($scope, formData).then(function (data) {
            if (data.status == '200') {
                $modalInstance.close();
            } else if (data.status == '400') {
                $scope.formData.error = data.error;
            }

            //System Permission Disallow
            //Do logout from the system
            if (angular.isDefined(data.systemPermissionGranted) && data.systemPermissionGranted == '401') {
                $modalInstance.close();
                loginService.logout();
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
});