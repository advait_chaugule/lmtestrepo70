/*****
 * Services for Login
 * Author - Arikh Akher
 */

'use strict';

//Start of loginService
app.service('userService', ['$http', '$q', '$location', 'tokenService', 'sessionService', '$rootScope', function ($http, $q, $location, tokenService, sessionService, $rootScope) {

        this.getUsers = function ($scope) {
            var deferred = $q.defer();
            $http.get(serviceEndPoint + 'api/v1/users?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        //Create User 
        this.createUser = function ($scope, formdata) {
            var deferred = $q.defer();
            var params = {};
            $http.post(serviceEndPoint + 'api/v1/users?access_token=' + sessionService.get('access_token'), {
                email: $scope.email,
                username: $scope.username,
                //role_id: $scope.role_id.id,
                first_name: $scope.first_name,
                last_name: $scope.last_name,
                password: $scope.password,
                password_confirmation: $scope.password_confirmation,
                is_admin: $scope.is_admin
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        
        //Edit User 
        this.updateUser = function ($scope, formdata) {
            var deferred = $q.defer();
            var params = {};
            $http.post(serviceEndPoint + 'api/v1/users/user_id/'+$scope.user_id+'?access_token=' + sessionService.get('access_token'), {
                email: formdata.email,
                user_id: $scope.user_id,
                username: formdata.username,
                //role_id: $scope.role_id.id,
                first_name: formdata.first_name,
                last_name: formdata.last_name,               
                is_admin: formdata.is_admin,
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.getUserDetail = function ($scope) {
            var deferred = $q.defer();
            $http.get(serviceEndPoint + 'api/v1/users/user_id/'+$scope.user_id+'?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.delUsers = function ($scope) {
            var deferred = $q.defer();
            $http.delete(serviceEndPoint + 'api/v1/users/user_id/' + $scope.user_id + '?access_token=' + sessionService.get('access_token'), {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };
        
        this.activateUser = function ($scope) {
            var deferred = $q.defer();
            $http.post(serviceEndPoint + 'api/v1/users-activate/user_id/' + $scope.user_id + '?access_token=' + sessionService.get('access_token'), {
                _method: 'PUT'
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

    }]);
