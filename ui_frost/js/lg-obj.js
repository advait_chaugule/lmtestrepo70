/*
Responsive table script

Credit to http://css-tricks.com/responsive-data-tables/
*/

!function($){
    var className = 'lc_responsivetable',
        maxWindowWidth = 700,
        bodyElement = document.body,
        windowWidth = window.innerWidth,
        windowHeight = window.innerHeight,
        largeTables = document.getElementsByTagName('table'),
        largeImages = document.getElementsByClassName('ls_large-image'),
        svgEquations = document.getElementsByTagName("svg"),
        equations = document.getElementsByTagName('math'), // or m:math??
        scalable = 1,
        smallDevice,
        supportsTouch;

    if(window.innerWidth > maxWindowWidth){
        smallDevice = false;
    }else{
        smallDevice = true;
    }


    //Check if it's touch device
    function isTouchDevice() { 
        supportsTouch = ('ontouchstart' in window) || !!(navigator.msMaxTouchPoints);
        console.log('supportsTouch: ' + supportsTouch);
        return supportsTouch;
    }

    function zoomIn(event, target){
        scalable = scalable + 0.2
        var imageId = target.getAttribute('data-target')
        var targetImage = document.getElementById(imageId)
        targetImage.style.transform = "scale(" + scalable +"," + scalable + ")"
        targetImage.style.transformOrigin = "0 0"
        targetImage.style.webkitTransform = "scale(" + scalable +"," + scalable + ")"
        targetImage.style.webkitTransformOrigin = "0 0"
    
    }
    
    // Wrap an HTMLElement around each element in an HTMLElement array.
    HTMLElement.prototype.wrap = function(elms) {
        // Convert `elms` to an array, if necessary.
        if (!elms.length) elms = [elms];
        
        // Loops backwards to prevent having to clone the wrapper on the
        // first element (see `child` below).
        for (var i = elms.length - 1; i >= 0; i--) {
            var child = (i > 0) ? this.cloneNode(true) : this;
            var el    = elms[i];
            
            // Cache the current parent and sibling.
            var parent  = el.parentNode;
            var sibling = el.nextSibling;
            
            // Wrap the element (is automatically removed from its current
            // parent).
            child.appendChild(el);
            
            // If the element had a sibling, insert the wrapper before
            // the sibling to maintain the HTML structure; otherwise, just
            // append it to the parent.
            if (sibling) {
                parent.insertBefore(child, sibling);
            } else {
                parent.appendChild(child);
            }
        }
    }

    function zoomOut(event, target){
         scalable = scalable - 0.2
         if (scalable > 0.2) {
            var imageId = target.getAttribute('data-target')
            var targetImage = document.getElementById(imageId)
            targetImage.style.transform = "scale(" + scalable +"," + scalable + ")"
            targetImage.style.transformOrigin = "0 0"
            targetImage.style.webkitTransform = "scale(" + scalable +"," + scalable + ")"
            targetImage.style.webkitTransformOrigin = "0 0"    
        }
    }

    function zoomReset(event, target){
        scalable = 1
        var imageId = target.getAttribute('data-target')
        var targetImage = document.getElementById(imageId)
        targetImage.style.transform = "scale(" + scalable +"," + scalable + ")"
        targetImage.style.transformOrigin = "0 0"
        targetImage.style.webkitTransform = "scale(" + scalable +"," + scalable + ")"
        targetImage.style.webkitTransformOrigin = "0 0"
    
    }
    
    function scaleEquation(equation, width, parentW){
        // if this fires, the equation needs scaling
        var scaleRatio = parentW / width,
            height = equation.offsetHeight * scaleRatio

        equation.style.webkitTransform = "scale(" + scaleRatio +"," + scaleRatio + ")"
        equation.style.webkitTransformOrigin = "0 0"
        equation.style.mozTransform = "scale(" + scaleRatio +"," + scaleRatio + ")"
        equation.style.mozTransformOrigin = "0 0"
        equation.style.transform = "scale(" + scaleRatio +"," + scaleRatio + ")"
        equation.style.transformOrigin = "0 0"
        equation.style.width = width + "px"
        equation.style.maxWidth = width + "px"
        //equation.parentNode.style.height = height + "px"
    }
    
    function loader(){
        
        // bind the click events for the tables
        document.addEventListener("click", function(e){
            var targetClasses = e.target.className,
                target

            // if it's fa, then bubble to parent
            if(targetClasses.indexOf("fa") != -1){
                targetClasses = e.target.parentElement.className
                target = e.target.parentElement
            } else {
                target = e.target
            }

            if(targetClasses.indexOf("zoom") != -1){
                targetClasses = targetClasses.replace("zoom-btn ", "")
                switch(targetClasses) {
                    case "zoom-in":
                        zoomIn(e, target)
                        break
                    case "zoom-out":
                        zoomOut(e, target)
                        break
                    case "zoom-reset":
                        zoomReset(e, target)
                        break
                }
            }
        }, false)

        var selectedTable, otherEls, scaleRatio
        
        if (supportsTouch) {
             window.addEventListener("orientationchange", function(){
               //alert("orientation change ")
                if(largeTables.length > 0){
                    for (var i = 0; i < largeTables.length; i++) {
                        selectedTable = largeTables[i]
                        //otherEls = document.querySelector('body > section figure'),
                        if(selectedTable.id != "highlightPopupContent"){
                            var nestedImgs = selectedTable.getElementsByTagName('img')
                            for (var j = 0; j < nestedImgs.length; j++) {
                                var nestImage = nestedImgs[j]
                                nestImage.style.maxWidth = "none"
                            }
                            
                            var tempHeight = selectedTable.getBoundingClientRect().height
                            
                            console.log(tempHeight)
                            var div = document.createElement('div')
                            div.setAttribute("class", "lc_tablewrapper")
                            div.wrap(selectedTable)
                            otherEls = ancestorTag(selectedTable)
                           
                            console.log(otherEls.offsetWidth)
                            var ancestorPadding = window.getComputedStyle(otherEls, null).getPropertyValue("padding-left").replace('px','')*2
                            var ancestorWidth = otherEls.offsetWidth - ((window.getComputedStyle(otherEls, null).getPropertyValue("padding-left").replace('px',''))*2)
                            console.log(otherEls.scrollHeight)
                            console.log(otherEls.scrollHeight)
                            console.log(otherEls.offsetHeight)
                            console.log(otherEls.getBoundingClientRect().height)

                            console.log(selectedTable.clientWidth)
                            console.log(selectedTable.scrollWidth)
                            console.log(selectedTable.offsetWidth)
                            scaleRatio = ancestorWidth / (selectedTable.offsetWidth + ancestorPadding )
                            div.setAttribute("style", "width:" + ancestorWidth + "px; height: " + tempHeight*scaleRatio  + "px;  ")
                            
                            console.log(ancestorWidth)
                            console.log(otherEls.offsetWidth)
                            
                            
                            //console.log(selectedTable.currentStyle.width)

                            var adjustWidth = Math.max(selectedTable.clientWidth, selectedTable.scrollWidth, selectedTable.offsetWidth)
                            if (adjustWidth != selectedTable.offsetWidth) {
                                
                                scaleRatio = ancestorWidth / (adjustWidth + ancestorPadding )
                                div.setAttribute("style", "height: " + tempHeight*scaleRatio  + "px;  ")

                            }
                            
                            selectedTable.style.transform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                            selectedTable.style.transformOrigin = "0 0"
                            selectedTable.style.webkitTransform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                            selectedTable.style.webkitTransformOrigin = "0 0"
                            // if (scaleRatio < 1) {
                            //     selectedTable.style.transform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                            //     selectedTable.style.transformOrigin = "0 0"
                            //     selectedTable.style.webkitTransform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                            //     selectedTable.style.webkitTransformOrigin = "0 0"
                            // }else if (otherEls.offsetWidth == 320) {
                            //     scaleRatio =  selectedTable.offsetWidth / selectedTable.scrollWidth
                            //     selectedTable.style.transform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                            //     selectedTable.style.transformOrigin = "0 0"
                            //     selectedTable.style.webkitTransform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                            //     selectedTable.style.webkitTransformOrigin = "0 0"
                            // }
                            

                            
                        }
                    }
                }
                
                
                if(equations.length > 0 || svgEquations.length > 0){
                    var eqs = []
                    if(equations.length > 0){
                        for(var key in equations){
                            eqs.push(equations[key])
                        }
                    }
                    if(svgEquations.length > 0){
                        for(var i = 0; i < svgEquations.length; i++){
                            eqs.push(svgEquations[i])
                        }
                        // for(var key in svgEquations){
                        //     eqs.push(svgEquations[key])
                        // }
                    }
                    console.log(equations.length)
                    console.log(svgEquations.length)
                    console.log(eqs.length)

                    //set up the equations
                    for(var i = 0; i < eqs.length; i++){

                        var equation = eqs[i],
                            width
                        
                        //console.log(equation.parentElement[0])
                        var parentW = equation.parentElement.offsetWidth
                            
                            if (equation.childNodes[0].length == 0) {
                                width = equation.offsetWidth
                            }else {
                                width = equation.childNodes[0].offsetWidth
                               
                            }
                        
                        if(equation.parentNode.className.indexOf("inlineequation") === -1 && equation.style.display != "inline"){
                            // wrap it in a div for scaling purposes
                            var div = document.createElement('div')
                            div.wrap(equation)
                            div.setAttribute("style", "width: "+parentW+"px; overflow: visible;")
                            div.className = "lc_equationwrapper"

                            if(width > parentW){
                                // scale if it's bigger
                                scaleEquation(div, width, parentW)
                            }
                        }
                    }
                    
                }
                
                if(window.MathJax != undefined){
                    MathJax.Hub.Queue(function(){
                       var Equations = document.getElementsByClassName("MathJax_Display")
                        for(var i = 0; i < Equations.length; i++){
                            var equation = Equations[i]

                            if(equation.parentNode.className.indexOf("lc_equationwrapper") == -1 && equation.style.display != "inline"){
                                // oops, it's not wrapped for some reason... wrap it up, then continue
                                var div = document.createElement('div')
                                div.setAttribute("style", "width: "+equation.parentNode.offsetWidth+"px; overflow: visible;")
                                div.className = "lc_equationwrapper"
                                var newHTML = equation.parentNode.innerHTML,
                                    parent = equation.parentNode
                                div.innerHTML = newHTML
                                parent.innerHTML = ""
                                parent.appendChild(div)
                                equation = div.childNodes[2]
                            }
                            
                            var width = equation.childNodes[0].offsetWidth,
                                parentW = equation.parentNode.offsetWidth
                            if(width > parentW){
                                scaleEquation(equation.parentNode, width, parentW)
                            }
                        }
                    });
                }
            });
        } else if(!supportsTouch){
            var css = '.lc_imagewrapper {width:100%; overflow: auto; padding: 0 0 0 32px;} \
                       .zoom-buttons { position:absolute; left: 0; width: 25px; z-index:5; } \
                       .zoom-btn { -webkit-box-shadow: 0px 1px 3px rgba(0,0,0,0.4); box-shadow: 0px 1px 3px rgba(0,0,0,0.4);} \
                       .zoom-in, .zoom-in:hover, .zoom-out, .zoom-out:hover {display:block; font-size:18px; font-weight:bold; background:#fff; border:1px solid #000; color: #000; padding: 2px; line-height: 100%; width: 25px; border-radius: 0; -webkit-border-radius: 0;} \
                       .zoom-in, .zoom-in:hover {border-bottom: 0} \
                       .zoom-reset, .zoom-reset:hover {border:none; font-size: 12px; background: transparent; padding: 0; box-shadow: none; color: #08c; font-weight: normal; } ',
            head = document.head || document.getElementsByTagName('head')[0],
            style = document.createElement('style');
            style.type = 'text/css';
            if (style.styleSheet){
              style.styleSheet.cssText = css;
            } else {
              style.appendChild(document.createTextNode(css));
            }
            head.appendChild(style);

            for (var i = 0; i < largeImages.length; i++) {
                var selectedImage = largeImages[i]
                var randomId = Math.random().toString(36).substr(2);
                selectedImage.setAttribute("id", randomId);
                selectedImage.parentElement.setAttribute("style", "position: relative;")
                var div = document.createElement('div')
                div.setAttribute("class", "lc_imagewrapper")
                div.wrap(selectedImage)
                var div_control = [ 
                    '<div class="zoom-buttons">',
                        '<button data-target="' + randomId + '" class="zoom-btn zoom-in">+</button>',
                        '<button data-target="' + randomId + '" class="zoom-btn zoom-out">-</button>',
                        '<button class="zoom-btn zoom-reset" data-target="' + randomId + '" >Reset</button>',
                    '</div>'
                ].join('\n')
                
                div.insertAdjacentHTML('afterBegin', div_control)
            }         
        }
        
        if(largeTables.length > 0){
            for (var i = 0; i < largeTables.length; i++) {
                selectedTable = largeTables[i]
                //otherEls = document.querySelector('body > section figure'),
                if(selectedTable.id != "highlightPopupContent"){
                    var nestedImgs = selectedTable.getElementsByTagName('img')
                    for (var j = 0; j < nestedImgs.length; j++) {
                        var nestImage = nestedImgs[j]
                        nestImage.style.maxWidth = "none"
                    }
                    
                    var tempHeight = selectedTable.getBoundingClientRect().height
                    
                    console.log(tempHeight)
                    var div = document.createElement('div')
                    div.setAttribute("class", "lc_tablewrapper")
                    div.wrap(selectedTable)
                    otherEls = ancestorTag(selectedTable)
                   
                    console.log(otherEls.offsetWidth)
                    var ancestorPadding = window.getComputedStyle(otherEls, null).getPropertyValue("padding-left").replace('px','')*2
                    var ancestorWidth = otherEls.offsetWidth - ((window.getComputedStyle(otherEls, null).getPropertyValue("padding-left").replace('px',''))*2)
                    console.log(otherEls.scrollHeight)
                    console.log(otherEls.scrollHeight)
                    console.log(otherEls.offsetHeight)
                    console.log(otherEls.getBoundingClientRect().height)

                    console.log(selectedTable.clientWidth)
                    console.log(selectedTable.scrollWidth)
                    console.log(selectedTable.offsetWidth)
                    scaleRatio = ancestorWidth / (selectedTable.offsetWidth + ancestorPadding )
                    div.setAttribute("style", "width:" + ancestorWidth + "px; height: " + tempHeight*scaleRatio  + "px;  ")
                    
                    console.log(ancestorWidth)
                    console.log(otherEls.offsetWidth)
                    
                    
                    //console.log(selectedTable.currentStyle.width)

                    var adjustWidth = Math.max(selectedTable.clientWidth, selectedTable.scrollWidth, selectedTable.offsetWidth)
                    if (adjustWidth != selectedTable.offsetWidth) {
                        
                        scaleRatio = ancestorWidth / (adjustWidth + ancestorPadding )
                        div.setAttribute("style", "height: " + tempHeight*scaleRatio  + "px;  ")

                    }
                    
                    selectedTable.style.transform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                    selectedTable.style.transformOrigin = "0 0"
                    selectedTable.style.webkitTransform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                    selectedTable.style.webkitTransformOrigin = "0 0"
                    // if (scaleRatio < 1) {
                    //     selectedTable.style.transform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                    //     selectedTable.style.transformOrigin = "0 0"
                    //     selectedTable.style.webkitTransform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                    //     selectedTable.style.webkitTransformOrigin = "0 0"
                    // }else if (otherEls.offsetWidth == 320) {
                    //     scaleRatio =  selectedTable.offsetWidth / selectedTable.scrollWidth
                    //     selectedTable.style.transform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                    //     selectedTable.style.transformOrigin = "0 0"
                    //     selectedTable.style.webkitTransform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                    //     selectedTable.style.webkitTransformOrigin = "0 0"
                    // }
                    

                    
                }
            }
        }
        
        
        if(equations.length > 0 || svgEquations.length > 0){
            var eqs = []
            if(equations.length > 0){
                for(var key in equations){
                    eqs.push(equations[key])
                }
            }
            if(svgEquations.length > 0){
                for(var i = 0; i < svgEquations.length; i++){
                    eqs.push(svgEquations[i])
                }
                // for(var key in svgEquations){
                //     eqs.push(svgEquations[key])
                // }
            }
            console.log(equations.length)
            console.log(svgEquations.length)
            console.log(eqs.length)

            //set up the equations
            for(var i = 0; i < eqs.length; i++){

                var equation = eqs[i],
                    width
                
                //console.log(equation.parentElement[0])
                var parentW = equation.parentElement.offsetWidth
                    
                    if (equation.childNodes[0].length == 0) {
                        width = equation.offsetWidth
                    }else {
                        width = equation.childNodes[0].offsetWidth
                       
                    }
                
                if(equation.parentNode.className.indexOf("inlineequation") === -1 && equation.style.display != "inline"){
                    // wrap it in a div for scaling purposes
                    var div = document.createElement('div')
                    div.wrap(equation)
                    div.setAttribute("style", "width: "+parentW+"px; overflow: visible;")
                    div.className = "lc_equationwrapper"

                    if(width > parentW){
                        // scale if it's bigger
                        scaleEquation(div, width, parentW)
                    }
                }
            }
            
        }
        
        if(window.MathJax != undefined){
            MathJax.Hub.Queue(function(){
               var Equations = document.getElementsByClassName("MathJax_Display")
                for(var i = 0; i < Equations.length; i++){
                    var equation = Equations[i]

                    if(equation.parentNode.className.indexOf("lc_equationwrapper") == -1 && equation.style.display != "inline"){
                        // oops, it's not wrapped for some reason... wrap it up, then continue
                        var div = document.createElement('div')
                        div.setAttribute("style", "width: "+equation.parentNode.offsetWidth+"px; overflow: visible;")
                        div.className = "lc_equationwrapper"
                        var newHTML = equation.parentNode.innerHTML,
                            parent = equation.parentNode
                        div.innerHTML = newHTML
                        parent.innerHTML = ""
                        parent.appendChild(div)
                        equation = div.childNodes[2]
                    }
                    
                    var width = equation.childNodes[0].offsetWidth,
                        parentW = equation.parentNode.offsetWidth
                    if(width > parentW){
                        scaleEquation(equation.parentNode, width, parentW)
                    }
                }
            });
        }
        
    }
    
    window.addEventListener("load", isTouchDevice(), false);
    
    window.addEventListener("load", loader(), false);
   
    window.addEventListener("resize", resizeThrottler, false);
    
    
    

    function addIdToTable(){
        for (var i = 0; i < largeTables.length; i++) {
            var selectedTable = largeTables[i];
            var randomId = Math.random().toString(36).substr(2);
            selectedTable.setAttribute("id", randomId);
            var parentFigureEl = ancestorTag(selectedTable, 'figure') != null ? ancestorTag(selectedTable, 'figure'): ancestorTag(selectedTable, 'section')
            parentFigureEl.classList.add(selectedTable.id);
            console.log(selectedTable.id)
            
        }
    }

    function makeTableFit(){
        var selectedTable, otherEls, scaleRatio
        for (var i = 0; i < largeTables.length; i++) {
            selectedTable = largeTables[i]
            if(selectedTable.id != "highlightPopupContent"){
                //otherEls = document.querySelector('body > section figure'),
                otherEls = ancestorTag(selectedTable)
                var ancestorWidth = otherEls.offsetWidth - ((window.getComputedStyle(otherEls, null).getPropertyValue("padding-left"))*2)
                scaleRatio = ancestorWidth / selectedTable.offsetWidth
                console.log(ancestorWidth)
                console.log(selectedTable.offsetWidth)
                // scaleRatio = otherEls.clientWidth / selectedTable.scrollWidth
                selectedTable.style.transform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                selectedTable.style.transformOrigin = "0 0"
                selectedTable.style.webkitTransform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                selectedTable.style.webkitTransformOrigin = "0 0"
                selectedTable.style.width = otherEls.offsetWidth + "px"
                selectedTable.style.maxWidth = otherEls.offsetWidth + "px"
                //selectedTable.getElementsByTagName("tbody")[0].style.overflowX = "auto"
                var tempHeight = selectedTable.getBoundingClientRect().height
                if ( ancestorTag(selectedTable, 'div') ) {
                    ancestorTag(selectedTable, 'div').setAttribute("style", "width:" + ancestorWidth.offsetWidth + "px; height: " + tempHeight + "px;  ")
                } else{
                    var div = document.createElement('div')
                    div.setAttribute("class", "lc_tablewrapper")
                    div.wrap(selectedTable)
                    div.setAttribute("style", "width:" + ancestorWidth.offsetWidth + "px; height: " + tempHeight + "px;  ")

                }
            }

        }
    }

    

    var resizeTimeout;
    function resizeThrottler() {
    // ignore resize events as long as an actualResizeHandler execution is in the queue
        if ( !resizeTimeout && !supportsTouch) {
            resizeTimeout = setTimeout(function() {
                resizeTimeout = null;
                resizeWatcher();
                // The resize Watcher will execute at a rate of 15fps
            }, 66);
        }
    }

    function resizeWatcher() {
       
        if(largeTables.length > 0){
            // todo - this is breaking on resize, might be otherEls
            for (var i = 0; i < largeTables.length; i++) {
                var selectedTable, otherEls, scaleRatio

                selectedTable = largeTables[i]
                if(selectedTable.id != "highlightPopupContent"){
                    //otherEls = document.querySelector('body > section figure'),
                    otherEls = ancestorTag(selectedTable)
                    //console.log(selectedTable.id)
                    var ancestorWidth = otherEls.offsetWidth - ((parseInt(window.getComputedStyle(otherEls, null).getPropertyValue("padding-left")))*2)
                    console.log(ancestorWidth)
                    console.log(selectedTable.offsetWidth)
                    scaleRatio = ancestorWidth / selectedTable.offsetWidth
                    //scaleRatio =  otherEls.offsetWidth / selectedTable.offsetWidth
                    selectedTable.style.webkitTransform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                    selectedTable.style.webkitTransformOrigin = "0 0"
                    selectedTable.style.transform = "scale(" + scaleRatio +"," + scaleRatio + ")"
                    selectedTable.style.transformOrigin = "0 0"
                    selectedTable.style.width = otherEls.offsetWidth
                    //selectedTable.getElementsByTagName("tbody")[0].style.overflowX = "auto"
                    var tempHeight = selectedTable.getBoundingClientRect().height
                    //console.log(tempHeight)
                    if (selectedTable.parentNode.className.indexOf("lc_tablewrapper") != -1) {
                        //console.log(selectedTable.offsetHeight)
                        //ancestorTag(selectedTable, 'div').outerHTML = ancestorTag(selectedTable, 'div').innerHTML
                        selectedTable.parentNode.setAttribute("style", "height:" + tempHeight + "px; width:" + ancestorWidth + "px; ")

                    } else{
                        var div = document.createElement('div')
                        div.setAttribute("class", "lc_tablewrapper")
                        div.wrap(selectedTable)
                        div.setAttribute("style", "height:" + tempHeight + "px; width:" + ancestorWidth + "px;")


                    }
                }
            }
        }
        
        
        // scale the equations here
        var equations = document.getElementsByClassName("lc_equationwrapper")
        
        if(equations.length > 0){
            for(var i = 0; i < equations.length; i++){
                var equation = equations[i],
                    width = equation.offsetWidth,
                    innerWidth = 0,
                    innerHeight = equation.offsetHeight,
                    screenWidth = equation.parentNode.offsetWidth

                // get the inner width
                if(equation.childNodes[1] && equation.childNodes[1].className.indexOf("MathJax") != -1){
                    if(equation.childNodes[1].childNodes[0]){
                        innerWidth = equation.childNodes[1].childNodes[0].offsetWidth
                    } else {
                        innerWidth = equation.childNodes[2].childNodes[0].offsetWidth                    
                    }
                } else {
                    innerWidth = equation.childNodes[0].offsetWidth
                }

                if(innerWidth > screenWidth) {
                    scaleEquation(equation, innerWidth, screenWidth)
                } else {
                    equation.setAttribute("style", "width: "+screenWidth+"px; overflow: visible; margin: 0 auto;")
                    //equation.parentNode.setAttribute("style", "height: "+innerHeight+"px")
                } 
            }
            
        }
        
    }


    //find the closest figure parent
    function findAncestor (el, classname) {
        while ((el = el.parentElement) && !el.classList.contains(classname));
        return el;
    }
    function ancestorTag(node){
        // walk tree until you reach a section
        var newNode = node,
            isParent = false
        
        do {
            newNode = newNode.parentNode
            if(newNode.nodeName.toLowerCase() == "figure" || newNode.nodeName.toLowerCase() == "section" || newNode.nodeName.toLowerCase() == "aside" || newNode.nodeName.toLowerCase() == "li") isParent = true
            //console.log(newNode)
        } while(!isParent)
        
        return newNode
    }
    

    //find the closest figure parent
    function hasClass(el, selector) {
       var className = " " + selector + " ";
      
       if ((" " + el.className + " ").replace(/[\n\t]/g, " ").indexOf(className) > -1) {
        return true;
       }
      
       return false;
    }

    //auto width columns
    function autoCalculateColWidth(tableEl){
        var $table = $(tableEl);
        
        

        var $theadCells = $table.find('thead tr').children(), colCount
        // var colCount = $table.find('thead tr').length,
        //  colWidth = $table.parent().width() / colCount


        var $tbodyCells = $table.find('tbody tr:first').children();

        // Get the tbody columns width array
        colWidth = $tbodyCells.map(function() {
            return $(this).width();
        });
        
        // Set the width of thead columns
        $theadCells.each(function(i, v) {
            $(v).width(colWidth[i]);
        });    
        
    }

}(window.jQuery)