var splitview = {};

splitview.generate = function (theList) {
    var a = theList;
    for (var i = 0; i < a.length; i++) {
        createStr(a[i]);
    }

    function createStr(obj) {
        if (!obj.classList.contains("dontuch")) {

        }
        else {
            obj.classList.remove("dontuch");
            if (!obj.classList.length) {
                obj.removeAttribute("class");
            }
            var out = obj.outerHTML;
            obj.innerHTML = splitview.replacer(out, true);
            obj.classList.add("dontuch");
//            obj.insertAdjacentHTML('afterend',"<br>");
        }
        var theChild = obj.children;
        for (var j = 0; j < theChild.length; j++) {
            createStr(theChild[j]);
        }
    }
}

splitview.replacer = function (c, hasEndTags) {
    var t = c.split("");
    if (t[0] === "<")
        t[0] = "&lt;";
    c = t.join("");
    var revT = c.split("").reverse();
    if (hasEndTags) {
        for (var i = 0; i < revT.length; i++) {
            if (revT[i] === "<") {
                revT[i] = "&lt;";
                break;
            }
        }
    }
    return revT.reverse().join("");
}

splitview.showTagsAsText = function (tagName, hasEndTags, listedIn) {     //mention the id of dummy container of contents.
    hasEndTags = false; //Made false forcefully
    if (arguments[2] === undefined) {
        listedIn = "forsplit";               //Developed as per FROST settings.
    }
    var needle = document.getElementById(listedIn).getElementsByTagName(tagName);
    while (needle.length) {
        needle[0].classList.remove("dontuch");
        if (!needle[0].classList.length) {
            needle[0].removeAttribute("class");
        }
        var out = "";
        if (jQuery.inArray(needle[0].tagName, tagsWithoutContent) > -1) {
            needle[0].innerHTML = "";
            out = needle[0].outerHTML;
        }
        else {
            out = needle[0].innerHTML;
        }
        var replaced = splitview.replacer(out, hasEndTags);
        if (needle[0].id.length) {
            needle[0].outerHTML = "<span id='" + needle[0].id + "'>" + replaced + "</span>";
        }
        else {
            needle[0].outerHTML = "<span>" + replaced + "</span>";
        }
    }
}

splitview.updateCode = function (elemId) {
    htmlEditor.convertDivToFrame(jQuery(".cke_wysiwyg_frame").contents());
    var refData = CKEDITOR.instances.editor1.getData();
    jQuery('#trial_load').contents().find("body").html(refData);
    var selHtml = jQuery.trim(jQuery('#trial_load').contents().find("body").find("#" + elemId).prop("outerHTML"));
    htmlEditor.convertFrameToDiv();
    function sourceUpdater() {
        jQuery("#forsplit").find("*").addClass("dontuch");
        var theCh = document.getElementById("forsplit").children;
        splitview.generate(theCh);
        splitview.showTagsAsText("img", false);
        splitview.showTagsAsText("iframe", true);
        splitview.showTagsAsText("video", true);
        splitview.showTagsAsText("source", false);
        splitview.showTagsAsText("object", true);
        splitview.showTagsAsText("table", true);
        splitview.showTagsAsText("li", true);
    }
    document.getElementById("forsplit").innerHTML = refData;
    sourceUpdater();

    jQuery("#sourceVal").html(document.getElementById("forsplit").innerHTML);
    document.getElementById("forsplit").innerHTML = selHtml;
    sourceUpdater();

    var newCh = document.getElementById("forsplit").innerHTML;
    jQuery("#sourceVal").find("#" + elemId).replaceWith(newCh);
    splitview.highlightCode(elemId, false, 'editor');
};

splitview.highlightCode = function (id, shouldscroll, reqFrom) {
    var editor = jQuery(".cke_wysiwyg_frame").contents().find("body");
    var source = jQuery("#sourceVal");
    source.find(".mark-me").removeClass("mark-me");
    //editor.find(".indicator").removeClass("indicator");
    try {
        /*source.find("#" + id).addClass("mark-me");
         editor.find("#" + id).addClass("indicator");*/
        var scrollSrcTo = jQuery("#sourceVal").scrollTop() + jQuery("#sourceVal").find("#" + id).offset().top - jQuery("#sourceVal").height();
        var scrollEditorTo = editor.find(".indicator").offset().top;
    }
    catch (exc) {
        id = editor.find("#" + id).parents("*[id]").attr("id");
        source.find("#" + id).addClass("mark-me");
        //editor.find("#" + id).addClass("indicator");
        try {
            var scrollSrcTo = jQuery("#sourceVal").scrollTop() + jQuery("#sourceVal").find("#" + id).offset().top - jQuery("#sourceVal").height();

            var scrollEditorTo = editor.find(".indicator").offset().top;
        } catch (exc) {
            console.log('Window says: ' + exc);
        }

    }
    if (reqFrom == 'editor') {
        source.find("#" + id).addClass("mark-me");
    }
    if (reqFrom == 'source') {
        editor.find("#" + id).addClass("indicator");
    }
    if (shouldscroll) {
        scrollSrcTo = scrollSrcTo > 0 ? scrollSrcTo : 0;
        scrollEditorTo = scrollEditorTo > 0 ? scrollEditorTo : 0;
        if (reqFrom == 'editor')
            jQuery("#sourceVal").scrollTop(scrollSrcTo);
        if (reqFrom == 'source')
            editor.scrollTop(scrollEditorTo);
    }
}

htmlEditor.splitView = function () {
    jQuery(".cke_contents.cke_reset").toggleClass("split-view");
    var showCode = false;
    htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents());
    htmlEditor.convertDivToFrame(jQuery(".cke_wysiwyg_frame").contents());
    document.getElementById("forsplit").innerHTML = CKEDITOR.instances.editor1.getData();
    htmlEditor.convertFrameToDiv();
    jQuery("#forsplit").find("*").addClass("dontuch");
    var theCont = document.getElementById("forsplit").children;
    splitview.generate(theCont);
    if (jQuery(".lm-edt-pane.middle-pane").hasClass("active")) {
        jQuery(".lm-edit-more").trigger("click");
        jQuery(".right-pane,.edt-rt").addClass("hideElm");
        htmlEditor.onWindowResize();
        showCode = true;
    }
    else if (jQuery(".cke_contents.cke_reset").hasClass("split-view")) {
        jQuery(".right-pane,.edt-rt").addClass("hideElm");
        showCode = true;
        htmlEditor.onWindowResize();
    }
    else {
        jQuery(".right-pane,.edt-rt").removeClass("hideElm");
        htmlEditor.onWindowResize();
    }
    jQuery(".split-source-container").toggleClass("hideElm");

    if (showCode) {
        CKEDITOR.instances.editor1.getCommand("source").disable();
        insertSource();
    }
    else {
        CKEDITOR.instances.editor1.getCommand("source").enable();
    }

    function getIdToHighlight(elem, source) {
        var tgt = elem;
        var id = tgt.id === "" ? jQuery(tgt).parents("*[id]").attr("id") : tgt.id;
        splitview.highlightCode(id, true, source);
    }

    jQuery("#sourceVal").off("click").on("click", "*", function (e) {
        htmlEditor.removeUnwanteds(jQuery("iframe.cke_wysiwyg_frame").contents());
        var tgt = e.target;
        getIdToHighlight(tgt, 'source');
    });
    var mathTimer;
    jQuery("#sourceVal").on("keyup", function (e) {

        var elemVal = htmlEditor.getDocParSel();
        var elemPar = elemVal.parentElement;

        var elemId = -1;
        if ((jQuery(elemVal).parents("*[id]").attr("id")) != "sourceVal") {
            elemId = elemPar.id === "" ? jQuery(elemVal).parents("*[id]").attr("id") : elemPar.id;
        }
        else {
            elemId = jQuery(elemVal).attr("id");
        }
        htmlEditor.removeUnwanteds(jQuery("iframe.cke_wysiwyg_frame").contents());
        var isMathJaxModified = elemPar.classList.contains("math-tex") || elemPar.classList.contains("math-tex");
        if (mathTimer)
            clearTimeout(mathTimer);
        function emptyEditor() {
            if (jQuery("#sourceVal").children("*").length == 1)
            {
                if (e.keyCode == 8 || e.keyCode == 46)
                {
                    jQuery("iframe.cke_wysiwyg_frame").contents().find('body').empty();
                }
            }
        }
        try {
            var elemcont = document.getElementById(elemId).textContent;
            jQuery("iframe.cke_wysiwyg_frame").contents().find("body").find("#" + elemId).replaceWith(elemcont);
            
        } catch (exc) {
            console.log(exc);

        }
        emptyEditor();
        htmlEditor.convertFrameToDiv();
        splitview.highlightCode(elemId, false, 'source');

        if (isMathJaxModified) {
            mathTimer = setTimeout(function () {
                var contents = CKEDITOR.instances.editor1.getData();
                htmlEditor.setData(contents);
                htmlEditor.currContent = contents;
                htmlEditor.mathMlEdited = true;
                htmlEditor.enableSave();
            }, 3000);
        }
        htmlEditor.enableSave();
    });

    jQuery("iframe.cke_wysiwyg_frame").contents().find("body").find("*").on("click", function (e) {
        if (jQuery("#sourceVal:visible").length) {
            htmlEditor.removeUnwanteds(jQuery("iframe.cke_wysiwyg_frame").contents());
            var tgt = e.target;
            getIdToHighlight(tgt, 'editor');
        }
    });

    jQuery("iframe.cke_wysiwyg_frame").contents().find("body").on("keyup", function (e) {
        onKeyPressFired = true;
        htmlEditor.liveUpdateView();
        onKeyPressFired = false;
    });

    function insertSource() {
        try {
            CKEDITOR.instances.sourceVal.destroy();
        }
        catch (exc) {

        }
        splitview.showTagsAsText("img", false, "forsplit");     //Tagname and the dummy div holder's id
        splitview.showTagsAsText("iframe", true);
        splitview.showTagsAsText("video", true);
        splitview.showTagsAsText("source", false);
        splitview.showTagsAsText("object", true);
        splitview.showTagsAsText("table", true);
        splitview.showTagsAsText("li", true);
        var cont = document.getElementById("forsplit").innerHTML;
        jQuery(".split-source").find("#sourceVal").html(cont).attr("contenteditable", true);
    }
}
htmlEditor.timer;
htmlEditor.liveUpdateView = function () {
    if (htmlEditor.timer)
        window.clearTimeout(htmlEditor.timer);
    htmlEditor.timer = window.setTimeout(function () {
        if (jQuery("#sourceVal:visible").length) {
            var sel = htmlEditor.getEditorSelection();
            var selElm = sel.getStartElement().$;
            var selElmId = selElm.id === "" ? jQuery(selElm).parents("*[id]").attr("id") : selElm.id;
            htmlEditor.removeUnwanteds(jQuery("iframe.cke_wysiwyg_frame").contents());
            splitview.updateCode(selElmId);
        }
    }, 100);

}

htmlEditor.getDocParSel = function () {
    var selection = document.getSelection();
    var focussedNode = selection.focusNode;
    return focussedNode;
}