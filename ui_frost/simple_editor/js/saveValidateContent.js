// Functions to save data from the editor.

htmlEditor.createVersion = true;                    // Set if version is to be created.
htmlEditor.autoMerge = false;                        // Set to false by default.

htmlEditor.saveContent = function () {
    htmlEditor.removeUnwanteds(jQuery('.cke_wysiwyg_frame').contents());
    var new_node_title = jQuery('#node_title').text();
    var ver_id = jQuery("#verAccordion").find(".version_num").eq(0).attr("version_id");         //get latest version from the list.

    node_title = new_node_title.replace(/&nbsp;/gi, '').trim();

    if (htmlEditor.needsToBeMerged) {
        swal({
            //            title: "You have been inactive for 5 minutes",
            //            html:     'You can use <b>bold text</b>, ' +     '<a href="//github.com">links</a> ' +     'and other HTML tags',
            html: "<h1>There's been a change!!</h1><p style='font-size:20px;'>Changes has been made by others on this particular content. Choose the mode of merging.</p>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Merge manually",
            cancelButtonText: "Let System Merge For Me",
            closeOnConfirm: true,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                htmlEditor.mergeWithNewVersion();
            } else {
                htmlEditor.autoMerge = true;
                htmlEditor.webWorker(1, htmlEditor.createVersion, ver_id); // pass true to create version on saving.
                jQuery("#save_toc_id").button('loading');
            }
        });
    }
    else {
        htmlEditor.webWorker(1, htmlEditor.createVersion, ver_id); // pass true to create version on saving.
        jQuery("#save_toc_id").button('loading');
    }
};
// Fetch content from the editor.
htmlEditor.getContent = function () {
    var chapToBeSaved = CKEDITOR.instances.editor1.getData() //+ htmlEditor.extraContents;
    var trialFrame = jQuery(".editor-container5 iframe#trial_load").contents();
    trialFrame.find("body").html(chapToBeSaved);
    htmlEditor.removeUnwanteds(trialFrame); // Remove classes & elements before saving.
    htmlEditor.convertDivToFrame(trialFrame);
    htmlEditor.imageTimeStamp(trialFrame.find("body"), false);
    var toBeSaved = trialFrame.find("body").html();
//    htmlEditor.setData(toBeSaved);
//    toBeSaved = CKEDITOR.instances.editor1.getData();
//    htmlEditor.convertFrameToDiv();
    trialFrame.find("body").empty();
    return toBeSaved;
};

htmlEditor.webWorker = function(showresponse, versioning, version_id) {
     if(htmlEditor.backToDashboardFlag){
        htmlEditor.backToDashboard();
        htmlEditor.backToDashboardFlag=false;
    }
    var w;
    if (typeof (Worker) !== "undefined") {              // Check if webworker is supported.
        if (typeof (w) == "undefined") {
            w = new Worker("js/save_at_back.js");
        }
        var values = {};
        values.url = htmlEditor.PXEmappedAPI.saveContentAPI;
        versioning = versioning ? versioning : "";
        values.data = {content: htmlEditor.getContent(), node_id: node_id, project_id: project_id, node_title: node_title, versioning: versioning, version_id: version_id, auto_merging: htmlEditor.autoMerge};
        w.postMessage(values); // Send data value to the webworker to save it.
        // Check if response needs to be rendered to the user after successful save.
        if (showresponse) {
            w.onmessage = function (event) {
                htmlEditor.currContent = htmlEditor.getLatestSavedContent();
                var e = JSON.parse(event.data);
                var new_content = e.data.description;

                try{
                    if(jQuery("#saveModal")){
                         jQuery("#saveModal").modal("hide");
                         jQuery("#save_toc_id").button('reset');
                         
                     }
                    //htmlEditor.setData(new_content);
                     htmlEditor.disableSave();
                }
                catch (exc) {

                }

                try {                                   //Give option to post a saving note while hitting save button
                    var save_note = jQuery("#savingnote:visible").val().trim();         //get the note 
                    var dataForSavingNotes = {
                        version_id: e.data.version_id,
                        comment: save_note
                    }
                    jQuery.ajax({
                        url: htmlEditor.PXEmappedAPI.saveSavingNote,
                        type: 'POST',
                        data: dataForSavingNotes,
                        xhrFields: {
                            withCredentials: true
                        },
                        crossDomain: true,
                        async: true,
                        success: function (data, textStatus, jqXHR) {
                            jQuery("#savingnote").val("");
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("There was some error posting the comment");
                        }
                    });
                }
                catch (exc) {

                }
                htmlEditor.getAllVersions();
                htmlEditor.editorEvents();
                jQuery("#save_toc_id").button('reset');
//                htmlEditor.validateContent();
                if (htmlEditor.createVersion) {                 //If new version is created, fetch all the version list.
                    htmlEditor.createVersion = false;
                    htmlEditor.autoMerge = false;
                }
                jQuery("#savingnote").val("");
                jQuery("#saveModal").modal("hide");
                htmlEditor.mathMlEdited = false;
            };
        }
    } else {
        swal("Sorry! No Web Worker support.");
    }
};

htmlEditor.validateContent = function () {
    var x;
    if (typeof (Worker) !== "undefined") {              // Check if webworker is supported.
        if (typeof (x) == "undefined") {
            x = new Worker("js/validate_at_back.js");
        }
        var values = {};
        values.url = htmlEditor.PXEmappedAPI.validateContentAPI;
        x.postMessage(values); // Send data value to the webworker to save it.
        // Check if response needs to be rendered to the user after successful save.
        x.onmessage = function (event) {
            var result = JSON.parse(event.data);
            htmlEditor.validateResponse(result);
        };
    } else {
        swal("Sorry! No Web Worker support.");
    }
}

htmlEditor.validateResponse = function (data) {
    var responsedata = data.validation_response;
    if (responsedata.data.errors || responsedata.data.warnings) {
        jQuery("#validation_notification").show();
    } else {
        jQuery("#validation_notification").hide();
    }
    htmlEditor.showValidationContent(responsedata);
}


htmlEditor.showValidationContent = function (responsedata) {
    var content = '';
    if (responsedata.data_json.errors) {
        jQuery(".error-count").text(responsedata.data_json.errors).show();
    }
    else {
        jQuery(".error-count").text("0").show();
    }
    if (responsedata.data_json.status == 'fatal') {
        content = '<div class="lm-rochak lm-val-error"><div><span class="fa  fa-exclamation-circle"></span></div><strong>FATAL ERROR</strong><h4>TOTAL ERRORS: ' + responsedata.data_json.errors + '</h4></div>';
    } else {
        if (responsedata.data_json.warnings || responsedata.data_json.errors) {
            content = '<div class="lm-rochak lm-val-error"><div><span class="fa  fa-exclamation-circle"></span></div><strong>You need to fix these errors to get a valid PXE</strong><h4>TOTAL ERRORS: ' + responsedata.data_json.errors + '</h4></div>';
            if (responsedata.data_json.warnings) {
                jQuery.each(responsedata.data_json.file_list, function (idx, obj) {
                    content += '<div class=" lm-err-list">'
//            content += '<div>You need to fix these warnings to get a valid PXE';
//            content += 'Total Warnings' + obj.warnings;
//            content += "</div>"

                    jQuery.each(obj.warning_list, function (idx, obj) {
                        content += "<div>" + obj.msg + "<p>line number " + obj.line + ",@col " + obj.col + "</p></div>";
                    });
                    content += '</div>';
                });
            }
            if (responsedata.data_json.errors) {
                jQuery.each(responsedata.data_json.file_list, function (idx, obj) {
                    content += '<div class=" lm-err-list">'
//            content += '<div>You need to fix these errors to get a valid PXE';
//            content += 'Total Erros:' + obj.errors;
//            content += "</div>"

                    jQuery.each(obj.error_list, function (idx, obj) {
                        content += "<div>" + obj.msg + "<p>line number " + obj.line + ",@col " + obj.col + "</p></div>";
                    });
                    content += '</div>';
                });
            }
            if (responsedata.data_json.message) {
                content += '<div class=" lm-err-list">';
                content += "<div>" + responsedata.data_json.message + "</div>";
                content += '</div>';
            }

        } else if (!responsedata.data_json.warnings && !responsedata.data_json.errors) {
            content = '<div class="lm-rochak lm-val-success"><div><span class="fa fa-check-square"></span></div><h4>VALID DOCUMENT</h4></div>';
        }
    }
//    content = JSON.stringify(content);
//    content = content.substring(1, content.length - 1);
    jQuery("#validation_result").find(".validation_details").html(content);
};