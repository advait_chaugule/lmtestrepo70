/* EDUPUB rule setter JS
Applicable for Sections & Asides
*/

/* Method that initiates level rules to assign */
$.fn.setLevelRule = function(){
    var that = this, //Current element scope
        vElmType = that.prop('tagName'), //Current element tag name
        isSecAsd = vElmType === 'SECTION' ? vElmType : vElmType === 'ASIDE' ? vElmType : false, //Returns element type
        maxLevel = 6, //Maximum level that elements can reach to [class/header]
        classStr = "level", //Section class name string
        hClass = "title"; //Inside <header><h1,h2,h3,....> class name

    /* Method that sets actual header level 
     @elm: Current Element that is referred to
     @lvl: Current elements level
    */

    function setHeader(elm, lvl){
        return;
        var assignLevel = lvl < maxLevel ? Number(lvl) + 1 : lvl, //Level to assign to the element
            headerElm = elm.find('header'), //Refers to header element
            hElm = headerElm.find("h1"), //Refers to child element of header
            hContent = hElm.html(), //Contents of header child element
            hClass = hElm.attr('class'), //Header child's class
            hFinalContent = "<h"+assignLevel+" class="+hClass+">" + hContent + "</h"+assignLevel+">"; //String generated to append to header element
        console.log(elm.html())
        headerElm.html(hFinalContent); //Dumping the string to header element
    }

    /* Method that sets actual class level 
     @elm: Current Element that is referred to
     @lvl: Current elements level
    */

    function setSecLevel(elm, lvl){
        var assignLevel = lvl < maxLevel ? Number(lvl) + 1 : lvl,
            assignClass = classStr + assignLevel;

        elm.removeAttr('class').addClass(assignClass);
    }

    /* Method that return section class level 
     @elm: Current Element that is referred to
    */

    function getSectionLevel(elm){
        var isLevel = false, //Flag that checks level
            elmClasses = elm.attr('class'); //Classes of current element

        try{
            elmClasses = elmClasses.split(' ');
        }
        catch(exc){
            var tempStor = elmClasses; //For temporary storage
            elmClasses = [tempStor];
        }


        try{
            for(var i=0; i < elmClasses.length; i++){
                var currClassName = elmClasses[i],
                    isTypeLevel = currClassName.substring(0,5) === classStr ? currClassName.substring(0,5) : false,
                    levelVal = 0;

                if(isTypeLevel){
                    levelVal = currClassName.substring(5,6);
                    isLevel = levelVal;
                }
            }
        }
        catch(exc){
            console.info(exc);
        }

        return isLevel;
    }

    /* Method that return aside class level 
     @elm: Current Element that is referred to
    */

    function getAsideLevel(elm){
        var isLevel = false,
            currElm = elm,
            headerLevel = 1;

        /* Method that return section/aside with header 
         @elm: Current Element that is referred 
        */

        function getParWithHeader(elm){
            currElm = elm.parent(); //Current elements parent
            var headerCheck = currElm.children('header').length, //Checking if header is present
                headerNotFound = (headerCheck === 0),
                isNotBody = elm.prop('tagName') !== 'BODY'; //If current element is BODY
            
            if(headerNotFound && isNotBody){
                getParWithHeader(currElm); //Parent element that has header
            }
            else{
                return currElm; //Returns current element
            }
        }

        var parHasHeader = getParWithHeader(currElm); //Parent element of referred element that has header
        parHasHeader = typeof parHasHeader === 'undefined' ? false : parHasHeader; //Parent with header check

        if(parHasHeader){
            var hType = (parHasHeader.find('.'+hClass).prop('tagName')).toString(), //Checking H1|H2|H....
                hLevel = hType.charAt(1); //Get header value

            headerLevel = hLevel; //Assigning header level
        }
        else{
            headerLevel = 0;
        }
        return headerLevel; //Return current header level
    }

    /* Method that sets rules of sections
    */

    function rulesForSection(){
        var parNode = that.parents('section'); //Parent node that is of type SECTION
        var getLevel = getSectionLevel(parNode); //Get section level

        if(getLevel)
        {
            setSecLevel(that, getLevel); //Sets section class level
            setHeader(that, getLevel); //Sets section header level
        }
    }

    /* Method that sets rules of asides
    */

    function rulesForAside(){
        var currNode = that; //Get current element
        var getLevel = getAsideLevel(currNode); //Get Aside header level

        if(getLevel)
        {
            setHeader(that, getLevel); //Sets section header level
        }
    }

    /* Sets rules for element types
    */

    switch(isSecAsd){
        case "SECTION":
            rulesForSection(); //Sets Section rules
            break;
        case "ASIDE":
            rulesForAside(); //Sets Aside rules
            break;
        default:
            break;
    }
}

/* Method that initiates drop rules to assign */

var setDropRule = function(el,dElm){
    var that = el,
        keyType = 'class', // !! Need to find way for class/epub:type
        pUpto = 'section',
        cElmClass = dElm.attr('default-class'),
        pElm = that.parents(pUpto),
        kValue = pElm.attr(keyType);

    if(hasMultipleClass(jQuery.trim(kValue))){
        kValue = kValue.split(' ');
    }
    else{
        kValue = [kValue]
    }

    /* Method that checks drop rules for pattens[PXE]
    */

    return (function(){
        var allowToDrop = true;

        for(var i=0; i<kValue.length; i++){
            var cKey = kValue[i],
                naList = typeof patternRules[cKey] !== 'undefined' ? patternRules[cKey].notAllowed : false;

            if(naList){
                for(var j=0; j<naList.length; j++){
                    var cVal = naList[j];

                    if(cVal === cElmClass){
                        allowToDrop = false;   
                    }
                }

            }
        }

        return allowToDrop;
    })();

    /* Method that checks if string contanins white-space
    */

    function hasMultipleClass(s) {
        return /\s/g.test(s);
    }
}
