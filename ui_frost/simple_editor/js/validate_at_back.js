self.onmessage = function(event) {
    var details = event.data;
    var url = details.url;
    var xhr = new XMLHttpRequest();
    if ('withCredentials' in xhr) {
        xhr.open("GET", url, true);
        xhr.withCredentials = "true";
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        //xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send();
        xhr.onreadystatechange = function()
        {
            if (xhr.readyState == 4 && xhr.status == 200)
            {
                postMessage(xhr.responseText);
            }
        }
    }
};