/* 
 * Dependency browser should support post-message
 */


(function () {
    /*
     * Function that listen to Parent response
     */

    //document.addEventListener("click", resizeFrameEl);

    if (window.addEventListener) {
        window.addEventListener("message", listenParentMessage, false);
    } else {
        window.attachEvent("onmessage", listenParentMessage);
    }

    function listenParentMessage(msg) {
        var response = msg.data;
        try {
            if (response.type == 'GET') {
                widgetDataGetter('POST', response);
            }
            else if (response.type == 'IMAGE') {
                widgetImageSetter(response);
            }
        }
        catch (exc) {
            //console.log(exc);
        }
    }


})();
function resizeFrameEl() {
    var getDefaultData = {
        'type': 'SIZE',
        'frame_id': parRef,
        'height': '260',
        'width': document.body.clientWidth
    };
    top.postMessage(getDefaultData, "*"); // Posts message(Request) to parent
    parent.postMessage(getDefaultData, "*");

    setTimeout(function () {
        var getDefaultData = {
            'type': 'SIZE',
            'frame_id': parRef,
            'height': '260',
            'width': document.body.clientWidth
        };
        top.postMessage(getDefaultData, "*"); // Posts message(Request) to parent
        parent.postMessage(getDefaultData, "*");
    }, 500);
}

function widgetDataSetter(reqType, data) {
    reqType = reqType.toUpperCase();

    var getDefaultData = {
        'type': reqType,
        'dataset': data
    }

    top.postMessage(getDefaultData, "*"); // Posts message(Request) to parent
    parent.postMessage(getDefaultData, "*");
}

function widgetImageSetter() {
    var imageSet = arguments[0].image_detail;
    //  
    /*  
     * var imageSet is an object that contains
     * imageSet.url - Image Path
     * imageSet.alt - Alternate Text
     * imageSet.height - Image height
     * imageSet.width - Image width
     */
    _widget_.setImage(imageSet);
}

var parRef = '';
function widgetDataGetter() {
    if (arguments[0] == 'POST') {
        parRef = arguments[1].frame_id;
        _widget_.initData(arguments[1]); //Return data to child frame [Widget function that sets data should be called here]
        resizeFrameEl();
    }
    else if (arguments[0] == 'IMAGE') {
        var getDefaultData = {
            'type': arguments[0],
            'frame_id': parRef
        };
        top.postMessage(getDefaultData, "*"); // Posts message(Request) to parent
        parent.postMessage(getDefaultData, "*");
    }
    else {
        var getDefaultData = {
            'type': arguments[0],
            'height': '590',
            'width': $(document).width()
        };
        top.postMessage(getDefaultData, "*"); // Posts message(Request) to parent
        parent.postMessage(getDefaultData, "*");
    }
}
