
htmlEditor.createMath = function() {
    jQuery("#mathModal").modal("show");
    jQuery("#mathConf").off().on("click", function() {
        var inline = jQuery("#inline").is(":checked");          //Check if user wants the equation to be placed inline.
        if (inline) {
            var html = "<span class='inlineequation' id=''>";
            html += jQuery("#viewLatex").contents().find("body").find(".equation").html();
            html +="</span>";
        }
        else {
            var html = jQuery("#viewLatex").contents().find("body").find(".equation").prop("outerHTML");
        }
        var element = CKEDITOR.dom.element.createFromHtml(html);
        var generatedId = guid();
        element.setAttribute("id", generatedId());
        CKEDITOR.instances.editor1.insertElement(element);
        jQuery("#mathModal").modal("hide");
    });
}


htmlEditor.runMathML = function(frameId) {                         // Function that converts native equation to eqn form.
    var win = document.getElementById(frameId).contentWindow;
    win.postMessage("true", "*");
}


htmlEditor.updatePreview = function(field) {
    var val = field.value;
    jQuery("#viewLatex").contents().find("body").find(".equation").html(val);
    htmlEditor.runMathML("viewLatex");
}