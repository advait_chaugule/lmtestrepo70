htmlEditor.callTickets = function() {
    var ticketObj,tickets;
    jQuery.when(ticketObj=htmlEditor.ajaxCalls(htmlEditor.PXEmappedAPI.getAllReviews)).done(function() {
        tickets = ticketObj.responseJSON.data.ticket;
        htmlEditor.createList(tickets);
    })
}

htmlEditor.createList = function(list) {                                 // Create the accordion containing all the issues.
    var ticketCollection = '<div class="add-new-ticket"><button class="btn btn-default create new-action" data-loading-text="Creating New Issue...">Add New Issue</button><div class="new-ticket-container"></div></div><div class="comments-wrapper"><div class="panel-group" id="accordionTicket" role="tablist" aria-multiselectable="true">';
    var pre_tickets = list;
    var numOfIssues = pre_tickets.length;
    var open_issues_count = 0;
    var ticketHeaderId = "";
    var ticketContentId = "",
            attachImage = "";    
    if (numOfIssues) {
        for (var i = 0; i < pre_tickets.length; i++) {         
            if(pre_tickets[i].attachment === null){
                attachImage = false;
            }
            else{
                attachImage = pre_tickets[i].attachment[0].path
            }
            
            ticketHeaderId = "ticket" + i;
            ticketContentId = "collapseTicket" + i;
             var dateResponseString = (pre_tickets[i].created_at).toString();
                        var date = (jQuery.format.date(dateResponseString, "MM/dd/yyyy HH:mm"));
            ticketCollection += '<div class="panel panel-default"><div class="panel-heading" role="tab" id="' + ticketHeaderId + '"><div class="panel-title"><a data-ticket-id="' + pre_tickets[i].ticket_display_id + '" data-parent-ticket-id="'+pre_tickets[i].ticket_id+'"  class="collapsed" data-toggle="collapse" data-parent="#accordionTicket" href="#' + ticketContentId + '" aria-expanded="true" aria-controls="' + ticketContentId + '"><div class="issue-no">Issue# ' + pre_tickets[i].ticket_display_id + '<p><b>Assigned to: </b><span class="assignee" user-id="' + pre_tickets[i].assigned_to + '">' + pre_tickets[i].assigned_to_username + '</span></p></div></div></a></div><div id="' + ticketContentId + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="' + ticketHeaderId + '"><div class="panel-body"><ul class="comments issueCreated"><li data-comment-id=""><a class="author-name right-of-avatar" href="#">' + pre_tickets[i].created_by_username + '</a><a class="author-date right-of-avatar" href="#">' + date + '</a><p class="comment right-of-avatar">' + pre_tickets[i].issue_description + '</p><label class="action-link ' + pre_tickets[i].type.toLowerCase() + ' labl">' + pre_tickets[i].type + '</label><label class="action-link status">' + pre_tickets[i].status + '</label><label class="action-link delete glyphicon glyphicon-trash" title="Delete"></label>';
            if(attachImage){
               ticketCollection += '<a href="'+attachImage+'" target="_blank" class="ticketAttachImg glyphicon glyphicon-paperclip" title="View Attachment"></a></li></ul><button class="btn add-comment new-action" data-loading-text="Creating New Comment...">Leave a comment</button><div class="new-ticket-container"></div><div class="comments-on-ticket"><ul class="comments thread">';
            }
            else{
                ticketCollection += '</li></ul><button class="btn add-comment new-action" data-loading-text="Creating New Comment...">Leave a comment</button><div class="new-ticket-container"></div><div class="comments-on-ticket"><ul class="comments thread">';
            }
            
            var comments = pre_tickets[i].ticket_comments;          // Comments on issues raised.
            if (comments.length) {
                for (var j = 0; j < comments.length; j++) {
                     var dateResponseString2 = (comments[j].created_at).toString();
                         date2 = (jQuery.format.date(dateResponseString2, "MM/dd/yyyy HH:mm"));
                    ticketCollection += '<li data-comment-id="' + comments[j].ticket_comment_id + '"><a class="author-name right-of-avatar" href="#">' + comments[j].created_by_username + '</a><a class="author-date right-of-avatar" href="#">' + date2 + '</a><p class="comment right-of-avatar">' + comments[j].comment + '</p><label class="action-link del glyphicon glyphicon-trash" title="Delete"></label></li>';
                }
            }
            else {
                ticketCollection += "<li><em> No comments yet</em></li>";
            }
            ticketCollection += '</ul></div></div></div></div>';
            if (pre_tickets[i].status) {
                open_issues_count++;
            }
        }
        jQuery(".edt-rt").find(".issue-count").text(open_issues_count).show();
    }
    else {
        ticketCollection += '<div class="lm-rochak"><div><span class="fa fa-comments"></span></div><strong>No Comments added Yet!</strong>Click on the button above to add new comment</div>';
        jQuery(".edt-rt").find(".issue-count").text("0").hide();
    }
    ticketCollection += "</div></div>";
    jQuery("#issues").find(".right-comment .post-comment").html(ticketCollection);
    htmlEditor.bindTicketEvents();
};

htmlEditor.getUsers = function() {                                 // Fetch all users associeated with the current project.
    var userObj;
    jQuery.when(userObj= htmlEditor.ajaxCalls(htmlEditor.PXEmappedAPI.getUsersAPI)).done(function(){
        htmlEditor.infoObj.userList = userObj.responseJSON.data;
    })
};

htmlEditor.getStatus = function() {                                     // Fetch all the status & types for the current project.
    return jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.getAllStatus,
        type: 'GET',
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            var listOfStatus = data.data.ticket_status;
            var listOfStatusTypes = data.data.ticket_issue_types;
            htmlEditor.infoObj.statusList = listOfStatus;
            htmlEditor.infoObj.statusTypes = listOfStatusTypes;
        },
        error: function() {
            alert("There was an error fetching the Ticket status.")
        }
    })
}
