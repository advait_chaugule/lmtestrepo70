/*************************************************/
/** Sortability in iframe - Supriyo Chakroborty **/
/** Dependency - jQuery.min.js && jQuery.ui.js **/
/************************************************/

//========================================================================
//  Function that initiates Sortability in iframe
//========================================================================

var allowDragReInitiate = true;

function createSortOnIframe(pattern_arr_obj, jQuery_obj) {
    var jQ, drag_class, iframe, containment_class, sort_highlight_class, elm_highlight, overlay_on_drop, class_to_hide, drag_stop_cond, contentArray, indexOfDragged, iframeBodyChildPos, iframeBodyChildPosTop, iframeBodyChildPosTop, iframeBodyChildPosTopLeft, iframeObj, iframeBodyElm, iframeBodyChild, iframeScrollPos, currIframeElm, currIframeElmRgtPos, trueDrop;

    jQ = jQuery_obj; //jQuery object
    drag_class = jQ('.right-pane .menu-block'); //Draggable element
    iframe = jQ('iframe.cke_wysiwyg_frame'); //iFrame element
    containment_class = ".editor-container5"; //Containment element
    sort_highlight_class = 'sort_highlight'; //Highlight class while sorting
    elm_highlight = 'elm_name'; //Class that highlights name of current element
    overlay_on_drop = '.iframeDropOverlay'; //Overlay class
    class_to_hide = 'hideElm'; //Class to hide elements
    drag_stop_cond = jQ("iframe.cke_wysiwyg_frame").contents().find('body').find('.disable_click').length > 0; //Condition to stop dragging [Returns boolean true or false]

    //===============================================================================
    //    Don't set any values of below mentioned variables as it is used internally
    //===============================================================================

    contentArray = pattern_arr_obj; //Array that contains the pattern types
    indexOfDragged = 0; //Index of the element currently dragged
    iframeBodyChildPos = {}; //Contains the positions of the children within the iframe
    iframeBodyChildPosTop = new Array(); //Contains the offset top of the children within the iframe
    iframeBodyChildPosTopLeft = new Array(); //Contains the offset top & left of the children within the iframe
    iframeObj = iframe.contents(); //Returns the iframe element
    iframeBodyElm = iframeObj.find('body'); //Returns the iframe body element 
    iframeBodyChild = iframeBodyElm.find('*'); //Returns the children within the body of the iframe element
    iframeScrollPos = iframeBodyElm.scrollTop(); //Returns the iframe element scroll position
    currIframeElm = ''; //Returns the current iframe element
    currIframeElmRgtPos = 0; //Returns the current iframe element right postion (i.e. Offset.left() + width)
    trueDrop = false; //Returns a boolean value for a drop

    jQ(overlay_on_drop).width(iframe.width()); //Sets width of the overlay with respect to iframe

    //==============================================================
    //  Function that initiates variables
    //==============================================================

    var initiateObj = (function() {
        iframeBodyChild = iframeBodyElm.find('*');
        var prevTop = new Array();
        iframeBodyChild.each(function(i, v) {
            var offTop = jQ(v).offset().top;
            var offLeft = jQ(v).offset().left;
            iframeBodyChildPosTop.push(offTop);
            iframeBodyChildPosTopLeft.push(offTop + "." + offLeft);

            if (jQ.inArray(offTop, prevTop) === -1) {
                iframeBodyChildPos[offTop] = new Array();
                iframeBodyChildPos[offTop].push(offLeft);
                prevTop.push(offTop);
            }
            else {
                iframeBodyChildPos[offTop].push(offLeft);
            }
        });
    })();

    //===============================================================
    // Function that initiates draggablity of elements
    //===============================================================
    if (allowDragReInitiate) {
        var currPatternDropped = '';
        function createDraggable() {
            var drag_settings = {
                containment: containment_class,
                'revert': 'invalid',
                scroll: false,
                helper: function(e, li) {
                    var this_width = jQ(this).innerWidth();
                    var this_height = jQ(this).innerHeight();
                    copyHelper = jQ(this).clone();
                    copyHelper.innerWidth(this_width);
                    copyHelper.innerHeight(this_height);
                    return copyHelper;
                },
                'refreshPositions': true,
                start: function(evt, ui) {
                    if (drag_stop_cond)
                        return false;
                    jQ(overlay_on_drop).removeClass(class_to_hide);
                    trueDrop = false;
                    indexOfDragged = jQ(this).index();
                },
                drag: function(evt, ui) {
                    var iframeOffsetLeft = iframe.offset().left;
                    var uiItemObjPos = ui.position;
                    var uiItemObjOffset = copyHelper.offset();
                    var uiItemOffsetTop = uiItemObjPos.top;
                    var uiItemOffsetLeft = uiItemObjOffset.left - iframeOffsetLeft;
                    iframeScrollPos = iframeBodyElm.scrollTop();

                    var currIndexFromTop = getClosestIndex(iframeBodyChildPosTop, (uiItemOffsetTop + iframeScrollPos));
                    var currTopClosest = iframeBodyChildPosTop[currIndexFromTop];

                    //  To Enable Scroll functionality in iFrame while dragging
                    if (iframeBodyElm.width() > uiItemOffsetLeft) {
                        if (uiItemOffsetTop <= iframeBodyElm.offset().top) {
                            iframeScrollPos = iframeScrollPos - 10;
                            iframeBodyElm.scrollTop(iframeScrollPos);
                        }
                        else if (uiItemOffsetTop >= iframe.height()) {
                            iframeScrollPos = iframeScrollPos + 10;
                            iframeBodyElm.scrollTop(iframeScrollPos);
                        }
                    }

                    //  To Enable Sortable functionality in iFrame
                    var currIndexFromLeft = getClosestIndex(iframeBodyChildPos[currTopClosest], uiItemOffsetLeft);

                    // Calculations for drop in Tables
                    var currLeftClosest = iframeBodyChildPos[currTopClosest][currIndexFromLeft];
                    var currTopLeft = currTopClosest + "." + currLeftClosest;

                    var currIndx = iframeBodyChildPos[currTopClosest].length < 2 ? currIndexFromTop : jQ.inArray(currTopLeft, iframeBodyChildPosTopLeft);

                    currIframeElm = iframeBodyChild.eq(currIndexFromTop);
                    var oELmType = currIframeElm.prop('tagName');
                    var isTable = (jQ.inArray(oELmType, tableContent) !== -1);

                    if(eCtrlPressed){
                        if(isTable){
                            currIframeElm = iframeBodyChild.eq(currIndx).parent();
                        }
                        else{
                            currIframeElm = iframeBodyChild.eq(currIndexFromTop).parent();
                        }
                    }
                    else{
                        if(isTable){
                            currIframeElm = iframeBodyChild.eq(currIndx);
                        }
                    }

                    var currIframeElm_tagName = currIframeElm[0].tagName;
                    var currIframeElm_className = currIframeElm[0].className;
                    currIframeElmRgtPos = currIframeElm.offset().left + currIframeElm.width();
                    iframeBodyElm.find('.' + sort_highlight_class).remove();
                    if ((uiItemObjOffset.left + 100) > iframeOffsetLeft) {
                        var diff = (currIframeElm.height()) / 2;

                        function majorClassChecker(oElm){
                            var inMajorClass = false;
                            for (var i = 0; i < majorClasses.length; i++) { //Check if currElem is immediate to majorclasses
                                if (oElm.hasClass(majorClasses[i])) {
                                    inMajorClass = true;
                                }
                            }
                            return inMajorClass;
                        }

                        var aNotToBeDroppedIn = ['P','BR','SPAN'];

                        function toPlaceholder(oElm, vType){
                            switch (vType){
                                case 'append':
                                    if(currIframeElm_className !== 'dummy_div' && jQ.inArray(currIframeElm_tagName ,aNotToBeDroppedIn) === -1){
                                        oElm.append('<div class="' + sort_highlight_class + '"><div class="' + elm_highlight + '"><strong>In: </strong>' + currIframeElm_tagName + ' ' + currIframeElm_className + '</div></div>');
                                    }
                                    else{
                                        if(oElm.prop('tagName') === 'BODY'){
                                            oElm.append('<div class="' + sort_highlight_class + '"><div class="' + elm_highlight + '"><strong>In: </strong>BODY</div></div>');
                                        }
                                        else{
                                            oElm.after('<div class="' + sort_highlight_class + '"><div class="' + elm_highlight + '"><strong>After: </strong>' + currIframeElm_tagName + ' ' + currIframeElm_className + '</div></div>');
                                        }

                                    }

                                    break;
                                case 'prepend':
                                    if(jQ.inArray(currIframeElm_tagName ,aNotToBeDroppedIn) === -1){
                                        oElm.prepend('<div class="' + sort_highlight_class + '"><div class="' + elm_highlight + '"><strong>In: </strong>' + currIframeElm_tagName + ' ' + currIframeElm_className + '</div></div>');
                                    }
                                    else{
                                        oElm.after('<div class="' + sort_highlight_class + '"><div class="' + elm_highlight + '"><strong>After: </strong>' + currIframeElm_tagName + ' ' + currIframeElm_className + '</div></div>');
                                    }
                                    break;
                                case 'after':
                                    oElm.after('<div class="' + sort_highlight_class + '"><div class="' + elm_highlight + '"><strong>After: </strong>' + currIframeElm_tagName + ' ' + currIframeElm_className + '</div></div>');
                                    break;
                            }
                        }

                        if (currIndexFromTop === 0 && uiItemOffsetTop < diff) {
                            toPlaceholder(currIframeElm,'prepend');
                        }
                        else{
                            if (jQuery("iframe.cke_wysiwyg_frame").contents().find('body').find('*').length > 1) {
                                if (majorClassChecker(currIframeElm)) {
                                    toPlaceholder(currIframeElm,'prepend');
                                }
                                else if(!eShiftPressed){
                                    toPlaceholder(currIframeElm,'after');
                                }
                                else{
                                    toPlaceholder(currIframeElm,'append');
                                }
                            }
                            else{
                                if (jQuery("iframe.cke_wysiwyg_frame").contents().find('body').find('*').length < 2) {
                                    var oFrameBody = jQuery("iframe.cke_wysiwyg_frame").contents().find('body');
                                    toPlaceholder(oFrameBody,'append');
                                }
                            }
                        }

                        if (jQuery("iframe.cke_wysiwyg_frame").contents().find('body').find('*').length > 4) {
                            trueDrop = onDropValidator(currIframeElm, jQuery_obj(this).text());
                        }
                        else {
                            var vCurrDragItem = (jQuery_obj(this).text()).toLowerCase();
                            trueDrop = jQuery_obj.inArray(vCurrDragItem, dropSetting["*"]) !== -1;
                        }
                        currPatternDropped = jQuery_obj.trim(jQuery_obj(this).text());
                    }

                    if (currIframeElmRgtPos <= (uiItemOffsetLeft + 50)) {
                        iframeBodyElm.find('.' + sort_highlight_class).remove();
                        trueDrop = false;
                    }

                    if (jQuery_obj.inArray('*', dropSetting[currIframeElm_tagName]) !== -1) {
                        trueDrop = false;
                    }
                },
                stop: function(evt, ui) {
                    if (!trueDrop) {
                        if (currPatternDropped === 'CHAPTER') {
                            swal({title: "Illegal Drag!",
                                  text: "Chapter already exists. Please delete 'section.chapter', to enable drop of this widget."
                                 });
                        }
                        iframeBodyElm.find('.' + sort_highlight_class).remove();
                    }
                    else {
                        try {
                            var snippet = contentArray[indexOfDragged - 1].pattern,
                                oDroppedOn = iframeBodyElm.find('.' + sort_highlight_class).parent(),
                                oDroppedPosition = iframeBodyElm.find('.' + sort_highlight_class).index();
                            iframeBodyElm.find('.' + sort_highlight_class).replaceWith(snippet);

                            var this_id = guid();

                            (oDroppedOn.children().eq(oDroppedPosition)).attr('id', this_id);

                            //Create TOC tree
                            (oDroppedOn.children().eq(oDroppedPosition)).createTOC(oDroppedOn);
                            //

                            htmlEditor.contentUpdated = true;
                            initiateObj();
                        }
                        catch (exc) {
                            copyHelper.remove();
                        }
                        if (currPatternDropped === 'GADGET' || currPatternDropped === 'VIDEO')
                        {
                            try {
                                htmlEditor.convertFrameToDiv();
                            }
                            catch (exc) {
                            }
                        }

                        htmlEditor.bindIframeDragDrop();
                        htmlEditor.getCurrentPattern();
                        htmlEditor.checkUserAct();

                    }
                    if (iframeBodyElm.find('.chapter').length < 1) {
                        htmlEditor.disableToolbar();
                    }
                    else{
                        htmlEditor.enableToolbar();
                    }

                    if (iframeBodyElm.find('.dummy_div').length > 0 && iframeBodyElm.children().length > 1) {
                        iframeBodyElm.find('.dummy_div').remove();
                    }
                    copyHelper.remove();
                    jQ(overlay_on_drop).addClass(class_to_hide);

                }
            };
            if (drag_class.draggable())
                drag_class.draggable('destroy');
            drag_class.draggable(drag_settings);
        }
        allowDragReInitiate = false;
    }


    if (iframeBodyChild.length < 1) {
        iframeBodyElm.append('<div class="dummy_div"></div>');
        iframeBodyChild = iframeBodyElm.find('*');
        allowDragReInitiate = true;
        createDraggable();
    }
    else {
        iframeBodyChild = iframeBodyElm.find('*');
        allowDragReInitiate = true;
        createDraggable();
    }

    //================================================================
    //  Function that returns closest array index
    //================================================================
    var previousPos = 0;
    var previousIndx = 0;
    var getClosestIndex = function(arr, target) {
        if(target < previousPos || target > previousPos){
            if (!(arr) || arr.length == 0)
                return null;

            // If array has only one child so return the first.
            if (arr.length == 1)
                return arr[0];


            for (var i=1; i<arr.length; i++) {
                // As soon as a number bigger than target is found, return the previous
                if (arr[i] > target) {
                    var prev = i-1;
                    previousPos = arr[prev];
                    previousIndx = prev;
                    //Used to get the difference between previous and current index [Commented as it is buggy]
                    //var curr = arr[i]
                    //return Math.abs( prev-target ) < Math.abs( curr-target ) ? prev : curr;
                    return previousIndx;
                }
            }
            // No number in array is bigger so return the last.
            return arr.length-1;
        }
        else{
            return previousIndx;
        }
    };

    //==============================================================
    //  Function that allow elements to be dropped
    //==============================================================

    function onDropValidator(vCurrElm, vDragElm) {
        var vClassName = vCurrElm[0].className,
            vpatternType = vCurrElm.getPattern(),
            vCurrDragItem = vDragElm.toLowerCase(),
            aDropValidate = dropSetting[vpatternType.attr('class')];

        return jQuery_obj.inArray(vCurrDragItem, dropSetting["*"]) !== -1 && jQ("iframe.cke_wysiwyg_frame").contents().find('body').find('.' + vCurrDragItem).length > 0 ? false : jQuery_obj.inArray(vCurrDragItem, aDropValidate) !== -1 ? false : jQuery_obj.inArray('*', aDropValidate) !== -1 ? false : true;
    }
    ;

    //==============================================================
    //  Function that returns pattern type
    //==============================================================

    jQuery_obj.fn.getPattern = function() {
        var oThisElm = this,
            oThisClass = oThisElm.attr('class'),
            aPatternList = htmlEditor.topLevelPatternClass,
            vGotPattern = jQuery_obj.inArray(oThisClass, aPatternList) === -1,
            vIsNotBody = (oThisElm.prop('tagName').toLowerCase()) !== 'body';

        while (vGotPattern && vIsNotBody) {
            oThisElm = oThisElm.parent();
            oThisClass = oThisElm.attr('class');
            vGotPattern = jQuery_obj.inArray(oThisClass, aPatternList) === -1;
        }
        return oThisElm;
    };
}


