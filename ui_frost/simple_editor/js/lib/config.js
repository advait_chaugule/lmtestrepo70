/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function(config) {

    // %REMOVE_START%
    // The configuration options below are needed when running CKEditor from source files.
    config.plugins = 'dialogui,dialog,about,a11yhelp,basicstyles,blockquote,clipboard,panel,floatpanel,menu,contextmenu,resize,button,toolbar,elementspath,enterkey,entities,popup,find,filebrowser,floatingspace,listblock,liststyle,richcombo,format,horizontalrule,htmlwriter,wysiwygarea,indentblock,indentlist,fakeobjects,link,list,maximize,pastetext,pastefromword,removeformat,showborders,sourcearea,specialchar,menubutton,scayt,stylescombo,tab,table,tabletools,undo,wsc,lineutils,widget,justify,mathjax,codemirror,customimage,splitview';
    config.skin = 'moono';
    config.allowedContent = true;
    config.title = false;
    config.extraAllowedContent = 'section article header nav aside[*]';
    // %REMOVE_END%

    // Define changes to default configuration here.
    // For complete reference see:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config

    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
        {name: 'styles'},
        {name: 'basicstyles', groups: ['basicstyles']},
        {name: 'paragraph', groups: ['list', 'bidi', 'blocks']},
        {name: 'insert'},
        {name: 'links'},
        {name: 'tools'},
        {name: 'clipboard', groups: ['clipboard', 'undo']},
//		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
//
//		{ name: 'forms' },
        {name: 'find'},
        {name: 'document', groups: ['mode', 'document', 'doctools']},
//        {name: 'others'}, //for the cresteticket plugin
//		'/',

//		{ name: 'colors' },
//		{ name: 'about' }
    ];

    config.codemirror = {
        // Set this to the theme you wish to use (codemirror themes)
        theme: 'neat',
        // Whether or not you want to show line numbers
        lineNumbers: true,
        // Whether or not you want to use line wrapping
        lineWrapping: true,
        // Whether or not you want to highlight matching braces
        matchBrackets: true,
        // Whether or not you want tags to automatically close themselves
        autoCloseTags: true,
        // Whether or not you want Brackets to automatically close themselves
        autoCloseBrackets: true,
        // Whether or not to enable search tools, CTRL+F (Find), CTRL+SHIFT+F (Replace), CTRL+SHIFT+R (Replace All), CTRL+G (Find Next), CTRL+SHIFT+G (Find Previous)
        enableSearchTools: true,
        // Whether or not you wish to enable code folding (requires 'lineNumbers' to be set to 'true')
        enableCodeFolding: true,
        // Whether or not to enable code formatting
        enableCodeFormatting: true,
        // Whether or not to automatically format code should be done when the editor is loaded
        autoFormatOnStart: true,
        // Whether or not to automatically format code should be done every time the source view is opened
        autoFormatOnModeChange: true,
        // Whether or not to automatically format code which has just been uncommented
        autoFormatOnUncomment: true,
        // Define the language specific mode 'htmlmixed' for html including (css, xml, javascript), 'application/x-httpd-php' for php mode including html, or 'text/javascript' for using java script only
        mode: 'htmlmixed',
        // Whether or not to show the search Code button on the toolbar
        showSearchButton: true,
        // Whether or not to show Trailing Spaces
        showTrailingSpace: true,
        // Whether or not to highlight all matches of current word/selection
        highlightMatches: true,
        // Whether or not to show the format button on the toolbar
        showFormatButton: false,
        // Whether or not to show the comment button on the toolbar
        showCommentButton: false,
        // Whether or not to show the uncomment button on the toolbar
        showUncommentButton: false,
        // Whether or not to show the showAutoCompleteButton button on the toolbar
        showAutoCompleteButton: false,
        // Whether or not to highlight the currently active line
        styleActiveLine: false
    };


    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = 'Styles,Cut,Copy,Paste,PasteFromWord,PasteText,SpecialChar,HorizontalRule,Maximize,Strike,Anchor,Link,Find';

    // Set the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';

    // Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced;link:advanced';
    //controlling the dialog box appearing on clicking over the links & images
    CKEDITOR.on('dialogDefinition', function(ev) {
        // Take the dialog name and its definition from the event data.
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        // Check if the definition is from the dialog we're
        // interested in (the 'link' dialog).
        if (dialogName == 'link') {
//        Remove the 'Upload' and 'Advanced' tabs from the 'Link' dialog.
//        dialogDefinition.removeContents('upload');
//        dialogDefinition.removeContents('advanced');

            // Get a reference to the 'Link Info' tab.
            var infoTab = dialogDefinition.getContents('info');
            // Remove unnecessary widgets from the 'Link Info' tab.
            infoTab.remove('linkType');
            infoTab.remove('protocol');
            //infoTab.remove('browse');

            // Get a reference to the "Target" tab and set default to '_blank'
            var targetTab = dialogDefinition.getContents('target');
            var targetField = targetTab.get('linkTargetType');
            targetField['default'] = '_blank';

        } else if (dialogName == 'image') {
//        Remove the 'Link' and 'Advanced' tabs from the 'Image' dialog.
            dialogDefinition.removeContents('Link');
//        dialogDefinition.removeContents('advanced');
            dialogDefinition.title = "Asset Properties";
            // Get a reference to the 'Image Info' tab.
            var infoTab = dialogDefinition.getContents('info');
            // Remove unnecessary widgets/elements from the 'Image Info' tab.
//            infoTab.remove('browse');
            infoTab.remove('txtHSpace');
            infoTab.remove('txtVSpace');
            infoTab.remove('txtBorder');
//            infoTab.remove('txtHeight');
//            infoTab.remove('txtWidth');
//            infoTab.remove('ratioLock');
//            infoTab.remove('cmbAlign');
//            infoTab.remove('htmlPreview');

        }
        else if (dialogName == 'table') {
            var infoTab = dialogDefinition.getContents('info');
            var txtWidth = infoTab.get('txtWidth');
            txtWidth['default'] = '100%';
//            infoTab.remove('txtWidth');
//            infoTab.remove('txtHeight');
        }
        else if(dialogName == 'replace'){
            dialogDefinition.removeContents('find');
        }
    });

    config.height = '548px';
    CKEDITOR.config.keystrokes = [[CKEDITOR.CTRL + CKEDITOR.SHIFT + 109 /*COMMA*/, 'subscript'],
        [CKEDITOR.CTRL + CKEDITOR.SHIFT + 107 /*PERIOD*/, 'superscript']];

};
