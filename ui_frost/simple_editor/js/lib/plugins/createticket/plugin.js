CKEDITOR.plugins.add('createticket', {
    icons: 'createticket',
    init: function(editor) {
        editor.addCommand('createIssue', {
            exec: function() {
                if (!jQuery(".ticket-container").is(":visible")) {
                    htmlEditor.createTicketFromEditor();
                }
            }
        });
        editor.ui.addButton('createticket', {
            label: 'Create Issue/Ticket',
            command: 'createIssue'
        });
    }
});