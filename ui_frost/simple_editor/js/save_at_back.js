self.onmessage = function(event) {
    var details = event.data;
    var url = details.url;
    var tobeposted = param(details.data);
    var xhr = new XMLHttpRequest();
    if ('withCredentials' in xhr) {
        xhr.open("POST", url, true);
        xhr.withCredentials = "true";
        xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=utf-8');
        //xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        xhr.send(tobeposted);
        xhr.onreadystatechange = function()
        {
            if (xhr.readyState == 4 && xhr.status == 200)
            {
                postMessage(xhr.responseText);
            }
        }
    }
};

// Function to serialize the object.
var param = function(obj) {
    var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

    for (name in obj) {
        value = obj[name];

        if (value instanceof Array) {
            for (i = 0; i < value.length; ++i) {
                subValue = value[i];
                fullSubName = name + '[' + i + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query += param(innerObj) + '&';
            }
        }
        else if (value instanceof Object) {
            for (subName in value) {
                subValue = value[subName];
                fullSubName = name + '[' + subName + ']';
                innerObj = {};
                innerObj[fullSubName] = subValue;
                query += param(innerObj) + '&';
            }
        }
        else if (value !== undefined && value !== null)
            query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
    }

    return query.length ? query.substr(0, query.length - 1) : query;
};

