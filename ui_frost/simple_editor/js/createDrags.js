htmlEditor.bindIframeDragDrop = function() {
//    createSortOnIframe(editor_patterns, jQuery);  
    var obj = {
        frame: ".cke_wysiwyg_frame",                //Class name of the frame
        draggable:  ".menu-block",                  //Class name of the draggables
        placeholder: "sort_highlight",              //Class of the line to be displayed
        detailClass: "elm_name",                    //Class of the div in which the info about elements are displayed 
        containment: "editor-container5",           //Containment for teh draggables.
        excludeTags: excludetags,                   //Array of items that should not be allowed to be sorted.
        excludeChildrenIn: excludeChildrenIn,       //Array containing elements that should not be sorted.
        patterns: editor_patterns,                  //Object containing the pattern snippets.
        doNotInsertIn: addAfter                     //Array of elements which do not allow appending/ prepending.
    }
    
    if (htmlEditor.permissionTo.dragPattern) {
            createSortable(obj);
        }
    
};