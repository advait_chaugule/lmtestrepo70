//Frost - 16-Oct-2014 Supriyo Chakroborty

var jQuery = $.noConflict();
var htmlEditor = {};
var returnVal = {
    "content": '',
    "leftPanelTree": ''
};
var returnPatterns = {};
////        ======= Function to get the params of various keys =========
String.prototype.getParam = function(str) {
    str = str.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]*" + str + "=([^&#]*)");
    var results = regex.exec(this);
    if (results == null) {
        return "";
    } else {
        return results[1];
    }
}

var str = decodeURIComponent(window.location);
var project_id = str.getParam("project_id");
var node_id = str.getParam("node_id");
var node_title = str.getParam("title");
var node_type_name = str.getParam("node_type_name");
var node_type_id = str.getParam("node_type_id");
var access_token = str.getParam("access_token");
//===============================================================
//var serviceEndPoint = 'http://localhost:8081/';
htmlEditor.PXEmappedAPI = {
    "getContentAPI": serviceEndPoint + 'api/v1/toc-content/' + node_id + '?access_token=' + access_token + '&project_id=' + project_id + '&s3=0',
    "getPatternsAPI": serviceEndPoint + 'api/v1/project-pattern?access_token=' + access_token + '&project_id=' + project_id + '&pattern_status=1001',
    "saveContentAPI": serviceEndPoint + 'api/v1/toc-content/' + node_id + '?access_token=' + access_token,
    "getLeftPaneAPI": serviceEndPoint + 'api/v1/content-left-panel?access_token=' + access_token,
    "getGlossaryAPI": serviceEndPoint + 'api/v1/glossary?access_token=' + access_token
};


htmlEditor.onDocumentReady = function() {
    htmlEditor.getJsonValue();
};

jQuery(window).resize(function () {
    htmlEditor.onWindowResize();
});

htmlEditor.onWindowResize = function(){
    var winHeight = jQuery(document).height()
    var HeaderHeight = jQuery('.header-container').height();                
    jQuery(".pattern-list-container").css("height", (winHeight-HeaderHeight));
    
}

htmlEditor.getJsonValue = function() {
    var queryString = '&project_id=' + project_id;
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.getGlossaryAPI + queryString,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            if (data.status == '200') {

                jQuery("#node_title").html(node_title);
                jQuery("#project_name").html(data.project_name);

                var glossary_list = data.data.glossary_detail;

                var glossaryListString = '';
                for (i = 0; i < glossary_list.length; i++) {
                    glossaryListString += "<dt>" + glossary_list[i].original_term + "</dt><dd>" + glossary_list[i].description + "</dd>";
                }
                /*var winHeight = jQuery(document).height()
                var HeaderHeight = jQuery('.header-container').height();                
                jQuery(".pattern-list-container").css("height", (winHeight-HeaderHeight));
                */
                htmlEditor.onWindowResize();
                jQuery("#glossaryList").html(glossaryListString);
            } else {
                alert("Content not loaded");
            }
        },
        error: function() {
            alert("Content not loaded");
        }
    });
    return returnVal;
};

jQuery(document).ready(function() {
    htmlEditor.onDocumentReady();
    htmlEditor.onWindowResize();  
});

htmlEditor.backToDashboard = function() {
    window.location = clientUrl + '#/edit_toc/' + project_id;
}
htmlEditor.goToGlossary = function() {
    window.location = clientUrl + '#/glossary/' + project_id;
}