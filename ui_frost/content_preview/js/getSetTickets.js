htmlEditor.callTickets = function() {
    var tickets = "";
    jQuery.ajax({
        url: htmlEditor.ticketAPI.getAllReviews,
        type: 'GET',
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            tickets = data.data.ticket;
            htmlEditor.createList(tickets);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal("Unable to Fetch tickets");
        }
    });
}

htmlEditor.createList = function(list) {                                 // Create the accordion containing all the issues.
    var ticketCollection = '<div class="add-new-ticket"><button class="btn btn-default create new-action" data-loading-text="Creating New Issue..." disabled>Add New Issue</button><div class="new-ticket-container"></div></div><div class="comments-wrapper"><div class="panel-group" id="accordionTicket" role="tablist" aria-multiselectable="true">';
    var pre_tickets = list;
    var ticketHeaderId = "";
    var ticketContentId = "";
    if (pre_tickets.length) {
        for (var i = 0; i < pre_tickets.length; i++) {
            ticketHeaderId = "ticket" + i;
            ticketContentId = "collapseTicket" + i;
            var dateResponseString = (pre_tickets[i].created_at).toString();
            var date = (jQuery.format.date(dateResponseString, "MM/dd/yyyy HH:mm"));
            ticketCollection += '<div class="panel panel-default"><div class="panel-heading" role="tab" id="' + ticketHeaderId + '"><div class="panel-title"><a data-ticket-id="' + pre_tickets[i].ticket_display_id + '" data-parent-ticket-id="'+pre_tickets[i].ticket_id+'" class="collapsed" data-toggle="collapse" data-parent="#accordionTicket" href="#' + ticketContentId + '" aria-expanded="true" aria-controls="' + ticketContentId + '"><div class="issue-no">Issue# ' + pre_tickets[i].ticket_display_id + '<p><b>Assigned to: </b><span class="assignee" user-id="' + pre_tickets[i].assigned_to + '">' + pre_tickets[i].assigned_to_username + '</span></p></div></div></a></div><div id="' + ticketContentId + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="' + ticketHeaderId + '"><div class="panel-body"><ul class="comments issueCreated"><li data-comment-id=""><a class="author-name right-of-avatar" href="#">' + pre_tickets[i].created_by_username + '</a><a class="author-date right-of-avatar" href="#">' + date + '</a><p class="comment right-of-avatar"><pre>' + pre_tickets[i].issue_description + '</pre></p><label class="action-link ' + pre_tickets[i].type.toLowerCase() + ' labl">' + pre_tickets[i].type + '</label><label class="action-link status">' + pre_tickets[i].status + '</label></li></ul><button class="btn add-comment new-action" data-loading-text="Creating New Comment...">Leave a comment</button><div class="new-ticket-container"></div><div class="comments-on-ticket"><ul class="comments thread">';
            var comments = pre_tickets[i].ticket_comments;          // Comments on issues raised.
            if (comments.length) {
                for (var j = 0; j < comments.length; j++) {
                    var dateResponseString2 = (comments[j].created_at).toString();
                    var date2 = (jQuery.format.date(dateResponseString2, "MM/dd/yyyy HH:mm"));
                    ticketCollection += '<li data-comment-id="' + comments[j].ticket_comment_id + '"><a class="author-name right-of-avatar" href="#">' + comments[j].created_by_username + '</a><a class="author-date right-of-avatar" href="#">' + date2 + '</a><p class="comment right-of-avatar"><pre>' + comments[j].comment + '</pre></p></li>';
                }
            }
            else {
                ticketCollection += "<li><em> No comments yet</em></li>";
            }
            ticketCollection += '</ul></div></div></div></div>';
        }
    }
    else{
        ticketCollection += '<div class="lm-rochak"><div><span class="fa fa-comments"></span></div><strong>No Comments added Yet!</strong>Click on the button above to add new comment</div>';
    }
    ticketCollection += "</div></div>";
    jQuery(".right-comment .post-comment").html(ticketCollection);
    htmlEditor.bindTicketEvents();
};

htmlEditor.initiateSave = function() {
    var w;
    if (typeof (Worker) !== "undefined") {              // Check if webworker is supported.
        if (typeof (w) == "undefined") {
            w = new Worker("js/savereviewed.js");
        }
        var values = {};
        values.url = htmlEditor.ticketAPI.saveReviewedContent;
        values.data = {content: htmlEditor.getContent(), node_id: node_id, project_id: project_id, node_title: node_title};
        w.postMessage(values);                          // Send data value to the webworker to save it.
        // Check if response needs to be rendered to the user after successful save.
        w.onmessage = function(event) {
            
        };
    } else {
        alert("Sorry! No Web Worker support.");
    }
};

htmlEditor.getContent = function() {
    var mainFrame = jQuery("#preview-frame").contents().find("#content-container");
    mainFrame.find(".highlightClicked").removeClass("highlightClicked");
    mainFrame.find(".active").removeClass("active");
    var chapToBeSaved = mainFrame.find("section").eq(0).prop("outerHTML");  //Send only the chapter section to be saved
    var trialFrame = jQuery("iframe#trial_load").contents();
    trialFrame.find("body").html(chapToBeSaved);
    trialFrame.find(".math-tex").each(function(idx) {               //Convert the mathML view to its native view.
        jQuery(this).html(htmlEditor.eqnList[idx]);
    });
    htmlEditor.enableAnchors(trialFrame);
    var toBeSaved = trialFrame.find("body").prop("outerHTML");
    return toBeSaved;
};

htmlEditor.enableAnchors = function(frame) {
    var preview = jQuery("#preview-frame").contents();
    frame.find("a").each(function(ind) {
        var the_anchor = jQuery(this);
        //Get the value set in its "data-refer" attribute and assign it to href attribute.
        var reference = preview.find("a").eq(ind).data("refer");
        the_anchor.attr("href", reference);
    })
}
