htmlEditor.setPreviewContent = function() {
    var showContent = htmlEditor.stripData();                                   // Returns the body of the content fetched.
    var previewFrame = jQuery('iframe#preview-frame').contents();
    previewFrame.find('#content-container').html(showContent).removeAttr("onload");     //Remove onload if the editor loads.
//    if (!previewFrame.find(".buttonHolder").length) {
//        previewFrame.find('#content-container').append('<div class="buttonHolder"><button type="button" class="btn btn-primary hideBtn" id="newCom"> + </button></div>');
//    }
    jQuery('#project_name').empty().html(returnVal.project_name);
    jQuery('#node_title').empty().html(returnVal.node_title);
    htmlEditor.disableAnchors(previewFrame);
    htmlEditor.includeCSS();
    htmlEditor.getMathEq();
    htmlEditor.runMathML();
    htmlEditor.bindEvents();
//    htmlEditor.changeOffsetOnResize();
    htmlEditor.viewPort(0);
    jQuery(".loading-g").hide();
};

htmlEditor.stripData = function() {
    var new_data = returnVal.content;
    jQuery("iframe#trial_load").contents().find("html").html(new_data);
    var chap = jQuery("iframe#trial_load").contents().find("body").html();
    new_data = chap;
    return new_data;
};

htmlEditor.eqnList = [];

htmlEditor.getMathEq = function() {                         // Gets and saves all the MathML equation in its native form in eqnList.
    var equations = jQuery('iframe#preview-frame').contents().find('#content-container span.math-tex');
    equations.each(function() {
        htmlEditor.eqnList.push(jQuery(this).html());
    });
}

htmlEditor.runMathML = function() {                         // Function that converts native equation to eqn form.
    var win = document.getElementById("preview-frame").contentWindow;
    win.postMessage("true", "*");
}

htmlEditor.disableAnchors = function(frame) {
    frame.find("a").each(function() {
        var the_anchor = jQuery(this);
        // Reassign the actual link to another attribute called 'data-refer'.
        var link = the_anchor.attr("href");
        the_anchor.data("refer", link);
        //Remove the "href" attribute from the a tag.
        the_anchor.removeAttr("href");
    })
};