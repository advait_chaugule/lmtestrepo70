
htmlEditor.bindTicketEvents = function() {
    try {       //Check if a user is permitted to create replies against tickets.
        if (!htmlEditor.userProjectPermission["ticket.comment.create"]["grant"]) {
            jQuery(".add-comment.new-action").attr("disabled", true);
        }
    }
    catch (exc) {

    }
    // Function to create the text-box to input comments and various values.
    function createCommentBox(type) {
        if (type === "issue") {
            var _HTML = '<div class="inputComment create-issue"><div class="comment-form active"><p class="author-name">You</p><textarea class="comment-box right-of-avatar" placeholder="Add an Issue..."></textarea><div class="clearfix"></div><div class="actions"><select class="readytotest">';
        }
        else if (type === "comment") {
            var _HTML = '<div class="inputComment create-issue"><div class="comment-form active"><p class="author-name">You</p><textarea class="comment-box right-of-avatar" placeholder="Add a comment..."></textarea><div class="clearfix"></div><div class="actions"><select class="readytotest">';
        }
        jQuery.each(htmlEditor.infoObj.statusList, function(k, v) {
            _HTML += '<option value="' + k + '">' + v + '</option>';
        });
        _HTML += '</select><select class="readytotest2">';
        for (var i = 0; i < htmlEditor.infoObj.userList.length; i++) {
            _HTML += '<option value="' + htmlEditor.infoObj.userList[i].user_id + '">' + htmlEditor.infoObj.userList[i].created_by_name + '</option>';
        }
        _HTML += '</select><select class="readytotest1">';
        jQuery.each(htmlEditor.infoObj.statusTypes, function(k, v) {
            _HTML += '<option value="' + k + '">' + v + '</option>';
        })
        _HTML += '</select><div class="clearfix"></div></div><div class="submit-action actions"><a href="#" class="action-link post active">Submit</a><a href="#" class="action-link cancel">Cancel</a><div class="clearfix"></div></div></div></div>';
        return _HTML;
    }

    // Functionality on clicking on the Add Comment button, to create a new ticket.

    jQuery(".post-comment .add-new-ticket").find(".create").off().on("click", function() {
        if (htmlEditor.userProjectPermission["ticket.create"]["grant"]) {
            var textBox = createCommentBox("issue");
            jQuery(this).attr("disabled", true);
            var url_to_create_ticket = htmlEditor.ticketAPI.createTicketAPI;
            jQuery(this).parents(".add-new-ticket").find(".new-ticket-container").html(textBox);
            htmlEditor.setCommentPosting(url_to_create_ticket);
            htmlEditor.cancelEvent();
        }
        else {
            swal("You are not permitted to create issues. Please contact Admin.")
        }
    });

    htmlEditor.prevDetails = {};
    //Add an identifier to the active tab in the accordion.
    jQuery(".post-comment").find(".comments-wrapper").find("#accordionTicket").find("a").off().on("click", function() {
        var ticketNum = "";
        jQuery(this).parents("#accordionTicket").find("a").removeClass("activatedTab");
        jQuery(".action-link.cancel").trigger("click");                 // Close all the textareas.
        jQuery("button.create").attr("disabled", true);             // Disable the create new ticket button.
        jQuery("#preview-frame").contents().find("body").find(".highlightClicked").removeClass("highlightClicked"); // Remove Other clicked classes.

        if (jQuery(this).hasClass("collapsed")) {
            ticketNum = jQuery(this).data("ticket-id");
            jQuery(this).addClass("activatedTab");
            //Functionality on clicking on the Add comment button to comment on an existing ticket.
            jQuery(this).parents(".panel").find(".panel-body").find(".add-comment").off().on("click", function() {
                if (htmlEditor.userProjectPermission["ticket.comment.create"]["grant"]) {
                    var textBox = createCommentBox("comment");
                    var selected_issue = jQuery(this);
                    htmlEditor.prevDetails.prev_user = selected_issue.parents(".panel").find(".panel-heading").find(".assignee").attr("user-id");
                    selected_issue.attr("disabled", true);
                    htmlEditor.prevDetails.prev_type = selected_issue.siblings("ul.issueCreated").find(".action-link.labl").text();
                    htmlEditor.prevDetails.prev_status = selected_issue.siblings("ul.issueCreated").find(".action-link.status").text();
                    var url_to_create_comment = htmlEditor.ticketAPI.createCommentAPI;
                    selected_issue.parents(".panel-body").find(".new-ticket-container").html(textBox).find(".readytotest2").val(htmlEditor.prevDetails.prev_user);           //Keep the current assignee name selected in the dropDown.
                    selected_issue.parents(".panel-body").find(".new-ticket-container").find(".readytotest1").val(htmlEditor.prevDetails.prev_type);
                    htmlEditor.setCommentPosting(url_to_create_comment);
                    htmlEditor.cancelEvent();
                }
                else {
                    swal("You don't have the permission to reply to this ticket. Please contact Admin.")
                }
            });
        }
        if (ticketNum) {
            highlightElement(ticketNum);
        }
        function highlightElement(ticket) {
            var contentIn = jQuery("#preview-frame").contents().find("body");
            contentIn.find(".ticketRaised").removeClass("ticketRaised");
            var ticketAgainst = contentIn.find("*[ticket-id=" + ticket + "]");  // To check if the element having ticket exists.
            if (ticketAgainst.length) {
                ticketAgainst.addClass("ticketRaised");
                var tobescrolled = ticketAgainst.offset().top - 20;
                contentIn.animate({scrollTop: tobescrolled}, 1000);
            }
            else {
                swal("Element against the issue was removed.");
            }
        }

    });

    // Functionality on cancelling a ticket creation process.
};

htmlEditor.cancelEvent = function() {
    jQuery(".action-link.cancel").off().on("click", function() {
        jQuery(this).parents(".new-ticket-container").empty().siblings("button.new-action").attr("disabled", false);
    })
}

htmlEditor.setCommentPosting = function(url) {
    jQuery(".action-link.post").off().on("click", function() {
        var flag = 0;                                                         // To check if the post is comment or new ticket.
        var containerBox = jQuery(this).parents(".inputComment");
        var issue_desc = containerBox.find("textarea.comment-box").val().trim();
        var commentTag = (issue_desc).replace(/</g,'&lt;').replace(/>/g,'&gt;');
        var assignee = containerBox.find("select.readytotest2:visible").val();
        var issue_type = containerBox.find("select.readytotest1:visible").val();
        var issue_status = containerBox.find("select.readytotest:visible").val();
        var parent_id = "";
        issue_type === undefined ? "NOCHANGE" : issue_type;
        var sendData = {};
        var username = htmlEditor.getUserName(assignee);
        var loadingBtn = "";
        if (containerBox.parents(".panel").length) {                          // If post is Hit from inside an accordion.
            if (htmlEditor.prevDetails.prev_status === issue_status && htmlEditor.prevDetails.prev_user === assignee && htmlEditor.prevDetails.prev_type === issue_type && !issue_desc.length) {
                swal("You have not changed anything. The comment cannot be posted.");
                return false;
            }
            else {
                if (!issue_desc.length) {
                    issue_desc = "CHANGED STATUS/ TYPE/ ASSIGNEE";
                }
                parent_id = jQuery("a.activatedTab").data("parent-ticket-id");            //Get the ticket id.
                sendData = {
                    "project_id": project_id, 
                    "object_id": node_id, 
                    "comment": commentTag, 
                    "assigned_to": assignee, 
                    "status": issue_status, 
                    "parent_ticket_id": parent_id, 
                    "type": issue_type
                };
                jQuery(this).parents(".new-ticket-container").siblings("button.new-action").attr("disabled", false);
                loadingBtn = "button.add-comment";
//            jQuery("button.add-comment").button('loading');
            }
        }
        else {
            if (issue_desc.length) {
                loadingBtn = "button.create";
//            jQuery("button.create").button('loading');
                flag = 1;
                sendData = {"project_id": project_id, "object_id": node_id, "title": "title", "issue_description": commentTag, "assigned_to": assignee, "status": issue_status, "type": issue_type};
            }
            else {
                swal("Issue cannot be empty. Please add some text.");
                return false;
            }
        }
        jQuery(loadingBtn).button('loading');
        jQuery.ajax({
            url: url,
            type: 'POST',
            data: sendData,
            async: true,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data, textStatus, jqXHR) {
                var response = data.data[0];
                var ticketList = jQuery(".right-comment").find(".comments-wrapper").find("#accordionTicket");
                if (flag) {                                                 // If a new ticket is raised.
                    var ticket_display_id = response.ticket_display_id;
                    //var ticket_id = response.ticket_id;
                    var parent_ticket_id = response.ticket_id;
                    var lengthoftickets = ticketList.find(".panel").length;
                    var dateResponseString = response.created_at.toString();
                    var date = (jQuery.format.date(dateResponseString, "MM/dd/yyyy HH:mm"));
                    var commentTag = (response.issue_description).replace(/</g,'&lt;').replace(/>/g,'&gt;');
                    var newTicket = '<div class="panel panel-default"><div class="panel-heading" role="tab" id="ticket' + lengthoftickets + '"><div class="panel-title"><a data-ticket-id="' + ticket_display_id + '" data-parent-ticket-id="'+parent_ticket_id+'" class="collapsed" data-toggle="collapse" data-parent="#accordionTicket" href="#collapseTicket' + lengthoftickets + '" aria-expanded="true" aria-controls="collapseTicket' + lengthoftickets + '"><div class="issue-no">Issue# ' + ticket_display_id + '<p><b>Assigned to: </b><span class="assignee" user-id=' + response.assigned_to + '>' + response.assigned_to_username + '</span></p></div></a></div></div><div id="collapseTicket' + lengthoftickets + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ticket' + lengthoftickets + '"><div class="panel-body"><ul class="comments issueCreated"><li data-comment-id=""><a class="author-name right-of-avatar" href="#">' + response.created_by_username + '</a><a class="author-date right-of-avatar" href="#">' + date + '</a><p class="comment right-of-avatar"><pre>' + commentTag + '</pre></p><label class="action-link ' + response.type.toLowerCase() + ' labl">' + response.type + '</label><label class="action-link status">' + response.status + '</label></li></ul><button class="btn add-comment new-action">Leave a comment</button><div class="new-ticket-container"></div><div class="comments-on-ticket"><ul class="comments thread"><li><em> No comments yet</em></li></ul></div></div></div></div>';
//                    ticketList.prepend(newTicket);
                    if (ticketList.find(".panel").length) {       //Check if comments already exists.
                        ticketList.prepend(newTicket);
                    }
                    else {
                        ticketList.html(newTicket);
                    }
                    jQuery("#preview-frame").contents().find("body").find(".highlightClicked").attr("ticket-id", ticket_display_id).removeClass(".highlightClicked");
                    jQuery("button.create").button('reset');
                    htmlEditor.initiateSave();
                }
                else {                                          // If a Comment is posted.
                    var listedComments = ticketList.find(".panel-collapse.collapse.in").find(".comments.thread");
                    var dateResponseString1 = response.created_at.toString();
                    var date1 = (jQuery.format.date(dateResponseString1, "MM/dd/yyyy HH:mm"));
                     var commentTag = (response.comment).replace(/</g,'&lt;').replace(/>/g,'&gt;');
                    var newCom = '<li data-comment-id="' + response.ticket_comment_id + '"><a class="author-name right-of-avatar" href="#">' + response.created_by_username + '</a><a class="author-date right-of-avatar" href="#">' + date1 + '</a><p class="comment right-of-avatar"><pre>' + commentTag + '</pre></p></li>';
                    if (listedComments.find("li").data("comment-id")) {
                        listedComments.prepend(newCom);
                    }
                    else {
                        listedComments.html(newCom);
                    }
                    ticketList.find(".panel-collapse.collapse.in").find(".comments.issueCreated").find(".action-link.status").text(issue_status);
                    var prevClass = ticketList.find(".panel-collapse.collapse.in").find(".comments.issueCreated").find(".action-link.labl").text().toLowerCase();
                    ticketList.find(".panel-collapse.collapse.in").find(".comments.issueCreated").find(".action-link.labl").removeClass(prevClass).text(issue_type).addClass(issue_type.toLowerCase());
                    ticketList.find(".activatedTab").find(".assignee").attr("user-id", assignee).text(username);
                    jQuery("button.add-comment").button("reset");
                }
                htmlEditor.bindTicketEvents();
            },
            error: function() {
                swal("Unable to create ticket");
                jQuery("button.new-action").button("reset");
            }
        });
        containerBox.remove();
    });
}