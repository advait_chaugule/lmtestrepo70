////        ======= Function to get the params of various keys =========
String.prototype.getParam = function(str) {
    str = str.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]*" + str + "=([^&#]*)");
    var results = regex.exec(this);
    if (results == null) {
        return "";
    } else {
        return results[1];
    }
};

var str = decodeURIComponent(window.location);
var project_id = str.getParam("project_id");
var project_type_id = str.getParam("project_type_id");
var node_id = str.getParam("node_id");
var node_title = str.getParam("title");
var node_type_name = str.getParam("node_type_name");
var node_type_id = str.getParam("node_type_id");
var access_token = str.getParam("access_token");
var version_id = str.getParam("version_id");

//===============================================================
htmlEditor.ticketAPI = {
    "getUserProjectPermissionAPI": serviceEndPoint + 'api/v1/projects/project_id/' + project_id + '?access_token=' + access_token,
    "getContentAPI": serviceEndPoint + 'api/v1/object-revision/object_id/' + node_id + '/project_id/' + project_id + '?access_token=' + access_token + '&version_id=' + version_id,
    "getUsersAPI": serviceEndPoint + 'api/v1/project-users/project_id/' + project_id + '?access_token=' + access_token,
    "getAllStatus": serviceEndPoint + 'api/v1/ticket-types-status?access_token=' + access_token,
    "createTicketAPI": serviceEndPoint + 'api/v1/ticket?access_token=' + access_token,
    "createCommentAPI": serviceEndPoint + 'api/v1/ticket-reply?access_token=' + access_token,
    "getTicketDetailsAPI": serviceEndPoint + 'api/v1/ticket/ticket_id/', //Imcomplete API, remaining part concatenated in the main script
    "saveReviewedContent": serviceEndPoint + 'api/v1/object-revision/object_id/' + node_id + '/project_id/' + project_id + '?access_token=' + access_token,
    "getAllReviews": serviceEndPoint + "api/v1/ticket-all/project_id/" + project_id + "/is_global/0?access_token=" + access_token + "&object_id=" + node_id
}