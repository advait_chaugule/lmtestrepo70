htmlEditor.onWindowResize = function() {
    var win_height = jQuery(window).height() - 0;
    var win_width = jQuery(window).width() - 0;
    var header_height = jQuery('.header-container').outerHeight();
    var content_height = win_height - header_height;          //set editor height on window resize.
    var right_width = jQuery(".right-comment").outerWidth();
    var iframeElmContainer = jQuery('.iframeContainer');
    iframeElmContainer.width(win_width - right_width);
    iframeElmContainer.height(content_height);
    jQuery(".right-comment").height(content_height);
//    htmlEditor.changeOffsetOnResize();
//    htmlEditor.setCommentPanel();
};

// Changes the position of the Existing Ticket Buttons as per the new viewport
htmlEditor.changeOffsetOnResize = function() {
    var referenceElem = jQuery("#preview-frame").contents().find(".highlightClicked");
    if (referenceElem.length) {
        htmlEditor.setNewBtnPosition(referenceElem);
    }
    htmlEditor.setTicketBtnPosition();
}

// Function to position the button when creating a new Ticket.
htmlEditor.setNewBtnPosition = function(ref) {
    var posTop = jQuery(ref).offset().top;
    var posRight = 0;
    var cont = jQuery("iframe#preview-frame").contents();
    cont.find("#newCom").css("top", posTop);
    var comCounter = 0;
    cont.find(".ticketRaised").each(function() {
        var thisTop = jQuery(this).offset().top;
        var tickt = jQuery(this).attr("ticket-id");
        var bufferedUpTop = thisTop + cont.find("#newCom").height();
        var bufferedDwnTop = thisTop - cont.find("#newCom").height();
        if (posTop > bufferedDwnTop && posTop < bufferedUpTop) {
            comCounter++;
            posRight += cont.find("#" + tickt).outerWidth();
        }
    });
    cont.find("#newCom").css("right", posRight);
};

// Function to set Buttons having ticket details.
htmlEditor.setTicketBtnPosition = function() {
    var offsetHolder = [];
    var contentHolder = jQuery("iframe#preview-frame").contents();
    contentHolder.find("body#content-container *").each(function() {
        var hasTicket = jQuery(this).attr("ticket-id");
        if (hasTicket !== undefined) {
            jQuery(this).addClass("ticketRaised");
        }
    });
    var counter = 0;
    contentHolder.find(".ticketRaised").each(function() {
        var offsetElem = jQuery(this).offset();
        var topPos = offsetElem.top;
        var rtPos = 0;
        var numOfElem = offsetHolder.length;
        if (numOfElem) {
            var lastElemPosTop = offsetHolder[numOfElem - 1].top;
            var btnHt = contentHolder.find("#newCom").height();
            var btnWidth = contentHolder.find("#newCom").outerWidth();
            var lowLimit = lastElemPosTop - btnHt;
            var upLimit = lastElemPosTop + btnHt;
            if (topPos > lowLimit && topPos < upLimit) {
                counter++;
                rtPos = btnWidth * counter;
            }
            else {
                counter = 0;
            }
        }
        offsetHolder.push(offsetElem);
        var mapper = jQuery(this).attr("ticket-id");
        jQuery("iframe#preview-frame").contents().find(".commentBtn#" + mapper).css({"top": topPos, "right": rtPos});
    });
    var offsetSortedArray = offsetHolder.sort();
    var repCount = 0;
    for (var i = 0; i < offsetSortedArray.length; i++) {
        if (offsetSortedArray[i] === offsetSortedArray[i + 1]) {
            repCount++;
        }
        else {
            repCount = 0;
        }
    }
}

// Show contents in various veiwports.
htmlEditor.viewPort = function(val) {
    switch (val) {
        case 0:
            var height = "100%";
            var width = "100%";
            htmlEditor.setViewPort(height, width);
            break;
        case 1:
            var height = 485;
            var width = 800;
            htmlEditor.setViewPort(height, width);
            break;
        case 2:
            var height = 485;
            var width = 646;
            htmlEditor.setViewPort(height, width);
            break;
        case 3:
            var height = 485;
            var width = 278;
            htmlEditor.setViewPort(height, width);
            break;
        default:
    }
}

htmlEditor.setViewPort = function(h, w) {                           // Set frames width and height.
    jQuery("#preview-frame").width(w);
    jQuery("#preview-frame").height(h);
//    htmlEditor.changeOffsetOnResize();
}