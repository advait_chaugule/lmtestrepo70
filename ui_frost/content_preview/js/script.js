//Frost

var jQuery = $.noConflict();
var htmlEditor = {};
//================================================================
var returnVal = {
    "content": '',
    "img_urls": '',
    "theme_location": ''
};

htmlEditor.infoObj = {};                                                //Object to store values for users & ticket types.

htmlEditor.onDocumentReady = function () {
    var win_height = jQuery(window).height() - 0;
    var win_width = jQuery(window).width() - 0;
    var header_height = jQuery('.header-container').outerHeight();
    var content_height = win_height - header_height;          //set preview height on window resize.
    var right_width = jQuery(".right-comment").outerWidth();
    var iframeElmContainer = jQuery('.iframeContainer');
    iframeElmContainer.width(win_width - right_width);
    iframeElmContainer.height(content_height);
    jQuery(".right-comment").height(content_height);
    jQuery.when(htmlEditor.currentUserPermissions()).done(function () {      // Fetches the current user details & permissions.        
        htmlEditor.getJsonValue();                                          // Fetches content to be reviewed.
        htmlEditor.getUsers();                                              // Fetches all the users associated with the project.
        htmlEditor.getStatus();                                             // Fetches the issue types & status for the project.
    });
};

//        ================================Resize handler function ends ===================================

htmlEditor.userProjectPermission = {};

htmlEditor.currentUserPermissions = function () {
    return jQuery.ajax({
        url: htmlEditor.ticketAPI.getUserProjectPermissionAPI,
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {            
            htmlEditor.userProjectPermission = data.data.project_permissions;   // Save permission object locally.
            htmlEditor.cssFiles = data.data.theme_location;            
        }
    })
}

htmlEditor.getJsonValue = function () {
    jQuery.ajax({
        url: htmlEditor.ticketAPI.getContentAPI,
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data) {
            if (data.status == '200' && data.data !== null) {
                returnVal = {
                    "content": data.data.content,
                    "node_title": data.data.node_title,
                    "project_name": data.data.object_name,
                    "img_urls": data.data.img_urls,
                    "theme_location": data.data.theme_location,
                };
                htmlEditor.setPreviewContent();                             // Set contents to be reviewed.
                htmlEditor.editorTOC = data.data.editor_toc;
                tocInit();
            } else if (data.data.content === null) {                        // In case no content is found, inform user.
                htmlEditor.bindEvents();
                swal("No content to review.");
            } else {
                swal("Content not loaded");
            }
        },
        error: function () {
            swal("Content not loaded");
        }
    });
    return returnVal;
};

htmlEditor.includeCSS = function () {
    var styles = htmlEditor.cssFiles;
    var cssHead = '';
    try {
        for (var i = 0; i < styles.length; i++) {
            cssHead += '<link rel="stylesheet" type="text/css" href=' + styles[i] + '>';
        }
    }
    catch (exc) {

    }
    jQuery("#preview-frame").contents().find("head").append(cssHead);
}

//          ============Check if text selection has been made to be reviewed. ===============

htmlEditor.checkSelection = function () {
    var iframe = document.getElementById("preview-frame");
    htmlEditor.getSelectedText(iframe);
};

htmlEditor.getSelectedText = function (iframe) {
    var iWindow = iframe.contentWindow;
    var iDocument = iWindow.document;
    var selection = iDocument.getSelection();
    var selectedText = selection.toString();
    if (selectedText.length) {
        var selectedRange = selection.getRangeAt(0);
        var selectedPart = selectedRange.extractContents();
        var newElement = document.createElement("div");
        var innerDiv = document.createElement("div");
        var infoDiv = document.createElement("div");
        var commentField = document.createElement("textarea");
        var statusField = document.createElement("div");
        newElement.className = "comment-container";
        innerDiv.className = "comment-subcontainer";
        infoDiv.className = "comment-info";
        commentField.className = "comment-panel";
        statusField.className = "comment-status";
        newElement.appendChild(innerDiv);
        innerDiv.appendChild(infoDiv);
        innerDiv.appendChild(commentField);
        innerDiv.appendChild(statusField);
        var updateWithNode = document.createElement("span");
        updateWithNode.className = "ticketRaised";
        updateWithNode.appendChild(selectedPart);
        selectedRange.insertNode(newElement);
        selectedRange.insertNode(updateWithNode);
    }
    else {
        swal("No selection.")
    }
};

//===============================================================================================

htmlEditor.bindEvents = function () {
    var iframeContent = jQuery("iframe#preview-frame").contents();

    htmlEditor.callTickets();

    // Change the viewports //
    jQuery(".resize-view li").off().on("click", function () {
        htmlEditor.viewPort(jQuery(this).index());
    })
    // Functionality to create tickets. 
    // Check if the current user is allowed to create ticket.
    if (!jQuery.isEmptyObject(htmlEditor.userProjectPermission) && htmlEditor.userProjectPermission["ticket.create"]["grant"]) {
        iframeContent.find("body *").off().on("click", function (e) {
            iframeContent.find(".highlightClicked").removeClass("highlightClicked");
            iframeContent.find(".ticketRaised").removeClass("ticketRaised");
            try {
                jQuery(".activatedTab").trigger("click");
            }
            catch (exc) {

            }
            var tgt = e.target;
            if (!tgt.hasAttribute("ticket-id")) {
                jQuery(".right-comment .post-comment").find(".create").attr("disabled", false);
//                if(jQuery(tgt).parents("math").length){
//                    tgt= jQuery(tgt).parents("math")[0];      //Check for mathml equations
//                }
                jQuery(tgt).addClass("highlightClicked");
            }
            else {
                jQuery(".right-comment .post-comment").find(".create").attr("disabled", true);
            }
        });
    }
};

htmlEditor.getUsers = function () {                                      // Fetch all users associeated with the current project.
    return jQuery.ajax({
        url: htmlEditor.ticketAPI.getUsersAPI,
        type: 'GET',
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            var listOfUsers = data.data;
            htmlEditor.infoObj.userList = listOfUsers;
        },
        error: function () {
            swal("There was an error fetching the users list.")
        }
    })
};

htmlEditor.getUserName = function (user_id) {
    var user_name = "";
    for (var i = 0; i < htmlEditor.infoObj.userList.length; i++) {
        if (user_id == htmlEditor.infoObj.userList[i].user_id) {
            user_name = htmlEditor.infoObj.userList[i].username;
        }
    }
    return  user_name;
}

htmlEditor.getStatus = function () {                                     // Fetch all the status & types for the current project.
    return jQuery.ajax({
        url: htmlEditor.ticketAPI.getAllStatus,
        type: 'GET',
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data, textStatus, jqXHR) {
            var listOfStatus = data.data.ticket_status;
            var listOfStatusTypes = data.data.ticket_issue_types;
            htmlEditor.infoObj.statusList = listOfStatus;
            htmlEditor.infoObj.statusTypes = listOfStatusTypes;
        },
        error: function () {
            swal("There was an error fetching the Ticket status.")
        }
    })
}

jQuery(document).ready(function () {
    htmlEditor.onDocumentReady();
});

jQuery(window).resize(function () {
    htmlEditor.onWindowResize();
});

htmlEditor.backToDashboard = function () {
    window.location = clientUrl + '#/edit_toc/' + project_id;
}

//        =======================Patterns fetched and stored as returnPatterns for further usability==============

htmlEditor.createLoader = function () {
    if (!jQuery(".editor-loader").length) {                             //Check if loader already exists.
        jQuery("body").append('<div class="editor-loader"></div>');
    }
};

//      ========================Destroy the loading screen on successful data load================================
htmlEditor.destroyLoader = function () {
    jQuery('.editor-loader').remove();
    jQuery('body').css('overflow', '');
};
// 