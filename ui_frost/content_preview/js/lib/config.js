/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here.
    // For complete reference see:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config
    //Support for HTML5 tags
    config.allowedContent = true;
    config.extraAllowedContent = 'section article header nav aside[*]';
    config.extraPlugins = 'blockimagepaste';
    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbar = [
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup'], items: ['Bold', 'Italic', 'Underline', 'Strike','Superscript', 'Subscript']},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList', '-', 'Blockquote','-','Outdent', 'Indent','-','JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']},
//        {name: 'links', items: ['Link']},
        {name: 'insert', items: ['Image', 'Table']},
//        {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
        {name: 'tools', items: ['Maximize']},
        {name: 'clipboard', groups: ['clipboard', 'undo'], items: ['Undo', 'Redo']},
        {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source']},
    ];

//	config.toolbarGroups = [
////		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
////		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
//                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
//		{ name: 'links' },
//		{ name: 'insert' },
////		{ name: 'forms' },
////		{ name: 'tools' },
//		{ name: 'others' },
////		'/',
//		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
//		{ name: 'styles' },
//                { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
////		{ name: 'colors' },
////		{ name: 'about' }
//	];

    // Remove some buttons provided by the standard plugins, which are
    // not needed in the Standard(s) toolbar.
    config.removeButtons = 'StrikeThrough,Unlink,Anchor,Image';

    // Set the most common block elements.
    //config.format_tags = 'p;h1;h2;h3;pre;h4;section;article;nav';
    config.format_tags = 'p;h1;h2;h3;h4;pre';

    // Simplify the dialog windows.
    config.removeDialogTabs = 'image:advanced';

    //controlling the dialog box appearing on clicking over the links & images
    CKEDITOR.on('dialogDefinition', function (ev) {
        // Take the dialog name and its definition from the event data.
        var dialogName = ev.data.name;
        var dialogDefinition = ev.data.definition;

        // Check if the definition is from the dialog we're
        // interested in (the 'link' dialog).
        if (dialogName == 'link') {
//        Remove the 'Upload' and 'Advanced' tabs from the 'Link' dialog.
//        dialogDefinition.removeContents('upload');
//        dialogDefinition.removeContents('advanced');

            // Get a reference to the 'Link Info' tab.
            var infoTab = dialogDefinition.getContents('info');
            // Remove unnecessary widgets from the 'Link Info' tab.
            infoTab.remove('linkType');
            infoTab.remove('protocol');
            //infoTab.remove('browse');

            // Get a reference to the "Target" tab and set default to '_blank'
            var targetTab = dialogDefinition.getContents('target');
            var targetField = targetTab.get('linkTargetType');
            targetField['default'] = '_blank';

        } else if (dialogName == 'image') {
//        Remove the 'Link' and 'Advanced' tabs from the 'Image' dialog.
            dialogDefinition.removeContents('Link');
//        dialogDefinition.removeContents('advanced');
            dialogDefinition.title = "Asset Properties";
            // Get a reference to the 'Image Info' tab.
            var infoTab = dialogDefinition.getContents('info');
            // Remove unnecessary widgets/elements from the 'Image Info' tab.
//            infoTab.remove('browse');
            infoTab.remove('txtHSpace');
            infoTab.remove('txtVSpace');
            infoTab.remove('txtBorder');
//            infoTab.remove('txtHeight');
//            infoTab.remove('txtWidth');
//            infoTab.remove('ratioLock');
//            infoTab.remove('cmbAlign');
//            infoTab.remove('htmlPreview');

        }
    });

    config.height = '548px';
    CKEDITOR.config.keystrokes = [[CKEDITOR.CTRL + CKEDITOR.SHIFT + 109 /*COMMA*/, 'subscript'],
        [CKEDITOR.CTRL + CKEDITOR.SHIFT + 107 /*PERIOD*/, 'superscript']];

};
