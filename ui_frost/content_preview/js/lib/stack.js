// JavaScript

function Stack () {
	var aElements = [];
	
	this.isEmpty = function () {
		return (aElements.length == 0);
	};
	
	this.push = function (mixElement) {
		aElements.push(mixElement);
	};
	
	this.pop = function () {
		if (aElements.length == 0) {
			return null;
		}
		
		return aElements.pop();
	};
}