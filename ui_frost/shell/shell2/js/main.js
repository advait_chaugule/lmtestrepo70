var current = -1;
var toc_html = '';
var totalPage = 0;
var started = false;
var isOnline = true;
var pageLocation = "";
var windowHeight, contentHeight;

function get_page(id, page) {
    var index = parseInt($('#' + id).attr('rel'));
    //        id = $(this).attr('id'),
    //        page = $(this).attr('file');
    $('li a').removeClass('active');
    $('#' + id).addClass('active');

    if (page) {
        var pageUrl = page;
        var content = "";
        if (isOnline) {
            var pageUrl = pageLocation + 'html/' + page;
        }
        if (!started) {
            ajaxCall(pageUrl, $(".middleContent .contentData"), index);
        } else {
            ajaxCall(pageUrl, $(".rightContent .contentData"), index);
        }
        started = true;
    }

}

function ajaxCall(pageUrl, elem, index) {
    $.get(pageUrl, function(data) {
        $("section.content").html('').html("Keep patience.. Loading content..");
    }).done(function(data) {
        content = data.slice((data.indexOf('<section')), (data.lastIndexOf('</section>') + ('</section>').length));
        elem.html('').html(content);
        initDataSetAndMultipleVideo();
        movePage(index);
    });
}

function movePage(index) {

    var direction;
    if (current != index) {
        if (current < index) {
            fromRight(index);
        } else {
            fromLeft(index);
        }
    }
}

function fromLeft(index, pageUrl) {
    var content = "",
        pageIndexing = "";
    $('.tocItem').addClass('disabledToc');
    $('.middleContent').removeClass('from-left to-right from-right to-left');
    $('.middleContent').addClass('to-right');
    $('.mobSelectionPanelToc').hide();
    setTimeout(function() {
        $('.middleContent').removeClass('current middleContent').addClass('leftContent');
        $('.rightContent').removeClass('from-left to-right from-right to-left').addClass('current from-left');
        $('.rightContent').removeClass('rightContent').addClass('middleContent');
        $('.leftContent').removeClass('leftContent').addClass('rightContent');
        pageIndexing = '<strong>Page ' + index + '</strong> of <strong>' + totalPage + '</strong>';
        $('.middleContent .aligned').html(pageIndexing);
        current = index;
        $('.tocItem').removeClass('disabledToc');

    }, 100);
}

function fromRight(index, pageUrl) {
    var content = "",
        pageIndexing = "";
    $('.tocItem').addClass('disabledToc');
    if (index === 1) {
        pageIndexing = '<strong>Page ' + index + '</strong> of <strong>' + totalPage + '</strong>';
        $('.middleContent .aligned').html(pageIndexing);
        $('.middleContent').addClass('current from-right');
        current = index;
    } else {
        $('.middleContent').removeClass('from-left to-right from-right to-left');
        $('.middleContent').addClass('to-left');
        $('.mobSelectionPanelToc').hide();
        setTimeout(function() {
            $('.middleContent').removeClass('current middleContent').addClass('leftContent');
            $('.rightContent').removeClass('from-left to-right from-right to-left').addClass('current from-right');
            $('.rightContent').removeClass('rightContent').addClass('middleContent');
            $('.leftContent').removeClass('leftContent').addClass('rightContent');
            pageIndexing = '<strong>Page ' + index + '</strong> of <strong>' + totalPage + '</strong>';
            $('.middleContent .aligned').html(pageIndexing);
            current = index;
            $('.tocItem').removeClass('disabledToc');
        }, 100);
    }
}

function navigationMove() {
    var tocIndex;
    for (var i = 0; i < $('.leftSelectionPanelToc .tocItem').length; i++) {
        if ($('.leftSelectionPanelToc .tocItem').eq(i).hasClass('active')) {
            tocIndex = i;
        }
    }
    if (tocIndex === undefined) {
        for (i = 0; i < $('.mobSelectionPanelToc .tocItem').length; i++) {
            if ($('.mobSelectionPanelToc .tocItem').eq(i).hasClass('active')) {
                tocIndex = i;
            }
        }
    }
    if ($(this).hasClass('previous') && tocIndex > 0) {
        $('.tocItem').eq(tocIndex - 1).trigger('click');
        pageIndexSetter();
    } else if ($(this).hasClass('next') && tocIndex < $('.leftSelectionPanelToc .tocItem').length - 1) {
        $('.tocItem').eq(tocIndex + 1).trigger('click');
        pageIndexSetter();
    }

}

function pageIndexSetter() {
    totalPage = $('.leftSelectionPanelToc li').length;
    pageIndexing = '<strong>Page ' + index + '</strong> of <strong>' + totalPage + '</strong>';
    $('.middleContent .aligned').html(pageIndexing);
}

function populateTOC(toc) {
    var tocData = toc;
    for (var key in tocData) {
        var value = tocData[key];
        var ext_res = value.ext_res_type;
        if (ext_res) {
            toc_html += "<li ><a class='tocItem' id='" + value.id + "' target='_blank' href='" + value.ext_res_link + "'>" + value.title + "</a>";
            if (value.children.length > 0) {
                toc_html += "<ul>";
                populateTOC(value.children);
                toc_html += "</ul>";
            }
            toc_html += "</li>";
        } else {
            toc_html += "<li ><a class='tocItem' id='" + value.id + "' href='javascript:void(0);' onClick=get_page(\"" + value.id + "\",\"" +
                value.file + "\") file='" + value.file + "'>" + value.title + "</a>";
            if (value.children.length > 0) {
                toc_html += "<ul>";
                populateTOC(value.children);
                toc_html += "</ul>";
            }
            toc_html += "</li>";

        }

    }
    return toc_html;
}

$(document).ready(function() {
    function init() {
        windowHeight = $(document).height();
        contentHeight = windowHeight - 96;
        $('.contentData').css("height", contentHeight + 'px');
        if (!isOnline) {
            toc_html = populateTOC(tocData.children);
            $(".leftSelectionPanelToc").html(toc_html);
            $(".mobSelectionPanelToc").html(toc_html);
        }
        totalPage = $('.leftSelectionPanelToc li').length;
        for (var i = 0; i < $('.leftSelectionPanelToc li').length; i++) {
            $('.leftSelectionPanelToc li a').eq(i).attr('rel', i + 1);
            $('.mobSelectionPanelToc li a').eq(i).attr('rel', i + 1);
        }
        $('.glyphicon-th-list').on('click', function() {
            $('.mobSelectionPanelToc').toggle();
        });
        $('.pageControl').on('click', navigationMove);
    }
    init();
    parent.postMessage("shell", "*");
    $(window).on('message', function(data) {
        if (data.originalEvent.data.path) {
            // if(data.originalEvent.data.identifier==='shell'){
            pageLocation = data.originalEvent.data.path;
            $(".toc").html(data.originalEvent.data.toc_html);
            //  }
        }


    });
    $('.tocItem').on('click', get_page);

    $('.tocItem').addClass('disabledToc');
    $(window).resize(function() {
        var width = $(window).width();
        var height = $(window).height();
        $('.leftSelectionPanelToc').css('max-height', height);
        if (width <= 880) {
            $('.pocContent').removeClass('contentInnerWrapper').addClass('contentInnerWrapper-mob');
        } else {
            $('.pocContent').removeClass('contentInnerWrapper-mob').addClass('contentInnerWrapper');
        }
    }).resize();
    setTimeout(function() {
        jQuery(jQuery('.tocItem')[0]).trigger('click');
    }, 400);
});