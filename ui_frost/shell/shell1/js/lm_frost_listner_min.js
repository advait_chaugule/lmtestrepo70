jQuery(window).on("message", function(data) {
    jQuery(".assessment").each(function() {
        var iframeElement = $(this),
            id = $(this).attr("id"),
            iframeWindow = iframeElement[0].contentWindow,
            dataPath = iframeElement.attr("data-path");
        iframeWindow.postMessage({ data_path: dataPath, id: id }, "*")
    })

    if (data.originalEvent.data.height) {
        if (data.originalEvent.data.type == 'adjustIframe') {
            lastPos = $(window).scrollTop();
        }

        $("#" + data.originalEvent.data.id).css({ 'height': data.originalEvent.data.height, overflow: data.originalEvent.data.overflow });
        if ($("#" + data.originalEvent.data.id).offset()) {
            var scrollTo = $("#" + data.originalEvent.data.id).offset().top;
            if (data.originalEvent.data.scroll) {
                $(window).scrollTop(scrollTo);
            }

        }

        if (data.originalEvent.data.type == 'close') {
            $(window).scrollTop(lastPos);
        }
        return
    }
    // jQuery(".assessment").each(function () {
    //   var iframeElement = $(this), id = $(this).attr("id"), iframeWindow = iframeElement[0].contentWindow, dataPath =   iframeElement.attr("data-path");
    //      var id=iframeElement.attr("id");
    //     iframeWindow.postMessage({data_path: dataPath, id: id}, "*")
    // })


}), ! function() {
    function t(t) {

        h = g.getInstance();
        try {
            o()
        } catch (r) {}

        var d = t.data;
        if (t.data === undefined) {
            return false;
        }
        if (void 0 == d.type)
            return !1;
        var u = d.type.toUpperCase();
        if ("GET" === u && h.find("iframe.exwidget").length >= s)
            s++;
        else if ("PUT" === u)
            try {

                var l = htmlEditor.thisFrame.attr("data-path"),
                    p = htmlEditor.thisFrame.attr("id"),
                    w = { type: "GET", frame_id: p, data_path: l, images: c };
                n("POST", "widFrame", w)
            } catch (r) {}
        else
            "CLOSE" === u ? e() : "SIZE" === u && (f = [], f.push({ height: d.height, width: d.width }), i(d.frame_id, 0));
        switch (u) {
            case "GET":
                var v = d.height,
                    y = d.width;
                f.push({ height: v, width: y }), h.find("iframe.exwidget").length === s && m(f);
                break;
            case "POST":
                a(htmlEditor.thisFrame.attr("id"), d.dataset, htmlEditor.thisFrame.attr("widget-id"))
        }
    }

    function e() {
        jQuery("#exwidgetModal").find("button.close").trigger("click")
    }

    function a(t, a, i) {
        var o = r(t, a, i);
        jQuery(".cke_wysiwyg_frame").contents().find("#" + t).attr("data-path", o);
        var d = { frame_id: t, data_path: o };
        n("POST", "widFrame", d), e()
    }

    function r(t, e, a) {
        var r = "",
            i = e,
            n = { project_id: project_id, project_type_id: project_type_id, ex_widgets: i, repo_id: t, object_id: node_id, widget_id: a };
        return jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.saveWidgetDataAPI,
            async: !1,
            method: "POST",
            data: n,
            xhrFields: { withCredentials: !0 },
            crossDomain: !0,
            success: function(t) {
                r = t.json_file_path
            },
            error: function() {
                alert("Error occured..Retry")
            }
        }), r
    }

    function i(t, e) {
        var a = t,
            r = h.find("#" + a)[0],
            i = Number(f[e].height) + 40;
        try {
            r.height = l > i ? i : l
        } catch (n) {}
    }

    function n(t, e, a) {
        if ("POST" === t) {
            var r = e,
                i = jQuery("body").find("#" + r)[0];
            i.contentWindow.postMessage(a, "*")
        } else if ("GET" === t) {
            var r = e,
                i = h.find("#" + r)[0];
            i.contentWindow.postMessage(a, "*")
        }
    }

    function o() {
        var t = "",
            e = [];
        jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.getLocalImagesAPI,
            async: !0,
            type: "GET",
            xhrFields: { withCredentials: !0 },
            crossDomain: !0,
            success: function(a) {
                if (t = a.data, null !== t)
                    for (var r = 0; r < t.length; r++)
                        e.push({ name: t[r].original_name, path: t[r].asset_location });
                c = e
            },
            error: function() {
                swal({ title: "Error!", text: "Some error occured while fetching the assets." })
            }
        })
    }

    function d(t) {
        var e = [];
        return h.find("iframe.exwidget").each(function() {
            var a = jQuery(this).attr(t);
            e.push(a)
        }), e
    }
    var s = 0,
        h = "",
        c = [],
        u = !0,
        f = [],
        l = 610,
        g = function() {
            return {
                getInstance: function() {
                    var t = jQuery("#testCont").length;
                    return t ? jQuery("#testCont").contents() : jQuery("body")
                },
                getAbsolutePath: function(t) {
                    var e = document.createElement("a");

                    if (e.protocol === undefined) {
                        return t;
                    }
                    return e.href = t, e.protocol + "//" + e.host + e.pathname + e.search + e.hash
                }
            }
        }();
    window.addEventListener ? window.addEventListener("message", t, !1) : window.attachEvent("onmessage", t);
    var m = function() {
        var t = d("id"),
            e = d("data-path");
        if (u)
            for (var a = 0; a < e.length; a++) {
                var r = g.getAbsolutePath(e[a]);
                e[a] = r
            }
        for (var a = 0; a < t.length; a++) {
            var i = { type: "GET", frame_id: t[a], data_path: e[a] };
            n("GET", t[a], i)
        }
        s = 0
    }
}();

var donotOpenLink = function() {
    alert("Link do not function in Build and Test mode.");
}