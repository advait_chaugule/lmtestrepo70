var PageSize;
var rateOfChange;
var currentIndex;
var pages;
var pageLocation="";
var toc_html = "";
var isOnline=true;
//reset height on window resize
jQuery(window).resize(function () { 
     calWidthHeight();
     if(jQuery(window).innerWidth() < 1000){
        jQuery("#toc_pane").removeClass("open");
        jQuery("#btn_toc").removeClass("active");
        jQuery(".content-pane").removeClass("open");
     }
});
function calWidthHeight(){
    var wh = jQuery(window).innerHeight(),
    ww = jQuery(window).innerWidth(),
    f = jQuery("footer").height(),
    t = jQuery(".toc").width(),
    h = wh - f;
    if (jQuery("#btn_toc").hasClass("active")){
        ww = ww - t;
    }
    jQuery(".content").css("height", h); //content-pane
    jQuery(".content").css("width", ww);
    var frameHeight = jQuery(".content").height() - parseInt(jQuery(".content").css('margin-top'));
    jQuery("#contentDataId").attr("height", frameHeight);
    jQuery("#contentDataId").attr("width", ww - 10);
    var learningHeight = jQuery(".learningObjective").css("height");
    var tocFinalHeight = h - parseInt(learningHeight);
    jQuery('.toc').css( "height", tocFinalHeight);
}

jQuery(document).ready(function () {
     parent.postMessage("shell2","*");
       $(window).on('message',function(data){
          if(data.originalEvent.data.path){
                //if(data.originalEventdata.data.identifier==='shell'){
             pageLocation=data.originalEvent.data.path;
      //  }
          }
      
      
    });
    setTimeout(function () {
        calWidthHeight();
       // var toc_html = populateTOC(tocData.children); 
        //jQuery(".toc").html(toc_html);
        
        var toc = jQuery(".toc li a");
        pages=jQuery(".toc").find('a');
        PageSize=pages.length-1;
        rateOfChange=100/PageSize;
        currentIndex=0;
        toc.each(function () {
            var file_attr = jQuery("#" + this.id).attr("file");
            if (file_attr) {
                get_page(this.id, file_attr);
                return false;
            }
        });
        bindEvents();
    }, 1000);

});
function progressBar(currentIndex){
if(currentIndex===PageSize){
$('.next').css('pointer-events','none');
}
else{
$('.next').css('pointer-events','auto');
}
if(currentIndex===0){
$('.prev').css('pointer-events','none');
}
else{
$('.prev').css('pointer-events','auto');
}
jQuery('.pos').text(''+currentIndex+' of '+PageSize+'');
jQuery('.progress-bar').css('width',currentIndex*rateOfChange+'%');
}
function bindEvents(){

progressBar(currentIndex);
jQuery('.next').on('click',function(){
currentIndex++;
progressBar(currentIndex);
pages[currentIndex].click(currentIndex);
});
jQuery('.prev').on('click',function(){
currentIndex--;
progressBar(currentIndex);
pages[currentIndex].click();
});
}
function toggleNav() {
    if (jQuery("#btn_toc").hasClass("active")) {
        jQuery("#toc_pane").removeClass("open");
        jQuery("#btn_toc").removeClass("active");
        jQuery(".content-pane").removeClass("open");
    } else {
        jQuery("#toc_pane").removeClass("open").addClass("open");
        jQuery("#btn_toc").removeClass("active").addClass("active");
        jQuery(".content-pane").removeClass("open").addClass("open");
    }
    calWidthHeight();
}

function get_page(id, page) {
    console.info(id + " -- " + page);
    jQuery("li a").removeClass("active");
    jQuery("#" + id).addClass("active");
    if(event===undefined){
        if( pages!=null){
            currentIndex = pages.index(pages.filter(".active"));
        }else{
            return;
        }
    }else{
	currentIndex = $.inArray(event.target,pages);
    }
    progressBar(currentIndex);
    if (page) {
        var pageUrl = page;
        var content = "";
        if(isOnline){
             var pageUrl = pageLocation+ 'html/' + page;
        }
        jQuery("section.content").html('').html("Keep patience.. Loading content..");
        jQuery.get(pageUrl, function (data) {
            jQuery("section.content").html('').html("Keep patience.. Loading content..");
        }).done(function (data) {
            // content=$(data).find('body').find('.container');
            content=data.slice((data.indexOf('<section')),(data.lastIndexOf('</section>')+('</section>').length));
//            console.info(data);
//			jQuery(data).each(function(id,item){
//  if($(item).hasClass('container')){
//   content = $(item);
//     
//  }
//            });
  jQuery("section.content").html('').html(content);
         $("#gallery").remove();
              initImgPopup();
      });
		 //jQuery("#contentDataId").attr('src',pageUrl);
        //var content = jQuery("#contentDataId").attr('src',pageUrl);
       
    }
    runNow();         
}


jQuery.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
}

function populateTOC(toc){
    var tocData = toc;
    for(var key in tocData){
        var value = tocData[key];
        toc_html += "<li style='list-style-type: none;'><a id='" + value.id + "' href='javascript:void(0);' file='"        + value.file + "' onClick=get_page(\"" + value.id + "\",\"" + value.file + "\");>" + value.title + "</a>";
        if (value.children.length > 0) {
            toc_html += "<ul style='list-style-type: none;'>";
            populateTOC(value.children);
            toc_html += "</ul>";
        }
        toc_html += "</li>";
    }
    return toc_html;
}


