
var PageSize;
var rateOfChange;
var currentIndex;
var pages;
jQuery(document).ready(function () {
 
    setTimeout(function () {
        var toc = jQuery("#toc li a");
		pages=jQuery("#toc").find('a');
	    PageSize=pages.length-1;
	    rateOfChange=100/PageSize;
        currentIndex=1;
        toc.each(function () {
            var file_attr = jQuery("#" + this.id).attr("file");
            if (file_attr) {
                get_page(this.id, file_attr);
                return false;
            }
        });
        var paneHeight = jQuery(".content-pane").css("height");
        //console.log(paneHeight);
        var footerHeight = jQuery("footer").css("height");
        //console.log(footerHeight);
        var tocHeight = (parseInt(paneHeight)) - (parseInt(footerHeight));
        //console.log(tocHeight);
        var learningHeight = jQuery(".learningObjective").css("height");
        //console.log(learningHeight);
        var tocFinalHeight = tocHeight-parseInt(learningHeight);
        jQuery('.toc').css( "height",tocFinalHeight+49.5);
		bindEvents();
    }, 1000);

});
function progressBar(currentIndex){
if(currentIndex===PageSize){
$('.next').css('pointer-events','none');
}
else{
$('.next').css('pointer-events','auto');
}
if(currentIndex===0){
$('.prev').css('pointer-events','none');
}
else{
$('.prev').css('pointer-events','auto');
}
jQuery('.pos').text(''+currentIndex+' of '+PageSize+'');
jQuery('.progress-bar').css('width',currentIndex*rateOfChange+'%');
}
function bindEvents(){

progressBar(currentIndex);
jQuery('.next').on('click',function(){
currentIndex++;
progressBar(currentIndex);
pages[currentIndex].click(currentIndex);
});
jQuery('.prev').on('click',function(){
currentIndex--;
progressBar(currentIndex);
pages[currentIndex].click();
});
}
function toggleNav() {
    if (jQuery("#btn_toc").hasClass("active")) {
        jQuery("#toc_pane").removeClass("open");
        jQuery("#btn_toc").removeClass("active");
        jQuery(".content-pane").removeClass("open");
    } else {
        jQuery("#toc_pane").removeClass("open").addClass("open");
        jQuery("#btn_toc").removeClass("active").addClass("active");
        jQuery(".content-pane").removeClass("open").addClass("open");
    }
}

function get_page(id, page) {
    console.info(id + " -- " + page);
    jQuery("li a").removeClass("active");
    jQuery("#" + id).addClass("active");
	if(event===undefined){
	return;}
	currentIndex=$.inArray(event.target,pages);
	progressBar(currentIndex);
    var pid = jQuery.urlParam("pid");
    if (page) {
        var content = "";
        var pageUrl = 'https://frost-qa.s3.amazonaws.com/local_dev_167/' + pid + '/html/' + page;
        jQuery("section.content").html('').html("Keep patience.. Loading content..");
        jQuery.get(pageUrl, function (data) {
            jQuery("section.content").html('').html("Keep patience.. Loading content..");
        }).done(function (data) {
            //console.info(data);
			jQuery(data).each(function(id,item){
  if($(item).hasClass('container')){
   content = $(item);
   return;
      }
            
});
            /*if (jQuery(data).find('body')) {
                content = jQuery(data).find('body').html();
                if (typeof content === 'undefined') {
                    content = jQuery("<html/>").html(data).find('body').html();
                }
            } else {
                content = jQuery("<html/>").html(data).find('body').html();
            }*/
            jQuery("section.content").html('').html(content);
			$('head').append('<script src="js/lm_popup.js" type="text/javascript"></script>');
			/*jQuery(data).each(function(id,item){
      $(item).prop("tagName")=='SCRIPT'?$('head').append(item):''
})*/
            runNow();
        });
        //var paneHeight = jQuery("content-pane").height();
        //console.log(paneHeight);
    }
    //var paneHeight = jQuery("content-pane").css("height");
    //console.log(paneHeight);
}

jQuery.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
}

