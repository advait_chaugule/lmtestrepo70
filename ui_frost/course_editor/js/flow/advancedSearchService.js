/*****
 * Services for Advanced Search
 * Author - Alamgir Hossain Sk
 */
'use strict';

//Start of advanced search Services
app.service('advancedSearchService', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {

        this.getUsers = function ($scope) {
            var deferred = $q.defer();
            $http.get(serviceEndPoint + 'api/v1/users?access_token=' + access_token, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);//Return the roles list                
            }).error(function (data, status, headers, config) {
                //$rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };


    this.getGlobalQuestionPreview = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&repo_id=' + $scope.repo_id + '&entity_type_id=' + $scope.entity_type_id;
            $http.get(serviceEndPoint + 'api/v1/assessment-global-preview?access_token=' + access_token + queryString, {
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the Question Details             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };
        this.getQuestionPreview = function ($scope) {
            var deferred = $q.defer();
            var queryString = '&project_id=' + $scope.project_id + '&project_type_id=' + $scope.project_type_id + '&repo_id=' + $scope.repo_id + '&entity_type_id=' + $scope.entity_type_id;
            $http.get(serviceEndPoint + 'api/v1/assessment-object-preview?access_token=' + access_token + queryString, {
            }).success(function (data, status, headers, config) {
                if (data.projectPermissionGranted == 401) {
                    $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                }
                deferred.resolve(data);//Return the Question Details             
            }).error(function (data, status, headers, config) {
                $rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;

        };
        
        this.getKeyValuesByObject = function ($scope, objet_id) {
            var params = {};
            params.access_token = access_token;
            var deferred = $q.defer();
            var queryString = '';

            if (angular.isDefined(project_id) && angular.isDefined(project_id)) {
                queryString += '&project_id=' + $scope.project_id;
            }

            $http.get(serviceEndPoint + 'api/v1/key-value-object/' + objet_id + '?access_token=' + access_token + queryString, {
            }).success(function (data, status, headers, config) {
                /*if (data.projectPermissionGranted == 401) {
                 $rootScope.move_unauthorised_user_to_toc_with_message(data.projectPermissionGranted, $scope.project_id);
                 }*/ //Commented as it was redirecting the user though Global permissions are all checked FROS-2082
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                //$rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        this.getTagsByObject = function ($scope, object_id) {
            var params = {};
            params.access_token = access_token;
            var deferred = $q.defer();
            var queryString = '';
            $http.get(serviceEndPoint + 'api/v1/tag-object/' + object_id + '?access_token=' + access_token + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                //$rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

        /**
         * Get taxonomy
         */
        this.getTaxonomy = function ($scope, node_id) {
            var params = {};
            params.access_token = access_token;
            var deferred = $q.defer();
            var queryString = '';
            if (angular.isDefined(node_id)) {
                queryString += '&object_id=' + node_id;
            }
            if (angular.isDefined($scope.taxonomyStatus)) {
                queryString += '&status=' + $scope.taxonomyStatus;
            }
            $http.get(serviceEndPoint + 'api/v1/taxonomy?access_token=' + access_token + queryString, {
            }).success(function (data, status, headers, config) {
                deferred.resolve(data);
            }).error(function (data, status, headers, config) {
                //$rootScope.unauthorised_redirection(data.status);
            });
            return deferred.promise;
        };

    }]);

