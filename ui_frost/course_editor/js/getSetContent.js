htmlEditor.needsToBeMerged = false; //Checks if theres a change in the latest version

htmlEditor.userProjectPermission = {};

htmlEditor.currentUserPermissions = function() {
    return jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.projectDetailWithPermissionsAPI,
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            htmlEditor.userProjectPermission = data.data.project_permissions; // Save permission object locally.
        }
    })
}

htmlEditor.checkLockObject = function(dataId) {
    var ObjectLock = 0;
    jQuery.ajax({
        url: serviceEndPoint + 'api/v1/get-object-lock?access_token=' + access_token + '&object_id=' + dataId + '&user_id=' + user_id,
        async: false,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            if (Number(data.current_lock)) {
                swal("Cancelled", "You cannot access this content right now, as it is locked. Please try again later!", "error");
                ObjectLock = 1;
            } else {
                ObjectLock = 0;
            }
        }
    })
    return ObjectLock;
};
htmlEditor.hideTocandSidebar = function() {
    if (htmlEditor.isGlobalProject) {
        // jQuery('.edt-rt').hide();
        jQuery('.toc-btn').hide();
        jQuery('#project_name').hide();
        jQuery('[data-target="#testPreviewModal"]').hide();
        jQuery("#lockPage").hide();
        jQuery('.loc-or-global').hide();
    }
};
htmlEditor.getJsonValue = function() { // Fetch the content to be displayed.
    var editorContent;

    jQuery.when(editorContent = htmlEditor.ajaxCalls(htmlEditor.PXEmappedAPI.getContentAPI)).done(function() {
        htmlEditor.currentUserPermissions();
        var contObj = editorContent.responseJSON;
        var returnedContentScriptObj = stripScripts(contObj.data.description);
        var content = returnedContentScriptObj.content;
        htmlEditor.strippedScripts = '';
        htmlEditor.strippedScripts = returnedContentScriptObj.script;

        function stripScripts(strInput) {
            var dummyDiv = document.createElement('div');
            dummyDiv.innerHTML = strInput;
            var strOutput = '';
            var scripts = dummyDiv.getElementsByTagName('script');
            var len = scripts.length;
            if (len) {
                while (len--) {
                    strOutput += scripts[len].outerHTML;
                    scripts[len].parentNode.removeChild(scripts[len]);
                }
            }
            return { 'content': dummyDiv.innerHTML, 'script': strOutput };
        }

        htmlEditor.islocked = contObj.data.is_locked;
        user_id = contObj.data.user_id;
        htmlEditor.editorTOC = contObj.data.editor_toc;
        htmlEditor.editorTOCMerged = contObj.data.editor_merged_toc;
        var lockedbyuser = contObj.data.locked_by_user;
        if (lockedbyuser !== undefined) {
            if (user_id === lockedbyuser || user_id === 1) { // 1 = admin
                if (htmlEditor.islocked) {
                    jQuery("#lockPage").find(".fa-unlock").removeClass("fa-unlock").addClass("fa-lock");
                    htmlEditor.islocked = true;
                }
            } else {
                //window.location.assign(clientUrl + "/#edit_toc/" + project_id);
            }
        }

        //        var current_node = contObj.data.object_location;        //Holds the section Id current node maps to.
        //        var section_id = current_node.split("#")[1];
        htmlEditor.current_node = section_id;

        htmlEditor.cssFiles = contObj.data.theme_location;
        htmlEditor.glossaryFile = contObj.data.glossary_file_name;
        htmlEditor.footnoteFile = 'footnote.xhtml';
        try {
            var validation_response = JSON.parse(contObj.data.validation_response);
            htmlEditor.showValidationContent(validation_response);
        } catch (exc) {
            jQuery(".error-count").hide();
        }
        htmlEditor.initiateEditor();
        CKEDITOR.on('instanceReady', function(ev) {
            jQuery.when(htmlEditor.setData(content)).done(function() {
                jQuery(".cke_wysiwyg_frame").contents().find("html").attr({ "xmlns:epub": "http://www.idpf.org/2007/ops", "xmlns:svg": "http://www.w3.org/2000/svg", "xmlns": "http://www.w3.org/1999/xhtml", "xmlns:m": "http://www.w3.org/1998/Math/MathML", "epub:prefix": "index: http://www.index.com/", "xml:lang": "en" })
            });
            //console.info(CKEDITOR.dom.element('section'));
            jQuery(".cke_wysiwyg_frame").attr("id", "editor-frame");
            htmlEditor.changeEditorClass();
            htmlEditor.addToolbarBtn();
            if (!htmlEditor.permissionTo.viewSourceCode) {
                jQuery('.cke_button__source').addClass('hideElm');
            }

            // Enable drag drop and other functionalities after redo/undo.
            CKEDITOR.instances.editor1.on('afterCommandExec', handleAfterCommandExec);

            function handleAfterCommandExec(event) {
                var commandName = event.data.name;
                // For 'redo/ undo' commmand
                if (commandName === 'redo' || commandName === 'undo')
                    htmlEditor.bindEvents();

                if (commandName === 'source') {
                    //Change the arrow direction of the button
                    jQuery(".lm-edit-more").removeClass("active").find("span.fa").removeClass("fa-chevron-right");
                    jQuery(".lm-edit-more").parents(".lm-edt-pane").removeClass("active").attr({ 'disabled': 'disabled' });

                    htmlEditor.includeCSS(jQuery("iframe.cke_wysiwyg_frame"));
                    var oFrameElm = jQuery(".cke_wysiwyg_frame"),
                        isPreviewMode = oFrameElm.length;
                    if (isPreviewMode) {
                        setTimeout(function() {
                            jQuery(".lm-edit-more").parents(".lm-edt-pane").removeAttr('disabled');
                            //htmlEditor.includeCSS(jQuery("iframe.cke_wysiwyg_frame"));
                            htmlEditor.bindIframeDragDrop();
                            htmlEditor.convertFrameToDiv();
                        }, 100);
                    }
                    //htmlEditor.convertFrameToDiv();//for fixing the undefined title path of assessment widget player.
                    htmlEditor.bindEvents();
                    //alert(jQuery(".cke_wysiwyg_frame").contents().find("body").find('*').length)
                    /*jQuery(".cke_wysiwyg_frame").contents().find("body").html("");  //Make sure html is empty
                     htmlEditor.bindIframeDragDrop();console.log()
                     htmlEditor.disableToolbar();*/
                }
            }

            CKEDITOR.instances.editor1.on('beforeCommandExec', handleBeforeCommandExec);

            function handleBeforeCommandExec(event) {
                var cmdName = event.data.name;
                // For 'source' commmand
                if (cmdName === 'source') {
                    htmlEditor.convertDivToFrame(jQuery(".cke_wysiwyg_frame").contents().find("body"));
                    htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents().find("body"));
                }
            }
            htmlEditor.aOnclickValueArr = [];
            jQuery('a.cke_button').each(function() {
                htmlEditor.aOnclickValueArr.push(jQuery(this).attr("onclick"));
            });
        });
        var projectName = contObj.data.object_name;
        var chapName = contObj.data.node_title;
        jQuery("#node_title").html(chapName);

        if (projectName.length > 30) {
            jQuery("#project_name").html(projectName.substring(0, 30) + "... / ");
        } else {
            jQuery("#project_name").html(projectName + ' / ');
        }
    })
};
htmlEditor.imageTimeStamp = function(el, req) {
    var refEl = jQuery(el),
        images = refEl.find('img'),
        addTimeStamp = req;

    jQuery(images).each(function() {
        if (addTimeStamp) {
            var timeStamp = new Date().getTime(),
                thisElm = jQuery(this),
                srcWithTimeStamp = thisElm.attr('src') + '?' + timeStamp;
            thisElm.attr('src', srcWithTimeStamp);
        } else {
            var thisElm = jQuery(this),
                upto = '?',
                srcWithoutTimeStamp = thisElm.attr('src').split(upto)[0];
            thisElm.attr('src', srcWithoutTimeStamp);
        }
    });
    htmlEditor.videoTimeStamp(el, req)
}

htmlEditor.videoTimeStamp = function(el, req) {
    var refEl = jQuery(el),
        videos = refEl.find('video').find('source'),
        addTimeStamp = req;

    jQuery(videos).each(function() {
        if (addTimeStamp) {
            var timeStamp = new Date().getTime(),
                thisElm = jQuery(this),
                srcWithTimeStamp = thisElm.attr('src') + '?' + timeStamp;
            thisElm.attr('src', srcWithTimeStamp);
        } else {
            var thisElm = jQuery(this),
                upto = '?',
                srcWithoutTimeStamp = thisElm.attr('src').split(upto)[0];
            thisElm.attr('src', srcWithoutTimeStamp);
        }
    });
}

htmlEditor.setData = function(content) { // Set the fetched content in the editor.
    //console.log(content);
    htmlEditor.disableSave();
    var cloneFrame = document.createElement('div');
    //var cloneFrame = jQuery("#testCont").contents().find('body');

    //console.info(CKEDITOR.instances.editor1.find('html'));



    jQuery(cloneFrame).html(content);
    htmlEditor.imageTimeStamp(cloneFrame, true);
    content = jQuery(cloneFrame).html();
    CKEDITOR.instances.editor1.setData(content);
    //console.log(content);
    /*var contentIn = jQuery(".cke_wysiwyg_frame").contents().find("body");
     console.log(contentIn);
     var quadObject = contentIn.find(".widget");
     console.log(quadObject.length);
     if(quadObject.length>0){

     }*/

    var frame = jQuery("iframe.cke_wysiwyg_frame");
    htmlEditor.includeCSS(frame);
    htmlEditor.onWindowResize();
    jQuery(".loading-g").hide();
    //var curr_ver = jQuery("#versionDropdown").find(".curr-ver").attr("version_id");
    var curr_ver = jQuery(".version_num").attr("version_id"); //Get current user version
    var last_users = "";
    //Initiate track for a concurrent user.
    var es = new EventSource(serviceEndPoint + "api/v1/concurrent-editor-user?object_id=" + node_id + "&project_id=" + project_id + "&version_id=" + curr_ver + "&pageAction=INSERT", { withCredentials: true });
    var listener = function(event) {
        //console.log(event.data);
        //var user_ver = jQuery("#versionDropdown").find(".curr-ver").attr("version_id"); //Get current user version.
        var user_ver = jQuery(".version_num").attr("version_id"); //Get current user version.
        var resp = JSON.parse(event.data);
        if (resp.status == '403') {
            //window.location.assign(clientUrl + "/#edit_toc/" + project_id);
        }
        var userlist = resp.users;
        var latest_version = resp.latest_version_id;
        var users = userlist.join();
        if (userlist.length) {
            htmlEditor.allowLocking = false;
            jQuery(".concurrent-info").removeClass("hideElm").find("span.con-user").text(users);
            //Check if a merge option needs to be provided to the user.
            if (user_ver !== latest_version && !htmlEditor.manualSelection) {
                htmlEditor.needsToBeMerged = true;
            } else {
                htmlEditor.needsToBeMerged = false;
            }

            if (htmlEditor.manualSelection && user_ver !== latest_version) {
                htmlEditor.createVersion = false;
            } else {
                htmlEditor.createVersion = true;
            }
            //            }
        } else {
            htmlEditor.allowLocking = true;
            jQuery(".concurrent-info").addClass("hideElm");
            if (user_ver !== latest_version && htmlEditor.manualSelection) {
                htmlEditor.createVersion = false;
            }
        }
        if (htmlEditor.allowLocking) {
            jQuery("#lockPage").attr("disabled", false);
        } else {
            jQuery("#lockPage").attr("disabled", true);
        }
    };
    es.addEventListener("message", listener);
    //=====================================

    jQuery.when((function() {
        setTimeout(function() {
            htmlEditor.currContent = htmlEditor.getLatestSavedContent();
            try {
                var posOfNode = frame.contents().find("#" + htmlEditor.current_node).offset().top; //Get offset of the node section in the current content.
                frame.contents().scrollTop(posOfNode); //Focus the content according to the section id.
            } catch (exc) {
                frame.contents().scrollTop(0);
            }
        }, 1000);

        frame.contents().find('body').createTOC();
        //        frame.contents().find('body').createCFI();
    })()).done(function() {
        try {
            htmlEditor.convertFrameToDiv();

        } catch (exc) {}
        htmlEditor.bindEvents();

        //If widget overlay is not created due to slow API response
        var framesUnconvertedLength = frame.contents().find('body').find('iframe').length;
        /*var frameConvertChecker = function () {
         if (framesUnconvertedLength) {
         htmlEditor.convertFrameToDiv();
         htmlEditor.bindEvents();
         framesUnconvertedLength = frame.contents().find('body').find('iframe').length;
         frameConvertChecker();
         }
         else
         return false;
         };*/
        //frameConvertChecker();
        /**/
    });
    //Store the content to map any changes in the content.

}

htmlEditor.getLatestSavedContent = function() {
    var content = CKEDITOR.instances.editor1.getData();

    jQuery("#trial_load").contents().find("body").html(content);
    htmlEditor.removeUnwanteds(jQuery("#trial_load").contents().find("body"));
    jQuery("#trial_load").contents().find("body").find("*").removeAttr("ticket-id").removeAttr("data-pos");
    var finalContent = jQuery("#trial_load").contents().find("body").html();
    return finalContent;
}

htmlEditor.includeCSS = function(frame) { // Include redseriff css in editor.
    var editorFrame = frame; //jQuery("iframe.cke_wysiwyg_frame");
    var cssFiles = htmlEditor.cssFiles;
    if (editorFrame.hasClass("cke_wysiwyg_frame")) { //Check if the css is included in editor.
        var linkHTML = '<link rel="stylesheet" type="text/css" href="css/iframe.css"/><link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">';
        editorFrame.contents().find('style').remove();
    } else {
        var linkHTML = '<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">'; //Default it to include font awesome
    }
    editorFrame.contents().find('head').find('link').remove();
    if (cssFiles != null) {
        for (var i = 0; i < cssFiles.length; i++) {
            linkHTML += '<link rel="stylesheet" type="text/css" href="' + cssFiles[i] + '"/>';
        }
    }
    editorFrame.contents().find('head').append(linkHTML);
};

htmlEditor.loadPatterns = function() {
    //Fetch the patterns for this project
    var patternQueryString = '&project_id=' + project_id + '&pattern_status=1001';
    var url = htmlEditor.PXEmappedAPI.getPatternsAPI + patternQueryString;
    var patternObj;
    jQuery.when(patternObj = htmlEditor.ajaxCalls(url)).done(function() {
        var patterns = patternObj.responseJSON.data;
        htmlEditor.loadWidgets(patterns);
    });
}
htmlEditor.patternNameFormatter = function(patternName) {
        var title = patternName;
        //    console.log(title);
        if (title.length > 19) {
            var shortText = jQuery.trim(title).substring(0, 15) + "...";
            return shortText;
        }
        return title;
    }
    /*htmlEditor.getQuad = function () {
     //var patternQueryString = '&project_id=' + project_id + '&pattern_status=1001';
     jQuery.when(editorContent = htmlEditor.ajaxCalls(htmlEditor.PXEmappedAPI.getContentAPI)).done(function () {
     var contObj = editorContent.responseJSON;
     var returnedContentScriptObj = stripScripts(contObj.data.description);
     var content = returnedContentScriptObj.content;
     function stripScripts(strInput) {
     var dummyDiv = document.createElement('div');
     dummyDiv.innerHTML = strInput;
     var strOutput = '';
     var scripts = dummyDiv.getElementsByTagName('script');
     var len = scripts.length;
     if (len) {
     while (len--) {
     strOutput += scripts[len].outerHTML;
     scripts[len].parentNode.removeChild(scripts[len]);
     }
     }
     return {'content': dummyDiv.innerHTML, 'script': strOutput};
     }
     var contentIn = jQuery(".cke_wysiwyg_frame").contents().find("body");
     // console.log(contentIn);
     var qidArr=[];
     var quadObject = contentIn.find(".widget");
     console.log(quadObject);
     // var qId= quadObject.attr('id');
     //console.log(qId);
     for(var i=0;i <quadObject.length ; i++){
     qidArr.push(quadObject.attr('id'));

     }
     console.log(qidArr);
     });

     }*/
    /*sessionStorage.setItem('SNOR343203','{"status":200,"data":null,"message":"Quad object data saved successfuly","error":null,"json_file_path":"http:\/\/frost-qa.s3.amazonaws.com\/local_dev_160\/47\/OPS\/frost_widgets_data\/SNORL343203.js","widget_launch_file":"http:\/\/frost-qa.s3.amazonaws.com\/local_dev_160\/frost_widgets\/quad\/index.html","json_value":"{\"quiz\":[{\"QuestionData\":{\"question_stem\":{\"text\":\"<p>Under what conditions do we think life as we know it began?<\\\/p>\"},\"question_title\":{\"text\":\"Under what conditions do we th\"},\"instruction_text\":{\"text\":\"\"},\"question_type\":{\"text\":\"essay\"},\"essay_text\":{\"text\":\"Essay\"},\"max_characters\":{\"text\":\"10000\"},\"toolbar\":{\"type\":\"editor-basic\"},\"global_correct_feedback\":{\"text\":\"\"},\"global_incorrect_feedback\":{\"text\":\"\"},\"hints\":[{\"hintid\":1,\"text\":\"\"}],\"exhibit\":[{\"path\":\"\",\"exhibit_type\":false}],\"settings\":{\"score\":\"1\"}},\"VersionNo\":\"2\",\"DisplayQuestionId\":\"SNORL343203\",\"OriginalQuestionId\":\"917FD1C8-438D-9A72-15FB-678D1BFBD848\",\"IsDeleted\":false,\"SelectedOptions\":null}],\"settings\":\"\"}"}');
     sessionStorage.setItem('SNORL343203','{"status":200,"data":null,"message":"Quad object data saved successfuly","error":null,"json_file_path":"http:\/\/frost-qa.s3.amazonaws.com\/local_dev_160\/47\/OPS\/frost_widgets_data\/SNORL343203.js","widget_launch_file":"http:\/\/frost-qa.s3.amazonaws.com\/local_dev_160\/frost_widgets\/quad\/index.html","json_value":"{\"quiz\":[{\"QuestionData\":{\"question_stem\":{\"text\":\"<p>Under what conditions do we think life as we know it began?<\\\/p>\"},\"question_title\":{\"text\":\"Under what conditions do we th\"},\"instruction_text\":{\"text\":\"\"},\"question_type\":{\"text\":\"essay\"},\"essay_text\":{\"text\":\"Essay\"},\"max_characters\":{\"text\":\"10000\"},\"toolbar\":{\"type\":\"editor-basic\"},\"global_correct_feedback\":{\"text\":\"\"},\"global_incorrect_feedback\":{\"text\":\"\"},\"hints\":[{\"hintid\":1,\"text\":\"\"}],\"exhibit\":[{\"path\":\"\",\"exhibit_type\":false}],\"settings\":{\"score\":\"1\"}},\"VersionNo\":\"2\",\"DisplayQuestionId\":\"SNORL343203\",\"OriginalQuestionId\":\"F271E1ED-3369-3AA8-5BE1-7D18A2FAE290\",\"IsDeleted\":false,\"SelectedOptions\":null}],\"settings\":\"\"}"}');
     sessionStorage.setItem('SNORL343203','{"status":200,"data":null,"message":"Quad object data saved successfuly","error":null,"json_file_path":"http:\/\/frost-qa.s3.amazonaws.com\/local_dev_160\/47\/OPS\/frost_widgets_data\/SNORL343203.js","widget_launch_file":"http:\/\/frost-qa.s3.amazonaws.com\/local_dev_160\/frost_widgets\/quad\/index.html","json_value":"{\"quiz\":[{\"QuestionData\":{\"question_stem\":{\"text\":\"<p>Under what conditions do we think life as we know it began?<\\\/p>\"},\"question_title\":{\"text\":\"Under what conditions do we th\"},\"instruction_text\":{\"text\":\"\"},\"question_type\":{\"text\":\"essay\"},\"essay_text\":{\"text\":\"Essay\"},\"max_characters\":{\"text\":\"10000\"},\"toolbar\":{\"type\":\"editor-basic\"},\"global_correct_feedback\":{\"text\":\"\"},\"global_incorrect_feedback\":{\"text\":\"\"},\"hints\":[{\"hintid\":1,\"text\":\"\"}],\"exhibit\":[{\"path\":\"\",\"exhibit_type\":false}],\"settings\":{\"score\":\"1\"}},\"VersionNo\":\"2\",\"DisplayQuestionId\":\"SNORL343203\",\"OriginalQuestionId\":\"F271E1ED-3369-3AA8-5BE1-7D18A2FAE290\",\"IsDeleted\":false,\"SelectedOptions\":null}],\"settings\":\"\"}"}');*/
    //var s=sessionStorage.getItem('SNORL343203');
    //console.log(s);
    //var s1=sessionStorage.getItem('SNORL343204');

//console.log(s1);
htmlEditor.loadWidgets = function(object) {
    var widgetUrl = htmlEditor.PXEmappedAPI.getWidgetAPI,
        patternArr = [],
        widgetObj,
        statusCheck = '1001';

    jQuery.ajax({
        url: widgetUrl,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(response) {
            try {
                widgetObj = response.data;
                var widgetList = widgetObj.list;

                widgetScope = widgetObj.widget_config;

                if (widgetList.length) {
                    for (var idx = 0; idx < widgetList.length; idx++) {
                        //if (widgetList[idx].status == statusCheck) {
                        patternArr.push({
                            pattern: widgetList[idx].code_snippet,
                            default_class: 'exwidget',
                            default_tag: 'iframe',
                            meta_class_options: '[{"pattern_classes":[{"value":"exwidget"}],"default_class":"exwidget"}]',
                            pattern_classes: '[{"value":"exwidget"}]',
                            pattern_type: 0,
                            project_id: "1",
                            project_type_id: 4,
                            status: widgetList[idx].status,
                            tenant_id: 1,
                            title: htmlEditor.patternNameFormatter(widgetList[idx].name),
                            toc_type: 0
                        });
                        //}

                    }
                    editor_patterns = jQuery.merge(object, patternArr);
                    htmlEditor.setWidgetsPatterns(editor_patterns);
                }
            } catch (exc) {
                editor_patterns = object;
                htmlEditor.setWidgetsPatterns(editor_patterns);
            }

        },
        error: function() {
            //alert("Error occured..Retry");
            editor_patterns = object;
            htmlEditor.setWidgetsPatterns(editor_patterns);
        }
    });
};

htmlEditor.setWidgetsPatterns = function(object) {
    var patterns = object;
    jQuery(".right-pane").empty();
    jQuery(".right-pane").html('<div class="panel-refresh-reload" >Patterns</div>');
    htmlEditor.totalWidgets = patterns.length;
    var statusCheck = '1001';
    for (var i = 0; i < patterns.length; i++) {
        var pattName = patterns[i].title;
        if (patterns[i].status == statusCheck) {
            jQuery(".right-pane").append("<div class='menu-block' default-class=" + patterns[i].default_class + " tag-type=" + patterns[i].default_tag + "><span class='glyphicon glyphicon-th' style='font-size:9px;color:#fff;'></span><div class='tab-name'>" + htmlEditor.patternNameFormatter(pattName) + "</div></div>");
        } else {
            jQuery(".right-pane").append("<div class='menu-block hideElm' default-class=" + patterns[i].default_class + " tag-type=" + patterns[i].default_tag + "><span class='glyphicon glyphicon-th' style='font-size:9px;color:#fff;'></span><div class='tab-name'>" + htmlEditor.patternNameFormatter(pattName) + "</div></div>");
        }
    }
    editor_patterns = patterns;
    if (htmlEditor.isGlobalProject) {
        jQuery(".right-pane").append("<div class='sliderArrows'><button id='prevArrow' class='sprite'>Prev</button><button id='nextArrow' class='sprite'>Next</button></div>");
    } else {
        jQuery(".right-pane").append("<div class='sliderArrows'><button id='prevArrow' class='sprite'>Prev</button> <button class='glyphicon glyphicon-plus addPattern'></button> <button id='nextArrow' class='sprite'>Next</button></div>");
    }

    jQuery(".menu-block").hide();
    htmlEditor.initial = 0;
    //to solve the problem of editor freezing after dragging of patterns.

    //jQuery('.cke_top .cke_toolbox').after("<div class='clearfix'></div>");
    htmlEditor.calculateWidgetsOnResize();
    htmlEditor.bindIframeDragDrop();
    htmlEditor.bindEvents();
}

htmlEditor.calculateWidgetsOnResize = function() { //Calculate number of patterns to be displayed.
    var rightHeight = jQuery(".right-pane").height() - (jQuery('.sliderArrows').height() + jQuery('.panel-refresh-reload').height());
    var widgetHeight = jQuery(".menu-block").eq(0).outerHeight();
    jQuery(".menu-block").each(function() { //Get the maximum height among all patterns.
        var this_height = jQuery(this).outerHeight();
        if (this_height > widgetHeight) {
            widgetHeight = this_height;
        }
    });
    htmlEditor.numOfWidgets = Math.floor(rightHeight / widgetHeight) - 2;
    htmlEditor.numOfWidgets = htmlEditor.numOfWidgets > htmlEditor.totalWidgets ? htmlEditor.totalWidgets : htmlEditor.numOfWidgets;
    //    htmlEditor.numOfWidgets --;                                     // To adjust to the current height of the pane.
    var displayUpto = htmlEditor.numOfWidgets + htmlEditor.initial;
    htmlEditor.createWidgets(htmlEditor.initial, displayUpto);
};

htmlEditor.createWidgets = function(startFrom, UptoInd) { //Display the widgets/ Patterns
    jQuery(".menu-block").hide();
    var hiddenWidgets = 0;
    for (var i = startFrom; i < UptoInd; i++) {
        if (jQuery(".menu-block").eq(i).hasClass("hideElm") === false) { // fix for issue FROS-3131
            jQuery(".menu-block").eq(i).show();
        } else {
            UptoInd++;
            hiddenWidgets++;
        }
    }
    htmlEditor.numOfHidddenWidgets = hiddenWidgets;
    jQuery("#nextArrow").attr("disabled", false).css("opacity", "1");
    jQuery("#prevArrow").attr("disabled", false).css("opacity", "1");
    if (htmlEditor.totalWidgets <= UptoInd) {
        jQuery("#nextArrow").attr("disabled", true);
        jQuery("#nextArrow").css("opacity", "0.5");
    }
    if (htmlEditor.initial == 0) {
        jQuery("#prevArrow").attr("disabled", true);
        jQuery("#prevArrow").css("opacity", "0.5");
    }
};
//Fetch .vtt files
htmlEditor.getMediaVtt = function(page, search_flag, url) {
    var searchText;
    var num_of_items = 10;
    jQuery("#selectVtt .loc-or-global").attr("disabled", true);
    searchText = window.btoa(jQuery('#selectVtt').find('.search_text').val());
    var params = { "pageNumber": page, "itemsPerPage": num_of_items, "object_types": "113", "project_id": project_id, "search_text": searchText, "taxonomy_id": 0 };
    jQuery("#selectVtt #video_thumb .inputfrom").empty();
    // jQuery(".video_conf").attr("disabled", false);

    jQuery.ajax({
        url: url,
        async: true,
        type: "GET",
        data: params,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            var d = data.data;
            if (d != null && d.length > 0) {
                jQuery(".paginate").show();
                var str = "";
                for (var i = 0; i < d.length; i++) {
                    if (htmlEditor.checkedVttPath == d[i].asset_location) {
                        str += '<div class="get-container"><div class="box-theme vtt-box active"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box checked"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="../images/default-doc.png" alt="" vttPath = "' + d[i].asset_location + '" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '">' + d[i].original_name + '</div></div></div>';
                    } else {
                        str += '<div class="get-container"><div class="box-theme vtt-box"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="../images/default-doc.png" alt="" vttPath = "' + d[i].asset_location + '" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '">' + d[i].original_name + '</div></div></div>';
                    }
                }
                jQuery("#selectVtt .inputfrom").html(str);
                if (page == 1) { //Pagination implementation
                    var navStr = "";
                    var total_images = data.totalAssets;
                    var max_page = Math.ceil(total_images / num_of_items);
                    //Pagination using plugin.
                    jQuery('#selectVtt').find(".paginate").bootpag({
                        total: max_page, // total pages
                        page: 1, // default page
                        maxVisible: 10, // visible pagination
                        leaps: true // next/prev leaps through maxVisible
                    }).off("page").off("page").on("page", function(event, num) {
                        htmlEditor.getMediaVtt(num, search_flag, url);
                    });
                }

                jQuery('.box-theme.vtt-box').off("click").on("click", function() {
                    var is_global = jQuery(this).find('img').attr('class').split('_')[1];
                    if (is_global == 1 && htmlEditor.userProjectPermission['asset.use_global'].grant == false) {
                        jQuery('.box-theme').removeClass('active');
                        swal("Permission Denied!");
                    } else {
                        var url;
                        if (jQuery(this).hasClass('active')) {
                            jQuery(this).removeClass('active');
                            jQuery(this).find('.check-box').removeClass('checked');
                            url = "";
                            htmlEditor.checkedVttPath = "";
                        } else {
                            jQuery('.box-theme').removeClass('active');
                            jQuery(this).addClass('active');
                            jQuery(this).find('.check-box').addClass('checked');
                            url = jQuery('#selectVtt .box-theme.active').find("img").attr("vttPath");
                            htmlEditor.checkedVttPath = jQuery('#selectVtt .box-theme.active').find("img").attr("vttPath");

                        }
                    }
                    htmlEditor.newVidObj.url = url;
                    htmlEditor.newVidObj.id.length = 0;
                    htmlEditor.newVidObj.id.push(jQuery('#selectVtt .box-theme.active').find("img").attr("id"));
                    if (url !== undefined && url.length) {
                        jQuery(".video_conf").attr("disabled", false);
                    } else {
                        jQuery(".video_conf").attr("disabled", true);
                    }
                });
                jQuery('.video_get_media').attr("disabled", true);

            } else {
                if (page == 1)
                    jQuery("#selectVtt .inputfrom").html("<p  class='col-md-12'>No assets Found.</p>");
                jQuery(".paginate").hide();
            }
            jQuery("#selectVtt .loc-or-global").attr("disabled", false);
        },
        error: function() {
            swal({ title: "Error!", text: "Some error occured while fetching the assets." });
        }
    });
};
htmlEditor.getLocalGlobalForAdvanceSearch = function() {
        var asset_local_global = {};
        var fetchFrom = jQuery("#selectVid .loc-or-global.active").text();
        var actives = jQuery("#selectVid .loc-or-global.active").length;
        if (actives > 1) {
            asset_local_global.local = true;
            asset_local_global.global = true;
        } else {
            if (fetchFrom.toLowerCase() === "local") {
                asset_local_global.local = true;
                asset_local_global.global = false;
            } else if (fetchFrom.toLowerCase() === "global") {
                asset_local_global.local = false;
                asset_local_global.global = true;
            } else if (fetchFrom.toLowerCase() === "all") {
                asset_local_global.local = true;
                asset_local_global.global = true;
            }
        }
        return asset_local_global;
    }
    //Fetch the videos for insertion
htmlEditor.getMediaVideo = function(page, search_flag, url, type) { //same function is used for audio also
    var request_type = "GET";
    var searchText;
    var num_of_items = 10;
    // htmlEditor.advanced_search = {};
    // htmlEditor.advanced_search.params = {};
    // htmlEditor.advanced_search.enableSearch = false;
    // htmlEditor.advanced_search.active = false;
    if (type === "forAudio") {
        jQuery("#audioModal .loc-or-global").attr("disabled", true);
        searchText = window.btoa(jQuery('#selectAudio .search_text').val());
        var params = { "pageNumber": page, "itemsPerPage": num_of_items, "object_types": "102", "project_id": project_id, "search_text": searchText, "taxonomy_id": 0 };
        jQuery("#selectAudio #audio_thumb .inputfrom").empty();
        // jQuery(".audio_conf").attr("disabled", false);
    } else if (type === 'forVideo') {
        if (htmlEditor.advanced_search.enableSearch) {
            htmlEditor.advanced_search.params.access_token = access_token;
            htmlEditor.advanced_search.params.asset_local_global = JSON.stringify(htmlEditor.getLocalGlobalForAdvanceSearch());
            htmlEditor.advanced_search.params.pageNumber = page;
            htmlEditor.advanced_search.params.itemsPerPage = num_of_items;
            htmlEditor.advanced_search.params.project_id = project_id;
            request_type = "POST";
            params = htmlEditor.advanced_search.params;

        } else {
            jQuery("#selectVid .loc-or-global").attr("disabled", true);
            searchText = window.btoa(jQuery('#selectVid .search_text').val());
            var params = { "pageNumber": page, "itemsPerPage": num_of_items, "object_types": "103", "project_id": project_id, "search_text": searchText, "taxonomy_id": 0 };
            jQuery("#selectVid #audio_thumb .inputfrom").empty();
            // jQuery(".video_conf").attr("disabled", false);
        }

    }


    jQuery.ajax({
        url: url,
        async: true,
        type: request_type,
        data: params,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            var d = data.data;
            var modalId;
            if (type === 'forVideo') {
                modalId = '#videoModal';
            } else if (type === 'forAudio') {
                modalId = '#audioModal';
            }
            if (d != null && d.length > 0) {
                jQuery(".paginate").show();
                var str = "";
                for (var i = 0; i < d.length; i++) {
                    if (type === "forAudio") {
                        if (htmlEditor.checkedAudioPath == d[i].asset_location) {
                            str += '<div class="get-container"><div class="box-theme aud-vid-box active"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box checked"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="../images/default-video.jpg" alt="" audioPath = "' + d[i].asset_location + '" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '">' + d[i].original_name + '</div></div></div>';
                        } else {
                            str += '<div class="get-container"><div class="box-theme aud-vid-box"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="../images/default-video.jpg" alt="" audioPath = "' + d[i].asset_location + '" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '">' + d[i].original_name + '</div></div></div>';
                        }
                    } else if (type === 'forVideo') {
                        if (htmlEditor.checkedVideoPath == d[i].asset_location) {
                            str += '<div class="get-container"><div class="box-theme aud-vid-box active"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box checked"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="../images/default-video.jpg" alt="" videoPath = "' + d[i].asset_location + '" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '">' + d[i].original_name + '</div></div></div>';
                        } else {
                            str += '<div class="get-container"><div class="box-theme aud-vid-box"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="../images/default-video.jpg" alt="" videoPath = "' + d[i].asset_location + '" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '">' + d[i].original_name + '</div></div></div>';
                        }
                    }

                }
                if (type === 'forVideo') {
                    jQuery("#selectVid .inputfrom").html(str);
                } else if (type === 'forAudio') {
                    jQuery("#selectAudio .inputfrom").html(str);
                }


                if (page == 1) { //Pagination implementation
                    var navStr = "";
                    var total_images = data.totalAssets;
                    var max_page = Math.ceil(total_images / num_of_items);
                    //Pagination using plugin.
                    var selector;
                    if (type === 'forVideo') {
                        selector = "#selectVid";
                    } else if (type === 'forAudio') {
                        selector = "#selectAudio";
                    }
                    jQuery(selector).find(".paginate").bootpag({
                        total: max_page, // total pages
                        page: 1, // default page
                        maxVisible: 10, // visible pagination
                        leaps: true // next/prev leaps through maxVisible
                    }).off("page").off("page").on("page", function(event, num) {
                        htmlEditor.getMediaVideo(num, search_flag, url, type);
                    });
                }
                jQuery('.box-theme.aud-vid-box').off("click").on("click", function() {
                    var is_global = jQuery(this).find('img').attr('class').split('_')[1];
                    if (is_global == 1 && htmlEditor.userProjectPermission['asset.use_global'].grant == false) {
                        jQuery('.box-theme').removeClass('active');
                        swal("Permission Denied!");
                    } else {
                        var url;
                        if (jQuery(this).hasClass('active')) {
                            jQuery(this).removeClass('active');
                            jQuery(this).find('.check-box').removeClass('checked');
                            url = "";
                            htmlEditor.checkedVideoPath = "";
                        } else {
                            jQuery('.box-theme').removeClass('active');
                            jQuery(this).addClass('active');
                            jQuery(this).find('.check-box').addClass('checked');
                            if (type === 'forAudio') {
                                url = jQuery(modalId + ' .box-theme.active').find("img").attr("audioPath");
                                htmlEditor.checkedAudioPath = jQuery(modalId + ' .box-theme.active').find("img").attr("audioPath");
                            } else if (type === 'forVideo') {
                                url = jQuery(modalId + ' .box-theme.active').find("img").attr("videoPath");
                                htmlEditor.checkedVideoPath = jQuery(modalId + ' .box-theme.active').find("img").attr("videoPath");
                            }

                        }
                    }
                    htmlEditor.newVidObj.url = url;
                    htmlEditor.newVidObj.id.length = 0;
                    htmlEditor.newVidObj.id.push(jQuery(modalId + ' .box-theme.active').find("img").attr("id"));
                    if (type === 'forVideo') {
                        if (url !== undefined && url.length) {
                            jQuery(".video_conf").attr("disabled", false);
                        } else {
                            jQuery(".video_conf").attr("disabled", true);
                        }
                    } else if (type === 'forAudio') {
                        if (url !== undefined && url.length) {
                            jQuery(".audio_conf").attr("disabled", false);
                        } else {
                            jQuery(".audio_conf").attr("disabled", true);
                        }
                    }


                });
                if (type === 'forAudio') {
                    jQuery('.audio_get_media').attr("disabled", true);
                } else if (type === 'forVideo') {
                    jQuery('.video_get_media').attr("disabled", true);
                }
            } else {
                if (page == 1)
                    if (type === 'forAudio') {
                        jQuery("#selectAudio #audio_thumb .inputfrom").html("<p  class='col-md-12'>No assets Found.</p>");
                    } else if (type === 'forVideo') {
                    jQuery("#selectVid #video_thumb .inputfrom").html("<p  class='col-md-12'>No assets Found.</p>");
                }

                jQuery(".paginate").hide();
            }
            jQuery(modalId + " .loc-or-global").attr("disabled", false);
        },
        error: function() {
            swal({ title: "Error!", text: "Some error occured while fetching the assets." });
            if (type === "forAudio") {
                jQuery("#selectAudio .loc-or-global").attr("disabled", false);
            } else if (type === "forVideo") {
                jQuery("#selectVid .loc-or-global").attr("disabled", false);
            }
        }
    });
};

htmlEditor.getAd = function(page, search_flag, url) {
    var searchText;
    var num_of_items = 10;
    var request_type = "GET";
    // if (htmlEditor.advanced_search.enableSearch) {
    //     htmlEditor.advanced_search.params.access_token = access_token;
    //     htmlEditor.advanced_search.params.asset_local_global = JSON.stringify(htmlEditor.getLocalGlobalForVttAdvanceSearch());
    //     htmlEditor.advanced_search.params.pageNumber = page;
    //     htmlEditor.advanced_search.params.itemsPerPage = num_of_items;
    //     htmlEditor.advanced_search.params.project_id = project_id;
    //     request_type = "POST";
    //     params = htmlEditor.advanced_search.params;

    // } else {
    jQuery("#selectAd .loc-or-global").attr("disabled", true);
    searchText = window.btoa(jQuery('#selectAd').find('.search_text').val());
    var params = { "pageNumber": page, "itemsPerPage": num_of_items, "object_types": "103", "project_id": project_id, "search_text": searchText, "taxonomy_id": 0 };
    jQuery("#selectAd #audio_thumb .inputfrom").empty();
    // jQuery(".audio_conf").attr("disabled", false);
    // }


    jQuery.ajax({
        url: url,
        async: true,
        type: request_type,
        data: params,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            var d = data.data;
            if (d != null && d.length > 0) {
                jQuery(".paginate").show();
                var str = "";
                for (var i = 0; i < d.length; i++) {
                    if (htmlEditor.checkedAdPath == d[i].asset_location) {
                        str += '<div class="get-container"><div class="box-theme vtt-box active"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box checked"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="../images/default-doc.png" alt="" vttPath = "' + d[i].asset_location + '" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '">' + d[i].original_name + '</div></div></div>';
                    } else {
                        str += '<div class="get-container"><div class="box-theme vtt-box"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="../images/default-doc.png" alt="" vttPath = "' + d[i].asset_location + '" id="' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '">' + d[i].original_name + '</div></div></div>';
                    }
                }
                jQuery("#selectAd .inputfrom").html(str);
                if (page == 1) { //Pagination implementation
                    var navStr = "";
                    var total_Ads = data.totalAssets;
                    var max_page = Math.ceil(total_Ads / num_of_items);
                    //Pagination using plugin.
                    jQuery('#selectAd').find(".paginate").bootpag({
                        total: max_page, // total pages
                        page: 1, // default page
                        maxVisible: 10, // visible pagination
                        leaps: true // next/prev leaps through maxVisible
                    }).off("page").off("page").on("page", function(event, num) {
                        htmlEditor.getAd(num, search_flag, url);
                    });
                }

                jQuery('.box-theme.vtt-box').off("click").on("click", function() {
                    var is_global = jQuery(this).find('img').attr('class').split('_')[1];
                    if (is_global == 1 && htmlEditor.userProjectPermission['asset.use_global'].grant == false) {
                        jQuery('.box-theme').removeClass('active');
                        swal("Permission Denied!");
                    } else {
                        var url;
                        if (jQuery(this).hasClass('active')) {
                            jQuery(this).removeClass('active');
                            jQuery(this).find('.check-box').removeClass('checked');
                            url = "";
                            htmlEditor.checkedAdPath = "";
                        } else {
                            jQuery('.box-theme').removeClass('active');
                            jQuery(this).addClass('active');
                            jQuery(this).find('.check-box').addClass('checked');
                            url = jQuery('#selectAd .box-theme.active').find("img").attr("vttPath");
                            htmlEditor.checkedAdPath = jQuery('#selectAd .box-theme.active').find("img").attr("vttPath");

                        }
                    }
                    htmlEditor.newVidObj.url = url;
                    htmlEditor.newVidObj.id.length = 0;
                    htmlEditor.newVidObj.id.push(jQuery('#selectAd .box-theme.active').find("img").attr("id"));
                    if (url !== undefined && url.length) {
                        jQuery(".audio_conf").attr("disabled", false);
                    } else {
                        jQuery(".audio_conf").attr("disabled", true);
                    }
                });
                jQuery('.video_get_media').attr("disabled", true);

            } else {
                if (page == 1)
                    jQuery("#selectAd .inputfrom").html("<p  class='col-md-12'>No assets Found.</p>");
                jQuery(".paginate").hide();
            }
            jQuery("#selectAd .loc-or-global").attr("disabled", false);
        },
        error: function() {
            swal({ title: "Error!", text: "Some error occured while fetching the assets." });
        }
    });
};

htmlEditor.getLocalGlobalForGadget = function() {
    var asset_local_global = {};
    var fetchFrom = jQuery("#selectGadget .loc-or-global.active").text();
    var actives = jQuery("#selectGadget .loc-or-global.active").length;
    if (actives > 1) {
        asset_local_global.local = true;
        asset_local_global.global = true;
    } else {
        if (fetchFrom.toLowerCase() === "local") {
            asset_local_global.local = true;
            asset_local_global.global = false;
        } else if (fetchFrom.toLowerCase() === "global") {
            asset_local_global.local = false;
            asset_local_global.global = true;
        } else if (fetchFrom.toLowerCase() === "all") {
            asset_local_global.local = true;
            asset_local_global.global = true;
        }
    }
    return asset_local_global;
}

//Fetch the list of gadgets.
htmlEditor.getGadgetList = function(page, search_flag, url) {
    jQuery("#iframeModal .loc-or-global").attr("disabled", true);
    var num_of_items = 10;
    var searchText = window.btoa(jQuery('#iframeModal').find('.search_text').val());
    var params = { "pageNumber": page, "itemsPerPage": num_of_items, "object_types": "103", "project_id": project_id, "search_text": searchText, "taxonomy_id": 0 };
    var request_type = "GET";
    jQuery("#iframeModal .listed-gadgets").empty();
    if (htmlEditor.advanced_search.enableSearch) {
        htmlEditor.advanced_search.params.access_token = access_token;
        htmlEditor.advanced_search.params.asset_local_global = JSON.stringify(htmlEditor.getLocalGlobalForGadget());
        htmlEditor.advanced_search.params.pageNumber = page;
        htmlEditor.advanced_search.params.itemsPerPage = num_of_items;
        htmlEditor.advanced_search.params.project_id = project_id;
        request_type = "POST";
        params = htmlEditor.advanced_search.params;
    }
    //    jQuery(".video_conf").attr("disabled", true);
    jQuery.ajax({
        url: url,
        async: true,
        type: request_type,
        data: params,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            var d = data.data;
            if (d != null && d.length > 0) {
                jQuery(".paginate").show();
                var str = "";
                for (var i = 0; i < d.length; i++) {
                    if (htmlEditor.checkedWidgetPath == d[i].id) {
                        str += '<div class="get-container"><div class="box-theme active"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box checked"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="../images/default-widget.png" alt="" gadgetId = "' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '">' + d[i].original_name + '</div></div></div>';
                    } else {
                        str += '<div class="get-container"><div class="box-theme"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="../images/default-widget.png" alt="" gadgetId = "' + d[i].id + '"/></div><div class="box-theme-title" title="' + d[i].original_name + '">' + d[i].original_name + '</div></div></div>';
                    }
                }
                jQuery("#iframeModal .listed-gadgets").html(str);

                if (page == 1) { //Pagination implementation
                    var navStr = "";
                    var total_images = data.totalAssets;
                    var max_page = Math.ceil(total_images / num_of_items);
                    //Pagination using plugin.
                    jQuery('#iframeModal').find(".paginate").bootpag({
                        total: max_page, // total pages
                        page: 1, // default page
                        maxVisible: 10, // visible pagination
                        leaps: true // next/prev leaps through maxVisible
                    }).off("page").on("page", function(event, num) {
                        htmlEditor.getGadgetList(num, search_flag, url);
                    });
                }

                jQuery('.box-theme').off("click").on("click", function() {
                    var is_global = jQuery(this).find('img').attr('class').split('_')[1];
                    if (is_global == 1 && htmlEditor.userProjectPermission['asset.use_global'].grant == false) {
                        jQuery('.box-theme').removeClass('active');
                        swal("Permission Denied!");
                    } else {
                        if (jQuery(this).hasClass('active')) {
                            jQuery(this).removeClass('active');
                            jQuery(this).find('.check-box').removeClass('checked');
                            htmlEditor.newGadgetObj.gadgetId.length = 0;
                            htmlEditor.checkedWidgetPath = "";
                        } else {
                            jQuery('.box-theme').removeClass('active');
                            jQuery(this).addClass('active');
                            jQuery(this).find('.check-box').addClass('checked');
                            jQuery('#iframeTerm').val("");
                            htmlEditor.newGadgetObj.gadgetId.push(jQuery('.listed-gadgets').find('.active').find("img").attr("gadgetid"));
                            htmlEditor.checkedWidgetPath = jQuery('.listed-gadgets').find('.active').find("img").attr("gadgetid");
                        }
                    }
                    if (htmlEditor.newGadgetObj.gadgetId.length) {
                        jQuery(".iframe_conf").attr("disabled", false);
                    } else {
                        jQuery(".iframe_conf").attr("disabled", true);
                    }

                });
                //                jQuery('.video_get_media').attr("disabled", true);
            } else {
                if (page == 1)
                    jQuery("#iframeModal .listed-gadgets").html("<p  class='col-md-12'>No assets Found.</p>");
                jQuery(".paginate").hide();
            }
            jQuery("#iframeModal .loc-or-global").attr("disabled", false);
        },
        error: function() {
            swal({ title: "Error!", text: "Some error occured while fetching the assets." });
        }
    });
}

//assign guids to every element in the editor

htmlEditor.generateIds = function(elem) {
    var numOfelem = elem.length;
    for (var i = 0; i < numOfelem; i++) {
        var checkForId = elem[i].hasAttribute("id") && elem[i].id.length; //if id against this element exists
        var checkNATag = elem[i].tagName === "A" || elem[i].tagName === "STRONG" || elem[i].tagName === "EM" || elem[i].tagName === "SUB" || elem[i].tagName === "SUP" || elem[i].tagName === "U" || elem[i].tagName === "math" || jQuery(elem[i]).hasClass('math-tex') ? true : false;

        var checkGadget = jQuery(elem[i]).parents(".converted").length ? true : false; //If placeholder
        if (!(checkForId || checkNATag || checkGadget)) {
            elem[i].id = guid()();
        }
    }
}