//Convert the element in "_toBeConverted" into div
htmlEditor.resetQuadData = function() {
    htmlEditor.aContent = [];
    htmlEditor.cCounter = 0;

}

htmlEditor.userProjectPermission = {};
htmlEditor.curr_iFrameEntityType = undefined;

htmlEditor.currentUserPermissions = function() {
    return jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.projectDetailWithPermissionsAPI,
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            htmlEditor.userProjectPermission = data.data.project_permissions; // Save permission object locally.
        }
    })
}

htmlEditor.resetQuadData();
htmlEditor.VideoModelBool = true;
htmlEditor.addWidgetStyleScript = true;
htmlEditor.saveSelection;

htmlEditor.convertFrameToDiv = function() {

    for (var i = 0; i < _toBeConverted.length; i++) {

        jQuery(".cke_wysiwyg_frame").contents().find(_toBeConverted[i]).each(function() {
            var thisFrame = jQuery(this);
            var dataPos = thisFrame.attr('data-pos');
            var theSrc = "";
            var trackSrc = "";
            var poster = "";
            var patt_name = "";
            var id = thisFrame.attr("id") === undefined ? "" : thisFrame.attr("id");
            // //console.log(id);
            var theClasses = thisFrame.attr("class") === undefined ? "" : thisFrame.attr("class");
            var divContent = "";
            var scrolling;
            // Functions to generate the placeholders for elements mentioned in _toBeConverted variable.
            function forIframe() {

                theSrc = thisFrame.attr("src") === undefined ? thisFrame.attr("data") : thisFrame.attr("src");
                var isobject = thisFrame.prop("tagName") === "IFRAME" ? false : thisFrame.hasClass("converted") ? thisFrame.attr("isobject") : true;
                var profileSettings = htmlEditor.getProfileSettings(thisFrame);
                var gadgetID = thisFrame.attr('gadget-id') ? thisFrame.attr('gadget-id') : thisFrame.attr('data-gadget-id'),
                    scrolling = thisFrame.attr("scrolling") === undefined ? "yes" : thisFrame.attr("scrolling");
                var frameHeight = thisFrame.attr('height');
                var frameWidth = thisFrame.attr('width');
                patt_name = "Gadget";
                //                divContent = '<div type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '" contenteditable="false" src="' + theSrc + '" isobject="' + isobject + '" id="' + id + '" scrolling="' + scrolling + '" height="' + theHeight + '" width="' + theWidth + '" ' + profileSettings + ' data-gadget-id="' + gadgetID + '" style="height:' + theHeight + 'px;width:' + theWidth + 'px;"><div class="lm-rochak-out" ' + profileSettings + '><div class="lm-rochak" ' + profileSettings + '><div ' + profileSettings + '><div class="fa fa-file-code-o" ' + profileSettings + '> </div></div><strong ' + profileSettings + '>Gadget</strong><p class="mode-text"' + profileSettings + '>(Can be viewed in the test mode)</p><span class="url-link" ' + profileSettings + '>' + patt_name + ' URL: ' + theSrc + '</span></div></div></div>';
                divContent = '<div pattern-type="gadget" type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '" contenteditable="false" src="' + theSrc + '" isobject="' + isobject + '" id="' + id + '" data-pos="' + dataPos + '" scrolling="' + scrolling + '" height="' + frameHeight + '" width="' + frameWidth + '" ' + profileSettings + ' data-gadget-id="' + gadgetID + '" style="height:' + frameHeight + 'px; width:' + frameWidth + 'px;"><div id="' + id + 'child' + '" class="lm-rochak-out" ' + profileSettings + '><div id="' + id + 'child1' + '" class="lm-rochak" ' + profileSettings + '><div ' + profileSettings + ' id="' + id + 'child2' + '"><div id="' + id + 'child3' + '" class="fa fa-file-code-o" ' + profileSettings + '> </div></div><strong id="' + id + 'child4' + '" ' + profileSettings + '>Gadget</strong><p id="' + id + 'child5' + '" class="mode-text"' + profileSettings + '>(Can be viewed in the test mode)</p><span id="' + id + 'child6' + '" class="url-link" ' + profileSettings + '>' + patt_name + ' URL: ' + theSrc + '</span></div></div></div>';

            }

            function forVideo() {
                theSrc = thisFrame.find("source").attr("src") === undefined ? thisFrame.attr("src") : thisFrame.find("source").attr("src");
                trackSrc = thisFrame.find("track").attr("src") === undefined ? thisFrame.attr("src") : thisFrame.find("track").attr("src");

                poster = !thisFrame.is("[poster]") ? "" : thisFrame.attr("poster");
                patt_name = "Video";
                var profileSettings = htmlEditor.getProfileSettings(thisFrame);
                if (poster.length) {
                    poster = poster.replace("video", "images");
                    dataPos
                    divContent = '<div pattern-type="video" type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '" contenteditable="false" trackSrc="' + trackSrc + '" src="' + theSrc + '" id="' + id + '" poster="' + poster + '" data-pos="' + dataPos + '" style="height: ' + theHeight + 'px; width:' + theWidth + 'px;background-image:url(' + poster + '); background-size: 100%;background-repeat: no-repeat;" ' + profileSettings + '><div id="' + id + 'child" class="lm-rochak-out" ' + profileSettings + '><div id="' + id + 'child1" class="lm-rochak" ' + profileSettings + '><div ' + profileSettings + '><div id="' + id + 'child2" class="fa fa-file-video-o" ' + profileSettings + '> </div></div><strong id="' + id + 'child3">Video</strong><p   id="' + id + 'child4" class="mode-text" ' + profileSettings + '>(Can be viewed in the test mode))</p><span id="' + id + 'child5" class="url-link" ' + profileSettings + '>' + patt_name + ' URL: ' + theSrc + '</span></div><div></div></div></div>';
                } else {
                    divContent = '<div pattern-type="video" type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '"contenteditable="false" trackSrc="" src="' + theSrc + '" id="' + id + '" data-pos="' + dataPos + '" style="height: ' + theHeight + 'px; width:' + theWidth + 'px;" ' + profileSettings + '><div  id="' + id + 'child" class="lm-rochak-out" ' + profileSettings + '><div id="' + id + 'child1" class="lm-rochak" ' + profileSettings + '><div id="' + id + 'child2" ' + profileSettings + '><div id="' + id + 'child3" class="fa fa-file-video-o" ' + profileSettings + '> </div></div><strong id="' + id + 'child4" ' + profileSettings + '>Video</strong><p  id="' + id + 'child5" class="mode-text" ' + profileSettings + '>(Can be viewed in the test mode))</p><span id="' + id + 'child6" class="url-link" ' + profileSettings + '>' + patt_name + ' URL: ' + theSrc + '</span></div><div></div></div></div>';
                }
            }

            function forAudio() {
                theSrc = thisFrame.find("source").attr("src") === undefined ? thisFrame.attr("src") : thisFrame.find("source").attr("src");
                trackSrc = thisFrame.find("track").attr("src") === undefined ? thisFrame.attr("src") : thisFrame.find("track").attr("src");
                patt_name = "Audio";
                var profileSettings = htmlEditor.getProfileSettings(thisFrame);

                divContent = '<div pattern-type="audio" type=' + _toBeConverted[i] + ' contenteditable="false" src="' + theSrc + '"  trackSrc="' + trackSrc + '" data-pos="' + dataPos + '" class="converted" id="' + id + '" ' + profileSettings + '><div  id="' + id + 'child1" class="lm-rochak-out" ' + profileSettings + '><div  id="' + id + 'child2" class="lm-rochak" ' + profileSettings + '><div  id="' + id + 'child3" ' + profileSettings + '><div  id="' + id + 'child4" class="fa fa-file-audio-o" ' + profileSettings + '> </div></div><strong  id="' + id + 'child5" ' + profileSettings + '>Audio</strong><p  id="' + id + 'child6" class="mode-text" ' + profileSettings + '>(Can be played in the test mode)</p><span  id="' + id + 'child7" class="url-link" ' + profileSettings + '>' + patt_name + ' URL: ' + theSrc + '</span></div><div></div></div></div>';
            }

            function forObject() {
                theSrc = thisFrame.attr("data");
                patt_name = "Object";
                var datauuid = thisFrame.attr("data-uuid");
                var paramName = thisFrame.children().attr("name");
                var paramVal = thisFrame.children().attr("value");
                var profileSettings = htmlEditor.getProfileSettings(thisFrame);

                divContent = '<div pattern-type="gadget" type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '" contenteditable="false" src="' + theSrc + '" id="' + id + '" data-pos="' + dataPos + '" datauuid="' + datauuid + '" paramName="' + paramName + '" paramVal= "' + paramVal + '" ' + profileSettings + '><div  id="' + id + 'child" class="lm-rochak-out" ' + profileSettings + '><div  id="' + id + 'child1" class="lm-rochak" ' + profileSettings + '><div  id="' + id + 'child2" ' + profileSettings + '><div  id="' + id + 'child3" class="fa fa-file-code-o" ' + profileSettings + '> </div></div><strong id="' + id + 'child4" ' + profileSettings + '>Gadget</strong><p  id="' + id + 'child5" class="mode-text" ' + profileSettings + '>(Can be viewed in the test mode)</p><span id="' + id + 'child6" class="url-link" ' + profileSettings + '>' + patt_name + ' URL: ' + theSrc + '</span></div></div></div>';
            }

            function forQuadType() {
                theSrc = thisFrame.attr("data-qID");
                scrolling = thisFrame.attr("scrolling") === undefined ? "yes" : thisFrame.attr("scrolling");
                patt_name = "Quad";
                var theHeight = thisFrame.attr("height") !== undefined ? thisFrame.attr("height") : thisFrame.height();
                //console.log(theHeight);
                var theWidth = thisFrame.attr("width") !== undefined ? thisFrame.attr("width") : thisFrame.width();
                var qID = thisFrame.attr('data-qID') === undefined ? "" : thisFrame.attr('data-qID');
                var iframeEntityType = thisFrame.attr('data-iframeEntityType') === undefined ? "" : thisFrame.attr('data-iframeEntityType');
                var iframeEntityTypeText = iframeEntityType == 3 ? ' Question' : iframeEntityType == 2 ? ' Assessment' : '';
                var profileSettings = htmlEditor.getProfileSettings(thisFrame);
                divContent = '<div pattern-type="Quad" type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '" contenteditable="false" data-qID="' + qID + '" data-iframeEntityType ="' + iframeEntityType + '"  src="' + theSrc + '" id=' + id + ' data-pos=' + dataPos + ' scrolling="' + scrolling + '" ' + profileSettings + ' height="' + theHeight + '" width="' + theWidth + '"><div class="lm-rochak-out" ' + profileSettings + '><div class="lm-rochak" ' + profileSettings + '><div ' + profileSettings + '><div class="fa fa-file-code-o" ' + profileSettings + '> </div></div><strong ' + profileSettings + '>Quad</strong><p class="mode-text" ' + profileSettings + '>(Can be viewed in the test mode)</p><span class="url-link" ' + profileSettings + '>' + patt_name + iframeEntityTypeText + ': ' + qID + '</span></div></div></div>';
            }

            function forAssessmentType() {

                theSrc = thisFrame.attr("src") === undefined ? thisFrame.attr("data") : thisFrame.attr("src");
                var dataPath = thisFrame.attr("data-path") === undefined ? "" : thisFrame.attr("data-path");
                scrolling = thisFrame.attr("scrolling") === undefined ? "yes" : thisFrame.attr("scrolling");
                var patt_name = "";

                var iframeEntityType = thisFrame.attr('data-iframeentitytype') === undefined ? "" : thisFrame.attr('data-iframeentitytype');
                var dataAssessmentId = thisFrame.attr('data-assessment-id') === undefined ? "" : thisFrame.attr('data-assessment-id');
                var dataQuestionId = thisFrame.attr('data-assessment-question-id') === undefined ? "" : thisFrame.attr('data-assessment-question-id');
                var dataQuestionNumber = thisFrame.attr('data-assessment-question-number') === undefined ? "" : thisFrame.attr('data-assessment-question-number');
                var assementTitle = thisFrame.attr('data-assessment-title') === undefined ? '' : thisFrame.attr('data-assessment-title');
                var questionTitle = thisFrame.attr('data-question-title') === undefined ? '' : thisFrame.attr('data-question-title');
                if (parseInt(iframeEntityType) === 2) {
                    patt_name = "Assessment: " + assementTitle;
                } else if (parseInt(iframeEntityType) === 3) {
                    patt_name = "Question: " + questionTitle;
                }

                //console.info("XXXXX " + patt_name);
                var profileSettings = htmlEditor.getProfileSettings(thisFrame);
                divContent = '<div pattern-type="assessment"  type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '"contenteditable="false" data-iframeEntityType ="' + iframeEntityType + '" id="' + id + '" data-pos="' + dataPos + '" scrolling="' + scrolling + '" ' + profileSettings + ' height="' + theHeight + '" width="' + theWidth + '" data-assessment-id="' + dataAssessmentId + '" data-assessment-question-id="' + dataQuestionId + '" data-assessment-question-number="' + dataQuestionNumber + '" data-assessment-title="' + assementTitle + '" data-question-title="' + questionTitle + '" src="' + theSrc + '" data-path="' + dataPath + '"><div  id="' + id + 'child" class="lm-rochak-out" ' + profileSettings + '><div id="' + id + 'child1" class="lm-rochak" ' + profileSettings + '><div  id="' + id + 'child2" ' + profileSettings + '><div  id="' + id + 'child3" class="fa fa-file-code-o" ' + profileSettings + '> </div></div><strong id="' + id + 'child4" ' + profileSettings + '>Assessment</strong><p  id="' + id + 'child5" class="mode-text" ' + profileSettings + '>(Can be viewed in the test mode)</p><span  id="' + id + 'child6" class="url-link" ' + profileSettings + '>' + patt_name + '</span></div></div></div>';
            }

            function forExWidgets() {
                //console.log("forExWidgets==============");
                //Build mode
                theSrc = thisFrame.attr("src");
                /*if(theSrc ==="undefined"){
                 forExWidgets();
                 }*/
                //console.log(theSrc);
                patt_name = "Widget";
                //theHeight = 150; // test purpose
                var dataPath = thisFrame.attr("data-path"),
                    widgetType = thisFrame.attr('widget-type') ? thisFrame.attr('widget-type') : thisFrame.attr('data-widget-type'),
                    configPath = "",
                    configFrameEl = jQuery('#exwidgetModal').find('iframe'),
                    configModalHeaderEl = jQuery('#exwidgetModal').find('#modalBoxTitle'),
                    widget_id = thisFrame.attr('widget-id') ? thisFrame.attr('widget-id') : thisFrame.attr('data-widget-id'),
                    profileSettings = htmlEditor.getProfileSettings(thisFrame);

                if (widget_id) {
                    //console.log("widget_id");
                    try {
                        //console.log("try");
                        configPath = widgetScope.configPage[widget_id];
                    } catch (exc) {
                        //console.log(exc);
                    }

                }

                configFrameEl.attr('src', configPath); //Sets widgets dynamic frame path

                divContent = '<div type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '" contenteditable="false" src="' + theSrc + '" id="' + id + '" data-pos="' + dataPos + '" data-path="' + dataPath + '" data-widget-id="' + widget_id + '" data-widget-type="' + widgetType + '" ' + profileSettings + ' height="' + theHeight + '" width="' + 740 + '"><div id="' + id + 'child1" class="lm-rochak-out" ' + profileSettings + '><div id="' + id + 'child2" class="lm-rochak" ' + profileSettings + '><div id="' + id + 'child3" ' + profileSettings + '><div id="' + id + 'child4" class="fa fa-file-code-o" ' + profileSettings + '> </div></div><strong id="' + id + 'child5" ' + profileSettings + '>Widget</strong><p id="' + id + 'child6" class="mode-text" ' + profileSettings + '>(Can be viewed in the test mode)</p><span id="' + id + 'child7" class="url-link" ' + profileSettings + '>Type: ' + (widgetType.toUpperCase()) + ' </span></div></div></div>';
            }

            function forBrightCoveIframe() {
                theSrc = thisFrame.attr("src") === undefined ? thisFrame.attr("data") : thisFrame.attr("src");
                var isobject = thisFrame.prop("tagName") === "IFRAME" ? false : thisFrame.hasClass("converted") ? thisFrame.attr("isobject") : true,
                    profileSettings = htmlEditor.getProfileSettings(thisFrame);

                var brightcoveID = thisFrame.attr('src').split('videoId=')[1];
                scrolling = thisFrame.attr("scrolling") === undefined ? "yes" : thisFrame.attr("scrolling");
                //console.info(brightcoveID);
                brightcoveID = brightcoveID === undefined ? "" : brightcoveID;

                patt_name = "Brightcove video";
                divContent = '<div type="' + _toBeConverted[i] + '" class="converted ' + theClasses + '" contenteditable="false" src="' + theSrc + '" isobject=' + isobject + ' id="' + id + '" data-pos="' + dataPos + '" scrolling="' + scrolling + '" height="' + theHeight + '" width="' + theWidth + '" ' + profileSettings + ' data-brightcove-id="' + brightcoveID + '" style="height:' + theHeight + 'px;width:' + theWidth + 'px;"><div class="lm-rochak-out" ' + profileSettings + '><div class="lm-rochak" ' + profileSettings + '><div ' + profileSettings + '><div class="fa fa-file-code-o" ' + profileSettings + '> </div></div><strong ' + profileSettings + '>Brightcove video</strong><p class="mode-text"' + profileSettings + '>(Can be viewed in the test mode)</p><span class="url-link" ' + profileSettings + '>' + patt_name + ' Id:' + brightcoveID + '</span></div></div></div>';
            }

            switch (_toBeConverted[i]) {
                case ".gadget":
                    var theHeight = thisFrame.attr("height") !== undefined ? thisFrame.attr("height") : 150;
                    //console.log(theHeight);
                    var theWidth = thisFrame.attr("width") !== undefined ? thisFrame.attr("width") : 700;
                    //console.log(theWidth);
                    forIframe();
                    break;
                case ".ext-gadget":
                    var theHeight = thisFrame.attr("height") !== undefined ? thisFrame.attr("height") : 150;
                    //console.log(theHeight);
                    var theWidth = thisFrame.attr("width") !== undefined ? thisFrame.attr("width") : 700;
                    //console.log(theWidth);
                    forIframe();
                    break;

                case "video":
                    var theHeight = thisFrame.attr("height") !== undefined ? thisFrame.attr("height") : thisFrame.height();
                    //console.log(theHeight);
                    var theWidth = thisFrame.attr("width") !== undefined ? thisFrame.attr("width") : thisFrame.width();
                    //console.log(theWidth);
                    forVideo();
                    break;
                case "audio":
                    var theHeight = thisFrame.attr("height") !== undefined ? thisFrame.attr("height") : 50;
                    var theWidth = thisFrame.attr("width") !== undefined ? thisFrame.attr("width") : 700;
                    forAudio();
                    break;
                case ".gadget[data]":
                    var theHeight = thisFrame.attr("height") !== undefined ? thisFrame.attr("height") : thisFrame.height();
                    //console.log(theHeight);
                    var theWidth = thisFrame.attr("width") !== undefined ? thisFrame.attr("width") : thisFrame.width();
                    //console.log(theWidth);
                    forObject();
                    break;

                case ".widget":
                    var theHeight = thisFrame.attr("height") !== undefined ? thisFrame.attr("height") : thisFrame.height();
                    //console.log(theHeight);
                    var theWidth = thisFrame.attr("width") !== undefined ? thisFrame.attr("width") : thisFrame.width();
                    //console.log(theWidth);
                    forQuadType();
                    //forAssessmentType();
                    break;

                case "iframe.exwidget":
                    var theHeight = thisFrame.attr("height") !== undefined ? thisFrame.attr("height") : thisFrame.height();
                    //console.log(theHeight);
                    var theWidth = thisFrame.attr("width") !== undefined ? thisFrame.attr("width") : thisFrame.width();
                    //console.log(theWidth);
                    forExWidgets();
                    break;

                case ".brightCoveVideo":
                    var theHeight = thisFrame.attr("height") !== undefined ? thisFrame.attr("height") : thisFrame.height();
                    //console.log(theHeight);
                    var theWidth = thisFrame.attr("width") !== undefined ? thisFrame.attr("width") : thisFrame.width();
                    //console.log(theWidth);
                    forBrightCoveIframe();
                    break;
                case ".assessment":
                    var theHeight = thisFrame.attr("height") !== undefined ? thisFrame.attr("height") : thisFrame.height();
                    //console.log(theHeight);
                    var theWidth = thisFrame.attr("width") !== undefined ? thisFrame.attr("width") : thisFrame.width();
                    forAssessmentType();
                    break;
                default:
                    break;
            }
             thisFrame.replaceWith(divContent);
            // if (thisFrame.next().prop("tagName") === "P") {
            //     thisFrame.replaceWith(divContent);
            // } else {
            //     thisFrame.replaceWith(divContent + "<p>&nbsp;</p>");
            // }
        })
    }
    //htmlEditor.bindEvents();
    //CKEDITOR.instances.editor1.updateElement();
};

//Convert div back to its element
htmlEditor.convertDivToFrame = function(selectFrame) {
    jQuery(selectFrame).find("div.converted").each(function() {
        var thisDiv = jQuery(this);
        var theSrc = thisDiv.attr("src");
        var track = thisDiv.attr("trackSrc");
        var poster = !thisDiv.is("[poster]") ? "" : thisDiv.attr("poster");
        var theId = thisDiv.attr("id");
        var allowScroll = thisDiv.attr("scrolling");
        var theHeight = parseInt(thisDiv.attr("height"));
        //console.log(theHeight);
        var vidHeight = thisDiv.height();
        var vidWidth = thisDiv.width();
        var theWidth = parseInt(thisDiv.attr("width"));
        //console.log(theWidth);
        thisDiv.removeClass("converted"); // Remove the identifier class.
        var theClasses = thisDiv.attr("class");
        var elem = thisDiv.attr("type");
        var origElem = "";
        var profileSettings = htmlEditor.getProfileSettings(thisDiv);

        function forIframe() {

            title = "Gadget";
            // var gadgetID = thisDiv.attr('gadget-id') ? thisDiv.attr('gadget-id') : thisDiv.attr('data-gadget-id');

            if (thisDiv.attr('data-gadget-id').length) {

                origElem = '<iframe data-pos="' + thisDiv.attr('data-pos') + '" id="' + theId + '" name="frame' + theId + '" class="gadget" src="' + theSrc + '" title="' + title + '" style="border:none;overflow:auto;" height="' + theHeight + '" width="' + theWidth + '" data-gadget-id="' + thisDiv.attr('data-gadget-id') + '"></iframe>';
            } else {

                origElem = '<iframe id="' + theId + '" name="frame' + theId + '" class="ext-gadget" src="' + theSrc + '" title="' + title + '" style="border:none;overflow:auto;" height="' + theHeight + '" width="' + theWidth + '" data-gadget-id=""></iframe>';
            }


        }

        function forVideo() {
            if (poster.length) {
                origElem = '<section style="width:' + vidWidth + 'px;"><video id="' + theId + '" class="' + theClasses + '" height="' + vidHeight + '" width="' + vidWidth + '" poster="' + poster + '" controls><source src="' + theSrc + '" type="video/mp4"><track src="' + track + '" kind="subtitles"></video></section>';
            } else {
                origElem = '<section style="width:' + vidWidth + 'px;"><video id="' + theId + '" class="' + theClasses + '" height="' + vidHeight + '" width="' + vidWidth + '" controls><source src="' + theSrc + '" type="video/mp4"><track src="' + track + '" kind="subtitles"></video></section>';
            }
        }

        function forAudio() {
            origElem = '<audio id=' + theId + ' class="' + theClasses + '" controls><source src="' + theSrc + '" type="audio/mp3" /><track src="' + track + '" kind="subtitles" /></audio>';
        }

        function forObject() {
            var datauuid = thisDiv.attr("datauuid");
            var paramName = thisDiv.attr("paramName");
            var paramVal = thisDiv.attr("paramVal");
            origElem = '<object type="text/html" id="' + theId + '" data-uuid="' + datauuid + '" class="' + theClasses + '" data="' + theSrc + '"><param value="' + paramVal + '" name="' + paramName + '"/>Fallback for Gadgets</object>';
        }
        var qID = '';

        function forQuadType() {

            qID = thisDiv.attr('data-qID');
            var iframeEntityType = thisDiv.attr('data-iframeEntityType') === undefined ? "" : thisDiv.attr('data-iframeEntityType');
            //var source = clientUrl + '/widgets/quad/questionPreview.html';
            var source = s3_domain_url + '/widgets/quad/questionPreview.html';
            theSrc = source;
            htmlEditor.getQuadResponse(qID, iframeEntityType);
            //console.log(htmlEditor.switch_btn_state);
            var jPath = jQuery.trim(htmlEditor.dataPath.path);
            var objHeight = thisDiv.attr('height') === undefined ? thisDiv.height() : thisDiv.attr('height');
            //origElem = '<object id="' + theId + '" name="frame' + theId + '" class="' + theClasses + '" data="' + theSrc + '" height="auto" width="' + theWidth + '" data-qID="' + qID + '" data-path="' + jPath + '" data-iframeEntityType="' + iframeEntityType + '" ' + profileSettings + '></object>';
            origElem = '<object id="' + theId + '" name="frame' + theId + '" class="' + theClasses + '" data="' + theSrc + '" style="border:none;overflow:auto;height:' + objHeight + 'px;" data-qID="' + qID + '" data-path="' + jPath + '" data-iframeEntityType="' + iframeEntityType + '">Fallback for assessment</object>';

        }


        function forExWidgets() {
            //Preview mode

            var widgetType = thisDiv.attr("widget-type") ? thisDiv.attr("widget-type") : thisDiv.attr("data-widget-type"),
                dataPath = thisDiv.attr("data-path"),
                widget_id = thisDiv.attr('widget-id') ? thisDiv.attr('widget-id') : thisDiv.attr('data-widget-id'),
                playerPath = widgetScope.launchPage[widget_id],
                title = "Widget" + " " + widgetType;
            title = title.toLowerCase().replace(/\b[a-z]/g, function(title) {
                return title.toUpperCase();
            });
            //console.log(title);
            if (widget_id) {
                playerPath = widgetScope.launchPage[widget_id];
                title = "Widget" + " " + widgetType;
                title = title.toLowerCase().replace(/\b[a-z]/g, function(title) {
                    return title.toUpperCase();
                });
            }
            origElem = '<iframe id="' + theId + '" name="frame' + theId + '" class="' + theClasses + '" title="' + title + '"src="' + playerPath + '" style="frameBorder:0; border:0; " data-path="' + dataPath + '" data-widget-id="' + widget_id + '" data-widget-type="' + widgetType + '"height="' + theHeight + '" width="' + theWidth + '"></iframe>';
        }

        function BrightCoveVideo() {

            var brightcoveID = thisDiv.attr('brightcove-id') ? thisDiv.attr('brightcove-id') : thisDiv.attr('data-brightcove-id');
            origElem = '<iframe id="' + theId + '" name="frame' + theId + '" class="' + theClasses + '" src="' + theSrc + '" style="border:none;overflow:auto;"  height="' + theHeight + '" width="' + theWidth + '"></iframe>';


        }

        function forAssessment() {

            assessmentID = thisDiv.attr("data-assessment-id");

            questionID = thisDiv.attr("data-assessment-question-id");
            questionNumber = thisDiv.attr("data-assessment-question-number");
            var iframeEntityType = thisDiv.attr('data-iframeEntityType') === undefined ? "" : thisDiv.attr('data-iframeEntityType');

            jPath = thisDiv.attr("data-path");
            title = "Assessment Widget";
            /*if (iframeEntityType == "2") {
             assesmentTitle = thisDiv.attr("data-assessment-title") === undefined ? "" : thisDiv.attr('data-assessment-title');
             //console.log(assesmentTitle);
             origElem = '<iframe id="' + theId + '" name="frame' + theId + '" title="' + title + '" class="' + theClasses + '" src="' + theSrc + '" style="border:none;overflow:auto;" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true" data-assessment-title="' + assesmentTitle + '" data-assessment-id="' + assessmentID + '" data-path="' + jPath + '" data-iframeEntityType="' + iframeEntityType + '" height="550" width="550"></iframe>';
             }
             else*/
            if (iframeEntityType == "3") {
                QuestionTitle = thisDiv.attr("data-question-title") === undefined ? "" : thisDiv.attr('data-question-title');
                origElem = '<iframe id="' + theId + '" name="frame' + theId + '" title="' + title + '" class="' + theClasses + '" src="' + theSrc + '" style="border:none;overflow:auto;"  data-question-title="' + QuestionTitle + '" data-assessment-id="' + assessmentID + '"data-assessment-question-id="' + questionID + '"data-assessment-question-number="' + questionNumber + '" data-path="' + jPath + '" data-iframeEntityType="' + iframeEntityType + '" height="480" width="700"></iframe>';
            } else {
                assesmentTitle = thisDiv.attr("data-assessment-title") === undefined ? "" : thisDiv.attr('data-assessment-title');
                origElem = '<iframe id="' + theId + '" name="frame' + theId + '" title="' + title + '" class="' + theClasses + '" src="' + theSrc + '" style="border:none;overflow:auto;"  data-assessment-title="' + assesmentTitle + '" data-assessment-id="' + assessmentID + '" data-path="' + jPath + '" data-iframeEntityType="' + iframeEntityType + '" height="480" width="700"></iframe>';
            }

        }

        function forMultipleVideo() {

            var MultipleVideoId = thisDiv.attr('data-multiplevideo-id') ? thisDiv.attr('data-multiplevideo-id') : thisDiv.attr('data-multiplevideo-id');
            var videoTitle = thisDiv.attr('data-video-title') === undefined ? '' : thisDiv.attr('data-video-title');

            var dataMutipleVideoPath = thisDiv.attr('data-multipleVideo-path');
            origElem = '<div id="' + theId + '"class="' + theClasses + '"style="cursor:pointer;border:none;overflow:auto;color: #1064bd;" data-video-title="' + videoTitle + '" data-multipleVideo-path="' + dataMutipleVideoPath + '" data-multiplevideo-id="' + MultipleVideoId + '" height="350" width="700">Open Playlist <i class="fa fa-file-video-o"></i></div>';

        }

        switch (elem) {
            case ".gadget":
                forIframe();
                break;
            case ".ext-gadget":
                forIframe();
                break;
            case "video":
                forVideo();
                break;
            case "audio":
                forAudio();
                break;
            case ".gadget[data]":
                forObject();
                break;

            case ".widget":
                forQuadType();
                break;

            case "iframe.exwidget":
                forExWidgets();
                break;
            case ".brightCoveVideo":
                BrightCoveVideo();
                break;
            case ".assessment":
                forAssessment();
                break;
            default:
                break;
        }
        thisDiv.replaceWith(origElem);


        if (elem == '.exwidget') { // for external widgets
            loadExternalWidget();
        }

        if (elem == 'iframe.widget') {
            var objRef = (jQuery(selectFrame).contents().find('#' + theId))[0];

            //htmlEditor.getQuadResponse(qID);
            if (objRef !== undefined) {
                try {
                    var jData = JSON.parse(htmlEditor.dataPath.data),
                        jPath = jQuery.trim(htmlEditor.dataPath.path);
                    htmlEditor.aContent.push({
                        'el': objRef,
                        'data': jData,
                        'path': jPath
                    });
                } catch (exc) {
                    htmlEditor.aContent.push({
                        'el': objRef,
                        'data': '',
                        'path': jPath
                    });
                }
            }

            if (jQuery(selectFrame).contents().find('.widget').length == htmlEditor.aContent.length) {

                var updateFrameCont = function() {
                    var frameEl = htmlEditor.aContent[htmlEditor.cCounter].el,
                        frameData = htmlEditor.aContent[htmlEditor.cCounter].data,
                        frameDataPath = htmlEditor.aContent[htmlEditor.cCounter].path;


                    try {
                        if (frameData.length != 0) {
                            jQuery(frameEl).attr('data-path', frameDataPath);
                        }

                        htmlEditor.cCounter++;
                        if (htmlEditor.cCounter < jQuery('#testCont').contents().find('.widget').length) {
                            //setTimeout(function(){
                            updateFrameCont();
                            //},500);
                        } else {
                            htmlEditor.resetQuadData();
                            /*htmlEditor.aContent = [];
                             htmlEditor.cCounter = 0;*/
                        }
                    } catch (exc) {
                        //setTimeout(function(){
                        updateFrameCont();
                        //},500);
                    }
                }
                updateFrameCont();
            }
        }
    })
};

htmlEditor.getProfileSettings = function(el) {
    var data_profile_deliveryformat = el.attr('data-profile-deliveryformat');
    var data_parent_added_profile = el.attr('data-parent-added-profile');

    var hasProfileSettings = (typeof data_profile_deliveryformat !== undefined || !data_profile_deliveryformat) ? "data-profile-deliveryformat = '" + data_profile_deliveryformat + "'" : false,
        isProfileAddedByParent = (typeof data_parent_added_profile != undefined || !data_parent_added_profile) ? "data-parent-added-profile = '" + data_parent_added_profile + "'" : false;

    return hasProfileSettings + ' ' + isProfileAddedByParent;
}

//Show the settings option on the div element created.
htmlEditor.showSettingForFrame = function(frame) {

        var the_editor = jQuery(".cke_wysiwyg_frame").contents().find("body");
        try {
            htmlEditor.removeFrameSettings();
        } catch (exc) {

        }
        //console.log(the_editor.removeClass("highlightSelected"));
        var ht = frame.height(); // height of the gadget or the target elem.
        var wide = frame.width(); // width of the target element.
        var offset = frame.offset(); // Offset of the target element.

        var type = frame.attr("type");
        var dataWidgetType = frame.attr("data-widget-type");
        var dataPos = frame.attr("data-pos");

        var overlay = '';
        if (frame.hasClass('widget')) {
            overlay = '<div class="iframe-setting" style="top:' + offset.top + 'px; left: ' + offset.left + 'px; width: ' + wide + 'px; height:' + ht + 'px" data-pos=' + dataPos + '><div class="toolBoxLarge to-del" contenteditable="false"><span class="glyphicon glyphicon-trash tool" title="Delete"></span> DELETE</div><div class="toolBoxLarge to-set" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> EDIT </div><div class="toolBoxLarge to-refresh" contenteditable="false"><span class="glyphicon glyphicon-refresh" title=""></span> REFRESH </div></div>';
        } else if (frame.hasClass('exwidget')) {
            if (dataWidgetType == "flashcard") {
                htmlEditor.newFlashcardDataObj = {};
                //As import functionality is only for flashcard
                overlay = '<div class="iframe-setting" style="top:' + offset.top + 'px; left: ' + offset.left + 'px; width: ' + wide + 'px; height:' + ht + 'px" data-pos=' + dataPos + '><div class="toolBoxLarge to-del" contenteditable="false"><span class="glyphicon glyphicon-trash tool" title="Delete"></span> DELETE</div><div class="toolBoxLarge to-set" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> EDIT </div> <div class="toolBoxLarge to-copy" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> REUSE EXISTING </div></div>';
                htmlEditor.newFlashcardDataObj.data_widget_type = frame.attr("data-widget-type");
                htmlEditor.newFlashcardDataObj.data_widget_id = frame.attr('data-widget-id');
                htmlEditor.newFlashcardDataObj.id = frame.attr('id');
                htmlEditor.newFlashcardDataObj.frame = frame;
            } else {
                overlay = '<div class="iframe-setting" style="top:' + offset.top + 'px; left: ' + offset.left + 'px; width: ' + wide + 'px; height:' + ht + 'px" data-pos=' + dataPos + '><div class="toolBoxLarge to-del" contenteditable="false"><span class="glyphicon glyphicon-trash tool" title="Delete"></span> DELETE</div><div class="toolBoxLarge to-set" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> EDIT </div> <div class="toolBoxLarge to-copy" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> REUSE EXISTING </div> </div>';
            }

        } else {
            overlay = '<div class="iframe-setting" style="top:' + offset.top + 'px; left: ' + offset.left + 'px; width: ' + wide + 'px; height:' + ht + 'px" data-pos=' + dataPos + '><div class="toolBoxLarge to-del" contenteditable="false"><span class="glyphicon glyphicon-trash tool" title="Delete"></span> DELETE</div><div class="toolBoxLarge to-set" contenteditable="false"><span class="glyphicon glyphicon-cog tool" title=""></span> EDIT </div></div>';
        }
        //console.log(the_editor);
        the_editor.append(overlay);
        var scrolled = the_editor.scrollTop();
        if (scrolled > offset.top) {
            the_editor.scrollTop(offset.top);
        }

        function showModalCloseBtn(status) {
            if (status) {
                jQuery('#exwidgetModal button.close').show();
                jQuery('#widget_copy_modal button.close').show();
                jQuery('#quadModal button.close').show();
            } else {
                jQuery('#exwidgetModal button.close').hide();
                jQuery('#widget_copy_modal button.close').hide();
                jQuery('#quadModal button.close').hide();
            }
        }
        the_editor.find(".iframe-setting .toolBoxLarge.to-copy").off().on("click", function(e) {
            e.stopPropagation();
            e.preventDefault();
            showModalCloseBtn(false);
            htmlEditor.thisFrame = frame;
            htmlEditor.intiateWidgetClone(frame);
            //console.log(frame);
            jQuery('#widget_copy_modal').modal('show');

            /*jQuery('#widgetCopyCont').off('click').on('click', function(){
             alert(1)
             });*/
        });
        the_editor.find(".iframe-setting .toolBoxLarge.to-set").off().on("click", function(e) {
            e.stopPropagation();
            e.preventDefault();

            switch (type) {
                case "video":
                    showModalCloseBtn(true);
                    if (htmlEditor.VideoModelBool) {
                        htmlEditor.openVideoModal(frame);
                    } else {
                        swal("Video Uploading in Progress.");
                    }
                    break;
                case "audio":
                    showModalCloseBtn(true);
                    htmlEditor.openAudioModal(frame);
                    break;
                case ".gadget":
                    //console.log("gadget");
                    showModalCloseBtn(true);
                    htmlEditor.enableSetting(frame);
                    break;

                case ".ext-gadget":
                    showModalCloseBtn(true);
                    htmlEditor.enableSetting(frame);
                    break;

                case ".gadget[data]":
                    showModalCloseBtn(true);
                    htmlEditor.enableSettingForObject(frame);
                    break;

                case ".widget":
                    //console.log("widget");
                    showModalCloseBtn(false);
                    htmlEditor.enableQuadSetting(frame);
                    //htmlEditor.enableAssessment(frame);
                    break;
                case "iframe.exwidget":
                    //console.log("iframe.widget");
                    showModalCloseBtn(false);
                    htmlEditor.exwidgetSetting(frame);
                    htmlEditor.enableSave();
                    break;
                case ".brightCoveVideo":
                    showModalCloseBtn(false);
                    htmlEditor.brightCoveSetting(frame);
                    break;
                case ".assessment":
                    showModalCloseBtn(false);
                    htmlEditor.enableAssessment(frame);
                    break;

                case ".exwidget":
                    //console.log("iframe.widget");
                    showModalCloseBtn(false);
                    htmlEditor.exwidgetSetting(frame);
                    htmlEditor.enableSave();
                    break;

                    //            case".multipleVideoPopup":
                    //                showModalCloseBtn(false);
                    //                htmlEditor.multipleVideoSetting(frame);
                    //                break;

                default:
                    break;
            }
        });
        //htmlEditor.widgetDelSet = [];
        the_editor.find(".iframe-setting .toolBoxLarge.to-del").off().on("click", function(e) {
            /*var widget_data_id = jQuery(frame).attr('id'),
             widget_id = jQuery(frame).attr('widget-id'),
             object_id = node_id;

             htmlEditor.widgetDelSet.push({
             "project_id" : project_id,
             "widget_id" : widget_id,
             "object_id" : object_id,
             "widget_data_id" : widget_data_id
             });*/

            e.stopPropagation();
            e.preventDefault();
            var frameParId = jQuery(frame).parents("[id]").attr("id");
            if (jQuery(frame).hasClass('brightCoveVideo') === true && jQuery(frame.parent()).hasClass('embed-responsive')) {
                jQuery(frame.parent()).deleteTOCNode();
                jQuery(frame.parent()).remove();
            } else {
                if (frame[0].id.split("=")[1] != null && jQuery(frame).hasClass('gadget')) {
                    the_editor.find(".iframe-setting").attr('data-pos', frame[0].id.split("=")[1]);
                }
                the_editor.find(".iframe-setting").deleteTOCNode();
            }
            jQuery(frame).remove();

            //        jQuery(".iframe-setting").addClass("hideElm");
            //the_editor.find(".iframe-setting").deleteTOCNode();
            jQuery(this).parents(".iframe-setting").remove();
            if (jQuery("#sourceVal").length) {
                splitview.updateCode(frameParId);
            }

            htmlEditor.enableSave();
        });

        the_editor.find(".iframe-setting .toolBoxLarge.to-refresh").off().on("click", function(e) {
            e.stopPropagation();
            e.preventDefault();

            var qID = jQuery(frame).attr('data-qID');
            var iframeEntityType = jQuery(frame).attr('data-iframeEntityType');
            if (qID != undefined)
                sessionStorage.removeItem(qID);
            htmlEditor.getQuadResponse(qID, iframeEntityType);
            htmlEditor.enableSave();
        });

        //    the_editor.find(".iframe-setting").on("mouseleave", function(){           //remove the overlay on mouseout
        //        jQuery(this).remove();
        //    })
    }
    //settings for the multiple video pop up
    //htmlEditor.multipleVideoSetting = function (theFrame) {
    //
    //    //var myHtml="";
    //    jQuery("#videoLoader").html('');
    //    var this_ifrm = theFrame;
    //    jQuery('#multipleVideoModal').modal('show');
    //    jQuery('#multipleVideoModal .multipleVideo_conf').attr("disabled", true);
    //    //jQuery('#AssessmentModal .assessment_conf').attr("disabled", false);
    //    var params={};
    //    params.dataApi = htmlEditor.PXEmappedAPI.getvideoPlaylistApi;
    //    params.param = {project_id: project_id};
    //    jQuery.ajax({
    //        url: params.dataApi ,
    //        async: false,
    //        method: 'GET',
    //        data:params.param,
    //        xhrFields: {
    //            withCredentials: true
    //        },
    //        crossDomain: true,
    //        success: function (data) {
    //            //console.log(data);
    //            //debugger;
    //            myHtml = "<ul>";
    //            jQuery.each(data.playlist, function (key, value) {
    //                //debugger;
    //                if (jQuery(this_ifrm).attr('data-multiplevideo-id') === value.playlist_id) {
    //                    //htmlEditor.saveSelection = value.video_id;
    //                    //console.log(htmlEditor.saveSelection);
    //                    myHtml += "<li><pre><input type='radio' name='multiVideo' id='" + value.playlist_id + "' value='" + value.key + "' checked>   " + value.key + "</pre></li>";
    //                }
    //                else {
    //                    myHtml += "<li><pre><input type='radio' name='multiVideo' id='" + value.playlist_id + "' value='" + value.key + "'>   " + value.key + "</pre></li>";
    //                }
    //            });
    //
    //            myHtml += "</ul>";
    //            //debugger;
    //            jQuery("#videoLoader").append(myHtml);
    //
    //
    //        }
    //
    //    });
    //    jQuery('input[name=multiVideo]').change(function () {
    //        jQuery('#multipleVideoModal .multipleVideo_conf').attr("disabled", false);
    //    });
    //    jQuery('#multipleVideoModal .multipleVideo_conf').off('click').on('click', function () {
    //        var typeText = 'Video: ' + jQuery('input[name=multiVideo]:checked').val();
    //        this_ifrm.attr("data-video-title", jQuery('input[name=multiVideo]:checked').val());
    //        this_ifrm.attr("data-multiplevideo-id", jQuery('input[name=multiVideo]:checked').attr('id'));
    //
    //         var params = {};
    //         var dataMultipleVideoId = this_ifrm.attr("data-multiplevideo-id");
    //         //console.log(dataMultipleVideoId);
    //        //console.log(jQuery('input[name=multiVideo]:checked').attr('id'));
    //      // var params={};
    //             params.dataApi=htmlEditor.PXEmappedAPI.getVideoList;
    //            //jQuery("#multipleVideoModalPreview").modal('show');
    //            //debugger;
    //            jQuery.ajax({
    //                url: params.dataApi + dataMultipleVideoId +'?access_token=' + access_token,
    //                async: false,
    //                method: 'GET',
    //                xhrFields: {
    //                    withCredentials: true
    //                },
    //                crossDomain: true,
    //                success: function (data) {
    //                //console.log(data);
    //                //debugger;
    //                 //htmlEditor.getDataset.setAttribute("data-path",data.json_file_path);
    //                 //console.log(data);
    //                 this_ifrm.attr("data-multiplevideo-path",data.data.dataSetFile);
    //            }
    //
    //        });
    //
    //
    //
    //        var createInfo = typeText; //+'<br> Height: '+iframeHeight+'px <br> Width: '+iframeWidth+'px';
    //        this_ifrm.find(".url-link").html(createInfo);
    //        jQuery('#multipleVideoModal').modal('hide');
    //        htmlEditor.enableSave();
    //    });
    //    jQuery('#multipleVideoModal .multipleVideo_cancel').off('click').on('click', function () {
    //        htmlEditor.saveSelection = jQuery('input[name=multiVideo]:checked').attr('id');
    //    });
    //
    //
    //
    //};
    // Settings in the modal window of video element.
htmlEditor.openVideoModal = function(this_Vid) {
    jQuery('a[href=#selectVid]').trigger('click');
    jQuery('.video_conf').attr('disabled', true);
    htmlEditor.checkedVideoPath = "";
    htmlEditor.checkedVttPath = "";
    jQuery("#selectVid .loc-or-global").removeClass('active');
    jQuery("#selectVid .loc-or-global").addClass('active');
    jQuery("#selectVtt .loc-or-global").removeClass('active');
    jQuery("#selectVtt .loc-or-global").addClass('active');
    var this_elm = this_Vid;
    var page_num = 1;
    htmlEditor.advanced_search = {};
    htmlEditor.advanced_search.params = {};
    htmlEditor.advanced_search.enableSearch = false;
    htmlEditor.advanced_search.active = false;
    var ctrlSel = 'div[ng-controller=advanceVideoSearchController]';
    angular.element(ctrlSel).scope().advanced_search.enableSearch = false;
    angular.element(ctrlSel).scope().advanced_search.active = false;
    angular.element(ctrlSel).scope().advanced_search.isCollapsed = false;
    angular.element(ctrlSel).scope().applyAdvancedClear();
    angular.element(ctrlSel).scope().$apply();
    //    htmlEditor.tabIndex = 0;
    jQuery(".paginate").hide();
    jQuery('#videoModal').find('.search_text').val('');
    htmlEditor.newVidObj.url = this_elm.attr("src");
    var getVidFrom = htmlEditor.PXEmappedAPI.getAllVideos;
    var getVttFrom = htmlEditor.PXEmappedAPI.getAllVtt;
    htmlEditor.getMediaVideo(page_num, false, getVidFrom, 'forVideo');
    htmlEditor.getMediaVtt(page_num, false, getVttFrom);
    jQuery("#videoModal .loc-or-global").addClass("active"); //Select the local by default.

    //Set default values
    //    jQuery('#videoTerm').val(this_elm.attr('src'));
    jQuery('#videoHeight').val(this_elm.height());
    jQuery('#videoWidth').val(this_elm.width());

    jQuery('#videoModal').modal('show');
    jQuery('.video_conf').off('click').on('click', function() {
        htmlEditor.chngVideoSrc(this_elm);
    });
    jQuery("#video_thumb .inputfrom").empty();
    jQuery('.video_get_media').attr("disabled", false);
    //    jQuery('#videoTerm').val(this_elm.attr('src'));

    jQuery(".video_conf").attr("disabled", true);
    jQuery('#videoHeight, #videoWidth').on('change keyup', function() {
        var position = this.selectionStart;
        var videoHeight = jQuery('#videoHeight').val();
        var videoWidth = jQuery('#videoWidth').val();
        var sanitized1 = "";
        var sanitized2 = "";

        sanitized1 = videoHeight.replace(/[^0-9]/g, '');
        jQuery('#videoHeight').val(sanitized1);
        this.selectionEnd = position;

        sanitized2 = videoWidth.replace(/[^0-9]/g, '');
        jQuery('#videoWidth').val(sanitized2);
        this.selectionEnd = position;


        if (sanitized1 == "" || sanitized2 == "") {

            jQuery(".video_conf").attr("disabled", true);
        } else {
            jQuery(".video_conf").attr("disabled", false);
        }

    });

    htmlEditor.getRepositoryTypeForVideo = function(forType) { //get the repository type, i.e, local / global.
        if (forType === "vtt") {
            var fetchFrom = jQuery("#selectVtt .loc-or-global.active").text();
            var fromUrl;
            var actives = jQuery("#selectVtt .loc-or-global.active").length;
            if (actives > 1) {
                fromUrl = htmlEditor.PXEmappedAPI.getAllVtt;
            } else {
                if (fetchFrom.toLowerCase() === "local") {
                    fromUrl = htmlEditor.PXEmappedAPI.getLocalVtt;
                } else if (fetchFrom.toLowerCase() === "global") {
                    fromUrl = htmlEditor.PXEmappedAPI.getGlobalVtt;
                } else if (fetchFrom.toLowerCase() === "all") {
                    fromUrl = htmlEditor.PXEmappedAPI.getAllVtt;
                }
            }
        } else if (forType === "video") {
            var fetchFrom = jQuery("#selectVid .loc-or-global.active").text();
            var fromUrl;
            var actives = jQuery("#selectVid .loc-or-global.active").length;
            if (actives > 1) {
                fromUrl = htmlEditor.PXEmappedAPI.getAllVideos;
            } else {
                if (fetchFrom.toLowerCase() === "local") {
                    fromUrl = htmlEditor.PXEmappedAPI.getLocalVidsAPI;
                } else if (fetchFrom.toLowerCase() === "global") {
                    fromUrl = htmlEditor.PXEmappedAPI.getGlobalVidsAPI;
                } else if (fetchFrom.toLowerCase() === "all") {
                    fromUrl = htmlEditor.PXEmappedAPI.getAllVideos;
                }
            }
        }

        return fromUrl; //Return the URL to be used.
    }

    jQuery('#selectVid').find('.search_btn').off('click').on('click', function() {
        var url = htmlEditor.getRepositoryTypeForVideo('video');
        htmlEditor.getMediaVideo(page_num, true, url, 'forVideo');
    });
    jQuery('#selectVid').find('.clear_text').off('click').on('click', function() {
        var url = htmlEditor.getRepositoryTypeForVideo('video');
        jQuery('#selectVid').find('.search_text').val('');
        htmlEditor.getMediaVideo(page_num, false, url, 'forVideo');
    });

    jQuery("#selectVid .loc-or-global").off().on("click", function() {
        jQuery(this).toggleClass("active");
        if (htmlEditor.advanced_search.enableSearch) {
            htmlEditor.getMediaVideo(page_num, false, serviceEndPoint + 'api/v1/asset-search', 'forVideo');
        } else {
            htmlEditor.getMediaVideo(page_num, false, htmlEditor.getRepositoryTypeForVideo('video'), 'forVideo');
        }

    })

    // search functionality for vtt files
    jQuery('#selectVtt').find('.search_btn').off('click').on('click', function() {
        var url = htmlEditor.getRepositoryTypeForVideo('vtt');
        htmlEditor.getMediaVtt(page_num, true, url);
    });
    jQuery('#selectVtt').find('.clear_text').off('click').on('click', function() {
        var url = htmlEditor.getRepositoryTypeForVideo('vtt');
        jQuery('#selectVtt').find('.search_text').val('');
        htmlEditor.getMediaVtt(page_num, false, url);
    });

    jQuery("#selectVtt .loc-or-global").off().on("click", function() {
        jQuery(this).toggleClass("active");
        htmlEditor.getMediaVtt(page_num, false, htmlEditor.getRepositoryTypeForVideo('vtt'));
    })
};

//Change the description on the editor about the audio.
htmlEditor.openAudioModal = function(this_Vid) {
    jQuery('a[href=#selectAudio]').trigger('click');
    jQuery('.audio_conf').attr('disabled', true);
    htmlEditor.checkedAdPath = "";
    htmlEditor.checkedAudioPath = "";
    jQuery("#selectAudio .loc-or-global").removeClass('active');
    jQuery("#selectAudio .loc-or-global").addClass('active');
    jQuery("#selectAd .loc-or-global").removeClass('active');
    jQuery("#selectAd .loc-or-global").addClass('active');
    var this_elm = this_Vid;
    var page_num = 1;
    jQuery(".paginate").hide();
    jQuery('#audioModal').find('.search_text').val('');
    htmlEditor.newVidObj.url = this_elm.attr("src");
    var getFrom = htmlEditor.PXEmappedAPI.getAllAudios;
    var getAdFrom = htmlEditor.PXEmappedAPI.getAllVtt;
    htmlEditor.getMediaVideo(page_num, false, getFrom, 'forAudio');
    htmlEditor.getAd(page_num, false, getAdFrom);
    jQuery("#videoModal .loc-or-global").addClass("active"); //Select the local by default.
    jQuery('#audioModal').modal('show');
    jQuery('.audio_conf').off('click').on('click', function() {
        htmlEditor.changeAudioSrc(this_elm);
    });
    jQuery("#video_thumb .inputfrom").empty();
    jQuery('.video_get_media').attr("disabled", false);
    jQuery(".video_conf").attr("disabled", true);
    var getRepositoryType = function(type) { //get the repository type, i.e, local / global.
        if (type === "ad") {
            var fetchFrom = jQuery("#selectAd .loc-or-global.active").text();
            var fromUrl;
            var actives = jQuery("#selectAd .loc-or-global.active").length;
            if (actives > 1) {
                fromUrl = htmlEditor.PXEmappedAPI.getAllVtt;
            } else {
                if (fetchFrom.toLowerCase() === "local") {
                    fromUrl = htmlEditor.PXEmappedAPI.getLocalVtt;
                } else if (fetchFrom.toLowerCase() === "global") {
                    fromUrl = htmlEditor.PXEmappedAPI.getGlobalVtt;
                } else if (fetchFrom.toLowerCase() === "localglobal") {
                    fromUrl = htmlEditor.PXEmappedAPI.getAllVtt;
                }
            }
        } else if (type === "audio") {
            var fetchFrom = jQuery("#selectAudio .loc-or-global.active").text();
            var fromUrl;
            var actives = jQuery("#selectAudio .loc-or-global.active").length;
            if (actives > 1) {
                fromUrl = htmlEditor.PXEmappedAPI.getAllAudios;
            } else {
                if (fetchFrom.toLowerCase() === "local") {
                    fromUrl = htmlEditor.PXEmappedAPI.getLocalAudiosAPI;
                } else if (fetchFrom.toLowerCase() === "global") {
                    fromUrl = htmlEditor.PXEmappedAPI.getGlobalAudiosAPI;
                } else if (fetchFrom.toLowerCase() === "all") {
                    fromUrl = htmlEditor.PXEmappedAPI.getAllAudios;
                }
            }
        }

        return fromUrl;
    }
    jQuery('#selectAudio').find('.search_btn').off('click').on('click', function() {
        var url = getRepositoryType('audio');
        htmlEditor.getMediaVideo(page_num, true, url, 'forAudio');
    });
    jQuery('#selectAd').find('.search_btn').off('click').on('click', function() {
        var url = getRepositoryType('ad');
        htmlEditor.getAd(page_num, true, url);
    });
    jQuery('#selectAudio').find('.clear_text').off('click').on('click', function() {
        var url = getRepositoryType('audio');
        jQuery('#selectAudio').find('.search_text').val('');
        htmlEditor.getMediaVideo(page_num, false, url, 'forAudio');
    });
    jQuery('#selectAd').find('.clear_text').off('click').on('click', function() {
        var url = getRepositoryType('ad');
        jQuery('#selectAd').find('.search_text').val('');
        htmlEditor.getAd(page_num, false, url);
    });
    jQuery("#selectAudio .loc-or-global").off().on("click", function() {
        jQuery(this).toggleClass("active");
        htmlEditor.getMediaVideo(page_num, false, getRepositoryType('audio'), 'forAudio');
    })
    jQuery("#selectAd .loc-or-global").off().on("click", function() {
        jQuery(this).toggleClass("active");
        htmlEditor.getAd(page_num, false, getRepositoryType('ad'));
    })
}

htmlEditor.newVidObj = {};
htmlEditor.newVidObj.id = [];
htmlEditor.chngVideoSrc = function(this_elm) {
    //    var video_src = jQuery('#videoTerm').val();
    var src = htmlEditor.newVidObj.url;
    var isVideoIndex = (htmlEditor.newVidObj.url).indexOf('.mp4');
    var video_height, video_width;
    var type = "";
    if (isVideoIndex >= 0) {
        type = ".mp4";
    }
    if (type === ".mp4") {
        video_height = jQuery('#videoHeight').val() > 0 ? jQuery('#videoHeight').val() : false;
        video_width = jQuery('#videoWidth').val() > 0 ? jQuery('#videoWidth').val() : false;
    }
    var videoElm = this_elm;
    //        Send the video back to the server to be mapped against local/global asset list
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.sendAssetsForCheck,
        type: 'POST',
        data: {
            'project_id': project_id,
            'is_global': '',
            'object_id': JSON.stringify(htmlEditor.newVidObj.id)
        },
        async: true,
        xhrFields: {
            withCredentials: true
        },
        success: function(data, textStatus, jqXHR) {

        },
        error: function(jqXHR, textStatus, errorThrown) {
            //console.log("unable to put up the request")
        }
    })
    if (type === ".mp4") {
        if (video_height) {
            if (video_height >= 50 && video_height < 125 || video_height < 50) {
                videoElm.find(".lm-rochak").addClass("width125");
            } else {
                videoElm.find(".lm-rochak").removeClass("width125");
            }
            videoElm.height(video_height);
        }
        if (video_width)
            videoElm.width(video_width);
        videoElm.attr('src', src);
        videoElm.css("background-size", video_width + "px " + video_height + "px");
        var createInfo = 'Video URL: ' + src;
        videoElm.find(".url-link").html(createInfo + "<div> </div>");
    } else {
        videoElm.attr('trackSrc', src);
    }
    videoElm.trigger("click");
    htmlEditor.enableSave();
    jQuery('#videoModal').modal('hide');
};
// htmlEditor.changeVTTsrc = function(vtt_elm) {
//     var vtt_src = htmlEditor.newVidObj.url;
//     var vttElm = vtt_elm;
//     jQuery.ajax({
//         url: htmlEditor.PXEmappedAPI.sendAssetsForCheck,
//         type: 'POST',
//         data: {
//             'project_id': project_id,
//             'is_global': '',
//             'object_id': JSON.stringify(htmlEditor.newVidObj.id)
//         },
//         async: true,
//         xhrFields: {
//             withCredentials: true
//         },
//         success: function(data, textStatus, jqXHR) {

//         },
//         error: function(jqXHR, textStatus, errorThrown) {
//             //console.log("unable to put up the request")
//         }
//     })
//     vttElm.attr('trackSrc', vtt_src);
//     htmlEditor.enableSave();
//     jQuery('#videoModal').modal('hide');
// }
htmlEditor.changeAudioSrc = function(this_elm) {
    var src = htmlEditor.newVidObj.url;
    var filteType = (src).indexOf('.mp3');
    var type = "";
    if (filteType >= 0) {

        type = ".mp3";
    }
    var audioElm = this_elm;
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.sendAssetsForCheck,
        type: 'POST',
        data: {
            'project_id': project_id,
            'is_global': '',
            'object_id': JSON.stringify(htmlEditor.newVidObj.id)
        },
        async: true,
        xhrFields: {
            withCredentials: true
        },
        success: function(data, textStatus, jqXHR) {

        },
        error: function(jqXHR, textStatus, errorThrown) {

        }
    });
    if (type === ".mp3") {
        audioElm.attr('src', src);
        var createInfo = 'Audio URL: ' + src;
        audioElm.find(".url-link").html(createInfo + "<div> </div>");
    } else {
        audioElm.attr('trackSrc', src);
    }
    audioElm.trigger("click");
    htmlEditor.enableSave();
    jQuery('#audioModal').modal('hide');
}
htmlEditor.newGadgetObj = {}; //Holds the details of the gadget, while setting is being changed.

// Settings modal window for the iframe elements
htmlEditor.enableSetting = function(theFrame) {

    htmlEditor.checkedWidgetPath = "";
    htmlEditor.advanced_search = {};
    htmlEditor.advanced_search.params = {};
    htmlEditor.advanced_search.enableSearch = false;
    htmlEditor.advanced_search.active = false;
    jQuery(".iframe_conf").attr("disabled", true);
    jQuery('#iframeModal').modal('show');
    var ctrlSel = 'div[ng-controller=advancedGadgetSearchController]';
    angular.element(ctrlSel).scope().advanced_search.enableSearch = false;
    angular.element(ctrlSel).scope().advanced_search.active = false;
    angular.element(ctrlSel).scope().advanced_search.isCollapsed = false;
    angular.element(ctrlSel).scope().applyAdvancedClear();
    angular.element(ctrlSel).scope().$apply();
    jQuery('#iframeModal').find('.search_text').val('');
    htmlEditor.newGadgetObj.gadgetId = [];
    var page_num = 1;
    htmlEditor.getGadgetList(page_num, false, htmlEditor.PXEmappedAPI.getAllGadgets);
    jQuery("#iframeModal .loc-or-global").addClass("active"); //Select the local by default.
    var scrollStatus = "no";
    var this_ifrm = theFrame;
    var this_ifrm_hgt = this_ifrm.height();
    //console.log(this_ifrm_hgt);
    var this_ifrm_wdth = this_ifrm.width();
    //console.log(this_ifrm_wdth);
    var this_ifrm_url = jQuery.trim(this_ifrm.attr('src'));
    var this_ifrm_scroll = this_ifrm.attr("scrolling");

    jQuery('#iframeHeight').on('keydown', dotChecker);
    jQuery('#iframeHeight').on('keyup', function(e) {
        if (e.keyCode === 190)
            jQuery(this).val((jQuery(this).val()).split('>')[0])
    });

    jQuery('#iframeWidth').on('keydown', dotChecker);
    jQuery('#iframeWidth').on('keyup', function(e) {
        if (e.keyCode === 190)
            jQuery(this).val((jQuery(this).val()).split('>')[0])
    });

    function dotChecker(e) {
        var contArray = (jQuery(this).val()).split('');
        var dotLength = 0;
        jQuery(contArray).each(function(i, v) {
            if (v === '.')
                dotLength++;
        });

        if (dotLength > 0 || (e.keyCode === 110 || e.keyCode === 190))
            return false;

        if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode == 67 && e.ctrlKey === true) || (e.keyCode == 88 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    jQuery('#iframeTerm').val(this_ifrm_url);
    jQuery('#iframeHeight').val(this_ifrm_hgt);
    jQuery('#iframeWidth').val(this_ifrm_wdth);

    if (this_ifrm_scroll == "yes") {
        jQuery("#iframeScroll").prop("checked", true);
    } else {
        jQuery("#iframeScroll").prop("checked", false);
    }
    jQuery("#iframeScroll").click(function() {

        jQuery(".iframe_conf").attr("disabled", false);
    });
    htmlEditor.getRepositoryTypeForGadget = function() { //get the repository type, i.e, local / global.
        var fetchFrom = jQuery("#iframeModal .loc-or-global.active").text();
        var fromUrl;
        var actives = jQuery("#iframeModal .loc-or-global.active").length;
        if (actives > 1) {
            fromUrl = htmlEditor.PXEmappedAPI.getAllGadgets;
        } else {
            if (fetchFrom.toLowerCase() === "local") {
                fromUrl = htmlEditor.PXEmappedAPI.getLocalGadgets;
            } else if (fetchFrom.toLowerCase() === "global") {
                fromUrl = htmlEditor.PXEmappedAPI.getGlobalGadgets;
            } else if (fetchFrom.toLowerCase() === "all") {
                fromUrl = htmlEditor.PXEmappedAPI.getAllGadgets;
            }
        }
        return fromUrl; //Return the URL to be used.
    }

    jQuery('#iframeModal').find('.search_btn').off('click').on('click', function() {
        var url = htmlEditor.getRepositoryTypeForGadget();
        htmlEditor.getGadgetList(page_num, true, url);
    });
    jQuery('#iframeModal').find('.clear_text').off('click').on('click', function() {
        var url = htmlEditor.getRepositoryTypeForGadget();
        jQuery('#iframeModal').find('.search_text').val('');
        htmlEditor.getGadgetList(page_num, false, url);
    });

    jQuery('#iframeTerm').on("change", function() {
        jQuery(".iframe_conf").attr("disabled", false);
    })
    jQuery("#iframeModal .loc-or-global").off().on("click", function() {
        jQuery(this).toggleClass("active");
        if (htmlEditor.advanced_search.enableSearch) {
            htmlEditor.getGadgetList(page_num, false, serviceEndPoint + 'api/v1/asset-search');
        } else {
            htmlEditor.getGadgetList(page_num, false, htmlEditor.getRepositoryTypeForGadget());
        }

    })
    jQuery('#iframeHeight').on('keydown', function() {
        //console.log(this_ifrm_hgt);
        jQuery(".iframe_conf").attr("disabled", false);

    });
    jQuery('#iframeWidth').on('keydown', function() {
        jQuery(".iframe_conf").attr("disabled", false);
    });

    jQuery('#iframeModal .iframe_conf').off('click').on('click', function() {

        htmlEditor.enableSave();
        var gadgetObj, iframeTermVal;
        if (!jQuery('#iframeHeight').val() || jQuery('#iframeHeight').val() <= 0) {
            jQuery('#iframeHeight').val(250);
        }
        if (!jQuery('#iframeWidth').val() || jQuery('#iframeWidth').val() <= 0) {
            jQuery('#iframeWidth').val(700);
        }
        iframeHeight = jQuery.trim(jQuery('#iframeHeight').val());
        var iframeWidth = jQuery.trim(jQuery('#iframeWidth').val());
        iframeTermVal = jQuery.trim(jQuery('#iframeTerm').val());

        //check if the user has selected some local gadget or entered an external URL, and proceed accrodingly .
        if (htmlEditor.newGadgetObj.gadgetId.length && !iframeTermVal.length) {
            //        Send the gadget back to the server to be mapped against local/global asset list
            jQuery.ajax({
                url: htmlEditor.PXEmappedAPI.sendAssetsForCheck,
                type: 'POST',
                data: {
                    'project_id': project_id,
                    'is_global': '',
                    'object_id': JSON.stringify(htmlEditor.newGadgetObj.gadgetId)
                },
                async: true,
                xhrFields: {
                    withCredentials: true
                },
                success: function(data, textStatus, jqXHR) {

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //console.log("unable to put up the request")
                }
            })

            var url = htmlEditor.PXEmappedAPI.getGadgetManifest + "/object_id/" + htmlEditor.newGadgetObj.gadgetId + "?access_token=" + access_token + '&project_id=' + project_id; //Url to get the gadget page.


            jQuery.when(gadgetObj = htmlEditor.ajaxCalls(url)).then(function() { //Get the url of gadget
                iframeTermVal = gadgetObj.responseJSON.data.manifest.launch_file;
                this_ifrm.attr("isobject", true); //mark this particular gadget for converting into object
                setValues();
            })
        } else {
            this_ifrm.attr("isobject", false); //mark this particular gadget for converting into object
            setValues();
        }

        function setValues() {
            if (iframeHeight >= 50 && iframeHeight <= 125 || iframeHeight < 50) { //Reduce the font-size & stuffs if height is less.
                this_ifrm.find(".lm-rochak").addClass("width125");
            } else {
                this_ifrm.find(".lm-rochak").removeClass("width125");
            }

            if (jQuery("#iframeScroll").is(":checked")) {
                scrollStatus = "yes";
            } else {
                scrollStatus = "no";
            }

            var iframeScroll = scrollStatus;

            if (iframeHeight > 0 && iframeWidth > 0) {
                //                if (iframeHeight < 500) {
                ////                    swal("Gadget Height cannot be lesser than 500. Applying values...")
                //                    iframeHeight = 500;
                //                }
                this_ifrm.attr("src", iframeTermVal);
                this_ifrm.css("height", iframeHeight + 'px');
                this_ifrm.css("width", iframeWidth + 'px');
                this_ifrm.attr("scrolling", iframeScroll);
                this_ifrm.attr("height", iframeHeight);
                this_ifrm.attr("width", iframeWidth);

                var createInfo = 'Gadget URL: ' + iframeTermVal;
                this_ifrm.find(".url-link").html(createInfo);
                this_ifrm.trigger("click");
                if (this_ifrm.attr('gadget-id') && htmlEditor.newGadgetObj.gadgetId.length) {
                    this_ifrm.attr('gadget-id', htmlEditor.newGadgetObj.gadgetId);
                } else {
                    this_ifrm.attr('gadget-id', "");
                }
                if (htmlEditor.newGadgetObj.gadgetId.length) {
                    this_ifrm.attr('data-gadget-id', htmlEditor.newGadgetObj.gadgetId);


                } else {
                    this_ifrm.attr('data-gadget-id', "");
                }
                //                this_ifrm.find(".gadgetURL").text(iframeTermVal);
                //                this_ifrm.find(".gadgetHt").text(iframeHeight);
                //                this_ifrm.find(".gadgetWidth").text(iframeWidth);
                jQuery('#iframeModal').modal('hide');
            } //else {
            //swal("Input Correct values");
            // }
        }
    });
};

//Settings modal window for brightcove video
//
htmlEditor.brightCoveSetting = function(theFrame) {

    htmlEditor.checkedWidgetPath = "";
    jQuery("#BrightCoveModal .iframe_conf").attr("disabled", true);
    jQuery('#BrightCoveModal').modal('show');
    //jQuery('#iframeModal').find('.search_text').val('');
    htmlEditor.newGadgetObj.gadgetId = [];
    var page_num = 1;
    //htmlEditor.getGadgetList(page_num, false, htmlEditor.PXEmappedAPI.getAllGadgets);
    //jQuery("#iframeModal .loc-or-global").addClass("active"); //Select the local by default.
    var scrollStatus = "no";
    var this_ifrm = theFrame;
    //console.log(this_ifrm);
    var this_ifrm_hgt = this_ifrm.height();
    var iframeTermVal1 = jQuery.trim(this_ifrm.attr('src').split('videoId=')[1]);
    //console.log(iframeTermVal1);
    //console.log(this_ifrm_hgt);
    var this_ifrm_wdth = this_ifrm.width();
    //console.log(this_ifrm_wdth);
    //jQuery.trim(this_ifrm.attr('src').val=="");
    var this_ifrm_url = jQuery.trim(this_ifrm.attr('src'));
    //console.log(this_ifrm_url);
    var this_ifrm_scroll = this_ifrm.attr("scrolling");
    jQuery('#brightcoveHeight').on('keydown', dotChecker);
    jQuery('#brightcoveHeight').on('keyup', function(e) {
        if (e.keyCode === 190)
            jQuery(this).val((jQuery(this).val()).split('>')[0])
    });
    jQuery('#brightcoveWidth').on('keydown', dotChecker);
    jQuery('#brightcoveWidth').on('keyup', function(e) {
        if (e.keyCode === 190)
            jQuery(this).val((jQuery(this).val()).split('>')[0])
    });

    function dotChecker(e) {
        var contArray = (jQuery(this).val()).split('');
        var dotLength = 0;
        jQuery(contArray).each(function(i, v) {
            if (v === '.')
                dotLength++;
        });
        if (dotLength > 0 || (e.keyCode === 110 || e.keyCode === 190))
            return false;
        if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode == 67 && e.ctrlKey === true) || (e.keyCode == 88 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    jQuery('#brightcoveTerm').val(iframeTermVal1);
    jQuery('#brightcoveHeight').val(this_ifrm_hgt);
    jQuery('#brightcoveWidth').val(this_ifrm_wdth);
    jQuery('#brightcoveTerm').on("change", function() {
        //alert(1);
        jQuery("#BrightCoveModal .iframe_conf").attr("disabled", false);
    });
    jQuery('#brightcoveHeight').on('keydown', function() {
        //console.log(this_ifrm_hgt);
        jQuery("#BrightCoveModal .iframe_conf").attr("disabled", false);
    });
    jQuery('#brightcoveWidth').on('keydown', function() {
        jQuery("#BrightCoveModal .iframe_conf").attr("disabled", false);
    });
    jQuery('#brightcoveTerm').on('keydown', function() {
        jQuery("#BrightCoveModal .iframe_conf").attr("disabled", false);
    });
    jQuery('#BrightCoveModal .iframe_conf').off('click').on('click', function() {

        htmlEditor.enableSave();
        var gadgetObj, iframeTermVal;

        if (!jQuery('#brightcoveHeight').val() || jQuery('#brightcoveHeight').val() <= 0) {
            jQuery('#brightcoveHeight').val(350);
        }
        if (!jQuery('#brightcoveWidth').val() || jQuery('#brightcoveWidth').val() <= 0) {
            jQuery('#brightcoveWidth').val(700);
        }
        iframeHeight = jQuery.trim(jQuery('#brightcoveHeight').val());
        iframeWidth = jQuery.trim(jQuery('#brightcoveWidth').val());
        iframeTermVal = jQuery.trim(jQuery('#brightcoveTerm').val());
        var brightCoveUrl = bright_cove_url + iframeTermVal;
        this_ifrm.attr("isobject", false); //mark this particular gadget for converting into object
        setValues();
        //check if the user has selected some local gadget or entered an external URL, and proceed accrodingly .
        function setValues() {
            if (iframeHeight >= 50 && iframeHeight <= 125 || iframeHeight < 50) { //Reduce the font-size & stuffs if height is less.
                this_ifrm.find(".lm-rochak").addClass("width125");
            } else {
                this_ifrm.find(".lm-rochak").removeClass("width125");
            }
            /*if (jQuery("#iframeScroll").is(":checked")) {
             scrollStatus = "yes";
             } else {
             scrollStatus = "no";
             }*/
            //var iframeScroll = scrollStatus;
            if (iframeHeight > 0 && iframeWidth > 0) {

                this_ifrm.attr("src", brightCoveUrl);
                this_ifrm.attr("iframeTermVal", iframeTermVal);

                this_ifrm.css("height", iframeHeight + 'px');
                this_ifrm.css("width", iframeWidth + 'px');
                this_ifrm.attr("height", iframeHeight);
                this_ifrm.attr("width", iframeWidth);
                //this_ifrm.attr("scrolling", iframeScroll);
                var createInfo = 'Brightcove Video Id: ' + iframeTermVal;
                this_ifrm.find(".url-link").html(createInfo);
                this_ifrm.trigger("click");
                jQuery('#BrightCoveModal').modal('hide');
            } //else {
            //swal("Input Correct values");
            // }
        }
    });
};

htmlEditor.populateAsssessmentList = function(frameParam) {
    if (!this_ifrm.attr("data-iframeentitytype")) {
        jQuery(".assessmentType:first").prop('checked', true);
        this_ifrm.attr("data-iframeentitytype", 2);
    }
    if (htmlEditor.assessmentDataResponse.assessment_list) {
        if (parseInt(this_ifrm.attr("data-iframeentitytype")) === 2) {
            myHtml = "<div>";
            jQuery.each(htmlEditor.assessmentDataResponse.assessment_list, function(key, value) {
                var disabled = '';
                if (parseInt(value.total_question) == 0) {
                    var disabled = 'disabled';
                }
                if (this_ifrm.attr("data-assessment-id") == value.shortcode) {
                    if (this_ifrm.attr("data-assessment-question-id") != '' || this_ifrm.attr("data-assessment-question-id") == 'undefined') {
                        myHtml += "<div class='prediv'><div class='radio'> <label class='wordBreak'><input type='radio' name='assessmentId' data-value-title='" + value.title + "'  value='" + value.shortcode + "'>   " + value.title + "</label> <button class='btn btn-default2 prevbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.shortcode + "'>Preview</button></div></div>";
                    } else {
                        myHtml += "<div class='prediv'><div class='radio'><label class='wordBreak'><input type='radio' name='assessmentId' data-value-title='" + value.title + "'  value='" + value.shortcode + "' checked='checked'>   " + value.title + "</label> <button class='btn btn-default2 prevbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.shortcode + "'>Preview</button></div></div>";
                    }
                } else {
                    myHtml += "<div class='prediv'><div class='radio'><label class='wordBreak'><input type='radio' name='assessmentId' data-value-title='" + value.title + "'  value='" + value.shortcode + "'>   " + value.title + "</label> <button class='btn btn-default2 prevbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.shortcode + "'>Preview</button></div></div>";
                }
            });
            myHtml += "</div>"
            jQuery("#assessmentLoader").html(myHtml);
        } else if (parseInt(this_ifrm.attr("data-iframeentitytype")) === 3) {
            var selectedAssessmentId = "";
            var myHtml = "<div id='dropdownList' class='dropdown editassessment'><button class='btn btn-default dropdown-toggle' type='button' id='questionSelector' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'><p id='dropDownSelection' class='wordBreak'>Select Assessment</p><span class='caret'></span></button><ul class='dropdown-menu' aria-labelledby='dropdownMenu'>";
            if (htmlEditor.assessmentDataResponse.assessment_list) {
                jQuery.each(htmlEditor.assessmentDataResponse.assessment_list, function(key, value) {
                    if (this_ifrm.attr("data-assessment-id") == value.shortcode) {
                        selectedAssessmentId = value.shortcode;
                        htmlEditor.selectedTitle = value.title;

                    }
                    myHtml += "<li class='selectedQuestion' value='" + value.shortcode + "'><a href='#' > " + value.title + "</a></li>";
                });
            }
            myHtml += "</ul></div>";
            jQuery("#assessmentLoader").html(myHtml);
        }
    } else {
        jQuery("#assessmentLoader").html("<h4>No assessment found!!</h4>");
    }
    htmlEditor.bindAssessmentEvents();
}
htmlEditor.enableAssessment = function(theFrame, filter) {
    var this_ifrm = theFrame;
    var assessment_options = "";
    var question_number = null;
    htmlEditor.advanced_search = {};
    htmlEditor.advanced_search.params = {};
    htmlEditor.advanced_search.enableSearch = false;
    htmlEditor.advanced_search.active = false;
    var ctrlSel = 'div[ng-controller=advancedAssessmentSearchController]';
    console.log(angular.element(ctrlSel).scope());
    angular.element(ctrlSel).scope().advanced_search.enableSearch = false;
    angular.element(ctrlSel).scope().advanced_search.active = false;
    angular.element(ctrlSel).scope().advanced_search.isCollapsed = false;
    angular.element(ctrlSel).scope().applyAdvancedClear();
    angular.element(ctrlSel).scope().$apply();
    var params = {};
    if (!htmlEditor.curr_iFrameEntityType) {
        htmlEditor.curr_iFrameEntityType = jQuery(this_ifrm).attr("data-iframeentitytype");
    }
    htmlEditor.bindAssessmentEvents = function() {
        //jQuery(document).on("click", ".selectedQuestion", function(){
        jQuery('.selectedQuestion').off().on('click', function() {
            //jQuery(".dropdown-toggle").dropdown();
            jQuery("#questionSelector").trigger("click");
            //            jQuery('.dropdown-menu').click(function(){
            //
            //            });
            //jQuery(".dropdown-toggle").dropdown();
            jQuery("#dropDownSelection").text(jQuery(this).text());
            jQuery('#AssessmentModal .assessment_conf').attr("disabled", true);
            //console.log(params);
            params.dataApi = htmlEditor.PXEmappedAPI.questionGetUrl;
            params.assessment_id = jQuery(this).attr('value');
            params.project_id = project_id;
            params.param = "";
            //console.info(jQuery(this).val());
            htmlEditor.callQuestionData(params);
            //console.info(htmlEditor.questiondatalist);
            var myHtml = "<div>";
            jQuery.each(htmlEditor.questiondatalist.question_list, function(key, value) {
                var disabled = '';
                if (parseInt(value.total_question) == 0) {
                    var disabled = 'disabled';
                }
                if (this_ifrm.attr("data-assessment-question-id") == value.search_name) {
                    myHtml += "<div class='prediv'><div class='radio'> <label class='wordBreak'><input type='radio' name='questionId' data-value-title='" + value.title + "' value='" + value.search_name + "' checked='checked'>" + value.title + "</label><button class='btn btn-default2 prevquestbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.search_name + "'>Preview</button></div></div>";
                } else {
                    myHtml += "<div class='prediv'><div class='radio'> <label class='wordBreak'><input type='radio' name='questionId' data-value-title='" + value.title + "' value='" + value.search_name + "'>" + value.title + "</label><button class='btn btn-default2 prevquestbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.search_name + "'>Preview</button></div></div>";
                }
            });
            var qNumberDiv = "<div id='qNumber' style='padding-top: 15px;'> Question Number: <input type='text' name='questionNumber' style='width:45px;'> <i>Optional field. Add a Question number that you want to see in your widget</i></div>";
            myHtml += "</div>";
            jQuery("#questionLoader").html(myHtml);
            jQuery("#qNumber").remove();
            jQuery(qNumberDiv).insertAfter("#questionLoader");
            jQuery(".prevquestbtn").off().on('click', function() {
                var short_code = jQuery(this).attr("value");
                angular.element(document.getElementById('questionLoader')).scope().preview_question(short_code);
            });
            jQuery('input[name=questionId]').change(function() {
                jQuery('#AssessmentModal .assessment_conf').attr("disabled", false);
                jQuery('input[name=questionNumber]').val('');
                if (jQuery('input[name=questionId]:checked').val() === this_ifrm.attr("data-assessment-question-id")) {
                    jQuery('input[name=questionNumber]').val(this_ifrm.attr("data-assessment-question-number"));
                }
            });
        })
        jQuery('input[name=assessmentId]').change(function() {
            jQuery('#AssessmentModal .assessment_conf').attr("disabled", false);
            params.assessment_id = jQuery(this).val();
        });
    }
    if (!filter) {
        jQuery("#AssessmentModal .search_text").val('');
    }
    htmlEditor.populateSearchData = function() {
        if (!this_ifrm.attr("data-iframeentitytype")) {
            jQuery(".assessmentType:first").prop('checked', true);
            this_ifrm.attr("data-iframeentitytype", 2);
        }
        if (htmlEditor.assessmentDataResponse.assessment_list) {
            if (parseInt(this_ifrm.attr("data-iframeentitytype")) === 2) {
                myHtml = "<div>";
                jQuery.each(htmlEditor.assessmentDataResponse.assessment_list, function(key, value) {
                    var disabled = '';
                    if (parseInt(value.total_question) == 0) {
                        var disabled = 'disabled';
                    }
                    if (this_ifrm.attr("data-assessment-id") == value.shortcode) {
                        if (this_ifrm.attr("data-assessment-question-id") != '' || this_ifrm.attr("data-assessment-question-id") == 'undefined') {
                            myHtml += "<div ng-controller='TestController' class='prediv'><div class='radio'> <label class='wordBreak'><input type='radio' name='assessmentId' data-value-title='" + value.title + "'  value='" + value.shortcode + "'>   " + value.title + "</label> <button class='btn btn-default2 prevbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.shortcode + "' >Preview</button></div></div>";

                        } else {
                            myHtml += "<div class='prediv'><div class='radio'><label class='wordBreak'><input type='radio' name='assessmentId' data-value-title='" + value.title + "'  value='" + value.shortcode + "' checked='checked'>   " + value.title + "</label><button class='btn btn-default2 prevbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.shortcode + "'>Preview</button></div></div>";
                        }
                    } else {
                        myHtml += "<div class='prediv'><div class='radio'><label class='wordBreak'><input type='radio' name='assessmentId' data-value-title='" + value.title + "'  value='" + value.shortcode + "'>   " + value.title + "</label><button class='btn btn-default2 prevbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.shortcode + "'>Preview</button></div></div>";
                    }
                });
                myHtml += "</div>"
                jQuery("#assessmentLoader").html(myHtml);
            } else if (parseInt(this_ifrm.attr("data-iframeentitytype")) === 3) {
                var selectedAssessmentId = "";
                var myHtml = "<div id='dropdownList' class='dropdown editassessment'><button class='btn btn-default dropdown-toggle' type='button' id='questionSelector' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'><p id='dropDownSelection' class='wordBreak'>Select Assessment</p><span class='caret'></span></button><ul class='dropdown-menu' aria-labelledby='dropdownMenu'>";
                if (htmlEditor.assessmentDataResponse.assessment_list) {
                    jQuery.each(htmlEditor.assessmentDataResponse.assessment_list, function(key, value) {
                        if (this_ifrm.attr("data-assessment-id") == value.shortcode) {
                            selectedAssessmentId = value.shortcode;
                            htmlEditor.selectedTitle = value.title;

                        }
                        myHtml += "<li class='selectedQuestion' value='" + value.shortcode + "'><a href='#' > " + value.title + "</a></li>";
                    });
                }
                myHtml += "</ul></div>";
                jQuery("#assessmentLoader").html(myHtml);
            }
        } else {
            jQuery("#assessmentLoader").html("<h4>No assessment found!!</h4>");
        }
        htmlEditor.bindAssessmentEvents();
    }
    jQuery(".assessmentType").off().on('click', function() {
        jQuery(this_ifrm).attr("data-iframeentitytype", jQuery(this).val());
        jQuery("#dropdownList").val(0);
        var filteredState = jQuery("#AssessmentModal .loc-or-global.active").text();
        htmlEditor.enableAssessment(this_ifrm, filteredState.toLowerCase());
    });
    jQuery("#AssessmentModal .search_btn").off().on('click', function() {
        var searchParam = {};
        searchParam.param = {};
        searchParam.param.access_token = access_token;
        searchParam.param.pageNumber = 1;
        searchParam.param.itemsPerPage = 10;
        // searchParam.param.orderDirection = 'ASC';
        // searchParam.param.orderField = "title";
        var filter = jQuery("#AssessmentModal .loc-or-global.active").text();
        if (filter.toLowerCase() === "global") {
            searchParam.param.assessment_local_global = JSON.stringify({ 'local': false, 'global': true });
        } else if (filter.toLowerCase() === "local") {
            searchParam.param.assessment_local_global = JSON.stringify({ 'local': true, 'global': false });
        } else if (filter.toLowerCase() === "localglobal") {
            searchParam.param.assessment_local_global = JSON.stringify({ 'local': true, 'global': true });
        }
        searchParam.param.search_text = jQuery('#AssessmentModal').find('.search_text').val();
        searchParam.dataApi = "";
        htmlEditor.callAssessmentData(searchParam, true);
        htmlEditor.populateSearchData();
    });
    jQuery('#AssessmentModal').find('.clear_text').off('click').on('click', function() {
        jQuery("#AssessmentModal .search_text").val('');
        htmlEditor.enableAssessment(this_ifrm, 'localglobal');
    });
    jQuery("#AssessmentModal button[data-dismiss='modal'].btn").off().on('click', function() {
        jQuery(this_ifrm).attr("data-iframeentitytype", htmlEditor.curr_iFrameEntityType);
        htmlEditor.curr_iFrameEntityType = undefined;
    });

    if (!filter) {
        jQuery("#AssessmentModal .loc-or-global").removeClass('active');
        jQuery("#AssessmentModal .loc-or-global").addClass('active');
    }
    jQuery("#AssessmentModal .loc-or-global").off().on("click", function() {
        jQuery(this).toggleClass("active");
        if (htmlEditor.advanced_search.enableSearch) {
            var ctrlSel = 'div[ng-controller=advancedAssessmentSearchController]';
            angular.element(ctrlSel).scope().getAdvancedSearchFields();
            htmlEditor.callAssessmentData();
            htmlEditor.populateSearchData();
        } else {
            var filter = jQuery("#AssessmentModal .loc-or-global.active").text();
            if (filter.toLowerCase() === "global") {
                htmlEditor.enableAssessment(this_ifrm, 'global');
            } else if (filter.toLowerCase() === "local") {
                htmlEditor.enableAssessment(this_ifrm, 'local');
            } else if (filter.toLowerCase() === "localglobal") {
                htmlEditor.enableAssessment(this_ifrm, 'localglobal');
            } else {
                jQuery("#assessmentLoader").html("<h4>No assessment found!!</h4>");
            }
        }


    });


    //console.info(jQuery(this_ifrm).outerHTML());
    jQuery("#questionLoader").html("");
    jQuery("#assessmentLoader").html("");
    jQuery('#AssessmentModal .assessment_conf').attr("disabled", true);
    if (this_ifrm.attr("data-iframeentitytype")) {
        if (parseInt(this_ifrm.attr("data-iframeentitytype")) === 2) {
            if (htmlEditor.isGlobalProject) {
                params.dataApi = htmlEditor.PXEmappedAPI.globalAssessmentGetUrl;
            } else {
                if (filter) {
                    if (filter === 'global') {
                        params.dataApi = htmlEditor.PXEmappedAPI.globalAssessmentGetUrl;
                    } else if (filter === 'local') {
                        params.dataApi = htmlEditor.PXEmappedAPI.localAssessmentGetUrl;
                    } else if (filter === "localglobal") {
                        params.dataApi = htmlEditor.PXEmappedAPI.assessmentGetUrl;
                    }
                } else {
                    params.dataApi = htmlEditor.PXEmappedAPI.assessmentGetUrl;
                }
            }
            params.param = "";
            htmlEditor.callAssessmentData(params);
            var myHtml;
            // jQuery("#AssessmentModal .search_text").val('');
            if (htmlEditor.assessmentDataResponse.assessment_list) {
                myHtml = "<div>";
                jQuery.each(htmlEditor.assessmentDataResponse.assessment_list, function(key, value) {
                    var disabled = '';
                    if (parseInt(value.total_question) == 0) {
                        var disabled = 'disabled';
                    }
                    if (this_ifrm.attr("data-assessment-id") == value.shortcode) {
                        if (this_ifrm.attr("data-assessment-question-id") != '' || this_ifrm.attr("data-assessment-question-id") == 'undefined') {
                            myHtml += "<div class='prediv'><div class='radio'> <label class='wordBreak'><input type='radio' name='assessmentId' data-value-title='" + value.title + "'  value='" + value.shortcode + "'>   " + value.title + "</label><button class='btn btn-default2 prevbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.shortcode + "'>Preview</button></div></div>";
                        } else {
                            myHtml += "<div class='prediv'><div class='radio'><label class='wordBreak'><input type='radio' name='assessmentId' data-value-title='" + value.title + "'  value='" + value.shortcode + "' checked='checked'>   " + value.title + "</label><button class='btn btn-default2 prevbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.shortcode + "'>Preview</button></div></div>";
                        }
                    } else {
                        myHtml += "<div class='prediv'><div class='radio'><label class='wordBreak'><input type='radio' name='assessmentId' data-value-title='" + value.title + "'  value='" + value.shortcode + "'>   " + value.title + "</label><button class='btn btn-default2 prevbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.shortcode + "'>Preview</button></div></div>";
                    }
                });
                myHtml += "</div>"
                jQuery("#assessmentLoader").html(myHtml);
                htmlEditor.bindAssessmentEvents();
            } else {
                jQuery("#assessmentLoader").html("<h4>No assessment found!!</h4>");
            }
            jQuery("#qNumber").remove();
            jQuery(".assessmentType:first").prop('checked', true);
        } else if (parseInt(this_ifrm.attr("data-iframeentitytype")) === 3) {
            jQuery(".assessmentType").eq(1).prop('checked', true);
            if (htmlEditor.isGlobalProject) {
                params.dataApi = htmlEditor.PXEmappedAPI.globalAssessmentGetUrl;
            } else {
                if (filter) {
                    if (filter === 'global') {
                        params.dataApi = htmlEditor.PXEmappedAPI.globalAssessmentGetUrl;
                    } else if (filter === 'local') {
                        params.dataApi = htmlEditor.PXEmappedAPI.localAssessmentGetUrl;
                    } else if (filter === "localglobal") {
                        params.dataApi = htmlEditor.PXEmappedAPI.assessmentGetUrl;
                    }
                } else {
                    params.dataApi = htmlEditor.PXEmappedAPI.assessmentGetUrl;
                }
            }
            params.param = "";
            htmlEditor.callAssessmentData(params);
            //console.log(this_ifrm.attr("data-assessment-id"));
            var selectedAssessmentId = "";
            //var myHtml = "<select id='dropdownList' class='form-control'>"
            var myHtml = "<div id='dropdownList' class='dropdown editassessment'><button class='btn btn-default dropdown-toggle' type='button' id='questionSelector' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'><p id='dropDownSelection' class='wordBreak'>Select Assessment</p><span class='caret'></span></button><ul class='dropdown-menu' aria-labelledby='dropdownMenu'>";
            if (htmlEditor.assessmentDataResponse.assessment_list) {
                jQuery.each(htmlEditor.assessmentDataResponse.assessment_list, function(key, value) {
                    if (this_ifrm.attr("data-assessment-id") == value.shortcode) {
                        selectedAssessmentId = value.shortcode;
                        htmlEditor.selectedTitle = value.title;

                    }
                    myHtml += "<li class='selectedQuestion' value='" + value.shortcode + "'><a href='#' > " + value.title + "</a></li>";
                });
            }
            myHtml += "</ul></div>";
            jQuery.when(jQuery("#assessmentLoader").html(myHtml)).done(function() {
                jQuery(".dropdown-toggle").dropdown();
                htmlEditor.bindAssessmentEvents();
                if (selectedAssessmentId != "") {
                    setTimeout(function() {
                        params.dataApi = htmlEditor.PXEmappedAPI.questionGetUrl;
                        params.assessment_id = selectedAssessmentId;
                        params.project_id = project_id;
                        params.param = "";
                        jQuery('#dropdownList').val(selectedAssessmentId);
                        htmlEditor.callQuestionData(params);

                        if (htmlEditor.questiondatalist) {
                            var myHtml = "<div>";
                            jQuery.each(htmlEditor.questiondatalist.question_list, function(key, value) {
                                var disabled = '';
                                if (parseInt(value.total_question) == 0) {
                                    var disabled = 'disabled';
                                }
                                if (this_ifrm.attr("data-assessment-question-id") == value.search_name) {
                                    myHtml += "<div class='prediv'><div class='radio'> <label class='wordBreak'><input type='radio' name='questionId' data-value-title='" + value.title + "' value='" + value.search_name + "' checked='checked'>" + value.title + "</label><button class='btn btn-default2 prevquestbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.search_name + "'>Preview</button></div></div>";
                                } else {
                                    myHtml += "<div class='prediv'><div class='radio'> <label class='wordBreak'> <input type='radio' name='questionId' data-value-title='" + value.title + "' value='" + value.search_name + "'>" + value.title + "</label><button class='btn btn-default2 prevquestbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.search_name + "'>Preview</button></div></div>";
                                }

                            });
                            var qNumberDiv = "<div id='qNumber' style='padding-top: 15px;'> Question Number: <input type='text' name='questionNumber' style='width:45px;'> <i>Optional field. Add a Question number that you want to see in your widget</i></div>";
                            myHtml += "</div>";
                            jQuery("#questionLoader").html(myHtml);
                            htmlEditor.bindAssessmentEvents();
                            jQuery("#qNumber").remove();
                            jQuery(qNumberDiv).insertAfter("#questionLoader");
                            jQuery(".prevquestbtn").off().on('click', function() {
                                var short_code = jQuery(this).attr("value");
                                angular.element(document.getElementById('questionLoader')).scope().preview_question(short_code);
                            });
                            jQuery('#dropDownSelection').text(htmlEditor.selectedTitle);
                            //setting value of optional field question_number
                            jQuery('input[name=questionNumber]').val(this_ifrm.attr("data-assessment-question-number"));

                        }
                        jQuery('input[name=questionNumber]').focus(function() {
                            if (jQuery('input[name=questionId]:checked').length > 0)
                                jQuery('#AssessmentModal .assessment_conf').attr("disabled", false);
                        });
                        jQuery('input[name=questionId]').change(function() {
                            jQuery('#AssessmentModal .assessment_conf').attr("disabled", false);
                            jQuery('input[name=questionNumber]').val('');
                            if (jQuery('input[name=questionId]:checked').val() === this_ifrm.attr("data-assessment-question-id")) {
                                jQuery('input[name=questionNumber]').val(this_ifrm.attr("data-assessment-question-number"));
                            }
                        });
                    }, 500);
                    //console.info(htmlEditor.questiondatalist.question_list);
                }
            });
        }
    } else {
        jQuery(".assessmentType:first").prop('checked', true);
        if (htmlEditor.isGlobalProject) {
            params.dataApi = htmlEditor.PXEmappedAPI.globalAssessmentGetUrl;
        } else {
            if (filter) {
                if (filter === 'global') {
                    params.dataApi = htmlEditor.PXEmappedAPI.globalAssessmentGetUrl;
                } else if (filter === 'local') {
                    params.dataApi = htmlEditor.PXEmappedAPI.localAssessmentGetUrl;
                } else if (filter === "localglobal") {
                    params.dataApi = htmlEditor.PXEmappedAPI.assessmentGetUrl;
                }
            } else {
                params.dataApi = htmlEditor.PXEmappedAPI.assessmentGetUrl;
            }
        }
        params.param = "";
        htmlEditor.callAssessmentData(params);
        // if (htmlEditor.assessmentDataResponse.assessment_list == null) {
        //     swal({ title: "Warning!", type: "warning", text: "No assessment found" });

        // }

        var myHtml = "";
        if (htmlEditor.assessmentDataResponse.assessment_list) {
            myHtml = "<div>";
            jQuery.each(htmlEditor.assessmentDataResponse.assessment_list, function(key, value) {
                var disabled = '';
                if (parseInt(value.total_question) == 0) {
                    var disabled = 'disabled';
                }
                myHtml += "<div class='prediv'><div class='radio'> <label class='wordBreak'> <input type='radio' name='assessmentId' data-value-title='" + value.title + "'  value='" + value.shortcode + "'>  " + value.title + "</label> <button class='btn btn-default2 prevbtn' style='float:right;padding:2px 12px;' " + disabled + " value='" + value.shortcode + "'>Preview</button></div></div>";

            });
            myHtml += "</div>"
            jQuery("#assessmentLoader").html(myHtml);
            htmlEditor.bindAssessmentEvents();
            jQuery("#qNumber").remove();
        } else {
            jQuery("#assessmentLoader").html("<h4>No assessment found!!</h4>");
        }

    }

    jQuery(".prevbtn").off().on("click", function() {
        var short_code = jQuery(this).attr("value");
        angular.element(document.getElementById('advancedAssessmentSearch')).scope().preview_assesment(short_code);
    });
    jQuery(".prevquestbtn").off().on('click', function() {
        var short_code = jQuery(this).attr("value");
        angular.element(document.getElementById('questionLoader')).scope().preview_question(short_code);
    });

    // jQuery(document).off().on('click', '#questionLoader .prevquestbtn', function() {
    //     var short_code = jQuery(this).attr("value");
    //     angular.element(document.getElementById('questionLoader')).scope().preview_question(short_code);
    // });
    // jQuery("#questionLoader .prevquestbtn").off().on("click", function(e) {
    //     e.preventDefault();
    //     debugger;
    //     alert("pre");
    //     //var short_code = jQuery(this).attr("value");
    //     //angular.element(document.getElementById('questionLoader')).scope().preview_question(short_code);
    // });


    // jQuery('#dropdownList').on('click', '.selectedQuestion', function() {




    //console.log(jQuery('input[name=assessmentId]:checked').val());

    jQuery('#AssessmentModal').modal('show');
    htmlEditor.callQuestionData = function(params) {
        //console.info(params.assessment_id);
        jQuery.ajax({
            url: params.dataApi + '/assessment_id/' + params.assessment_id + '/project_id/' + params.project_id + '?access_token=' + access_token,
            async: false,
            method: 'GET',
            data: params.data,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                //console.log(data);
                if (data.status == '200') {
                    if (data.data) {
                        htmlEditor.questiondatalist = data.data;
                    } else {
                        htmlEditor.questiondatalist = [];
                    }
                } else if (data.status == '400') {
                    swal({ title: "Warning!", type: "warning", text: "Sorry! Access Prohibited!" });
                } else {
                    htmlEditor.questiondatalist = [];
                }

            }
        });
    }


    jQuery('#AssessmentModal .assessment_conf').off('click').on('click', function() {
        var arguments = {};
        arguments.assessmentVal = params.assessment_id;
        arguments.questionVal = jQuery('input[name=questionId]:checked').val();
        arguments.iframeEntityType = jQuery('input[name=Assessment]:checked').val();
        question_number = jQuery('input[name=questionNumber]').val();

        this_ifrm.attr("data-assessment-id", arguments.assessmentVal);
        this_ifrm.attr('data-iframeEntityType', arguments.iframeEntityType);
        if (arguments.iframeEntityType == '2') {
            this_ifrm.attr("data-assessment-question-id", '');
        } else {
            this_ifrm.attr("data-assessment-question-id", arguments.questionVal);
        }

        params.dataApi = htmlEditor.PXEmappedAPI.assessmentDataSaveUrl;
        params.param = { assessment_id: arguments.assessmentVal, entity_type_id: arguments.iframeEntityType, question_id: arguments.questionVal, question_number: question_number, object_id: node_id };
        //htmlEditor.saveAssessmentData(params);

        jQuery.when(htmlEditor.saveAssessmentData(params)).done(function(data, textStatus, jqXHR) {
            this_ifrm.attr("src", htmlEditor.assessmentSaveDataResponse.widget_launch_file);
            this_ifrm.attr("data-path", htmlEditor.assessmentSaveDataResponse.json_file_path);
            //setting value of optional field question_number
            if (htmlEditor.assessmentSaveDataResponse.json_value.indexOf("question_number") !== -1) {
                question_number = JSON.parse(htmlEditor.assessmentSaveDataResponse.json_value).quiz[0].question_number;
                if (question_number !== null)
                    this_ifrm.attr("data-assessment-question-number", question_number);
            }
        });


        if (arguments.iframeEntityType == 3) {
            var typeText = 'Question: ' + jQuery('input[name=questionId]:checked').attr('data-value-title');
            this_ifrm.attr("data-question-title", jQuery('input[name=questionId]:checked').attr('data-value-title'));
            this_ifrm.attr("data-assessment-title", '');
        } else if (arguments.iframeEntityType == 2) {
            var typeText = 'Assessment: ' + jQuery('input[name=assessmentId]:checked').attr('data-value-title');
            this_ifrm.attr("data-assessment-title", jQuery('input[name=assessmentId]:checked').attr('data-value-title'));
            this_ifrm.attr("data-question-title", '');
        }
        var createInfo = typeText; //+'<br> Height: '+iframeHeight+'px <br> Width: '+iframeWidth+'px';
        this_ifrm.find(".url-link").html(createInfo);
        this_ifrm.trigger("click");
        //console.info(jQuery(this_ifrm).outerHTML());
        jQuery('#AssessmentModal').modal('hide');

        htmlEditor.enableSave();

    });

}
htmlEditor.getLocalGlobalForAssessmentAdvanceSearch = function() {
    var asset_local_global = {};
    var fetchFrom = jQuery("#AssessmentModal .loc-or-global.active").text();
    var actives = jQuery("#AssessmentModal .loc-or-global.active").length;
    if (actives > 1) {
        asset_local_global.local = true;
        asset_local_global.global = true;
    } else {
        if (fetchFrom.toLowerCase() === "local") {
            asset_local_global.local = true;
            asset_local_global.global = false;
        } else if (fetchFrom.toLowerCase() === "global") {
            asset_local_global.local = false;
            asset_local_global.global = true;
        } else if (fetchFrom.toLowerCase() === "all") {
            asset_local_global.local = true;
            asset_local_global.global = true;
        }
    }
    return asset_local_global;
}
htmlEditor.callAssessmentData = function(params, onSearch) {
    if (onSearch) {
        params.dataApi = serviceEndPoint + 'api/v1/assessment/project_id/' + project_id;
    }
    var request_type = 'GET';
    htmlEditor.assessmentDataResponse = [];
    if (htmlEditor.advanced_search.enableSearch) {
        htmlEditor.advanced_search.params.access_token = access_token;
        htmlEditor.advanced_search.params.assessment_local_global = JSON.stringify(htmlEditor.getLocalGlobalForAssessmentAdvanceSearch());
        htmlEditor.advanced_search.params.pageNumber = 1;
        htmlEditor.advanced_search.params.itemsPerPage = 10;
        htmlEditor.advanced_search.params.project_id = project_id;
        request_type = "POST";
        params = {};
        params.param = {};
        params.dataApi = {};
        params.param = htmlEditor.advanced_search.params;
        params.dataApi = serviceEndPoint + 'api/v1/assessment-search/project_id/' + project_id;
    }
    jQuery.ajax({
        url: params.dataApi,
        async: false,
        method: request_type,
        data: params.param,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            if (data.status == '200') {
                if (data.data) {
                    htmlEditor.assessmentDataResponse = data.data;
                } else {
                    htmlEditor.assessmentDataResponse = [];
                }
            } else if (data.status == '400') {
                swal({ title: "Warning!", type: "warning", text: "Sorry! Access Prohibited!" });
            } else {
                htmlEditor.assessmentDataResponse = [];
            }
        },
        error: function(data) {

        }
    });
}

htmlEditor.saveAssessmentData = function(params) {
    jQuery.ajax({
        url: params.dataApi,
        async: false,
        method: 'GET',
        data: params.param,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            htmlEditor.assessmentSaveDataResponse = data;
        },
        error: function(data) {
            //console.log(data);
        }
    });
}

// Settings modal window for the Quad elements
htmlEditor.enableQuadSetting = function(theFrame) {
    jQuery('#quadModal').modal('show');
    var scrollStatus = "no";
    var this_ifrm = theFrame;
    var this_ifrm_hgt = this_ifrm.height();
    var this_ifrm_wdth = this_ifrm.width();
    var this_ifrm_url = jQuery.trim(this_ifrm.attr('src'));
    var this_qID = jQuery.trim(this_ifrm.attr('data-qID')) == undefined ? '' : jQuery.trim(this_ifrm.attr('data-qID'));
    var this_entity_type_id = this_ifrm.attr('data-iframeentitytype') == undefined || this_ifrm.attr('data-iframeentitytype').length == 0 ? '0' : this_ifrm.attr('data-iframeentitytype');
    var this_ifrm_scroll = this_ifrm.attr("scrolling");

    function checkConfirm(flg) {
        this_entity_type_id = jQuery("#quadFrameEntityType :selected").attr('value');
        var iframeTermVal = jQuery.trim(jQuery('#quadFrameTerm').val());
        var iframeHeight = jQuery.trim(jQuery('#quadFrameHeight').val());
        var iframeWidth = jQuery.trim(jQuery('#quadFrameWidth').val());
        if (flg && Number(this_entity_type_id) && iframeTermVal.length && iframeHeight > 0) {
            jQuery('#quadModal .iframe_conf').removeAttr('disabled');
        } else {
            jQuery('#quadModal .iframe_conf').attr('disabled', 'true');
        }
    }

    jQuery('#quadFrameEntityType').on('change', function() {
        checkConfirm(true);
    });

    jQuery('#quadFrameTerm, #quadFrameHeight, #quadFrameWidth').on('keyup', function() {
        checkConfirm(true);
    });

    jQuery('#quadFrameHeight').on('keydown', dotChecker);
    jQuery('#quadFrameHeight').on('keyup', function(e) {
        if (e.keyCode === 190)
            jQuery(this).val((jQuery(this).val()).split('>')[0])
    });

    jQuery('#quadFrameWidth').on('keydown', dotChecker);
    jQuery('#quadFrameWidth').on('keyup', function(e) {
        if (e.keyCode === 190)
            jQuery(this).val((jQuery(this).val()).split('>')[0])
    });

    function dotChecker(e) {
        var contArray = (jQuery(this).val()).split('');
        var dotLength = 0;
        jQuery(contArray).each(function(i, v) {
            if (v === '.')
                dotLength++;
        });

        if (dotLength > 0 && (e.keyCode === 110 || e.keyCode === 190))
            return false;

        if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && e.ctrlKey === true) || (e.keyCode == 67 && e.ctrlKey === true) || (e.keyCode == 88 && e.ctrlKey === true) || (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    }
    jQuery('#quadFrameTerm').val(this_qID);
    jQuery('#quadFrameEntityType').val(this_entity_type_id);
    jQuery('#quadFrameHeight').val(this_ifrm_hgt);
    jQuery('#quadFrameWidth').val(this_ifrm_wdth);
    if (this_ifrm_scroll == "yes") {
        jQuery("#quadFrameScroll").prop("checked", true);
    } else {
        jQuery("#quadFrameScroll").prop("checked", false);
    }

    jQuery('#quadModal .iframe_conf').off('click').on('click', function() {
        htmlEditor.enableSave();
        var iframeTermVal = jQuery.trim(jQuery('#quadFrameTerm').val());
        var iframeHeight = jQuery.trim(jQuery('#quadFrameHeight').val());
        var iframeWidth = jQuery.trim(jQuery('#quadFrameWidth').val());
        var iframeEntityType = jQuery('#quadFrameEntityType').val();
        htmlEditor.getQuadResponse(iframeTermVal, iframeEntityType);
        if (iframeHeight >= 50 && iframeHeight <= 125 || iframeHeight < 50) { //Reduce the font-size & stuffs if height is less.
            this_ifrm.find(".lm-rochak").addClass("width125");
        } else {
            this_ifrm.find(".lm-rochak").removeClass("width125");
        }

        if (jQuery("#quadFrameScroll").is(":checked")) {
            scrollStatus = "yes";
        } else {
            scrollStatus = "no";
        }

        var iframeScroll = scrollStatus;
        if (iframeTermVal.length && iframeHeight > 0) {

            //var source = clientUrl + '/widgets/quad/questionPreview.html';
            var source = s3_domain_url + '/widgets/quad/questionPreview.html';
            this_ifrm.attr("src", source);
            this_ifrm.attr("data-qID", iframeTermVal);
            this_ifrm.attr('data-iframeEntityType', iframeEntityType);
            this_ifrm.css("height", iframeHeight);
            this_ifrm.css("width", iframeWidth);
            this_ifrm.attr("scrolling", iframeScroll);
            var typeText = this_ifrm.attr('data-iframeEntityType') == 3 ? 'Question' : this_ifrm.attr('data-iframeEntityType') == 2 ? 'Assessment' : '';
            var createInfo = 'Quad ' + typeText + ': ' + iframeTermVal; //+'<br> Height: '+iframeHeight+'px <br> Width: '+iframeWidth+'px';
            this_ifrm.find(".url-link").html(createInfo);
            this_ifrm.trigger("click");
            //                this_ifrm.find(".gadgetURL").text(iframeTermVal);
            //                this_ifrm.find(".gadgetHt").text(iframeHeight);
            //                this_ifrm.find(".gadgetWidth").text(iframeWidth);
            jQuery('#quadModal').modal('hide');
        } else {
            swal("Input Correct values");
        }
    });
    checkConfirm(false);
};
var qidGetItem = "";
htmlEditor.getQuadResponse = function(qID, type) {
    var apiURL = htmlEditor.PXEmappedAPI.quadAPI;

    var param = {
        'repo_id': qID,
        'project_id': project_id,
        'project_type_id': project_type_id,
        'entity_type_id': type,
    };
    htmlEditor.dataPath = {
        "data": "",
        "path": ""
    };
    if (sessionStorage.getItem(qID)) {
        var qIDdata = JSON.parse(sessionStorage.getItem(qID));
        htmlEditor.dataPath.data = qIDdata.data;
        htmlEditor.dataPath.path = qIDdata.path;

    } else {
        jQuery.ajax({
            url: apiURL,
            async: false,
            method: 'GET',
            data: param,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                var json_value = data.json_value;
                var json_file_path = data.json_file_path;
                var quadJsonData = {
                    'data': json_value,
                    'path': json_file_path
                };
                sessionStorage.setItem(qID, JSON.stringify(quadJsonData));
                var qIDdata = JSON.parse(sessionStorage.getItem(qID));
                htmlEditor.dataPath.data = qIDdata.data;
                htmlEditor.dataPath.path = qIDdata.path;
                qidGetItem = sessionStorage.getItem("quadIds");

                if (qidGetItem == null) {
                    qidGetItem = sessionStorage.setItem("quadIds", qID);
                } else {
                    var searchQid = qidGetItem.indexOf(qID);
                    if (searchQid == -1) {
                        qidGetItem += ',' + qID;
                        sessionStorage.setItem("quadIds", qidGetItem);
                    }
                }
            },
            error: function() {
                alert("Error occured");
            }

        });

    }
};

var questionidGetItem = "";
//var assessmentObj={};
htmlEditor.getAssessmentResponse = function(arguments) {
    var apiURL = htmlEditor.PXEmappedAPI.assessmentAPI;
    var QuestionURL = htmlEditor.PXEmappedAPI.questionAPI;
    var param = {
        'assessment_id': arguments.assessmentId,
        'question_id': arguments.questionId,
        'project_id': project_id,
        'project_type_id': project_type_id,
        'entity_type_id': arguments.iframeEntityType,
    };
    htmlEditor.dataPath = {
        "data": "",
        "path": ""
    };
    /*if (sessionStorage.getItem(questionId)) {
     var questionIdData = JSON.parse(sessionStorage.getItem(questionId));
     htmlEditor.dataPath.data = questionIdData.data;
     htmlEditor.dataPath.path = questionIdData.path;

     } else {*/

    jQuery.ajax({
        url: apiURL,
        async: false,
        method: 'GET',
        data: param,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            //console.log(data);
            var json_value = data.json_value;
            var json_file_path = data.json_file_path;
            var quadJsonData = {
                'data': json_value,
                'path': json_file_path
            };
            /*sessionStorage.setItem(questionId, JSON.stringify(quadJsonData));
             var questionIdData = JSON.parse(sessionStorage.getItem(questionId));
             htmlEditor.dataPath.data = questionIdData.data;
             htmlEditor.dataPath.path = questionIdData.path;
             assessidGetItem = sessionStorage.getItem("questionIds");

             if (assessidGetItem == null) {
             assessidGetItem = sessionStorage.setItem("questionIds", questionId);
             } else {
             var searchQid = assessidGetItem.indexOf(questionId);
             if (searchQid == -1) {
             assessidGetItem += ',' + questionId;
             sessionStorage.setItem("questionIds", questionidGetItem);
             }
             }*/
        },
        error: function() {
            alert("Error occured");
        }

    });


    //}
};

htmlEditor.enableSettingForObject = function(frame) {
    jQuery('#objectModal').modal('show');
    var oldurl = frame.attr("src");
    jQuery("#objectURL").val(oldurl);
    jQuery(".object_conf").off().on("click", function() {
        var newurl = jQuery("#objectURL").val().trim();
        frame.attr("src", newurl);
        frame.find(".url-link").text(newurl);
        jQuery('#objectModal').modal('hide');
    })
}

//Settings panel on clicking the edit button for external widgets.
htmlEditor.exwidgetSetting = function(frame) {
    //config page

    htmlEditor.thisFrame = frame;
    var theClasses = htmlEditor.thisFrame.attr("class") === undefined ? "" : htmlEditor.thisFrame.attr("class"),
        widgetType = htmlEditor.thisFrame.attr('widget-type') ? htmlEditor.thisFrame.attr('widget-type') : htmlEditor.thisFrame.attr('data-widget-type'),
        configPath = "",
        configFrameEl = jQuery('#exwidgetModal').find('iframe'),
        configModalHeaderEl = jQuery('#exwidgetModal').find('#modalBoxTitle'),
        widget_id = htmlEditor.thisFrame.attr('widget-id') ? htmlEditor.thisFrame.attr('widget-id') : htmlEditor.thisFrame.attr('data-widget-id');

    if (widget_id) {
        try {
            configPath = widgetScope.configPage[widget_id];
        } catch (exc) {
            //console.log(exc);
        }

    }

    configFrameEl.attr('src', configPath); //Sets widgets dynamic frame path
    configModalHeaderEl.html('Edit ' + (widgetType.toUpperCase())); //Sets widgets modal header value
    jQuery("#exwidgetModal").modal("show");
    return;
    //    var editPath = clientUrl + '/widgets/flashcard/config/edit.html'
    jQuery("#fcConf").attr("disabled", false);
    var path = thisFrame.attr("data-path");
    var height = thisFrame.height();
    var wide = thisFrame.width();
    jQuery("#exwidgetModal").modal("show");
    jQuery("#widgetHeight").val(height);
    jQuery("#widgetWidth").val(wide);
    jQuery("#add-fields").off().on("click", function() {
        var str = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled selected>Select Type</option><option value="text">Text</option><option value="image">Image</option><option value="video">Video</option><option value="audio">Audio</option></select></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title" disabled/></td><td><input type="text" class="form-control fc-desc" placeholder="Description" disabled/></td></tr>';
        jQuery("table#editFC").append(str);
    })
    if (path !== "undefined") { //If the values are already saved once, show them to be edited.
        jQuery.ajax({
            url: path,
            async: false,
            method: 'GET',
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                var valAsStr = data.replace("var ex_widgets = ", "");
                var semiparsed = JSON.parse(valAsStr);
                var realVal = JSON.parse(semiparsed);
                populateFields(realVal);
            },
            error: function() {
                alert("Error occured..Retry");
            }
        })

        //Populate options based on the response received
        function populateFields(dataPack) {
            var actName = dataPack.quiz[0].TemplateName;
            var flashes = dataPack.quiz[0].OptionStem;
            var str = "";
            var strGenerator = function(optiontype) {
                var nstr;
                switch (optiontype) {
                    case "text":
                        nstr = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled>Select Type</option><option value="text" selected>Text</option><option value="image">Image</option><option value="video">Video</option><option value="audio">Audio</option></select></td><td><input type="text" class="form-control fc-value" placeholder="Enter Text" value="' + flashes[i].OptionValue + '" /></td><td><input type="text" class="form-control fc-desc" placeholder="Description" value="' + flashes[i].OptionDescription + '"/></td></tr>';
                        break;

                    case "image":
                        nstr = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled>Select Type</option><option value="text">Text</option><option value="image" selected>Image</option><option value="video">Video</option><option value="audio">Audio</option></select></td><td><button class="fc-asset-selector">Select</button><input type="text" class="form-control fc-value" placeholder="" value="' + flashes[i].OptionValue + '" disabled/></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title" value="' + flashes[i].OptionTitle + '" /></td><td><input type="text" class="form-control fc-desc" placeholder="Description" value="' + flashes[i].OptionDescription + '"/></td></tr>';
                        break;

                    case "video":
                        nstr = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled>Select Type</option><option value="text">Text</option><option value="image">Image</option><option value="video" selected>Video</option><option value="audio">Audio</option></select></td><td><button class="fc-asset-selector">Select</button><input type="text" class="form-control fc-value" placeholder="" value="' + flashes[i].OptionValue + '" disabled/></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title" value="' + flashes[i].OptionTitle + '" /></td><td><input type="text" class="form-control fc-desc" placeholder="Description" value="' + flashes[i].OptionDescription + '"/></td></tr>';
                        break;

                    case "audio":
                        nstr = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled>Select Type</option><option value="text">Text</option><option value="image">Image</option><option value="video">Video</option><option value="audio" selected>Audio</option></select></td><td><button class="fc-asset-selector">Select</button><input type="text" class="form-control fc-value" placeholder="" value="' + flashes[i].OptionValue + '" disabled/></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title" value="' + flashes[i].OptionTitle + '" /></td><td><input type="text" class="form-control fc-desc" placeholder="Description" value="' + flashes[i].OptionDescription + '"/></td></tr>';
                        break;
                }
                return nstr;
            }
            for (var i = 0; i < flashes.length; i++) {
                str += strGenerator(flashes[i].OptionDataType);
            }
            jQuery("table#editFC").html(str);
        }
    } else {
        var str = '<tr><td><select class="form-control dropdown fc-type" onchange="htmlEditor.changeInputType(this)"><option disabled selected>Select Type</option><option value="text">Text</option><option value="image">Image</option><option value="video">Video</option><option value="audio">Audio</option></select></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title" disabled/></td><td><input type="text" class="form-control fc-desc" placeholder="Description" disabled/></td></tr>';
        jQuery("table#editFC").html(str);
    }


    //    jQuery("#exwidgetModal").modal("show");
    var id = thisFrame.attr("id");
    var source = clientUrl + "/widgets/flashcard/index.html";
    thisFrame.attr("src", source);
    //    var editContainer = jQuery("#exwidgetModal").find(".editWidgetOptions");
    //    editContainer.load(editPath, function() {

    //To be executed on successfully saving the widget data
    function successHandler(data) {
        jQuery("#exwidgetModal").modal("hide");
        thisFrame.attr("data-path", data.json_file_path);
        htmlEditor.removeUnwanteds(jQuery(".cke_wysiwyg_frame").contents())
    }

    //Call the api to save the changes made
    function saveWidgetData(widgetCont, repoId) {
        var param = {
            "project_id": project_id,
            "project_type_id": project_type_id,
            "ex_widgets": widgetCont,
            "repo_id": repoId
        };
        jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.saveWidgetDataAPI,
            async: false,
            method: 'POST',
            data: param,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data) {
                successHandler(data);
            },
            error: function() {
                alert("Error occured..Retry");
            }
        })
    }

    //On hitting the confirm Button
    jQuery("#fcConf").off().on("click", function() {
            jQuery(this).attr("disabled", true);
            var arrOfInputs = [];
            jQuery("table#editFC").find("tr").each(function() { //Creat the object that is to be saved.
                var obj = {};
                var type = jQuery(this).find(".fc-type").val();
                try {
                    var title = jQuery(this).find(".fc-title").val().trim();
                } catch (exc) {

                }
                var valueFC = jQuery(this).find(".fc-value").val().trim();
                var desc = jQuery(this).find(".fc-desc").val().trim();
                obj.OptionDataType = type;
                obj.OptionTitle = title;
                obj.OptionValue = valueFC;
                obj.OptionDescription = desc;
                arrOfInputs.push(obj);
            });

            var ht = jQuery("#widgetHeight").val().trim();
            var wid = jQuery("#widgetWidth").val().trim();
            thisFrame.css("height", ht);
            thisFrame.css("width", wid);

            var dataToBeSent = {
                "quiz": [{
                    "TemplateName": "FlashCard",
                    "TemplateType": "FlashCard",
                    "QuestionId": id,
                    "QuestionText": "Question text goes here",
                    "OptionStem": arrOfInputs
                }]
            }
            var dataAsStr = JSON.stringify(dataToBeSent);
            saveWidgetData(dataAsStr, id);
        })
        //    });
}


//    Change the input types depending on selection
htmlEditor.changeInputType = function(thisSel) {
    var type = thisSel.value;
    switch (type) {
        case "text":
            forTextType();
            break;

        case "image":
            forImageType();
            break;
    }

    function forTextType() {
        var html = '<td><input type="text" class="form-control fc-value" placeholder="Enter Text"/></td>';
        //        jQuery(thisSel).parents("tr").find(".fc-title").parent().replaceWith(html);
        if (jQuery(thisSel).parents("tr").find(".fc-value").length) {
            jQuery(thisSel).parents("tr").find(".fc-value").parent("td").remove();
        }
        jQuery(thisSel).parents("tr").find(".fc-desc").attr("disabled", false);
        jQuery(thisSel).parents("tr").find(".fc-title").parent().replaceWith(html);
    }

    function forImageType() {
        var html = '<td><button class="btn btn-primary fc-asset-selector">Select</button><input type="text" class="form-control fc-value" placeholder="" disabled/></td><td><input type="text" class="form-control fc-title" placeholder="Enter Title"/></td>';
        if (jQuery(thisSel).parents("tr").find(".fc-title").length) {
            jQuery(thisSel).parents("tr").find(".fc-title").parent().replaceWith(html);
        } else {
            jQuery(thisSel).parents("tr").find(".fc-value").parent().replaceWith(html);
        }
        jQuery(thisSel).parents("tr").find(".fc-desc").attr("disabled", false);
    }

    jQuery(document).on("click", ".fc-asset-selector", function() {
        jQuery("#assetSelector").modal("show");
        var theValueField = jQuery(this).siblings(".fc-value");

        var num_of_items = 10; //Set no. of items in a given page.
        getAllImages(1, num_of_items);
        //Fetching the images from asset library. Similar to htmlhtmlEditor.callImages();
        function getAllImages(page, num_of_items) {
            var numPerPage = num_of_items;
            var params = {
                "pageNumber": page,
                "itemsPerPage": numPerPage
            };

            jQuery.ajax({
                url: htmlEditor.PXEmappedAPI.getImagesAPI,
                async: true,
                type: "GET",
                data: params, // object_type=104[image]
                xhrFields: {
                    withCredentials: true
                },
                crossDomain: true,
                success: function(data) {
                    populateImages(data);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    swal("Error occured while fetching Images")
                },
            });
        }


        function populateImages(data) {
            var d = data.data;
            if (d != null && d.length > 0) {
                var imgContainers = jQuery("#assetSelector").find(".inner-image-content");
                var str = "";
                jQuery("#assetSelector .paginate").show();
                for (var i = 0; i < d.length; i++) {
                    str += '<div class="get-container"><div class="box-theme"><div class="images-select"><input type="checkbox" id="cbtest" disabled/><label for="cbtest" class="check-box"></label></div><div class="block-img"><img class="assetImg global_' + d[i].is_global + '" src="' + d[i].asset_location + '" alt=""/></div><div class="box-theme-title">' + d[i].original_name + '</div></div></div>';
                }
                str += '<div class="clearfix"></div>';
                //                if (page == 1) {        //Pagination implementation
                var total_images = data.totalAssets;
                var max_page = Math.ceil(total_images / num_of_items);
                //Pagination using plugin.
                jQuery("#assetSelector .paginate").bootpag({
                    total: max_page, // total pages
                    page: 1, // default page
                    maxVisible: 10, // visible pagination
                    leaps: true // next/prev leaps through maxVisible
                }).on("page", function(event, num) {
                    getAllImages(num, num_of_items);
                });
                //                }
                imgContainers.html(str);

                var container = imgContainers;
                container.find('.box-theme').off("click").on("click", function() {
                    var is_global = jQuery(this).find('img').attr('class').split('_')[1];
                    if (is_global == 1 && htmlEditor.userProjectPermission['asset.use_global'].grant == false) {
                        jQuery('.box-theme').removeClass('active');
                        swal("Permission Denied!");
                    } else {
                        if (jQuery(this).hasClass('active')) {
                            jQuery(this).removeClass('active');
                            jQuery(this).find('.check-box').removeClass('checked');
                        } else {
                            jQuery('.box-theme').removeClass('active');
                            jQuery(this).addClass('active');
                            jQuery(this).find('.check-box').addClass('checked');
                        }
                    }
                    var imgUrl = jQuery('.box-theme.active').find("img").attr("src");
                    if (imgUrl !== undefined) {
                        jQuery("#widgetConf").attr("disabled", false);
                        theValueField.val(imgUrl);
                        jQuery("#assetSelector").modal("hide");
                    } else {
                        jQuery("#imgCont").attr("disabled", true);
                    }
                });
            } else {
                //                if (page == 1)
                jQuery("#assetSelector .modal-content-container").html("<p>No assets Found. Upload some and retry.</p>");
                jQuery("#assetSelector .paginate").hide();
            }
        }
    })
}
htmlEditor.getWidgetType = function(cl) {
    var clList = [],
        wigdetType = "";
    try {
        clList = cl.split(' ');
    } catch (exc) {
        clList = cl;
    }
    //widgetClasses
    for (var ind = 0; ind < clList.length; ind++) {
        if (jQuery.inArray(clList[ind], widgetClasses) != -1) {
            wigdetType = clList[ind];
        }
    }

    if (!wigdetType.length)
        wigdetType = false;

    return wigdetType;
}

/**
 * A jQuery plugin to display the frame/div
 * @returns {jQuery}
 */
jQuery.fn.outerHTML = function() {
    return jQuery('<div />').append(this.eq(0).clone()).html();
};