var listingCfi;
var dropdown;
var newparent;
var counter;
var tracker = 0;
var data;
var cfi_title;
var elmIds;
var cfiName = "";
var eventExists;
var showAccordionList = function () { //method to toggle hide/show functionalities
    jQuery("#accordion").show();
    jQuery('.tree').remove();
    jQuery('#createCFI').prop('disabled', false);
    jQuery("#cfiTitle").val("")
    jQuery("#textContainer").hide();
    jQuery(".cfiAddingFooter").hide();
    // remove highlighter from content
    jQuery(".cke_wysiwyg_frame").contents().find('body').find('.cfiHighlighter').removeClass('cfiHighlighter');
    if (!jQuery('.cfiList').length) {
        jQuery('#accordionCfi').show();
    }

}
var cfiStringToReturn = [];
var getSelectedIds = function () { //method used return "selected" class if cfi_id matches with element id.
    elmIds = [];
    jQuery(".selected").each(function (ind, item) {
        elmIds.push(item.getAttribute('data-id'));
    });
    var newObj = {
        "title": jQuery("#cfiTitle").val(),
        "element_ids": JSON.stringify(elmIds)
    }
    return newObj;
}
var bindCFIEvents = function () { //method used t bind events to the interactive elements
     jQuery('#createCFI').off().on('click', function () {
        if (typeof htmlEditor.userProjectPermission['cfi.create'] != "undefined" && !htmlEditor.userProjectPermission['cfi.create'].grant) {
            swal("Permission Denied!");
            return false;
        }
        counter = 0;
        tracker = 0;
        jQuery("h3[class^='cfiList'][aria-selected='true']").trigger('click');
        jQuery('.selected').removeClass('selected');
        jQuery("#textContainer").show();
        jQuery(this).prop('disabled', true);
        jQuery('#accordionCfi').hide();
        jQuery("#accordion").hide();
        htmlEditor.toggler = [];
        data.setData(newparent);
        data.getData();
        jQuery('.cfi_view').append(cfiStringToReturn.join('') + '<div class="cfiAddingFooter" style="padding: 14px"><button class="addNewCfi disabledCls" disabled>CONFIRM</button><button class="cancelCreation">CANCEL</button></div>');
        cfiStringToReturn = [];
        jQuery(".cfi").off().on('click', addCFIsnippet);
        jQuery(".addNewCfi").off().on('click', function () {
            if (typeof htmlEditor.userProjectPermission['cfi.create'] != "undefined" && !htmlEditor.userProjectPermission['cfi.create'].grant) {
                swal("Permission Denied!");
                return false;
            }
            if (jQuery('#cfiTitle').val().trim() == "") {
                swal("Oops...", "CFI name cannot be empty!", "error");
                return;
            }
            jQuery("h3[class^='cfiList'][aria-selected='true']").trigger('click');
            addCustomCFI(getSelectedIds());
        });
        jQuery(".cancelCreation").off().on('click', function () {
            showAccordionList();
        });
    });
    jQuery(".cfi").off().on('click', addCFIsnippet);
    jQuery(".cfiList").off().on('click', function (event) {
        eventExists = event.originalEvent;
        cfiName = "";
        jQuery(".cke_wysiwyg_frame").contents().find('.cfiHighlighter').removeClass('cfiHighlighter');
        if (jQuery(this).hasClass('ui-state-active')) {
            return
        }
        var id = jQuery(this).attr('value');
        getCustomCFIById(id);


    });
   }
var validateInput = function () {
    jQuery('.cfiSave').removeAttr('disabled').removeClass('disabledCls');
}
var populateTreeWithCfi = function (cfiIds, id) { //method used to populate the nodes of the tree with cfi attached
    htmlEditor.toggler = cfiIds;
    data.setData(newparent);
    data.getData();
    jQuery('.tree').remove();
    var editTitle = '<div class="cfiname form-group"><label>Name:</label><input  maxlength="40" onkeydown="validateInput()" class="titleEditor form-control" type="text"/></div>';
    jQuery("#" + id).html(editTitle + cfiStringToReturn.join(''));
    jQuery("#" + id).append('<div class="cfiFooter"><button class="cfiSave disabledCls" disabled>CONFIRM</button><button class="cancelSave">CANCEL</button><i class="deleteCFI fa fa-trash-o" aria-hidden="true"></i></div>');
    cfiStringToReturn = [];
    counter = jQuery('.selected').length;
    addCfiHighlightertoDom();
    // scroll to the highlighter
    if (cfiIds[0] !== undefined) {
        toHighlightCfi(cfiIds[0]);
    }
    tracker = counter;
    var initVal = jQuery("h3[class^='cfiList'][aria-selected='true']").text();
    cfiName = initVal;
    jQuery(".titleEditor").val(initVal)
    jQuery(".titleEditor").off().on('keyup', function () {
        jQuery("h3[class^='cfiList'][aria-selected='true']").text(jQuery(this).val())
    })
    jQuery(".cfi").off().on('click', addCFIsnippet);
    jQuery('#accordion').accordion("refresh");
    // delete cfi
    jQuery(".deleteCFI").off().on('click', function () {
        if (typeof htmlEditor.userProjectPermission['cfi.delete'] != "undefined" && !htmlEditor.userProjectPermission['cfi.delete'].grant) {
            swal("Permission Denied!");
            return false;
        }
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
        function () {
            deleteCustomCFI(id);
        });
    });
    jQuery(".cfiSave").off().on('click', function () {
        if (typeof htmlEditor.userProjectPermission['cfi.update'] != "undefined" && !htmlEditor.userProjectPermission['cfi.update'].grant) {
            swal("Permission Denied!");
            return false;
        }
        if (jQuery('.titleEditor').val().trim() == "") {
            swal("Oops...", "CFI name cannot be empty!", "error");
            return;
        }
        jQuery(this).attr('disabled', true).addClass('disabledCls');
        var cfiData = getSelectedIds();
        cfiData.title = jQuery("h3[class^='cfiList'][aria-selected='true']").text()
        editCustomCFI(cfiData, id);
    });
    jQuery(".cancelSave").off().on('click', function () {
        jQuery("h3[class^='cfiList'][aria-selected='true']").text(cfiName);
        jQuery("h3[class^='cfiList'][aria-selected='true']").trigger('click');
    });
}
var addCfiHighlightertoDom = function () {
    if (eventExists) {
        jQuery('.selected').each(function (ind, val) {
            if (jQuery(".cke_wysiwyg_frame").contents().find('body').find('#' + val.getAttribute('data-id')).parents('.cfiHighlighter').length) {
                return;
            }
            jQuery(".cke_wysiwyg_frame").contents().find('body').find('#' + val.getAttribute('data-id')).addClass('cfiHighlighter');

        })
    }

};
function cfiContent() {
    var parseElm = {};
    return {
        setData: function (elm) {
            parseElm = elm;
        },
        getData: function () {
            genChild(parseElm, true);
        }
    }
}
;
var toHighlightCfi = function (id) {// method used to highlight DOM when a node is selected from tree
    id = id.replace(/(:|\.|\[|\])/g, "\\$1");
    if (eventExists) {
        if (jQuery(".cke_wysiwyg_frame").contents().find('body').find('#' + id).parents('.cfiHighlighter').length) {
            return;
        }
        jQuery(".cke_wysiwyg_frame").contents().find('body').find('#' + id).addClass('cfiHighlighter');
        var cfiAgainst = jQuery(".cke_wysiwyg_frame").contents().find('body').find('#' + id);
        var tobescrolled = cfiAgainst.offset().top - 20;
        jQuery(".cke_wysiwyg_frame").contents().find('body').animate({scrollTop: tobescrolled}, 1000);
    }

};
var setClass = function (elmId) {
    var x = jQuery.inArray(elmId, htmlEditor.toggler);
    if (x > -1) {
        return 'selected';
    }
    return "";
}
function genChild(Elem, FirstElem) { //generate the DOM tree
    if (FirstElem) {
        cfiStringToReturn.push('<ul class="tree">');
    } else {
        cfiStringToReturn.push('<ul>');
    }
    Elem.children().each(function (index, value) {
        var elmTagName = jQuery(value).tagName();
        if(elmTagName==="SCRIPT"){
            return false;
        }
        var elmId = jQuery(value).attr('id');
        var elmClassName = jQuery(value).className();
        var hasChild = jQuery(value).children().length > 0;
        var elmText = jQuery(value).text().substr(0, 15) + '...';
        var profileType = jQuery(value).attr('data-profile-deliveryformat') != undefined ? jQuery(value).attr('data-profile-deliveryformat') : false;
        var strAttribute = '';
        if (profileType) {
        }
        cfiStringToReturn.push('<li><a style="cursor:pointer" data-id="' + elmId + '" class="cfi ' + setClass(elmId) + '">' + elmTagName + ' [<span class="desc">' + elmText + '</span>]</a>');
        if (hasChild) {
            genChild(jQuery(value), false);
        }
        cfiStringToReturn.push('</li>');
    });
    cfiStringToReturn.push('</ul>');
}
var populateData = function () {
    cfi_title = '<div id="textContainer" class="cfiname form-group" style="display:none"><label>NAME:</label><input id="cfiTitle" class="form-control" maxlength="40" type="text"/></div>';
    var cfi_str1 = '<div class="add-new-cfi"><button id="createCFI" class="btn btn-default create new-action" data-loading-text="Creating New CFI...">Add New CFI</button><div class="new-ticket-container"></div></div>';
    var cfi_str2 = '<div class="panel-group" id="accordionCfi" style="display:none" role="tablist" aria-multiselectable="true"><div class="lm-rochak"><div><span class="fa fa-comments"></span></div><strong>No CFI added yet!</strong>Click on the button above to add new CFI</div></div>';
    var cfi_str = cfi_str1 + cfi_str2;
    jQuery('.cfi_view').html(cfi_str + cfi_title + dropdown);
    if (!listingCfi.length) {
        jQuery('#accordionCfi').show();
    }
    cfiStringToReturn = [];
    jQuery(function () {
        jQuery("#accordion").accordion({
            heightStyle: "fill",
            active: false,
            collapsible: true,
            animate: 100,
        });
    });

    // jQuery('.cfiList').eq(0).trigger('click');

    bindCFIEvents();
}
//========================================================================
//  Function that initiates TOC tree
//========================================================================
jQuery.fn.createCFI = function (args) {
    // ckeck create permission
    counter = jQuery('.selected').length;
    tracker = counter;
    data = '';
    data = new cfiContent();
    newparent = jQuery('<div></div>');
    newparent.append(jQuery(htmlEditor.getContent()));
    getCustomCFI();
};
var addCFIsnippet = function (event) { //method used to add CFI to the tree
    eventExists = event.originalEvent;
    // remove highlighter from current element and it's children
    jQuery(".cke_wysiwyg_frame").contents().find('body').find('#' + jQuery(this).attr('data-id')).removeClass('cfiHighlighter');
    jQuery(".cke_wysiwyg_frame").contents().find('body').find('#' + jQuery(this).attr('data-id')).find('.cfiHighlighter').removeClass('cfiHighlighter');
    if (!jQuery(this).hasClass('selected')) {
        var hasSiblingWithSelection = checkForAdjSibling(jQuery(this).parent());
        if (jQuery(this).parent().parent('li').children('a').hasClass('selected')) {
            return
        }
        if (jQuery(this).parent().find('a').hasClass('selected')) {
            jQuery(this).parent().find('a').removeClass('cfi').addClass('selected');
            tracker++;
            jQuery('.cfiSave').removeAttr('disabled').removeClass('disabledCls');
            jQuery('.addNewCfi').removeAttr('disabled').removeClass('disabledCls');
            toHighlightCfi(jQuery(this).attr('data-id'));
        }
        if (!hasSiblingWithSelection && jQuery('.selected').length > 0) {
            return false;
        } else {
            jQuery(this).parent().find('a').removeClass('cfi').addClass('selected');

            jQuery('.cfiSave').removeAttr('disabled').removeClass('disabledCls');
            jQuery('.addNewCfi').removeAttr('disabled').removeClass('disabledCls');
            toHighlightCfi(jQuery(this).attr('data-id'));
        }
    } else {
        if (jQuery(this).parent().parent('li').children('a').hasClass('selected')) {
            return
        } else {
            if (jQuery(this).parent().parents('li').children('.selected').length > 0) {
                return
            }
            if (nextANDprevSelected(jQuery(this).parent()) > 1) {
                return;
            } else {
                jQuery(this).parent().find('a').removeClass('selected').addClass('cfi');
                jQuery('.cfiSave').removeAttr('disabled').removeClass('disabledCls');
                jQuery('.addNewCfi').removeAttr('disabled').removeClass('disabledCls');
            }
        }
    }
    tracker = jQuery('.selected').length;
    if (counter === tracker) {
        jQuery('.addNewCfi').attr('disabled', true).addClass('disabledCls');
        jQuery('.cfiSave').attr('disabled', true).addClass('disabledCls');
    }
}
var nextANDprevSelected = function (elem) {//method used to check next and prev siblings
    var next = elem.next().children('.selected').length;
    var prev = elem.prev().children('.selected').length;
    return (next + prev)
}
var checkForAdjSibling = function (elem) {
    var next = elem.next().length;
    var prev = elem.prev().length;
    if (next > 0) {
        if (elem.next().children('a').hasClass('selected')) {
            return true;
        }
    }
    if (prev > 0) {
        if (elem.prev().children('a').hasClass('selected')) {
            return true;
        }
    }
    return false;
}