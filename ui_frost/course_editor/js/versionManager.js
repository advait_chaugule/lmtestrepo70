htmlEditor.manualSelection = false;

htmlEditor.getAllVersions = function(version_id) {
    var versionData = "";
    if (version_id) {
        versionData = {"version_id": version_id};//input version id
        jQuery(".loading-g").show();
    } else {
        versionData = {"remove_first_version": true};//input version id
    }
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.getAllVersionsAPI,
        async: true,
        method: 'GET',
        data: versionData,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data) {
            if (data.status == '200') {
                if (version_id) {
                    htmlEditor.setversioncontent(data.data);
                }
                else {
                    htmlEditor.createVersionList(data.data.all_versions, 0);         //Pass the index of version to be selected in dropdown
                }
            } else {
                //alert("LeftPane not generated");
            }
        },
        error: function() {
            //alert("LeftPane not generated");
        }
    });
};

htmlEditor.createVersionList = function(versions, selected_version) {
    var str = "";
    for (var i = 0; i < versions.length; i++) {
//        str += '<div class="review-value" version_id= ' + versions[i].version_id + '><div class="rv-t-1">Version ' + (versions.length - i) + '</div><div class="rv-t-2">' + versions[i].created_at + '</div><div class="rv-t-2"> ' + versions[i].username + '</div><div class="clearfix"></div></div>';
        str += '<div class="panel panel-default"><div class="panel-heading" role="tab" id="heading' + i + '"><a role="button" data-toggle="collapse" data-parent="#verAccordion" href="#collapse' + i + '" aria-expanded="true" aria-controls="collapse' + i + '" class="collapsed version_num" version_id= ' + versions[i].version_id + '><strong>Version ' + (versions.length - i) + '</strong><span class="pull-right"><i class="fa fa-plus-square"></i></span><div class="clearfix"></div><time>' + versions[i].created_at + '</time><span class="pull-right">' + versions[i].username + '</span><div class="clearfix"></div></a></div><div id="collapse' + i + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading' + i + '"><div class="panel-body"><ul>';
        var comments = versions[i].comments;
        try {
            for (var j = 0; j < comments.length; j++) {
                if (jQuery.trim(comments[j].text).length == 0) {
                    str += '<li><b style="color: #880e4f;">No comments</em></b>';
                }
                else {
                    str += '<li>' + comments[j].text + '</li>';
                }

            }
        }
        catch (exc) {
            str += '<li><b style="color: #880e4f;">No comments</em></b>';
        }
        str += '</ul></div></div></div>';
    }
    jQuery("#verAccordion").html(str);
    if (versions.length > 1) {
        jQuery("#compare_toc_versions").attr("disabled", false);
    }
    htmlEditor.bindVersionEvent();
}

htmlEditor.setversioncontent = function(version_cont) {
    var version_content = version_cont.version_detail[0].content;
    var curr_ver = version_cont.version_detail[0].version_id;
    //var stripped_content = htmlEditor.stripData(version_content);
    var other_versions = version_cont.all_versions;
    var ver_index;
    jQuery.each(other_versions, function(k, v) {
        if (this.version_id === curr_ver) {
            ver_index = k;
            return false;
        }
    })
    var stripped_content = version_content;
    htmlEditor.setData(stripped_content);
    htmlEditor.createVersionList(other_versions, ver_index);
    htmlEditor.bindEvents();
}

htmlEditor.bindVersionEvent = function() {
    jQuery(".version-dropdown").find(".dropdown-menu").find(".review-value").off().on("click", function() {
        var version = jQuery(this).find(".rv-t-1").text();
        var version_id = jQuery(this).attr("version_id");
        htmlEditor.getAllVersions(version_id);
        if (jQuery(".lm-edit-more").hasClass("active")) {           // Expand the editor size and disable toggler button.
            jQuery(".lm-edit-more").trigger("click");
        }
        jQuery("#versionDropdown").find(".rv-t-1").text(version).attr("version_id", version_id);
        var latest_version = jQuery(".version-dropdown").find(".dropdown-menu").find(".review-value").eq(0).attr("version_id");
        htmlEditor.manualSelection = true;
//        if (latest_version === version_id ) {
//            htmlEditor.createVersion = true;
//        }
//        else {
//            htmlEditor.createVersion = false;
//        }
    })
}

htmlEditor.bindCompareEvent = function(theIframe) {
    var frame = theIframe.contents()[0];
    function removeOptions() {
        jQuery(frame).find(".keep-or-not").remove();
    }
    jQuery(frame).find("body").off("mouseenter").on("mouseenter", "ins,del", function() {
        var thisElm = jQuery(this);
        var thisOffset = thisElm.offset();
        var thisHt = thisElm.height();
        var thisWidth = thisElm.width();
        var checker = '<div class="keep-or-not" style="top:' + (thisOffset.top + thisHt / 2) + '; left:' + (thisOffset.left + thisWidth / 2) + ';"><button class="fa fa-check keep"></button><button class="fa fa-times dont-keep"></button></div>';
        removeOptions();
        jQuery(frame).find("body").append(checker);

        jQuery(frame).find("body").find(".keep").off("click").on("click", function() {
            var content = thisElm.html();
            thisElm.replaceWith(content);
            removeOptions();
        });

        jQuery(frame).find("body").find(".dont-keep").off("click").on("click", function() {
            thisElm.remove();
            removeOptions();
        });
    });
    //Disable clicks on compare versions
    jQuery(frame).find('body a').on('click', function(e) {
        e.preventDefault();
        swal("Links do not function when comparing versions.");
    });
    jQuery(frame).find("body").on("keydown", function(e) {
        if (e.which === 27) {           //Escape key pressed.
            removeOptions();
        }
    });

//        jQuery(frame).find("body").off("mouseleave").on("mouseleave", "ins,del",function(){
//            removeOptions();
//        });
}


//Function when clicking to compare versions.
htmlEditor.compareVersions = function() {
    var versionList = jQuery("#verAccordion").find(".panel.panel-default");
    var versionHolder = {};
    var str = "";
    var totalVersions = versionList.length;
    versionList.each(function(i, v) {
        var thisVerId = jQuery(this).find("a.version_num").attr("version_id");
        versionHolder[i + 1] = thisVerId;
        str += '<option value=' + thisVerId + '> Version ' + (totalVersions - i) + '</option>';
    })

    jQuery("#compareDropdown1, #compareDropdown2").html(str);
    var prevVer = jQuery("#compareDropdown2").find("option").eq(1).val(); // Set value of version on one frame to butcurrent.
    jQuery("#compareDropdown2").val(prevVer);
    jQuery("#merged-view").prop("checked", false);
    var latestVer = jQuery("#heading0").find(".version_num").attr("version_id");
    var butLatestVer = jQuery("#heading1").find(".version_num").attr("version_id");
    var view = 2;
    htmlEditor.getComparativeResult(butLatestVer, latestVer, view);
    jQuery("#changeView").val(view);     //Select merged view by default.
    jQuery(".compare-frame").find("iframe").contents().find("html").html("");        //Clear the previous comparison results.
    jQuery("#updateNsave").attr("disabled", true);                 //disable the save button. enable after data is fetched
    jQuery("#compare-version").modal("show");
    jQuery("#compare_btn").button('loading');           //Show loading text on the compare button
    jQuery("#updateNsave").off().on("click", function() {
        var content = jQuery("#compare-version").find("iframe").contents();
        if (content.find("ins").length || content.find("del").length) {
            swal("Please complete the merge process or choose automerge to let system merge for you.")
        }
        else {
            htmlEditor.autoMerge = false;
            var finalContent = content.find("body").html();
            var curr_ver = jQuery("#versionDropdown").find(".curr-ver").attr("version_id");
            //htmlEditor.setData(finalContent);
//            htmlEditor.webWorker(1, true, curr_ver); //Always create version while saving after version comparison
//            jQuery("#saveModal").modal("hide");
            htmlEditor.createVersion = true;
            jQuery("#compare-version").modal("hide");
            jQuery("#save_modal").trigger("click");     //trigger the modal to save the notes alongwth the content.
        }
    })
}

htmlEditor.getComparativeResult = function(ver1, ver2, type) {
    var ms_start = (new Date()).getTime();

    var dataToBePosted = {
        file1_versionid: ver1,
        file2_versionid: ver2,
        object_id: node_id,
        project_id: project_id,
        display_option: type
    }
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.compareVersions,
        type: 'POST',
        data: dataToBePosted,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        async: true,
        success: function(data, textStatus, jqXHR) {
            var htmlContent = data.data.replace(/..\/images\/default-img.png/g, '../images/default-img.png');
            htmlContent = htmlContent.replace(/..\/images\/default-video.jpg/g, '../images/default-video.jpg');
            var compareRes = htmlContent;
            var theme_location = data.theme_location;

            var ms_end = (new Date()).getTime();
            var totaltime = (ms_end - ms_start) / 1000 + 's';
            showResult(compareRes, theme_location, totaltime);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal("Could not fetch the Comparision Results.");
        }
    });

    function showResult(data, theme_location, totaltime) {
        jQuery(".compare-frame").find('iframe').attr('id', 'cmp-frame');
        var compareFrame = jQuery(".compare-frame").find("iframe").contents();
        htmlEditor.includeCSS(jQuery(".compare-frame").find("iframe"));
        if (!compareFrame.find("head").find("style").length) {                  //Check if style is already added.
            compareFrame.find('head').append('<style type="text/css"> body{word-break: break-all;} .diff td{ /*padding:0 0.667em; vertical-align:top; white-space:pre; white-space:pre-wrap; font-family:Consolas,"Courier New",Courier,monospace; font-size:0.75em; line-height:1.333; */ border: 0px;}  .diff span{ /*display:block; min-height:1.333em; margin-top:-1px; padding:0 3px; */}   html .diff span{ height:1.333em; }  .diff img{ height:auto; max-width: 300px !important; }  .diff span:first-child{ margin-top:0; }  .diffDeleted , del p, .diffdel, del.diffmod{ /*border:1px solid rgb(255,192,192);*/position: relative; background:rgb(255,224,224); }  .diffInserted , ins p, .diffins, ins.diffmod{ /*border:1px solid rgb(192,255,192);*/ position: relative;background:rgb(224,255,224); }  .diffUnmodified  { /*border:1px solid rgb(192,192,255);*/ /*background:rgb(224,224,255);*/ }  #toStringOutput{ margin:0 2em 2em; }  #outputDiv {width:100%;padding: 0;} #outputDiv table.diff tr{background: transparent none repeat scroll 0 0;} .keep-or-not{  position: absolute;  text-align: center;  top: 50%;  left: 50%;  height: 18px;  width: 40px;  margin-left: -25px;  border-radius: 3px;  margin-top: -10px;  background-color: white;} .keep-or-not button, .keep-or-not button:hover{border:0;font-size: 10px;  padding: 0px;  margin: 2px;  width: 14px;  height: 14px; display: table-cell;  vertical-align: middle;  text-align: center;color: white;} .keep-or-not .keep{background-color: green;} .keep-or-not .dont-keep{ background-color: red;} table.diff{ border:0; margin-bottom:0 !important;}</style>');
        }
        compareFrame.find('head').append('<link rel="stylesheet" href="' + theme_location[0] + '"/> <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400,600" rel="stylesheet" type="text/css">');
        try {
            compareFrame.find("body").html(data);
            htmlEditor.bindCompareEvent(jQuery(".compare-frame").find("iframe"));
        }
        catch (exc) {

        }

        jQuery("#compare_btn").button('reset');

        var type = jQuery("#changeView").val();
        if (type === "2") {
            jQuery("#updateNsave").attr("disabled", false);
        }
        jQuery("#changeView").attr("disabled", false);
//        jQuery("#compareTime").html('Time=' + totaltime);

    }

    function bytesToSize(bytes) {
        var sizes = ['bytes', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
        if (bytes == 0)
            return 'n/a';
        var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
        if (i == 0) {
            return (bytes / Math.pow(1024, i)) + ' ' + sizes[i];
        }
        return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];
    }
    ;
}

htmlEditor.mergeWithNewVersion = function() {
    jQuery("#keepMine, #keepTheirs").attr("disabled", false);
    var dataToBePosted = {"object_id": node_id, "project_id": project_id, "content": htmlEditor.getContent()};
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.compareContentToBeMerged,
        type: 'POST',
        data: dataToBePosted,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        async: true,
        success: function(data, textStatus, jqXHR) {
            var htmlContent = data.data.replace(/..\/images\/default-img.png/g, '../images/default-img.png');
            htmlContent = htmlContent.replace(/..\/images\/default-video.jpg/g, '../images/default-video.jpg');
            var compareRes = htmlContent;
            var theme_location = data.theme_location;
            showRunTimeComaprison(compareRes, theme_location);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            swal("Error Fetching Merged Content");
        }
    });

    function showRunTimeComaprison(result, theme) {
        var compareFrame = jQuery("#compareLatestContent").find("iframe").contents();
        htmlEditor.includeCSS(jQuery("#compareLatestContent").find("iframe"));
        if (!compareFrame.find("head").find("style").length) {                  //Check if style is already added.
            compareFrame.find('head').append('<style type="text/css">  .diff td{ /*padding:0 0.667em; vertical-align:top; white-space:pre; white-space:pre-wrap; font-family:Consolas,"Courier New",Courier,monospace; font-size:0.75em; line-height:1.333; */ border: 0px;}  .diff span{ /*display:block; min-height:1.333em; margin-top:-1px; padding:0 3px; */}   html .diff span{ height:1.333em; }  .diff img{ height:auto; max-width: 300px !important; }  .diff span:first-child{ margin-top:0; }  .diffDeleted , del p, .diffdel, del.diffmod{ /*border:1px solid rgb(255,192,192);*/position: relative; background:rgb(255,224,224); }  .diffInserted , ins p, .diffins, ins.diffmod{ /*border:1px solid rgb(192,255,192);*/ position: relative;background:rgb(224,255,224); }  .diffUnmodified  { /*border:1px solid rgb(192,192,255);*/ /*background:rgb(224,224,255);*/ }  #toStringOutput{ margin:0 2em 2em; }  #outputDiv {width:100%;padding: 0;} #outputDiv table.diff tr{background: transparent none repeat scroll 0 0;} .keep-or-not{  position: absolute;  text-align: center;  top: 50%;  left: 50%;  height: 18px;  width: 40px;  margin-left: -25px;  border-radius: 3px;  margin-top: -10px;  background-color: white;} .keep-or-not button, .keep-or-not button:hover{border:0;font-size: 10px;  padding: 0px;  margin: 2px;  width: 14px;  height: 14px; display: table-cell;  vertical-align: middle;  text-align: center;color: white;} .keep-or-not .keep{background-color: green;} .keep-or-not .dont-keep{ background-color: red;} table.diff{ border:0; margin-bottom:0 !important;}</style><script src="js/lm_frost_listner_compare.js"></script>');
        }
        compareFrame.find('head').append('<link rel="stylesheet" href="' + theme[0] + '"/> <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:400,600" rel="stylesheet" type="text/css">');
        compareFrame.find("body").html(result);
        jQuery("#compareLatestContent").modal("show");
        htmlEditor.bindCompareEvent(jQuery("#compareLatestContent").find("iframe"));
    }
    var content = jQuery("#compareLatestContent").find("iframe").contents();
    jQuery("#compareNSave").off().on("click", function() {
        if (content.find("ins").length || content.find("del").length) {
            swal("Please complete the merge process or choose automerge to let system merge for you.")
        }
        else {
            htmlEditor.autoMerge = false;
            var finalContent = content.find("body").html();
            var curr_ver = jQuery("#versionDropdown").find(".curr-ver").attr("version_id");
            //htmlEditor.setData(finalContent);
            htmlEditor.webWorker(1, true, curr_ver);
            jQuery("#saveModal").modal("hide");
            jQuery("#compareLatestContent").modal("hide");
        }
    });

    jQuery("#keepTheirs").off().on("click", function() {
        content.find("body").find("ins").remove();                  //remove all "mine" changes
        content.find(".keep-or-not").remove();                      //remove any hovered option tools.
        content.find("body").find("del").each(function() {
            var orig = jQuery(this).html();
            jQuery(this).replaceWith(orig);
        });
        jQuery("#keepMine, #keepTheirs").attr("disabled", true);
    })

    jQuery("#keepMine").off().on("click", function() {
        content.find("body").find("del").remove();              //remove all the "theirs" changes
        content.find(".keep-or-not").remove();                   //remove any hovered option tools.
        content.find("body").find("ins").each(function() {
            var orig = jQuery(this).html();
            jQuery(this).replaceWith(orig);
        })
        jQuery("#keepMine, #keepTheirs").attr("disabled", true);
    })
}