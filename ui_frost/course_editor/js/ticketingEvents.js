var ticketNum = "",
    attachImage;
var globobj = {};
var selectedElem;

htmlEditor.userProjectPermission = {};

htmlEditor.currentUserPermissions = function() {
    return jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.projectDetailWithPermissionsAPI,
        async: true,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function(data, textStatus, jqXHR) {
            htmlEditor.userProjectPermission = data.data.project_permissions; // Save permission object locally.

            if (htmlEditor.userProjectPermission['cfi.create'].grant && !htmlEditor.userProjectPermission['cfi.show.all'].grant) {
                swal('cfi view permission enabled automaically');
            }
            if (!htmlEditor.userProjectPermission['cfi.create'].grant && !htmlEditor.userProjectPermission['cfi.show.all'].grant) {
                jQuery('.cfi_el').addClass('hideElm');
                jQuery('.items').each(function() {
                    jQuery(this).removeAttr('data-content');
                });
            }
            if (!htmlEditor.userProjectPermission['cfi.create'].grant && htmlEditor.userProjectPermission['cfi.show.all'].grant) {

                jQuery('.cfi_el').addClass('hideElm');
            }

            htmlEditor.cssFiles = data.data.theme_location;
        }
    })
}


htmlEditor.bindTicketEvents = function() {
    htmlEditor.currentUserPermissions();

    // Function to create the text-box to input comments and various values.
    function createCommentBox(type) {
        if (type === "issue") {
            var _HTML = '<div class="inputComment create-issue"><div class="comment-form active"><p class="author-name">You</p><textarea class="comment-box right-of-avatar" placeholder="Add an Issue..."></textarea><div class="clearfix"></div><div class="actions"><select class="readytotest">';
        } else if (type === "comment") {
            var _HTML = '<div class="inputComment create-issue"><div class="comment-form active"><p class="author-name">You</p><textarea class="comment-box right-of-avatar" placeholder="Add a comment..."></textarea><div class="clearfix"></div><div class="actions"><select class="readytotest">';
        }
        jQuery.each(htmlEditor.infoObj.statusList, function(k, v) {
            _HTML += '<option value="' + k + '">' + v + '</option>';
        });
        _HTML += '</select><select class="readytotest2">';
        for (var i = 0; i < htmlEditor.infoObj.userList.length; i++) {
            _HTML += '<option value="' + htmlEditor.infoObj.userList[i].user_id + '">' + htmlEditor.infoObj.userList[i].created_by_name + '</option>';
        }
        _HTML += '</select><select class="readytotest1">';
        jQuery.each(htmlEditor.infoObj.statusTypes, function(k, v) {
            var selectedStatus = '';
            if (k == 'MEDIUM') {
                selectedStatus = 'selected="selected"';
            }
            _HTML += '<option value="' + k + '" ' + selectedStatus + ' >' + v + '</option>';
        });
        if (type === "issue") {
            _HTML += '</select><div class="clearfix"></div></div><div class="submit-action actions"><input type="file" name="ticket_attachment" id="ticket_attachment"/><a href="#" class="action-link post active" draggable="false">Submit</a><a href="#" class="action-link cancel" draggable="false">Cancel</a><div class="clearfix"></div></div></div></div>';
        } else if (type === "comment") {
            _HTML += '</select><div class="clearfix"></div></div><div class="submit-action actions"><a href="#" class="action-link post active" draggable="false">Submit</a><a href="#" class="action-link cancel" draggable="false">Cancel</a><div class="clearfix"></div></div></div></div>';
        }

        return _HTML;
    }
    //setTimeout(function(){  jQuery("button.create").attr("disabled", true); }, 30);
    //jQuery("button.create").attr("disabled", true);
    // Functionality on clicking on the Add Comment button, to create a new ticket.
    jQuery(".post-comment .add-new-ticket").find(".create").off().on("click", function() {
        if (htmlEditor.userProjectPermission["ticket.create"]["grant"]) {
            //        var editorFrame= document.getElementById("editor-frame");
            //        var idoc= editorFrame.contentDocument || editorFrame.contentWindow.document;
            //        var getType = idoc.getSelection();
            var selection = htmlEditor.getEditorSelection();
            selectedElem = selection.getStartElement().$;
            /*if (selectedElem.hasAttribute("ticket-id")) {
             swal("Ticket already exists against this Element.");
             }
             else*/
            if (jQuery(selectedElem).parents(".cke_editable").length) {

                var execute = true;
                execute = htmlEditor.checkForVidFrame(selectedElem);
                if (execute) {
                    //                if(jQuery(selectedElem).parents("math").length){
                    //                    selectedElem= jQuery(selectedElem).parents("math")[0]; //Create ticket against math,and not its subset.
                    //                }
                    var textBox = createCommentBox("issue");
                    jQuery(this).attr("disabled", true);
                    var url_to_create_ticket = htmlEditor.PXEmappedAPI.createTicketAPI;
                    jQuery(this).parents(".add-new-ticket").find(".new-ticket-container").html(textBox);
                    htmlEditor.setCommentPosting(url_to_create_ticket, selectedElem);
                    htmlEditor.cancelEvent();
                } else {
                    swal("Cannot create issues against Video/ Gadgets/ Widgets");
                }
            } else {
                swal("Issues can be created only against elements within Chapter");
            }
        } else {
            swal("You are not permitted to create issues. Please contact Admin.");
        }
    });

    htmlEditor.prevDetails = {};
    //Add an identifier to the active tab in the accordion.
    jQuery(".post-comment").find(".comments-wrapper").find("#accordionTicket").find("a").off().on("click", function() {
        //ticketNum = "";
        jQuery(this).parents("#accordionTicket").find("a").removeClass("activatedTab");
        jQuery(".action-link.cancel").trigger("click"); // Close all the textareas.
        jQuery("button.create").attr("disabled", true); // Disable the create new ticket button.

        if (jQuery(this).hasClass("collapsed")) {
            ticketNum = jQuery(this).data("ticket-id");
            jQuery(this).addClass("activatedTab");
            //Functionality on clicking on the Add comment button to comment on an existing ticket.
            jQuery(this).parents(".panel").find(".panel-body").find(".add-comment").off().on("click", function() {
                if (htmlEditor.userProjectPermission["ticket.comment.create"]["grant"]) {
                    var textBox = createCommentBox("comment");
                    var selected_issue = jQuery(this);
                    htmlEditor.prevDetails.prev_user = selected_issue.parents(".panel").find(".panel-heading").find(".assignee").attr("user-id");
                    selected_issue.attr("disabled", true);
                    htmlEditor.prevDetails.prev_type = selected_issue.siblings("ul.issueCreated").find(".action-link.labl").text();
                    htmlEditor.prevDetails.prev_status = selected_issue.siblings("ul.issueCreated").find(".action-link.status").text();
                    var url_to_create_comment = htmlEditor.PXEmappedAPI.createCommentAPI;
                    selected_issue.parents(".panel-body").find(".new-ticket-container").html(textBox).find(".readytotest2").val(htmlEditor.prevDetails.prev_user); //Keep the current assignee name selected in the dropDown.
                    selected_issue.parents(".panel-body").find(".new-ticket-container").find(".readytotest1").val(htmlEditor.prevDetails.prev_type);
                    htmlEditor.setCommentPosting(url_to_create_comment);
                    htmlEditor.cancelEvent();
                } else {
                    swal("You are not permitted to create comments against issues. Please contact Admin.");
                }
            });
        }

        if (!jQuery(".activatedTab").length) { //Check if all the tabs are collapsed.
            jQuery("button.create").attr("disabled", false);
            jQuery(".cke_wysiwyg_frame").contents().find("body").find(".indicator").removeClass("indicator");
        }
        jQuery(this).closest('.panel').find('.panel-heading:eq(0) a').addClass('activatedTab'); //to keep activatedTab class for the current ticket
        if (ticketNum) {
            highlightElement(ticketNum);
        }

        function highlightElement(ticket) {
            var contentIn = jQuery(".cke_wysiwyg_frame").contents().find("body");
            contentIn.find(".indicator").removeClass("indicator");
            var allTicketsObject = contentIn.find("[ticket-id]");
            var index;
            for (var k = 0; k < allTicketsObject.length; k++) {
                var individualTickets = jQuery(allTicketsObject[k]).attr("ticket-id").split(',');
                for (var p = 0; p < individualTickets.length; p++) {
                    if (parseInt(individualTickets[p]) == parseInt(ticket)) {
                        index = k;
                        /*var ticketAgainst = jQuery(allTicketsObject[k]);
                        if (ticketAgainst.length) {
                            ticketAgainst.addClass("indicator");
                            var tobescrolled = ticketAgainst.offset().top - 20;
                            contentIn.animate({scrollTop: tobescrolled}, 1000);
                        } else {
                            swal("Element against the issue was removed.");
                        }*/
                    }
                }
            }
            if (index === undefined) {
                swal("Element against the issue was removed.");
            } else {
                var ticketAgainst = jQuery(allTicketsObject[index]);
                ticketAgainst.addClass("indicator");
                var tobescrolled = ticketAgainst.offset().top - 20;
                contentIn.animate({ scrollTop: tobescrolled }, 1000);
            }
        }
    });
    // Functionality on delete a ticket/comment.
    jQuery('.action-link.delete').on('click', ticketDeleteConfirm);

    function ticketDeleteConfirm() {
        var that = jQuery(this);
        swal({
            title: "Want to delete the ticket?",
            text: "This ticket and related comments will be deleted.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: false
        }, function(isConfirm) {
            if (isConfirm) {
                ticketDeleteEvent(that);
            } else {

            }
        });
    }

    function ticketDeleteEvent(el) {
        var contentIn = jQuery(".cke_wysiwyg_frame").contents().find("body");
        var allTicketsObject = contentIn.find("[ticket-id]");
        for (var k = 0; k < allTicketsObject.length; k++) {
            var individualTickets = jQuery(allTicketsObject[k]).attr("ticket-id").split(',');
            for (var p = 0; p < individualTickets.length; p++) {
                if (parseInt(individualTickets[p]) == parseInt(ticketNum)) {
                    individualTickets.splice(p, 1);
                    if (individualTickets.length == 0) {
                        jQuery(allTicketsObject[k]).removeAttr("ticket-id");
                    } else {
                        jQuery(allTicketsObject[k]).attr("ticket-id", individualTickets.toString());
                    }
                }
            }
        }
        var remEl = el.parents('.panel.panel-default'),
            editorEl = jQuery(".cke_wysiwyg_frame").contents().find("[ticket-id='" + ticketNum + "']"),
            ver_id = jQuery("#verAccordion").find(".version_num").eq(0).attr("version_id"),
            parentTicketId = remEl.find('.activatedTab').data('parent-ticket-id');

        var ticketDeleteAPI = serviceEndPoint + 'api/v1/ticket/ticket_id/' + parentTicketId + '/project_id/' + project_id + '/object_id/' + node_id + '?access_token=' + access_token + '&_method=DELETE';
        jQuery.ajax({
            url: ticketDeleteAPI,
            type: 'POST',
            async: true,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data, textStatus, jqXHR) {
                var isProjectPermissionGranted = data.status == '200' ? true : (data.status == '400' && data.projectPermissionGranted == '401') ? false : true;
                if (isProjectPermissionGranted) {
                    var remElParent = remEl.parent();
                    remEl.remove();
                    if (remElParent.children().length == '0') {
                        remElParent.html('<div class="panel-group" id="accordionTicket" role="tablist" aria-multiselectable="true"><div class="lm-rochak"><div><span class="fa fa-comments"></span></div><strong>No Comments added Yet!</strong>Click on the button above to add new comment</div></div>');
                    }
                    editorEl.removeAttr('ticket-id');
                    jQuery('.add-new-ticket').find('.btn.btn-default.create.new-action').removeAttr('disabled');
                    var issueCountElm = jQuery('.numbers.issue-count'),
                        currentIssueCount = parseInt(issueCountElm.html()) - 1;
                    if (currentIssueCount > 0) {
                        issueCountElm.html(currentIssueCount);
                    } else {
                        issueCountElm.css('display', 'none');
                    }
                    htmlEditor.webWorker(0, false, ver_id); // No param for creating version.
                } else {
                    swal(data.message);
                }

            },
            error: function(err) {
                swal("Something went wrong, unable to delete ticket.");
            }
        });
    }
    jQuery('.action-link.del').on('click', commentDeleteConfirm);

    function commentDeleteConfirm() {
        var that = jQuery(this);
        swal({
            text: " Want to Delete the comment?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true,
            allowOutsideClick: false
        }, function(isConfirm) {
            if (isConfirm) {
                commentDeleteEvent(that);
            } else {

            }
        });
    }

    function commentDeleteEvent(el) {
        var remEl = el.parent();
        var commentID = el.parent().attr('data-comment-id'),
            moveToParent = el.parents('.panel.panel-default'),
            parentTicketId = moveToParent.find('.activatedTab').data('parent-ticket-id');

        commentDeleteAPI = serviceEndPoint + 'api/v1/ticket-comment/ticket_coment_id/' + commentID + '/ticket_id/' + parentTicketId + '/project_id/' + project_id + '/object_id/' + node_id + '?access_token=' + access_token + '&_method=DELETE';

        jQuery.ajax({
            url: commentDeleteAPI,
            type: 'POST',
            async: true,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function(data, textStatus, jqXHR) {
                var isProjectPermissionGranted = data.status == '200' ? true : (data.status == '400' && data.projectPermissionGranted == '401') ? false : true;
                if (isProjectPermissionGranted) {
                    var remElParent = remEl.parent();
                    remEl.remove();
                    if (remElParent.children().length == '0') {
                        remElParent.html('<li><em> No comments yet</em></li>');
                    }
                } else {
                    swal(data.message);
                }
            },
            error: function(err) {
                swal("Something went wrong, unable to delete ticket.");
            }
        });
    }
};

// Function to check if the element on which the issue is being created is a video / frame / some converted frame.
htmlEditor.checkForVidFrame = function(theElement) {
    return checking();

    function checking() {
        var proceed = true;
        jQuery(theElement).parents().each(function() { //Check if the target is a child of some converted frame.
            if ((this).hasAttribute("type") || jQuery(this).hasClass("converted")) {
                proceed = false;
                return false;
            }
        });

        if (jQuery(theElement).hasClass("converted") || theElement.hasAttribute("type")) { //Check if the target itself is a converted div.
            proceed = false;
        }
        return proceed;
    }
};

htmlEditor.cancelEvent = function() {
    jQuery(".action-link.cancel").off().on("click", function() {
        jQuery(this).parents(".new-ticket-container").empty().siblings("button.new-action").attr("disabled", false);
    })
}

htmlEditor.setCommentPosting = function(url, theElem) {
    jQuery(".action-link.post").off().on("click", function() {
        var flag = 0; // To check if the post is comment or new ticket.
        var containerBox = jQuery(this).parents(".inputComment");
        var issue_desc = containerBox.find("textarea.comment-box").val().trim();
        var commentTag = (issue_desc).replace(/</g, '&lt;').replace(/>/g, '&gt;');
        var assignee = containerBox.find("select.readytotest2:visible").val();
        var issue_type = containerBox.find("select.readytotest1:visible").val();
        var issue_status = containerBox.find("select.readytotest:visible").val();
        var parent_id = "";
        issue_type === undefined ? "NOCHANGE" : issue_type;
        var sendData = {};
        var username = htmlEditor.getUserName(assignee);
        var loadingBtn = "";
        if (containerBox.parents(".panel").length) { // If post is Hit from inside an accordion.
            if (htmlEditor.prevDetails.prev_status === issue_status && htmlEditor.prevDetails.prev_user === assignee && htmlEditor.prevDetails.prev_type === issue_type && !issue_desc.length) {
                swal("You have not changed anything. The comment cannot be posted.");
                return false;
            } else {
                if (!issue_desc.length) {
                    issue_desc = "CHANGED STATUS/ TYPE/ ASSIGNEE";
                }
                parent_id = jQuery("a.activatedTab").data("parent-ticket-id"); //Get the ticket id.
                /*sendData = {"project_id": project_id, "object_id": node_id, "comment": issue_desc, "assigned_to": assignee, "status": issue_status, "parent_ticket_id": parent_id, "type": issue_type};*/
                sendData = {};
                var formData = new FormData();
                formData.append("project_id", project_id);
                formData.append("object_id", node_id);
                formData.append("comment", commentTag);
                formData.append("assigned_to", assignee);
                formData.append("status", issue_status);
                formData.append("parent_ticket_id", parent_id);
                formData.append("type", issue_type);
                sendData = formData;
                jQuery(this).parents(".new-ticket-container").siblings("button.new-action").attr("disabled", false);
                loadingBtn = "button.add-comment";
                //            jQuery("button.add-comment").button('loading');
            }
        } else {
            if (issue_desc.length) {
                loadingBtn = "button.create";
                //            jQuery("button.create").button('loading');
                flag = 1;
                /*sendData = {"project_id": project_id, "object_id": node_id, "title": "title", "issue_description": issue_desc, "assigned_to": assignee, "status": issue_status, "type": issue_type};*/
                sendData = {};
                var formData = new FormData();
                formData.append("project_id", project_id);
                formData.append("object_id", node_id);
                formData.append("title", "title");
                formData.append("issue_description", commentTag);
                formData.append("assigned_to", assignee);
                formData.append("status", issue_status);
                formData.append("type", issue_type);
                var file_data = jQuery("#ticket_attachment").prop("files")[0];
                formData.append("ticket_attachment", file_data);
                sendData = formData;
            } else {
                swal("Issue cannot be empty. Please add some text.");
                return false;
            }
        }
        var overlayOnIssues = '<div class="overl" style="height: 100%;width: 100%;background:transparent; position: absolute;top: 135px;"></div>';
        jQuery("#issues").find(".post-comment").append(overlayOnIssues);
        //        if (issue_desc.length) {
        jQuery(loadingBtn).button('loading');


        jQuery.ajax({
            url: url,
            type: 'POST',
            data: sendData,
            async: true,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            processData: false,
            contentType: false,
            success: function(data, textStatus, jqXHR) {
                //console.log(data)
                var response = data.data[0];
                var isProjectPermissionGranted = data.status == '200' ? true : (data.status == '400' && data.projectPermissionGranted == '401') ? false : true;
                try {
                    attachImage = response.attachment[0].path;
                } catch (exc) {

                    attachImage = false;
                }

                var ticketList = jQuery(".right-comment").find(".comments-wrapper").find("#accordionTicket");
                if (isProjectPermissionGranted) {
                    var date;
                    if (flag) { // If a new ticket is raised.

                        var ticket_display_id = response.ticket_display_id;
                        var parent_ticket_id = response.ticket_id;
                        var lengthoftickets = ticketList.find(".panel").length;
                        var ver_id = jQuery("#verAccordion").find(".version_num").eq(0).attr("version_id"); //get latest version from the list.
                        var dateResponseString = response.created_at.toString();
                        date = (jQuery.format.date(dateResponseString, "MM/dd/yyyy HH:mm"));
                        var newTicket = '<div class="panel panel-default"><div class="panel-heading" role="tab" id="ticket' + lengthoftickets + '"><div class="panel-title"><a data-ticket-id="' + ticket_display_id + '" data-parent-ticket-id="' + parent_ticket_id + '" class="collapsed" data-toggle="collapse" data-parent="#accordionTicket" href="#collapseTicket' + lengthoftickets + '" aria-expanded="true" aria-controls="collapseTicket' + lengthoftickets + '"><div class="issue-no">Issue# ' + ticket_display_id + '<p><b>Assigned to: </b><span class="assignee" user-id=' + response.assigned_to + '>' + response.assigned_to_username + '</span></p></div></a></div></div><div id="collapseTicket' + lengthoftickets + '" class="panel-collapse collapse" role="tabpanel" aria-labelledby="ticket' + lengthoftickets + '"><div class="panel-body"><ul class="comments issueCreated"><li data-comment-id=""><a class="author-name right-of-avatar" href="#">' + response.created_by_username + '</a><a class="author-date right-of-avatar" href="#">' + date + '</a><p class="comment right-of-avatar">' + response.issue_description + '</p><label class="action-link ' + response.type.toLowerCase() + ' labl">' + response.type + '</label><label class="action-link status">' + response.status + '</label><label class="action-link delete glyphicon glyphicon-trash" title="Delete"></label>';
                        if (attachImage) {
                            newTicket += '<a href="' + attachImage + '" target="_blank" class="ticketAttachImg glyphicon glyphicon-paperclip" title="View Attachment"></a></li></ul><button class="btn add-comment new-action">Leave a comment</button><div class="new-ticket-container"></div><div class="comments-on-ticket"><ul class="comments thread"><li><em> No comments yet</em></li></ul></div></div></div></div>';
                        } else {
                            newTicket += '</li></ul><button class="btn add-comment new-action">Leave a comment</button><div class="new-ticket-container"></div><div class="comments-on-ticket"><ul class="comments thread"><li><em> No comments yet</em></li></ul></div></div></div></div>';
                        }

                        if (ticketList.find(".panel").length) { //Check if comments already exists.
                            ticketList.prepend(newTicket);
                        } else {
                            ticketList.html(newTicket);
                        }
                        var arr_upperTool = ["STRONG", "EM", "SUB", "SUP", "U"];
                        var resultupper = arr_upperTool.indexOf(selectedElem.tagName);
                        if (selectedElem.classList.contains("keyword") || selectedElem.classList.contains("ulink") || selectedElem.classList.contains("pageref") || selectedElem.classList.contains("xref") || jQuery(selectedElem).children().hasClass("math-tex") || resultupper >= 0) {
                            var str = jQuery(theElem).parents().attr("id").toString();
                        } else {
                            var str = jQuery(theElem).attr("id").toString();
                        }
                        if (str in globobj) {
                            if (jQuery(selectedElem).children().hasClass("math-tex") || resultupper >= 0) {
                                globobj[jQuery(theElem).parent().attr("id")].push(ticket_display_id);
                            } else {
                                globobj[jQuery(theElem).attr("id")].push(ticket_display_id);
                            }

                        } else {
                            if (jQuery(selectedElem).children().hasClass("math-tex") || resultupper >= 0) {
                                globobj[jQuery(theElem).parent().attr("id")] = selectedElem.parentElement.hasAttribute("ticket-id") ? jQuery(theElem).parent().attr("ticket-id").split(",") : [];
                                globobj[jQuery(theElem).parent().attr("id")].push(ticket_display_id);
                            } else {
                                globobj[jQuery(theElem).attr("id")] = (theElem).hasAttribute("ticket-id") ? jQuery(theElem).attr("ticket-id").split(",") : [];
                                globobj[jQuery(theElem).attr("id")].push(ticket_display_id);
                            }

                        }
                        if (jQuery(selectedElem).children().hasClass("math-tex") || resultupper >= 0) {
                            var ticket_id_str = (globobj[jQuery(theElem).parent().attr("id")]).toString();
                            jQuery(theElem).parent().attr("ticket-id", ticket_id_str);
                        } else {
                            var ticket_id_str = (globobj[jQuery(theElem).attr("id")]).toString();
                            jQuery(theElem).attr("ticket-id", ticket_id_str);
                        }


                        jQuery("button.create").button('reset');

                        if (response.status) {
                            var issueCount = jQuery('#accordionTicket .panel.panel-default').length;
                            //var issueCount = parseInt(jQuery(".issue-count").eq(0).text());
                            //var newnum = issueCount + 1;
                            jQuery(".issue-count").text(issueCount).show();
                        }
                        htmlEditor.webWorker(0, false, ver_id); // No param for creating version.
                    } else { // If a Comment is posted.
                        var listedComments = ticketList.find(".panel-collapse.collapse.in").find(".comments.thread");
                        var dateResponseString1 = response.created_at.toString();
                        var date1 = (jQuery.format.date(dateResponseString1, "MM/dd/yyyy HH:mm"));
                        var commentTag = (response.comment).replace(/</g, '&lt;').replace(/>/g, '&gt;');
                        var newCom = '<li data-comment-id="' + response.ticket_comment_id + '"><a class="author-name right-of-avatar" href="#">' + response.created_by_username + '</a><a class="author-date right-of-avatar" href="#">' + date1 + '</a><p class="comment right-of-avatar">' + commentTag + '</p><label class="action-link del glyphicon glyphicon-trash" title="Delete"></label></li>';
                        if (listedComments.find("li").data("comment-id")) {
                            listedComments.prepend(newCom);
                        } else {
                            listedComments.html(newCom);
                        }
                        ticketList.find(".panel-collapse.collapse.in").find(".comments.issueCreated").find(".action-link.status").text(issue_status);
                        var prevClass = ticketList.find(".panel-collapse.collapse.in").find(".comments.issueCreated").find(".action-link.labl").text().toLowerCase();
                        if (issue_type === null) {
                            ticketList.find(".panel-collapse.collapse.in").find(".comments.issueCreated").find(".action-link.labl").removeClass(prevClass).text(issue_type);
                        } else {
                            ticketList.find(".panel-collapse.collapse.in").find(".comments.issueCreated").find(".action-link.labl").removeClass(prevClass).text(issue_type).addClass(issue_type.toLowerCase());
                        }

                        ticketList.find(".activatedTab").find(".assignee").attr("user-id", assignee).text(username);
                        if (issue_status == "CLOSED") {
                            var issueCount = parseInt(jQuery(".issue-count").eq(0).text());
                            var newnum = issueCount;
                            jQuery(".issue-count").text(newnum).show();
                        }
                        jQuery("button.add-comment").button("reset");
                    }
                    htmlEditor.bindTicketEvents();
                    jQuery("#issues").find(".post-comment").find(".overl").remove();
                } else {
                    swal(data.message);
                }
            },
            error: function() {
                jQuery("#issues").find(".post-comment").find(".overl").remove();
                alert("Unable to create ticket");
                jQuery("button.new-action").button("reset");
            }
        });
        containerBox.remove();
        //    }
        //        else {
        //            if (flag) {
        //                swal("Issue cannot be empty. Please add some text.")
        //            }
        //            else {
        //                swal("You have not changed anything. The comment cannot be posted.")
        //            }
        //        }
    });
};

htmlEditor.getUserName = function(user_id) {
    var user_name = "";
    for (var i = 0; i < htmlEditor.infoObj.userList.length; i++) {
        if (user_id == htmlEditor.infoObj.userList[i].user_id) {
            user_name = htmlEditor.infoObj.userList[i].username;
        }
    }
    return user_name;
}