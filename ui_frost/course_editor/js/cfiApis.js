// get all custom cfi
var getCustomCFI = function () {
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.customCfiAPI + '?access_token=' + access_token,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data) {
            if (data.data != null) {
                listingCfi = data.data.list;
                dropdown = '<div id="accordion">';
                listingCfi.forEach(function (item) {
                    dropdown += '<h3 class="cfiList" value=' + item.id + '>' + item.title + ' </h3><div><p id=' + item.id + '></p></div>';
                });
                dropdown += '</div>';
                populateData();
                 jQuery('.cfiList').eq(0).trigger('click');
            }

        }
    });
};
// get all custom cfi by id
var getCustomCFIById = function (cfiId) {
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.customCfiAPI + '/cfi_id/' + cfiId + '?access_token=' + access_token,
        async: true,
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data) {
            populateTreeWithCfi(data.data.cfi_element_ids, cfiId);
           
        }
    });
};
// add custom cfi
var addCustomCFI = function (cfi_data) {
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.customCfiAPI + '?access_token=' + access_token,
        async: true,
        method: 'POST',
        data: cfi_data,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data) {
            jQuery("#accordion").prepend('<h3 class="cfiList" value="' + data.data.cfi_id + '">' + cfi_data.title + '</h3><div><p   id="' + data.data.cfi_id + '"></p></div>');
            showAccordionList();
            swal("New CFI added");
             bindCFIEvents();
             jQuery('#accordion').accordion("refresh");
             if(jQuery('.cfiList ').eq(0).hasClass('ui-accordion-header-active')){
                 jQuery('.cfiList').eq(0).trigger('click');
             }
//             if(jQuery('.cfiList').length==1){
//                //jQuery('.cfiList').eq(0).trigger('click');
//            }
                          jQuery("#ui-accordion-accordion-panel-0").css('display','none');
                          
//             }
             
             
             
        },
        error: function () {
            alert("Error occured");
        }
    });
};
// edit custom cfi
var editCustomCFI = function (cfi_data, cfi_id) {
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.customCfiAPI + '/cfi_id/' + cfi_id + '?access_token=' + access_token,
        async: true,
        method: 'PUT',
        data: cfi_data,
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data) {
            swal("CFI updated");
        },
        error: function () {
            alert("Error occured");
        }
    });
};
// delete custom cfi
var deleteCustomCFI = function (cfi_id) {
    jQuery.ajax({
        url: htmlEditor.PXEmappedAPI.customCfiAPI + '/cfi_id/' + cfi_id + '?access_token=' + access_token,
        async: true,
        method: 'DELETE',
        xhrFields: {
            withCredentials: true
        },
        crossDomain: true,
        success: function (data) {
            swal("CFI deleted");
            jQuery("h3[class^='cfiList'][aria-selected='true']").remove('h3');
            jQuery("div[class^='ui-accordion-content'][style^='display: block;']").remove('div');
            if (!jQuery('.cfiList').length) {
                jQuery('#accordionCfi').show();
            }
            jQuery(".cke_wysiwyg_frame").contents().find('body').find('.cfiHighlighter').removeClass('cfiHighlighter');
        },
        error: function () {
            alert("Error occured");
        }
    });
};

