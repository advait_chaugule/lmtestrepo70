htmlEditor.bindProfileEvents = function () {
    jQuery('#myonoffswitch').off().on('click', function () {
        var that = jQuery(this);
        if (that.attr('on') == 'true') {
            that.attr('on', 'false');
            //OFF
            htmlEditor.enableProfile = false;
        }
        else {
            that.attr('on', 'true');
            //ON
            htmlEditor.enableProfile = true;
        }
    });

    jQuery('.profile-preview').off().on('click', function () {
        var that = jQuery(this);
        htmlEditor.previewType = that.attr('profile-type');
        htmlEditor.previewProfile();
    });
    
}
htmlEditor.setTestProfileState = function (indx) {
    var profileBtnRef = jQuery('.profile-preview');
    profileBtnRef.removeClass('active');
    profileBtnRef.eq(indx).trigger('click');
};

htmlEditor.setProfileSettingsForMathML = function (ref) {
    var mathMlRef = ref.find('.math-tex'),
            allMathMlChild = ref.find(mathMlRef).find('*'),
            isMathExist = mathMlRef.length;

    if (isMathExist) {
        var visibilityState = mathMlRef.css('visibility');

        allMathMlChild.each(function () {
            var that = jQuery(this);
            that.css('visibility', visibilityState);
        });
    }

};

htmlEditor.previewProfile = function () {
    //Added for making toggle button disabled in test mode
    jQuery(".switch").css({"pointer-events":"none","opacity":".5"});
    var frameEl = jQuery('#testCont').contents().find('body'),
            frameAllEl = frameEl.find('*');

    if (htmlEditor.previewType == 'print') {
        /*frameAllEl.css('visibility', 'hidden');
        frameEl.find('[data-profile-deliveryformat~="print"]').css('visibility', 'visible');*/
        /*frameAllEl.css('visibility', 'visible');
        frameEl.find('[data-profile-deliveryformat~="digital"]').css('visibility', 'hidden');*/
        frameAllEl.removeClass('hideElm');
        frameEl.find('[data-profile-deliveryformat~="digital"]').addClass('hideElm');
    }
    else if (htmlEditor.previewType == 'digital') {
        /*frameAllEl.css('visibility', 'hidden');
        frameEl.find('[data-profile-deliveryformat~="digital"]').css('visibility', 'visible');*/
        /*frameAllEl.css('visibility', 'visible');
        frameEl.find('[data-profile-deliveryformat~="print"]').css('visibility', 'hidden');*/
        frameAllEl.removeClass('hideElm');
        frameEl.find('[data-profile-deliveryformat~="print"]').addClass('hideElm');
    }
    else {
        /*frameAllEl.css('visibility', 'visible');*/
        frameAllEl.removeClass('hideElm');
    }
    htmlEditor.setProfileSettingsForMathML(frameEl);
}

htmlEditor.previewType = 'all';
htmlEditor.enableProfile = false;
htmlEditor.profileState = '';

htmlEditor.profileConfig = (function () {
    htmlEditor.profileNames = [], htmlEditor.subProfiles = [];
    for (var i = 0; i < profileList.length; i++) {
        var profilename = profileList[i].name,
                subprofiles = profileList[i].subProfile;
        htmlEditor.profileNames.push(profilename);
        htmlEditor.subProfiles.push(subprofiles);
    }
})();
htmlEditor.profileToggle = function (el, evt) {
    if (evt.target.getAttribute('class') != 'items' || !htmlEditor.enableProfile)
        return;
    htmlEditor.profileState = el.attr('data-content');
    var treeElm = el,
            posInProfArray = jQuery.inArray(htmlEditor.profileState, htmlEditor.profileNames),
            posToAssign = 0;

    if (posInProfArray != htmlEditor.profileState) {
        if (posInProfArray != (htmlEditor.profileNames.length - 1)) {
            posToAssign = Number(posInProfArray + 1);
        } else {
            posToAssign = 0;
        }

        htmlEditor.profileState = htmlEditor.profileNames[posToAssign];
    }

    var allChildElm = htmlEditor.currentEditorEl.find('*');
    if (htmlEditor.profileState.length) {
        htmlEditor.currentEditorEl.attr('data-profile-deliveryformat', htmlEditor.profileState);
        allChildElm.attr({'parent-added-profile': true, 'data-profile-deliveryformat': htmlEditor.profileState});
    } else {
        htmlEditor.currentEditorEl.removeAttr('data-profile-deliveryformat');
        allChildElm.each(function () {
            var that = jQuery(this),
                    isProfileAddedByParent = that.attr('parent-added-profile') != 'undefined' ? that.attr('parent-added-profile') : false;
            if (isProfileAddedByParent) {
                that.removeAttr('data-profile-deliveryformat').removeAttr('parent-added-profile');
            }
        });
    }
    var treeElmAllChild = treeElm.parent().find('.items');
    treeElm.attr('data-content', htmlEditor.profileNames[posToAssign]);
    treeElmAllChild.each(function () {
        jQuery(this).attr('data-content', htmlEditor.profileNames[posToAssign]);
    });

    htmlEditor.enableSave();
}


