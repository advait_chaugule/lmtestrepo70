 /******************************************************************/
 /** JS to generate and manipulate DOM Tree - Supriyo Chakroborty **/
 /** Dependency - jQuery.min.js && jQuery.ui.js **/
 /******************************************************************/

 var optCount = 0;

 //========================================================================
 //  Function that gets/sets contents of TOC tree
 //========================================================================
 function content() {
     var parseElm = {};
     return {
         setData: function(elm) {
             parseElm = elm;
         },
         getData: function(arg) {
             var stringToReturn = '<ul>';
             var elemId;
             parseElm.children().each(function(ind) {
                 var elmTagName = jQuery(this).tagName();
                 var elmClassName = jQuery(this).className();
                 elemId = jQuery(this).attr('id');
                 var hasChild = jQuery(this).children().length > 0;
                //  var elmText = jQuery(this).text().substr(0, 15) + '...';
                 var widgetType = jQuery(this).attr('data-widget-type') != undefined ? jQuery(this).attr('data-widget-type').toUpperCase() : false;
                 var patternType = jQuery(this).attr('pattern-type') != undefined ? jQuery(this).attr('pattern-type').toUpperCase() : false;
                 var profileType = jQuery(this).attr('data-profile-deliveryformat') != undefined ? jQuery(this).attr('data-profile-deliveryformat') : false;
                 var strAttribute = '';
                 optCount++;
                 jQuery(this).attr('data-pos', 'p' + optCount);
                 if (profileType) {
                     strAttribute = 'data-content=' + "";
                 }
                 if (hasChild) {
                    if(patternType){
                         stringToReturn += '<li class="closed patternChild"><span class="items" id="' + elemId + '" map=p' + optCount + ' ' + strAttribute + '>' + patternType + '<span class="expandToc" >+</span></span></li>';
                     }
                     else if(widgetType){
                         stringToReturn += '<li class="patternChild"><span class="items" id="' + elemId + '" map=p' + optCount + ' ' + strAttribute + '>' + widgetType + '</span><ul><li class="li_vis hidden"><p>######</p></li></ul></li>';
                     }
                      else {
                         stringToReturn += '<li class="closed patternChild"><span class="items" id="' + elemId + '" map=p' + optCount + ' ' + strAttribute + '>' + elmTagName + '<span class="expandToc" >+</span></span></li>';
                     }
                 } else {
                         stringToReturn += '<li class="patternChild"><span class="items" id="' + elemId + '" map=p' + optCount + ' ' + strAttribute + '>' + elmTagName + '</span><ul><li class="li_vis hidden"><p>######</p></li></ul></li>';
                 }
             });
             stringToReturn += '</ul>';
             return stringToReturn;
         },
         getDataOfDrop: function() {    
             var stringToReturn = '<ul>';
             var patternName=jQuery('.ui-draggable-dragging .tab-name').text().toUpperCase();
             parseElm.each(function(ind) {
                 var elmTagName = jQuery(this).tagName();
                //  if(elmTagName=='IFRAME'){
                //      patternName=jQuery(this).attr('widget-type').toUpperCase();
                //  }
                //  else{
                //      patternName=jQuery(this).attr('pattern-type').toUpperCase();
                //  }
                //  var elmClassName = jQuery(this).className().toUpperCase();
                 var elmId = jQuery(this).attr('id');
                 var hasChild = jQuery(this).children().length > 0;
                 optCount++;
                jQuery(this).attr('data-pos', 'p' + optCount);
                if (hasChild) {
                         stringToReturn += '<li class="closed patternType"><span class="items" id="' + elmId + '" map=p' + optCount + '>' + patternName + '<span class="expandToc" >+</span></span></li>';
                     
                 } else {
                         stringToReturn += '<li class="patternType"><span class="items" id="' + elmId + '" map=p' + optCount + '>' + patternName + '</span><ul><li class="li_vis hidden"><p>######</p></li></ul></li>';
                     
                 }
             });
             stringToReturn += '</ul>';
             return stringToReturn;
         }
     }
 };

 //========================================================================
 //  Function that initiates TOC tree
 //========================================================================

 jQuery.fn.createTOC = function(args) {
     var data = '';
     data = new content();
     data.setData(this);
     if (arguments.length === 0) {
         jQuery('.tree_view').html(data.getData())
     } else if (this.className() === '.CHAPTER') {
         var oCurrElm = this.parent();
         (oCurrElm.find('.dummy_div')).remove();
         data = new content();
         data.setData(oCurrElm);
         jQuery('.tree_view').html(data.getData())
     } else {
         if (typeof(args.parent()).className() === 'undefined' || (args.parent()).className() === '.ITEMS') {
             args.parent().parent().append(data.getData());
         } else {
             var vMapTo = typeof args.attr('data-pos') === 'undefined' ? false : args.attr('data-pos'),
                 oTocParentElm = jQuery('.tree_view'),
                 oTocCurrElm = oTocParentElm.find("*[map=" + vMapTo + "]");

             if (oTocCurrElm.children().length === 0) {
                 oTocCurrElm.append('<span class="expandToc" >+</span>');
                 oTocCurrElm.parent().removeClass('opened').addClass('closed');
                 oTocCurrElm.parent().find('ul').remove();
             } else {
                 var oDroppedParentElm = this.parent(),
                     vDroppedParentHasChild = (oDroppedParentElm.children()).length > 0,
                     vDroppedElmIndx = vDroppedParentHasChild === true ? this.index() : false;

                 if (vDroppedElmIndx !== false) {   
                     var oTocCurrElmPar = oTocCurrElm.parent(),
                         //oTocCurrElmIndx = Number(vDroppedElmIndx  + 1);
                         oTocCurrElmIndx = vDroppedElmIndx;

                     oTocCurrElmPar.children().eq(oTocCurrElmIndx).after(data.getDataOfDrop());
                 } else {
                     oTocCurrElm.parent().append(data.getDataOfDrop());
                 }
             }

         }
     }
     //========================================================================
     //  Function that binds TOC tree events
     //========================================================================
     var tocEvents = (function() {
         function tocEventList() {
             jQuery('.tree_view .items').off().on({
                 click /*mouseover*/: function(e) {
                     jQuery('.tree_view .items').removeAttr('active');
                     jQuery(this).attr('active', 'true');
                     jQuery(this).toHighlightAndScroll();
                     htmlEditor.currentTreeEl = jQuery(this);
                     htmlEditor.profileToggle(htmlEditor.currentTreeEl, e);
                     e.stopPropagation();
                 },
                 mouseout: function(e) {
                     var oParentElm = jQuery("iframe.cke_wysiwyg_frame").contents().find('body');
                     oParentElm.find('.indicator').removeClass('indicator');
                     e.stopPropagation();
                 }
             });

             jQuery('.tree_view .expandToc').off().on({
                 click: function() {
                     var oThisElm = jQuery(this);
                     oThisElm.parent().toHighlightAndScroll();
                     if (jQuery(this).parent().parent().children('ul').length === 0) {
                         oThisElm.toggleBtnState();
                         oThisElm.parent().mapElements().createTOC(jQuery(this));
                     } else {
                         oThisElm.toggleBtnState();
                     }
                 }
             });
         }


         jQuery(".tree_view ul ul").sortable({
             placeholder: 'ui-state-highlight-left-pane',
             connectWith: '.tree_view ul ul',
             tolerance: 'intersect',
             dropOnEmpty: true,
             start: function(e, ui) {
                 onStart(ui, jQuery(this));
             },
             update: function(e, ui) {
                 onUpdate(ui, jQuery(this));
             },
             stop: onStop
         }).disableSelection();

         var oUiInitialPos = 0,
             oUiInitialParent = {},
             oUiInitialParentMap = 0;

         function onStart(oUi, oThisElm) {
             var oUiItem = oUi.item;
             oUiInitialParent = oThisElm;
             oUiInitialParentMap = oThisElm.prev().attr('map');
             oUiInitialPos = oUiItem.getIndex();
             jQuery('.li_vis').removeClass('hidden').addClass('show');
             oUiItem.find('.li_vis').removeClass('show').addClass('hidden');
         }

         function onUpdate(oUi, oThisElm) {
             var oUiItem = oUi.item,
                 oParentElm = oUiItem.parent(),
                 oUiFinalPos = oUiItem.getIndex(),
                 oParentFrameElm = jQuery("iframe.cke_wysiwyg_frame").contents().find('body'),
                 oRefToFrameElm_1 = oUiItem.children('.items').mapElements(),
                 oRefToFrameElm_2 = (oParentElm.children()
                     .eq(oUiFinalPos - 1)).children('.items').mapElements(),
                 oRefToFrameElmClone = oRefToFrameElm_1.clone();
             if (oUiFinalPos > 0) {
                 if (typeof oRefToFrameElm_2.prop('tagName') !== 'undefined') {
                     oRefToFrameElm_2.after(oRefToFrameElmClone);
                     oRefToFrameElm_1.remove();
                     oUiInitialParent.setLiForEmptyNode();
                 }
             } else {
                 oRefToFrameElm_2 = (oParentElm.children()
                     .eq(oUiFinalPos + 1)).children('.items').mapElements();

                 if (typeof oRefToFrameElm_2.prop('tagName') === 'undefined') {
                     var oParElm = oUiItem.parent();
                     oRefToFrameElm_2 = (oParElm.prev()).mapElements();
                     oParElm.parent().find('.items').eq(0).find('.expandToc').remove();
                     oParElm.parent().find('.items').eq(0).append('<span class="expandToc" >-</span>');
                     oRefToFrameElm_2.append(oRefToFrameElmClone);
                     oRefToFrameElm_1.remove();
                     tocEventList();
                 } else {
                     oRefToFrameElm_2.before(oRefToFrameElmClone);
                     oRefToFrameElm_1.remove();
                     oUiInitialParent.setLiForEmptyNode();
                 }
             }

             htmlEditor.populateMeta();
         }

         function onStop() {
             jQuery('.li_vis').removeClass('show').addClass('hidden');
             htmlEditor.enableSave();
         }
         tocEventList();
     })();
 }

 //========================================================================
 //  Function that sets content for empty nodes of tree
 //========================================================================
 jQuery.fn.setLiForEmptyNode = function() {
     if (this.find('li').length < 2) {
         //this.prev().find('.expandToc').remove();
         if (this.prev().find('li').length < 1) {
             this.prev().find('.expandToc').remove();
         }
         if (this.next().find('li').length < 1) {
             this.next().find('.expandToc').remove();
         }
         if (this.children().length === 0) {
             this.html('<li class="li_vis hidden"><p>######</p></li>');
         }

     }
 };

 //========================================================================
 //  Function that returns position of current tree element
 //========================================================================
 jQuery.fn.getIndex = function() {
     var oParentElm = jQuery(this).parents().children();
     return oParentElm.index(this);
 };


 //========================================================================
 //  Function that changes buttons states of TOC tree
 //========================================================================
 jQuery.fn.toggleBtnState = function() {
     var oThisElm = this;
     if (oThisElm.text() === '+') {
         oThisElm.text('-');
         oThisElm.parent().parent().removeClass('closed').addClass('opened');
     } else {
         oThisElm.text('+');
         oThisElm.parent().parent().removeClass('opened').addClass('closed');
     }
 };

 //========================================================================
 //  Function that maps Iframe elements with that of current tree element
 //========================================================================
 jQuery.fn.mapElements = function() {
     var id = this.attr('id');
     var datapos = this.attr('map');
     var oParentElm = jQuery("iframe.cke_wysiwyg_frame").contents().find('body');
     if (datapos != "undefined" && id != "undefined") {
         return oParentElm.find("*[id=" + id + "]");
     } else if (datapos != "undefined") {
         return oParentElm.find("*[data-pos=" + datapos + "]");
     } else {
         return oParentElm.find("*[id=" + id + "]");
     }

 };

 //=======================================================================================
 //  Function that maps Iframe elements with that of current tree element and removes it
 //=======================================================================================
 jQuery.fn.deleteTOCNode = function(dataPos) {
     var dataPos=dataPos;
    //  var dataPos = this.attr('data-pos');
     if(typeof dataPos === "undefined"){
         dataPos = this.attr('data-pos');
     }
     var oParentElm = jQuery(".tree_view");
     var parentElement=oParentElm.find("*[map=" + dataPos + "]").closest('li.patternType');
     var totalChildren=parentElement.find('ul li.patternChild').length;
    if(totalChildren <= 1){
         oParentElm.find("*[map=" + dataPos + "]").parent().remove();
         parentElement.remove();
     }
     else{
         oParentElm.find("*[map=" + dataPos + "]").parent().remove();
     }    
 };

 //======================================================================
 //  Function that current elements name
 //======================================================================
 jQuery.fn.tagName = function() {
     return this.prop("tagName").toUpperCase();
 };

 //======================================================================
 //  Function that current elements name of the class
 //======================================================================
 jQuery.fn.className = function() {
     if (typeof this.attr("class") === 'undefined') return '';
     return '.' + this.attr("class").toUpperCase();
 };

 //======================================================================
 //  Function that scroll Iframe body upon highlight
 //======================================================================
 jQuery.fn.toHighlightAndScroll = function() {
     var oToHighlight = this.mapElements();
     if (typeof oToHighlight.offset() === 'undefined') {
         swal("DOM navigation is disabled in source view");
     } else {
         var oIframeEditorElm = jQuery("iframe.cke_wysiwyg_frame").contents(),
             oToHighlightToScrollTop = oToHighlight.offset().top - 60;
         oIframeEditorElm.scrollTop(oToHighlightToScrollTop);
         htmlEditor.currentEditorEl = oToHighlight;
         oToHighlight.addClass('indicator');
     }
 };