var _widget_ = {
    data: {}, // set a variable to store all required data.
    guid: "", // set guid for global use.
    data_path: "", // set script path for global use.
    init: function () {
       widgetDataGetter("GET");
    },
    initData: function () {
        //Get Iframe guid & data-path
        _widget_.guid = arguments[0].frame_id; // set dynamic guid
        _widget_.data_path = arguments[0].data_path; // set dynamic data path

        _widget_.loadData(_widget_.data_path, _widget_.guid); // call loadData method to get the data from external resource
    },
    loadData: function (path, id) {
        var aScript = document.createElement('script'); //Creates script element
        aScript.setAttribute("type", "text/javascript");
        aScript.setAttribute("src", path);
        aScript.setAttribute("data-guid", id);

        try {
            document.head.appendChild(aScript); //Appends the script to head of iframe/object

            // Callback fires after script gets loaded
            aScript.onload = function () {
                // console.log("success!!");
                 try{
                    $.extend(_widget_.data, widget_data);
                }
                catch(exc){
                   // console.log(exc)
                }
                _widget_.bindEvents();
            }
            // Callback fires if script fails to load
            aScript.onerror = function (e) {
                // console.log("error!");

                _widget_.bindEvents();
            }
        } catch (e) {// error handler
            // console.log("error received", e);
        }
    },
    bindEvents: function () {

        $(".collapsed-title").html(_widget_.data.title); // set html content for header.
        $(".collapsed-description").html(_widget_.data.description); // set html content for body part

        /*
         * Set data attr toggle on page load
         */
        $(".toggle-collapse").parent().find(".collapsed-description").data("toggle", "slideUp").slideUp(300);

        /*
         * Bind click event on header. Will toggle collapse on click.
         */
        $(".toggle-collapse").off("click").on("click", function () {
            var collapsable = $(this).parent().find(".collapsed-description");
            if (collapsable.data("toggle") === "slideUp") {
                collapsable.slideDown(300);
                collapsable.data("toggle", "slideDown");
                $(this).find("a").removeClass("collapsed");
            } else if (collapsable.data("toggle") === "slideDown") {
                collapsable.slideUp(300);
                collapsable.data("toggle", "slideUp");
                $(this).find("a").addClass("collapsed");
            }
        });
    }
};


$(window).load(function () {
    _widget_.init();
});
