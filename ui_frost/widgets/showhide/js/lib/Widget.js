function Widget(){
  // Name of the Widget
  this.Name = "";
  // Widget version
  this.Version = "";
  // unique Identifier
  this.GUID = ""
  // Description of the widget
  this.Description = "";
  // data ID
  this.Dataid = "";
  // data path
  this.Datapath = "";
  // Launch HTML file of the widget
  this.LaunchHTML = "";
  // Config HTML file of the widget
  this.ConfigHTML = "";
  // Launch CSS file of the widget
  this.LaunchCSS = "";
  // Config CSS file of the widget
  this.ConfigCSS = "";
  // Launch JS file of the widget
  this.LaunchJS = "";
  // Config JS file of the widget
  this.ConfigJS = "";
  // Init Data
  this.InitData = {};
  // Config Data Path
  this.ConfigData = "";
  // Imagepath Array
  this.Images = [];
  // Other Files
  this.OtherFiles = [];
  // Helper function
  this.Helpers = {};
  
  return this;
}