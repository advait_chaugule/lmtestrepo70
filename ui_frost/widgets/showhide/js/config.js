var _widget_ = {
    data: {}, // set a variable to store all required data/
    guid: "", // set guid for global use.
    data_path: "", // set script path for global use.
    init: function () {
        widgetDataGetter("PUT"); //Request to get default values of widget
    },
    initData: function () {
        _widget_.guid = arguments[0].frame_id;
        ; // set static guid
        _widget_.data_path = arguments[0].data_path; // set static data path

        console.info(arguments[0].images)
        _widget_.loadData(_widget_.data_path, _widget_.guid); // call loadData method to get the data from external resource
    },
    loadData: function (path, id) {
        var aScript = document.createElement('script'); //Creates script element
        aScript.setAttribute("type", "text/javascript"); // set script type
        aScript.setAttribute("src", path); // set path
        aScript.setAttribute("data-guid", id); // set arg id as data attr.

        try {
            document.head.appendChild(aScript); //Appends the script to head of iframe/object

            // Callback fires after script gets loaded
            aScript.onload = function () {
                // console.log("success!!");

                try {
                    $.extend(_widget_.data, widget_data);
                }
                catch (exc) {
                    // console.log(exc)
                }
                _widget_.bindEvents();
            }
            // Callback fires if script fails to load
            aScript.onerror = function (e) {
                // console.log("error!");

                _widget_.bindEvents();
            }
        } catch (e) {// error handler
            console.log("error received", e);
        }
    },
    bindEvents: function () {

        /*
         * Instantiate summernote editor(MIT licenced).
         */
        $("#sh-txt-area-editor").summernote({
            height: 300, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: true, // set focus to editable area after initializing summernote
            toolbar: [// set toobar options
                ['style', ['bold', 'italic', 'underline']], // show only bold, italic and underline features
                ['para', ['ul', 'ol']], // set bullet and numbering
            ],
            styleWithSpan: false,
            onInit: function () {
                _widget_.showContent(_widget_.data);
            }
        });
        /*
         * Append buttons to increase or decrease line indentation
         */
        $(".note-para").append('<button type="button" class="btn btn-default btn-sm" title="" data-event="indent" tabindex="-1" data-original-title="Indent (CTRL+])"><i class="fa fa-indent"></i></button><button type="button" class="btn btn-default btn-sm" title="" data-event="outdent" tabindex="-1" data-original-title="Outdent (CTRL+[)"><i class="fa fa-outdent"></i></button>');

        /*
         * Bind click event to save button
         */
        $("#save-btn").off().on("click", function () {

            var title = $("#sh-title").val();
            var description = $("#sh-txt-area-editor").code();

            if (typeof title != undefined && title != "" && title != null) {
                _widget_.data.title = title;

                if (typeof description != undefined && description != "" && description != null) {
                    _widget_.data.description = description;

                    //console.log(_widget_.data);
                    widgetDataSetter('POST', _widget_.data); //@param type, json_data
                    // set_data(data);
                } else {
                    alert("Description field cannot be empty!!");
                }
            } else {
                alert("Title field cannot be empty!!");
            }
        });

        /*
         * Bind click event to cancel button
         */
        $("#cancel-btn").off().on("click", function () {
            widgetDataGetter("CLOSE");
        });
    },
    showContent: function (obj) {
        var title = obj.title;
        var description = obj.description;

        if (typeof title != undefined && title != "" && title != null && $("#sh-title").length > 0) {
            $("#sh-title").val(title);
        }
        if (typeof description != undefined && description != "" && description != null && $("#sh-txt-area-editor").length > 0) {
            $("#sh-txt-area-editor").code(description);
        }
    }
};


$(window).load(function () {
    _widget_.init();
});
