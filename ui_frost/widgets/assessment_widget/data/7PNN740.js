var advJsonData = {
    "quiz": [{
            "QuestionId": "8MAVC34",
            "QuestionData": {
                "question_type": {
                    "text": "open-ended"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": '<math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mn>1</mn><mn>2</mn></mfrac><msqrt><mn>5</mn></msqrt><mo>&#x2208;</mo><mo>&#x220B;</mo><mo>&#x2229;</mo><mo>&#x2282;</mo><mo>&#x2283;</mo><mi mathvariant="normal">&#x3C0;</mi><menclose notation="updiagonalstrike"><mn>5</mn><mstyle displaystyle="false"><munderover><mo>&#x2211;</mo><mn>2</mn><mn>1</mn></munderover></mstyle></menclose></math>'
                },
                "question_title": {
                    "text": "This is a test for Open Ended"
                },
                "instruction_text": {
                    "text": "This is a instructional text for Open Ended"
                },
                "response_type": {
                    "text": "with-response"
                },
                "grading_type": {
                    "text": "without-grading"
                },
                "sample_answer": {
                    "text": "Hello"
                },
                "feedback": {
                    "text": "Hello"
                },
                "media": {
                    "type": "assets",
                    "url": "http://techslides.com/demos/sample-videos/small.mp4",
                    "title": "Lighthouse.jpg",
                    "asset_type": "video"
                }
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC34",
            "OriginalQuestionId": "8MAVC34",
            "IsDeleted": false,
            "SelectedOptions": null
    }]
}