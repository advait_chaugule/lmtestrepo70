var advJsonData = {
    "quiz": [{
            "QuestionId": "8MAVC31",
            "QuestionData": {
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "New Question With The Data?"
                },
                "question_title": {
                    "text": "New Question Test"
                },
                "instruction_text": {
                    "text": "Please answer the question"
                },
                "correct_answer": "1,3",
                "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": "Choice 1",
                        "correct_feedback": "Correct Feedback Data 1",
                        "incorrect_feedback": "Incorrect Feedback data 1",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Choice 2",
                        "correct_feedback": "Correct Feedback Data 2",
                        "incorrect_feedback": "Incorrect Feedback data 2",
                        "score": "0",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": true,
                        "text": "Choice 3",
                        "correct_feedback": "Correct Feedback Data 3",
                        "incorrect_feedback": "Incorrect Feedback data 3",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 4,
                        "assess": false,
                        "text": "Choice 4",
                        "correct_feedback": "Correct Feedback Data 4",
                        "incorrect_feedback": "Incorrect Feedback data 4",
                        "score": "0",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct feedback data"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect feedback data"
                },
                "hints": [{
                        "hintid": 1,
                        "text": "Please check the hint for the question"
                    }, {
                        "hintid": 2,
                        "text": "You have to choose multiple answer to get the full  marks"
                    }],
                "settings": {
                    "score": "2"
                },
                "media": {
                    "type": "brightcove",
                    "brightcove_id": "5116441932001",
                    "url": "\/\/players.brightcove.net\/2402232200001\/default_default\/index.html?videoId=5116441932001",
                    "height": "660",
                    "width": "540"
                }
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC31",
            "OriginalQuestionId": "8MAVC31",
            "IsDeleted": false,
            "SelectedOptions": null
        }, {
            "QuestionId": "8MAVC31",
            "QuestionData": {
                "question_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "New Question With The Data2?"
                },
                "question_title": {
                    "text": "New Question Test"
                },
                "instruction_text": {
                    "text": "Please answer the question"
                },
                "correct_answer": "1,3",
                "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": "Choice 1",
                        "correct_feedback": "Correct Feedback Data 1",
                        "incorrect_feedback": "Incorrect Feedback data 1",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Choice 2",
                        "correct_feedback": "Correct Feedback Data 2",
                        "incorrect_feedback": "Incorrect Feedback data 2",
                        "score": "0",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": false,
                        "text": "Choice 3",
                        "correct_feedback": "Correct Feedback Data 3",
                        "incorrect_feedback": "Incorrect Feedback data 3",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 4,
                        "assess": false,
                        "text": "Choice 4",
                        "correct_feedback": "Correct Feedback Data 4",
                        "incorrect_feedback": "Incorrect Feedback data 4",
                        "score": "0",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct feedback data"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect feedback data"
                },
                "hints": [{
                        "hintid": 1,
                        "text": "Please check the hint for the question"
                    }, {
                        "hintid": 2,
                        "text": "You have to choose multiple answer to get the full  marks"
                    }],
                "settings": {
                    "score": "2"
                },
                "media": {
                    "type": "no-media",
                    "brightcove_id": "5116441932001",
                    "url": "",
                    "height": "150",
                    "width": "300"
                }
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC31",
            "OriginalQuestionId": "8MAVC31",
            "IsDeleted": false,
            "SelectedOptions": null
        }, {
            "QuestionId": "8MAVC31",
            "QuestionData": {
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "New Question With The Data3?"
                },
                "question_title": {
                    "text": "New Question Test"
                },
                "instruction_text": {
                    "text": "Please answer the question"
                },
                "correct_answer": "1,3",
                "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": "Choice 1",
                        "correct_feedback": "Correct Feedback Data 1",
                        "incorrect_feedback": "Incorrect Feedback data 1",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Choice 2",
                        "correct_feedback": "Correct Feedback Data 2",
                        "incorrect_feedback": "Incorrect Feedback data 2",
                        "score": "0",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": true,
                        "text": "Choice 3",
                        "correct_feedback": "Correct Feedback Data 3",
                        "incorrect_feedback": "Incorrect Feedback data 3",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 4,
                        "assess": false,
                        "text": "Choice 4",
                        "correct_feedback": "Correct Feedback Data 4",
                        "incorrect_feedback": "Incorrect Feedback data 4",
                        "score": "0",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct feedback data"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect feedback data"
                },
                "hints": [{
                        "hintid": 1,
                        "text": "Please check the hint for the question"
                    }, {
                        "hintid": 2,
                        "text": "You have to choose multiple answer to get the full  marks"
                    }],
                "settings": {
                    "score": "2"
                },
                "media": {
                    "type": "assets",
                    "url": "http://cdn.wonderfulengineering.com/wp-content/uploads/2014/03/high-resolution-wallpapers-25.jpg",
                    "title": "Lighthouse.jpg",
                    "asset_type": "image"
                }
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC31",
            "OriginalQuestionId": "8MAVC31",
            "IsDeleted": false,
            "SelectedOptions": null
        }, {
            "QuestionId": "8MAVC31",
            "QuestionData": {
                "question_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "New Question With The Data4?"
                },
                "question_title": {
                    "text": "New Question Test"
                },
                "instruction_text": {
                    "text": "Please answer the question"
                },
                "correct_answer": "1,3",
                "choices": [{
                        "choiceid": 1,
                        "assess": false,
                        "text": "Choice 1",
                        "correct_feedback": "Correct Feedback Data 1",
                        "incorrect_feedback": "Incorrect Feedback data 1",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Choice 2",
                        "correct_feedback": "Correct Feedback Data 2",
                        "incorrect_feedback": "Incorrect Feedback data 2",
                        "score": "0",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": true,
                        "text": "Choice 3",
                        "correct_feedback": "Correct Feedback Data 3",
                        "incorrect_feedback": "Incorrect Feedback data 3",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 4,
                        "assess": false,
                        "text": "Choice 4",
                        "correct_feedback": "Correct Feedback Data 4",
                        "incorrect_feedback": "Incorrect Feedback data 4",
                        "score": "0",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct feedback data"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect feedback data"
                },
                "hints": [{
                        "hintid": 1,
                        "text": "Please check the hint for the question"
                    }, {
                        "hintid": 2,
                        "text": "You have to choose multiple answer to get the full  marks"
                    }],
                "settings": {
                    "score": "2"
                },
                "media": {
                    "type": "brightcove",
                    "brightcove_id": "5116441932001",
                    "url": "\/\/players.brightcove.net\/2402232200001\/default_default\/index.html?videoId=5116441932001",
                    "height": "150",
                    "width": "300"
                }
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC31",
            "OriginalQuestionId": "8MAVC31",
            "IsDeleted": false,
            "SelectedOptions": null
        }]
};