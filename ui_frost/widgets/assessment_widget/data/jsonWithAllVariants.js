var advJsonData = {
    "quiz": [{
        "QuestionId": "Z96C02",
        "QuestionData": {
            "question_type": {
                "text": "fib"
            },
            "question_layout": {
                "text": ""
            },
            "question_stem": {
                "text": "<p>jack and [[1]] went up the [[2]] at [[3]] pm<\/p>"
            },
            "question_title": {
                "text": "FIB teXT"
            },
            "instruction_text": {
                "text": "<p>fill in the blanks<\/p>"
            },
            "textwithblanks": {
                "text": "<p>jack and [[1]] went up the [[2]] at [[3]] pm<\/p>"
            },
            "choices": [{
                "blankid": 1,
                "noforeachblank": "[[1]]",
                "textforeachblank": [{
                    "text": "jill"
                }, {
                    "text": "mill"
                }],
                "correct_feedback": "",
                "incorrect_feedback": "",
                "score": 0,
                "interaction": "textentry",
                "inputtype": "alphanumeric"
            }, {
                "blankid": 2,
                "noforeachblank": "[[2]]",
                "textforeachblank": [{
                    "text": "hill"
                }, {
                    "text": "till"
                }],
                "correct_feedback": "",
                "incorrect_feedback": "",
                "score": 0,
                "interaction": "textentry",
                "inputtype": "alphanumeric"
            }, {
                "blankid": 3,
                "noforeachblank": "[[3]]",
                "textforeachblank": [{
                    "text": "10"
                }],
                "correct_feedback": "",
                "incorrect_feedback": "",
                "score": 0,
                "interaction": "textentry",
                "inputtype": "alphanumeric"
            }],
            "global_correct_feedback": {
                "text": "<p>you are a genius<\/p>"
            },
            "global_incorrect_feedback": {
                "text": "<p>try harder<\/p>"
            },
            "media": {
                "type": "no-media"
            }
        },
        "VersionNo": "0",
        "DisplayQuestionId": "Z96C02",
        "OriginalQuestionId": "Z96C02",
        "IsDeleted": false,
        "SelectedOptions": null,
        "question_number": ""
    }, {
      "QuestionData": {
        "question_stem": {
          "text": "<p>FIB DragDrop</p>"
        },
        "question_title": {
          "text": ""
        },
        "instruction_text": {
          "text": '<math xmlns="http://www.w3.org/1998/Math/MathML"><mrow><mi>A</mi><mo>=</mo><mfenced open="[" close="]"><mtable><mtr><mtd><mi>x</mi></mtd><mtd><mi>y</mi></mtd></mtr><mtr><mtd><mi>z</mi></mtd><mtd><mi>w</mi></mtd></mtr></mtable></mfenced></mrow></math>'
        },
        "textwithblanks": {
          "text":  "<p>jack and [[1]] went up the [[2]] at [[3]] pm</p>"
        },
        "question_type": {
          "text": "fib"
        },
        "question_layout": {
          "text": "top"
        },
        "choices": [
          {
            "assess": "",
            "noforeachblank": "[[1]]",
            "textforeachblank": [
              {
                "text": "jill"
              },
                {
                "text": "mill"
              }
            ],
            "correct_feedback": "",
            "incorrect_feedback": "",
            "score": "0",
            "interaction": "dragdrop",
            "currentClass": "",
            "CurrentOptionIndex": 1,
            "displayfeedback": false,
            "arrList": [],
            "Canvas": [],
            "Drop": true,
            "key": 0,
            "UserInput": "",
            "AnswerClass": "",
            "width": "auto",
            "showOptionFeedbackModal": true,
            "showAttempt": true,
            "ShowGlobalFeedback": true,
            "multiSelectItem": [],
            "accept": true,
            "FibClass": "",
            "$$hashKey": "object:76"
          },
              {
            "assess": "",
            "noforeachblank": "[[2]]",
            "textforeachblank": [
              {
                "text": "hill"
              }
            ],
            "correct_feedback": "",
            "incorrect_feedback": "",
            "score": "0",
            "interaction": "dragdrop",
            "currentClass": "",
            "CurrentOptionIndex": 2,
            "displayfeedback": false,
            "arrList": [],
            "Canvas": [],
            "Drop": true,
            "key": 0,
            "UserInput": "",
            "AnswerClass": "",
            "width": "auto",
            "showOptionFeedbackModal": true,
            "showAttempt": true,
            "ShowGlobalFeedback": true,
            "multiSelectItem": [],
            "accept": true,
            "FibClass": "",
            "$$hashKey": "object:76"
          },
              {
            "assess": "",
            "noforeachblank": "[[3]]",
            "textforeachblank": [
              {
                "text": "10"
              }
            ],
            "correct_feedback": "dfgfdg",
            "incorrect_feedback": "dfgg",
            "score": "0",
            "interaction": "dragdrop",
            "currentClass": "",
            "CurrentOptionIndex": 3,
            "displayfeedback": false,
            "arrList": [],
            "Canvas": [],
            "Drop": true,
            "key": 0,
            "UserInput": "",
            "AnswerClass": "",
            "width": "auto",
            "showOptionFeedbackModal": true,
            "showAttempt": true,
            "ShowGlobalFeedback": true,
            "multiSelectItem": [],
            "accept": true,
            "FibClass": "",
            "$$hashKey": "object:76"
          }
        ],
             "distractors": [
          {
            "distractorid": 1,
            "text": '<p><span><math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mn>1</mn><mn>2</mn></mfrac><mfenced open="|" close="|"><msup><maction actiontype="argument"><mrow></mrow></maction><msqrt><mroot><mfenced open="[" close="]"><mfenced open="[" close="]"><msqrt><mo>&plusmn;</mo><mo>&plusmn;</mo><mo>&divide;</mo></msqrt></mfenced></mfenced><mrow></mrow></mroot></msqrt></msup></mfenced></math></span></p>'
          },
            {
            "distractorid": 2,
            "text": '<math xmlns="http://www.w3.org/1998/Math/MathML"><msqrt><mfenced open="|" close="|"><mroot><mrow><mo>&#xD7;</mo><mo>&#x2208;</mo><mo>&#x2282;</mo><mo>&#xF7;</mo><mi>h</mi><mi>k</mi><mi>j</mi><mi>f</mi><mi>g</mi><mi>h</mi><mfenced><mn>654984</mn></mfenced><mo>&#x2205;</mo><mo>&#x2264;</mo><mo>&#x2265;</mo><mfenced><mrow/></mfenced></mrow><mrow/></mroot></mfenced></msqrt></math>'
          },
            {
            "distractorid": 1,
            "text": "dist3"
          },
            {
            "distractorid": 2,
            "text": "dist4"
          },
            {
            "distractorid": 1,
            "text": "dist5"
          },
            {
            "distractorid": 2,
            "text": "dist6"
          }
        ],
        /*"distractors": [
          {
            "distractorid": 1,
            "text": ""
          }
        ],*/
        "global_correct_feedback": {
          "text": "ryy"
        },
        "global_incorrect_feedback": {
          "text": "ftytt"
        },
        "hints": [],
        "exhibit": [
          {
            "path": "",
            "exhibit_type": false
          }
        ],
        "settings": {
          "score": "0"
        }
      },
      "VersionNo": 0,
      "DisplayQuestionId": "0",
      "OriginalQuestionId": "",
      "IsDeleted": false,
      "SelectedOptions": null,
      "passage_id": 0,
      "passage_title": null,
      "passage_text": null,
      "CurrentQuestion": 1
    },{
            "QuestionData": {
                "question_stem": {
                    "text": "<p>Sample FIB</p>"
                },
                "question_title": {
                    "text": "Sample FIB"
                },
                "instruction_text": {
                    "text": "<p>hello hi bye</p>"
                },
                "textwithblanks": {
                    "text": "<p>jack and [[1]] went up the [[2]] at [[3]] pm</p>"
                },
                "question_type": {
                    "text": "fib"
                },
                "choices": [
                    {
                        "assess": "",
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "jill"
              }
            ],
                 
                        "correct_feedback": "correct",
                        "incorrect_feedback": "incorrect",
                        "score": "0",
                        "interaction": "textentry",
                        "inputalignment": "right",
                        "characters": "",
                        "inputtype": "alphabet",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 1,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>jill</span>"
            ],
                        "key": 0,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1027"
          },
                    {
                        "assess": "",
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "hill"
              }
            ],
                               "distractorforeachblank": [{
                            "text": "hello"
                }, {
                            "text": "hill"
                }, {
                            "text": "bye"
                }],
                        "correct_feedback": "correct answer",
                        "incorrect_feedback": "incorrect answer",
                        "score": "0",
                        "interaction": "inline",
                        "inputalignment": "left",
                        "characters": "",
                        "inputtype": "alphabet",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 2,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>hill</span>"
            ],
                        "key": 1,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1028"
          },
                    {
                        "assess": "",
                        "noforeachblank": "[[3]]",
                        "textforeachblank": [
                            {
                                "text": "10"
              }
            ],
                        "correct_feedback": "correct ans",
                        "incorrect_feedback": "incorrect ans",
                        "score": "0",
                        "interaction": "textentry",
                        "inputalignment": "left",
                        "characters": "5",
                        "inputtype": "numeric",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 3,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>10</span>"
            ],
                        "key": 2,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1029"
          }
        ],
                "global_correct_feedback": {
                    "text": "<p>correct</p>"
                },
                "global_incorrect_feedback": {
                    "text": "<p>incorrect</p>"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<p>no hint</p>"
          }
        ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": false
          }
        ],
                "settings": {
                    "score": "0"
                }
            },
            "VersionNo": 3,
            "DisplayQuestionId": "344123",
            "OriginalQuestionId": "4ef15c5ac60fa04aa99c4f5fbf5dd032",
            "IsDeleted": false,
            "SelectedOptions": null,
            "passage_id": 0,
            "passage_title": null,
            "passage_text": null,
            "CurrentQuestion": 1
    },{
        "QuestionId": "7PNN739",
        "QuestionData": {
            "question_type": {
                "text": "in-context"
            },
            "question_response_type": {
                "text": "mcss"
            },
            "question_interaction": {
                "text": "clickable"
            },
            "question_layout": {
                "text": "vertical"
            },
            "question_stem": {
                "text": "In Context Stem"
            },
            "question_title": {
                "text": "In Context Title"
            },
            "instruction_text": {
                "text": "In Context Text"
            },
            "sentences": [{
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "In Context Stem2"
                },
                "sentence-id":"0",
                "sentence":{
                  "text":"1st sentence"
                },
                "question_title": {
                    "text": "New Question Test"
                },
                "instruction_text": {
                    "text": ""
                },
                "correct_answer": "1",
                "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": '<p><span><math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mn>1</mn><mn>2</mn></mfrac><mfenced open="|" close="|"><msup><maction actiontype="argument"><mrow></mrow></maction><msqrt><mroot><mfenced open="[" close="]"><mfenced open="[" close="]"><msqrt><mo>&plusmn;</mo><mo>&plusmn;</mo><mo>&divide;</mo></msqrt></mfenced></mfenced><mrow></mrow></mroot></msqrt></msup></mfenced></math></span></p>',
                        "correct_feedback": "Correct Feedback Data 1",
                        "incorrect_feedback": "Incorrect Feedback data 1",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Choice 2",
                        "correct_feedback": "Correct Feedback Data 2",
                        "incorrect_feedback": "Incorrect Feedback data 2",
                        "score": "0",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": false,
                        "text": "Choice 3",
                        "correct_feedback": "Correct Feedback Data 3",
                        "incorrect_feedback": "Incorrect Feedback data 3",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 4,
                        "assess": false,
                        "text": "Choice 4",
                        "correct_feedback": "Correct Feedback Data 4",
                        "incorrect_feedback": "Incorrect Feedback data 4",
                        "score": "0",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct feedback data"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect feedback data"
                },
                "media": {
                    "type": "no-media",
                    "brightcove_id": "5116441932001",
                    "url": "",
                    "height": "150",
                    "width": "300"
                }
            },{
                "question_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "In Context Stem3"
                },
                "sentence-id":"0",
                "sentence":{
                  "text":"1st sentence"
                },
                "question_title": {
                    "text": "New Question Test"
                },
                "instruction_text": {
                    "text": ""
                },
                "correct_answer": "1",
                "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": "Choice 1",
                        "correct_feedback": "Correct Feedback Data 1",
                        "incorrect_feedback": "Incorrect Feedback data 1",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Choice 2",
                        "correct_feedback": "Correct Feedback Data 2",
                        "incorrect_feedback": "Incorrect Feedback data 2",
                        "score": "0",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": false,
                        "text": "Choice 3",
                        "correct_feedback": "Correct Feedback Data 3",
                        "incorrect_feedback": "Incorrect Feedback data 3",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 4,
                        "assess": false,
                        "text": "Choice 4",
                        "correct_feedback": "Correct Feedback Data 4",
                        "incorrect_feedback": "Incorrect Feedback data 4",
                        "score": "0",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct feedback data"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect feedback data"
                },
                "media": {
                    "type": "no-media",
                    "brightcove_id": "5116441932001",
                    "url": "",
                    "height": "150",
                    "width": "300"
                }
            },{
                "question_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "In Context Stem"
                },
                "sentence-id":"0",
                "sentence":{
                  "text":"1st sentence"
                },
                "question_title": {
                    "text": "New Question Test"
                },
                "instruction_text": {
                    "text": ""
                },
                "correct_answer": "1",
                "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": "Choice 1",
                        "correct_feedback": 'xyz',
                        "incorrect_feedback": "Incorrect Feedback data 1",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Choice 2",
                        "correct_feedback": '<math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mn>1</mn><mn>2</mn></mfrac><msqrt><mn>5</mn></msqrt><mo>&#x2208;</mo><mo>&#x220B;</mo><mo>&#x2229;</mo><mo>&#x2282;</mo><mo>&#x2283;</mo><mi mathvariant="normal">&#x3C0;</mi><menclose notation="updiagonalstrike"><mn>5</mn><mstyle displaystyle="false"><munderover><mo>&#x2211;</mo><mn>2</mn><mn>1</mn></munderover></mstyle></menclose></math>',
                        "incorrect_feedback":'<math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mn>1</mn><mn>2</mn></mfrac><msqrt><mn>5</mn></msqrt><mo>&#x2208;</mo><mo>&#x220B;</mo><mo>&#x2229;</mo><mo>&#x2282;</mo><mo>&#x2283;</mo><mi mathvariant="normal">&#x3C0;</mi><menclose notation="updiagonalstrike"><mn>5</mn><mstyle displaystyle="false"><munderover><mo>&#x2211;</mo><mn>2</mn><mn>1</mn></munderover></mstyle></menclose></math>',
                        "score": "0",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": false,
                        "text": "Choice 3",
                        "correct_feedback": "Correct Feedback Data 3",
                        "incorrect_feedback": "Incorrect Feedback data 3",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 4,
                        "assess": false,
                        "text": "Choice 4",
                        "correct_feedback": "Correct Feedback Data 4",
                        "incorrect_feedback": "Incorrect Feedback data 4",
                        "score": "0",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct feedback data"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect feedback data"
                },
                "media": {
                    "type": "no-media",
                    "brightcove_id": "5116441932001",
                    "url": "",
                    "height": "150",
                    "width": "300"
                }
            }],
            "media": {
                "type": "no-media"
            }
        },
        "VersionNo": "0",
        "DisplayQuestionId": "7PNN739",
        "OriginalQuestionId": "7PNN739",
        "IsDeleted": false,
        "SelectedOptions": null
    },{
        "QuestionId": "W623S54",
        "QuestionData": {
            "question_type": {
                "text": "multi-part"
            },
            "question_interaction": {
                "text": "clickable"
            },
            "question_layout": {
                "text": "vertical"
            },
            "question_stem": {
                "text": "HEY i am multipart"
            },
            "question_title": {
                "text": "New Question"
            },
            "instruction_text": {
                "text": "multipart New Question"
            },
            "subquestions": [{
                "question_stem": {
                    "text": "<p>Sample FIB</p>"
                },
                "question_title": {
                    "text": "Sample FIB"
                },
                "instruction_text": {
                    "text": "<p>hello hi bye</p>"
                },
                "textwithblanks": {
                    "text": "<p>jack and [[1]] went up the [[2]] at [[3]] pm</p>"
                },
                "question_type": {
                    "text": "fib"
                },
                "choices": [
                    {
                        "assess": "",
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "jill"
              }
            ],
                 
                        "correct_feedback": "correct",
                        "incorrect_feedback": "incorrect",
                        "score": "0",
                        "interaction": "textentry",
                        "inputalignment": "right",
                        "characters": "",
                        "inputtype": "alphabet",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 1,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>jill</span>"
            ],
                        "key": 0,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1027"
          },
                    {
                        "assess": "",
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "hill"
              }
            ],
                               "distractorforeachblank": [{
                            "text": "hello"
                }, {
                            "text": "hill"
                }, {
                            "text": "bye"
                }],
                        "correct_feedback": "correct answer",
                        "incorrect_feedback": "incorrect answer",
                        "score": "0",
                        "interaction": "inline",
                        "inputalignment": "left",
                        "characters": "",
                        "inputtype": "alphabet",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 2,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>hill</span>"
            ],
                        "key": 1,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1028"
          },
                    {
                        "assess": "",
                        "noforeachblank": "[[3]]",
                        "textforeachblank": [
                            {
                                "text": "10"
              }
            ],
                        "correct_feedback": "correct ans",
                        "incorrect_feedback": "incorrect ans",
                        "score": "0",
                        "interaction": "textentry",
                        "inputalignment": "left",
                        "characters": "5",
                        "inputtype": "numeric",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 3,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>10</span>"
            ],
                        "key": 2,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1029"
          }
        ],
                "global_correct_feedback": {
                    "text": "<p>correct</p>"
                },
                "global_incorrect_feedback": {
                    "text": "<p>incorrect</p>"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<p>no hint</p>"
          }
        ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": false
          }
        ],
                "settings": {
                    "score": "0"
                }
            },{
            "question_type": {
                "text": "in-context"
            },
            "question_response_type": {
                "text": "mcms"
            },
            "question_interaction": {
                "text": "clickable"
            },
            "question_layout": {
                "text": "vertical"
            },
            "question_stem": {
                "text": "In Context new "
            },
            "question_title": {
                "text": "In Context "
            },
            "instruction_text": {
                "text": "In Context "
            },
            "sentences": [{
                "sentence_id": 1,
                "sentence": {
                    "text": "In Context  S1 new1"
                },
                "question_title": {
                    "text": "In Context  S1"
                },
                "question_stem": {
                    "text": "In Context  S1 new1"
                },
                "instruction_text": {
                    "text": ""
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "choices": [{
                    "choiceid": 1,
                    "assess": false,
                    "text": "In Context 1.1",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 2,
                    "assess": true,
                    "text": "New In Context 1.4",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 3,
                    "assess": false,
                    "text": "In Context 1.2",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 4,
                    "assess": false,
                    "text": "In Context 1.3",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }],
                "correct_answer": "2,3",
                "global_correct_feedback": {
                    "text": ""
                },
                "global_incorrect_feedback": {
                    "text": ""
                },
                "media": {
                    "type": "no-mdeia"
                }
            }, {
                "sentence_id": 2,
                "sentence": {
                    "text": "In Context  s2 new2"
                },
                "question_title": {
                    "text": "In Context  s2"
                },
                "question_stem": {
                    "text": "In Context  s2 new2"
                },
                "instruction_text": {
                    "text": ""
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "choices": [{
                    "choiceid": 1,
                    "assess": false,
                    "text": "In Context  2.2",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 2,
                    "assess": true,
                    "text": "In Context  2.1",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }],
                "correct_answer": "",
                "global_correct_feedback": {
                    "text": ""
                },
                "global_incorrect_feedback": {
                    "text": ""
                },
                "media": {
                    "type": "no-mdeia"
                }
            }, {
                "sentence_id": 3,
                "sentence": {
                    "text": "In Context S3 new3"
                },
                "question_title": {
                    "text": "In Context S3"
                },
                "question_stem": {
                    "text": "In Context S3 new3"
                },
                "instruction_text": {
                    "text": ""
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "choices": [{
                    "choiceid": 1,
                    "assess": false,
                    "text": "In Context 3.1",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 2,
                    "assess": false,
                    "text": "In Context 3.3",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 3,
                    "assess": true,
                    "text": "In Context 3.2",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }],
                "correct_answer": "3",
                "global_correct_feedback": {
                    "text": ""
                },
                "global_incorrect_feedback": {
                    "text": ""
                },
                "media": {
                    "type": "no-mdeia"
                }
            }],
            "media": {
                "type": "no-media"
            }
        },{
            "question_type": {
                "text": "in-context"
            },
            "question_response_type": {
                "text": "mcms"
            },
            "question_interaction": {
                "text": "clickable"
            },
            "question_layout": {
                "text": "vertical"
            },
            "question_stem": {
                "text": "In Context "
            },
            "question_title": {
                "text": "In Context "
            },
            "instruction_text": {
                "text": "In Context "
            },
            "sentences": [{
                "sentence_id": 1,
                "sentence": {
                    "text": "In Context  S1"
                },
                "question_title": {
                    "text": "In Context  S1"
                },
                "question_stem": {
                    "text": "In Context  S1"
                },
                "instruction_text": {
                    "text": ""
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "choices": [{
                    "choiceid": 1,
                    "assess": false,
                    "text": "In Context 1.1",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 2,
                    "assess": true,
                    "text": "New In Context 1.4",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 3,
                    "assess": false,
                    "text": "In Context 1.2",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 4,
                    "assess": false,
                    "text": "In Context 1.3",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }],
                "correct_answer": "2,3",
                "global_correct_feedback": {
                    "text": ""
                },
                "global_incorrect_feedback": {
                    "text": ""
                },
                "media": {
                    "type": "no-mdeia"
                }
            }, {
                "sentence_id": 2,
                "sentence": {
                    "text": "In Context  s2"
                },
                "question_title": {
                    "text": "In Context  s2"
                },
                "question_stem": {
                    "text": "In Context  s2"
                },
                "instruction_text": {
                    "text": ""
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "choices": [{
                    "choiceid": 1,
                    "assess": false,
                    "text": "In Context  2.2",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 2,
                    "assess": true,
                    "text": "In Context  2.1",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }],
                "correct_answer": "",
                "global_correct_feedback": {
                    "text": ""
                },
                "global_incorrect_feedback": {
                    "text": ""
                },
                "media": {
                    "type": "no-mdeia"
                }
            }, {
                "sentence_id": 3,
                "sentence": {
                    "text": "In Context S3"
                },
                "question_title": {
                    "text": "In Context S3"
                },
                "question_stem": {
                    "text": "In Context S3"
                },
                "instruction_text": {
                    "text": ""
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "choices": [{
                    "choiceid": 1,
                    "assess": false,
                    "text": "In Context 3.1",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 2,
                    "assess": false,
                    "text": "In Context 3.3",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 3,
                    "assess": true,
                    "text": "In Context 3.2",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }],
                "correct_answer": "3",
                "global_correct_feedback": {
                    "text": ""
                },
                "global_incorrect_feedback": {
                    "text": ""
                },
                "media": {
                    "type": "no-mdeia"
                }
            }],
            "media": {
                "type": "no-media"
            }
        },{
                "question_type": {
                    "text": "open-ended"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "READ the passage below"
                },
                "question_title": {
                    "text": "New Openendeds"
                },
                "instruction_text": {
                    "text": '<math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mn>1</mn><mn>2</mn></mfrac><msqrt><mn>5</mn></msqrt><mo>&#x2208;</mo><mo>&#x220B;</mo><mo>&#x2229;</mo><mo>&#x2282;</mo><mo>&#x2283;</mo><mi mathvariant="normal">&#x3C0;</mi><menclose notation="updiagonalstrike"><mn>5</mn><mstyle displaystyle="false"><munderover><mo>&#x2211;</mo><mn>2</mn><mn>1</mn></munderover></mstyle></menclose></math>'
                },
                "response_type": {
                    "text": "with-response"
                },
                "media": {
                    "type": "assets",
                    "url": "https://wallpaperscraft.com/image/mercedes_gelandewagen_g63_brabus_black_g_tuning_2013_2014_suv_jeep_b63_4x4_hd_6k_93020_602x339.jpg",
                    "title": "Lighthouse.jpg",
                    "asset_type": "image"
                },
                "sample_answer": {
                    "text": "HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHello"
                },
                "feedback": {
                    "text": "i am the feedback"
                },
            },{
                "question_type": {
                    "text": "open-ended"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "Open Ended Question"
                },
                "question_title": {
                    "text": "Open Ended Question"
                },
                "instruction_text": {
                    "text": "Open Ended Question"
                },
                "response_type": {
                    "text": "without-response"
                },
                "answer": {
                    "text": "Open Ended Question"
                },
                "media": {
                    "type": "no-media"
                }
            },{
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "New Question With The Data?"
                },
                "question_title": {
                    "text": "New Question Test"
                },
                "instruction_text": {
                    "text": "Please answer the question"
                },
                "correct_answer": "1,3",
                "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": "Choice 1",
                        "correct_feedback": "Correct Feedback Data 1",
                        "incorrect_feedback": "Incorrect Feedback data 1",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Choice 2",
                        "correct_feedback": "Correct Feedback Data 2",
                        "incorrect_feedback": "Incorrect Feedback data 2",
                        "score": "0",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": true,
                        "text": "Choice 3",
                        "correct_feedback": "Correct Feedback Data 3",
                        "incorrect_feedback": "Incorrect Feedback data 3",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 4,
                        "assess": false,
                        "text": "Choice 4",
                        "correct_feedback": "Correct Feedback Data 4",
                        "incorrect_feedback": "Incorrect Feedback data 4",
                        "score": "0",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct feedback data"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect feedback data"
                },
                "hints": [{
                        "hintid": 1,
                        "text": "Please check the hint for the question"
                    }, {
                        "hintid": 2,
                        "text": "You have to choose multiple answer to get the full  marks"
                    }],
                "settings": {
                    "score": "2"
                },
                "media": {
                    "type": "brightcove",
                    "brightcove_id": "5116441932001",
                    "url": "https:\/\/players.brightcove.net\/2402232200001\/default_default\/index.html?videoId=5116441932001",
                    "height": "660",
                    "width": "540"
                }
            },{
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "New Question With The Data?"
                },
                "question_title": {
                    "text": "mcms with vdo"
                },
                "instruction_text": {
                    "text": "Please answer the question"
                },
                "correct_answer": "1,3",
                "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": "Choice 1",
                        "correct_feedback": "Correct Feedback Data 1",
                        "incorrect_feedback": "Incorrect Feedback data 1",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Choice 2",
                        "correct_feedback": "Correct Feedback Data 2",
                        "incorrect_feedback": "Incorrect Feedback data 2",
                        "score": "0",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": true,
                        "text": "Choice 3",
                        "correct_feedback": "Correct Feedback Data 3",
                        "incorrect_feedback": "Incorrect Feedback data 3",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 4,
                        "assess": false,
                        "text": "Choice 4",
                        "correct_feedback": "Correct Feedback Data 4",
                        "incorrect_feedback": "Incorrect Feedback data 4",
                        "score": "0",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct feedback data"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect feedback data"
                },
                "hints": [{
                        "hintid": 1,
                        "text": "Please check the hint for the question"
                    }, {
                        "hintid": 2,
                        "text": "You have to choose multiple answer to get the full  marks"
                    }],
                "settings": {
                    "score": "2"
                },
                "media": {
                    "type": "brightcove",
                    "brightcove_id": "5116441932001",
                    "url": "https:\/\/players.brightcove.net\/2402232200001\/default_default\/index.html?videoId=5116441932001",
                    "height": "660",
                    "width": "540"
                }
            },
                  {
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "New Question With The Data?"
                },
                "question_title": {
                    "text": "New Question Test"
                },
                "instruction_text": {
                    "text": "Please answer the question"
                },
                "correct_answer": "1,3",
                "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": "Choice 1",
                        "correct_feedback": "Correct Feedback Data 1",
                        "incorrect_feedback": "Incorrect Feedback data 1",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Choice 2",
                        "correct_feedback": "Correct Feedback Data 2",
                        "incorrect_feedback": "Incorrect Feedback data 2",
                        "score": "0",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": true,
                        "text": "Choice 3",
                        "correct_feedback": "Correct Feedback Data 3",
                        "incorrect_feedback": "Incorrect Feedback data 3",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 4,
                        "assess": false,
                        "text": "Choice 4",
                        "correct_feedback": "Correct Feedback Data 4",
                        "incorrect_feedback": "Incorrect Feedback data 4",
                        "score": "0",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct feedback data"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect feedback data"
                },
                "hints": [{
                        "hintid": 1,
                        "text": "Please check the hint for the question"
                    }, {
                        "hintid": 2,
                        "text": "You have to choose multiple answer to get the full  marks"
                    }],
                "settings": {
                    "score": "2"
                },
                "media": {
                    "type": "brightcove",
                    "brightcove_id": "5116441932001",
                    "url": "\/\/players.brightcove.net\/2402232200001\/default_default\/index.html?videoId=5116441932001",
                    "height": "660",
                    "width": "540"
                }
            },{
                "question_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "New Question with Media"
                },
                "question_title": {
                    "text": "New Question with Medias"
                },
                "instruction_text": {
                    "text": "New Question with Media"
                },
                "correct_answer": "3",
                "choices": [{
                        "choiceid": 1,
                        "assess": false,
                        "text": "New Choice 2",
                        "correct_feedback": "Correct Feedback 1",
                        "incorrect_feedback": "Incorrect Feedback 1",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "New Choice 1",
                        "correct_feedback": "Correct Feedback 2",
                        "incorrect_feedback": "Incorrect Feedback 2",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": true,
                        "text": "New Choice",
                        "correct_feedback": "Correct Feedback 3",
                        "incorrect_feedback": "Incorrect Feedback 3",
                        "score": "",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "esfgsege"
                },
                "global_incorrect_feedback": {
                    "text": "gsegseg"
                },
                "hints": [{
                        "hintid": 1,
                        "text": ""
                    }],
                "settings": {
                    "score": "1"
                },
                "media": {
                    "type": "assets",
                    "url": "https://wallpaperscraft.com/image/mercedes_gelandewagen_g63_brabus_black_g_tuning_2013_2014_suv_jeep_b63_4x4_hd_6k_93020_602x339.jpg",
                    "title": "Lighthouse.jpg",
                    "asset_type": "image"
                }
            }, {
                "question_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "Mcq Question Data"
                },
                "question_title": {
                    "text": "Mcq Question Data"
                },
                "instruction_text": {
                    "text": "Mcq Question Data"
                },
                "correct_answer": "1",
                "choices": [{
                    "choiceid": 1,
                    "assess": true,
                    "text": "Mcq Question Data",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 2,
                    "assess": false,
                    "text": "Mcq Question Data",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 3,
                    "assess": false,
                    "text": "Mcq Question Data",
                    "correct_feedback": "",
                    "incorrect_feedback": "",
                    "score": "",
                    "pinanswer": 0
                }],
                "global_correct_feedback": {
                    "text": "dsfdgfds"
                },
                "global_incorrect_feedback": {
                    "text": "fesfesf"
                },
                "hints": [{
                    "hintid": 1,
                    "text": ""
                }],
                "settings": {
                    "score": ""
                },
                "media": {
                    "type": "no-media"
                }
            }],
            "media": {
                "type": "assets",
                "url": "http:\/\/frost-qa.s3.amazonaws.com\/local_dev_ar\/assets\/images\/cczufc3376c187563ef2f49911e2937b\/Desert.jpg",
                "title": "Desert.jpg",
                "asset_type": "image"
            },
            "global_correct_feedback": {
                    "text": "u r correct"
                },
                "global_incorrect_feedback": {
                    "text": "incorrect"
                },
        },
        "VersionNo": "0",
        "DisplayQuestionId": "W623S54",
        "OriginalQuestionId": "W623S54",
        "IsDeleted": false,
        "SelectedOptions": null
    },{
            "QuestionId": "8MAVC41",
            "QuestionData": {
                "question_type": {
                    "text": "open-ended"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "READ the passage below"
                },
                "question_title": {
                    "text": "New Openendeds"
                },
                "instruction_text": {
                    "text": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
                },
                "response_type": {
                    "text": "with-response"
                },
                "media": {
                    "type": "assets",
                    "url": "http://www.expono.com/go/photo/839365_s.jpg",
                    "title": "Lighthouse.jpg",
                    "asset_type": "image"
                },
                "sample_answer": {
                    "text": "HelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHelloHello"
                },
                "feedback": {
                    "text": "i am the feedback"
                },
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC41",
            "OriginalQuestionId": "8MAVC41",
            "IsDeleted": false,
            "SelectedOptions": null
        }, {
            "QuestionId": "8MAVC38",
            "QuestionData": {
                "question_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "New Question with Media"
                },
                "question_title": {
                    "text": "New Question with Medias"
                },
                "instruction_text": {
                    "text": "New Question with Media"
                },
                "correct_answer": "3",
                "choices": [{
                        "choiceid": 1,
                        "assess": false,
                        "text": "New Choice 2",
                        "correct_feedback": "Correct Feedback 1",
                        "incorrect_feedback": "Incorrect Feedback 1",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "New Choice 1",
                        "correct_feedback": "Correct Feedback 2",
                        "incorrect_feedback": "Incorrect Feedback 2",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": true,
                        "text": "New Choice",
                        "correct_feedback": "Correct Feedback 3",
                        "incorrect_feedback": "Incorrect Feedback 3",
                        "score": "",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": ""
                },
                "global_incorrect_feedback": {
                    "text": ""
                },
                "hints": [{
                        "hintid": 1,
                        "text": ""
                    }],
                "settings": {
                    "score": "1"
                },
                "media": {
                    "type": "assets",
                    "url": "http://i40.photobucket.com/albums/e213/cartman101/ssp68.gif",
                    "title": "Lighthouse.jpg",
                    "asset_type": "image"
                }
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC38",
            "OriginalQuestionId": "8MAVC38",
            "IsDeleted": false,
            "SelectedOptions": null
        }, {
            "QuestionId": "8MAVC31",
            "QuestionData": {
                "question_type": {
                    "text": "mcms"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "New Question With The Data?"
                },
                "question_title": {
                    "text": "New Question Test"
                },
                "instruction_text": {
                    "text": "Please answer the question"
                },
                "correct_answer": "1,3",
                "choices": [{
                        "choiceid": 1,
                        "assess": false,
                        "text": "Choice 1",
                        "correct_feedback": "Correct Feedback Data 1",
                        "incorrect_feedback": "Incorrect Feedback data 1",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Choice 2",
                        "correct_feedback": "Correct Feedback Data 2",
                        "incorrect_feedback": "Incorrect Feedback data 2",
                        "score": "0",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": false,
                        "text": "Choice 3",
                        "correct_feedback": "Correct Feedback Data 3",
                        "incorrect_feedback": "Incorrect Feedback data 3",
                        "score": "1",
                        "pinanswer": 0
                    }, {
                        "choiceid": 4,
                        "assess": false,
                        "text": "Choice 4",
                        "correct_feedback": "Correct Feedback Data 4",
                        "incorrect_feedback": "Incorrect Feedback data 4",
                        "score": "0",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct feedback data"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect feedback data"
                },
                "hints": [{
                        "hintid": 1,
                        "text": "Please check the hint for the question"
                    }, {
                        "hintid": 2,
                        "text": "You have to choose multiple answer to get the full  marks"
                    }],
                "settings": {
                    "score": "2"
                },
                "media": {
                    "type": "brightcove",
                    "brightcove_id": "5116441932001",
                    "url": "\/\/players.brightcove.net\/2402232200001\/default_default\/index.html?videoId=5116441932001",
                    "height": "660",
                    "width": "540"
                }
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC31",
            "OriginalQuestionId": "8MAVC31",
            "IsDeleted": false,
            "SelectedOptions": null
        }, {
            "QuestionId": "8MAVC30",
            "QuestionData": {
                "question_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "Hi data with the new question and question is here."
                },
                "question_title": {
                    "text": "This is The Tested Question Title"
                },
                "instruction_text": {
                    "text": "Please follow the instruction "
                },
                "correct_answer": "2",
                "choices": [{
                        "choiceid": 1,
                        "assess": false,
                        "text": "Choice 1",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": true,
                        "text": "Choice 2",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": false,
                        "text": "Choice 3",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }],
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [{
                        "hintid": 1,
                        "text": "This is the 1st Hint."
                    }, {
                        "hintid": 2,
                        "text": "Hello This is a hint."
                    }],
                "settings": {
                    "score": "1"
                },
                "media": {
                    "type": "no-media"
                }
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC30",
            "OriginalQuestionId": "8MAVC30",
            "IsDeleted": false,
            "SelectedOptions": null
        }, {
            "QuestionId": "8MAVC41",
            "QuestionData": {
                "question_type": {
                    "text": "open-ended"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "Openedned  Stem"
                },
                "question_title": {
                    "text": "New Openendeds"
                },
                "instruction_text": {
                    "text": "Instruction Text Instruction TextInstruction TextInstruction TextInstruction Text"
                },
                "response_type": {
                    "text": "without-response"
                },
                "answer": {
                    "text": "Hello Response"
                },
                "media": {
                    "type": "no-media"
                }
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC41",
            "OriginalQuestionId": "8MAVC41",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "8MAVC41",
            "QuestionData": {
                "question_type": {
                    "text": "open-ended"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "Openedned  Stem"
                },
                "question_title": {
                    "text": "New Openendeds"
                },
                "instruction_text": {
                    "text": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
                },
                "response_type": {
                    "text": "without-response"
                },
                "answer": {
                    "text": "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)."
                },
                "media": {
                    "type": "brightcove",
                    "brightcove_id": "5116441932001",
                    "url": "\/\/players.brightcove.net\/2402232200001\/default_default\/index.html?videoId=5116441932001",
                    "height": "660",
                    "width": "540"
                }
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC41",
            "OriginalQuestionId": "8MAVC41",
            "IsDeleted": false,
            "SelectedOptions": null
        }, {
            "QuestionId": "8MAVC34",
            "QuestionData": {
                "question_type": {
                    "text": "open-ended"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": '<math xmlns="http://www.w3.org/1998/Math/MathML"><mfrac><mn>1</mn><mn>2</mn></mfrac><msqrt><mn>5</mn></msqrt><mo>&#x2208;</mo><mo>&#x220B;</mo><mo>&#x2229;</mo><mo>&#x2282;</mo><mo>&#x2283;</mo><mi mathvariant="normal">&#x3C0;</mi><menclose notation="updiagonalstrike"><mn>5</mn><mstyle displaystyle="false"><munderover><mo>&#x2211;</mo><mn>2</mn><mn>1</mn></munderover></mstyle></menclose></math>'
                },
                "question_title": {
                    "text": "This is a test for Open Ended"
                },
                "instruction_text": {
                    "text": "This is a instructional text for Open Ended"
                },
                "response_type": {
                    "text": "with-response"
                },
                "grading_type": {
                    "text": "without-grading"
                },
                "sample_answer": {
                    "text": "Hello"
                },
                "feedback": {
                    "text": "Hello"
                },
                "media": {
                    "type": "assets",
                    "url": "http://techslides.com/demos/sample-videos/small.mp4",
                    "title": "Lighthouse.jpg",
                    "asset_type": "video"
                }
            },
            "VersionNo": "0",
            "DisplayQuestionId": "8MAVC34",
            "OriginalQuestionId": "8MAVC34",
            "IsDeleted": false,
            "SelectedOptions": null
        }]
}