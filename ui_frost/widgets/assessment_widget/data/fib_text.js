var advJsonData = {
    "quiz": [
        {
            "QuestionData": {
                "question_stem": {
                    "text": "<p>ONLY DROPDOWN</p>"
                },
                "question_title": {
                    "text": "Sample FIB"
                },
                "instruction_text": {
                    "text": "<p>Jack and 'jill' went up the 'hill' at '10' pm</p>"
                },
                "textwithblanks": {
                    "text": "<p>jack and [[1]] went up the [[2]] at [[3]] pm</p>"
                },
                "question_type": {
                    "text": "fib"
                },
                "choices": [
                    {
                        "assess": "",
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "jill"
              }
            ],
                        "distractorforeachblank": [{
                            "text": "mill"
                }, {
                            "text": "jill"
                }, {
                            "text": "gill"
                }],
                        "correct_feedback": "correct ans",
                        "incorrect_feedback": "incorrect ans",
                        "score": "0",
                        "interaction": "inline",
                        "inputalignment": "right",
                        "characters": "",
                        "inputtype": "alphabet",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 1,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>jill</span>"
            ],
                        "key": 0,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1027"
          },
                    {
                        "assess": "",
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "hill"
              }
            ],
                        "distractorforeachblank": [{
                            "text": "hell"
                }, {
                            "text": "hill"
                }, {
                            "text": "mountain"
                }],
                        "correct_feedback": "correct answer",
                        "incorrect_feedback": "incorrect answer",
                        "score": "0",
                        "interaction": "inline",
                        "inputalignment": "left",
                        "characters": "",
                        "inputtype": "alphabet",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 2,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>hill</span>"
            ],
                        "key": 1,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1028"
          },
                    {
                        "assess": "",
                        "noforeachblank": "[[3]]",
                        "textforeachblank": [
                            {
                                "text": "10"
              }
            ],
                        "distractorforeachblank": [{
                            "text": "10"
                }, {
                            "text": "20"
                }, {
                            "text": "30"
                }],
                        "correct_feedback": "correct ans",
                        "incorrect_feedback": "incorrect ans",
                        "score": "0",
                        "interaction": "inline",
                        "inputalignment": "left",
                        "characters": "5",
                        "inputtype": "numeric",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 3,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>10</span>"
            ],
                        "key": 2,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1029"
          }
        ],
                "global_correct_feedback": {
                    "text": "<p>correct1</p>"
                },
                "global_incorrect_feedback": {
                    "text": "<p>incorrect1</p>"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<p>no hint</p>"
          }
        ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": false
          }
        ],
                "settings": {
                    "score": "0"
                }
            },
            "VersionNo": 3,
            "DisplayQuestionId": "344123",
            "OriginalQuestionId": "4ef15c5ac60fa04aa99c4f5fbf5dd032",
            "IsDeleted": false,
            "SelectedOptions": null,
            "passage_id": 0,
            "passage_title": null,
            "passage_text": null,
            "CurrentQuestion": 1
    }, {
            "QuestionData": {
                "question_stem": {
                    "text": "<p>ONLY TEXT</p>"
                },
                "question_title": {
                    "text": "Sample FIB"
                },
                "instruction_text": {
                    "text": "<p>Jack and 'jill' went up the 'hill' at '10' pm</p>"
                },
                "textwithblanks": {
                    "text": "<p>jack and [[1]] went up the [[2]] at [[3]] pm</p>"
                },
                "question_type": {
                    "text": "fib"
                },
                "choices": [
                    {
                        "assess": "",
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "jill"
              }
            ],
                        "correct_feedback": "correct ans",
                        "incorrect_feedback": "incorrect ans",
                        "score": "0",
                        "interaction": "textentry",
                        "inputalignment": "right",
                        "characters": "",
                        "inputtype": "alphabet",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 1,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>jill</span>"
            ],
                        "key": 0,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1027"
          },
                    {
                        "assess": "",
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "hill"
              }
            ],
                        "distractorforeachblank": [{
                            "text": "hello"
                }, {
                            "text": "hill"
                }, {
                            "text": "bye"
                }],
                        "correct_feedback": "correct answer",
                        "incorrect_feedback": "incorrect answer",
                        "score": "0",
                        "interaction": "textentry",
                        "inputalignment": "left",
                        "characters": "",
                        "inputtype": "alphabet",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 2,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>hill</span>"
            ],
                        "key": 1,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1028"
          },
                    {
                        "assess": "",
                        "noforeachblank": "[[3]]",
                        "textforeachblank": [
                            {
                                "text": "10"
              }
            ],
                        "correct_feedback": "correct ans",
                        "incorrect_feedback": "incorrect ans",
                        "score": "0",
                        "interaction": "textentry",
                        "inputalignment": "left",
                        "characters": "5",
                        "inputtype": "numeric",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 3,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>10</span>"
            ],
                        "key": 2,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1029"
          }
        ],
                "global_correct_feedback": {
                    "text": "<p>correct2</p>"
                },
                "global_incorrect_feedback": {
                    "text": "<p>incorrect2</p>"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<p>no hint</p>"
          }
        ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": false
          }
        ],
                "settings": {
                    "score": "0"
                }
            },
            "VersionNo": 3,
            "DisplayQuestionId": "344123",
            "OriginalQuestionId": "4ef15c5ac60fa04aa99c4f5fbf5dd032",
            "IsDeleted": false,
            "SelectedOptions": null,
            "passage_id": 0,
            "passage_title": null,
            "passage_text": null,
            "CurrentQuestion": 1
    }, 
        {
      "QuestionData": {
        "question_stem": {
          "text": "<p>FIB DragDrop</p>"
        },
        "question_title": {
          "text": ""
        },
        "instruction_text": {
          "text": ""
        },
        "textwithblanks": {
          "text":  "<p>jack and [[1]] went up the [[2]] at [[3]] pm</p>"
        },
        "question_type": {
          "text": "fib"
        },
        "question_layout": {
          "text": "top"
        },
        "choices": [
          {
            "assess": "",
            "noforeachblank": "[[1]]",
            "textforeachblank": [
              {
                "text": "jill"
              },
                {
                "text": "mill"
              }
            ],
            "correct_feedback": "",
            "incorrect_feedback": "",
            "score": "0",
            "interaction": "dragdrop",
            "currentClass": "",
            "CurrentOptionIndex": 1,
            "displayfeedback": false,
            "arrList": [],
            "Canvas": [],
            "Drop": true,
            "key": 0,
            "UserInput": "",
            "AnswerClass": "",
            "width": "auto",
            "showOptionFeedbackModal": true,
            "showAttempt": true,
            "ShowGlobalFeedback": true,
            "multiSelectItem": [],
            "accept": true,
            "FibClass": "",
            "$$hashKey": "object:76"
          },
              {
            "assess": "",
            "noforeachblank": "[[2]]",
            "textforeachblank": [
              {
                "text": "hill"
              }
            ],
            "correct_feedback": "",
            "incorrect_feedback": "",
            "score": "0",
            "interaction": "dragdrop",
            "currentClass": "",
            "CurrentOptionIndex": 2,
            "displayfeedback": false,
            "arrList": [],
            "Canvas": [],
            "Drop": true,
            "key": 0,
            "UserInput": "",
            "AnswerClass": "",
            "width": "auto",
            "showOptionFeedbackModal": true,
            "showAttempt": true,
            "ShowGlobalFeedback": true,
            "multiSelectItem": [],
            "accept": true,
            "FibClass": "",
            "$$hashKey": "object:76"
          },
              {
            "assess": "",
            "noforeachblank": "[[3]]",
            "textforeachblank": [
              {
                "text": "10"
              }
            ],
            "correct_feedback": "dfgfdg",
            "incorrect_feedback": "dfgg",
            "score": "0",
            "interaction": "dragdrop",
            "currentClass": "",
            "CurrentOptionIndex": 3,
            "displayfeedback": false,
            "arrList": [],
            "Canvas": [],
            "Drop": true,
            "key": 0,
            "UserInput": "",
            "AnswerClass": "",
            "width": "auto",
            "showOptionFeedbackModal": true,
            "showAttempt": true,
            "ShowGlobalFeedback": true,
            "multiSelectItem": [],
            "accept": true,
            "FibClass": "",
            "$$hashKey": "object:76"
          }
        ],
        "distractors": [
          {
            "distractorid": 1,
            "text": "dist1"
          },
            {
            "distractorid": 2,
            "text": "dist2"
          },
            {
            "distractorid": 1,
            "text": "dist3"
          },
            {
            "distractorid": 2,
            "text": "dist4"
          },
            {
            "distractorid": 1,
            "text": "dist5"
          },
            {
            "distractorid": 2,
            "text": "dist6"
          }
        ],
        "global_correct_feedback": {
          "text": "ryy"
        },
        "global_incorrect_feedback": {
          "text": "ftytt"
        },
        "hints": [],
        "exhibit": [
          {
            "path": "",
            "exhibit_type": false
          }
        ],
        "settings": {
          "score": "0"
        }
      }
        },{
            "QuestionData": {
                "question_stem": {
                    "text": "<p>Sample FIB</p>"
                },
                "question_title": {
                    "text": "Sample FIB"
                },
                "instruction_text": {
                    "text":"<p>Jack and 'jill' went up the 'hill' at '10' pm</p>"
                },
                "textwithblanks": {
                    "text": "<p>jack and [[1]] went up the [[2]] at [[3]] pm for [[4]]</p>"
                },
                "question_type": {
                    "text": "fib"
                },
                "choices": [
                    {
                        "assess": "",
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "jill"
              }
            ],
                        "correct_feedback": "correct ans",
                        "incorrect_feedback": "incorrect ans",
                        "score": "0",
                        "interaction": "textentry",
                        "inputalignment": "right",
                        "characters": "",
                        "inputtype": "alphabet",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 1,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>jill</span>"
            ],
                        "key": 0,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1027"
          },
                    {
                        "assess": "",
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "hill"
              }
            ],
                        "distractorforeachblank": [{
                            "text": "hello"
                }, {
                            "text": "hill"
                }, {
                            "text": "bye"
                }],
                        "distractorforeachblank": [{
                            "text": "hello"
                }, {
                            "text": "hill"
                }, {
                            "text": "bye"
                }],
                        "correct_feedback": "correct answer",
                        "incorrect_feedback": "incorrect answer",
                        "score": "0",
                        "interaction": "inline",
                        "inputalignment": "left",
                        "characters": "",
                        "inputtype": "alphabet",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 2,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>hill</span>"
            ],
                        "key": 1,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1028"
          },
               
                    {
                        "assess": "",
                        "noforeachblank": "[[3]]",
                        "textforeachblank": [
                            {
                                "text": "10"
              }
            ],
                        "correct_feedback": "correct ans",
                        "incorrect_feedback": "incorrect ans",
                        "score": "0",
                        "interaction": "textentry",
                        "inputalignment": "left",
                        "characters": "5",
                        "inputtype": "numeric",
                        "decimalplace": "0",
                        "currentClass": "",
                        "CurrentOptionIndex": 3,
                        "displayfeedback": false,
                        "arrList": [
              "<span math dynamic>10</span>"
            ],
                        "key": 2,
                        "UserInput": "",
                        "AnswerClass": "",
                        "width": "auto",
                        "showOptionFeedbackModal": true,
                        "showAttempt": true,
                        "ShowGlobalFeedback": true,
                        "multiSelectItem": [],
                        "accept": true,
                        "FibClass": "fibclass",
                        "$$hashKey": "object:1029"
          },
                            {
            "assess": "",
            "noforeachblank": "[[4]]",
            "textforeachblank": [
              {
                "text": "icecream"
              },
                {
                "text": "toys"
              }
            ],
            "correct_feedback": "correct",
            "incorrect_feedback": "incorrect",
            "score": "0",
            "interaction": "dragdrop",
            "currentClass": "",
            "CurrentOptionIndex": 4,
            "displayfeedback": false,
            "arrList": [],
            "Canvas": [],
            "Drop": true,
            "key": 0,
            "UserInput": "",
            "AnswerClass": "",
            "width": "auto",
            "showOptionFeedbackModal": true,
            "showAttempt": true,
            "ShowGlobalFeedback": true,
            "multiSelectItem": [],
            "accept": true,
            "FibClass": "",
            "$$hashKey": "object:76"
          }
        ],
                 "distractors": [
          {
            "distractorid": 1,
            "text": "dist1"
          },
            {
            "distractorid": 2,
            "text": "dist2"
          },
            {
            "distractorid": 1,
            "text": "dist3"
          },
            {
            "distractorid": 2,
            "text": "dist4"
          },
            {
            "distractorid": 1,
            "text": "dist5"
          },
            {
            "distractorid": 2,
            "text": "dist6"
          }
        ],
                "global_correct_feedback": {
                    "text": "<p>correct3</p>"
                },
                "global_incorrect_feedback": {
                    "text": "<p>incorrect3</p>"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<p>no hint</p>"
          }
        ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": false
          }
        ],
                "settings": {
                    "score": "0"
                }
            },
            "VersionNo": 3,
            "DisplayQuestionId": "344123",
            "OriginalQuestionId": "4ef15c5ac60fa04aa99c4f5fbf5dd032",
            "IsDeleted": false,
            "SelectedOptions": null,
            "passage_id": 0,
            "passage_title": null,
            "passage_text": null,
            "CurrentQuestion": 1
    }
  ],
    "settings": {}
}