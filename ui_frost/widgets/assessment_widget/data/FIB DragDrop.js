var advJsonData = {
  "quiz": [
    {
      "QuestionData": {
        "question_stem": {
          "text": "<p>FIB DragDrop</p>"
        },
        "question_title": {
          "text": ""
        },
        "instruction_text": {
          "text": ""
        },
        "textwithblanks": {
          "text":  "<p>jack and [[1]] went up the [[2]] at [[3]] pm</p>"
        },
        "question_type": {
          "text": "fib"
        },
        "question_layout": {
          "text": "top"
        },
        "choices": [
          {
            "assess": "",
            "noforeachblank": "[[1]]",
            "textforeachblank": [
              {
                "text": "jill"
              },
                {
                "text": "mill"
              }
            ],
            "correct_feedback": "",
            "incorrect_feedback": "",
            "score": "0",
            "interaction": "dragdrop",
            "currentClass": "",
            "CurrentOptionIndex": 1,
            "displayfeedback": false,
            "arrList": [],
            "Canvas": [],
            "Drop": true,
            "key": 0,
            "UserInput": "",
            "AnswerClass": "",
            "width": "auto",
            "showOptionFeedbackModal": true,
            "showAttempt": true,
            "ShowGlobalFeedback": true,
            "multiSelectItem": [],
            "accept": true,
            "FibClass": "",
            "$$hashKey": "object:76"
          },
              {
            "assess": "",
            "noforeachblank": "[[2]]",
            "textforeachblank": [
              {
                "text": "hill"
              }
            ],
            "correct_feedback": "",
            "incorrect_feedback": "",
            "score": "0",
            "interaction": "dragdrop",
            "currentClass": "",
            "CurrentOptionIndex": 2,
            "displayfeedback": false,
            "arrList": [],
            "Canvas": [],
            "Drop": true,
            "key": 0,
            "UserInput": "",
            "AnswerClass": "",
            "width": "auto",
            "showOptionFeedbackModal": true,
            "showAttempt": true,
            "ShowGlobalFeedback": true,
            "multiSelectItem": [],
            "accept": true,
            "FibClass": "",
            "$$hashKey": "object:76"
          },
              {
            "assess": "",
            "noforeachblank": "[[3]]",
            "textforeachblank": [
              {
                "text": "10"
              }
            ],
            "correct_feedback": "dfgfdg",
            "incorrect_feedback": "dfgg",
            "score": "0",
            "interaction": "dragdrop",
            "currentClass": "",
            "CurrentOptionIndex": 3,
            "displayfeedback": false,
            "arrList": [],
            "Canvas": [],
            "Drop": true,
            "key": 0,
            "UserInput": "",
            "AnswerClass": "",
            "width": "auto",
            "showOptionFeedbackModal": true,
            "showAttempt": true,
            "ShowGlobalFeedback": true,
            "multiSelectItem": [],
            "accept": true,
            "FibClass": "",
            "$$hashKey": "object:76"
          }
        ],
        "distractors": [
          {
            "distractorid": 1,
            "text": "dist1"
          },
            {
            "distractorid": 2,
            "text": "dist2"
          },
            {
            "distractorid": 1,
            "text": "dist3"
          },
            {
            "distractorid": 2,
            "text": "dist4"
          },
            {
            "distractorid": 1,
            "text": "dist5"
          },
            {
            "distractorid": 2,
            "text": "dist6"
          }
        ],
        "global_correct_feedback": {
          "text": "ryy"
        },
        "global_incorrect_feedback": {
          "text": "ftytt"
        },
        "hints": [],
        "exhibit": [
          {
            "path": "",
            "exhibit_type": false
          }
        ],
        "settings": {
          "score": "0"
        }
      },
      "VersionNo": 0,
      "DisplayQuestionId": "0",
      "OriginalQuestionId": "",
      "IsDeleted": false,
      "SelectedOptions": null,
      "passage_id": 0,
      "passage_title": null,
      "passage_text": null,
      "CurrentQuestion": 1
    }
  ],
  "settings": {}
}