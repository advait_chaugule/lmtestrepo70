var advJsonData = {
    "quiz": [{
        "QuestionId": "6DN8J621",
        "QuestionData": {
            "question_type": {
                "text": "multi-part"
            },
            "question_interaction": {
                "text": "clickable"
            },
            "question_layout": {
                "text": "vertical"
            },
            "question_stem": {
                "text": "awerqwr"
            },
            "question_title": {
                "text": "Q1"
            },
            "instruction_text": {
                "text": "zfsdfd"
            },
            "subquestions": [{
                "question_type": {
                    "text": "in-context"
                },
                "question_response_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "sgfs"
                },
                "question_title": {
                    "text": "ICR1"
                },
                "instruction_text": {
                    "text": "gfd"
                },
                "sentences": [{
                    "sentence_id": 1,
                    "sentence": {
                        "text": "rtr"
                    },
                    "question_title": {
                        "text": "rtr"
                    },
                    "question_stem": {
                        "text": "rtr"
                    },
                    "instruction_text": {
                        "text": ""
                    },
                    "question_type": {
                        "text": "mcss"
                    },
                    "question_interaction": {
                        "text": "clickable"
                    },
                    "question_layout": {
                        "text": "vertical"
                    },
                    "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": "dg",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "dfg",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": false,
                        "text": "dfg",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }],
                    "correct_answer": "1",
                    "global_correct_feedback": {
                        "text": ""
                    },
                    "global_incorrect_feedback": {
                        "text": ""
                    },
                    "media": {
                        "type": "no-mdeia"
                    }
                }],
                "media": {
                    "type": "no-media"
                }
            }, {
                "question_type": {
                    "text": "in-context"
                },
                "question_response_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "Updated Again System Testing team is responsible for <br\/>a) Performing the data validations <br\/>b) Performing the Usability Testing <br\/>c) Performing the Beta Testing <br\/>d) None of the above"
                },
                "question_title": {
                    "text": "IRS Questions"
                },
                "instruction_text": {
                    "text": "Please check"
                },
                "sentences": [{
                    "sentence_id": 1,
                    "sentence": {
                        "text": "Testing Process comprised of\na) Test Plan and Test Cases\nb) Test log and Test Status\nc) Defect Tracking\nd) All of the above"
                    },
                    "question_title": {
                        "text": "Testing Process comprised of\na) Test Plan and Test Cases\nb) Test log and Test Status\nc) Defect Tracking\nd) All of the above"
                    },
                    "question_stem": {
                        "text": "Testing Process comprised of\na) Test Plan and Test Cases\nb) Test log and Test Status\nc) Defect Tracking\nd) All of the above"
                    },
                    "instruction_text": {
                        "text": ""
                    },
                    "question_type": {
                        "text": "mcss"
                    },
                    "question_interaction": {
                        "text": "clickable"
                    },
                    "question_layout": {
                        "text": "vertical"
                    },
                    "choices": [{
                        "choiceid": 1,
                        "assess": false,
                        "text": "CCC",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "BBB",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": true,
                        "text": "AAA",
                        "correct_feedback": "CF1",
                        "incorrect_feedback": "IF1",
                        "score": "",
                        "pinanswer": 0
                    }],
                    "correct_answer": "3",
                    "global_correct_feedback": {
                        "text": ""
                    },
                    "global_incorrect_feedback": {
                        "text": ""
                    },
                    "media": {
                        "type": "no-mdeia"
                    }
                }, {
                    "sentence_id": 2,
                    "sentence": {
                        "text": "Smoke Testing\na) To find whether the hardware burns out\nb) Same as build verification test\nc) To find that software is stable\nd) None of the above"
                    },
                    "question_title": {
                        "text": "Smoke Testing\na) To find whether the hardware burns out\nb) Same as build verification test\nc) To find that software is stable\nd) None of the above"
                    },
                    "question_stem": {
                        "text": "Smoke Testing\na) To find whether the hardware burns out\nb) Same as build verification test\nc) To find that software is stable\nd) None of the above"
                    },
                    "instruction_text": {
                        "text": ""
                    },
                    "question_type": {
                        "text": "mcss"
                    },
                    "question_interaction": {
                        "text": "clickable"
                    },
                    "question_layout": {
                        "text": "vertical"
                    },
                    "choices": [{
                        "choiceid": 1,
                        "assess": false,
                        "text": "C",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "B",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": true,
                        "text": "A",
                        "correct_feedback": "CF2",
                        "incorrect_feedback": "IF2",
                        "score": "",
                        "pinanswer": 0
                    }],
                    "correct_answer": "3",
                    "global_correct_feedback": {
                        "text": ""
                    },
                    "global_incorrect_feedback": {
                        "text": ""
                    },
                    "media": {
                        "type": "no-mdeia"
                    }
                }, {
                    "sentence_id": 3,
                    "sentence": {
                        "text": "Test Plan\na) Road map for testing\nb) Tells about the acutal results and expected results\nc) Both a and b\nd) None of the above"
                    },
                    "question_title": {
                        "text": "Test Plan\na) Road map for testing\nb) Tells about the acutal results and expected results\nc) Both a and b\nd) None of the above"
                    },
                    "question_stem": {
                        "text": "Test Plan\na) Road map for testing\nb) Tells about the acutal results and expected results\nc) Both a and b\nd) None of the above"
                    },
                    "instruction_text": {
                        "text": ""
                    },
                    "question_type": {
                        "text": "mcss"
                    },
                    "question_interaction": {
                        "text": "clickable"
                    },
                    "question_layout": {
                        "text": "vertical"
                    },
                    "choices": [{
                        "choiceid": 1,
                        "assess": false,
                        "text": "cc",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "bb",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": true,
                        "text": "aa",
                        "correct_feedback": "CF3",
                        "incorrect_feedback": "IF3",
                        "score": "",
                        "pinanswer": 0
                    }],
                    "correct_answer": "3",
                    "global_correct_feedback": {
                        "text": ""
                    },
                    "global_incorrect_feedback": {
                        "text": ""
                    },
                    "media": {
                        "type": "no-mdeia"
                    }
                }, {
                    "sentence_id": 4,
                    "sentence": {
                        "text": "7.Test Script\na) written version of test cases\nb) Code used in manual testing\nc) Always used when we use tools\nd) A code segment to replace the test case"
                    },
                    "question_title": {
                        "text": "7.Test Script\na) written version of test cases\nb) Code used in manual testing\nc) Always used when we use tools\nd) A code segment to replace the test case"
                    },
                    "question_stem": {
                        "text": "7.Test Script\na) written version of test cases\nb) Code used in manual testing\nc) Always used when we use tools\nd) A code segment to replace the test case"
                    },
                    "instruction_text": {
                        "text": ""
                    },
                    "question_type": {
                        "text": "mcss"
                    },
                    "question_interaction": {
                        "text": "clickable"
                    },
                    "question_layout": {
                        "text": "vertical"
                    },
                    "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": "ghf",
                        "correct_feedback": "CF3",
                        "incorrect_feedback": "ICF3",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "gfh",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": false,
                        "text": "hgfh",
                        "correct_feedback": "",
                        "incorrect_feedback": "",
                        "score": "",
                        "pinanswer": 0
                    }],
                    "correct_answer": "1",
                    "global_correct_feedback": {
                        "text": ""
                    },
                    "global_incorrect_feedback": {
                        "text": ""
                    },
                    "media": {
                        "type": "no-mdeia"
                    }
                }, {
                    "sentence_id": 5,
                    "sentence": {
                        "text": "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden floFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden floFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flo"
                    },
                    "question_title": {
                        "text": "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden floFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden floFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flo"
                    },
                    "question_stem": {
                        "text": "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden floFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden floFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flo"
                    },
                    "instruction_text": {
                        "text": ""
                    },
                    "question_type": {
                        "text": "mcss"
                    },
                    "question_interaction": {
                        "text": "clickable"
                    },
                    "question_layout": {
                        "text": "vertical"
                    },
                    "choices": [{
                        "choiceid": 1,
                        "assess": true,
                        "text": "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden floFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flo",
                        "correct_feedback": "CF4",
                        "incorrect_feedback": "IF4",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden floFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flo",
                        "correct_feedback": "CF2",
                        "incorrect_feedback": "IF2",
                        "score": "",
                        "pinanswer": 0
                    }, {
                        "choiceid": 3,
                        "assess": false,
                        "text": "Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden floFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flo",
                        "correct_feedback": "CF3",
                        "incorrect_feedback": "IF3",
                        "score": "",
                        "pinanswer": 0
                    }],
                    "correct_answer": "1",
                    "global_correct_feedback": {
                        "text": ""
                    },
                    "global_incorrect_feedback": {
                        "text": ""
                    },
                    "media": {
                        "type": "no-mdeia"
                    }
                }],
                "media": {
                    "type": "assets",
                    "url": "http:\/\/d3cfjqg4xt8ryh.cloudfront.net\/frostb11\/assets\/images\/qxbt3c7b0a091566141a18d262cf6965\/qxbt3c7b0a091566141a18d262cf6965_Far_far_away,_behind_the_word_mountains,_far_from_the_countries_Vokalia_and_Consonantia,_there_live_the_blind_texts._Separated_they_live_in_Bookmarksgrove_right_at_the_coast_of_the_Semantics,_a_large_language_ocean._A_small_river_named_Duden_flo.jpg",
                    "title": "Far_far_away,_behind_the_word_mountains,_far_from_the_countries_Vokalia_and_Consonantia,_there_live_the_blind_texts._Separated_they_live_in_Bookmarksgrove_right_at_the_coast_of_the_Semantics,_a_large_language_ocean._A_small_river_named_Duden_flo.jpg",
                    "asset_type": "image"
                }
            }, {
                "question_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_stem": {
                    "text": "What is FROST?????"
                },
                "question_title": {
                    "text": "MCQ1 without media"
                },
                "instruction_text": {
                    "text": "Please answer."
                },
                "correct_answer": "1",
                "choices": [{
                    "choiceid": 1,
                    "assess": true,
                    "text": "AAA",
                    "correct_feedback": "CF1",
                    "incorrect_feedback": "IF1",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 2,
                    "assess": false,
                    "text": "BBB",
                    "correct_feedback": "CF2",
                    "incorrect_feedback": "IF2",
                    "score": "",
                    "pinanswer": 0
                }, {
                    "choiceid": 3,
                    "assess": false,
                    "text": "CCC",
                    "correct_feedback": "CF3",
                    "incorrect_feedback": "IF3",
                    "score": "",
                    "pinanswer": 0
                }],
                "global_correct_feedback": {
                    "text": "GCF"
                },
                "global_incorrect_feedback": {
                    "text": "GIF"
                },
                "hints": [{
                    "hintid": 1,
                    "text": ""
                }],
                "settings": {
                    "score": ""
                },
                "media": {
                    "type": "no-media"
                }
            }],
            "global_correct_feedback": {
                "text": "GCF1"
            },
            "global_incorrect_feedback": {
                "text": "GIF1"
            },
            "media": {
                "type": "no-media"
            }
        },
        "VersionNo": "0",
        "DisplayQuestionId": "6DN8J621",
        "OriginalQuestionId": "6DN8J621",
        "IsDeleted": false,
        "SelectedOptions": null
    }]
}