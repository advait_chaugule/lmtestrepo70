/** 
 *Created by tarique hussain
 */
import MCQ from "./mcq";
export default class Fib extends MCQ {
    constructor(assessmentData) {
        super();
        this.assessmentData = assessmentData;
        this.elementreference = null;
        this.choices = this.assessmentData.getFibAnswers();
    }
    bindEvents() {
        super.bindEvents();
        //        $(this.elementreference).off("keypress", "input").on("keypress", "input", this.validateInputs.bind(this));
        //        $(this.elementreference).off("change", "select").on("change", "select", this.enableCheckBtn.bind(this));
        $(this.elementreference).off("change", "select").on("change", "select", (e, t) => {
            let elm = window.event ? window.event.target : (e) ? e.target : '';
            if (elm.value !== "") {
                $(elm).addClass("dirty");
            } else {
                $(elm).removeClass("dirty");
            }

            let index = elm.getAttribute('ansindex') - 1;
            this.assessmentData.setFibValues(elm.value, index);
            if(elm.value.length>0){
                  this.assessmentData.setDirtyClass('dirty', $(elm).attr('ansIndex')-1);
            }
            else{
            this.assessmentData.setDirtyClass(' ', $(elm).attr('ansIndex')-1);
            }
            this.assessmentData.setFibQuestions(this.assessmentData.getFibRawText());
           
            this.enableCheckBtn(event);
        });
        $(this.elementreference).off("keydown input").on("keydown input", (e, t) => {
            let charCode = (window.event) ? window.event.keyCode : (e) ? e.which : "",
                inputType = $(event.target).attr('inputType'),
                elm = event.target;
            if (charCode == 8 || charCode == 9) {
                this.checkEmptyVal(event);
                //return false;
            } else {
                switch (inputType) {
                case 'numeric':
                    {
                        if (charCode >= 48 && charCode <= 57) {
                            $(elm).addClass("dirty");
                            //this.assessmentData.setDirtyClass($(elm).val(), $(elm).attr('answerIndex'));
                        } else {
                            this.checkEmptyVal(event);
                            return false;
                        }
                        break;
                    }
                case 'alphanumeric':
                    {
                        if ((charCode >= 48 && charCode <= 57) ||
                            (charCode >= 65 && charCode <= 90) ||
                            (charCode >= 97 && charCode <= 122)) {
                            $(elm).addClass("dirty");
                            //this.assessmentData.setDirtyClass($(elm).val(), $(elm).attr('answerIndex'));
                        } else {
                            this.checkEmptyVal(event);
                            return false;
                        }
                        break;
                    }
                case 'alphabet':
                    {
                        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) {
                            $(elm).addClass("dirty");
                            //this.assessmentData.setDirtyClass($(elm).val(), $(elm).attr('answerIndex'));
                        } else {
                            this.checkEmptyVal(event);
                            return false;
                        }
                        break;
                    }
                default:
                    return true;
                }
            }
            let index = elm.getAttribute('ansindex') - 1;
            this.assessmentData.setFibValues(elm.value, index);
            this.assessmentData.setFibQuestions(this.assessmentData.getFibRawText());
            return this.enableCheckBtn(event);
        });

        $(this.elementreference).off("blur input").on("blur input", (e, t) => {
            let elm = window.event ? window.event.target : (e) ? e.target : '';
            let index = elm.getAttribute('ansindex') - 1;
            this.assessmentData.setFibValues(elm.value, index);
            if(elm.value.length>0){
                  this.assessmentData.setDirtyClass('dirty', $(elm).attr('ansIndex')-1);
            }
            else{
            this.assessmentData.setDirtyClass(' ', $(elm).attr('ansIndex')-1);
            }
            this.assessmentData.setFibQuestions(this.assessmentData.getFibRawText());
          
        });
    }
    checkEmptyVal(evt) {
        if ($(evt.target).val().length === 1 && $(evt.target).hasClass("dirty") && evt.keyCode === 8) {
            $(evt.target).removeClass("dirty");
            this.enableCheckBtn(evt);
        }
    }
    selectAnswers() { //method used to add a styles to options
    }
    updateSelectedOptions() {}

    checkCorrectAnswer() { //method used to validate the correct & incorrect answer
        $(this.elementreference).find('.commonClass').each((index, item) => {
            let currentOption = Object.values(this.choices[$(item).attr('ansindex') - 1][0]);
            $(item).attr('disabled', true);
            if ($.inArray($(item).val(), currentOption) !== -1) {
                $(item).parent().addClass('rightanswer');

                this.assessmentData.setFibClasses('rightanswer');
                this.assessmentData.setFibValues($(item).val());


            } else {
                $(item).parent().addClass('wronganswer');
                this.assessmentData.setFibClasses('wronganswer');

            }

            this.assessmentData.setDisableProp('disabled');
            this.assessmentData.setFibQuestions(this.assessmentData.getFibRawText());

        })
    }
    enableCheckBtn(event) {
        if ($('.commonClass').length === $('.dirty').length) {
            $("#checkAnswer").attr('disabled', false);
        } else {
            $("#checkAnswer").attr('disabled', true);
        }
        return true;
    }
    render(pageIndex, type, templateName) { // method returns the desired HTML which then gets rendered.
        let contentAreaClass = '';
        this.wrongAnswerCounter = 0;
        this.rightAnswerCounter = 0;
        let idValue = type + pageIndex;
        this.elementreference = "#" + idValue;
        if (type === 'multipart') {
            pageIndex = pageIndex.replace("_", ".");
        }
        this.assessmentData.checkedOptionArray(this.elementreference);
        let list = `<section class="contentArea contentAreaClass" id=${idValue}>
                    <fieldset><legend>
                    <section  class="question">
                    <div class="stem">
                    <div class="qst" data-cnfig="${this.assessmentData.getChoices()}">${pageIndex}: ${this.assessmentData.getQuestionText()}</div>
                    </div>
                    </section>
                    <div class="instruction">${this.assessmentData.getInstructionText()}</div>
                    </legend>
                    <section class="activity">
                    <div id="media"</div>
                    <div class="options">${this.assessmentData.getFibQuestionText()}</div>
                    <div class="clearfix"></div>
                    </section>
                    </fieldset>`;
        list += `</section>`;
        return list;
    }
    getChoiceList(pageIndex, ref) { //method build the radio button html list
    }
    retry() {
        $(this.elementreference).find('.fibclass').removeClass('wronganswer');
        $(this.elementreference).find('.fibclass').removeClass('rightanswer');
        $(this.elementreference).find('.commonClass').attr('disabled', false).removeClass('dirty');
        $(this.elementreference).find('.commonClass').val("");
        this.assessmentData.setDisableProp('enabled');
        this.assessmentData.setFibClasses('empty');
        this.assessmentData.setFibValues('empty');
        this.assessmentData.setFibQuestions(this.assessmentData.getFibRawText());

    }
}