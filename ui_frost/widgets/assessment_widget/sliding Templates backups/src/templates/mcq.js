/*created by tarique*/
import MainActivity from './mainActivity';
export default class MCQ extends MainActivity {
    constructor() {
        super();
        if (this.constructor === MCQ) {
            throw new Error("Abstract class can not be instantiated");
        }
        this.pageIndex = 1;
    }
    bindEvents() { //method binds click events checkbox & radio button
        $('.feedBackIcon').off().on('click keypress', function (e) {
            if (e.keyCode === 13 || e.type === "click") {
                let feedBackText = [];
               
                feedBackText = $(this).parent().text().split('|#@~');
                if ($(this).parents('.rightanswer').length) {
                    $('#modalBodyPanel').html(feedBackText[0]).attr('role', 'alert');
                    $('#myModal').modal('show');
                } else if ($(this).parents('.wronganswer').length) {
                    $('#modalBodyPanel').html(feedBackText[1]).attr('role', 'alert');
                    $('#myModal').modal('show');
                }
            }
        });
    }
    retry() { //method allows user to retry for a question
        this.assessmentData.resetPinanswer();
        this.assessmentData.resetSelectedOption();
        this.assessmentData.setTextAreaAriaDisabled('false');
        this.assessmentData.setTextAreaAttr('');
        $(this.elementreference).find('input[type="radio"]').attr('aria-disabled', 'false').prop('disabled', false);
        $(this.elementreference).find('input[type="radio"]').prop('disabled', false);
        $(this.elementreference).find('input[type="checkbox"]').prop('disabled', false);
        $(this.elementreference).find("#checkAnswer,#checkAnswerMobile").attr('disabled', true);
        $(this.elementreference).find('input[type="radio"]:checked').prop('checked', false);
        $(this.elementreference).find('input[type="checkbox"]:checked').prop('checked', false);
        $(this.elementreference).find('input[type="radio"]').parents('.option').removeClass('selected wronganswer rightanswer');
        $(this.elementreference).find('input[type="checkbox"]').parents('.option').removeClass('selected wronganswer rightanswer');
    }
    isItemChecked(item, ref) { //method for checking the item is already selected or not
        if (this.assessmentData.selectedOptions[ref] === undefined) {
            return false;
        }
        if (this.assessmentData.selectedOptions[ref].length === 0) {
            return item.pinanswer === 1 ? "checked" : "";
        }
        return this.assessmentData.selectedOptions[ref].indexOf(String(item.choiceid)) > -1 ? "checked" : "";
    }
    isCorrectFedbckVisible(wrongAnswerCounter, rightAnswerCounter) { //method decide the flag to show the correct feedback panel
        if ((wrongAnswerCounter === 0 && rightAnswerCounter === 0) || (wrongAnswerCounter > 0) || (rightAnswerCounter !== this.assessmentData.getCorrectAnswers().length)) {
            return;
        }
        return 'show';
    }
    isInCorrectFedbckVisible(wrongAnswerCounter, rightAnswerCounter) { //method decide the flag to show the incorrect feedback panel
        if ((wrongAnswerCounter > 0) && (rightAnswerCounter !== this.assessmentData.getCorrectAnswers().length)) {
            return 'show';
        }
        if (wrongAnswerCounter === 0 && rightAnswerCounter === 0) {
            return;
        }
        return;
    }
    updateSelectedOptions() { //method for updating selected option
        let selectedInput = $(this.elementreference).find('input:checked');
        for (let i = 0; i < selectedInput.length; i++) {
            this.assessmentData.addSelectedOption(this.elementreference, $(this.elementreference).find(selectedInput[i]).attr("id").split('checkbox')[1]);
        }
    }
    checkItemAttemptState(item, ref) { //method return the classname for already selected right or wrong answer
        if (this.assessmentData.selectedOptions[ref] === undefined) {
            return;
        }
        if (this.assessmentData.selectedOptions[ref].length === 0 && item.pinanswer === 1) {
            return "selected";
        }
        if (this.assessmentData.selectedOptions[ref].length > 0) {
            return this.submittedCheckedItemState(item, ref);
        }
    }
    submittedCheckedItemState(item, ref) {
        if (this.assessmentData.selectedOptions[ref].indexOf(String(item.choiceid)) === -1) {
            return;
        }
        if (this.assessmentData.selectedOptions[ref].indexOf(String(item.choiceid)) > -1 && item.assess === true) {
            this.rightAnswerCounter++;
            return "rightanswer";
        }
        if (this.assessmentData.selectedOptions[ref].indexOf(String(item.choiceid)) > -1 && item.assess === false) {
            this.wrongAnswerCounter++;
            return "wronganswer";
        }
    }
    addGlobalLabelFeedBack(type) { //method return the global correct & incorrect feedback html
        if (type === 'multipart') {
            return "";
        }
        let globalFeedbackHtml = '';
        if (this.assessmentData.getGlobalCorrectFeedback().replace(/^\s+|\s+$/gm, '') !== "") {
            globalFeedbackHtml += `<p class="alert alert-success ${this.isCorrectFedbckVisible(this.wrongAnswerCounter,this.rightAnswerCounter)}" >${this.assessmentData.getGlobalCorrectFeedback()}</p>`;
        }
        if (this.assessmentData.getGlobalInCorrectFeedback().replace(/^\s+|\s+$/gm, '') !== "") {
            globalFeedbackHtml += `<p class="alert alert-danger ${this.isInCorrectFedbckVisible(this.wrongAnswerCounter,this.rightAnswerCounter)}" >${this.assessmentData.getGlobalInCorrectFeedback()}</p>`;
        }
        return globalFeedbackHtml;
    }
    buttonSetterForIncontext() {
        $("#submitIncontext").prop('disabled', false);
    }
}