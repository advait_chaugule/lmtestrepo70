/*
 *created by tarique hussain on 10.20.16 */

import MainActivity from './mainActivity';
export default class OpenWithResponse extends MainActivity {
    constructor(assessmentData) {
        super();
        this.assessmentData = assessmentData;
        this.elementreference = null;
    }
    bindEvents() { //binds events to buttons presnt in DOM
        $(this.elementreference).off("click", "#toggler2").on("click", "#toggler2", this.checkCorrectAnswer.bind(this));
        $(this.elementreference).off("blur", "#responseArea").on("blur", "#responseArea", this.textInteractivity.bind(this));
        $(this.elementreference).off("keyup", "#responseArea").on("keyup", "#responseArea", this.disableSubmitButton.bind(this));

    }
    render(pageIndex, type) { // method returns the desired HTML which then gets rendered.
        let idValue = "mcq_" + pageIndex;
        this.elementreference = "#" + idValue;
        if (type === 'multipart') {
            pageIndex = pageIndex.replace("_", ".");
        }
        let list = `<section class="contentArea" id=${idValue}>
                   
                    <section  class="question">
                    <div class="stem">
                    <div class="qst">${pageIndex}: ${this.assessmentData.getQuestionText()}</div>
                    </div>
                    </section>
                    <div  class="instruction">${this.assessmentData.getInstructionText()}</div>
                    <section class="activity">
                    <div id="media" class=${this.mediaClass()}>${this.loadMedia()}</div>
                    <form>
                    <div class="form-group">
                    <textarea aria-disabled=${this.assessmentData.getTextAreaAriaDisabled()} tabindex="${this.assessmentData.getTabindex()}" class="form-control" ${this.assessmentData.getTextAreaAttr()} rows="5" id="responseArea" title="${this.assessmentData.getQuestionText()}">${this.assessmentData.getResponseText()}</textarea>
                    </div>
                    </form>
                    <button id="toggler2" class="btn btn-primary submitBtns ${this.assessmentData.getOpenEndedClass()}" ${this.assessmentData.getdisabledClass()}>Submit</button>
                    <p class="${this.assessmentData.getSampleAnswerClass()} openFeedback headingClass"><strong>Feedback:</strong><br>${this.assessmentData.getFeedback()}</p>
                    <p  class="${this.assessmentData.getSampleAnswerClass()} headingClass"><strong>Sample Answer:</strong><br>${this.assessmentData.getSampleAnswer()}</p>
                    <div class="clearfix"></div>
                    </section>
                  
                    </section>`;
        return list;
    }
    textInteractivity() { //method extracts the text from the text area
        this.assessmentData.setResponseText($('#responseArea').val());
    }
    checkCorrectAnswer() { //method submits the form and disables the text area
        this.assessmentData.setDisabledClass('');
        this.assessmentData.setOpenEndedClass('hideBtn');
        this.assessmentData.setSampleAnswerClass('showSampleAnswer');
        this.assessmentData.setTextAreaAttr('disabled');
        this.assessmentData.setTextAreaAriaDisabled('true');
        this.assessmentData.setTabindex('-1');
        $('.headingClass').eq(0).attr('role', 'alert').show();
        $('.headingClass').eq(1).show();
        $('#responseArea').prop('disabled', true);
        $('#responseArea').attr('aria-disabled', "true");
        $('#toggler2').hide();
        return true;
    }
    disableSubmitButton() {
        if ($("#responseArea").val() === "") {
            $('#toggler2').prop('disabled', true);
        } else {
            $('#toggler2').prop('disabled', false);
        }
    }
}