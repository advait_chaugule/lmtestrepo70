/*created by tarique*/
export default class MainActivity {
    constructor() {
        if (this.constructor === MainActivity) {
            throw new Error("Abstract class can not be instantiated");
        }
    }
    loadMedia() { //returns an iframe with the media 
        let frameHtml = "",
            mediaUrl = this.assessmentData.getMediaParameters('url'),
            mediaType = this.assessmentData.getMediaParameters('type'),
            assetType = this.assessmentData.getMediaParameters('assetType');
        if (mediaUrl !== "") {
            switch (assetType) {
            case null:
                frameHtml += `<iframe title='brightcove' class="mediaLoader" frameborder="0" src=${mediaUrl} height=${this.assessmentData.getMediaParameters('height')} width=${this.assessmentData.getMediaParameters('width')} allowfullscreen="allowfullscreen" webkitallowfullscreen="allowfullscreen" mozallowfullscreen="allowfullscreen"  style="max-width: 100%"></iframe>`;
                break;
            case 'image':
                frameHtml += `<img tabindex=0 alt='image' src=${mediaUrl} >`;
                break;
            case 'video':
                frameHtml += `<video tabindex=0 alt='video' width="400" controls>
       <source src=${mediaUrl}  type="video/mp4">
       </video>`;
                break;
            }
        }
        return frameHtml;
    }
    mediaClass() { //method for getting the class related to media
        if (this.assessmentData.getMediaParameters('url') !== "") {
            switch (this.assessmentData.getMediaParameters('assetType')) {
            case null:
                return 'media';
                break;
            default:
                return 'resizer media';
            }
        }
    }

}