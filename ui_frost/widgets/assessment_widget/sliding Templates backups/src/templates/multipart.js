/*
 *created by tarique hussain on 10.20.16 
 */

import templateProvider from './template_provider';
import MainActivity from './mainActivity';
export default class Multipart extends MainActivity {
    constructor(assessmentData) {
        super();
        this.assessmentData = assessmentData;
        this.subQuestions = this.getSubQuestions(this.assessmentData);
        this.elementreference = null;

    }
    getSubQuestions(assessmentData) {
        let subQuestionArray = [];
        for (let i = 0; i < assessmentData.subQuestions.length; i++) {
            subQuestionArray.push(templateProvider.get(assessmentData.subQuestions[i]))
        }
        return subQuestionArray;
    }
    bindEvents() { //binds events to button present in DOM
        $('body').off("click", "#retryMultipart").on("click", "#retryMultipart", this.retry.bind(this));
        $('body').off("click", "#submitMultipart").on("click", "#submitMultipart", this.checkAnswer.bind(this));
        for (let i = 0; i < this.subQuestions.length; i++) {
            this.subQuestions[i].bindEvents();
        }
        $(".submitBtns").hide();
        $("#checkAnswer,#checkAnswerMobile").hide();
    }
    render(pageIndex) { // method returns the desired HTML which then gets rendered.
        let idValue = "mcq_" + pageIndex
        this.elementreference = "#" + idValue;
        let list = `<section class="contentArea" id=${idValue}>
                    <fieldset class="multi-part-main"><legend>
                    <section class="question">
                    <div class="stem">
                    <div class="qst">${pageIndex}: ${this.assessmentData.getQuestionText()}</div>
                    </div>
                    </section>
                    <div class="instruction">${this.assessmentData.getInstructionText()}</div>
                    </legend>
                    <section class="activity">
                    <div id="media" class=${this.mediaClass()}>${this.loadMedia()}</div>
                    <div class="clearfix"></div>
                    </section>
                    </fieldset>
                    ${this.renderAllSubTemplates(pageIndex)}<button id="submitMultipart" class="btn btn-primary multipartBtns ${this.assessmentData.getMultipartBtnClass()}">Submit</button>
<button id="retryMultipart" class="btn btn-primary multipartBtns ${this.assessmentData.getMultipartRetryClass()}" >Retry</button>
 <p id="correct"  class="${this.assessmentData.getMultipartFeedBackClassCorrect()} alert alert-success"><strong>Feedback:</strong><br>${this.assessmentData.getGlobalCorrectFeedback()}</p>
<p id="incorrect" class="${this.assessmentData.getMultipartFeedBackClassIncorrect()} alert alert-danger"><strong>Feedback:</strong><br>${this.assessmentData.getGlobalInCorrectFeedback()}</p></section>`;
        return list;
    }
    renderAllSubTemplates(pageIndex) {
        let html = '',
            subQuestion;
        this.subQuestions.map(function (elem, index) {
            html += elem.render(pageIndex + "_" + (index + 1), 'multipart');
        });
        return html;
    }
    checkAnswer() {
        let isAnswerGiven = [],
            that = this;
        $("#retryMultipart").show();
        $("#submitMultipart").hide();
        $(".inputClass").addClass('pointerEvent');
        this.assessmentData.setMultipartRetryBtnClass('showAnswer');
        this.assessmentData.setMultipartBtnClass('hideAnswer');
        this.subQuestions.map(function (elem) {
            isAnswerGiven.push(elem.checkCorrectAnswer('multipart'));
            if (elem.assessmentData.type != 'open-ended') {
                elem.updateSelectedOptions();
            }
        });
        that.checkGlobalFeedback(isAnswerGiven);
    }
    checkGlobalFeedback(isAnswerGiven) {
        if (isAnswerGiven.indexOf(false) === -1 && $(".wronganswer").length === 0) {
            $('p').removeAttr('role')
            $("#correct").attr('role', 'alert').show();
            $("#incorrect").hide();
            if (this.assessmentData.getGlobalCorrectFeedback() === "") {
                this.assessmentData.setMultipartFeedBackClassCorrect('hideAnswer');
                $("#correct").hide();
                return false;
            }
            this.assessmentData.setMultipartFeedBackClassCorrect('showAnswer');
            this.assessmentData.setMultipartFeedBackClassIncorrect('hideAnswer');

        } else {
            if (this.assessmentData.getGlobalInCorrectFeedback() === "") {
                this.assessmentData.setMultipartFeedBackIncorrect('hideAnswer');
                $("#incorrect").hide();
                return false;
            }
            this.assessmentData.setMultipartFeedBackClassIncorrect('showAnswer');
            this.assessmentData.setMultipartFeedBackClassCorrect('hideAnswer');
            $('p').removeAttr('role')
            $("#incorrect").attr('role', 'alert').show();
            $("#correct").hide();
        }
    }
    retry() {
        $('p').removeAttr('role')
        this.assessmentData.setMultipartFeedBackClassIncorrect('hideAnswer');
        this.assessmentData.setMultipartFeedBackClassCorrect('hideAnswer');
        $('#modalBodyPanel').html('');
        $(".inputClass").removeClass('pointerEvent');
        $('.alert').hide().removeClass('show');
        $("#retryMultipart").hide();
        $("#submitMultipart").show();
        this.assessmentData.setMultipartRetryBtnClass('hideAnswer');
        this.assessmentData.setMultipartBtnClass('showAnswer');
        this.subQuestions.map(function (elem) {
            if (elem.assessmentData.type != 'open-ended') {
                elem.retry('multipart');
                elem.updateSelectedOptions();
            }
        });
    }
}