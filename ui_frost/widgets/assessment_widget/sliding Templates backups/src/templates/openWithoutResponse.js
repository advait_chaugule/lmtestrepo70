/*
 *created by tarique hussain on 10.20.16 
 */

import MainActivity from './mainActivity';
export default class OpenWithoutResponse extends MainActivity {
    constructor(assessmentData) {
        super();
        this.assessmentData = assessmentData;
          this.elementreference = null;
    }
    bindEvents() {//binds events to button present in DOM
         $(this.elementreference).off("click", "#toggler").on("click", "#toggler", this.checkCorrectAnswer.bind(this));
        
    }
    render(pageIndex,type) { // method returns the desired HTML which then gets rendered.
        let idValue = "mcq_" + pageIndex;
        this.elementreference = "#" + idValue;
        if(type==='multipart'){
            pageIndex=pageIndex.replace("_",".");
        }
        let list = `<section class="contentArea" id=${idValue}>
                    <section class="question">
                    <div class="stem">
                    <div class="qst">${pageIndex}: ${this.assessmentData.getQuestionText()}</div>
                    </div>
                    </section>
                    <div class="instruction">${this.assessmentData.getInstructionText()}</div>
                    <section class="activity">
                    <div id="media" class=${this.mediaClass()}>${this.loadMedia()}</div>
                    <button id="toggler" class="btn btn-primary submitBtns">${this.assessmentData.getButtonLabel()}</button>
                    <p id="answer" class="${this.assessmentData.getAnswerClass()}">${this.assessmentData.getAnswer()}</p>
                    <div class="clearfix"></div>
                    </section>
                    </section>`;
        return list;
    }
    checkCorrectAnswer() {//method is triggered when button is clicked,it toggles between hide and show.
        $('#answer').removeClass('showAnswer hideAnswer');
        if ($('#toggler').text() === 'SHOW ANSWER') {
            this.assessmentData.setAnswerClass('showAnswer');
            this.assessmentData.setButtonLabel('HIDE ANSWER');
            $('#answer').attr('role','alert').show()
            $('#toggler').text('HIDE ANSWER');
        } else {
            this.assessmentData.setAnswerClass('hideAnswer');
            this.assessmentData.setButtonLabel('SHOW ANSWER');
            $('#answer').hide();
            $('#toggler').text('SHOW ANSWER');
        }
        return true;
    }
}