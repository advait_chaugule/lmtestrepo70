/**
 * Created by debayan.das on 19-09-2016.
 */
import MCSS from "./mcss.js";
import MCMS from "./mcms.js";
import Multipart from "./multipart.js";
import OpenWithResponse from "./openWithResponse";
import OpenWithoutResponse from "./openWithoutResponse";
import Incontext from "./inContext";
import Fib from "./fib.js";
class TemplateProvider {
    constructor() {};
    get(question) { // method returns the desired template
        switch (question.getType()) {
        case "mcss":
            return new MCSS(question);
            break;
        case "mcms":
            return new MCMS(question);
            break;
        case "open-ended":
            if (question.getOpenendedType().text === 'without-response') {
                return new OpenWithoutResponse(question);
            }
            return new OpenWithResponse(question);
            break;
        case "multi-part":
            return new Multipart(question);
            break;
        case "in-context":
            return new Incontext(question);
            break;
        case "fib":
            return new Fib(question);
            break;
        default:
            console.log("default");
        }
    }
}
export default new TemplateProvider();