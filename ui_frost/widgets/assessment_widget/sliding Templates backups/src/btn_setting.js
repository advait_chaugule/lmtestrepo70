class Setup {
    loadFooterPanel() {
        $("#checkAnswer,#checkAnswerMobile,#prev,#prevMobile,#next,#nextMobile").show();
    }
    hideNavigation() {
        $("#checkAnswer,#checkAnswerMobile").show();
    }
}
export default new Setup();