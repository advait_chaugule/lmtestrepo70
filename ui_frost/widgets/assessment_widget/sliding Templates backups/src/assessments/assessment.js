/**
 * Created by debayan.das on 26-09-2016.
 */
/** 
 *edited by tarique hussain
 */
import templateProvider from "../templates/template_provider";
import CheckAnswerContoller from "./checkAnswer";
import NavigationController from "./navigation";
export default class AssessmentController {
    constructor() { //initialises variables
        this.currentTemplate = null;
        this.navigator = new NavigationController(this);
        this.checkAnswer = new CheckAnswerContoller(this);
        this.correctAnswers = null;
    }
    init(assessmentData) { // calls init function of different components with assessment data
        this.navigator.init(assessmentData);
        this.checkAnswer.init(assessmentData);
       // $(window).on('resize', this.resizeIframe);
    }
    loadQuestion(question, pageIndex, navbtn) { // method loads question i.e previews them
        this.currentTemplate = templateProvider.get(question);
        let htmlStr = this.currentTemplate.render(pageIndex + 1, 'assessment');
        let html = "<div class='active' style='position: absolute'>" + htmlStr + "</div>"
        
        if (event.type !== 'load') {
            //this.disableNavBtns();
            if (navbtn === 'next') {
                this.moveLeft(htmlStr);
            } else {
                this.moveRight(htmlStr);
            }
        } else {
            $('#playground').html(html);
             this.resizeIframe();
        }
         MathJax.Hub.Queue(["Typeset",MathJax.Hub, "playground"]);
        this.currentTemplate.bindEvents();
       // this.bindEvents();
            
        
    }
    disableNavBtns() {
        $("#next").attr('disabled', true);
        $("#prev").attr('disabled', true);
        $("#nextMobile").attr('disabled', true);
        $("#prevMobile").attr('disabled', true);
    }
    enableNavBtns() {
        $("#next").attr('disabled', false);
        $("#prev").attr('disabled', false);
        $("#nextMobile").attr('disabled', false);
        $("#prevMobile").attr('disabled', false);
    }
    moveLeft(htmlStr) {
        $("footer").attr('disabled', true);
        var html = '<div class="inactive right" style="position: absolute;left:785px; width:100%;">' + htmlStr + '</div>';
        $('#playground').append(html);
        $('.active').animate({
            left: '-=740',
        }, {
            easing: 'linear',
            duration: 500,
            complete:  ()=> {
                $('.active').removeClass('active').addClass('inactive left');
                $('.inactive').not('.left').removeClass('inactive right').addClass('active');
                $('.inactive.left').remove();
            }
        });
        $('.inactive.right').animate({
            left: '0',
        }, {
            easing: 'linear',
            duration: 500,
            complete: ()=> {
               // this.enableNavBtns();
                this.resizeIframe();
            }
        });
    }
    resizeIframe(){
   let height = $('.active').height()+40;
  window.parent.postMessage({"height": height}, "*")
  }
    moveRight(htmlStr) {
        var html = '<div class="inactive left"style="position:absolute;left:-740px;width:100%">' + htmlStr + '</div>';
        $('#playground').append(html);
        $('.active').animate({
            left: '785'
        }, {
            easing: 'linear',
            duration: 500,
            complete: () =>{
                $('.active').removeClass('active').addClass('inactive right');
                $('.inactive').not('.right').removeClass('inactive left').addClass('active');
                $('.inactive.right').remove();
            }
        });
        $('.inactive.left').animate({
            left: '0',
        }, {
            easing: 'linear',
            duration: 500,
            complete:  ()=> {
                //this.enableNavBtns();
                this.resizeIframe();
            }
        });
    }
    getInitialQuestion() { // method gets the very first question
        this.navigator.next();
    }
    getCorrectAnswer() { //method used to get Correct Answer
        this.currentTemplate.checkCorrectAnswer();
    }
    retry() { // method used to give user a preference to retry for particular question
        this.currentTemplate.retry();
    }
    checkAndUpdateSelectedOptions() { // checks and updates the selected options given by user
        if (this.currentTemplate) {
            this.currentTemplate.updateSelectedOptions();
        }
    }
    bindEvents(){
        //$('button').on('click',this.resizeIframe);
    }
   
};