class SaveStateController {
    constructor() {
        this.assessmentController = '';
        this.contextFlag = 0;
        this.selector = {
            retry: $("#retry"),
            retryMobile: $("#retryMobile"),
            checkAnswer: $("#checkAnswer"),
            checkAnswerMobile: $("#checkAnswerMobile")
        };
    }
    setAssesmentCtrl(assessmentControllerInstance) {
        this.assessmentController = assessmentControllerInstance;
    }
    performBasicNavigationCheck(currentQuestion, pageIndex,navbtn) {
        this.assessmentController.loadQuestion(currentQuestion, pageIndex,navbtn);
        switch (currentQuestion.type) {
        case 'mcss':
        case 'mcms':
            this.saveStateForMcq(currentQuestion);
            break;
        case 'open-ended':
            this.hideCheckAnswerButton();
            break;
        case 'multi-part':
        case 'in-context':
            this.saveStateForMultipart(currentQuestion);
            break;
        case 'fib':
            this.saveStateForFib(currentQuestion);
            break;
        }
    }
    saveStateForMcq(currentQuestion, parentQuestiontype) {
        var array = [];
        for (var key in currentQuestion.selectedOptions) {
            array.push(key)
        }
        if (currentQuestion.choices !== null && currentQuestion.choices !== undefined) {
            if (currentQuestion.selectedOptions[array[0]] === undefined) {
                return false;
            }
            if (currentQuestion.selectedOptions[array[0]].length === 0) {
                $(".inputClass").removeClass('pointerEvent');
                this.selector.retry.hide();
                this.selector.retryMobile.hide();
                if (this.isOptionChecked(currentQuestion)) {
                    this.selector.checkAnswer.show().attr('disabled', false);
                    this.selector.checkAnswerMobile.show().attr('disabled', false);
                } else {
                    this.selector.checkAnswer.show().attr('disabled', true);
                    this.selector.checkAnswerMobile.show().attr('disabled', true);
                }
            } else {
                this.buttonPropSetter();
                $(".inputClass").addClass('pointerEvent');
            }
            if (parentQuestiontype === 'multipart') {
                this.hideCheckAnswerButton();
            }
        }
    }
    buttonPropSetter() {
        this.selector.retry.show();
        this.selector.retryMobile.show();
        this.selector.checkAnswer.hide();
        this.selector.checkAnswerMobile.hide();
    }
    saveStateForFib(currentQuestion) {
        if (currentQuestion.disabledFib === 'disabled') {
            this.buttonPropSetter();
        } else {
            this.selector.retry.hide();
            this.selector.retryMobile.hide();
            if ($(".dirty").length===currentQuestion.fibAnswers.length) {
                this.selector.checkAnswer.show().attr('disabled', false);
                this.selector.checkAnswerMobile.show().attr('disabled', false);
            } else {
                this.selector.checkAnswer.show().attr('disabled', true);
                this.selector.checkAnswerMobile.show().attr('disabled', true);
            }
        }
    }
    hideCheckAnswerButton() {
        this.selector.checkAnswer.hide();
        this.selector.checkAnswerMobile.hide();
        this.selector.retry.hide();
        this.selector.retryMobile.hide();
    }
    isOptionChecked(currentQuestion) {
        if (currentQuestion.choices.length > 0) {
            for (let i = 0; i < currentQuestion.choices.length; i++) {
                if (currentQuestion.choices[i].pinanswer === 1) {
                    return true;
                }
            }
        }
    }
    saveStateForMultipart(currentQuestion) {
        this.hideCheckAnswerButton('multipart');
        if (currentQuestion.subQuestions.length === 0) {
            $("#submitMultipart").hide();
        }
        for (var i = 0; i < currentQuestion.subQuestions.length; i++) {
            switch (currentQuestion.subQuestions[i].type) {
            case 'mcss':
            case 'mcms':
                this.saveStateForMcq(currentQuestion.subQuestions[i], 'multipart');
                break;
            }
        }
    }
}
export default new SaveStateController();