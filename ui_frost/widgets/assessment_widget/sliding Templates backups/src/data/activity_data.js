/**
 * Created by debayan.das on 09-11-2016.
 */
import mcqDataService from "./data_service/mcq_data_service";
import OpenEndedDataService from "./data_service/open_ended_data_service";
import IncontextDataService from "./data_service/in_context_data_service";
import fibDataService from "./data_service/fib_data_service";
export default class ActivityData {
    constructor() {
        this.questionText = null; // variable used to store the question
        this.choices = null; // variable used to store the choice     
        this.instructionText = null; // variable used to store the instruction
        this.correctAnswers = null; // variable used to store the correct answer
        this.type = null; // variable used to store the type
        this.mediaUrl = ""; // variable used to store the media address
        this.mediaHeight = null; // variable used to store the media height
        this.mediaWidth = null; // variable used to store the media width
        this.assetType = null; // variable used to store the assesment type
        this.openEndedType = null; // variable used to store the open ended question type
        this.sampleAnswer = null; // variable used to store the sample answer
        this.feedback = null; // variable used to store the feedback
        this.selectedOptions = []; // variable used to store the selected option
        this.responsetext = ""; // variable used to store the response
        this.answer = null; // variable used to store the answer
        this.openEndedClass = ""; // variable used to store the openended classname
        this.sampleAnswerClass = ""; // variable used to store the sample answer classname 
        this.answerClass = "hideAnswer"; // variable used to store the answer classname
        this.btnLabelOpenEnded = "SHOW ANSWER"; // variable used to store the value of openended button label
        this.textAreaAttr = "enabled";
        this.tabindex = '0';
        this.subQuestions = [];
        this.sentences = [];
        this.tempArrayForMultipart = [];
        this.newKey = null;
        this.disabledClass = 'disabled';
        this.multipartBtnClass = 'showAnswer';
        this.multipartFeedbackClsCorrect = 'hideAnswer';
        this.multipartFeedbackClsIncorrect = 'hideAnswer';
        this.multipartRetryClass = 'hideAnswer';
        this.globalMultipartfeedback = [];
        this.ariaAttr = 'false';
        this.fibQuestion = null;
        this.fibAnswers = [];
        this.fibValues = [];
        this.FibClasses = [];
        this.disabledFib = '';
        this.dirtyClass = [];
        // variable used to store the classname of textarea
    }
    setChoices(choiceArray) { //method sets the choices of a question
        this.choices = choiceArray;
        this.correctAnswers = [];
        let currentChoice;
        for (let i = 0; i < choiceArray.length; i++) {
            currentChoice = choiceArray[i];
            if (currentChoice.assess) {
                this.correctAnswers.push(currentChoice);
            }
        }
    }
    setFibAnswers(choiceArray) { //method sets the choices of a question
        this.choices = choiceArray;
        this.correctAnswers = [];
        let currentChoice;
        for (let i = 0; i < choiceArray.length; i++) {
            this.fibAnswers.push(choiceArray[i].textforeachblank);
        }
    }
    getFibAnswers() {
        return this.fibAnswers;
    }
    setAttrs(obj) {}
    setFibChoices(choiceArray) { //method sets the choices of a question
        this.choices = choiceArray;
        this.setFibAnswers(choiceArray);
    }
    getFibChoices() {
        return this.choices;
    }
    renderQuestion(obj) {
        let template = obj.interaction;
        return this[template](obj);
    }
    textentry(obj) { // for rendering text input 
        let tempHtml = "<div id='spnCorrectIncorrect0' class='option fibclass  " + ((this.getFibClasses(obj.CurrentOptionIndex - 1)===undefined)?' ':this.getFibClasses(obj.CurrentOptionIndex - 1))+ " '><input ansindex='" + obj.CurrentOptionIndex + "' " + this.getDisableProp() + " class='form-control commonClass "+ ((this.getDirtyClass(obj.CurrentOptionIndex - 1)===undefined)? ' ':this.getDirtyClass(obj.CurrentOptionIndex - 1)) + "' type='" + ((obj.inputtype === 'numeric') ? 'number' : 'text') + "' inputType='" + obj.inputtype + "' maxlength='" + obj.characters + "' style='text-align:" + obj.inputalignment + "' value='" + ((this.getFibValues(obj.CurrentOptionIndex - 1) === undefined) ? '' : (this.getFibValues(obj.CurrentOptionIndex - 1))) + "'><div class='sign'><div class='sr-only'></div><span class='fa'></span></div><div class='fdback' data-toggle='modal'><div class='sr-only' aria-hidden='true' aria-label='Click to open feedback'>" + obj.correct_feedback + "|#@~" + obj.incorrect_feedback + "</div><span tabindex=0 class='fa fa-commenting feedBackIcon'></span></div></div>";
        return tempHtml;
    }
    inline(obj) { //for rendering dropDowns
        let optionLength = obj.distractorforeachblank.length;
        let tempHtml = "<div id='spnCorrectIncorrect0' class='option fibclass  " + ((this.getFibClasses(obj.CurrentOptionIndex - 1)===undefined)?' ':this.getFibClasses(obj.CurrentOptionIndex - 1))+ " '><select  " + this.getDisableProp(obj.CurrentOptionIndex - 1) + " value='" + this.getFibValues(obj.CurrentOptionIndex - 1) + "' ansindex='" + obj.CurrentOptionIndex + "' class='commonClass form-control "+ ((this.getDirtyClass(obj.CurrentOptionIndex - 1)===undefined)? ' ':this.getDirtyClass(obj.CurrentOptionIndex - 1)) + "'><option value=''>--select--</option>";
        for (let i = 0; i < optionLength; i++) {
            tempHtml += "<option " +
                ((this.getFibValues(obj.CurrentOptionIndex - 1) === obj.distractorforeachblank[i].text) ? 'selected' : '') + " value='" + obj.distractorforeachblank[i].text + "'>" + obj.distractorforeachblank[i].text + "</option>";
        }
        tempHtml += "</select><div class='sign'><div class='sr-only'></div><span class='fa'></span></div><div class='fdback' data-toggle='modal'><div class='sr-only' aria-hidden='true' aria-label='Click to open feedback'>" + obj.correct_feedback + "|#@~" + obj.incorrect_feedback + "</div><span tabindex=0 class='fa fa-commenting feedBackIcon'></span></div></div>";
        return tempHtml;
    }
    setFibQuestions(text) {
        this.fibRawText = text;
        this.html = "";
        this.fibQuestion = text.split(/(\[\[\d]])/g);
        this.fibQuestion.forEach((i, k) => {
            if (i.indexOf('[[') >= 0) {
                this.html += this.renderQuestion(this.searchChoiceObj(i, this.getFibChoices()));
            } else {
                i = (i.search('<p>') === 0) ? i : '<p class="question-simple-text">' + i + '</p>'
                i = i.replace('<p>', '<p class="question-simple-text">');
                this.html += i;
            }
        });
        this.fibQuestion = this.html;
    }
    getFibRawText() {
        return this.fibRawText;
    }
    setFibValues(val, index) {
        if (val !== 'empty') {
            this.fibValues[index] = val;
        } else {
            this.fibValues = [];
        }
    }
    setDirtyClass(cls, index) {
        this.dirtyClass[index] = cls
    }
    getDirtyClass(index) {
        return this.dirtyClass[index];
    }
    
    getFibValues(index) {
        return this.fibValues[index];
    }
    setDisableProp(prop) {
        this.disabledFib = prop;
    }
    getDisableProp() {
        return this.disabledFib;
    }
    setFibClasses(Class) {
        if (Class !== 'empty') {
            this.FibClasses.push(Class);
        } else {
            this.FibClasses = [];
        }
    }
    getFibClasses(index) {
        return this.FibClasses[index];
    }
    searchChoiceObj(nameKey, myArray) {
        for (var i = 0; i < myArray.length; i++) {
            if (myArray[i].noforeachblank === nameKey) {
                return myArray[i];
            }
        }
    }
    getFibQuestionText() {
        return this.fibQuestion
    }
    setSubQuestions(questionArray) {
        let subQuestionsArray = [],
            currentActivity = {};
        for (let i = 0; i < questionArray.length; i++) {
            currentActivity.QuestionData = questionArray[i];
            if (currentActivity.QuestionData.sentence != undefined) {
                this.sentences.push(currentActivity.QuestionData.sentence.text);
            }
            switch (currentActivity.QuestionData.question_type.text) {
            case "mcss":
            case "mcms":
                this.subQuestions.push(mcqDataService.getActivityData(currentActivity));
                break;
            case "open-ended":
                this.subQuestions.push(OpenEndedDataService.getActivityData(currentActivity));
                break;
            case "in-context":
                this.subQuestions.push(IncontextDataService.getActivityData(currentActivity));
                break;
            default:
            }
        }
    }
    getSubQuestions() {
        return this.subQuestions;
    }
    getChoices() { // method returns the choices
        return this.choices;
    }
    getCorrectAnswers() { // method returns correct responses
        return this.correctAnswers;
    }
    setQuestionText(questionText) { //method sets the text of the question
        this.questionText = questionText;
    }
    setInstructionText(instructionText) {
        this.instructionText = instructionText;
    }
    setAnswer(answer) {
        this.answer = answer;
    }
    getAnswer() {
        return this.answer;
    }
    setMediaParameters(mediaObj, mediaType) {
        this.mediaUrl = mediaObj.url;
        this.mediaType = mediaObj.type;
        if (this.mediaType === 'brightcove') {
            this.mediaHeight = mediaObj.height;
            this.mediaWidth = mediaObj.width;
        } else {
            this.assetType = mediaObj.asset_type;
        }
    }
    getQuestionText() { // method gets the question's text
        return this.questionText;
    }
    setGlobalCorrectFeedback(globalCorrectFeedback) {
        this.globalCorrectFeedback = globalCorrectFeedback;
    }
    getGlobalCorrectFeedback() { // method gets the question's text
        return this.globalCorrectFeedback;
    }
    setGlobalInCorrectFeedback(globalInCorrectFeedback) {
        this.globalInCorrectFeedback = globalInCorrectFeedback;
    }
    getGlobalInCorrectFeedback() { // method gets the question's text
        return this.globalInCorrectFeedback;
    }
    getSampleAnswer() {
        return this.sampleAnswer;
    }
    getMediaParameters(parameter) {
        if (parameter === 'url') {
            return this.mediaUrl;
        }
        if (parameter === 'height') {
            return this.mediaHeight;
        }
        if (parameter === 'width') {
            return this.mediaWidth;
        }
        if (parameter === 'type') {
            return this.mediaType;
        }
        if (parameter === 'assetType') {
            return this.assetType;
        }
    }
    getInstructionText() {
        return this.instructionText
    }
    setType(type, openEndedType) { //method sets the type of the question for example :MCQ,MCSS
        this.type = type;
        if (type === 'open-ended') {
            this.openEndedType = openEndedType;
        }
    }
    setSampleAnswer(sampleAnswer) {
        this.sampleAnswer = sampleAnswer;
    }
    getType() { //method returns the type of the question
        return this.type;
    }
    getOpenendedType() {
        return this.openEndedType;
    }
    addSelectedOption(ref, newOption) {
        if (this.newKey != ref) {
            this.tempArrayForMultipart = [];
        }
        this.tempArrayForMultipart.push(newOption);
        this.newKey = ref;
        this.selectedOptions[this.newKey] = this.tempArrayForMultipart;
    }
    checkedOptionArray(ref) {
        this.newKey = ref;
        this.selectedOptions[this.newKey] = this.tempArrayForMultipart;
    }
    resetSelectedOption() {
        this.selectedOptions = [];
        this.tempArrayForMultipart = [];
    }
    resetPinanswer() {
        for (let i = 0; i < this.choices.length; i++) {
            this.choices[i].pinanswer = 0;
        }
    }
    setPinanswer(index) {
        if (typeof index === 'number') {
            this.choices[index - 1].pinanswer = 1;
        } else if (typeof index === 'object') {
            for (let i = 0; i < this.choices.length; i++) {
                if (index.indexOf(this.choices[i].choiceid) > -1) {
                    this.choices[i].pinanswer = 1;
                } else {
                    this.choices[i].pinanswer = 0;
                }
            }
        }
    }
    setResponseText(responsetext) {
        this.responsetext = responsetext;
    }
    getResponseText() {
        return this.responsetext;
    }
    setOpenEndedClass(btnClassName) {
        this.openEndedClass = btnClassName;
    }
    getOpenEndedClass(btnClass) {
        return this.openEndedClass;
    }
    setSampleAnswerClass(className) {
        this.sampleAnswerClass = className;
    }
    getSampleAnswerClass() {
        return this.sampleAnswerClass
    }
    setTabindex(tabindex) {
        this.tabindex = tabindex;
    }
    setButtonLabel(label) {
        this.btnLabelOpenEnded = label;
    }
    getButtonLabel() {
        return this.btnLabelOpenEnded;
    }
    getTabindex() {
        return this.tabindex;
    }
    setAnswerClass(answerClass) {
        this.answerClass = answerClass
    }
    getAnswerClass() {
        return this.answerClass;
    }
    setTextAreaAttr(attr) {
        this.textAreaAttr = attr;
    }
    getdisabledClass() {
        return this.disabledClass;
    }
    getTextAreaAttr() {
        return this.textAreaAttr;
    }
    setDisabledClass(cls) {
        return this.disabledClass = cls;
    }
    setFeedback(feedback) {
        this.feedback = feedback;
    }
    getFeedback() {
        return this.feedback;
    }
    getMultipartBtnClass() {
        return this.multipartBtnClass;
    }
    setMultipartBtnClass(cls) {
        this.multipartBtnClass = cls;
    }
    getMultipartFeedBackClassCorrect() {
        return this.multipartFeedbackClsCorrect;
    }
    getMultipartFeedBackClassIncorrect() {
        return this.multipartFeedbackClsIncorrect;
    }
    setMultipartFeedBackClassCorrect(cls) {
        this.multipartFeedbackClsCorrect = cls;
    }
    setMultipartFeedBackClassIncorrect(cls) {
        this.multipartFeedbackClsIncorrect = cls;
    }
    getglobalMultipartfeedback() {
        return this.globalMultipartfeedback;
    }
    getMultipartRetryClass() {
        return this.multipartRetryClass;
    }
    setMultipartRetryBtnClass(cls) {
        this.multipartRetryClass = cls;
    }
    getTextAreaAriaDisabled() {
        return this.ariaAttr;
    }
    setTextAreaAriaDisabled(attr) {
        this.ariaAttr = attr;
    }
    getSentences() {
        return this.sentences;
    }
}