/**
 * Created by debayan.das on 09-11-2016.
 */
import ActivityData from "../activity_data";
class MultipartDataService {
    getActivityData(activityDataJSON) { // method returns the filtered data from JSON file
        let activityData = new ActivityData();
        activityData.setQuestionText(activityDataJSON.QuestionData.question_stem.text);
        activityData.setInstructionText(activityDataJSON.QuestionData.instruction_text.text);
        activityData.setSubQuestions(activityDataJSON.QuestionData.subquestions);//setSubquestions
        this.setMediaParameters(activityDataJSON, activityData);
        activityData.setType(activityDataJSON.QuestionData.question_type.text);
        activityData.setGlobalCorrectFeedback(activityDataJSON.QuestionData.global_correct_feedback.text);
        activityData.setGlobalInCorrectFeedback(activityDataJSON.QuestionData.global_incorrect_feedback.text);
        return activityData;
    }
    setMediaParameters(activityDataJSON, activityData) {
        switch (activityDataJSON.QuestionData.media.type) {
        case 'brightcove':
            activityData.setMediaParameters(activityDataJSON.QuestionData.media, 'brightcove');
            break;
        case 'assets':
            activityData.setMediaParameters(activityDataJSON.QuestionData.media, 'assets');
            break;
        }
    }
}
export default new MultipartDataService();