/**
 * Created by Tarique.das on 20-10-2016.
 */
import ActivityData from "../activity_data";
        class OpenEndedDataService {
        getActivityData(activityDataJSON) { // method returns the filtered data from JSON file
        let activityData = new ActivityData();
            activityData.setQuestionText(activityDataJSON.QuestionData.question_stem.text);
            if(activityDataJSON.question_number!= "" && activityDataJSON.question_number!=null)
                activityData.setQuestionNumber(activityDataJSON.question_number);
            activityData.setInstructionText(activityDataJSON.QuestionData.instruction_text.text);
            activityData.setMediaParameters(activityDataJSON.QuestionData.media);
            if (activityDataJSON.QuestionData.question_type.text === 'open-ended'){
                activityData.setType(activityDataJSON.QuestionData.question_type.text, activityDataJSON.QuestionData.response_type);
                if (activityDataJSON.QuestionData.response_type.text === 'with-response'){
                    activityData.setSampleAnswer(activityDataJSON.QuestionData.sample_answer.text);
                    activityData.setFeedback(activityDataJSON.QuestionData.feedback.text);
                } else{
                    activityData.setAnswer(activityDataJSON.QuestionData.answer.text);
                }
            } else{
                activityData.setType(activityDataJSON.QuestionData.question_type.text, null);
            }
            return activityData;
        }
        }

export default new OpenEndedDataService();