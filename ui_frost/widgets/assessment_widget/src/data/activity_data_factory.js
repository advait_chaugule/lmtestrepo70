/**
 * Created by debayan.das on 09-11-2016.
 */
/**edited by Tarique*/
import mcqDataService from "./data_service/mcq_data_service";
import MultiPartDataService from "./data_service/multipart_data_service";
import OpenEndedDataService from "./data_service/open_ended_data_service";
import IncontextDataService from "./data_service/in_context_data_service";
import fibDataService from "./data_service/fib_data_service";
class ActivityDataFactory {
    constructor() {}
    getActivityDataFromJSON(jsonData) {
        let allActivities = [],
            currentActivity;
        for (let i = 0; i < jsonData.length; i++) {
            currentActivity = jsonData[i];
            switch (currentActivity.QuestionData.question_type.text) {
                case "mcss":
                case "mcms":
                    allActivities.push(mcqDataService.getActivityData(currentActivity));
                    break;
                case "open-ended":
                    allActivities.push(OpenEndedDataService.getActivityData(currentActivity));
                    break;
                case "multi-part":
                    allActivities.push(MultiPartDataService.getActivityData(currentActivity));
                    break;
                case "in-context":
                    allActivities.push(IncontextDataService.getActivityData(currentActivity));
                    break;
                case "fib":
                case "fib-dragdrop":
                case "fib-dropdown":
                    allActivities.push(fibDataService.getActivityData(currentActivity));
                    break;
                default:
                    console.log("in default");
            }
        }
        return allActivities;
    }
}
export default new ActivityDataFactory();