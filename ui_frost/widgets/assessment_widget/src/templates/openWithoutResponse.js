/*
 *created by tarique hussain on 10.20.16 
 */
import MainActivity from './mainActivity';
export default class OpenWithoutResponse extends MainActivity {
    constructor(assessmentData) {
        super();
        this.assessmentData = assessmentData;
        this.elementreference = null;
    }
    bindEvents(type) {
        if (type != 'multipart') {
            $("#retryMultipart").hide();
            $("#retryMultipartMobile").hide();
            $("#submitMultipart").hide();
            $("#submitMultipartMobile").hide();
        }
        $(this.elementreference).off("click", "#toggler").on("click", "#toggler", this.checkCorrectAnswer.bind(this));
    }
    render(pageIndex, type) { // method returns the desired HTML which then gets rendered.
        let idValue = "mcq_" + pageIndex;
        this.elementreference = "#" + idValue;
        let qNumber = this.assessmentData.getQuestionNumber();
        if (type === 'multipart') {
            pageIndex = pageIndex.replace("_", ".");
        }
        let list = `<section class="contentArea" id=${idValue}>
                    <section class="question">
                    <div class="stem">
                    <div class="qst"><span class="fixedSpan">Q${$('#prev').css('display')=='none'&&type!='multipart'?(qNumber!=null?qNumber:""):pageIndex}:&nbsp;</span> ${this.assessmentData.getQuestionText()}  <div class="clearfix"></div></div>
                    </div>
                    </section>
                    <div class="${this.assessmentData.getInstructionText()=="borderBottom"?'':'instruction'}" >${this.assessmentData.getInstructionText()}<div class="clearfix"></div></div>
                    <section class="activity">
                    <div id="media" class=${this.mediaClass()}>${this.loadMedia()}</div>
                    <button id="toggler" class="btn btn-primary submitBtns">${this.assessmentData.getButtonLabel()}</button>
                    <div id="answer" class="${this.assessmentData.getAnswerClass()}">${this.assessmentData.getAnswer()}<div class="clearfix"></div></div>
                    <div class="clearfix"></div>
                    </section>
                    </section>`;
        return list;
    }
    checkCorrectAnswer() { //method is triggered when button is clicked,it toggles between hide and show.
        $(this.elementreference).find('#answer').removeClass('showAnswer hideAnswer');
        if ($(this.elementreference).find('#toggler').text() === 'Show Answer') {
            this.assessmentData.setAnswerClass('showAnswer');
            this.assessmentData.setButtonLabel('Hide Answer');
            $(this.elementreference).find('#answer').attr('role', 'alert').show()
            $(this.elementreference).find('#toggler').text('Hide Answer');
        } else {
            this.assessmentData.setAnswerClass('hideAnswer');
            this.assessmentData.setButtonLabel('Show Answer');
            $(this.elementreference).find('#answer').hide();
            $(this.elementreference).find('#toggler').text('Show Answer');
        }
        return true;
    }
}