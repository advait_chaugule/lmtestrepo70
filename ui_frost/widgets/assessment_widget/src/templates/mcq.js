/*created by tarique*/
import MainActivity from './mainActivity';
export default class MCQ extends MainActivity {
    constructor() {
        super();
        if (this.constructor === MCQ) {
            throw new Error("Abstract class can not be instantiated");
        }
        this.pageIndex = 1;
    }
    bindEvents() { //method binds click events checkbox & radio button
            $('.feedBackIcon').off().on('click keypress', (e) => {
                if (e.keyCode === 13 || e.type === "click") {
                    let feedBackText = [];
                    //debugger;
                    //                  var arr1=this.regexManager('<math xmlns="http://www.w3.org/1998/Math/MathML">',$(e.target).siblings().html());
                    //                  var arr2=this.regexManager('</math></script>',$(e.target).siblings().html());
                    //                this.replaceMathml(arr1,arr2,$(e.target).siblings().html());
                    var index = 0;
                    feedBackText = $(e.target).siblings().html().split('|#@~');
                    //feedBackText[0]=feedBackText.replace(feedBackText[0].replace(feedBackText[0].substr(feedBackText[0].indexOf())))
                    //                if(feedBackText[1].indexOf('<math xmlns')!=-1){
                    //                    index=feedBackText[1].indexOf('<math xmlns');
                    //                }
                    if ($(e.target).parents('.rightanswer').length) {
                        $('#modalBodyPanel').html(feedBackText[0].substring(index)).attr('role', 'alert');
                    } else if ($(e.target).parents('.wronganswer').length) {
                        $('#modalBodyPanel').html(feedBackText[1].substring(index)).attr('role', 'alert');
                    }
                    //MathJax.Hub.Queue(["Typeset",MathJax.Hub, "modalBodyPanel"]);
                    $('#myModal').modal('show');
                }
            });
        }
        //    regexManager(pat,c){
        //            var regexp = new RegExp(pat, 'g')
        //            var foo = c;
        //            var match, matches = [];
        //            while ((match = regexp.exec(foo)) != null) {
        //           matches.push(match.index);
        //}
        //        return matches
        //    }
        //    replaceMathml(arr1,arr2,str){
        //        debugger;
        //    }
    retry() { //method allows user to retry for a question
        this.assessmentData.resetPinanswer();
        this.assessmentData.resetSelectedOption();
        this.assessmentData.setTextAreaAriaDisabled('false');
        this.assessmentData.setTextAreaAttr('');
        $(this.elementreference).find('input[type="radio"]').attr('aria-disabled', 'false').prop('disabled', false);
        $(this.elementreference).find('input[type="radio"]').prop('disabled', false);
        $(this.elementreference).find('input[type="checkbox"]').prop('disabled', false);
        $(this.elementreference).find("#checkAnswer,#checkAnswerMobile").attr('disabled', true);
        $(this.elementreference).find('input[type="radio"]:checked').prop('checked', false);
        $(this.elementreference).find('input[type="checkbox"]:checked').prop('checked', false);
        $(this.elementreference).find('input[type="radio"]').parents('.option').removeClass('selected wronganswer rightanswer');
        $(this.elementreference).find('input[type="checkbox"]').parents('.option').removeClass('selected wronganswer rightanswer');
    }
    isItemChecked(item, ref) { //method for checking the item is already selected or not
        if (this.assessmentData.selectedOptions[ref] === undefined) {
            return false;
        }
        if (this.assessmentData.selectedOptions[ref].length === 0) {
            return item.pinanswer === 1 ? "checked" : "";
        }
        return this.assessmentData.selectedOptions[ref].indexOf(String(item.choiceid)) > -1 ? "checked" : "";
    }
    isCorrectFedbckVisible(wrongAnswerCounter, rightAnswerCounter) { //method decide the flag to show the correct feedback panel
        if ((wrongAnswerCounter === 0 && rightAnswerCounter === 0) || (wrongAnswerCounter > 0) || (rightAnswerCounter !== this.assessmentData.getCorrectAnswers().length)) {
            return;
        }
        return 'show';
    }
    isInCorrectFedbckVisible(wrongAnswerCounter, rightAnswerCounter) { //method decide the flag to show the incorrect feedback panel
        if ((wrongAnswerCounter > 0) && (rightAnswerCounter !== this.assessmentData.getCorrectAnswers().length)) {
            return 'show';
        }
        if (wrongAnswerCounter === 0 && rightAnswerCounter === 0) {
            return;
        }
        return;
    }
    updateSelectedOptions() { //method for updating selected option
        let selectedInput = $(this.elementreference).find('input:checked');
        for (let i = 0; i < selectedInput.length; i++) {
            this.assessmentData.addSelectedOption(this.elementreference, $(this.elementreference).find(selectedInput[i]).attr("id").split('checkbox')[1]);
        }
    }
    checkItemAttemptState(item, ref) { //method return the classname for already selected right or wrong answer
        if (this.assessmentData.selectedOptions[ref] === undefined) {
            return;
        }
        if (this.assessmentData.selectedOptions[ref].length === 0 && item.pinanswer === 1) {
            return "selected";
        }
        if (this.assessmentData.selectedOptions[ref].length > 0) {
            return this.submittedCheckedItemState(item, ref);
        }
    }
    submittedCheckedItemState(item, ref) {
        if (this.assessmentData.selectedOptions[ref].indexOf(String(item.choiceid)) === -1) {
            return;
        }
        if (this.assessmentData.selectedOptions[ref].indexOf(String(item.choiceid)) > -1 && item.assess === true) {
            this.rightAnswerCounter++;
            return "rightanswer";
        }
        if (this.assessmentData.selectedOptions[ref].indexOf(String(item.choiceid)) > -1 && item.assess === false) {
            this.wrongAnswerCounter++;
            return "wronganswer";
        }
    }
    addGlobalLabelFeedBack(type) { //method return the global correct & incorrect feedback html
        if (type === 'multipart') {
            return "";
        }
        let globalFeedbackHtml = '';
        if (this.assessmentData.getGlobalCorrectFeedback().replace(/^\s+|\s+$/gm, '') !== "") {
            globalFeedbackHtml += `<div class="alert alert-success ${this.isCorrectFedbckVisible(this.wrongAnswerCounter,this.rightAnswerCounter)}" >${this.assessmentData.getGlobalCorrectFeedback().replace(/(<p[^>]+?>|<p>|<\/p>)/img, "")}<div class="clearfix"></div></div>`;
        }
        if (this.assessmentData.getGlobalInCorrectFeedback().replace(/^\s+|\s+$/gm, '') !== "") {
            globalFeedbackHtml += `<div class="alert alert-danger ${this.isInCorrectFedbckVisible(this.wrongAnswerCounter,this.rightAnswerCounter)}" >${this.assessmentData.getGlobalInCorrectFeedback().replace(/(<p[^>]+?>|<p>|<\/p>)/img, "")}<div class="clearfix"></div></div>`;
        }
        return globalFeedbackHtml;
    }
    buttonSetterForIncontext() {
        $("#submitIncontext").prop('disabled', false);
    }
}