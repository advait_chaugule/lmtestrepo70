/**
 * Created by debayan.das on 19-09-2016.
 */
/** 
 *edited by tarique hussain
 */
import MCQ from "./mcq";
export default class MCMS extends MCQ {
    constructor(assessmentData) {
        super();
        this.assessmentData = assessmentData;
        this.elementreference = null;
    }
    bindEvents(type) {
        if (type != 'multipart') {
            $("#retryMultipart").hide();
            $("#retryMultipartMobile").hide();
            $("#submitMultipart").hide();
            $("#submitMultipartMobile").hide();
        }
        $(this.elementreference).on("click", "input", this.selectAnswers.bind(this));
        super.bindEvents();
    }
    selectAnswers() { //method used to add a styles to options
        this.buttonSetterForIncontext();
        //$(event.target).prop('checked', true);
        if ($(this.elementreference).find('input[name="optionsRadios"]:checked').length === 0) {
            $("#checkAnswer,#checkAnswerMobile").attr('disabled', true);
            $('#submitIncontext').attr('disabled', true);
        } else {
            $("#checkAnswer,#checkAnswerMobile").attr('disabled', false);
            $('#submitIncontext').attr('disabled', false);
        }
        $(this.elementreference).find('input[name="optionsRadios"]:not(:checked)').parents('.option').removeClass('selected');
        $(this.elementreference).find('input[name="optionsRadios"]:checked').parents('.option').addClass('selected');
        $(this.elementreference).find('.options .option').removeClass('rightanswer wronganswer');
        if ($(this.elementreference).find('input:checked').length === 0) {
            return;
        }
        let chekedIndxArr = [],
            checkedIndx = $(this.elementreference).find('input:checked');
        if (checkedIndx.length > 0) {
            for (let i = 0; i < checkedIndx.length; i++) {
                chekedIndxArr.push(parseInt(checkedIndx.eq(i).attr("id").split('checkbox')[1]));
            }
        }
        this.assessmentData.setPinanswer(chekedIndxArr);
    }
    checkCorrectAnswer() { //method used to check the response
        this.assessmentData.setTextAreaAttr('disabled');
        this.assessmentData.resetPinanswer();
        this.assessmentData.setTextAreaAriaDisabled('true');
        $(this.elementreference).find('input[type="checkbox"]').attr('aria-disabled', 'true').prop('disabled', true);
        let correctChoiceId = [];
        let selectedbtnId = $(this.elementreference).find('input:checked');
        if (selectedbtnId.length === 0) {
            return false;
        }
        if (selectedbtnId.length === this.assessmentData.getCorrectAnswers().length) {
            let totalCorrect = true;
            for (let i = 0; i < this.assessmentData.getCorrectAnswers().length; i++) {
                correctChoiceId.push(this.assessmentData.getCorrectAnswers()[i].choiceid);
            }
            for (let i = 0; i < selectedbtnId.length; i++) {
                if (correctChoiceId.indexOf(parseInt($(selectedbtnId[i]).attr("id").split('checkbox')[1])) === -1) {
                    totalCorrect = false;
                }
            }
            if (totalCorrect) {
                selectedbtnId.parents('.option').removeClass('selected').addClass('rightanswer');
                $(this.elementreference).find('.alert-success').attr('role', 'alert').show();
            } else {
                selectedbtnId.parents('.option').removeClass('selected').addClass('wronganswer');
                $(this.elementreference).find('.alert-danger').attr('role', 'alert').show();
            }
        } else {
            selectedbtnId.parents('.option').removeClass('selected').addClass('wronganswer');
            $(this.elementreference).find('.alert-danger').attr('role', 'alert').show();
        }
    }
    render(pageIndex, type, templateName) { // method returns the desired HTML which then gets rendered.
        this.wrongAnswerCounter = 0;
        this.rightAnswerCounter = 0;
        let contentAreaClass = '';
        let idValue = type + pageIndex;
        this.elementreference = "#" + idValue;
        let qNumber = this.assessmentData.getQuestionNumber();
        if (type === 'multipart') {
            pageIndex = pageIndex.replace("_", ".");
        }
        if (templateName === 'incontext') {
            contentAreaClass = 'contentAreaClass';
        }
        this.assessmentData.checkedOptionArray(this.elementreference);
        let list = `<section class="contentArea contentAreaClass" id=${idValue}>
                    <fieldset><legend>
                    <section class="question">
                    <div class="stem">
                    <div class="qst"><span class="fixedSpan">Q${$('#prev').css('display')=='none'&&type!='multipart'?(qNumber!=null?qNumber:""):pageIndex}:&nbsp;</span>${this.assessmentData.getQuestionText()}  <div class="clearfix"></div></div>
                    </div>
                    </section>
                    <section class="activity">
                    <div class="instruction">${this.assessmentData.getInstructionText()}<div class="clearfix"></div></div>
                    </legend>
                    <div id="media" class=${this.mediaClass()}>${this.loadMedia()}</div>
                    <div class="options">${this.getChoiceList(this.elementreference)} <div class="clearfix"></div></div>
                    <div class="clearfix"></div>
                    </section>
                    </fieldset>`;
        list += this.addGlobalLabelFeedBack(type);
        list += `</section>`;
        if (this.wrongAnswerCounter > 0) {
            return list.replace(/rightanswer/g, 'wronganswer');
        } else if (this.rightAnswerCounter !== this.assessmentData.getCorrectAnswers().length) {
            return list.replace(/rightanswer/g, 'wronganswer');
        } else {
            return list;
        }
    }
    getChoiceList(ref) { // method return the checkbox option html
        let choiceList = "",
            allChoices = this.assessmentData.getChoices();
        for (let i = 0; i < allChoices.length; i++) {
            choiceList += `<div class="col-md-12">
                            <div class="option ${this.checkItemAttemptState(allChoices[i],ref)}">
                            <div class="checkbox inputClass">
                            <label><input type="checkbox" name="optionsRadios" id="checkbox${i + 1}"                                               value="option${i + 1}" ${this.isItemChecked(allChoices[i],ref)} ${this.assessmentData.getTextAreaAttr()}>${allChoices[i].text}</label>
                            </div>
                            <div class="sign"><div class="sr-only"></div><span class="fa"></span>  <div class="clearfix"></div></div>
                              <div class="clearfix"></div></div>
                            </div>`;
        }
        return choiceList;
    }
}