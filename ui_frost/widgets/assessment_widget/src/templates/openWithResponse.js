/*
 *created by tarique hussain on 10.20.16 */
import MainActivity from './mainActivity';
export default class OpenWithResponse extends MainActivity {
    constructor(assessmentData) {
        super();
        this.assessmentData = assessmentData;
        this.elementreference = null;
    }
    bindEvents(type) {
        if (type != 'multipart') {
            $("#retryMultipart").hide();
            $("#retryMultipartMobile").hide();
            $("#submitMultipart").hide();
            $("#submitMultipartMobile").hide();
        }
        $(this.elementreference).off("click", "#toggler2").on("click", "#toggler2", this.checkCorrectAnswer.bind(this));
        $(this.elementreference).off("blur", "#responseArea").on("blur", "#responseArea", this.textInteractivity.bind(this));
        $(this.elementreference).off("keyup", "#responseArea").on("keyup", "#responseArea", this.disableSubmitButton.bind(this));
        //        $("#responseArea").attr('title', this.assessmentData.getQuestionText().replace(/<(?:.|\n)*?>/gm, '').replace(/&nbsp;/gi,''));
        $("#responseArea").attr('title', $(this.assessmentData.getQuestionText()).text().replace(/&nbsp;/gi, ''));
    }
    render(pageIndex, type) { // method returns the desired HTML which then gets rendered.
        let idValue = "mcq_" + pageIndex;
        this.elementreference = "#" + idValue;
        let qNumber = this.assessmentData.getQuestionNumber();
        if (type === 'multipart') {
            pageIndex = pageIndex.replace("_", ".");
        }
        let list = `<section class="contentArea" id=${idValue}>
                    <section  class="question">
                    <div class="stem">
                    <div class="qst"><span class="fixedSpan">Q${$('#prev').css('display')=='none'&&type!='multipart'?(qNumber!=null?qNumber:""):pageIndex}:&nbsp;</span>${this.assessmentData.getQuestionText()}  <div class="clearfix"></div></div>
                    </div>
                    </section>
                    <div  class="instruction">${this.assessmentData.getInstructionText()}<div class="clearfix"></div></div>
                    <section class="activity">
                    <div id="media" class=${this.mediaClass()}>${this.loadMedia()}</div>
                    <form>
                    <div class="form-group">
                    <textarea aria-disabled=${this.assessmentData.getTextAreaAriaDisabled()} tabindex="${this.assessmentData.getTabindex()}" class="form-control" ${this.assessmentData.getTextAreaAttr()} rows="5" id="responseArea" title="">${this.assessmentData.getResponseText()}</textarea>
                    </div>
                    </form>
                    <button id="toggler2" class="btn btn-primary submitBtns ${this.assessmentData.getOpenEndedClass()}" ${this.assessmentData.getdisabledClass()}>Show Answer</button>
                    <div class="${this.assessmentData.getSampleAnswerClass()} openFeedback headingClass"><strong>Feedback:</strong><br>${this.assessmentData.getFeedback()}  <div class="clearfix"></div></div>
                    <div  class="${this.assessmentData.getSampleAnswerClass()} headingClass"><strong>Sample Answer:</strong><br>${this.assessmentData.getSampleAnswer()}  <div class="clearfix"></div></div>
                    </section>
                    </section>`;
        return list;
    }
    textInteractivity() { //method extracts the text from the text area
        this.assessmentData.setResponseText($(this.elementreference).find('#responseArea').val());
    }
    checkCorrectAnswer() { //method submits the form and disables the text area
        this.assessmentData.setDisabledClass('');
        this.assessmentData.setOpenEndedClass('hideBtn');
        this.assessmentData.setSampleAnswerClass('showSampleAnswer');
        this.assessmentData.setTextAreaAttr('disabled');
        this.assessmentData.setTextAreaAriaDisabled('true');
        this.assessmentData.setTabindex('-1');
        $(this.elementreference).find('.headingClass').eq(0).attr('role', 'alert').show();
        $(this.elementreference).find('.headingClass').eq(1).show();
        $(this.elementreference).find('#responseArea').prop('disabled', true);
        $(this.elementreference).find('#responseArea').attr('aria-disabled', "true");
        $(this.elementreference).find('#toggler2').hide();
        return true;
    }
    disableSubmitButton() {
        if ($(this.elementreference).find("#responseArea").val() === "") {
            $(this.elementreference).find('#toggler2').prop('disabled', true);
        } else {
            $(this.elementreference).find('#toggler2').prop('disabled', false);
        }
    }
}