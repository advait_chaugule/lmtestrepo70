/**
 * Created by debayan.das on 19-09-2016.
 */
/** 
 *edited by tarique hussain
 */
import MCQ from "./mcq";
export default class MCSS extends MCQ {
    constructor(assessmentData) {
        super();
        this.assessmentData = assessmentData;
        this.elementreference = null;
    }
    bindEvents(type) {
        if (type != 'multipart') {
            $("#retryMultipart").hide();
            $("#retryMultipartMobile").hide();
            $("#submitMultipart").hide();
            $("#submitMultipartMobile").hide();
        }
        $(this.elementreference).off("click", "input").on("click", "input", this.selectAnswers.bind(this));
        super.bindEvents();
        $('.stem .qst:before').css('content', '');
        //$('.stem .qst:before').css('content','wwwwwwww');
    }
    selectAnswers() { //method used to add a styles to options
        this.buttonSetterForIncontext();
        $("#checkAnswer,#checkAnswerMobile").attr('disabled', false);
        $(this.elementreference).find('input[type="radio"]:not(:checked)').parents('.option').removeClass('selected');
        $(this.elementreference).find('input[type="radio"]:checked').parents('.option').addClass('selected');
        $(this.elementreference).find('.options .option').removeClass('rightanswer wronganswer');
        this.assessmentData.resetPinanswer();
        if ($(this.elementreference).find('input:checked').length === 0) {
            return false;
        }
        this.assessmentData.setPinanswer(parseInt($(this.elementreference).find('input:checked').attr("id").split('checkbox')[1]));
    }
    checkCorrectAnswer() { //method used to validate the correct & incorrect answer
        this.assessmentData.setTextAreaAriaDisabled('true');
        this.assessmentData.setTextAreaAttr('disabled');
        this.assessmentData.resetPinanswer();
        $(this.elementreference).find('input[type="radio"]').attr('aria-disabled', 'true').prop('disabled', true);
        let selectedbtnId, rightanswerChoiceId;
        if ($(this.elementreference).find('input:checked').length === 0) {
            return false;
        }
        selectedbtnId = $(this.elementreference).find('input[type="radio"]:checked').attr("id").split('checkbox')[1];
        if(this.assessmentData.getCorrectAnswers().length){
        rightanswerChoiceId = this.assessmentData.getCorrectAnswers()[0].choiceid;
        }
        else{
            rightanswerChoiceId="12121"
        }
        if (selectedbtnId === String(rightanswerChoiceId)) {
            $(this.elementreference).find('input[type="radio"]:checked').parents('.option').removeClass('selected').addClass('rightanswer');
            $(this.elementreference).find('.alert-success').attr('role', 'alert').show();
        } else {
            $(this.elementreference).find('input[type="radio"]:checked').parents('.option').removeClass('selected').addClass('wronganswer');
            $(this.elementreference).find('.alert-danger').attr('role', 'alert').show();
        }
    }
    render(pageIndex, type, templateName) { // method returns the desired HTML which then gets rendered.
        let contentAreaClass = '';
        this.wrongAnswerCounter = 0;
        this.rightAnswerCounter = 0;
        let idValue = type + pageIndex;
        let qNumber = this.assessmentData.getQuestionNumber();
        if (templateName === 'incontext') {
            contentAreaClass = 'contentAreaClass';
        }
        this.elementreference = "#" + idValue;
        if (type === 'multipart') {
            pageIndex = pageIndex.replace("_", ".");
        }
        this.assessmentData.checkedOptionArray(this.elementreference);
        let list = `<section class="contentArea contentAreaClass" id=${idValue}>
                    <fieldset><legend>
                    <section  class="question">
                    <div class="stem">
                    <div class="qst"><span class="fixedSpan">Q${$('#prev').css('display')=='none'&&type!='multipart'?(qNumber!=null?qNumber:""):pageIndex}:&nbsp;</span>
                    ${this.assessmentData.getQuestionText()}  <div class="clearfix"></div></div>
                    </div>
                    </section>
                    <div class="instruction">${this.assessmentData.getInstructionText()}<div class="clearfix"></div></div>
                    </legend>
                    <section class="activity">
                    <div id="media" class=${this.mediaClass()}>${this.loadMedia()}</div>
                    <div class="options">${this.getChoiceList(pageIndex,this.elementreference)}</div>
                    <div class="clearfix"></div>
                    </section>
                    </fieldset>`;
        list += this.addGlobalLabelFeedBack(type)
        list += `</section>`;
        return list;
    }
    getChoiceList(pageIndex, ref) { //method build the radio button html list
        let choiceList = "",
            allChoices = this.assessmentData.getChoices();
        for (let i = 0; i < allChoices.length; i++) {
            choiceList += `<div class="col-md-12">
                            <div class="option ${this.checkItemAttemptState(allChoices[i],ref)}">
                            <div class="radio inputClass">
                            <label><input type="radio" name="optionsRadios${pageIndex+ref}" aria-disabled=${this.assessmentData.getTextAreaAriaDisabled()} id="checkbox${i + 1}" value="option${i + 1}" ${this.isItemChecked(allChoices[i],ref)} ${this.assessmentData.getTextAreaAttr()}>${allChoices[i].text}</label>
                              <div class="clearfix"></div></div>
                            <div class="sign"><div class="sr-only"><div class="clearfix"></div></div><span class="fa"></span></div>`;
            if (this.addOptionLevelFeedback(allChoices[i]) !== undefined) {
                choiceList += this.addOptionLevelFeedback(allChoices[i]);
            }
            choiceList += `</div></div>`;
        }
        return choiceList;
    }
    addOptionLevelFeedback(allChoices) { //method used to decide the option lebel feedback
        if ((allChoices.correct_feedback.replace(/^\s+|\s+$/gm, '') !== "" && allChoices.incorrect_feedback.replace(/^\s+|\s+$/gm, '') !== "") || (allChoices.correct_feedback.replace(/^\s+|\s+$/gm, '') !== '' && allChoices.assess === true) || (allChoices.incorrect_feedback.replace(/^\s+|\s+$/gm, '') !== '' && allChoices.assess === false)) {
            var html = `<div class="fdback" data-toggle="modal"><div class="sr-only" aria-hidden="true" aria-label="Click to open feedback">${allChoices.correct_feedback}|#@~${allChoices.incorrect_feedback}<div class="clearfix"></div></div><span tabindex=0 class="fa fa-commenting feedBackIcon"></span><div class="clearfix"></div></div>`
            return html;
        }
    }
}