/**
 * Created by  tarique hussain on 16-12-2016.
 */
import saveState from '../assessments/saveState';
import MainActivity from './mainActivity';
import templateProvider from './template_provider';
export default class Incontext extends MainActivity {
    constructor(assessmentData) {
        super();
        this.assessmentData = assessmentData;
        this.subQuestions = this.getSubQuestions(this.assessmentData);
        this.elementreference = null;
        this.currentIndex = null;
    }
    getSubQuestions(assessmentData) {
        let subQuestionArray = [];
        for (let i = 0; i < assessmentData.subQuestions.length; i++) {
            subQuestionArray.push(templateProvider.get(assessmentData.subQuestions[i]))
        }
        return subQuestionArray;
    }
    bindEvents(type) {
        if (type != 'multipart') {
            $("#retryMultipart").hide();
            $("#retryMultipartMobile").hide();
            $("#submitMultipart").hide();
            $("#submitMultipartMobile").hide();
        }
        for (let i = 0; i < this.subQuestions.length; i++) {
            this.subQuestions[i].bindEvents('multipart');
        }
//        $('.qst').find("li").eq(0).css({
//            'list-style-type': 'none'
//        });
        $(this.elementreference).off("click", ".sentences").on("click", ".sentences", this.openModal.bind(this));
        $(this.elementreference).off("click", ".close").on("click", ".close", this.hideModal.bind(this));
        $(this.elementreference).off("click", "#submitIncontext").on("click", "#submitIncontext", this.checkCorrectAnswer.bind(this));
        $(this.elementreference).off("click", "#retryIncontext").on("click", "#retryIncontext", this.retry.bind(this));
        $(document).off("keydown", "body").on("keydown", "body", this.detectKeyPress.bind(this));
        $("#checkAnswer,#checkAnswerMobile").hide();
        //$(document).off("click", "#myModalContext").on("click", "#myModalContext", this.detectClick.bind(this));
        $(document).off("focus", "#dummyDivContext").on("focus", "#dummyDivContext", this.shiftFocus.bind(this));
    }
    render(pageIndex, type) { // method returns the desired HTML which then gets rendered.
        let idValue = "mcq_" + pageIndex;
        let hideBtnClass = '';
        this.elementreference = "#" + idValue;
        let qNumber = this.assessmentData.getQuestionNumber();
        if (type === 'multipart') {
            pageIndex = pageIndex.replace("_", ".");
            hideBtnClass = 'hideBtn'
        }
        let list = ` <section class="contentArea" id=${idValue}>
<fieldset><legend>
                    <section  class="question">
                    <div class="stem">
                    <div class="qst"><span class="fixedSpan">Q${$('#prev').css('display')=='none'&&type!='multipart'?(qNumber!=null?qNumber:""):pageIndex}:&nbsp;</span> ${this.assessmentData.getQuestionText()}  <div class="clearfix"></div></div>
                    </div>
                    </section>
  <div class="instruction">${this.assessmentData.getInstructionText()}<div class="clearfix"></div></div>
</legend>
                    <section class="activity">
                    <div id="media" class=${this.mediaClass()}>${this.loadMedia()}</div>
                    <div id="sentenceLoader">${this.renderSentences()} <div class="clearfix"></div></div>
                    <div class="clearfix"></div>
                    </section><div id="myModalContext" class="modalContext"><div class="modal-contentContext"><button tabindex="0" class="close">x</button><div class="modal-body contextBody" >${this.loadModal()}</div><footer id='modalfooter'><button disabled id="submitIncontext" class="btn btn-primary ${hideBtnClass} ${this.assessmentData.getMultipartBtnClass()} btn btn-primary submitBtns" >Submit</button><button id="retryIncontext" class="btn btn-primary ${this.assessmentData.getMultipartRetryClass()} ${hideBtnClass}" >Retry</button></footer>  <div class="clearfix"></div></div></div><div tabindex="0" id="dummyDivContext"></div></fieldset>`
        list += `</section>`;
        return list;
    }
    renderSentences() {
        let sentences = this.assessmentData.getSentences(),
            htmlstr = "";
        sentences.map(function (value, key) {
            htmlstr += `(${key+1})&nbsp<a class="sentences" href="javascript:void(0)" index=${key+1}>${value}</a>&nbsp`;
        });
        return htmlstr;
    }
    loadModal() {
        let html = '',
            subQuestion;
        for (let i = 0; i < this.subQuestions.length; i++) {
            html += this.subQuestions[i].render((i + 1), 'incontext' + (this.elementreference).replace('#', ''), 'incontext');
        }
        return html;
    }
    openModal(event) {
        if (event.target.tagName != "A") {
            this.currentIndex = $(event.target).parents('a').attr('index');
        } else {
            this.currentIndex = event.target.getAttribute('index');
        }
        this.showModal();
    }
    hideModal(event) {
        $(this.elementreference).find('#myModalContext').hide();
        $('body').css({
            'overflow': 'auto'
        });
    }
    detectKeyPress(event) {
            if (event.keyCode === 27) {
                this.hideModal();
            } else {
                this.trapTabKey(event);
            }
        }
            detectClick() {
                if (event.target.getAttribute('id') === 'myModalContext') {
                    this.hideModal();
                }
            }
    checkCorrectAnswer(type) {
        $(this.elementreference).find('#myModalContext').find("#retryIncontext").show();
        $(this.elementreference).find('#myModalContext').find("#submitIncontext").hide();
        let isAnswerGiven = [];
        if (type === 'multipart') {
            for (let i = 0; i < this.subQuestions.length; i++) {
                isAnswerGiven.push(this.subQuestions[i].checkCorrectAnswer());
                this.subQuestions[i].updateSelectedOptions();
            }
            if (isAnswerGiven.indexOf(false) === -1) {
                return true;
            }
            return false;
        }
        this.subQuestions[this.currentIndex - 1].checkCorrectAnswer();
        this.subQuestions[this.currentIndex - 1].updateSelectedOptions();
        $("#submitIncontext").prop('disabled', true);
        $('#retryIncontext').focus();
    }
    trapTabKey(e) {
        if (e.keyCode === 9) {
            if (e.shiftKey) {
                if (document.activeElement === this.firstTabStop) {
                    e.preventDefault();
                    this.lastTabStop.focus();
                }
            } else {
                if (document.activeElement === this.lastTabStop) {
                    e.preventDefault();
                    this.firstTabStop.focus();
                }
            }
        }
    }
    showModal() {
        $(this.elementreference).find('#myModalContext').find(".contentArea").hide();
        var currentSubTemplate = "#incontext" + (this.elementreference).replace('#', '') + this.currentIndex;
        $(this.elementreference).find('#myModalContext').find(currentSubTemplate).show();
        $(this.elementreference).find('#myModalContext').show();
        var focusableElementsString = 'input:not([disabled]),[tabindex="0"]';
        var focusableElements = document.getElementById(currentSubTemplate.replace('#', '')).querySelectorAll(focusableElementsString);
        var focusableElements = Array.prototype.slice.call(focusableElements);
        var firstTabStop = $('.close')[0];
        var lastTabStop = $('#dummyDivContext')[0];
        if ($(this.elementreference).find('#myModalContext').find("#incontext" + (this.elementreference).replace('#', '') + this.currentIndex).find('.rightanswer').length > 0 || $(this.elementreference).find('#myModalContext').find("#incontext" + (this.elementreference).replace('#', '') + this.currentIndex).find('.wronganswer').length > 0) {
            $(this.elementreference).find('#myModalContext').find("#submitIncontext").hide();
            $(this.elementreference).find('#myModalContext').find("#retryIncontext").show();
        } else if ($(this.elementreference).find('#myModalContext').find("#incontext" + (this.elementreference).replace('#', '') + this.currentIndex).find('.selected').length > 0) {
            $(this.elementreference).find("#submitIncontext").prop('disabled', false);
            $(this.elementreference).find('#myModalContext').find("#submitIncontext").show();
            $(this.elementreference).find('#myModalContext').find("#retryIncontext").hide();
        } else {
            $(this.elementreference).find("#submitIncontext").prop('disabled', true);
            $(this.elementreference).find('#myModalContext').find("#submitIncontext").show();
            $(this.elementreference).find('#myModalContext').find("#retryIncontext").hide();
        }
        $('body').css({
            'overflow': 'hidden'
        });
        this.shiftFocus();
    }
    shiftFocus() {
        setTimeout(()=>{
            $('.close').focus();
        },300);
       
    }
    updateSelectedOptions() {
        return false;
    }
    retry(type) {
        $("#submitIncontext").focus();
        this.assessmentData.setMultipartFeedBackClassIncorrect('hideAnswer');
        this.assessmentData.setMultipartFeedBackClassCorrect('hideAnswer');
        $(this.elementreference).find('#myModalContext').find(".inputClass").removeClass('pointerEvent');
        $(this.elementreference).find('#myModalContext').find('.alert').hide().removeClass('show');
        $(this.elementreference).find('#myModalContext').find("#retryIncontext").hide();
        $(this.elementreference).find('#myModalContext').find("#submitIncontext").show().prop('disabled', true);;
        this.assessmentData.setMultipartRetryBtnClass('hideAnswer');
        this.assessmentData.setMultipartBtnClass('showAnswer');
        if (type === 'multipart') {
            for (let i = 0; i < this.subQuestions.length; i++) {
                this.subQuestions[i].retry();
            }
            return;
        }
        this.subQuestions[this.currentIndex - 1].retry();
    }
}