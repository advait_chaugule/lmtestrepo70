/**
 *Created by tarique hussain
 */
import MCQ from "./mcq";
export default class Fib extends MCQ {
    constructor(assessmentData) {
        super();
        this.assessmentData = assessmentData;
        this.elementreference = null;
        this.choices = this.assessmentData.getFibAnswers();
    }
    bindEvents() {
        super.bindEvents();
        $(".mobdrop-box[disabled]").addClass('pointerEvent');
        $(this.elementreference).find('.drag').draggable({
            revert: 'invalid',
            containment: '#playground',
            scroll: true,
            zIndex: 99

        });
        $(this.elementreference).find('.mobdrop-box').on('click', function() {
            if ($(this).children().hasClass('dropped')) {
                $(this).removeClass('DropFIBCanvasCSS');
                $(this).children().appendTo($('.optionContainer'));

            }
            $('.mobdrop-box').removeClass('currActive');
            $(this).addClass('currActive');
            $('.drop-list-content-mobile').show();
            $(window).trigger('resize');
        });
        $(this.elementreference).find('.mobOpts .option').off('click').on('click', (event) => {
            $(event.target).addClass('dropped');
            if (!$(event.target).parents('.commonClass').length > 0) {
                $(event.target).parent().removeClass('DropFIBCanvasCSS');
                this.assessmentData.removeDraggables(event.target.outerHTML);
                $(this.elementreference).find('.currActive').append($(event.target).parent()).addClass('DropFIBCanvasCSS dirty').attr('dataValue', $(event.target).text());
                this.assessmentData.setDataValue($(event.target).text(), $('.currActive').attr('ansIndex') - 1);
                this.assessmentData.setDirtyClass('DropFIBCanvasCSS dirty', $('.currActive').attr('ansIndex') - 1);
                this.enableCheckBtn(event);
                $('.drop-list-content-mobile').hide();
            } else {
                $(event.target).parents('.commonClass').removeClass('dirty');
                $(event.target).parents('.commonClass').attr('dataValue', '');
                this.assessmentData.setDataValue('', $('.currActive').attr('ansIndex') - 1);
                this.assessmentData.setDirtyClass('', $(event.target).parents('.commonClass').attr('ansIndex') - 1);
                $(event.target).parents('.commonClass').removeClass('DropFIBCanvasCSS');
                $(event.target).parents('.mobOpts').appendTo($('ul.divDrpItems'));
                this.assessmentData.setDirtyClass(' ', $(event.target).parents('.commonClass').attr('ansIndex') - 1);
                this.assessmentData.addDraggables($(event.target)[0].outerHTML);
            }
            this.enableCheckBtn();
            this.assessmentData.fibValues = [];
            $(this.elementreference).find('.mobdrop-box .mobOpts').each((index, item) => {
                this.assessmentData.setFibValues(item.outerHTML, $(item).parent().attr('ansIndex') - 1);
            });
        });
        if ($(this.elementreference).find('.drag').parent().attr('disabled')) {
            $(this.elementreference).find('.drag').draggable('disable');
        } else {
            $(this.elementreference).find('.drag').draggable('enable');
        }
        $(this.elementreference).find('.mobOpts').find('.drag').draggable('disable');
        $(this.elementreference).find('.divDrpItems').droppable({
            accept: '.drag',
            tolerance: 'pointer',
            activeClass: "activeDrops",
            hoverClass: "hoverOnDrops",
            drop: (event, ui) => {
                ui.draggable.css({
                    'top': 0,
                    'left': 0
                });
                $(ui.draggable).removeClass('ui-draggable-dragging')
                if (!(ui.draggable.parents().hasClass('divDrpItems') && $(event.target).hasClass('divDrpItems'))) {
                    this.assessmentData.addDraggables(ui.draggable[0].outerHTML);
                    this.assessmentData.resetFibValues(ui.draggable.parent().attr('ansIndex') - 1);
                    ui.draggable.parents('.drop-box').attr('datavalue', '');
                    ui.draggable.parents('.drop-box').removeClass('dirty');
                }

                this.assessmentData.setDirtyClass(' ', ui.draggable.parent().attr('ansIndex') - 1);
                ui.draggable.appendTo(event.target);
                $(window).trigger('resize');
                //$(this.elementreference).find('.drop-box .drag').addClass('dirty');
                this.enableCheckBtn();

            }
        });
        $(this.elementreference).find('.drop-box').droppable({
            accept: '.drag',
            tolerance: 'pointer',
            activeClass: "activeDrops",
            hoverClass: "hoverOnDrops",
            drop: (event, ui) => {
                let elm = window.event ? window.event.target : (e) ? e.target : '';
                if ($(event.target).children().length > 0) {
                    if (!(ui.draggable.parents().hasClass('drop-box') && $(event.target).hasClass('drop-box'))) {
                        this.assessmentData.addDraggables($(event.target).children()[0].outerHTML);
                    }
                    ui.draggable.parent().attr('dataValue', $(event.target).children().text());
                    $(event.target).children().appendTo(ui.draggable.parent());
                }
                if ((ui.draggable.parents().hasClass('drop-box') && $(event.target).hasClass('drop-box'))) {
                    this.assessmentData.resetFibValues(ui.draggable.parent().attr('ansIndex') - 1);
                }
                this.assessmentData.setDirtyClass(' ', ui.draggable.parent().attr('ansIndex') - 1);
                ui.draggable.parents('.drop-box').attr('datavalue', '');
                ui.draggable.appendTo(event.target);

                this.assessmentData.setDirtyClass('dirty', $(event.target).attr('ansIndex') - 1);
                ui.draggable.css({
                    'top': 0,
                    'left': 0
                });
                $(event.target).attr('dataValue', ui.draggable.text());
                this.assessmentData.setDataValue(ui.draggable.text(), $(event.target).attr('ansIndex') - 1);
                this.assessmentData.removeDraggables(ui.draggable[0].outerHTML);
                $(this.elementreference).find('.dirty').removeClass('dirty');
                $(this.elementreference).find('.drop-box .drag').parent().addClass('dirty');
                $(this.elementreference).find('.drop-box .drag').each((index, item) => {
                    $(item).removeClass('ui-draggable-dragging');
                    this.assessmentData.setFibValues(item.outerHTML, $(item).parent().attr('ansIndex') - 1);
                });
                this.enableCheckBtn();
                $(window).trigger('resize');
            }
        });
        $(this.elementreference).off("change", "select").on("change", "select", (e, t) => {
            let elm = window.event ? window.event.target : (e) ? e.target : '';
            if (elm.value !== "") {
                $(elm).addClass("dirty");
            } else {
                $(elm).removeClass("dirty");
            }
            //  $(event.target).attr('dataValue',  $(event.target).val());
            let index = elm.getAttribute('ansindex') - 1;
            this.assessmentData.setFibValues(elm.value, index);
            if (elm.value.length > 0) {
                this.assessmentData.setDirtyClass('dirty', $(elm).attr('ansIndex') - 1);
            } else {
                this.assessmentData.setDirtyClass(' ', $(elm).attr('ansIndex') - 1);
            }
            this.enableCheckBtn(event);
        });
        $(this.elementreference).off("keyup").on("keyup", (e, t) => {
            this.deleteFun(e);
        });
        $(this.elementreference).off("keydown input paste").on("keydown input paste", (e, t) => {

            let charCode = (window.event) ? window.event.keyCode : (e) ? e.which : "",
                inputType = $(event.target).attr('inputType'),
                eventType = event.type,
                val = $(event.target).val(),
                elm = event.target;

            if (charCode == 8 || charCode == 9) {
                this.checkEmptyVal(event);
            } else {
                switch (inputType) {
                    case 'numeric':
                        {
                            if ((charCode >= 48 && charCode <= 57) || charCode === 32 || (charCode >= 186 && charCode <= 222) || eventType === "paste") {

                                $(elm).addClass("dirty");
                            } else {
                                this.checkEmptyVal(event);
                                return false;
                            }
                            break;
                        }
                    case 'alphanumeric':
                        {
                            if (e.ctrlKey && charCode) {
                                return false;
                            } else if ((charCode >= 48 && charCode <= 57) ||
                                (charCode >= 65 && charCode <= 90) ||
                                (charCode >= 97 && charCode <= 122) ||
                                (charCode >= 186 && charCode <= 222) ||
                                charCode === 32 ||
                                eventType === "paste") {
                                $(elm).addClass("dirty");
                            } else if ((charCode >= 37 && charCode <= 40) || charCode === 46) {
                                this.checkEmptyVal(event);
                                return true;
                            } else {
                                this.checkEmptyVal(event);
                                return false;
                            }
                            break;
                        }
                    case 'alphabet':
                        {
                            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode >= 186 && charCode <= 222) || charCode === 32 || eventType === "paste") {
                                $(elm).addClass("dirty");
                            } else {
                                this.checkEmptyVal(event);
                                return false;
                            }
                            break;
                        }
                    default:
                        return true;
                }
            }
            let index = elm.getAttribute('ansindex') - 1;
            this.assessmentData.setFibValues(elm.value, index);
            return this.enableCheckBtn(event);
        });
        $(this.elementreference).off("blur input").on("blur input", (e, t) => {
            let elm = window.event ? window.event.target : (e) ? e.target : '';
            let index = elm.getAttribute('ansindex') - 1;
            this.assessmentData.setFibValues(elm.value, index);
            $(event.target).attr('dataValue', $(event.target).val());
            this.assessmentData.setDataValue($(event.target).val(), $(event.target).attr('ansIndex') - 1);
            if (elm.value.length > 0) {
                this.assessmentData.setDirtyClass('dirty', $(elm).attr('ansIndex') - 1);
            } else {
                this.assessmentData.setDirtyClass(' ', $(elm).attr('ansIndex') - 1);
            }
        });
    }

    checkEmptyVal(evt) {
        if (($(evt.target).val().length === 1 && ($(evt.target).hasClass("dirty") && evt.keyCode === 8 || $(evt.target).hasClass("dirty") && evt.keyCode === 46)) || $(evt.target).val().length === window.getSelection().toString().length) {
            $(evt.target).removeClass("dirty");
            this.enableCheckBtn(evt);
        }
    }
    deleteFun(evt) { //method used to enable del btn functionality
        if ($(evt.target).val().length) {
            $(evt.target).addClass("dirty");
            this.enableCheckBtn(evt);
        }
    }
    updateSelectedOptions() {}
    checkCorrectAnswer() { //method used to validate the correct & incorrect answer
        // $(this.elementreference).find('.drag').addClass('pointerEvent');
        $(this.elementreference).find('.mobdrop-box').addClass('pointerEvent');
        $(this.elementreference).find('.commonClass:visible').each((index, item) => {
            let currentOption = this.choices[$(item).attr('ansindex') - 1].map(function(a) {
                return a.text.toLowerCase();
            });
            $(item).attr('disabled', true);
            $(this.elementreference).find('.drag:visible').draggable('disable');
            if ($.inArray($(item).attr('dataValue').toLowerCase(), currentOption) !== -1) {
                $(item).parent().addClass('rightanswer');
                this.assessmentData.setFibClasses('rightanswer');
                this.assessmentData.setFibValues($(item).val());
            } else {
                $(item).parent().addClass('wronganswer');
                this.assessmentData.setFibClasses('wronganswer');
            }
            this.assessmentData.setDisableProp('disabled');
        });
        if ($(this.elementreference).find('.wronganswer:visible').length > 0) {
            $(this.elementreference).find('.alert-danger').attr('role', 'alert').show();
        } else {
            $(this.elementreference).find('.alert-success').attr('role', 'alert').show();
        }
    }
    enableCheckBtn(event) {

        if ($(this.elementreference).find('.dirty:visible').length) {
            $("#checkAnswer").attr('disabled', false);
            $("#checkAnswerMobile").attr('disabled', false);
        } else {
            $("#checkAnswer").attr('disabled', true);
            $("#checkAnswerMobile").attr('disabled', true);
        }
        return true;
    }
    render(pageIndex, type, templateName) { // method returns the desired HTML which then gets rendered.
        let contentAreaClass = '';
        this.wrongAnswerCounter = 0;
        this.rightAnswerCounter = 0;
        let idValue = type + pageIndex;
        let qNumber = this.assessmentData.getQuestionNumber();
        this.elementreference = "#" + idValue;
        if (type === 'multipart') {
            pageIndex = pageIndex.replace("_", ".");
        }
        this.assessmentData.checkedOptionArray(this.elementreference);
        let list = `<section class="contentArea contentAreaClass" id=${idValue}>
                <fieldset><legend>
                <section  class="question">
                <div class="stem">
                <div class="qst"><span class="fixedSpan">Q${$('#prev').css('display')=='none'&&type!='multipart'?(qNumber!=null?qNumber:""):pageIndex}:&nbsp;</span> ${this.assessmentData.getQuestionText()}</div>
                </div>
                </section>
                <div class="instruction">${this.assessmentData.getInstructionText()}</div>
                </legend>${this.assessmentData.setFibQuestions(this.assessmentData.getFibRawText()) || ''}
                <section class="activity ${this.assessmentData.getFibQuestionTextMob().search("div")>-1?'desk':''}">
                <div id="media" class=${this.mediaClass()}>${this.loadMedia()}</div>
                <div class="options">${this.assessmentData.getFibQuestionText()}</div>
                <div class="dragContainer">${JSON.stringify(this.assessmentData).search('dragdrop')!=-1?                                       this.renderDraggableContent():''}</div>
                <div class="clearfix"></div>
                </section>
                <section class="activity mob ${this.assessmentData.getFibQuestionTextMob().search("div")>-1?'':'hide-elm'}">
                <div id="media"></div>
                <div class="options">${this.assessmentData.getFibQuestionTextMob()}</div>
                <div class="dragContainer"></div>
                <div class="fib drop-list-content-mobile" style="display:none">
                <button type="button" class="minimize"><i class="fa fa-chevron-circle-down"></i></button>
                <h6>Choose A Word</h6>
                <ul class="divDrpItems">
                ${this.renderOptions()}
                </ul>
                </div>
                <div class="clearfix"></div>
                </section>
                </fieldset>`;
        list += this.addGlobalLabelFeedBack(type);
        list += `</section>`;
        return list;
    }
    renderOptions() {
        let html = ``;
        let dragElems = this.assessmentData.getDraggables();
        dragElems.map(function(i, k) {
            i.map(function(item) {
                html += `<li class="mobOpts">${item}</li>`;
            });
        });
        return html;
    }
    renderDraggableContent() {
        let html = `<div class="fib drop-list-content  dragpanel col-sm-12 col-md-12 top-matching" style="min-height:50px;">            <div class="arrow up"></div><div class="drag-container " style="min-height:50px;"><div class="divDrpItems" data-              containment=".contentArea" data-drag-active="true" data-draggable="" data-drag-id="0" data-drag-class="drag-class"            data-drop-class="drop-class-2" data-clone-drop="1" data-max-clone-drop="1">`;
        let dragElems = this.assessmentData.getDraggables();
        dragElems.map(function(i, k) {
            i.map(function(item) {
                html += item;
            });
        });
        html += `</span></div></div>`;
        return html;
    }
    retry() {
        $(this.elementreference).find('.drag').draggable('enable');
        $('.commonClass').attr('dataValue', '');
        //$('.commonClass').attr('dataValue','');

        //  mobdrop-box
        //$(this.elementreference).find('.mobdrop-box').removeClass('pointerEvent');
        $(this.elementreference).find('.mobOpts>.drag').draggable('disable');
        $(this.elementreference).find('.commonClass').find('.mobOpts:visible').appendTo($('ul.divDrpItems'));
        $(this.elementreference).find('.commonClass').find('.drag:visible').appendTo($('div.divDrpItems'));
        $(this.elementreference).find('.fibclass').removeClass('wronganswer');
        $(this.elementreference).find('.fibclass').removeClass('rightanswer');
        $(this.elementreference).find('.commonClass').attr('disabled', false).removeClass('DropFIBCanvasCSS dirty pointerEvent');
        $(this.elementreference).find('.commonClass').val("");
        this.assessmentData.setDisableProp('enabled');
        this.assessmentData.setFibClasses('empty');
        this.assessmentData.setFibValues('empty');
        this.assessmentData.draggables = [];
        $(this.elementreference).find('.commonClass').each((indx, item) => {
            this.assessmentData.setDirtyClass(' ', $(item).attr('ansIndex') - 1);
            this.assessmentData.setDataValue('', $(item).attr('ansIndex') - 1);
        });
        $(this.elementreference).find('.mobOpts>.drag').each((ind, item) => {
            this.assessmentData.addDraggables(item.outerHTML);
        });
        //        $(this.elementreference).find('.drag:visible').not('.mobOpts>.drag').each((ind, item) => {
        //            this.assessmentData.addDraggables(item.outerHTML);
        //        });
    }
}