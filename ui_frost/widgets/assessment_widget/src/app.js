import AssessmentController from "./assessments/assessment";
import activityDataFactory from "./data/activity_data_factory";
import setup from "./btn_setting";
window.iframeId;
$(window).on("message", function (data) { // method recieves data from parent i.e frame
   
    if (!data.originalEvent.data.data_path) {
        return;
    }
    injectScript(data.originalEvent.data.data_path);
    window.iframeId=data.originalEvent.data.id;
});

function initializeAssessment() { // app bootstraps from here
    dataValidator();
    let allQuestions = activityDataFactory.getActivityDataFromJSON(advJsonData.quiz);
   
    let assessmentController = new AssessmentController();
    assessmentController.init(allQuestions);
    assessmentController.getInitialQuestion();
}

function injectScript(src){ //method for injecting the data script into head
   if($("script[src='"+src+"']").length === 0){
    let dataScript  = document.createElement('script');
    dataScript.type = 'text/javascript';
    dataScript.src = src;
    document.getElementsByTagName('head')[0].appendChild(dataScript);
    dataScript.onload = initializeAssessment;
    dataScript.error = function (e) {
        throw e;
    };
   }
  }

function dataValidator() { // this method scans questions
    switch (advJsonData.quiz.length) {
    case 0:
        $("#noQuestionAlert").show();
        $('footer').hide();
        return false;
        break;
    case 1:
        setup.hideNavigation();
        break;
    default:
        setup.loadFooterPanel();
    }
}
$(document).ready(function () { //method posts data  to top i.e outer parent
    top.postMessage("getDefaultData", "*");
     parent.postMessage("getDefaultData", "*");
});