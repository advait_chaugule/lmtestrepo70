import saveState from './saveState';
export default class NavigationController {
    constructor(assessmentControllerInstance) {
        this.assessmentController = assessmentControllerInstance;
        this.allQuestions = null;
        this.pageIndex = -1;
        saveState.setAssesmentCtrl(assessmentControllerInstance);
        this.selector = {
            body:$("body"),
            prev:$("#prev"),
            prevMobile:$("#prevMobile"),
            next:$("#next"),
            nextMobile:$("#nextMobile")
        };
    }
    init(data) { // initialises data
        this.allQuestions = data;
        this.bindEvents();
    }
    bindEvents() { //method binds events to next and previous button 
        this.selector.body.off("click", "#next").on("click", "#next", this.next.bind(this));
        this.selector.body.off("click", "#nextMobile").on("click", "#nextMobile", this.next.bind(this));
        this.selector.body.off("click", "#prev").on("click", "#prev", this.prev.bind(this));
        this.selector.body.off("click", "#prevMobile").on("click", "#prevMobile", this.prev.bind(this));
    }
    next() { // method used to navigate to next button
    let currentQuestion = this.allQuestions[++this.pageIndex];
      saveState.performBasicNavigationCheck(currentQuestion, this.pageIndex,'next');
        if (this.pageIndex > 0) {
            this.selector.prev.prop('disabled', false).attr('aria-disabled',false);
            this.selector.prevMobile.prop('disabled', false).attr('aria-disabled','false');
        }
        if (this.pageIndex === this.allQuestions.length - 1) {
            this.selector.next.prop('disabled', true).attr('aria-disabled','true');
            this.selector.nextMobile.prop('disabled', true).attr('aria-disabled','true');
        }
    }
    prev() { // method used  to navigate to prev button
        let currentQuestion = this.allQuestions[--this.pageIndex];
        saveState.performBasicNavigationCheck(currentQuestion, this.pageIndex,'prev');
        if (this.pageIndex === 0) {
            this.selector.prev.prop('disabled', true).attr('aria-disabled','true');
            this.selector.prevMobile.prop('disabled', true).attr('aria-disabled','true');
        }
        if (this.pageIndex < this.allQuestions.length) {
            this.selector.next.prop('disabled', false).attr('aria-disabled','false');
            this.selector.nextMobile.prop('disabled', false).attr('aria-disabled','false');
        }
    }
};