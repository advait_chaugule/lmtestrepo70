/**
 * Created by debayan.das on 26-09-2016.
 */
/** 
 *edited by tarique hussain
 */
import templateProvider from "../templates/template_provider";
import CheckAnswerContoller from "./checkAnswer";
import NavigationController from "./navigation";
export default class AssessmentController {
    constructor() { //initialises variables
        this.currentTemplate = null;
        this.navigator = new NavigationController(this);
        this.checkAnswer = new CheckAnswerContoller(this);
        this.correctAnswers = null;
    }

    init(assessmentData) { // calls init function of different components with assessment data
        this.navigator.init(assessmentData);
        this.checkAnswer.init(assessmentData);
    }
    loadQuestion(question, pageIndex) { // method loads question i.e previews them
        this.currentTemplate = templateProvider.get(question);
        $("#playground").html(this.currentTemplate.render(pageIndex + 1, 'assessment'));
        setTimeout(()=>{
        this.resizeIframe('resized');
        },1000);
      	try{
        MathJax.Hub.Queue(["Typeset", MathJax.Hub, "playground"]);
		}
		catch(err){
		
        }
		
        this.currentTemplate.bindEvents();
        this.bindEvents();
    }
    resizeIframe(param,adjustIframe){// resize iFrame according to content size

         let needScroll;
        if(param=="resized"){
            needScroll=false;
        }
        else{
            needScroll=true;
        }
         if($('.multi-part-main').length<1){
                    if($(this).hasClass('sentences')||$('.modalContext').css('display')=="block"||$('.fdbackmodal  ').css('display')=="block"||adjustIframe=='adjustIframe'){
            //needScroll=false;
            let height;
                 if($('.fdbackmodal ').css('display')=="block"){
                     height =$('.modal-content').height()+150;
                 }
           else{
                  height = 427;
           }
                        
           window.parent.postMessage({"height": height,"overflow":'hidden',"id":window.iframeId,"scroll":needScroll,"type":adjustIframe}, "*");
           return;
        }
         }

           let height = $("body").height()+10;
           window.parent.postMessage({"height": height,"overflow":'hidden',"id":window.iframeId,"scroll":needScroll,"type":adjustIframe}, "*");
    }
    getInitialQuestion() { // method gets the very first question
        this.navigator.next();
    }
    getCorrectAnswer() { //method used to get Correct Answer
        this.currentTemplate.checkCorrectAnswer();
    }
    retry() { // method used to give user a preference to retry for particular question
        this.currentTemplate.retry();
    }
    checkAndUpdateSelectedOptions() { // checks and updates the selected options given by user
        if (this.currentTemplate) {
            this.currentTemplate.updateSelectedOptions();
        }
    }
    bindEvents(){
         let i = 0;
         $('img').each((key,item)=>{
            var img = new Image();
            img.onload =()=> {
                i++;
                if (i == $('img').length)
                    this.resizeIframe();
            }
            img.src = $(item).attr('src');
        });
    
         $('.feedBackIcon').on('click',()=>{
            setTimeout(()=>{
                this.resizeIframe('','adjustIframe');
                   $('.fdbackmodal ').modal({backdrop: 'static'}); 
            },300);
        });
        $('#next,#nextMobile,#prev,#prevMobile,#retry,#retryMobile,#retryMultipart,#retryMultipartMobile').on('click',()=>{
            setTimeout(this.resizeIframe,600);
         });
        $(window).on('resize', ()=>{
            this.resizeIframe('resized','resizing');
        });
        $('.sentences').on('click',()=>{
            this.resizeIframe('','adjustIframe');
        });
         $('.modalContext .close').on('click',()=>{
            setTimeout(()=>{
                this.resizeIframe('', 'close');
            }, 300);
        });
        $('.modal-dialog .close').on('click',()=> {
            setTimeout(()=>{
                this.resizeIframe('', 'adjustIframe');
            }, 300);
        });
        $('.btn').on('click',()=>{
            setTimeout(()=>{
                this.resizeIframe('resized','');
            },300);
        });

         
    }
    
};
      