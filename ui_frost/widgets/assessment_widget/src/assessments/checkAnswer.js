/** 
 *edited by tarique hussain
 */
export default class CheckAnswerController {
    constructor(assessmentControllerInstance) {
        this.assessmentController = assessmentControllerInstance;
        this.allQuestions = null;
        this.selector = {
            body:$("body"),
            retry:$("#retry"),
            retryMobile:$("#retryMobile"),
            checkAnswer:$("#checkAnswer"),
            checkAnswerMobile:$("#checkAnswerMobile")
        };
    }
    init(assessmentData) { // this method initialises data
        this.allQuestions = assessmentData;
        this.bindEvents();
    }
    bindEvents() { // binds click event to check answer button
        this.selector.body.off("click", "#checkAnswer").on("click", "#checkAnswer", this.checkAnswer.bind(this));
        this.selector.body.off("click", "#checkAnswerMobile").on("click", "#checkAnswerMobile", this.checkAnswer.bind(this));
        this.selector.body.off("click", "#retry").on("click", "#retry", this.retry.bind(this));
        this.selector.body.off("click", "#retryMobile").on("click", "#retryMobile", this.retry.bind(this));
    }
    checkAnswer() { //method used to get response 
       // this.assessmentController.resizeIframe();
        this.selector.retry.show();
        this.selector.retryMobile.show();
        this.selector.checkAnswer.hide();
        this.selector.checkAnswerMobile.hide();
        $(".inputClass").addClass('pointerEvent');
        this.assessmentController.checkAndUpdateSelectedOptions();
        this.assessmentController.getCorrectAnswer();
    }
    retry() { // method used to give user a preference to retry for particular question
       // this.assessmentController.resizeIframe();
        this.selector.retry.hide();
        this.selector.retryMobile.hide();
        this.selector.checkAnswer.show().attr('disabled',true);
        this.selector.checkAnswerMobile.show().attr('disabled',true);;
        $('#modalBodyPanel').html('');
        $(".inputClass").removeClass('pointerEvent');
        $('.alert').hide().removeClass('show');
        this.assessmentController.retry();
    }
};