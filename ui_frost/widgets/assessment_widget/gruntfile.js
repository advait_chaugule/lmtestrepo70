/**
 * Created by debayan.das on 26-09-2016.
 */

module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['bower_components/jquery/dist/jquery.min.js', 'bower_components/bootstrap/dist/js/bootstrap.min.js','bower_components/jquery-ui/jquery-ui.min.js'],
                dest: 'build/lib.js'
            }
        },
        browserify: {
            dist: {
                options: {
                    transform: [
                        ["babelify", {
                                loose: "all"
                            }]
                    ]
                },
                files: {
                    'build/main.js': 'src/app.js'
                }
            }
        },
        watch: {
            scripts: {
                files: ["./src/**/*.js"],
                tasks: ["browserify"]
            }
        },
        compress: {
            main: {
                options: {
                    archive: 'build/assessment.zip'
                },
                files: [
                    {src: ['index.html'], dest: '/', filter: 'isFile'}, // includes files in path
                    {src: ['build/main.js', 'build/lib.js'], dest: '/'}, // includes files in path and its subdirs
                    {src: ['css/*.css'], dest: '/'}, // includes files in path and its subdirs
                    {src: ['fonts/*.*'], dest: '/'} // includes files in path and its subdirs
                ]
            }
        }
    });
    grunt.registerTask("default", ["watch"]);
    grunt.registerTask("build", ["concat", "browserify"]);
};