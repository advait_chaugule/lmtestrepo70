/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () { // method,this function posts message to iFrame content window
    $(window).on("message", function (data) {
        //mock implementation of parents message listener
        
            var iframeElement = $("#parentFrame");
                if(data.originalEvent.data.height){
                $("#"+data.originalEvent.data.id).css({'height': data.originalEvent.data.height,overflow:data.originalEvent.data.overflow});
                     var scrollTo=$("#"+data.originalEvent.data.id).offset().top;
                    $(window).scrollTop(scrollTo)
                }
                iframeWindow = iframeElement[0].contentWindow;
            var dataPath = iframeElement.attr("data-path");
            var id=iframeElement.attr("id");
            iframeWindow.postMessage({
                "data_path": dataPath,
                "id":id
            }, "*");
        
    });

});