﻿app.directive('fibvariation', function () {
    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            model: "=ngModel",
            UserInput: "=userInput",
            ParentCtrl: "=parentCtrl"
        },
        replace: true,
        templateUrl: 'FIBdirective.html',
        link: function (scope, element, attrs, ngModel) {
            scope.OnDragStart = function (option) {
                option.AnswerClass = "";
            },
            scope.CheckInputValue = function (e) {                
                var keyCode = e.which ? e.which : e.keyCode;
                ngModel.$modelValue.AnswerClass = "";
                var decimalplaces = (ngModel.$modelValue.decimalplace == "") ? 0 : ngModel.$modelValue.decimalplace;              
                if (!scope.ValidateUserInput(keyCode, ngModel.$modelValue.inputtype, decimalplaces)) {
                    e.preventDefault();
                    return false;
                }
            },
             scope.RemoveSelected = function () {                 
                 var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
                 if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
             },
            scope.showOptionFeedback = function (ParentCtrl, optionId) {
                  ParentCtrl.showOptionFeedback(optionId);
            },
            scope.CheckSelectedValue = function (value) {
                ngModel.$modelValue.UserInput = value;
                ngModel.$modelValue.AnswerClass = "";
            },          
            scope.CheckDropItem=function(event, index, item, type)
            {
                if (item.QuestionIndex != index) {
                    return false;
                }
                else {
                    return item;
                }
            },
            scope.ValidateUserInput = function (keyCode, inputtype, maxDecimals) {
                var resultType = false;
                var regexp = "";
                var specialKeys = new Array();
                specialKeys.push(8); //Backspace
                specialKeys.push(37); //left
                specialKeys.push(38); //up
                specialKeys.push(39); //right
                specialKeys.push(40); //down
                specialKeys.push(9); //Tab key
                specialKeys.push(32);//space bar
                switch (inputtype) {
                    case 'alphanumeric':
                        if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                            resultType = true;
                        break;
                    case 'numeric':
                        if (maxDecimals > 0) {
                            specialKeys.push(46); //decimal
                            regexp = "^-?\\d*\\.?\\d{0," + maxDecimals + "}$";
                        } else {
                            regexp = "^-?\\d*$";
                        }
                        if ((keyCode >= 48 && keyCode <= 57) || (specialKeys.indexOf(keyCode) != -1)) {
                            var validRegex = new RegExp(regexp);
                            resultType = validRegex.test(ngModel.$modelValue.UserInput);
                        }
                        break;
                    case 'alphabet':                        
                        if ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                            resultType = true;
                        break;
                    default:
                        break;
                }
                return resultType;
            }

        }
    }

});
