﻿app.directive('loading', ['$http', '$window', '$cookieStore', '$rootScope', function ($http, $window, $cookieStore, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            // if ($rootScope.token != undefined) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;

            };

            scope.$watch(scope.isLoading, function (v) {
                if (v) {
                    elm.show();
                    elm.removeClass('hide');
                } else {
                    elm.hide();
                }
            });
            //}
        }
    };
}]);




