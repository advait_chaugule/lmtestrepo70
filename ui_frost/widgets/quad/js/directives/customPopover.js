app.directive('popover', function () {
    return {
        restrict: 'A',
		template: '<span math dynamic></span>',
        link: function (scope, el, attrs, compile) {
			if(attrs.popoverHtml != undefined && attrs.popoverHtml != null && attrs.popoverHtml.trim() != "")
			{
				var choiceData;
				
				if(scope.$parent.CurrentQuestion != undefined)
				{
					if(scope.$parent.CurrentQuestion.QuestionData.choices != undefined)
						choiceData = scope.$parent.CurrentQuestion.QuestionData.choices;
				}
				else if(scope.$parent.ParentCtrl.CurrentQuestion !=undefined)
				{
					if(scope.$parent.ParentCtrl.CurrentQuestion.QuestionData.choices != undefined)
						choiceData = scope.$parent.ParentCtrl.CurrentQuestion.QuestionData.choices;
				}
				
				if(choiceData != undefined && choiceData.length > 0)
				{
					$(el).popover({
						trigger: 'click',
						html: true,
						content: choiceData[attrs.popoverHtml - 1].feedback,//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
						placement: "top"
					});
					
					$('.container-main').click(function(e){
						if (e.target.className != null && e.target.className != undefined && e.target.className.trim() != "" && e.target.className.trim() != 'fa fa-commenting-o')
						{
							$('.popover').hide(); 
						}
					});
				}
		}
        }
    };
});