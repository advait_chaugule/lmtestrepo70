﻿app.controller('CompositeEngineCtrl', function ($scope, $location, $window, $http, assessmentEngineSvc, Enum, $modal, $sce, $filter, $compile, $timeout, $modal, SweetAlert) {
	//$scope.template = '';
	

    $scope.init = function () {       
        //$scope.Questions = assessmentEngineSvc.GetCompositeQuestions();
        //$scope.template = 'Composite.html';
		$scope.Questions = $scope.$parent.CurrentQuestion.QuestionData.questions;     
        $scope.AllSelectedOptions = $scope.$parent.CompositeSelectedOption;
        $.each($scope.Questions, function (index, value) {
            $scope.Questions[index].Index = index;
            if (!$scope.isUndefined(value.stem_details) && value.stem_details.type == 'algorithimic') {
                $scope.SetAlgorithimicType(value);
            }
            if (value.question_type.text == Enum.QuestionType.FIB || value.question_type.text == Enum.QuestionType.FIBDND || value.question_type.text == Enum.QuestionType.Matching)
            {
                $scope.SetFIBType(value, index);
            }
            else if(value.question_type.text == Enum.QuestionType.Essay)
            {
                $scope.Questions[index].EssayText = '';
            }           
            angular.forEach(value.choices, function (option, key) {
                $scope.Questions[index].choices[key].currentClass = "";
                $scope.Questions[index].choices[key].QuestionIndex = index;                
            });
            if ($scope.AllSelectedOptions != null && !$scope.isUndefined($scope.AllSelectedOptions) && $scope.AllSelectedOptions.length >= index) {
               
                $scope.setCompositeQuestionOptions(value, $scope.AllSelectedOptions[index].UserSelectedOptions,index);
            }
        });
        $scope.$parent.compositeQuestions = $scope.Questions;

    };

    $scope.CheckInputValue = function (e, value) {
        var keyCode = e.which ? e.which : e.keyCode;
        value.AnswerClass = "";
        var decimalplaces = (value.decimalplace == "") ? 0 : value.decimalplace;
        if (!$scope.ValidateUserInput(keyCode, value.inputtype, decimalplaces, value)) {
            e.preventDefault();
            return false;
        }
    };
    $scope.ValidateUserInput = function (keyCode, inputtype, maxDecimals, value) {
        var resultType = false;
        var regexp = "";
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(37); //left
        specialKeys.push(38); //up
        specialKeys.push(39); //right
        specialKeys.push(40); //down
        specialKeys.push(9); //Tab key
        specialKeys.push(32);//space bar
        switch (inputtype) {
            case 'alphanumeric':
                if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                    resultType = true;
                break;
            case 'numeric':
                if (maxDecimals > 0) {
                    specialKeys.push(46); //decimal
                    regexp = "^-?\\d*\\.?\\d{0," + maxDecimals + "}$";
                } else {
                    regexp = "^-?\\d*$";
                }
                if ((keyCode >= 48 && keyCode <= 57) || (specialKeys.indexOf(keyCode) != -1)) {
                    var validRegex = new RegExp(regexp);
                    resultType = validRegex.test(value.UserInput);
                }
                break;
            case 'alphabet':
                if ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                    resultType = true;
                break;
            default:
                break;
        }
        return resultType;
    };
	
	$scope.$on('nextCompositeQuestion', function() {
	  //console.log('nextCompositeQuestion');
	  $scope.init();
    })
	
    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.trustAsHtml = function (string) {
        return $sce.trustAsHtml(string);
    };

    $scope.setOptions = function (option, OptionId,questionIndex) {       
        $(".option").removeClass('correct');
        $(".option").removeClass('incorrect');
        var questionType = '';
        if (!$scope.isUndefined(option.QuestionIndex)) {
            questionType = $scope.Questions[option.QuestionIndex].question_type.text;
        }
        else {
            questionType = $scope.Questions[questionIndex].question_type.text;
        }
        switch (questionType) {
            case Enum.QuestionType.MCSS:
            case Enum.QuestionType.GRAPHIC:
            case Enum.QuestionType.Exhibit:
                $($scope.Questions[option.QuestionIndex].choices).each(function (index) {
                    this.currentClass = "";
                    if (this.choiceid == option.choiceid) {
                        this.isAnswered = true;
                        option.currentClass = "selected";
                    }
                    else {
                        this.isAnswered = false;
                    }
                    this.isButtonClicked = true;
                    if (this.isAnswered) {
                        $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                        this.currentClass = "selected";
                    }
                });

                break;
            case Enum.QuestionType.MCMSVIDEO:
            case Enum.QuestionType.MCMS:
                $($scope.Questions[option.QuestionIndex].choices).each(function (index) {
                    if (this.choiceid == option.choiceid) {
                        if (this.isAnswered == true) {
                            this.isAnswered = false;
                            option.currentClass = "";
                        }
                        else {
                            this.isAnswered = true;
                            option.currentClass = "selected";
                        }
                    }
                    if (this.isAnswered == true) {
                        this.currentClass = "selected";
                        $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                    }
                });
                break;
            case Enum.QuestionType.ChoiceMatrix:
                $($scope.Questions[questionIndex].choices).each(function (index, value) {
                    $scope.Questions[questionIndex].selectedOption = OptionId;

                    $(value.suboption).each(function (sindex, soption) {
                        if (value.choiceid == OptionId) {
                            value.isChoiceAnswered = true;
                            if (option.identifier == soption.identifier) {
                                this.isOptionAnswered = true;
                                value.isAnswered = value.assess;
                                value.suboptionSelected = soption.identifier;
                                soption.isAnswered = soption.value;
                                soption.currentClass = "selected";
                            }
                            else {
                                this.isOptionAnswered = false;
                                soption.currentClass = "";
                                soption.isAnswered = !soption.value;
                            }
                        }
                        if (this.isOptionAnswered == true) {
                            this.currentClass = "selected";
                            $scope.Questions[questionIndex].userHasSelectedOptions = true;
                        }
                    });
                });
                break;
            case Enum.QuestionType.Order:
                $(option).each(function (index) {
                    this.isAnswered = true;
                    if (this.isAnswered) {
                        $scope.Questions[questionIndex].userHasSelectedOptions = true;
                    }
                });
                break;
            case Enum.QuestionType.FIB:
            case Enum.QuestionType.FIBDND:
            case Enum.QuestionType.Matching:
                var totalChoices = $scope.Questions[option.QuestionIndex].choices.length;
                var objList = $filter('filter')($scope.Questions[option.QuestionIndex].choices, { "Canvas": [] }, true);
                var objList2 = $filter('filter')($scope.Questions[option.QuestionIndex].choices, { "UserInput": "" }, true);
                if (objList.length < totalChoices)
                    $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                if (objList2.length < totalChoices)
                    $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                break;
        }
    }

    $scope.setCompositeQuestionOptions = function (question, selectedOptions, questionIndex) {
        $scope.text = '';
        if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
            if (selectedOptions) {
                var selectedOpt = angular.fromJson(selectedOptions);
                $(selectedOpt).each(function (selectindex, selectvalue) {
                    var inputValues = selectvalue.inputvalue.split("-");
                    $(question.choices).each(function (index) {
                        var optionindex = index + 1;                       
                        $(question.choices[index].suboption).each(function (index1, value1) {
                            question.choices[index].suboption[index1].currentClass = "";
                            if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                                question.choices[index].suboption[index1].isAnswered = true;
                            }
                        });

                    });
                });

            }

        }
        if (question.question_type.text == Enum.QuestionType.Matching || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.FIB) {
            if (selectedOptions) {
                var selctdOpt = angular.fromJson(selectedOptions);
                angular.forEach(question.choices, function (option, key) {
                    $(selctdOpt).each(function (fibindex, value) {
                        if ((option.interaction == 'textentry' || option.interaction == 'inline') && value.noforeachblank == option.noforeachblank) {
                            $scope.Questions[questionIndex].choices[key].UserInput = value.inputvalue;
                        }
                        else if (option.interaction == 'dragdrop' && value.noforeachblank == option.noforeachblank) {
                            angular.forEach($scope.Questions[questionIndex].droppableItems, function (ditem, dindex) {
                                if ($filter('lowercase')(ditem.title) == $filter('lowercase')(value.inputvalue)) {
                                    //$scope.droppableItems[dindex].title = "";
                                    $scope.Questions[questionIndex].choices[key].Canvas = [{ 'title': value.inputvalue, 'drag': true, selectedClass: '' }];
                                    $scope.Questions[questionIndex].droppableItems.splice(dindex, 1);
                                    dindex = dindex - 1;
                                }
                            });
                        }
                        else if (option.interaction == 'container' && value.noforeachblank == option.noforeachblank) {
                            var input = value.inputvalue.map(function (x) { return x.title; });
                            for (var i = 0; i < $scope.Questions[questionIndex].droppableItems.length; i++) {
                                if (input.indexOf($scope.Questions[questionIndex].droppableItems[i].title) != -1) {
                                    $scope.Questions[questionIndex].choices[key].Canvas.push({ 'title': $scope.Questions[questionIndex].droppableItems[i].title, 'drag': true, selectedClass: '' });
                                    $scope.Questions[questionIndex].droppableItems.splice(i, 1);
                                    i = i - 1;
                                }
                            }
                        }
                    });
                });
                
               }

        }
            //else if (question == Enum.QuestionType.Essay) {

            //    if (selectedOptions) {
            //        //$scope.essayTemplateText = data.SelectedOptions;
            //        if (data.QuestionData.toolbar.type == "math_3-5_palette" || data.QuestionData.toolbar.type == "math_6-8_palette") {
            //            if ($scope.operationmode == Enum.OperationMode.Offline) {
            //                mathEditorPrevLatex = $.trim(data.SelectedOptions[0].replace("?", ""));
            //                mathEditorType = data.QuestionData.toolbar.type;
            //            }
            //            else {
            //                mathEditorPrevLatex = $.trim(data.SelectedOptions.replace("?", ""));
            //                mathEditorType = data.QuestionData.toolbar.type;
            //            }
            //        } else {
            //            $("#txtEssayContent").val(data.SelectedOptions);
            //            $scope.text = data.SelectedOptions;
            //        }
            //    }
            //}
        else {
            var selctdoptions = '';
            $(question.choices).each(function (index) {
                //data.QuestionData.choices[index].isAnswered = false;
                //if (!$scope.isUndefined(data.QuestionData.image) && data.QuestionData.image.image.indexOf('http') === -1 && data.QuestionData.image.image != "") {
                //    data.QuestionData.choices[index].image = "Content/images/" + data.QuestionData.choices[index].image;
                //}
                var optionindex = index + 1;
                question.choices[index].CurrentOptionIndex = optionindex;
                if (selectedOptions) {
                    selctdoptions = selectedOptions.split(",");
                    $(selctdoptions).each(function (selectindex) {
                        question.choices[index].SelectedOptions = parseInt(selctdoptions[selectindex]);
                        if (optionindex == parseInt(selctdoptions[selectindex])) {
                            question.choices[index].isAnswered = true;
                            //question.choices[index].currentClass = "selected";

                        }
                    });
                }
            });          
            
        }
       // return data;
    }
    
    $scope.SetAlgorithimicType = function (question) {
        var textReplaced = false;
        if ($scope.isUndefined(question.AlgorithmicVariableArray)) {
            question.AlgorithmicVariableArray = [];
        }
        if (question.stem_details.textforeachvalue.length > 0) {
            angular.forEach(question.stem_details.textforeachvalue, function (textDetail, key) {
                if (textDetail.type == 'random') {
                    if (question.AlgorithmicVariableArray.length == angular.fromJson(question.stem_details.textforeachvalue).length) {
                        angular.forEach(question.AlgorithmicVariableArray, function (arrayVariable, akey) {
                            question.question_stem.text = question.question_stem.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        });
                    }
                    else {
                        var random = Math.floor((Math.random() * textDetail.maxvalue) + textDetail.minvalue);
                        question.question_stem.text = question.question_stem.text.replace(textDetail.text, random);
                        question.AlgorithmicVariableArray.push({ "TextToReplace": textDetail.text, "TextValue": random });
                    }
                }
            });

            angular.forEach(question.choices, function (option, key) {
                angular.forEach(question.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    if (isNaN(option.text) && option.text.indexOf(arrayVariable.TextToReplace) != -1) {
                        option.text = option.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        textReplaced = true;
                    }
                });
                if (question.stem_details.display == 'result' && textReplaced) {
                    option.text = eval(option.text);
                }
            });
        }
    };

    $scope.SetFIBType = function (data, index) {
        var questiontext = "";//data.QuestionData.textwithblanks.text;
		if(data.textwithblanks != undefined)
		{
			if(data.textwithblanks.text != undefined && data.textwithblanks.text != "")
				questiontext = data.textwithblanks.text;
		}
		
        //Array object to hold draggable item values
        $scope.Questions[index].droppableItems = [];
        $scope.Questions[index].FIBText = "";
        $scope.Questions[index].layoutType = ($scope.isUndefined(data.question_layout)) ? "" : data.question_layout.text;
        $scope.FillDroppableItems(data,index);
        angular.forEach(data.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            $scope.Questions[index].choices[key].key = key;       // Index of option
            $scope.Questions[index].choices[key].UserInput = "";  //User input value of textbox and dropdown value.         
            $scope.Questions[index].choices[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            $scope.Questions[index].choices[key].width = 'auto';

            //$scope.Questions[index].choice[key].ShowGlobalFeedback = $scope.ShowGlobalFeedback;

            


            questiontext = questiontext.replace(textToReplace, "<fibvariation ng-model='$parent.Questions[" + index + "].choices[" + key + "]' parent-ctrl='$parent' user-input='Questions[" + index + "].choices[" + key + "].UserInput'></fibvariation>");

          
        });
        $scope.Questions[index].FIBText = questiontext;
        if (!$scope.isUndefined($scope.Questions[index].layoutType)) {
            if ($scope.Questions[index].layoutType == 'right' || $scope.Questions[index].layoutType == 'left' || $scope.Questions[index].layoutType == 'vertical') {
                $scope.Questions[index].droptype = "";
                $scope.Questions[index].dragtype = "dragpanel col-sm-4 col-md-4 right";
            }
            else if ($scope.Questions[index].layoutType == 'row') {
                $scope.Questions[index].droptype = "";
                $scope.Questions[index].dragtype = "dragpanel text-center";
            }
            else {
                $scope.Questions[index].dragtype = "dragpanel";
                $scope.Questions[index].droptype = "";
            }
        }
    };

    $scope.CheckDropItem = function (event, index, item, type) {
        if (item.QuestionIndex != index) {
            return false;
        }
        else {
            if ($scope.Questions[index].question_type.text == Enum.QuestionType.Order)
            {
                $scope.setOptions($scope.Questions[index].choices, null, index);
            }
            return item;
        }
    },

    $scope.FillDroppableItems = function (data,index) {
		isDistractorsPushDone = false;
        angular.forEach(data.choices, function (option, key) {
            $scope.Questions[index].choices[key].arrList = [];
            if (option.interaction == "inline") {
                angular.forEach(option.distractorforeachblank, function (titem, tindex) {
                    $scope.Questions[index].choices[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                });
            }
			
			if (option.interaction == "dragdrop" || option.interaction == "container") {
				if(isDistractorsPushDone == false)
				{
					angular.forEach($scope.$parent.CurrentQuestion.QuestionData.questions[index].distractors, function (titem, tindex) {
						$scope.Questions[index].choices[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
					});
				}
				isDistractorsPushDone = true;
            }
			
			angular.forEach(option.textforeachblank, function (titem, tindex) {
				$scope.Questions[index].choices[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
			});

            if (option.interaction == "dragdrop" || option.interaction == "container") {
                angular.forEach($scope.Questions[index].choices[key].arrList, function (item, cindex) {
                    $scope.Questions[index].droppableItems.push({ 'title': item, 'drag': true, selectedClass: '', 'QuestionIndex': index });
                });
                $scope.Questions[index].choices[key].Canvas = [];
                $scope.Questions[index].choices[key].Drop = true;
            }
        });
    };

    $scope.isUndefined = function (thing) {
        return (typeof thing === "undefined");
    };

    $scope.getValue = function (text, index) {
        $scope.Questions[index].essayTemplateText = text;
    }

    $scope.OnDragStart = function (option) {
        option.AnswerClass = "";
    };

    $scope.AddDropCssOnOver = function (key, questionIndex) {
        $('#divDropOption' + key).addClass('active');       
        angular.forEach($scope.Questions[questionIndex].choices[key].Canvas, function (option, key) {
            option.AnswerClass = '';
        });
        return true;
    };

    $scope.RemoveDropCssOnLeave = function (questionIndex) {       
        angular.forEach($scope.Questions[questionIndex].choices, function (option, key) {
            $('#divDropOption' + key).removeClass('active');
        });
    };

    $scope.AddCssOnOver = function (item) {
        item.selectedClass = 'dragging';
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveSelected = function () {
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveCssOnOut = function (droppableItems) {
        angular.forEach(droppableItems, function (item, key) {
            item.selectedClass = '';
        });
    };

    $scope.init();
   
});
