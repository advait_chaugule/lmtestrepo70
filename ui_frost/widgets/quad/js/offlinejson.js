﻿{
    "quiz": [       
        {
            "QuestionId": "F562919D-77FF-465D-92CD-DE69ED267486",
            "QuestionData": {
                "question_stem": {
                    "text": "Math Questions with 3-5 palette"
                },
                "question_title": {
                    "text": "<strong> Math Questions 3-5 palette </strong><br />"
                },
                "instruction_text": {
                    "text": "Instruction text - &nbsp;&nbsp;&nbsp;&nbsp;Math Questions 3-5 palette"
                },
                "question_type": {
                    "text": "essay"
                },
                "essay_text": {
                    "text": "Essay"
                },
                "max_characters": {
                    "text": "100"
                },
                "toolbar": {
                    "type": "math_3-5_palette"
                },
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 11,
            "VersionNo": 0,
            "DisplayQuestionId": "0",
            "OriginalQuestionId": "F562919D-77FF-465D-92CD-DE69ED267486",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "88841286-83FF-40DA-9494-3488A7B58E3C",
            "QuestionData": {
                "question_stem": {
                    "text": "Math Questions with 6-8 palette"
                },
                "question_title": {
                    "text": "<strong> Math Questions 6-8 palette </strong><br />"
                },
                "instruction_text": {
                    "text": "Instruction text -  &nbsp;&nbsp;&nbsp;&nbsp; Math Questions 6-8 palette"
                },
                "question_type": {
                    "text": "essay"
                },
                "essay_text": {
                    "text": "Essay"
                },
                "max_characters": {
                    "text": "100"
                },
                "toolbar": {
                    "type": "math_6-8_palette"
                },
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint 1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint 2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 11,
            "VersionNo": 0,
            "DisplayQuestionId": "0",
            "OriginalQuestionId": "88841286-83FF-40DA-9494-3488A7B58E3C",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "EB0193AD-772B-4589-B67B-3FED28F4B1A3",
            "QuestionData": {
                "question_stem": {
                    "text": "Darwins's evolutionary <b><u>perspective</u></b> on nonverbal communication of emotion led him to predict that facial expressions were"
                },
                "question_title": {
                    "text": "Darwins's evolutionary perspective on nonverbal communication of emotion led him to predict that facial expressions were"
                },
                "instruction_text": {
                    "text": "<strong>Instruction text</strong> - mcss horizontal"
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_layout": {
                    "text": "horizontal"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "universal <b>across</b> all animal species",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "related to physiological reactions that proved to be a useful way to respond to particular type of stimulus",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "a way to increase, but not decrease input through senses such as vision and smell",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "text": "specific to particular cultures",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "1",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 1,
            "VersionNo": 1,
            "DisplayQuestionId": "121",
            "OriginalQuestionId": "EB0193AD-772B-4589-B67B-3FED28F4B1A3",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "B605DD8C-DB31-43AB-A2D4-0DD66BA443B0",
            "QuestionData": {
                "question_stem": {
                    "text": "Paul Ekman and <b>Walter</b> <i>Friesen</i> traveled to New Guinea to study the meaning of various facial expressions in the primitive South Fore tribe. What major conclusion did they reach?"
                },
                "question_title": {
                    "text": "Paul Ekman and Walter Friesen traveled to New Guinea to study the meaning of various facial expressions in the primitive South Fore tribe. What major conclusion did they reach?"
                },
                "instruction_text": {
                    "text": "Instruction text -  mcss vertical"
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "The members of the South Fore used different facial expressions  than Westerners to express the same emotion",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "There are nine <b>major</b> emotional expressions.",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "Facial expressions are not <i>universal</i> because they have different meanings in different cultures.",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "text": "The six major <b><u>emotional expressions</u></b> appear to be universal.",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "1",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    },
                    {
                        "hintid": 3,
                        "text": "<b>This is hint 3</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 1,
            "VersionNo": 1,
            "DisplayQuestionId": "122",
            "OriginalQuestionId": "B605DD8C-DB31-43AB-A2D4-0DD66BA443B0",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "9EC7333D-E4ED-4F74-9DA1-A179FC9F6290",
            "QuestionData": {
                "question_stem": {
                    "text": "What is the capital of India"
                },
                "question_title": {
                    "text": "MCSS Horizontal"
                },
                "instruction_text": {
                    "text": "Instruction text - mcss vertical with media"
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "New Delhi",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "Kolkata",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": true,
                        "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "Chennai",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "Mumbai",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "1",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 1,
            "VersionNo": 1,
            "DisplayQuestionId": "122",
            "OriginalQuestionId": "9EC7333D-E4ED-4F74-9DA1-A179FC9F6290",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "2916EF20-926A-46C0-8B50-2259DBD2B9B8",
            "QuestionData": {
                "question_stem": {
                    "text": "What is the capital of India"
                },
                "question_title": {
                    "text": "MCSS Horizontal"
                },
                "instruction_text": {
                    "text": "Instruction text -  mcss horizontal with media"
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_layout": {
                    "text": "horizontal"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "media": "<video width=\"400\" controls=\"\"><source src=\"Content/videos/areas.mp4\" type=\"video/mp4\"><source src=\"Content/videos/areas.ogg\" type=\"video/ogg\">Your browser does not support HTML5 video.</video>",
                        "text": "New Delhi",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "media": "<video width=\"400\" controls=\"\"><source src=\"Content/videos/areas.mp4\" type=\"video/mp4\"><source src=\"Content/videos/areas.ogg\" type=\"video/ogg\">Your browser does not support HTML5 video.</video>",
                        "text": "Kolkata",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": true,
                        "media": "<video width=\"400\" controls=\"\"><source src=\"Content/videos/areas.mp4\" type=\"video/mp4\"><source src=\"Content/videos/areas.ogg\" type=\"video/ogg\">Your browser does not support HTML5 video.</video>",
                        "text": "Chennai",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "media": "<video width=\"400\" controls=\"\"><source src=\"Content/videos/areas.mp4\" type=\"video/mp4\"><source src=\"Content/videos/areas.ogg\" type=\"video/ogg\">Your browser does not support HTML5 video.</video>",
                        "text": "Mumbai",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "1",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 1,
            "VersionNo": 1,
            "DisplayQuestionId": "122",
            "OriginalQuestionId": "2916EF20-926A-46C0-8B50-2259DBD2B9B8",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "7A7825D6-5A60-40CF-8ED1-946526BD912F",
            "QuestionData": {
                "question_stem": {
                    "text": "What is the <b>missing value</b> in the following number sentence?   7 x __ = 56"
                },
                "question_title": {
                    "text": "What is the missing value in the following number sentence?   7 x __ = 56"
                },
                "instruction_text": {
                    "text": "Instruction text - mcss exhibit"
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_layout": {
                    "text": "horizontal"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
						"media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "97.8",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
						"media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "99.2",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
						"media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "101.2",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
						"media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "102.6",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "1",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "<img height=\"500\" src=\"Content/images/4022_UF02_06.jpg\" title=\"{'asset_id':'5873','alt_tag':'','inst_id':'1','asset_name':'mediaKoala1433253602.jpg','asset_type':'Image','asset_other':''}\" width=\"500\" />",
                        "exhibit_type": "image"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 1,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "7A7825D6-5A60-40CF-8ED1-946526BD912F",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "F223BDB5-4EE0-446B-B1FA-9575DEC9378C",
            "QuestionData": {
                "question_stem": {
                    "text": "What  is the capital of India"
                },
                "question_title": {
                    "text": "MCSS Horizontal"
                },
                "instruction_text": {
                    "text": "Instruction text -  mcms horizontal with media"
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_layout": {
                    "text": "horizontal"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "media": "<video width=\"400\" controls=\"\"><source src=\"Content/videos/areas.mp4\" type=\"video/mp4\"><source src=\"Content/videos/areas.ogg\" type=\"video/ogg\">Your browser does not support HTML5 video.</video>",
                        "text": " New Delhi",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "media": "<video width=\"400\" controls=\"\"><source src=\"Content/videos/areas.mp4\" type=\"video/mp4\"><source src=\"Content/videos/areas.ogg\" type=\"video/ogg\">Your browser does not support HTML5 video.</video>",
                        "text": "Kolkata",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": true,
                        "media": "<video width=\"400\" controls=\"\"><source src=\"Content/videos/areas.mp4\" type=\"video/mp4\"><source src=\"Content/videos/areas.ogg\" type=\"video/ogg\">Your browser does not support HTML5 video.</video>",
                        "text": "Chennai",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "media": "<video width=\"400\" controls=\"\"><source src=\"Content/videos/areas.mp4\" type=\"video/mp4\"><source src=\"Content/videos/areas.ogg\" type=\"video/ogg\">Your browser does not support HTML5 video.</video>",
                        "text": "Mumbai",
                        "feedback": "Individual  Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "1",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 2,
            "VersionNo": 1,
            "DisplayQuestionId": "122",
            "OriginalQuestionId": "F223BDB5-4EE0-446B-B1FA-9575DEC9378C",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "546E3803-86B1-4952-810B-0396C00C8703",
            "QuestionData": {
                "question_stem": {
                    "text": "What is the capital of India"
                },
                "question_title": {
                    "text": "MCSS Horizontal"
                },
                "instruction_text": {
                    "text": "Instruction text - mcms vertical with media"
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "New Delhi",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "Kolkata",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": true,
                        "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "Chennai",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "Mumbai",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "1",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 2,
            "VersionNo": 1,
            "DisplayQuestionId": "122",
            "OriginalQuestionId": "546E3803-86B1-4952-810B-0396C00C8703",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "C890B42F-30EA-4FC7-88AD-9AD4A919ABDB",
            "QuestionData": {
                "question_stem": {
                    "text": "What &Agrave;  is the <b><i>equivalent</i></b> value for the formula \\((a+b)^2\\) ?"
                },
                "question_title": {
                    "text": "What is the equivalent value for the formula \\((a+b)^2\\) ?"
                },
                "instruction_text": {
                    "text": "Instruction text -  &Agrave; mcss horizontal with math <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math>"
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_layout": {
                    "text": "horizontal"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "<i><b>value is &laquo; &copy;</b></i> a\\(^2\\)+b\\(^2\\)-2ab",
                        "feedback": "Individual &Ntilde; \\((a+b)^2\\) Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "<u><b>value is &reg;</b></u> a\\(^2\\)-b\\(^2\\)+2ab",
                        "feedback": "Individual &Ntilde; \\((a+b)^2\\) Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "<u><b>value is &euro;</b></u> a\\(^2\\)+b\\(^2\\)+2ab",
                        "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "text": "<u><b>value is &trade;</b></u> a\\(^2\\)-b\\(^2\\)-2ab",
                        "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "2",
                "global_correct_feedback": {
                    "text": "Global Correct &Ntilde; \\((a+b)^2\\) Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b> - \\((a+b)^2\\)"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b> - \\((a+b)^2\\)"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 1,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "C890B42F-30EA-4FC7-88AD-9AD4A919ABDB",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "98D70C91-A206-4BA1-98B5-FE15C147B2FD",
            "QuestionData": {
                "question_stem": {
                    "text": "Ms. Nelson evenly distributes 32 crayons to 8 kindergarteners. How many crayons does each student get?"
                },
                "question_title": {
                    "text": "Ms. Nelson evenly distributes 32 crayons to 8 kindergarteners. How many crayons does each student get?"
                },
                "instruction_text": {
                    "text": "Instruction text -  mcss selectable label"
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "selectable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "3 crayons",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "4 crayons",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "6 crayons",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "text": "8 crayons",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "2",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 1,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "98D70C91-A206-4BA1-98B5-FE15C147B2FD",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "7B54E51D-27DF-4597-8B2C-C2D40AF8A5F2",
            "QuestionData": {
                "question_stem": {
                    "text": "If a <i><b>&amp;</b></i> b are integers greater than 100 such that a + b = 300, which of the following could be the exact ratio of a to b?"
                },
                "question_title": {
                    "text": "If a and b are integers greater than 100 such that a + b = 300, which of the following could be the exact ratio of a to b?"
                },
                "instruction_text": {
                    "text": "Instruction text - mcms horizontal"
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_layout": {
                    "text": "horizontal"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "9 to 1",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "5 to 2",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "4 to 1",
                        "feedback": "Individual  Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": true,
                        "text": "5 to 2",
                        "feedback": "Individual  Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "1,4",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect  Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 2,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "7B54E51D-27DF-4597-8B2C-C2D40AF8A5F2",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "5D68E8C3-292E-4A90-A938-8B19C82710E1",
            "QuestionData": {
                "question_stem": {
                    "text": "Drew has <i><b>2 large boxes and 4 small boxes</b></i> of magazines. Each large box contains 50 magazines. Each small box contains 35 magazines. How many magazines does Drew have in all?"
                },
                "question_title": {
                    "text": "Drew has 2 large boxes and 4 small boxes of magazines. Each large box contains 50 magazines. Each small box contains 35 magazines. How many magazines does Drew have in all?"
                },
                "instruction_text": {
                    "text": "Instruction text - mcms vertical"
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "<b><u>85 magazines</u></b>",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "<u><i>170 magazines</i></b>",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "230 magazines",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "text": "240 magazines",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "1,2",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 2,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "5D68E8C3-292E-4A90-A938-8B19C82710E1",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "AE50DC7E-B09F-49C0-A18D-49D07B61C250",
            "QuestionData": {
                "question_stem": {
                    "text": "What is the &laquo; equivalent &laquo; value &laquo; for the formula \\((a+b)^2\\) ?"
                },
                "question_title": {
                    "text": "What is the equivalent value for the formula \\((a+b)^2\\) ?"
                },
                "instruction_text": {
                    "text": "Instruction text -  &Agrave; mcms vertical with math <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math>"
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "&laquo; a\\(^2\\)+b\\(^2\\)-2ab",
                        "feedback": "Individual &Ntilde; \\((a+b)^2\\) Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "&raquo; a\\(^2\\)-b\\(^2\\)+2ab",
                        "feedback": "Individual &Ntilde; \\((a+b)^2\\) Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "&cent; a\\(^2\\)+b\\(^2\\)+2ab",
                        "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "text": "&divide; a\\(^2\\)-b\\(^2\\)-2ab",
                        "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "3",
                "global_correct_feedback": {
                    "text": "Global Correct &Ntilde; \\((a+b)^2\\) Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b> - \\((a+b)^2\\)"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b> - \\((a+b)^2\\)"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 2,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "AE50DC7E-B09F-49C0-A18D-49D07B61C250",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "8B8E1C62-98B4-4F6C-B3EF-68E3BF37A99C",
            "QuestionData": {
                "question_stem": {
                    "text": "Interpret 6 4 in a real world context"
                },
                "question_title": {
                    "text": "MCMS Graphic<br/>"
                },
                "instruction_text": {
                    "text": "Instruction text -  mcms exhibit"
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "97.8",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "99.2",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "101.2",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "text": "102.6",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "3",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "http://www.youtube.com/embed/nTFEUsudhfs",
                        "exhibit_type": "video"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 2,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "8B8E1C62-98B4-4F6C-B3EF-68E3BF37A99C",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "39DEDE56-41FC-43A9-A55E-F3FE39E35C52",
            "QuestionData": {
                "question_stem": {
                    "text": "Interpret 6 4 in a real world context"
                },
                "question_title": {
                    "text": "MCMS Graphic<br/>"
                },
                "instruction_text": {
                    "text": "Instruction text -  mcms exhibit"
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
						"media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "97.8",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
						"media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "99.2",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
						"media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "101.2",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
						"media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                        "text": "102.6",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "3",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "<img height=\"100\" src=\"Content/images/mediacrayons1432634172.bmp\" title=\"{'asset_id':'5877','alt_tag':'','inst_id':'1','asset_name':'media3 circles1433414644.jpg','asset_type':'Image','asset_other':''}\" width=\"100\" />",
                        "exhibit_type": "image"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 2,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "39DEDE56-41FC-43A9-A55E-F3FE39E35C52",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "CAD9E151-1759-40DF-A3A8-EFFE6AE24FA0",
            "QuestionData": {
                "question_stem": {
                    "text": "Ms. Nelson evenly distributes 32 crayons to 8 kindergarteners. How many crayons does each student get?"
                },
                "question_title": {
                    "text": "MCMS Select"
                },
                "instruction_text": {
                    "text": "Instruction text -  mcms selectable"
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "selectable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "3 crayons",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "4 crayons",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "6 crayons",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "text": "5 crayons",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "3,4",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint2"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 2,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "CAD9E151-1759-40DF-A3A8-EFFE6AE24FA0",
            "IsDeleted": false,
            "SelectedOptions": null
        },  
         {
             "QuestionId": "FA48B8C8-A149-48EE-BEA0-DA5789FEA393",
             "QuestionData": {
                 "question_stem": {
                     "text": "If a and b are integers greater than 100 such that a + b = 300, which of the following could be the exact ratio of a to b?"
                 },
                 "question_title": {
                     "text": "Choice Matrix"
                 },
                 "instruction_text": {
                     "text": "Instruction text - choice matrix"
                 },
                 "question_type": {
                     "text": "choicematrix"
                 },
                 "hint": {
                     "text": ""
                 },
                 "choices": [
                     {
                         "choiceid": "1",
                         "assess": false,
                         "text": "9 to 1",
                         "feedback": "Individual Feedback1",
                         "score": "1",
                         "pinanswer": 0,
                         "suboption": [
                             {
                                 "identifier": "1",
                                 "value": "true"
                             },
                             {
                                 "identifier": "2",
                                 "value": "false"
                             }
                         ]
                     },
                     {
                         "choiceid": "2",
                         "assess": false,
                         "text": "5 to 2",
                         "feedback": "Individual Feedback2",
                         "score": "1",
                         "pinanswer": 0,
                         "suboption": [
                             {
                                 "identifier": "1",
                                 "value": "true"
                             },
                             {
                                 "identifier": "2",
                                 "value": "false"
                             }
                         ]
                     },
                     {
                         "choiceid": "3",
                         "assess": false,
                         "text": "5 to 3",
                         "feedback": "Individual Feedback3",
                         "score": "1",
                         "pinanswer": 0,
                         "suboption": [
                             {
                                 "identifier": "1",
                                 "value": "true"
                             },
                             {
                                 "identifier": "2",
                                 "value": "false"
                             }
                         ]
                     },
                     {
                         "choiceid": "4",
                         "assess": false,
                         "text": "4 to 1",
                         "feedback": "Individual Feedback4",
                         "score": "1",
                         "pinanswer": 0,
                         "suboption": [
                             {
                                 "identifier": "1",
                                 "value": "true"
                             },
                             {
                                 "identifier": "2",
                                 "value": "false"
                             }
                         ]
                     }
                 ],
                 "image": {
                     "image": ""
                 },
                 "global_correct_feedback": {
                     "text": "Only choice (E) gives a correct ratio of a to b that satisfies all of the conditions in the problem. For (E), a = 180 and b = 120, and both integers are greater than 100."
                 },
                 "global_incorrect_feedback": {
                     "text": "This is Incorrect.Answer choices (B) and (C) can be eliminated since neither the sum of the two numbers in (B) nor the sum of the two numbers in (C) evenly divided 300. (5x + 2x = 300 does not yield an integer solution, nor does 5x + 3x = 300.)"
                 },
                 "hints": [
                     {
                         "hintid": 1,
                         "text": "This is hint1"
                     },
                     {
                         "hintid": 2,
                         "text": "This is hint2"
                     }
                 ],
                 "settings": {
                     "score": "1",
                     "question_type": "choice_matrix"
                 }
             },
             "QuestionTypeId": 10,
             "VersionNo": 0,
             "DisplayQuestionId": "123",
             "OriginalQuestionId": "FA48B8C8-A149-48EE-BEA0-DA5789FEA393",
             "IsDeleted": false,
             "SelectedOptions": null
         },
        {
            "QuestionId": "9E1562D2-6E63-4025-BFC0-09EBB0FD5C9B",
            "QuestionData": {
                "question_stem": {
                    "text": "What is Algebra in mathematics?"
                },
                "question_title": {
                    "text": "Question Stem sdf sdfsdf"
                },
                "instruction_text": {
                    "text": "Instruction text - Essay with basic editor"
                },
                "question_type": {
                    "text": "essay"
                },
                "essay_text": {
                    "text": "Essay"
                },
                "max_characters": {
                    "text": 10
                },
                "toolbar": {
                    "type": "editor-basic"
                },
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint 1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint 2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 11,
            "VersionNo": 0,
            "DisplayQuestionId": "0",
            "OriginalQuestionId": "9E1562D2-6E63-4025-BFC0-09EBB0FD5C9B",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "5087A88F-6C69-46F8-8ADB-E323CFA4CE07",
            "QuestionData": {
                "question_stem": {
                    "text": "What is Calculus in mathematics?"
                },
                "question_title": {
                    "text": "Essay"
                },
                "instruction_text": {
                    "text": "Instruction text - Essay without editor"
                },
                "question_type": {
                    "text": "essay"
                },
                "hint": {
                    "text": "click here to type"
                },
                "essay_text": {
                    "text": ""
                },
                "max_characters": {
                    "text": 10
                },
                "toolbar": {
                    "type": "no-toolbar"
                },
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint 1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint 1"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 11,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "5087A88F-6C69-46F8-8ADB-E323CFA4CE07",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "6481A364-0B34-48EB-A34A-2177F3498742",
            "QuestionData": {
                "question_stem": {
                    "text": "Parallel drag and drop Question on capital"
                },
                "question_title": {
                    "text": "Matching Parallel Drag and Drop"
                },
                "instruction_text": {
                    "text": "Instruction text - Matching dragdrop parallel"
                },
                "question_type": {
                    "text": "matching"
                },
                "question_layout": {
                    "text": "dragdrop_parallel"
                },
                "choices": [
                    {
                        "noforeachblank": "Oregon",
                        "textforeachblank": [
                            {
                                "text": "Salem"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "inputalignment": "left",
                        "feedback": "Individual Feedback1",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "India",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "inputalignment": "left",
                        "feedback": "Individual Feedback2",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "BanglaDesh",
                        "textforeachblank": [
                            {
                                "text": "Dhaka"
                            }
                        ],
                        "choiceid": "3",
                        "score": "",
                        "feedback": "Individual Feedback3",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "Pakistan",
                        "textforeachblank": [
                            {
                                "text": "Islamabad"
                            }
                        ],
                        "choiceid": "4",
                        "score": "",
                        "feedback": "Individual Feedback4",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Washington"
					},
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
						"text": "Mumbai"
					},
					{
						"text": "Chennai"
					},
					{
						"text": "Kualalumpur"
					},
					{
						"text": "Lahore"
					}
                ],
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint 1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint 2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 12,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "6481A364-0B34-48EB-A34A-2177F3498742",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "862CB54A-3B13-47F4-A8DD-3E3752FBCD12",
            "QuestionData": {
                "question_stem": {
                    "text": "Horizontal drag and drop question on capital"
                },
                "question_title": {
                    "text": "Matching Drag and Drop"
                },
                "instruction_text": {
                    "text": "<b>Instruction text</b> -  matching in to table horizontal"
                },
                "question_type": {
                    "text": "matching"
                },
                "question_layout": {
                    "text": "horizontal"
                },
                "choices": [
                    {
                        "noforeachblank": "Oregon",
                        "textforeachblank": [
                            {
                                "text": "Salem"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "India",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "BanglaDesh",
                        "textforeachblank": [
                            {
                                "text": "Dhaka"
                            }
                        ],
                        "choiceid": "3",
                        "score": "",
                        "feedback": "Individual Feedback3",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "Pakistan",
                        "textforeachblank": [
                            {
                                "text": "Islamabad"
                            }
                        ],
                        "choiceid": "4",
                        "score": "",
                        "feedback": "Individual Feedback4",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Washington"
					},
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
						"text": "Mumbai"
					},
					{
						"text": "Chennai"
					},
					{
						"text": "Kualalumpur"
					},
					{
						"text": "Lahore"
					}
				],
                "global_correct_feedback": {
                    "text": "Global Correct Answer"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Answer"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 12,
            "VersionNo": 0,
            "DisplayQuestionId": "985",
            "OriginalQuestionId": "862CB54A-3B13-47F4-A8DD-3E3752FBCD12",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "67F626D6-5D19-4BFB-A885-35E71F4ECBB93",
            "QuestionData": {
                "question_stem": {
                    "text": "Vertical drag and drop Question on capital"
                },
                "question_title": {
                    "text": "Matching Drag and Drop"
                },
                "instruction_text": {
                    "text": "<b>Instruction text</b> -  matching in to table vertical"
                },
                "question_type": {
                    "text": "matching"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "choices": [
                    {
                        "noforeachblank": "Oregon",
                        "textforeachblank": [
                            {
                                "text": "Salem"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "India",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "BanglaDesh",
                        "textforeachblank": [
                            {
                                "text": "Dhaka"
                            }
                        ],
                        "choiceid": "3",
                        "score": "",
                        "feedback": "Individual Feedback3",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "Pakistan",
                        "textforeachblank": [
                            {
                                "text": "Islamabad"
                            }
                        ],
                        "choiceid": "4",
                        "score": "",
                        "feedback": "Individual Feedback4",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Washington"
					},
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
						"text": "Mumbai"
					},
					{
						"text": "Chennai"
					},
					{
						"text": "Kualalumpur"
					},
					{
						"text": "Lahore"
					}
				],
                "global_correct_feedback": {
                    "text": "Global correct answer"
                },
                "global_incorrect_feedback": {
                    "text": "Global incorrect answer"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 12,
            "VersionNo": 0,
            "DisplayQuestionId": "785",
            "OriginalQuestionId": "67F626D6-5D19-4BFB-A885-35E71F4ECBB9",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "DEFFD04C-B34A-4ADC-9052-84813C6E9005",
            "QuestionData": {
                "question_stem": {
                    "text": "Image drag and drop Question on capital"
                },
                "question_title": {
                    "text": "Matching Drag and Drop"
                },
                "instruction_text": {
                    "text": "<b>Instruction text</b> -  matching (drag drop) on image"
                },
                "question_type": {
                    "text": "matching"
                },
                "question_layout": {
                    "text": "image"
                },
                "choices": [
                    {
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "Salem"
                            }
                        ],
                        "response_positions": {
                            "x": 75,
                            "y": 75
                        },
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            }
                        ],
                        "response_positions": {
                            "x": 35,
                            "y": 75
                        },
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "[[3]]",
                        "textforeachblank": [
                            {
                                "text": "Dhaka"
                            }
                        ],
                        "response_positions": {
                            "x": 35,
                            "y": 15
                        },
                        "choiceid": "3",
                        "score": "",
                        "feedback": "Individual Feedback3",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "[[4]]",
                        "textforeachblank": [
                            {
                                "text": "Islamabad"
                            }
                        ],
                        "response_positions": {
                            "x": 10,
                            "y": 43
                        },
                        "choiceid": "4",
                        "score": "",
                        "feedback": "Individual Feedback4",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Washington"
					},
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
						"text": "Mumbai"
					},
					{
						"text": "Chennai"
					},
					{
						"text": "Kualalumpur"
					},
					{
						"text": "Lahore"
					}
				],
                "global_correct_feedback": {
                    "text": "Global Correct Answer"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Answer"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "settings": {
                    "score": "1"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint2"
                    }
                ],
                "exhibit": [
                    {
                        "path": "Content/images/map.jpg",
                        "exhibit_type": "image"
                    }
                ]
            },
            "QuestionTypeId": 12,
            "VersionNo": 0,
            "DisplayQuestionId": "895",
            "OriginalQuestionId": "DEFFD04C-B34A-4ADC-9052-84813C6E9005",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "D9B9845A-3819-41ED-91FC-D82CB3D9E7CC",
            "QuestionData": {
                "question_stem": {
                    "text": "Multiple drag and drop Question with left drag container"
                },
                "question_title": {
                    "text": "Multiple Drag and Drop"
                },
                "instruction_text": {
                    "text": "<b>Instruction text</b> -  Matching in to multiple containers with left drag container"
                },
                "question_type": {
                    "text": "matching"
                },
                "question_layout": {
                    "text": "left"
                },
                "choices": [
                    {
                        "noforeachblank": "Oregon",
                        "textforeachblank": [
                            {
                                "text": "Washington"
                            },
                            {
                                "text": "Salem"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "India",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            },
                            {
                                "text": "Kolkata"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
						"text": "Mumbai"
					},
					{
						"text": "Chennai"
					}
				],
                "global_correct_feedback": {
                    "text": "Global Correct Answer"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Answer"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 12,
            "VersionNo": 0,
            "DisplayQuestionId": "785",
            "OriginalQuestionId": "D9B9845A-3819-41ED-91FC-D82CB3D9E7CC",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "A9DF925C-28DA-4751-86D4-9852F37B7CC9",
            "QuestionData": {
                "question_stem": {
                    "text": "Multiple drag and drop Question with right drag container"
                },
                "question_title": {
                    "text": "Multiple Drag and Drop"
                },
                "instruction_text": {
                    "text": "Instruction text -  Matching in to multiple containers with right drag container"
                },
                "question_type": {
                    "text": "matching"
                },
                "question_layout": {
                    "text": "right"
                },
                "choices": [
                    {
                        "noforeachblank": "Oregon",
                        "textforeachblank": [
                            {
                                "text": "Washington"
                            },
                            {
                                "text": "Salem"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "India",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            },
                            {
                                "text": "Kolkata"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
						"text": "Mumbai"
					},
					{
						"text": "Chennai"
					}
				],
                "global_correct_feedback": {
                    "text": "Global Correct Answer"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Answer"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 12,
            "VersionNo": 0,
            "DisplayQuestionId": "785",
            "OriginalQuestionId": "A9DF925C-28DA-4751-86D4-9852F37B7CC9",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "2B227A72-21B9-448B-8011-42764F618B72",
            "QuestionData": {
                "question_stem": {
                    "text": "Multiple drag and drop Question with top drag container"
                },
                "question_title": {
                    "text": "Matching Drag and Drop"
                },
                "instruction_text": {
                    "text": "Instruction text -  matching in to multiple containers with top drag container"
                },
                "question_type": {
                    "text": "matching"
                },
                "question_layout": {
                    "text": "top"
                },
                "choices": [
                    {
                        "noforeachblank": "Oregon",
                        "textforeachblank": [
                            {
                                "text": "Washington"
                            },
                            {
                                "text": "Salem"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "India",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            },
                            {
                                "text": "Kolkata"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "BanglaDesh",
                        "textforeachblank": [
                            {
                                "text": "Dhaka"
                            }
                        ],
                        "choiceid": "3",
                        "score": "",
                        "feedback": "Individual Feedback3",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "Pakistan",
                        "textforeachblank": [
                            {
                                "text": "Islamabad"
                            }
                        ],
                        "choiceid": "4",
                        "score": "",
                        "feedback": "Individual Feedback4",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
						"text": "Mumbai"
					},
					{
						"text": "Chennai"
					},
					{
						"text": "Kualalumpur"
					},
					{
						"text": "Lahore"
					}
				],
                "global_correct_feedback": {
                    "text": "Global Correct Answer"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Answer"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 12,
            "VersionNo": 0,
            "DisplayQuestionId": "785",
            "OriginalQuestionId": "2B227A72-21B9-448B-8011-42764F618B72",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "59EA8C73-1F09-4F5A-A4CF-511255E5B883",
            "QuestionData": {
                "question_stem": {
                    "text": "Multiple drag and drop Question with bottom drag container"
                },
                "question_title": {
                    "text": "Matching Drag and Drop"
                },
                "instruction_text": {
                    "text": "Instruction text -  matching in to multiple containers with bottom drag container"
                },
                "question_type": {
                    "text": "matching"
                },
                "question_layout": {
                    "text": "bottom"
                },
                "choices": [
                    {
                        "noforeachblank": "Oregon",
                        "textforeachblank": [
                            {
                                "text": "Washington"
                            },
                            {
                                "text": "Salem"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "India",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            },
                            {
                                "text": "Kolkata"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "BanglaDesh",
                        "textforeachblank": [
                            {
                                "text": "Dhaka"
                            }
                        ],
                        "choiceid": "3",
                        "score": "",
                        "feedback": "Individual Feedback3",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "Pakistan",
                        "textforeachblank": [
                            {
                                "text": "Islamabad"
                            }
                        ],
                        "distractorforeachblank": [
                            {
                                "text": "Lahore"
                            }
                        ],
                        "choiceid": "4",
                        "score": "",
                        "feedback": "Individual Feedback4",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
						"text": "Mumbai"
					},
					{
						"text": "Chennai"
					},
					{
						"text": "Kualalumpur"
					}
				],
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 12,
            "VersionNo": 0,
            "DisplayQuestionId": "785",
            "OriginalQuestionId": "59EA8C73-1F09-4F5A-A4CF-511255E5B883",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "2393460F-CD9D-48C0-9988-6CB6DD1C3116",
            "QuestionData": {
                "question_stem": {
                    "text": "Fill in the blank text entry type question"
                },
                "question_title": {
                    "text": "FIB text entry"
                },
                "instruction_text": {
                    "text": "Instruction text -  FIB text entry"
                },
                "textwithblanks": {
                    "text": "[[1]] is the capital of India. There are [[2]] states in India."
                },
                "question_type": {
                    "text": "fib"
                },
                "question_layout": {
                    "text": ""
                },
                "choices": [
                    {
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            },
                            {
                                "text": "Dilli"
                            }
                        ],
                        "choiceid": "1",
                        "distractorforeachblank": "",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "10",
                        "interaction": "textentry",
                        "inputtype": "alphabet",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "29"
                            }
                        ],
                        "distractorforeachblank": "",
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "2",
                        "interaction": "textentry",
                        "inputtype": "numeric",
                        "decimalplace": "0"
                    }
                ],
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global InCorrect Feedback"
                },
                "partial_correct_feedback": {
                    "text": "click here to type"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint 1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint 2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 3,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "2393460F-CD9D-48C0-9988-6CB6DD1C3116",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "ECBEE8B2-A4EF-4E35-999C-8D040A51C228",
            "QuestionData": {
                "question_stem": {
                    "text": "Fill in the blank Drag and Drop Question with drag container on right"
                },
                "question_title": {
                    "text": "FIB Drag and Drop vertical"
                },
                "instruction_text": {
                    "text": "Instruction text -  FIB DND with right drag container"
                },
                "textwithblanks": {
                    "text": "The capital of Oregon is [[1]]. Capital of India is [[2]]"
                },
                "question_type": {
                    "text": "fib_dnd"
                },
                "question_layout": {
                    "text": "right"
                },
                "choices": [
                    {
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "Salem"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Washington"
					},
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
                        "text": "Mumbai"
                    }
				],
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Gloabl Incorrect Feedback"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 13,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "ECBEE8B2-A4EF-4E35-999C-8D040A51C228",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "079BE411-9CAA-40B2-A091-E25C3226321A",
            "QuestionData": {
                "question_stem": {
                    "text": "Fill in the blank Drag and Drop Question with drag container on bottom"
                },
                "question_title": {
                    "text": "FIB Drag and Drop with bottom drag container"
                },
                "instruction_text": {
                    "text": "Instruction text -  FIB DND Bottom"
                },
                "textwithblanks": {
                    "text": "The capital of  Oregon is [[1]]. Capital of India is [[2]]"
                },
                "question_type": {
                    "text": "fib_dnd"
                },
                "question_layout": {
                    "text": "bottom"
                },
                "choices": [
                    {
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "Salem"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Washington"
					},
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
                        "text": "Mumbai"
                    }
				],
                "global_correct_feedback": {
                    "text": "Gloabl Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 13,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "079BE411-9CAA-40B2-A091-E25C3226321A",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "D3F317D3-CE7F-4162-8FED-F4F4A32B10B3",
            "QuestionData": {
                "question_stem": {
                    "text": "Fill in the blank Drag and Drop Question with drag container on left"
                },
                "question_title": {
                    "text": "FIB Drag and Drop with left drag container"
                },
                "instruction_text": {
                    "text": "Instruction text -  FIB Drag and Drop with left drag container"
                },
                "textwithblanks": {
                    "text": "The capital of Oregon is [[1]]. Capital of India is [[2]]"
                },
                "question_type": {
                    "text": "fib_dnd"
                },
                "question_layout": {
                    "text": "left"
                },
                "choices": [
                    {
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "Salem"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Washington"
					},
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
                        "text": "Mumbai"
                    }
				],
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 13,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "D3F317D3-CE7F-4162-8FED-F4F4A32B10B3",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "717CD3D3-933C-4D65-B31F-8E04E5A88BC5",
            "QuestionData": {
                "question_stem": {
                    "text": "Fill in the blank Drag and Drop Question with drag container on top"
                },
                "question_title": {
                    "text": "FIB Drag and Drop vertical"
                },
                "instruction_text": {
                    "text": "Instruction text -  FIB DND with top drag container"
                },
                "textwithblanks": {
                    "text": "The capital of Oregon is [[1]]. Capital of India is [[2]]"
                },
                "question_type": {
                    "text": "fib_dnd"
                },
                "question_layout": {
                    "text": "top"
                },
                "choices": [
                    {
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "Salem"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "New Delhi"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "dragdrop",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Washington"
					},
					{
						"text": "Vancouver"
					},
					{
						"text": "Newyork"
					},
					{
                        "text": "Mumbai"
                    }
				],
                "global_correct_feedback": {
                    "text": "Gloabl Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 13,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "717CD3D3-933C-4D65-B31F-8E04E5A88BC5",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "3B1366C2-3943-4815-A86F-7192B39509BA",
            "QuestionData": {
                "question_stem": {
                    "text": "Select from dropdown type question"
                },
                "question_title": {
                    "text": "FIB select type"
                },
                "instruction_text": {
                    "text": "Instruction -  FIB Select (DropDown)"
                },
                "textwithblanks": {
                    "text": "[[1]] is the  capital of Maharashtra. There are [[2]] districts in Maharashtra."
                },
                "question_type": {
                    "text": "fib"
                },
                "question_layout": {
                    "text": ""
                },
                "choices": [
                    {
                        "noforeachblank": "[[1]]",
                        "textforeachblank": [
                            {
                                "text": "Mumbai"
                            }
                        ],
                        "distractorforeachblank": [
                            {
                                "text": "Ahmedabad"
                            },
                            {
                                "text": "Hyderabad"
                            },
                            {
                                "text": "Bhopal"
                            }
                        ],
                        "choiceid": "1",
                        "score": 1,
                        "feedback": "Individual Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "inline",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "[[2]]",
                        "textforeachblank": [
                            {
                                "text": "36"
                            }
                        ],
                        "distractorforeachblank": [
                            {
                                "text": "32"
                            },
                            {
                                "text": "28"
                            },
                            {
                                "text": "30"
                            }
                        ],
                        "choiceid": "2",
                        "score": 1,
                        "feedback": "Individual Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "inline",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "partial_correct_feedback": {
                    "text": "click here to type"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 3,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "3B1366C2-3943-4815-A86F-7192B39509BA",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "9CB5E815-3E2C-4E4F-98B3-A3CF058356CF",
            "QuestionData": {
                "question_stem": {
                    "text": "Order type question with sentence ordering"
                },
                "question_title": {
                    "text": "Order type title"
                },
                "instruction_text": {
                    "text": "<b><u>Instruction</u></b> - Ordering Sentence"
                },
                "question_type": {
                    "text": "order"
                },
                "question_interaction": {
                    "text": "sentence"
                },
                "choices": [
                    {
                        "choiceid": "1",
                        "text": "22222222",
                        "feedback": "Individual Feedback1"
                    },
                    {
                        "choiceid": "2",
                        "text": "22222222",
                        "feedback": "Individual Feedback2"
                    },
                    {
                        "choiceid": "3",
                        "text": "44444444",
                        "feedback": "Individual Feedback3"
                    },
                    {
                        "choiceid": "4",
                        "text": "555555",
                        "feedback": "Individual Feedback4"
                    }
                ],
                "correct_answer": "1,2,3,4",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 4,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "9CB5E815-3E2C-4E4F-98B3-A3CF058356CF",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "3BB6BCBB-ED35-4F1B-9126-962F3B7FAD21",
            "QuestionData": {
                "question_stem": {
                    "text": "Order type question with sentence ordering - At the beginning of a 7 AM to 7 PM shift, a nurse receives a report, which is completed by 7:20 AM. Place in order of priority the tasks that should be performed by the nurse.<br/>"
                },
                "question_title": {
                    "text": "Order type title"
                },
                "instruction_text": {
                    "text": "<b><u>Instruction</u></b> - Ordering Sentence"
                },
                "question_type": {
                    "text": "order"
                },
                "question_interaction": {
                    "text": "sentence"
                },
                "choices": [
                    {
                        "choiceid": "1",
                        "text": " Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain",
                        "feedback": "Individual Feedback1"
                    },
                    {
                        "choiceid": "2",
                        "text": "Change a patient's dressing that must be done two times a  dayChange a patient&rsquo;s dressing that must be done two times a day Change a patient&rsquo;s dressing that must be done two times a day Change a patient's dressing that must be done two times a day",
                        "feedback": "Individual Feedback2"
                    },
                    {
                        "choiceid": "3",
                        "text": "Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath",
                        "feedback": "Individual Feedback3"
                    },
                    {
                        "choiceid": "4",
                        "text": "Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit",
                        "feedback": "Individual Feedback4"
                    }
                ],
                "correct_answer": "1,2,3,4",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 4,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "3BB6BCBB-ED35-4F1B-9126-962F3B7FAD21",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "7F8928D3-B30B-4A1D-8BAE-8DB1C3E506CC",
            "QuestionData": {
                "question_stem": {
                    "text": "Order type question with paragraph ordering - At the beginning of a 7 AM to 7 PM shift, a nurse receives a report, which is completed by 7:20 AM. Place in order of priority the tasks that should be performed by the nurse."
                },
                "question_title": {
                    "text": "Order type title"
                },
                "instruction_text": {
                    "text": "Instruction text -  Ordering Paragraph"
                },
                "question_type": {
                    "text": "order"
                },
                "question_interaction": {
                    "text": "paragraph"
                },
                "choices": [
                    {
                        "choiceid": "1",
                        "text": "Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain",
                        "feedback": "Individual Feedback1"
                    },
                    {
                        "choiceid": "2",
                        "text": "Change a patient's dressing that must be done two times a  dayChange a patient&rsquo;s dressing that must be done two times a day Change a patient&rsquo;s dressing that must be done two times a day Change a patient&rsquo;s dressing that must be done two times a day",
                        "feedback": "Individual Feedback2"
                    },
                    {
                        "choiceid": "3",
                        "text": "Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath",
                        "feedback": "Individual Feedback3"
                    },
                    {
                        "choiceid": "4",
                        "text": "Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit",
                        "feedback": "Individual Feedback4"
                    }
                ],
                "correct_answer": "1,2,3,4",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 4,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "7F8928D3-B30B-4A1D-8BAE-8DB1C3E506CC",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "D9734257-BC68-4BC3-B58B-A808CF6AD414",
            "QuestionData": {
                "question_stem": {
                    "text": "Order type question with media ordering Horizontal"
                },
                "question_title": {
                    "text": "Image Ordering question text"
                },
                "instruction_text": {
                    "text": "Instruction text -  Ordering Media Horizontal"
                },
                "question_type": {
                    "text": "order"
                },
                "question_interaction": {
                    "text": "media"
                },
                "question_layout": {
                    "text": "horizontal"
                },
                "choices": [
                    {
                        "choiceid": "1",
                        "feedback": "Individual Feedback1",
                        "text": " Image 1",
                        "media": "<img alt=\"test\" src=\"Content/images/mediacrayons1432634172.bmp\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1433414644.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                    },
                    {
                        "choiceid": "2",
                        "feedback": "Individual Feedback2",
                        "text": "Image 2",
                        "media": "<img alt=\"test\" src=\"Content/images/media2484804707_61307e3aa0_o[1]1432632285.jpg\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1432632285.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                    },
                    {
                        "choiceid": "3",
                        "feedback": "Individual Feedback3",
                        "text": "Image 3",
                        "media": "<img alt=\"test\" src=\"Content/images/media3 circles1432632285.jpg\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1432632285.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                    }
                ],
                "correct_answer": "1,2,3",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 4,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "D9734257-BC68-4BC3-B58B-A808CF6AD414",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "35C6F608-E7A1-477F-BE24-F01683D21C1E",
            "QuestionData": {
                "question_stem": {
                    "text": "Order type question with media ordering Vertical"
                },
                "question_title": {
                    "text": "Image Ordering question text<br/>"
                },
                "instruction_text": {
                    "text": "Instruction - Ordering Media Vertical"
                },
                "question_type": {
                    "text": "order"
                },
                "question_interaction": {
                    "text": "media"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "choices": [
                    {
                        "choiceid": "1",
                        "feedback": "Individual Feedback1",
                        "text": "Image 1",
                        "media": "<img alt=\"test\" src=\"Content/images/mediacrayons1432634172.bmp\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1432632285.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                    },
                    {
                        "choiceid": "2",
                        "feedback": "Individual Feedback2",
                        "text": "Image 2",
                        "media": "<img alt=\"test\" src=\"Content/images/media2484804707_61307e3aa0_o[1]1432632285.jpg\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1432632285.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                    },
                    {
                        "choiceid": "3",
                        "feedback": "Individual Feedback3",
                        "text": "Image 3",
                        "media": "<img alt=\"test\" src=\"Content/images/media3 circles1432632285.jpg\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1432632285.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                    }
                ],
                "correct_answer": "1,2,3",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint1"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint2"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 4,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "35C6F608-E7A1-477F-BE24-F01683D21C1E",
            "IsDeleted": false,
            "SelectedOptions": null
        },
         {
             "QuestionId": "FA48B8C8-A149-48EE-BEA0-CA5789FEA393",
             "QuestionData": {
                 "question_stem": {
                     "text": "Choice matrix with math ever where - If a and b are integers greater than 100 such that a + b = 300, which of the following could be the exact ratio of a to b? \\((a+b)^2\\)"
                 },
                 "question_title": {
                     "text": "Choice Matrix"
                 },
                 "instruction_text": {
                     "text": "Instruction text -  choice matrix with math ever where <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math>"
                 },
                 "question_type": {
                     "text": "choicematrix"
                 },
                 "hint": {
                     "text": ""
                 },
                 "choices": [
                     {
                         "choiceid": "1",
                         "assess": false,
                         "text": "9 to 1 \\((a+b)^2\\)",
                         "feedback": "Individual \\((a+b)^2\\) Feedback1",
                         "score": "1",
                         "pinanswer": 0,
                         "suboption": [
                             {
                                 "identifier": "1",
                                 "value": "true"
                             },
                             {
                                 "identifier": "2",
                                 "value": "false"
                             }
                         ]
                     },
                     {
                         "choiceid": "2",
                         "assess": false,
                         "text": "5 to 2 \\((a+b)^2\\)",
                         "feedback": "Individual \\((a+b)^2\\) Feedback2",
                         "score": "1",
                         "pinanswer": 0,
                         "suboption": [
                             {
                                 "identifier": "1",
                                 "value": "true"
                             },
                             {
                                 "identifier": "2",
                                 "value": "false"
                             }
                         ]
                     },
                     {
                         "choiceid": "3",
                         "assess": false,
                         "text": "5 to 3 \\((a+b)^2\\)",
                         "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback3",
                         "score": "1",
                         "pinanswer": 0,
                         "suboption": [
                             {
                                 "identifier": "1",
                                 "value": "true"
                             },
                             {
                                 "identifier": "2",
                                 "value": "false"
                             }
                         ]
                     },
                     {
                         "choiceid": "4",
                         "assess": false,
                         "text": "4 to 1 \\((a+b)^2\\)",
                         "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback4",
                         "score": "1",
                         "pinanswer": 0,
                         "suboption": [
                             {
                                 "identifier": "1",
                                 "value": "true"
                             },
                             {
                                 "identifier": "2",
                                 "value": "false"
                             }
                         ]
                     }
                 ],
                 "image": {
                     "image": ""
                 },
                 "global_correct_feedback": {
                     "text": "\\((a+b)^2\\) - Only choice (E) gives a correct ratio of a to b that satisfies all of the conditions in the problem. For (E), a = 180 and b = 120, and both integers are greater than 100."
                 },
                 "global_incorrect_feedback": {
                     "text": "<math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math>  - This is Incorrect.Answer choices (B) and (C) can be eliminated since neither the sum of the two numbers in (B) nor the sum of the two numbers in (C) evenly divided 300. (5x + 2x = 300 does not yield an integer solution, nor does 5x + 3x = 300.)"
                 },
                 "hints": [
                     {
                         "hintid": 1,
                         "text": "This is hint1 \\((a+b)^2\\)"
                     },
                     {
                         "hintid": 2,
                         "text": "This is hint2 \\((a+b)^2\\)"
                     }
                 ],
                 "settings": {
                     "score": "1",
                     "question_type": "choice_matrix"
                 }
             },
             "QuestionTypeId": 10,
             "VersionNo": 0,
             "DisplayQuestionId": "123",
             "OriginalQuestionId": "FA48B8C8-A149-48EE-BEA0-CA5789FEA393",
             "IsDeleted": false,
             "SelectedOptions": null
         },
        {
            "QuestionId": "59EA8C73-1F09-4F5A-A4CF-511255E5B883",
            "QuestionData": {
                "question_stem": {
                    "text": "Multiple drag and drop Question with bottom drag container and math every where \\((a+b)^2\\)"
                },
                "question_title": {
                    "text": "Matching Drag and Drop"
                },
                "instruction_text": {
                    "text": "Instruction text -  &Ntilde; matching in to multiple container bottom drag container and math every where <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math>"
                },
                "question_type": {
                    "text": "matching"
                },
                "question_layout": {
                    "text": "bottom"
                },
                "choices": [
                    {
                        "noforeachblank": " &Ntilde; Oregon \\((a+b)^2\\)",
                        "textforeachblank": [
                            {
                                "text": "Washington \\((a+b)^2\\)"
                            },
                            {
                                "text": " &Ntilde; Salem \\((a+b)^2\\)"
                            }
                        ],
                        "choiceid": "1",
                        "score": "",
                        "feedback": "Individual &Ntilde; \\((a+b)^2\\) Feedback1",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "India \\((a+b)^2\\)",
                        "textforeachblank": [
                            {
                                "text": "New Delhi \\((a+b)^2\\)"
                            },
                            {
                                "text": "Kolkata \\((a+b)^2\\)"
                            }
                        ],
                        "choiceid": "2",
                        "score": "",
                        "feedback": "Individual &Ntilde; \\((a+b)^2\\) Feedback2",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "BanglaDesh \\((a+b)^2\\)",
                        "textforeachblank": [
                            {
                                "text": "Dhaka \\((a+b)^2\\)"
                            }
                        ],
                        "choiceid": "3",
                        "score": "",
                        "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback3",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    },
                    {
                        "noforeachblank": "Pakistan \\((a+b)^2\\)",
                        "textforeachblank": [
                            {
                                "text": "Islamabad \\((a+b)^2\\)"
                            }
                        ],
                        "choiceid": "4",
                        "score": "",
                        "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback4",
                        "inputalignment": "left",
                        "characters": "",
                        "interaction": "container",
                        "inputtype": "",
                        "decimalplace": "0"
                    }
                ],
				"distractors": [
					{
						"text": "Vancouver \\((a+b)^2\\)"
					},
					{
						"text": "Newyork \\((a+b)^2\\)"
					},
					{
						"text": "Mumbai \\((a+b)^2\\)"
					},
					{
						"text": "Chennai \\((a+b)^2\\)"
					},
					{
						"text": "Kualalumpur \\((a+b)^2\\)"
					},
					{
						"text": "Lahore \\((a+b)^2\\)"
					}
				],
                "global_correct_feedback": {
                    "text": "Gloabl Correct Feedback- &Ntilde; \\((a+b)^2\\) "
                },
                "global_incorrect_feedback": {
                    "text": "Gloabl Incorrect Feedback - <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math>"
                },
                "partial_correct_feedback": {
                    "text": "Partial Correct"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is Hint 1 \\((a+b)^2\\)"
                    },
                    {
                        "hintid": 2,
                        "text": "This is Hint 2 &Ntilde; \\((a+b)^2\\)"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 12,
            "VersionNo": 0,
            "DisplayQuestionId": "785",
            "OriginalQuestionId": "59EA8C73-1F09-4F5A-A4CF-511255E5B883",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "7F8928D3-B30B-4A1D-8BAE-8DB1C3E506CC",
            "QuestionData": {
                "question_stem": {
                    "text": "Order type question with paragraph ordering and math every where - At the beginning of a 7 AM to 7 PM shift, a nurse receives a report, which is completed by 7:20 AM. Place in order of priority the tasks that should be performed by the nurse.  \\((a+b)^2\\)"
                },
                "question_title": {
                    "text": "Order type title"
                },
                "instruction_text": {
                    "text": "Instruction text -  Ordering Paragraph and math every where - <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math>"
                },
                "question_type": {
                    "text": "order"
                },
                "question_interaction": {
                    "text": "paragraph"
                },
                "choices": [
                    {
                        "choiceid": "1",
                        "text": "\\((a+b)^2\\) \\((a+b)^2\\)Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain",
                        "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback1"
                    },
                    {
                        "choiceid": "2",
                        "text": "<math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> - Change a patient's dressing that must be done two times a  dayChange a patient&rsquo;s dressing that must be done two times a day Change a patient&rsquo;s dressing that must be done two times a day Change a patient&rsquo;s dressing that must be done two times a day",
                        "feedback": "Individual \\((a+b)^2\\) Feedback2"
                    },
                    {
                        "choiceid": "3",
                        "text": "\\((a+b)^2\\) Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath",
                        "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback3"
                    },
                    {
                        "choiceid": "4",
                        "text": "<math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> - Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit",
                        "feedback": "Individual \\((a+b)^2\\) Feedback4"
                    }
                ],
                "correct_answer": "1,2,3,4",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback - \\((a+b)^2\\)"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback - <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math>"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "This is hint1 - <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math>"
                    },
                    {
                        "hintid": 2,
                        "text": "This is hint2 - \\((a+b)^2\\)"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 4,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "7F8928D3-B30B-4A1D-8BAE-8DB1C3E506CC",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "B605DD8C-DB31-43AB-A2D4-0DD66BA443B0",
            "QuestionData": {
                "question_stem": {
                    "text": "¿Cuál es el resultado de la adición de dos y tres?"
                },
                "question_title": {
                    "text": "¿Cuál es el resultado de la adición de dos y tres?"
                },
                "instruction_text": {
                    "text": "Texto de instrucciones - Opción múltiple sola pregunta de selección"
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": false,
                        "text": "El resultado es de cuatro",
                        "feedback": "Estás a un paso de resultado",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "El resultado es de nueve",
                        "feedback": "No creo que esta es la respuesta correcta",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "El resultado es de siete",
                        "feedback": "Esta es la respuesta equivocada",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": true,
                        "text": "El resultado es de cinco",
                        "feedback": "Buen trabajo esta es la respuesta correcta",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "4",
                "global_correct_feedback": {
                    "text": "Buen trabajo, usted tiene conocimiento de adición número básico"
                },
                "global_incorrect_feedback": {
                    "text": "Es necesario practicar más en adición número básico"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>La respuesta es un medio valor de diez</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>La respuesta es de cinco</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 1,
            "VersionNo": 1,
            "DisplayQuestionId": "122",
            "OriginalQuestionId": "B605DD8C-DB31-43AB-A2D4-0DD66BA443B0",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "B605DD8C-DB31-43AB-A2D4-0DD66BA443B0",
            "QuestionData": {
                "question_stem": {
                    "text": "Quel est le résultat de l'addition de deux et trois?"
                },
                "question_title": {
                    "text": "Quel est le résultat de l'addition de deux et trois?"
                },
                "instruction_text": {
                    "text": "Texte d'instructions - une question de sélection à choix multiples"
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": false,
                        "text": "Il en résulte quatre",
                        "feedback": "Vous n'êtes plus qu'à une étape de résultat",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "Le résultat est de neuf",
                        "feedback": "Je ne pense pas que ce soit la bonne réponse",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": true,
                        "text": "Résultat est la même que la soustraction de dix et cinq",
                        "feedback": "Bon travail, vous savez presque réponse",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": true,
                        "text": "Le résultat est cinq",
                        "feedback": "Bon travail cela est la bonne réponse",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "3,4",
                "global_correct_feedback": {
                    "text": "Bon travail, vous avez des connaissances de base de l'addition de plusieurs"
                },
                "global_incorrect_feedback": {
                    "text": "Vous avez besoin de plus de pratique en ajoutant nombre de base"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>La réponse est une valeur moyenne de dix</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>La réponse est de cinq</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 1,
            "VersionNo": 1,
            "DisplayQuestionId": "122",
            "OriginalQuestionId": "B605DD8C-DB31-43AB-A2D4-0DD66BA443B0",
            "IsDeleted": false,
            "SelectedOptions": null
        },        
        {
            "QuestionId": "EB0193AD-772B-4589-A67B-3FED28F4B1A3",
            "QuestionData": {
                "question_stem": {
                    "text": "<iframe name=\"pdfLaunchFrame\" src=\"Content/pdfs/Dummy.pdf\" id=\"pdfLaunchFrame\" frameborder=\"1\" style=\"width:100%; height:100%\"></iframe>"
                },
                "question_title": {
                    "text": "Darwins's evolutionary perspective on nonverbal communication of emotion led him to predict that facial expressions were"
                },
                "instruction_text": {
                    "text": "<strong>Instruction text</strong> - mcss horizontal"
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_layout": {
                    "text": "horizontal"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "<iframe name=\"pdfLaunchFrame\" src=\"Content/pdfs/Dummy.pdf\" id=\"pdfLaunchFrame\" frameborder=\"1\" style=\"width:100%; height:75%\"></iframe>",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "<iframe name=\"pdfLaunchFrame\" src=\"Content/pdfs/Dummy.pdf\" id=\"pdfLaunchFrame\" frameborder=\"1\" style=\"width:100%; height:100%\"></iframe>",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "<iframe name=\"pdfLaunchFrame\" src=\"Content/pdfs/Dummy.pdf\" id=\"pdfLaunchFrame\" frameborder=\"1\" style=\"width:100%; height:100%\"></iframe>",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "text": "<iframe name=\"pdfLaunchFrame\" src=\"Content/pdfs/Dummy.pdf\" id=\"pdfLaunchFrame\" frameborder=\"1\" style=\"width:100%; height:100%\"></iframe>",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "1",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": ""
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 1,
            "VersionNo": 1,
            "DisplayQuestionId": "121",
            "OriginalQuestionId": "EB0193AD-772B-4589-A67B-3FED28F4B1A3",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        {
            "QuestionId": "EB0193AD-772B-4589-A67B-3FED28F4B1K3",
            "QuestionData": {
                "question_stem": {
                    "text": "Question stem of Composite Question Type"
                },
                "question_title": {
                    "text": "Question stem of Composite Question Type"
                },
                "question_body": {
                    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum faucibus efficitur arcu facilisis interdum. Nulla efficitur, felis quis varius lacinia, lectus nunc fringilla mauris, ac blandit lacus erat nec sapien. In fringilla luctus lectus, non suscipit elit aliquet quis. Maecenas tristique est at nulla porttitor iaculis. Pellentesque blandit placerat tincidunt. Suspendisse et nulla eu neque dignissim fringilla et in massa. Pellentesque condimentum eleifend neque quis auctor. Integer elit ex, ultrices sit amet mollis quis, rhoncus rutrum nisl. Nunc laoreet varius mollis. "
                },
                "instruction_text": {
                    "text": "<b>Instruction Text:</b> Read below paragraph and answer questions - <b>Without Any Questions</b>"
                },
                "question_type": {
                    "text": "composite"
                },
                "settings": {
                    "score": "1"
                },
                "questions": []
            },
            "VersionNo": 1,
            "DisplayQuestionId": "121",
            "OriginalQuestionId": "EB0193AD-772B-4589-A67B-3FED28F4B1K3",
            "IsDeleted": false,
            "SelectedOptions": null
        },       
        {            
            "QuestionId": "EB0193AD-772B-4589-A67B-3FED28F4B1J3",
                "QuestionData": {
                    "question_stem": {
                        "text": "Question stem of Composite Question Type"
                    },
                    "question_title": {
                        "text": "Question stem of Composite Question Type"
                    },
                    "question_body": {
                        "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum faucibus efficitur arcu facilisis interdum. Nulla efficitur, felis quis varius lacinia, lectus nunc fringilla mauris, ac blandit lacus erat nec sapien. In fringilla luctus lectus, non suscipit elit aliquet quis. Maecenas tristique est at nulla porttitor iaculis. </br> </br> </br><video width=\"400\" controls=\"\"><source src=\"Content/videos/areas.mp4\" type=\"video/mp4\"><source src=\"Content/videos/areas.ogg\" type=\"video/ogg\">Your browser does not support HTML5 video.</video>"
                    },
                    "instruction_text": {
                        "text": "<b>Instruction Text:</b> View below Video and answer questions"
                    },
                    "question_type": {
                        "text": "composite"
                    },
                    "settings": {
                        "score": "1"
                    },
                    "questions": [
                        {
                            "question_stem": {
                                "text": "Multiple drag and drop Question with left drag container"
                            },
                            "question_title": {
                                "text": "Multiple Drag and Drop"
                            },
                            "instruction_text": {
                                "text": "<b>Instruction text</b> -  Matching in to multiple containers with left drag container"
                            },
                            "question_type": {
                                "text": "matching"
                            },
                            "question_layout": {
                                "text": "left"
                            },
                            "choices": [
                                {
                                    "noforeachblank": "Oregon",
                                    "textforeachblank": [
                                        {
                                            "text": "Washington"
                                        },
                                        {
                                            "text": "Salem"
                                        }
                                    ],
                                    "choiceid": "1",
                                    "score": "",
                                    "feedback": "Individual Feedback1",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "container",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                },
                                {
                                    "noforeachblank": "India",
                                    "textforeachblank": [
                                        {
                                            "text": "New Delhi"
                                        },
                                        {
                                            "text": "Kolkata"
                                        }
                                    ],
                                    "choiceid": "2",
                                    "score": "",
                                    "feedback": "Individual Feedback2",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "container",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                }
                            ],
							"distractors": [
								{
									"text": "Vancouver"
								},
								{
									"text": "Newyork"
								},
								{
									"text": "Mumbai"
								},
								{
									"text": "Chennai"
								}
							],
                            "global_correct_feedback": {
                                "text": "Global Correct Answer"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global Incorrect Answer"
                            },
                            "partial_correct_feedback": {
                                "text": "Partial Correct"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is hint1"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is hint2"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "Vertical drag and drop Question on capital"
                            },
                            "question_title": {
                                "text": "Matching Drag and Drop"
                            },
                            "instruction_text": {
                                "text": "<b>Instruction text</b> -  matching in to table vertical"
                            },
                            "question_type": {
                                "text": "matching"
                            },
                            "question_layout": {
                                "text": "vertical"
                            },
                            "choices": [
                                {
                                    "noforeachblank": "Oregon",
                                    "textforeachblank": [
                                        {
                                            "text": "Salem"
                                        }
                                    ],
                                    "choiceid": "1",
                                    "score": "",
                                    "feedback": "Individual Feedback1",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                },
                                {
                                    "noforeachblank": "India",
                                    "textforeachblank": [
                                        {
                                            "text": "New Delhi"
                                        }
                                    ],
                                    "choiceid": "2",
                                    "score": "",
                                    "feedback": "Individual Feedback2",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                },
                                {
                                    "noforeachblank": "BanglaDesh",
                                    "textforeachblank": [
                                        {
                                            "text": "Dhaka"
                                        }
                                    ],
                                    "choiceid": "3",
                                    "score": "",
                                    "feedback": "Individual Feedback3",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                },
                                {
                                    "noforeachblank": "Pakistan",
                                    "textforeachblank": [
									                                        {
                                            "text": "Islamabad"
                                        }
                                    ],
                                    "choiceid": "4",
                                    "score": "",
                                    "feedback": "Individual Feedback4",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                }
                            ],
							"distractors": [
								{
									"text": "Washington"
								},
								{
									"text": "Vancouver"
								},
								{
									"text": "Newyork"
								},
								{
									"text": "Mumbai"
								},
								{
									"text": "Chennai"
								},
								{
									"text": "Kualalumpur"
								},
								{
									"text": "Islamabad"
								},
								{
									"text": "Lahore"
								}
							],
                            "global_correct_feedback": {
                                "text": "Global correct answer"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global incorrect answer"
                            },
                            "partial_correct_feedback": {
                                "text": "Partial Correct"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is hint1"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is hint2"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },                        
                        {
                            "question_stem": {
                                "text": "Choice matrix with math ever where - If a and b are integers greater than 100 such that a + b = 300, which of the following could be the exact ratio of a to b? \\((a+b)^2\\)"
                            },
                            "question_title": {
                                "text": "Choice Matrix"
                            },
                            "instruction_text": {
                                "text": "Instruction text -  choice matrix with math ever where <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math>"
                            },
                            "question_type": {
                                "text": "choicematrix"
                            },
                            "hint": {
                                "text": ""
                            },
                            "choices": [
                                {
                                    "choiceid": "1",
                                    "assess": false,
                                    "text": "9 to 1 \\((a+b)^2\\)",
                                    "feedback": "Individual \\((a+b)^2\\) Feedback1",
                                    "score": "1",
                                    "pinanswer": 0,
                                    "suboption": [
                                        {
                                            "identifier": "1",
                                            "value": "true"
                                        },
                                        {
                                            "identifier": "2",
                                            "value": "false"
                                        }
                                    ]
                                },
                                {
                                    "choiceid": "2",
                                    "assess": false,
                                    "text": "5 to 2 \\((a+b)^2\\)",
                                    "feedback": "Individual \\((a+b)^2\\) Feedback2",
                                    "score": "1",
                                    "pinanswer": 0,
                                    "suboption": [
                                        {
                                            "identifier": "1",
                                            "value": "true"
                                        },
                                        {
                                            "identifier": "2",
                                            "value": "false"
                                        }
                                    ]
                                },
                                {
                                    "choiceid": "3",
                                    "assess": false,
                                    "text": "5 to 3 \\((a+b)^2\\)",
                                    "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback3",
                                    "score": "1",
                                    "pinanswer": 0,
                                    "suboption": [
                                        {
                                            "identifier": "1",
                                            "value": "true"
                                        },
                                        {
                                            "identifier": "2",
                                            "value": "false"
                                        }
                                    ]
                                },
                                {
                                    "choiceid": "4",
                                    "assess": false,
                                    "text": "4 to 1 \\((a+b)^2\\)",
                                    "feedback": "Individual <math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math> Feedback4",
                                    "score": "1",
                                    "pinanswer": 0,
                                    "suboption": [
                                        {
                                            "identifier": "1",
                                            "value": "true"
                                        },
                                        {
                                            "identifier": "2",
                                            "value": "false"
                                        }
                                    ]
                                }
                            ],
                            "image": {
                                "image": ""
                            },
                            "global_correct_feedback": {
                                "text": "\\((a+b)^2\\) - Only choice (E) gives a correct ratio of a to b that satisfies all of the conditions in the problem. For (E), a = 180 and b = 120, and both integers are greater than 100."
                            },
                            "global_incorrect_feedback": {
                                "text": "<math xmlns='http://www.w3.org/1998/Math/MathML'><msup><mrow><mo>(</mo><mrow><mi>a</mi><mo>+</mo><mi>b</mi></mrow><mo>)</mo></mrow><mn>2</mn></msup></math>  - This is Incorrect.Answer choices (B) and (C) can be eliminated since neither the sum of the two numbers in (B) nor the sum of the two numbers in (C) evenly divided 300. (5x + 2x = 300 does not yield an integer solution, nor does 5x + 3x = 300.)"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is hint1 \\((a+b)^2\\)"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is hint2 \\((a+b)^2\\)"
                                }
                            ],
                            "settings": {
                                "score": "1",
                                "question_type": "choice_matrix"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "Order type question with media ordering Vertical"
                            },
                            "question_title": {
                                "text": "Image Ordering question text<br/>"
                            },
                            "instruction_text": {
                                "text": "Instruction - Ordering Media Vertical"
                            },
                            "question_type": {
                                "text": "order"
                            },
                            "question_interaction": {
                                "text": "media"
                            },
                            "question_layout": {
                                "text": "vertical"
                            },
                            "choices": [
                                {
                                    "choiceid": "1",
                                    "feedback": "Individual Feedback1",
                                    "text": "Image 1",
                                    "media": "<img alt=\"test\" src=\"Content/images/mediacrayons1432634172.bmp\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1432632285.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                                },
                                {
                                    "choiceid": "2",
                                    "feedback": "Individual Feedback2",
                                    "text": "Image 2",
                                    "media": "<img alt=\"test\" src=\"Content/images/media2484804707_61307e3aa0_o[1]1432632285.jpg\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1432632285.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                                },
                                {
                                    "choiceid": "3",
                                    "feedback": "Individual Feedback3",
                                    "text": "Image 3",
                                    "media": "<img alt=\"test\" src=\"Content/images/media3 circles1432632285.jpg\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1432632285.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                                }
                            ],
                            "correct_answer": "1,3,2",
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global Incorrect Feedback"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is hint1"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is hint2"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "Order type question with media ordering Horizontal"
                            },
                            "question_title": {
                                "text": "Image Ordering question text"
                            },
                            "instruction_text": {
                                "text": "Instruction text -  Ordering Media Horizontal"
                            },
                            "question_type": {
                                "text": "order"
                            },
                            "question_interaction": {
                                "text": "media"
                            },
                            "question_layout": {
                                "text": "horizontal"
                            },
                            "choices": [
                                {
                                    "choiceid": "1",
                                    "feedback": "Individual Feedback1",
                                    "text": " Image 1",
                                    "media": "<img alt=\"test\" src=\"Content/images/mediacrayons1432634172.bmp\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1433414644.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                                },
                                {
                                    "choiceid": "2",
                                    "feedback": "Individual Feedback2",
                                    "text": "Image 2",
                                    "media": "<img alt=\"test\" src=\"Content/images/media2484804707_61307e3aa0_o[1]1432632285.jpg\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1432632285.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                                },
                                {
                                    "choiceid": "3",
                                    "feedback": "Individual Feedback3",
                                    "text": "Image 3",
                                    "media": "<img alt=\"test\" src=\"Content/images/media3 circles1432632285.jpg\" style=\"width: 100px; height: 100px;\" title=\"{\"asset_id\":\"5878\",\"alt_tag\":\"\",\"inst_id\":\"1\",\"asset_name\":\"media2484804707_61307e3aa0_o[1]1432632285.jpg\",\"asset_type\":\"Image\",\"asset_other\":\"\"}\" />"
                                }
                            ],
                            "correct_answer": "2,1,3",
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global Incorrect Feedback"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is hint1"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is hint2"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "Order type question with paragraph ordering - At the beginning of a 7 AM to 7 PM shift, a nurse receives a report, which is completed by 7:20 AM. Place in order of priority the tasks that should be performed by the nurse."
                            },
                            "question_title": {
                                "text": "Order type title"
                            },
                            "instruction_text": {
                                "text": "Instruction text -  Ordering Paragraph"
                            },
                            "question_type": {
                                "text": "order"
                            },
                            "question_interaction": {
                                "text": "paragraph"
                            },
                            "choices": [
                                {
                                    "choiceid": "1",
                                    "text": "Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain Give a PRN pain medication to a patient in pain",
                                    "feedback": "Individual Feedback1"
                                },
                                {
                                    "choiceid": "2",
                                    "text": "Change a patient's dressing that must be done two times a  dayChange a patient&rsquo;s dressing that must be done two times a day Change a patient&rsquo;s dressing that must be done two times a day Change a patient&rsquo;s dressing that must be done two times a day",
                                    "feedback": "Individual Feedback2"
                                },
                                {
                                    "choiceid": "3",
                                    "text": "Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath Obtain the vital signs of a patient reporting shortness of breath",
                                    "feedback": "Individual Feedback3"
                                },
                                {
                                    "choiceid": "4",
                                    "text": "Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit Administer the ordered 8:00 AM medications to the patients on the unit",
                                    "feedback": "Individual Feedback4"
                                }
                            ],
                            "correct_answer": "2,1,4,3",
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global Incorrect Feedback"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is hint1"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is hint2"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "Order type question with sentence ordering"
                            },
                            "question_title": {
                                "text": "Order type title"
                            },
                            "instruction_text": {
                                "text": "<b><u>Instruction</u></b> - Ordering Sentence"
                            },
                            "question_type": {
                                "text": "order"
                            },
                            "question_interaction": {
                                "text": "sentence"
                            },
                            "choices": [
                                {
                                    "choiceid": "1",
                                    "text": "22222222",
                                    "feedback": "Individual Feedback1"
                                },
                                {
                                    "choiceid": "2",
                                    "text": "22222222",
                                    "feedback": "Individual Feedback2"
                                },
                                {
                                    "choiceid": "3",
                                    "text": "44444444",
                                    "feedback": "Individual Feedback3"
                                },
                                {
                                    "choiceid": "4",
                                    "text": "555555",
                                    "feedback": "Individual Feedback4"
                                }
                            ],
                            "correct_answer": "4,3,2,1",
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global Incorrect Feedback"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is hint1"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is hint2"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "Parallel drag and drop Question on capital"
                            },
                            "question_title": {
                                "text": "Matching Parallel Drag and Drop"
                            },
                            "instruction_text": {
                                "text": "Instruction text - Matching dragdrop parallel"
                            },
                            "question_type": {
                                "text": "matching"
                            },
                            "question_layout": {
                                "text": "dragdrop_parallel"
                            },
                            "choices": [
                                {
                                    "noforeachblank": "Oregon",
                                    "textforeachblank": [
                                        {
                                            "text": "Salem"
                                        }
                                    ],
                                    "choiceid": "1",
                                    "score": "",
                                    "inputalignment": "left",
                                    "feedback": "Individual Feedback1",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                },
                                {
                                    "noforeachblank": "India",
                                    "textforeachblank": [
                                        {
                                            "text": "New Delhi"
                                        }
                                    ],
                                    "choiceid": "2",
                                    "score": "",
                                    "inputalignment": "left",
                                    "feedback": "Individual Feedback2",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                },
                                {
                                    "noforeachblank": "BanglaDesh",
                                    "textforeachblank": [
                                        {
                                            "text": "Dhaka"
                                        }
                                    ],
                                    "choiceid": "3",
                                    "score": "",
                                    "feedback": "Individual Feedback3",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                },
                                {
                                    "noforeachblank": "Pakistan",
                                    "textforeachblank": [
                                        {
                                            "text": "Islamabad"
                                        }
                                    ],
                                    "choiceid": "4",
                                    "score": "",
                                    "feedback": "Individual Feedback4",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                }
                            ],
							"distractors": [
								{
									"text": "Washington"
								},
								{
									"text": "Vancouver"
								},
								{
									"text": "Newyork"
								},
								{
									"text": "Mumbai"
								},
								{
									"text": "Chennai"
								},
								{
									"text": "Kualalumpur"
								},
								{
									"text": "Lahore"
								}
							],
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global Incorrect Feedback"
                            },
                            "partial_correct_feedback": {
                                "text": "Partial Correct"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is Hint 1"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is Hint 2"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "Fill in the blank Drag and Drop Question with drag container on left"
                            },
                            "question_title": {
                                "text": "FIB Drag and Drop with left drag container"
                            },
                            "instruction_text": {
                                "text": "Instruction text -  FIB Drag and Drop with left drag container"
                            },
                            "textwithblanks": {
                                "text": "The capital of Oregon is [[1]]. Capital of India is [[2]]"
                            },
                            "question_type": {
                                "text": "fib_dnd"
                            },
                            "question_layout": {
                                "text": "left"
                            },
                            "choices": [
                                {
                                    "noforeachblank": "[[1]]",
                                    "textforeachblank": [
                                        {
                                            "text": "Salem"
                                        }
                                    ],
                                    "choiceid": "1",
                                    "score": "",
                                    "feedback": "Individual Feedback1",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                },
                                {
                                    "noforeachblank": "[[2]]",
                                    "textforeachblank": [
                                        {
                                            "text": "New Delhi"
                                        }
                                    ],
                                    "choiceid": "2",
                                    "score": "",
                                    "feedback": "Individual Feedback2",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                }
                            ],
							"distractors": [
								{
									"text": "Washington"
								},
								{
									"text": "Vancouver"
								},
								{
									"text": "Newyork"
								},
								{
									"text": "Mumbai"
								}
							],
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global Incorrect Feedback"
                            },
                            "partial_correct_feedback": {
                                "text": "Partial Correct"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is Hint1"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is Hint2"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "Fill in the blank Drag and Drop Question with drag container on right"
                            },
                            "question_title": {
                                "text": "FIB Drag and Drop vertical"
                            },
                            "instruction_text": {
                                "text": "Instruction text -  FIB DND with right drag container"
                            },
                            "textwithblanks": {
                                "text": "The capital of Oregon is [[1]]. Capital of India is [[2]]"
                            },
                            "question_type": {
                                "text": "fib_dnd"
                            },
                            "question_layout": {
                                "text": "right"
                            },
                            "choices": [
                                {
                                    "noforeachblank": "[[1]]",
                                    "textforeachblank": [
                                        {
                                            "text": "Salem"
                                        }
                                    ],
                                    "choiceid": "1",
                                    "score": "",
                                    "feedback": "Individual Feedback1",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                },
                                {
                                    "noforeachblank": "[[2]]",
                                    "textforeachblank": [
                                        {
                                            "text": "New Delhi"
                                        }
                                    ],
                                    "choiceid": "2",
                                    "score": "",
                                    "feedback": "Individual Feedback2",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "dragdrop",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                }
                            ],
							"distractors": [
								{
									"text": "Washington"
								},
								{
									"text": "Vancouver"
								},
								{
									"text": "Newyork"
								},
								{
									"text": "Mumbai"
								}
							],
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Gloabl Incorrect Feedback"
                            },
                            "partial_correct_feedback": {
                                "text": "Partial Correct"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is Hint1"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is Hint2"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "Fill in the blank text entry type question"
                            },
                            "question_title": {
                                "text": "FIB text entry"
                            },
                            "instruction_text": {
                                "text": "Instruction text -  FIB text entry"
                            },
                            "textwithblanks": {
                                "text": "[[1]] is the capital of India. There are [[2]] states in India."
                            },
                            "question_type": {
                                "text": "fib"
                            },
                            "question_layout": {
                                "text": ""
                            },
                            "choices": [
                                {
                                    "noforeachblank": "[[1]]",
                                    "textforeachblank": [
                                        {
                                            "text": "New Delhi"
                                        },
                                        {
                                            "text": "Dilli"
                                        }
                                    ],
                                    "choiceid": "1",
                                    "distractorforeachblank": "",
                                    "score": "",
                                    "feedback": "Individual Feedback1",
                                    "inputalignment": "left",
                                    "characters": "10",
                                    "interaction": "textentry",
                                    "inputtype": "alphabet",
                                    "decimalplace": "0"
                                },
                                {
                                    "noforeachblank": "[[2]]",
                                    "textforeachblank": [
                                        {
                                            "text": "29"
                                        }
                                    ],
                                    "distractorforeachblank": "",
                                    "choiceid": "2",
                                    "score": "",
                                    "feedback": "Individual Feedback2",
                                    "inputalignment": "left",
                                    "characters": "2",
                                    "interaction": "textentry",
                                    "inputtype": "numeric",
                                    "decimalplace": "0"
                                }
                            ],
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global InCorrect Feedback"
                            },
                            "partial_correct_feedback": {
                                "text": "click here to type"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is Hint 1"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is Hint 2"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "Select from dropdown type question"
                            },
                            "question_title": {
                                "text": "FIB select type"
                            },
                            "instruction_text": {
                                "text": "Instruction -  FIB Select (DropDown)"
                            },
                            "textwithblanks": {
                                "text": "[[1]] is the  capital of Maharashtra. There are [[2]] districts in Maharashtra."
                            },
                            "question_type": {
                                "text": "fib"
                            },
                            "question_layout": {
                                "text": ""
                            },
                            "choices": [
                                {
                                    "noforeachblank": "[[1]]",
                                    "textforeachblank": [
                                        {
                                            "text": "Mumbai"
                                        }
                                    ],
                                    "distractorforeachblank": [
                                        {
                                            "text": "Ahmedabad"
                                        },
                                        {
                                            "text": "Hyderabad"
                                        },
                                        {
                                            "text": "Bhopal"
                                        }
                                    ],
                                    "choiceid": "1",
                                    "score": 1,
                                    "feedback": "Individual Feedback1",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "inline",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                },
                                {
                                    "noforeachblank": "[[2]]",
                                    "textforeachblank": [
                                        {
                                            "text": "36"
                                        }
                                    ],
                                    "distractorforeachblank": [
                                        {
                                            "text": "32"
                                        },
                                        {
                                            "text": "28"
                                        },
                                        {
                                            "text": "30"
                                        }
                                    ],
                                    "choiceid": "2",
                                    "score": 1,
                                    "feedback": "Individual Feedback2",
                                    "inputalignment": "left",
                                    "characters": "",
                                    "interaction": "inline",
                                    "inputtype": "",
                                    "decimalplace": "0"
                                }
                            ],
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global Incorrect Feedback"
                            },
                            "partial_correct_feedback": {
                                "text": "click here to type"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "This is hint1"
                                },
                                {
                                    "hintid": 2,
                                    "text": "This is hint2"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },                  
                                {
                                    "question_stem": {
                                        "text": "Paul Ekman and <b>Walter</b> <i>Friesen</i> traveled to New Guinea to study the meaning of various facial expressions in the primitive South Fore tribe. What major conclusion did they reach?"
                                    },
                                    "question_title": {
                                        "text": "Paul Ekman and Walter Friesen traveled to New Guinea to study the meaning of various facial expressions in the primitive South Fore tribe. What major conclusion did they reach?"
                                    },
                                    "instruction_text": {
                                        "text": "Instruction text -  mcss vertical"
                                    },
                                    "question_type": {
                                        "text": "mcss"
                                    },
                                    "question_layout": {
                                        "text": "vertical"
                                    },
                                    "question_interaction": {
                                        "text": "clickable"
                                    },
                                    "choices": [
                                        {
                                            "choiceid": 1,
                                            "assess": true,
                                            "text": "The members of the South Fore used different facial expressions  than Westerners to express the same emotion",
                                            "feedback": "Individual Feedback1",
                                            "score": "Score",
                                            "pinanswer": 0
                                        },
                                        {
                                            "choiceid": 2,
                                            "assess": false,
                                            "text": "There are nine <b>major</b> emotional expressions.",
                                            "feedback": "Individual Feedback2",
                                            "score": "Score",
                                            "pinanswer": 0
                                        },
                                        {
                                            "choiceid": 3,
                                            "assess": false,
                                            "text": "Facial expressions are not <i>universal</i> because they have different meanings in different cultures.",
                                            "feedback": "Individual Feedback3",
                                            "score": "Score",
                                            "pinanswer": 0
                                        },
                                        {
                                            "choiceid": 4,
                                            "assess": false,
                                            "text": "The six major <b><u>emotional expressions</u></b> appear to be universal.",
                                            "feedback": "Individual Feedback4",
                                            "score": "Score",
                                            "pinanswer": 0
                                        }
                                    ],
                                    "correct_answer": "1",
                                    "global_correct_feedback": {
                                        "text": "Global Correct Feedback"
                                    },
                                    "global_incorrect_feedback": {
                                        "text": "Global Incorrect Feedback"
                                    },
                                    "hints": [
                                        {
                                            "hintid": 1,
                                            "text": "<b>This is hint 1</b>"
                                        },
                                        {
                                            "hintid": 2,
                                            "text": "<b>This is hint 2</b>"
                                        },
                                        {
                                            "hintid": 3,
                                            "text": "<b>This is hint 3</b>"
                                        }
                                    ],
                                    "exhibit": [
                                        {
                                            "path": "",
                                            "exhibit_type": ""
                                        }
                                    ],
                                    "settings": {
                                        "score": "1"
                                    }
                                },
                                {
                                    "question_stem": {
                                        "text": "What is the capital of India"
                                    },
                                    "question_title": {
                                        "text": "MCSS Horizontal"
                                    },
                                    "instruction_text": {
                                        "text": "Instruction text - mcss vertical with media"
                                    },
                                    "question_type": {
                                        "text": "mcss"
                                    },
                                    "question_layout": {
                                        "text": "vertical"
                                    },
                                    "question_interaction": {
                                        "text": "clickable"
                                    },
                                    "choices": [
                                        {
                                            "choiceid": 1,
                                            "assess": true,
                                            "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                                            "text": "New Delhi",
                                            "feedback": "Individual Feedback1",
                                            "score": "Score",
                                            "pinanswer": 0
                                        },
                                        {
                                            "choiceid": 2,
                                            "assess": false,
                                            "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                                            "text": "Kolkata",
                                            "feedback": "Individual Feedback2",
                                            "score": "Score",
                                            "pinanswer": 0
                                        },
                                        {
                                            "choiceid": 3,
                                            "assess": true,
                                            "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                                            "text": "Chennai",
                                            "feedback": "Individual Feedback3",
                                            "score": "Score",
                                            "pinanswer": 0
                                        },
                                        {
                                            "choiceid": 4,
                                            "assess": false,
                                            "media": "<img alt=\"test\" src=\"Content/images/4022_UF02_06.jpg\" style=\"width: 100px; height: 100px;\" title=\"{'asset_id':'5878','alt_tag':'','inst_id':'1','asset_name':'media2484804707_61307e3aa0_o[1]1433414644.jpg','asset_type':'Image','asset_other':''}\" />",
                                            "text": "Mumbai",
                                            "feedback": "Individual Feedback4",
                                            "score": "Score",
                                            "pinanswer": 0
                                        }
                                    ],
                                    "correct_answer": "1",
                                    "global_correct_feedback": {
                                        "text": "Global Correct Feedback"
                                    },
                                    "global_incorrect_feedback": {
                                        "text": "Global Incorrect Feedback"
                                    },
                                    "hints": [
                                        {
                                            "hintid": 1,
                                            "text": "<b>This is hint 1</b>"
                                        },
                                        {
                                            "hintid": 2,
                                            "text": "<b>This is hint 2</b>"
                                        }
                                    ],
                                    "exhibit": [
                                        {
                                            "path": "",
                                            "exhibit_type": ""
                                        }
                                    ],
                                    "settings": {
                                        "score": "1"
                                    }
                                },                           
                           
                        {
                            "question_stem": {
                                "text": "Interpret 6 4 in a real world context"
                            },
                            "question_title": {
                                "text": "MCMS Graphic<br/>"
                            },
                            "instruction_text": {
                                "text": "Instruction text -  mcms exhibit"
                            },
                            "question_type": {
                                "text": "mcms"
                            },
                            "question_layout": {
                                "text": "vertical"
                            },
                            "question_interaction": {
                                "text": "clickable"
                            },
                            "choices": [
                                {
                                    "choiceid": 1,
                                    "assess": true,
                                    "text": "97.8",
                                    "feedback": "Individual Feedback1",
                                    "score": "Score",
                                    "pinanswer": 0
                                },
                                {
                                    "choiceid": 2,
                                    "assess": false,
                                    "text": "99.2",
                                    "feedback": "Individual Feedback2",
                                    "score": "Score",
                                    "pinanswer": 0
                                },
                                {
                                    "choiceid": 3,
                                    "assess": false,
                                    "text": "101.2",
                                    "feedback": "Individual Feedback3",
                                    "score": "Score",
                                    "pinanswer": 0
                                },
                                {
                                    "choiceid": 4,
                                    "assess": false,
                                    "text": "102.6",
                                    "feedback": "Individual Feedback4",
                                    "score": "Score",
                                    "pinanswer": 0
                                }
                            ],
                            "correct_answer": "3",
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global Incorrect Feedback"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "<b>This is hint 1</b>"
                                },
                                {
                                    "hintid": 2,
                                    "text": "<b>This is hint 2</b>"
                                }
                            ],
                            "exhibit": [
                                {
                                    "path": "http://www.youtube.com/embed/nTFEUsudhfs",
                                    "exhibit_type": "video"
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "If a <i><b>&amp;</b></i> b are integers greater than 100 such that a + b = 300, which of the following could be the exact ratio of a to b?"
                            },
                            "question_title": {
                                "text": "If a and b are integers greater than 100 such that a + b = 300, which of the following could be the exact ratio of a to b?"
                            },
                            "instruction_text": {
                                "text": "Instruction text - mcms horizontal"
                            },
                            "question_type": {
                                "text": "mcms"
                            },
                            "question_layout": {
                                "text": "horizontal"
                            },
                            "question_interaction": {
                                "text": "clickable"
                            },
                            "choices": [
                                {
                                    "choiceid": 1,
                                    "assess": true,
                                    "text": "9 to 1",
                                    "feedback": "Individual Feedback1",
                                    "score": "Score",
                                    "pinanswer": 0
                                },
                                {
                                    "choiceid": 2,
                                    "assess": false,
                                    "text": "5 to 2",
                                    "feedback": "Individual Feedback2",
                                    "score": "Score",
                                    "pinanswer": 0
                                },
                                {
                                    "choiceid": 3,
                                    "assess": false,
                                    "text": "4 to 1",
                                    "feedback": "Individual  Feedback3",
                                    "score": "Score",
                                    "pinanswer": 0
                                },
                                {
                                    "choiceid": 4,
                                    "assess": true,
                                    "text": "5 to 2",
                                    "feedback": "Individual  Feedback4",
                                    "score": "Score",
                                    "pinanswer": 0
                                }
                            ],
                            "correct_answer": "1,4",
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global Incorrect  Feedback"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": "<b>This is hint 1</b>"
                                },
                                {
                                    "hintid": 2,
                                    "text": "<b>This is hint 2</b>"
                                }
                            ],
                            "exhibit": [
                                {
                                    "path": "",
                                    "exhibit_type": ""
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                        {
                            "question_stem": {
                                "text": "John is [[1]] old and Micky is [[2]] old. What is there combined age?."
                            },
                            "stem_details":{
                                "type":"algorithimic",
                                "display":"result",
                                "textforeachvalue":[
                                    {
                                        "text": "[[1]]",
                                        "type":"random",
                                        "minvalue":"10",
                                        "maxvalue":"50"
                                    },
                                    {
                                        "text": "[[2]]",
                                        "type":"random",
                                        "minvalue":"20",
                                        "maxvalue":"60"
                                    }
                                ]
                            },
                            "question_title": {
                                "text": "Sample MCQ Stem with Algo #ABC"
                            },
                            "instruction_text": {
                                "text": ""
                            },
                            "question_type": {
                                "text": "mcss"
                            },
                            "question_interaction": {
                                "text": "clickable"
                            },
                            "question_layout": {
                                "text": "horizontal"
                            },
                            "choices": [
                                {
                                    "choiceid": 1,
                                    "assess": false,
                                    "text": "[[1]] * [[2]]",
                                    "feedback": "Individual Feedback",
                                    "score": "Score",
                                    "pinanswer": 0
                                },
                                {
                                    "choiceid": 2,
                                    "assess": false,
                                    "text":  "[[1]] - [[2]]",
                                    "feedback": "Individual Feedback",
                                    "score": "Score",
                                    "pinanswer": 0
                                },
                                {
                                    "choiceid": 3,
                                    "assess": false,
                                    "text":  "[[1]] / [[2]]",
                                    "feedback": "Individual Feedback",
                                    "score": "Score",
                                    "pinanswer": 0
                                },
                                {
                                    "choiceid": 4,
                                    "assess": true,
                                    "text": "[[1]] + [[2]]",
                                    "feedback": "Individual Feedback",
                                    "score": "Score",
                                    "pinanswer": 0
                                }
                            ],
                            "correct_answer": "4",
                            "global_correct_feedback": {
                                "text": "Global Correct Feedback"
                            },
                            "global_incorrect_feedback": {
                                "text": "Global Incorrect Feedback"
                            },
                            "hints": [
                                {
                                    "hintid": 1,
                                    "text": ""
                                }
                            ],
                            "exhibit": [
                                {
                                    "path": "",
                                    "exhibit_type": false
                                }
                            ],
                            "settings": {
                                "score": "1"
                            }
                        },
                    {
                        "question_stem": {
                            "text": "John is [[1]] old and Micky is [[2]] old. Provide a formula for combining there ages?."
                        },
                        "stem_details":{
                            "type":"algorithimic",
                            "display":"separate",
                            "textforeachvalue":[
                                {
                                    "text": "[[1]]",
                                    "type":"random",
                                    "minvalue":"1",
                                    "maxvalue":"100"
                                },
                                {
                                    "text": "[[2]]",
                                    "type":"random",
                                    "minvalue":"1",
                                    "maxvalue":"100"
                                }
                            ]
                        },
                        "question_title": {
                            "text": "Sample MCQ Stem with Algo #ABC"
                        },
                        "instruction_text": {
                            "text": ""
                        },
                        "question_type": {
                            "text": "mcss"
                        },
                        "question_interaction": {
                            "text": "clickable"
                        },
                        "question_layout": {
                            "text": "vertical"
                        },
                        "choices": [
                            {
                                "choiceid": 1,
                                "assess": false,
                                "text": "[[1]] * [[2]]",
                                "feedback": "Individual Feedback",
                                "score": "Score",
                                "pinanswer": 0
                            },
                            {
                                "choiceid": 2,
                                "assess": false,
                                "text": "[[1]] - [[2]]",
                                "feedback": "Individual Feedback",
                                "score": "Score",
                                "pinanswer": 0
                            },
                            {
                                "choiceid": 3,
                                "assess": false,
                                "text": "[[1]] / [[2]]",
                                "feedback": "Individual Feedback",
                                "score": "Score",
                                "pinanswer": 0
                            },
                            {
                                "choiceid": 4,
                                "assess": true,
                                "text": "[[1]] + [[2]]",
                                "feedback": "Individual Feedback",
                                "score": "Score",
                                "pinanswer": 0
                            }
                        ],
                        "correct_answer": "4",
                        "global_correct_feedback": {
                            "text": "Global Correct Feedback"
                        },
                        "global_incorrect_feedback": {
                            "text": "Global Incorrect Feedback"
                        },
                        "hints": [
                            {
                                "hintid": 1,
                                "text": ""
                            }
                        ],
                        "exhibit": [
                            {
                                "path": "",
                                "exhibit_type": false
                            }
                        ],
                        "settings": {
                            "score": "1"
                        }
                    }

                    ]
                },
                "DisplayQuestionId": "121",
                "OriginalQuestionId": "EB0193AD-772B-4589-A67B-3FED28F4B1J3",
                "IsDeleted": false,
                "SelectedOptions": null
        },
        {
            "QuestionId": "EB0193AD-772B-4589-A67B-3FED28F4B1K3",
            "QuestionData": {
                "question_stem": {
                    "text": "Question stem of Composite Question Type"
                },
                "question_title": {
                    "text": "Question stem of Composite Question Type"
                },
                "question_body": {
                    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum faucibus efficitur arcu facilisis interdum. Nulla efficitur, felis quis varius lacinia, lectus nunc fringilla mauris, ac blandit lacus erat nec sapien. In fringilla luctus lectus, non suscipit elit aliquet quis. Maecenas tristique est at nulla porttitor iaculis. Pellentesque blandit placerat tincidunt. Suspendisse et nulla eu neque dignissim fringilla et in massa. Pellentesque condimentum eleifend neque quis auctor. Integer elit ex, ultrices sit amet mollis quis, rhoncus rutrum nisl. Nunc laoreet varius mollis. "
                },
                "instruction_text": {
                    "text": "<b>Instruction Text:</b> Read below paragraph and answer questions"
                },
                "question_type": {
                    "text": "composite"
                },
                "settings": {
                    "score": "1"
                },
                "questions": [
                    {
                        "question_stem": {
                            "text": "Darwins's evolutionary <b><u>perspective</u></b> on nonverbal communication of emotion led him to predict that facial expressions were"
                        },
                        "question_title": {
                            "text": "Darwins's evolutionary perspective on nonverbal communication of emotion led him to predict that facial expressions were"
                        },
                        "instruction_text": {
                            "text": "<strong>Instruction text</strong> - mcss horizontal"
                        },
                        "question_type": {
                            "text": "mcss"
                        },
                        "question_layout": {
                            "text": "horizontal"
                        },
                        "question_interaction": {
                            "text": "clickable"
                        },
                        "choices": [
                            {
                                "choiceid": 1,
                                "assess": true,
                                "text": "universal <b>across</b> all animal species",
                                "feedback": "Individual Feedback1",
                                "score": "Score",
                                "pinanswer": 0
                            },
                            {
                                "choiceid": 2,
                                "assess": false,
                                "text": "related to physiological reactions that proved to be a useful way to respond to particular type of stimulus",
                                "feedback": "Individual Feedback2",
                                "score": "Score",
                                "pinanswer": 0
                            },
                            {
                                "choiceid": 3,
                                "assess": false,
                                "text": "a way to increase, but not decrease input through senses such as vision and smell",
                                "feedback": "Individual Feedback3",
                                "score": "Score",
                                "pinanswer": 0
                            },
                            {
                                "choiceid": 4,
                                "assess": false,
                                "text": "specific to particular cultures",
                                "feedback": "Individual Feedback4",
                                "score": "Score",
                                "pinanswer": 0
                            }
                        ],
                        "correct_answer": "1",
                        "global_correct_feedback": {
                            "text": "Global Correct Feedback"
                        },
                        "global_incorrect_feedback": {
                            "text": "Global Incorrect Feedback"
                        },
                        "hints": [
                            {
                                "hintid": 1,
                                "text": "<b>This is hint 1</b>"
                            },
                            {
                                "hintid": 2,
                                "text": "<b>This is hint 2</b>"
                            }
                        ],
                        "exhibit": [
                            {
                                "path": "",
                                "exhibit_type": ""
                            }
                        ],
                        "settings": {
                            "score": "1"
                        }
                    },
                    {
                        "question_stem": {
                            "text": "If a <i><b>&amp;</b></i> b are integers greater than 100 such that a + b = 300, which of the following could be the exact ratio of a to b?"
                        },
                        "question_title": {
                            "text": "If a and b are integers greater than 100 such that a + b = 300, which of the following could be the exact ratio of a to b?"
                        },
                        "instruction_text": {
                            "text": "Instruction text - mcms horizontal"
                        },
                        "question_type": {
                            "text": "mcms"
                        },
                        "question_layout": {
                            "text": "horizontal"
                        },
                        "question_interaction": {
                            "text": "clickable"
                        },
                        "choices": [
                            {
                                "choiceid": 1,
                                "assess": true,
                                "text": "9 to 1",
                                "feedback": "Individual Feedback1",
                                "score": "Score",
                                "pinanswer": 0
                            },
                            {
                                "choiceid": 2,
                                "assess": false,
                                "text": "5 to 2",
                                "feedback": "Individual Feedback2",
                                "score": "Score",
                                "pinanswer": 0
                            },
                            {
                                "choiceid": 3,
                                "assess": false,
                                "text": "4 to 1",
                                "feedback": "Individual  Feedback3",
                                "score": "Score",
                                "pinanswer": 0
                            },
                            {
                                "choiceid": 4,
                                "assess": true,
                                "text": "5 to 2",
                                "feedback": "Individual  Feedback4",
                                "score": "Score",
                                "pinanswer": 0
                            }
                        ],
                        "correct_answer": "1,4",
                        "global_correct_feedback": {
                            "text": "Global Correct Feedback"
                        },
                        "global_incorrect_feedback": {
                            "text": "Global Incorrect  Feedback"
                        },
                        "hints": [
                            {
                                "hintid": 1,
                                "text": "<b>This is hint 1</b>"
                            },
                            {
                                "hintid": 2,
                                "text": "<b>This is hint 2</b>"
                            }
                        ],
                        "exhibit": [
                            {
                                "path": "",
                                "exhibit_type": ""
                            }
                        ],
                        "settings": {
                            "score": "1"
                        }
                    }
                ]
            },
            "VersionNo": 1,
            "DisplayQuestionId": "121",
            "OriginalQuestionId": "EB0193AD-772B-4589-A67B-3FED28F4B1K3",
            "IsDeleted": false,
            "SelectedOptions": null
        },
        
        {
            "QuestionId": "BE0193AD-772B-4589-B67B-3FED28F4B1A3",
            "QuestionData": {
                "question_stem": {
                    "text": "John is [[1]] old and Micky is [[2]] old. Provide a formula for combining there ages?."
                },
                "stem_details":{
                    "type":"algorithimic",
                    "display":"separate",
                    "textforeachvalue":[
                        {
                            "text": "[[1]]",
                            "type":"random",
                            "minvalue":"1",
                            "maxvalue":"100"
                        },
                        {
                            "text": "[[2]]",
                            "type":"random",
                            "minvalue":"1",
                            "maxvalue":"100"
                        }
                    ]
                },
                "question_title": {
                    "text": "Sample MCQ Stem with Algo #ABC"
                },
                "instruction_text": {
                    "text": ""
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": false,
                        "text": "[[1]] * [[2]]",
                        "feedback": "Individual Feedback",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "[[1]] - [[2]]",
                        "feedback": "Individual Feedback",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "[[1]] / [[2]]",
                        "feedback": "Individual Feedback",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": true,
                        "text": "[[1]] + [[2]]",
                        "feedback": "Individual Feedback",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "4",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": ""
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": false
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "VersionNo": 1,
            "DisplayQuestionId": "121",
            "OriginalQuestionId": "BE0193AD-772B-4589-B67B-3FED28F4B1A3",
            "IsDeleted": false,
            "SelectedOptions": null
        },
						{
            "QuestionId": "KE0193AD-772B-4589-B67B-3FED28F4B1A3",
            "QuestionData": {
                "question_stem": {
                    "text": "John is [[1]] old and Micky is [[2]] old. What is there combined age?."
                },
                "stem_details":{
                    "type":"algorithimic",
                    "display":"result",
                    "textforeachvalue":[
                        {
                            "text": "[[1]]",
                            "type":"random",
                            "minvalue":"1",
                            "maxvalue":"100"
                        },
                        {
                            "text": "[[2]]",
                            "type":"random",
                            "minvalue":"1",
                            "maxvalue":"100"
                        }
                    ]
                },
                "question_title": {
                    "text": "Sample MCQ Stem with Algo #ABC"
                },
                "instruction_text": {
                    "text": ""
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_interaction": {
                    "text": "clickable"
                },
                "question_layout": {
                    "text": "horizontal"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": false,
                        "text": "[[1]] * [[2]]",
                        "feedback": "Individual Feedback",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "[[1]] - [[2]]",
                        "feedback": "Individual Feedback",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "[[1]] / [[2]]",
                        "feedback": "Individual Feedback",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": true,
                        "text": "[[1]] + [[2]]",
                        "feedback": "Individual Feedback",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "4",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": ""
                    }
                ],
                "exhibit": [
                    {
                        "path": "",
                        "exhibit_type": false
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "VersionNo": 1,
            "DisplayQuestionId": "121",
            "OriginalQuestionId": "KE0193AD-772B-4589-B67B-3FED28F4B1A3",
            "IsDeleted": false,
            "SelectedOptions": null
        },
		{
            "QuestionId": "98D70C91-A206-4BA1-98B5-FE15C147B2FD",
            "QuestionData": {
                "question_stem": {
                    "text": "Ms. Nelson evenly distributes 32 crayons to 8 kindergarteners. How many crayons does each student get?"
                },
                "question_title": {
                    "text": "Ms. Nelson evenly distributes 32 crayons to 8 kindergarteners. How many crayons does each student get?"
                },
                "instruction_text": {
                    "text": "Instruction text -  mcss selectable label with Exhibit"
                },
                "question_type": {
                    "text": "mcss"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "selectable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "3 crayons",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "4 crayons",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "6 crayons",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "text": "8 crayons",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "2",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "<img height=\"500\" src=\"Content/images/4022_UF02_06.jpg\" title=\"{'asset_id':'5873','alt_tag':'','inst_id':'1','asset_name':'mediaKoala1433253602.jpg','asset_type':'Image','asset_other':''}\" width=\"500\" />",
                        "exhibit_type": "image"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 1,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "98D70C91-A206-4BA1-98B5-FE15C147B2FD",
            "IsDeleted": false,
            "SelectedOptions": null
        },
		{
            "QuestionId": "8B8E1C62-98B4-4F6C-B3EF-68E3BF37A99C",
            "QuestionData": {
                "question_stem": {
                    "text": "Interpret 6 4 in a real world context"
                },
                "question_title": {
                    "text": "MCMS Graphic<br/>"
                },
                "instruction_text": {
                    "text": "Instruction text -  mcms selectable with exhibit"
                },
                "question_type": {
                    "text": "mcms"
                },
                "question_layout": {
                    "text": "vertical"
                },
                "question_interaction": {
                    "text": "selectable"
                },
                "choices": [
                    {
                        "choiceid": 1,
                        "assess": true,
                        "text": "97.8",
                        "feedback": "Individual Feedback1",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 2,
                        "assess": false,
                        "text": "99.2",
                        "feedback": "Individual Feedback2",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 3,
                        "assess": false,
                        "text": "101.2",
                        "feedback": "Individual Feedback3",
                        "score": "Score",
                        "pinanswer": 0
                    },
                    {
                        "choiceid": 4,
                        "assess": false,
                        "text": "102.6",
                        "feedback": "Individual Feedback4",
                        "score": "Score",
                        "pinanswer": 0
                    }
                ],
                "correct_answer": "3",
                "global_correct_feedback": {
                    "text": "Global Correct Feedback"
                },
                "global_incorrect_feedback": {
                    "text": "Global Incorrect Feedback"
                },
                "hints": [
                    {
                        "hintid": 1,
                        "text": "<b>This is hint 1</b>"
                    },
                    {
                        "hintid": 2,
                        "text": "<b>This is hint 2</b>"
                    }
                ],
                "exhibit": [
                    {
                        "path": "http://www.youtube.com/embed/nTFEUsudhfs",
                        "exhibit_type": "video"
                    }
                ],
                "settings": {
                    "score": "1"
                }
            },
            "QuestionTypeId": 2,
            "VersionNo": 0,
            "DisplayQuestionId": "123",
            "OriginalQuestionId": "8B8E1C62-98B4-4F6C-B3EF-68E3BF37A99C",
            "IsDeleted": false,
            "SelectedOptions": null
        }
    ],
    "settings": {
        "Shuffle": false,
        "BackwardMovement": true,
        "QuestionMap": true,
        "AllowFlagging": false,
        "Attempts": 10,
        "QuestionAttempts": 3,
        "HintOn": true,
        "Timer": {
            "Start": true,
            "Minute": 0
        },
        "PauseTimer": true,
        "SkipQuestion": true,
        "ShowFeedBackAt": {
            "Question": true,
            "Option": true
        },
        "RandomizeQuestion": 0
    }
}