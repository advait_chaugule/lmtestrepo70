﻿window.app = angular.module('ACEApp', ['ngResource', 'ui.bootstrap', 'ui.sortable', 'textAngular', 'ngCookies', 'dndLists', 'timer', 'oitozero.ngSweetAlert']);
var Urls = {
    restAuthUrl: '/ACEAuth/oauth2/token',
    restUrl: '/ACEApi/Api',
    routeUrl: '/NewACE'
};
app.controller('AssessmentEngineCtrl', ['$scope', '$location', '$window', '$http', 'assessmentEngineSvc', 'Enum', '$modal', '$sce', '$filter', '$compile', '$timeout', '$modal', 'SweetAlert',
function ($scope, $location, $window, $http, assessmentEngineSvc, Enum, $modal, $sce, $filter, $compile, $timeout, $modal, SweetAlert) {

    $scope.showModal = false;
    if ($location.absUrl().indexOf("index.html") > -1) { $scope.showModal = true; }
    var routeUrl = Urls.routeUrl;
    var AssignmentID = 54;
    $scope.currentYear = new Date();
    var questionArray = [];
    $scope.mode = 'quiz';
    $scope.operationmode = 1;//0-online,1-offline
    $scope.offline = {};
    $scope.Enum = Enum;
    $scope.showFooter = false;
    $scope.showFeedback = false;
    $scope.showHint = false;
    $scope.visibleHints = [];
    $scope.isLastquestion = true;
    $scope.isBackButtonOn = false;
    $scope.hintCount = 0;
    $scope.isUndefined = function (thing) {
        return (typeof thing === "undefined");
    };
    $scope.AssignmentQuestions = [];
    $scope.questions = [];
    $scope.questiontosave = {};
    $scope.showExhibit = false;
    $scope.isComposite = false;
    $scope.isEssay = false;
    $scope.ShowHintBox = false;
    $scope.BackwardMovement = true;
    $scope.SkipQuestion = true;
    $scope.disableBackBtn = false;
    $scope.disableNextBtn = false;
    $scope.userHasSelectedOptions = false;
    $scope.CorrectAnswerCount = 0;
    $scope.DisableCorrectAnswer = false;

    $scope.ShowGlobalFeedback = false;
    $scope.ShowGlobalIncorrectFeedback = false;
    $scope.ShowGlobalCorrectFeedback = false;
    $scope.showOptionFeedbackModal = false;
    $scope.showOptionFeedbackIcon = false;
    $scope.showCheckAnswerbtn = false;

    $scope.isPaused = false;
    $scope.showTimer = false;
    $scope.callbackTimer = {};
    $scope.timerRunning = true;
    var timeStarted = false;

    //UserAssessmentActivity related variables
    $scope.BookMarked = false;
    $scope.showAttempt = false;
    $scope.NoLearned = true;
    $scope.QuestionMapBookMarkClicked = false;
    $scope.QuestionMapViewAllClicked = false;
    $scope.UserAssessmentActivityDetail = {
        QuestionStatus: 'not-visited',
        IsBookMarked: '',
        NotLearned: '',
        HintsUsed: 0,
        NoOfRetry: 0,
        TimeSpent: 0,
        ReportAProblem: '',
        QuizQuestID: '',
        UserAssessmentActivityType: 0,
        QuestionNo: 0
    };

    $scope.mathEditorLatex = "";
    $scope.mathEditorPrevLatex = "";
    $scope.mathEditorCurrLatex = "";
    var mathEditorPrevLatex = "";

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.trustAsHtml = function (string) {
        return $sce.trustAsHtml(string);
    };
       
    $scope.getQuizDetails = function (quizId) {
        $scope.quizID = quizId;
        //assessmentEngineSvc.SetQuizID(quizID);
        var params = { 'quizId': quizId };
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            if ($location.absUrl().indexOf("index.html") > -1) {
                $scope.loadjsonfile();
            }
        }
        else {
            assessmentEngineSvc.FetchQuizDetails(params).$promise
                   .then(function (response) {
                       $scope.quizName = response.AssignmentName;
                       $scope.CourseID = response.ClassId;
                       // var questions = '';
                       if (!$scope.isUndefined(response.QuestionIds) && response.QuestionIds != null && response.QuestionIds != "") {
                           questionsList = angular.fromJson(response.QuestionIds);
                           angular.forEach(questionsList, function (question, key) {
                               $scope.questions.push(question.QuestionId);
                           });
                           $scope.startQuiz($scope.questions);
                       }

                   }).catch(function (er) {
                       $scope.Message = er.error_description;
                   });
        }
    };

    $scope.startQuiz = function (questions, isCompositeQuestion) {
        //debugger;
        //  $scope.quizID = quizID;
        //var params = { quizID: quizID, currentQuestion: 0 };
        //var params = { 'questionId': questions[0], 'assignmentId': AssignmentID };

        $scope.showFooter = true;
        $scope.AssignmentQuestions = [];
        var getQuestion;
        if ($scope.operationmode == Enum.OperationMode.Offline) {

            if (questions != undefined) {
                $scope.offline.quiz = questions.quiz;
                $scope.AssignmentSetting = questions.settings;
                if (questions.quiz.length > 1) {
                    $scope.isLastquestion = false;
                    if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
                        $scope.showModal = true;
                    }
                    else {
                        $scope.showModal = false;
                    }
                }
                else {
                    $scope.isBackButtonOn = false;
                    $scope.isLastquestion = true;
                    if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
                        $scope.showModal = true;
                    }
                    else {
                        $scope.showModal = false;
                    }
                }
            }

			if ($scope.isLastquestion == true && $scope.showModal == false) {
                $scope.offline.quiz[0].CurrentQuestion = 1;
                $scope.initializeQuiz($scope.offline.quiz[0]);
            }
			else if ($scope.isLastquestion == false && $scope.showModal == false) {
                $scope.offline.quiz[0].CurrentQuestion = 1;
                $scope.initializeQuiz($scope.offline.quiz[0]);
            }
            else if (isCompositeQuestion || $scope.isComposite || $location.absUrl().indexOf("CompositeIndex.html") > -1) {
                $scope.offline.quiz[0].CurrentQuestion = 1;
                $scope.initializeQuiz($scope.offline.quiz[0]);
            }
            else {
                var modalInstance = $modal.open({
                    templateUrl: 'SettingDetails.html',
                    scope: $scope,
                    backdrop: 'static',
                    keyboard: false
                });

                $scope.Ok = function () {
                    modalInstance.close('cancel');
                    $('#quizContainer').show();
                    if ($scope.offline.quiz.length == 1)
                        $scope.isLastquestion = true;
                };

                modalInstance.result.then(function (result) {
                    var index = 0;
                    angular.forEach($scope.offline.quiz, function (question, key) {
                        var UserAssessmentActivityDetail = {
                            QuestionStatus:'not-visited',
                            IsBookMarked: '',
                            NotLearned: '',
                            HintsUsed: 0,
                            NoOfRetry: 0,
                            TimeSpent: 0,
                            ReportAProblem: '',
                            QuizQuestID: '',
                            UserAssessmentActivityType: 0,
                            QuestionNo:0
                        };
                        index = index + 1;
                        UserAssessmentActivityDetail.QuizQuestID = question.QuestionId;
                        UserAssessmentActivityDetail.QuestionNo = index;
                        $scope.AssignmentQuestions.push(UserAssessmentActivityDetail);                        
                    });
                    $scope.searchFilter = { QuestionStatus: '' };
                    $scope.offline.quiz[0].CurrentQuestion = 1;
                    $scope.initializeQuiz($scope.offline.quiz[0]);

                    $scope.Timer = angular.fromJson($scope.AssignmentSetting.Timer);

                    if ($scope.Timer != undefined) {
                        if ($scope.Timer.Start == true && $scope.Timer.Minute != 0) {
                            $scope.showTimer = true;
                            $scope.ActiveTimer($scope.Timer, $scope.CurrentQuestion);
                        }
                        else {
                            $scope.showTimer = false;
                        }
                    }
                }, function () { });
            }

        }
        else {
            assessmentEngineSvc.StartQuiz(JSON.stringify(params)).$promise
                .then(function (response) {
                    response[0].QuestionData = $.parseJSON(response[0].QuestionData)
                    response = response[0];
                    response.CurrentQuestion = 1;
                    $scope.initializeQuiz(response);
                    $scope.ActiveTimer($scope.CurrentQuestion);
                }).catch(function (er) {
                    $scope.Message = er.error_description;
                });
        }
        $('.btn-arrow-left').prop('disabled', true);
        resize_fun();
    };
    $scope.goTo = function (index, Next) {
        $scope.mathEditorRefactor();
        $("#loader").show();
        $("#loader").removeClass('hide');
        $scope.showHint = false;
        $scope.showExhibit = false;
        //$scope.userHasSelectedOptions = false;

        $scope.ShowGlobalFeedback = false;
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.ShowGlobalCorrectFeedback = false;
        $scope.showOptionFeedbackModal = false;

        $scope.DisableCorrectAnswer = false;
        $scope.showAttempt = false;
        $scope.QuestionAttempt = 0;

        $scope.CurrentQuestion.SelectedOptions = $scope.userSelectedOptions();
        $scope.CurrentQuestion.UserAssessmentActivityDetail = $scope.UserAssessmentActivityDetail;
        $scope.CurrentQuestion.CorrectAnswerCount = $scope.CorrectAnswerCount;
        //$scope.CurrentQuestion.isNext = Next;

        $scope.MapQuestionToSave(Next, index);

        if ($scope.userHasSelectedOptions == true) {
            $scope.SetQuestionMapSetting(true, Enum.AssessmentActivityType.SubmissionStatus);
        } else {
            $scope.AssignmentQuestions[index-2].QuestionStatus = 'answer-visited';
        }
        $scope.CurrentQuestion.TimeSpent = $scope.PassiveTimer.stopTimer();
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            if (index <= $scope.offline.quiz.length) {
                if (index == 1) {
                    $('.btn-arrow-left').prop('disabled', true);
                    $scope.isLastquestion = false;
                    $scope.isBackButtonOn = false;
                }
                else {
                    $('.btn-arrow-left').prop('disabled', false);
					$scope.isBackButtonOn = true;
                }
                if ($scope.offline.quiz.length == index) {
                    $('.btn-arrow-right').html('Submit');
                    $('.btn-arrow-right').addClass('dummyClass');
                    $('.btn-arrow-right').removeClass('btn-arrow-right');
                    $scope.isBackButtonOn = true;
                    $scope.isLastquestion = true;
                }
                else {
                    $('.dummyClass').html(' Next <i class="fa fa-arrow-circle-right fa-fw"></i>');
                    $('.dummyClass').addClass('btn-arrow-right');
                    $('.dummyClass').removeClass('dummyClass');
                    $scope.isLastquestion = false;
                }

                $scope.offline.quiz[index - 1].CurrentQuestion = index;
                $scope.initializeQuiz($scope.offline.quiz[index - 1]);
                //  if (document.getElementById('compositeIframe') != null) document.getElementById('compositeIframe').contentWindow.location.reload();
            }
            else {
                $scope.stopTimer();
                $scope.showFeedback = true;
                $scope.generateFeedback();
            }
            $("#loader").hide();
            $("#loader").addClass('hide');
        }
        else {
            if (index == 1) {
                $('.btn-arrow-left').prop('disabled', true);
            }
            else {
                $('.btn-arrow-left').prop('disabled', false);
            }

            if ($scope.totalItems == index) {
                $('.btn-arrow-right').html('Submit');
                $('.btn-arrow-right').removeClass('btn-arrow-right');
            }
            assessmentEngineSvc.FetchQuestion(serializeData(params)).$promise
                .then(function (response) {

                    if ($scope.questiontosave.NextQuestionId !== undefined) {//index <= $scope.totalItems
                        response[0].QuestionData = $.parseJSON(response[0].QuestionData)
                        response = response[0];
                        response.CurrentQuestion = index;
                        $scope.initializeQuiz(response);
                    }
                    else {
                        $scope.TimerClass.stopTimer(function () { });
                        $scope.generateFeedback();
                        $scope.showFeedback = true;
                    }
                }).catch(function (er) {
                    $scope.Message = er.error_description;
                });
        }


    };

    $scope.MapQuestionToSave = function (Next, index) {
        $scope.questiontosave.ApplicationID = 0,
        $scope.questiontosave.ProductID = 9209,
        $scope.questiontosave.CourseID = $scope.CourseID,
        $scope.questiontosave.ItemGUID = AssignmentID,
        $scope.questiontosave.UserID = 0,
        $scope.questiontosave.QuestionGUID = $scope.CurrentQuestion.QuestionId,
        $scope.questiontosave.SelectedOptions = $scope.userSelectedOptions(),
        $scope.questiontosave.MaxScore = 0,
        $scope.questiontosave.UserScore = 0,
        $scope.questiontosave.UserAnswerText = "",
        $scope.questiontosave.UserAudioFile = "",
        $scope.questiontosave.TimeSpent = 0,
        $scope.questiontosave.isNext = Next,
        $scope.questiontosave.CurrentQuestion = index,
        $scope.questiontosave.QuestionTypeID = $scope.CurrentQuestion.QuestionTypeId,
        $scope.questiontosave.NextQuestionId = $scope.questions[index - 1],
        $scope.questiontosave.AssignmentID = AssignmentID
    };

    $scope.generateFeedback = function () {
        $scope.showHint = false;
        $('#spnMathQuillCreateMathEquation').mathquill();
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            //$scope.loadjsonfile();
            $scope.FeedBack = $scope.offline.quiz;
            $.each($scope.FeedBack, function (index, value) {
                $scope.FeedBack[index].QuestionData = value.QuestionData;
                $scope.FeedBack[index].QuestionCount = index + 1;
                $scope.FeedBack[index].QuestionOptions = value.QuestionData.choices;
                //$scope.FeedBack[index].layoutType = value.QuestionData.question_layout.text;;
                $scope.FeedBack[index].draggableItems = [];
                $scope.FeedBack[index].leftvertical = "";
                $scope.FeedBack[index].rightvertical = "";
                var questionType = value.QuestionTypeId;

                if (!$scope.isUndefined(value.QuestionData.stem_details) && value.QuestionData.stem_details.type == 'algorithimic') {
                    $scope.SetFeedbackAlgorithimicType(value);
                }

                if (!$scope.isUndefined(value.QuestionData.question_type)) {
                    if ((value.QuestionData.question_type.text == 'composite' || value.QuestionData.question_type.text == 'choicematrix' || value.QuestionData.question_type.text == 'mcss' || value.QuestionData.question_type.text == 'mcms' || value.QuestionData.question_type.text == 'order' || value.QuestionData.question_type.text == 'fib' || value.QuestionData.question_type.text == 'fib_dnd' || value.QuestionData.question_type.text == 'matching' || value.QuestionData.question_type.text == 'essay') && value.QuestionData.question_type.text.length > 0) {
                        questionType = value.QuestionData.question_type.text;
                    }
                }
                $scope.FeedBack[index].questionTypeId = questionType;
                if (questionType == Enum.QuestionType.Composite) {                  
                    $scope.SetCompositeType($scope.FeedBack[index], index);
                }
                else if (questionType == Enum.QuestionType.FIB || questionType == Enum.QuestionType.FIBDND || questionType == Enum.QuestionType.Matching) {
                    var questiontext = "";
                    if (value.QuestionData.textwithblanks != undefined)
					{
                        if (value.QuestionData.textwithblanks.text != undefined && value.QuestionData.textwithblanks.text != "")
							questiontext = $scope.FeedBack[index].QuestionData.textwithblanks.text;
					}
					
                    $scope.FillFeedbackDroppableItems($scope.FeedBack[index]);
                    angular.forEach($scope.FeedBack[index].QuestionData.choices, function (option, key) {
                        var textToReplace = option.noforeachblank;
                        var htmlText = "";
                        var answerClass = "";
                        var inputValue = "";

                        if ($scope.FeedBack[index].SelectedOptions) {
                            var selectedOption = angular.fromJson($scope.FeedBack[index].SelectedOptions);
                            $(selectedOption).each(function (fibindex, value) {
                                if (value.noforeachblank == option.noforeachblank && value.inputvalue != "") {
                                    inputValue = value.inputvalue;
                                    answerClass = (value.isCorrectAnswer) ? "correct" : "incorrect";
                                }
                            });
						}
						if (option.interaction == 'textentry') {
							htmlText = "<span class='option " + answerClass + "'><input type='text' class='form-control' disabled value='" + inputValue + "'><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
						}
						else if (option.interaction == 'inline') {
							htmlText = "<span class='option " + answerClass + "'><select class='form-control' disabled><option>" + inputValue + "</option></select><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
						}
						else if (option.interaction == 'dragdrop' || option.interaction == 'container') {
							$scope.FeedBack[index].LayoutType = value.QuestionData.question_layout.text;

							if (option.interaction == 'dragdrop') {
								angular.forEach($scope.FeedBack[index].draggableItems, function (ditem, dindex) {
									if ($filter('lowercase')(inputValue) == $filter('lowercase')(ditem.title)) {
										ditem.title = "__________";
									}
								});
							}
							else if (option.interaction == 'container') {
								var input = !!inputValue ? inputValue.map(function (x) { return x.title; }) : [];
								angular.forEach($scope.FeedBack[index].draggableItems, function (ditem, dindex) {
									if (input.indexOf(ditem.title) != -1) {
										ditem.title = "__________";
									}
								});
							}

							if ($scope.FeedBack[index].QuestionData.question_layout.text != "dragdrop_parallel") {
								htmlText = "<span class='option drop " + answerClass + "'><span class='option drag'>" + inputValue + "</span><span class='sign'><span class='fa'></span></span></span>";
								if ($scope.FeedBack[index].QuestionData.question_layout.text === "right" || $scope.FeedBack[index].QuestionData.question_layout.text === "left" || $scope.FeedBack[index].QuestionData.question_layout.text === "vertical") {
									$scope.FeedBack[index].leftvertical = "col-md-10";
									$scope.FeedBack[index].rightvertical = "col-md-2 right";
								}
								else if ($scope.FeedBack[index].QuestionData.question_layout.text === "row") {
									$scope.FeedBack[index].leftvertical = "";
									$scope.FeedBack[index].rightvertical = "text-center";
								}

								$scope.FeedBack[index].QuestionOptions[key].isCorrectAnswer = answerClass;
								$scope.FeedBack[index].QuestionOptions[key].selectedAnswer = inputValue;
							}
							else {
								$scope.FeedBack[index].QuestionOptions[key].isCorrectAnswer = answerClass;
								$scope.FeedBack[index].QuestionOptions[key].selectedAnswer = inputValue;
							}
						}

						questiontext = questiontext.replace(textToReplace, htmlText);

                    });
                    $scope.FeedBack[index].FIBText = questiontext;

                }

               else if (questionType == Enum.QuestionType.MCSS || questionType == Enum.QuestionType.MCMS) {
                    value.questionSubType = value.QuestionData.question_interaction.text;
                    value.LayoutType = value.QuestionData.question_layout.text;
                    if (!$scope.isUndefined(value.QuestionData.exhibit) && value.QuestionData.exhibit.length > 0 && value.QuestionData.exhibit[0].exhibit_type != "") {
                        value.showExhibit = true;
                        if (value.QuestionData.exhibit[0].exhibit_type == 'image') {
                            var imagePath = String(value.QuestionData.exhibit[0].path).replace("'\'", "");
                            value.QuestionData.exhibit[0].path = imagePath;
                        }
                    }
                    else {
                        value.showExhibit = false;
                    }
                }

                $($scope.FeedBack[index].QuestionData.choices).each(function (indexOpt, valueOpt) {
                    //if (valueOpt.assess == true) {
                    if (questionType == Enum.QuestionType.MCSS || questionType == Enum.QuestionType.MCMS) {
                        var correctoptions = $scope.FeedBack[index].QuestionData.correct_answer.split(",");
                        $scope.FeedBack[index].CorrectOptions = correctoptions.toString();
                        var sSrchTxt = String(valueOpt.text);
                        if (sSrchTxt.indexOf('<img') != -1) {
                            var options = String(valueOpt.text).split('<img');
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].text = options[0];
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].MCSSImage = "<img" + options[1];
                        }
                        else {
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].text = sSrchTxt;
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].MCSSImage = null;
                        }

                        if (this.isAnswered == true) {
                            $(correctoptions).each(function (correctindex) {
                                if (parseInt(valueOpt.choiceid) != parseInt(correctoptions[correctindex])) {
                                    valueOpt.isCorrect = false;
                                    valueOpt.currentClass = "incorrect";
                                }
                            });
                        }

                        $(correctoptions).each(function (correctindex) {
                            if (parseInt(valueOpt.choiceid) == parseInt(correctoptions[correctindex])) {
                                valueOpt.isCorrect = true;
                                valueOpt.currentClass = "correct";
                            }
                        });
                    }

                    if (questionType == Enum.QuestionType.ChoiceMatrix) {
                        if (value.SelectedOptions) {
                            var selectedOpt = angular.fromJson(value.SelectedOptions);
                            $(selectedOpt).each(function (selectindex, selectvalue) {
                                var inputValues = selectvalue.inputvalue.split("-");
                                var optionindex = indexOpt + 1;
                                $($scope.FeedBack[index].QuestionData.choices[indexOpt].suboption).each(function (index1, value1) {
                                    if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                                        $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isAnswered = true;
                                        var assess;
                                        if (parseInt(inputValues[1]) == 1) {
                                            assess = true;
                                        }
                                        else {
                                            assess = false;
                                        }
                                        if ($scope.FeedBack[index].QuestionData.choices[indexOpt].assess === assess) {
                                            $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isCorrect = true;
                                        }
                                        else {
                                            $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isCorrect = false;
                                        }
                                    }
                                    else {
                                        var val = (value1.value === "true");
                                        if ($scope.FeedBack[index].QuestionData.choices[indexOpt].assess === val) {
                                            $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isCorrect = true;
                                        }
                                    }
                                });
                            });

                        }
                    }

                    else if (questionType == Enum.QuestionType.Order) {
                        $scope.FeedBack[index].OrderSetting = $scope.FeedBack[index].QuestionData.question_interaction.text;

                        var correctoptions = $scope.FeedBack[index].QuestionData.correct_answer.split(",");
                        var optionindex = valueOpt.choiceid;
                        if (parseInt(optionindex) == correctoptions[indexOpt]) {
                            valueOpt.isCorrect = true;
                        }
                        else {
                            valueOpt.isCorrect = false;
                        }

                        if ($scope.orderType == "media" && $scope.FeedBack[index].QuestionData.question_layout != null)
                            $scope.FeedBack[index].LayoutType = $scope.FeedBack[index].QuestionData.question_layout.text;
                    }
                });
                if (questionType == Enum.QuestionType.Essay) {
                    $scope.text = '';
                    $scope.FeedBack[index].EssayText = '';
                    if (value.SelectedOptions) {
                        if ($scope.FeedBack[index].QuestionData.toolbar.type == "math_3-5_palette" || $scope.FeedBack[index].QuestionData.toolbar.type == "math_6-8_palette") {
                            $scope.FeedBack[index].IsFeedbackMathType = true;
                            $('#spnMathQuillCreateMathEquation').mathquill('latex', value.SelectedOptions);

                            // Added the logic as Bootstrap is conflicting with the MATH font.
                            $('#spnMathQuillCreateMathEquation span.sqrt-prefix[style^="transform"]').attr("style", "transform: scale(1,1);");
                            $('#spnMathQuillCreateMathEquation span.scaled[style^="transform"]').attr("style", "transform: scale(1,1);");

                            $('#divMathQuillCreateMathEquation span').each(function () {
                                if ($(this).html() == '?') {
                                    $(this).html('');
                                }
                            });

                            $scope.FeedBack[index].feedbackMathType = $sce.trustAsHtml($('#divMathQuillCreateMathEquation').html());
                        } else {
                            $scope.FeedBack[index].IsFeedbackMathType = false;
                            $scope.FeedBack[index].EssayText = value.SelectedOptions; //$sce.trustAsHtml(value.SelectedOptions)
                        }
                    }

                }

            });
            $scope.getFeedbackOptions($scope.FeedBack);
        }


    };

    $scope.SetFeedbackAlgorithimicType = function (data) {
        if (!$scope.isUndefined(data.AlgorithmicVariableArray) && data.QuestionData.stem_details.textforeachvalue.length > 0) {
            angular.forEach(data.QuestionData.stem_details.textforeachvalue, function (textDetail, key) {
                angular.forEach(data.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    data.QuestionData.question_stem.text = data.QuestionData.question_stem.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                });
            });
        }
    }
 
    $scope.FillFeedbackDroppableItems = function (data) {
		isDistractorsPushDone = false;
        angular.forEach(data.QuestionData.choices, function (option, key) {
            if(isDistractorsPushDone == false)
			{
				angular.forEach(data.QuestionData.distractors, function (titem, tindex) {
					data.draggableItems.push({ 'title': titem.text, 'drag': false });
				});
				isDistractorsPushDone = true;
			}

            angular.forEach(option.textforeachblank, function (titem, tindex) {
                data.draggableItems.push({ 'title': titem.text, 'drag': false });
            });
        });
    }

    $scope.getFeedbackOptions = function (data) {
        $(data).each(function (index, value) {
            var questionType = value.QuestionTypeId;
            if (!$scope.isUndefined(value.QuestionData.question_type)) {
                if ((value.QuestionData.question_type.text == 'mcss' || value.QuestionData.question_type.text == 'mcms' || value.QuestionData.question_type.text == 'order' || value.QuestionData.question_type.text == 'fib' || value.QuestionData.question_type.text == 'fib_dnd' || value.QuestionData.question_type.text == 'matching' || value.QuestionData.question_type.text == 'essay') && value.QuestionData.question_type.text.length > 0) {
                    questionType = value.QuestionData.question_type.text;
                }
            }
            if (questionType != Enum.QuestionType.FIB && questionType != Enum.QuestionType.FIBDND && questionType != Enum.QuestionType.Matching && questionType != Enum.QuestionType.Essay && questionType != Enum.QuestionType.ChoiceMatrix) {
                if (value.SelectedOptions != "" && value.SelectedOptions != null && value.SelectedOptions != "[]") {
                    var selectedOptions = angular.fromJson(value.SelectedOptions);
                }
                if (value.CorrectOptions != null) {
                    var correctoptions = value.CorrectOptions.split(",");
                }
                $(data[index].QuestionData.choices).each(function (index1, value1) {
                    var optionindex = index1 + 1;
                    if (selectedOptions) {
                        $(selectedOptions).each(function (selectindex, selectvalue) {
                            var selectedindex = parseInt(selectvalue.choiceid) - 1;
                            $(correctoptions).each(function (correctindex, correctvalue) {
                                var correctindex = correctvalue - 1;
                                if (correctvalue === parseInt(selectvalue)) {
                                    data[index].QuestionData.choices[selectedindex].isCorrect = true;
                                }
                                else {

                                    data[index].QuestionData.choices[selectedindex].isCorrect = false;
                                    data[index].QuestionData.choices[correctindex].isCorrect = true;
                                }
                                data[index].QuestionData.choices[selectedindex].isSelected = true;
                            });
                        });
                    }
                });
            }

        });

    };

    $scope.loadjsonfile = function () {
        $("#loader").show();
        $("#loader").removeClass('hide');
        var file = "js/offlinejson.js";
        $http.get(file)
         .then(function (res) {
             var data = res.data.quiz;
             var settings = res.data.settings;
             //var data = jQuery.grep(res.data.quiz, function (n, i) {
             //    return (parseInt(n.ItemID) == parseInt($scope.quizID));
             //});;
             $scope.offline.quizInfo = "Demo Quiz";//data[0].quizName;
             $scope.offline.quiz = data;
             $scope.AssignmentSetting = settings;

             if ($scope.showFeedback == true) {
                 $scope.quizName = "Feedback - Demo";
             }
             else {
                 $scope.quizName = "Demo";
             }

             $scope.startQuiz();
             $("#loader").hide();
             $("#loader").addClass('hide');
         });
    }

    $scope.autoSubmit = function () {
        $scope.showFeedback = true;
        $scope.generateFeedback();       
        $scope.$apply();
    };

    $scope.ShowGlobalFeebackData = function (data) {
        if ($scope.isLastquestion && jQuery.isEmptyObject($scope.AssignmentSetting)) {
            $scope.ShowGlobalFeedback = true;
        }
        if (data == 'correct') {
            if ($scope.CurrentQuestion.QuestionData.global_correct_feedback.text != null && $scope.CurrentQuestion.QuestionData.global_correct_feedback.text != "" ) {
                $scope.globalCorrectFeedback = '<span math dynamic>' + $scope.CurrentQuestion.QuestionData.global_correct_feedback.text + '</span>';
                $scope.ShowGlobalCorrectFeedback = true;
                //$scope.ShowGlobalIncorrectFeedback = false;
            }
            else {
                $scope.ShowGlobalCorrectFeedback = false;
            }
        }
        else if (data == 'incorrect') {
            if ($scope.CurrentQuestion.QuestionData.global_incorrect_feedback.text != null && $scope.CurrentQuestion.QuestionData.global_incorrect_feedback.text != "") {
                $scope.globalInCorrectFeedback = '<span math dynamic>' + $scope.CurrentQuestion.QuestionData.global_incorrect_feedback.text + '</span>';
                $scope.ShowGlobalIncorrectFeedback = true;
                //$scope.ShowGlobalCorrectFeedback = false;
            }
            else {
                $scope.ShowGlobalIncorrectFeedback = false;
            }
        }

    };

    $scope.showHintData = function () {
        $scope.showHint = true;
        $scope.hintCount = $scope.hintCount - 1;
        $scope.UpdateUserAssessmentActivity($scope.UserAssessmentActivityDetail.HintsUsed, Enum.AssessmentActivityType.ShowHint);
        $scope.visibleHints.push('<span math dynamic>' + $scope.CurrentQuestion.QuestionData.hints[$scope.UserAssessmentActivityDetail.HintsUsed - 1].text + '</span>');
    };

    $scope.BookMarkQuestion = function () {
        if ($scope.BookMarked) {
            $scope.BookMarked = false;
        }
        else {
            $scope.BookMarked = true;
        }
        $scope.UpdateUserAssessmentActivity($scope.BookMarked, Enum.AssessmentActivityType.BookMark);
    };

    $scope.NotLearnedQuestion = function () {
        if ($scope.NotLearned) { return false; }
        $scope.NotLearned = true;
        $scope.disableNextBtn = false;
        $scope.UpdateUserAssessmentActivity($scope.NotLearned, Enum.AssessmentActivityType.NotLearned);
    };

    $scope.questionQuizPreviewCacncel = function () {
        angular.element('#quizContainer').hide();
    }

    $scope.UpdateUserAssessmentActivity = function (updateValue, activityType) {
        switch (activityType) {
            case Enum.AssessmentActivityType.BookMark:
                $scope.UserAssessmentActivityDetail.IsBookMarked = updateValue;
                break;
            case Enum.AssessmentActivityType.ShowHint:
                $scope.UserAssessmentActivityDetail.HintsUsed = $scope.UserAssessmentActivityDetail.HintsUsed + 1;
                break;
            case Enum.AssessmentActivityType.NotLearned:
                $scope.UserAssessmentActivityDetail.NotLearned = updateValue;
                break;
            case Enum.AssessmentActivityType.NoOfTry:
                $scope.UserAssessmentActivityDetail.NoOfRetry = $scope.UserAssessmentActivityDetail.NoOfRetry + 1;
                break;
            case Enum.AssessmentActivityType.ReportAProblem:
                $scope.UserAssessmentActivityDetail.ReportAProblem = updateValue;
                break;
            default:
                break;
        }
        $scope.UserAssessmentActivityDetail.QuizQuestID = $scope.CurrentQuestion.QuestionId;
        $scope.UserAssessmentActivityDetail.UserAssessmentActivityType = activityType;

        $scope.SetQuestionMapSetting(updateValue, activityType);
    };

    $scope.SetQuestionMapSetting = function (updateValue, activityType) {
        angular.forEach($scope.AssignmentQuestions, function (question, key) {
            if ($scope.CurrentQuestion.QuestionId == question.QuizQuestID) {
                switch (activityType) {
                    case Enum.AssessmentActivityType.BookMark:
                        question.IsBookMarked = updateValue;
                        if (question.QuestionStatus == 'not-visited')
                        question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.ShowHint:
                        question.HintsUsed = $scope.UserAssessmentActivityDetail.HintsUsed + 1;
                        if (question.QuestionStatus == 'not-visited')
                        question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.NotLearned:
                        question.NotLearned = updateValue;
                        if (question.QuestionStatus == 'not-visited')
                        question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.NoOfTry:
                        question.NoOfRetry = $scope.UserAssessmentActivityDetail.NoOfRetry + 1;
                        if (question.QuestionStatus == 'not-visited')
                        question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.SubmissionStatus:
                        question.QuestionAttempted = true;
                        question.QuestionStatus = 'answer-right';
                        break;
                    default:
                        break;
                }
                return;
            }
        });
    };
    
    $scope.ShowFilterQuestions = function (filterType) {
        switch (filterType) {
            case Enum.QuestionFilterType.NotVisited:
                $scope.searchFilter = { QuestionStatus: 'not-visited' };
                break;
            case Enum.QuestionFilterType.VisitedNotAnswered:
                $scope.searchFilter = { QuestionStatus: 'answer-visited' };
                break;
            case Enum.QuestionFilterType.Completed:
                $scope.searchFilter = { QuestionStatus: 'answer-right' };
                break;
            case Enum.QuestionFilterType.IsBookMarked:
                $scope.searchFilter = { IsBookMarked: true };                
                break;
            case Enum.QuestionFilterType.ViewAll:
                if ($scope.searchFilter.QuestionStatus != "") {
                    $scope.searchFilter = { QuestionStatus: '' };
                    $('#toggleTest').trigger('click');
                }
                break;
            case Enum.QuestionFilterType.ViewAttended:
                if ($scope.searchFilter.QuestionStatus != "not-visited") {
                    $scope.searchFilter = { QuestionStatus: 'not-visited' };
                    $('#toggleTest').trigger('click');
                }
                break;
            default:
                $scope.searchFilter = { QuestionStatus: '' };
                break;
        }

        if (filterType == Enum.QuestionFilterType.IsBookMarked) {
            $scope.QuestionMapBookMarkClicked = true;           
        }
        else {
            $scope.QuestionMapBookMarkClicked = false;
        }        
    };

    $(window).resize(function () {
        resize_fun();
    });

    

    $scope.showHideOptionFeedbackIcon = function () {
        if ($scope.questionTypeId != Enum.QuestionType.Composite) {
            var selectedOptions = angular.fromJson($scope.userSelectedOptions());
            $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
                $(selectedOptions).each(function (sindex, soption) {
                    //if (value != null && value != "") {
                    if (soption["choiceid"] != undefined && soption["choiceid"] != "") {
                        if (value.choiceid == soption["choiceid"]) {
                            $scope.showHideOptionFeedbackIconInternal(value);
                        }
                    } else if (soption["inputvalue"] != undefined && soption["inputvalue"] != "") {
                        if (value.choiceid == soption["inputvalue"].split('-')[0]) {
                            $scope.showHideOptionFeedbackIconInternal(value);
                        }
                    }
                });
            });
        }
    };

    $scope.showHideOptionFeedbackIconInternal = function (value) {
        if (!$scope.showOptionFeedbackModal) {
            if (value.feedback != null && value.feedback != "" && $scope.isLastquestion && jQuery.isEmptyObject($scope.AssignmentSetting)) {
                $scope.showOptionFeedbackModal = true;
                $('.fdback').removeClass('ng-hide');
            }
            else {
                $scope.showOptionFeedbackModal = false;
                $('.fdback').addClass('ng-hide');
            }
        }
    };

    $scope.showCorrectAnswer = function () {
        $scope.ShowGlobalCorrectFeedback = false;
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.selectedItems = [];
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            if ($scope.questionTypeId == Enum.QuestionType.Composite) {
                $scope.CheckCompositeAnswers($scope.CurrentQuestion.QuestionData.questions)
            }
            if ($scope.userSelectedOptions() != "" && $scope.userSelectedOptions() != "[]") {
                $scope.selectedItems = $scope.userSelectedOptions();
                if ($('#checkAnswerbtn').html() == 'Check Answer') {
                    $('#checkAnswerbtn').html('Try Again');
                    if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
                        $scope.UpdateUserAssessmentActivity($scope.UserAssessmentActivityDetail.NoOfRetry, Enum.AssessmentActivityType.NoOfTry);
                        $scope.showAttempt = true;
                        $scope.CorrectAnswerCount = $scope.CorrectAnswerCount + 1;
                        //if ($scope.QuestionAttempt > 1) $('#checkAnswerbtn').html('Try Again');
                        if ($scope.QuestionAttempt == $scope.CorrectAnswerCount) {
                            $scope.DisableCorrectAnswer = true;
                        }
                    }
                }
                else {
                    //Added by Khadir
                    $(".alert").addClass('ng-hide');
                    $('#checkAnswerbtn').html('Check Answer');
                    $(".option").removeClass('correct incorrect selected');
                    //$(".option").removeClass('incorrect');
                    //$(".option").removeClass('selected');
                    $(".option input:checked").prop('checked', false)
                    $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                        if ($scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.Matching) {
                            this.isAnswered = false;
                            this.AnswerClass = "";
                            if (!!value.UserInput) value.UserInput = "";
                            if (!!value.Canvas && value.Canvas.length > 0) {
                                $(value.Canvas).each(function (cindex, cvalue) {
                                    cvalue.AnswerClass = "";
                                    cvalue.selectedClass = "";
                                    $scope.droppableItems.push(cvalue);
                                });
                                value.Canvas = [];
                            }
                        }
                        else if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
                            value.isChoiceAnswered = false;
                            $(value.suboption).each(function (sindex, svalue) {
                                svalue.currentClass = "";
                                svalue.isAnswered = "";
                                svalue.isOptionAnswered = false;
                                value.suboptionSelected = "";
                            });
                        }
                        else {
                            this.isAnswered = false;
                            this.currentClass = "";
                        }
                    });

                    if ($scope.questionTypeId == Enum.QuestionType.Order) {
                        $scope.CurrentQuestion.QuestionData.choices.sort(function (a, b) {
                            if (a.CurrentOptionIndex < b.CurrentOptionIndex) return -1;
                            if (a.CurrentOptionIndex > b.CurrentOptionIndex) return 1;
                            return 0;
                        })
                    }

                    //$scope.showHint = false;
                    //$scope.UserAssessmentActivityDetail.HintsUsed = 0;
                    //$scope.hintCount = $scope.CurrentQuestion.QuestionData.hints.length;
                    //$scope.visibleHints = [];

                    //angular.forEach($scope.AssignmentQuestions, function (question, key) {
                    //    if ($scope.CurrentQuestion.QuestionId == question.QuizQuestID) {
                    //        question.HintsUsed = 0;
                    //    }
                    //});

                    return;
                }
            }

            $scope.showHideOptionFeedbackIcon();
            if ($scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.Matching) {
                $scope.checkCorrectTextEntered();
            }
            else if ($scope.questionTypeId == Enum.QuestionType.Order) {
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                    var correctoptions = $scope.CurrentQuestion.QuestionData.correct_answer.split(",");
                    var optionindex = value.choiceid;
                    if (parseInt(optionindex) == correctoptions[index]) {
                        value.isCorrect = true;
                        value.currentClass = "correct";
                        // $scope.ShowGlobalFeebackData(value.currentClass);
                    }
                    else {
                        value.isCorrect = false;
                        value.currentClass = "incorrect";
                        $scope.ShowGlobalIncorrectFeedback = true;
                        //$scope.ShowGlobalFeebackData(value.currentClass);
                    }
                });
                if ($scope.ShowGlobalIncorrectFeedback) {
                    $scope.ShowGlobalFeebackData("incorrect");
                }
                else {
                    $scope.ShowGlobalFeebackData("correct");
                }
            }
            else if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
                $scope.selectedoptions = angular.fromJson($scope.selectedItems);

                var currentAccess;
                if (parseInt($scope.selectedOption) == 1) {
                    currentAccess = true;
                }
                else {
                    currentAccess = false;
                }

                $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                    var optionindex = index + 1;
                    $(value.suboption).each(function (sindex, soption) {
                        if (value.suboptionSelected == soption.identifier) {
                            if (value.isAnswered.toString() == soption.isAnswered.toString()) {
                                $scope.CurrentQuestion.QuestionData.choices[index].suboption[sindex].currentClass = "correct";
                            } else {
                                $scope.CurrentQuestion.QuestionData.choices[index].suboption[sindex].currentClass = "incorrect";
                                $scope.ShowGlobalIncorrectFeedback = true;
                            }
                        }

                    });
                });

                if ($scope.selectedoptions.length > 0) {
                    if (!$scope.ShowGlobalIncorrectFeedback && ($scope.selectedoptions.length == $scope.CurrentQuestion.QuestionData.choices.length)) {
                        $scope.ShowGlobalFeebackData("correct");
                    }
                    else {
                        $scope.ShowGlobalFeebackData("incorrect");
                    }
                }
            }
            else {
                if ($scope.questionTypeId != Enum.QuestionType.Composite) {
                    $scope.correctoptions = $scope.CurrentQuestion.QuestionData.correct_answer.split(",");
                    $scope.selectedoptions = angular.fromJson($scope.selectedItems).map(function (x) { return x.choiceid.toString(); });

                    $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                        var optionindex = index + 1;
                        var answerFound = false;

                        if (this.isAnswered == true) {
                            //if (value.assess == true) {

                            $($scope.correctoptions).each(function (correctindex) {
                                if (parseInt(value.choiceid) == parseInt($scope.correctoptions[correctindex])) {
                                    answerFound = true;
                                }
                            });
                            if (answerFound) {
                                value.isCorrect = true;
                                value.currentClass = "correct";
                                //$scope.ShowGlobalFeebackData(value.currentClass);
                            }
                            else {
                                value.isCorrect = false;
                                value.currentClass = "incorrect";
                                // $scope.ShowGlobalFeebackData(value.currentClass);
                                // $scope.ShowGlobalIncorrectFeedback = true;
                            }
                        }
                        else {
                            value.currentClass = "";
                        }

                    });
                    if ($scope.selectedoptions.length > 0) {
                        var globalCorrectFeedback = checkEqualArray($scope.selectedoptions, $scope.correctoptions);
                        if (globalCorrectFeedback) {
                            $scope.ShowGlobalFeebackData("correct");
                        }
                        else {
                            $scope.ShowGlobalFeebackData("incorrect");
                        }
                    }
                }

            }

        }

    };
    //************************************Composite question code starts****************************/
    //****************************************Composite question Feedback***********************
    $scope.SetCompositeType = function (compositeQuestion, compositeIndex) {
        $.each(compositeQuestion.QuestionData.questions, function (qindex, question) {
            question.Index = qindex;
            var selectedOptions = ''; 
            if (compositeQuestion.SelectedOptions!=null &&  !$scope.isUndefined(compositeQuestion.SelectedOptions))
            {
                selectedOptions = compositeQuestion.SelectedOptions[qindex].UserSelectedOptions;
            }

            if (!$scope.isUndefined(question.stem_details) && question.stem_details.type == 'algorithimic') {
                $scope.SetCompositeFeedbackAlgorithimicType(question);
            }

            if (question.question_type.text == Enum.QuestionType.MCSS || question.question_type.text == Enum.QuestionType.MCMS) {
                $scope.SetFeedbackForCompositeMCSSMCMS(question, selectedOptions);
            }
            else if (question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
                $scope.SetFeedbackForCompositeFIBDNDMatching(question, selectedOptions);
            }
            else if (question.question_type.text == Enum.QuestionType.Order) {
                $scope.SetFeedbackForCompositeOrder(question, selectedOptions);
            }
            else if (question.question_type.text == Enum.QuestionType.ChoiceMatrix)
            {
                $scope.SetFeedbackForCompositeChoiceMatrix(question, selectedOptions);
            }

        });
    }
    $scope.SetCompositeFeedbackAlgorithimicType = function (question) {
        if (!$scope.isUndefined(question.AlgorithmicVariableArray) && question.stem_details.textforeachvalue.length > 0) {
            angular.forEach(question.stem_details.textforeachvalue, function (textDetail, key) {
                angular.forEach(question.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    question.question_stem.text = question.question_stem.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                });
            });
        }
    }

    $scope.SetFeedbackForCompositeMCSSMCMS = function (question, selectedOptions) {
        if (!$scope.isUndefined(question.exhibit) && question.exhibit.length > 0 && question.exhibit[0].exhibit_type != "") {
            question.showExhibit = true;
            if (question.exhibit[0].exhibit_type == 'image') {
                var imagePath = String(question.exhibit[0].path).replace("'\'", "");
                question.exhibit[0].path = imagePath;
            }
        }
        else {
            question.showExhibit = false;
        }
        $(question.choices).each(function (indexOpt, valueOpt) {
            var correctoptions = question.correct_answer.split(",");
            //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].CorrectOptions = correctoptions.toString();
            question.CorrectOptions = correctoptions.toString();
            var sSrchTxt = String(valueOpt.text);
            if (sSrchTxt.indexOf('<img') != -1) {
                var options = String(valueOpt.text).split('<img');
                question.choices[indexOpt].text = options[0];
                question.choices[indexOpt].MCSSImage = "<img" + options[1];
                question.choices[indexOpt].text = options[0];
                question.choices[indexOpt].MCSSImage = "<img" + options[1];
            }
            else {
                //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].choices[indexOpt].text = sSrchTxt;
                //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].choices[indexOpt].MCSSImage = null;
                question.choices[indexOpt].text = sSrchTxt;
                question.choices[indexOpt].MCSSImage = null;
            }

            if (this.isAnswered == true) {
                $(correctoptions).each(function (correctindex) {
                    if (parseInt(valueOpt.choiceid) != parseInt(correctoptions[correctindex])) {
                        valueOpt.isCorrect = false;
                        valueOpt.currentClass = "incorrect";
                    }
                });
            }

            $(correctoptions).each(function (correctindex) {
                if (parseInt(valueOpt.choiceid) == parseInt(correctoptions[correctindex])) {
                    valueOpt.isCorrect = true;
                    valueOpt.currentClass = "correct";
                }
            });
        });
    }

    $scope.SetFeedbackForCompositeOrder = function (question, selectedOptions) {
        $(question.choices).each(function (indexOpt, valueOpt) {
            var correctoptions = question.correct_answer.split(",");
            var optionindex = valueOpt.choiceid;
            if (parseInt(optionindex) == correctoptions[indexOpt]) {
                valueOpt.isCorrect = true;
            }
            else {
                valueOpt.isCorrect = false;
            }
            if (selectedOptions) {
                var userSelected = $.parseJSON(selectedOptions);
                //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].SelectedOptions = userSelected[0].selectedOrder;
                question.SelectedOptions = userSelected[0].selectedOrder;
            }
            //if (question.question_interaction.text == "media" && question.question_layout != null)
            //    $scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].LayoutType = question.question_layout.text;
        });
    }

    $scope.SetFeedbackForCompositeChoiceMatrix = function (question, selectedOptions) {
        $(question.choices).each(function (indexOpt, valueOpt) {
            if (selectedOptions) {
                var selectedOpt = angular.fromJson(selectedOptions);
                $(selectedOpt).each(function (selectindex, selectvalue) {
                    var inputValues = selectvalue.inputvalue.split("-");
                    var optionindex = indexOpt + 1;
                    $(valueOpt.suboption).each(function (index1, value1) {
                        if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                            valueOpt.suboption[index1].isAnswered = true;
                            var assess;
                            if (parseInt(inputValues[1]) == 1) {
                                assess = true;
                            }
                            else {
                                assess = false;
                            }
                            if (valueOpt.assess === assess) {
                                valueOpt.suboption[index1].isCorrect = true;
                            }
                            else {
                                valueOpt.suboption[index1].isCorrect = false;
                            }
                        }
                        else {
                            var val = (value1.value === "true");
                            if (valueOpt.assess === val) {
                                valueOpt.suboption[index1].isCorrect = true;
                            }
                        }
                    });
                });

            }

        });
    }

    $scope.SetFeedbackForCompositeFIBDNDMatching = function (question, selectedOptions) {
        //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].leftvertical = "";
        //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].rightvertical = "";
        //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].draggableItems = [];
        question.leftvertical = "";
        question.rightvertical = "";
        question.draggableItems = [];
        var questiontext = '';
        if (!$scope.isUndefined(question.textwithblanks)) {
            questiontext = question.textwithblanks.text;
        }
        $scope.FillFeedbackCompositeDroppableItems(question);
        angular.forEach(question.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            var htmlText = "";
            var answerClass = "";
            var inputValue = "";

            if (selectedOptions) {
                var selectedOption = angular.fromJson(selectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if (value.noforeachblank == option.noforeachblank && value.inputvalue != "") {
                        inputValue = value.inputvalue;
                        answerClass = (value.isCorrectAnswer) ? "correct" : "incorrect";
                    }
                });
            }
            if (option.interaction == 'textentry') {
                htmlText = "<span class='option " + answerClass + "'><input type='text' class='form-control' disabled value='" + inputValue + "'><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
            }
            else if (option.interaction == 'inline') {
                htmlText = "<span class='option " + answerClass + "'><select class='form-control' disabled><option>" + inputValue + "</option></select><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
            }
            else if (option.interaction == 'dragdrop' || option.interaction == 'container') {
                question.LayoutType = question.question_layout.text;
            }

            if (option.interaction == 'dragdrop') {
                angular.forEach(question.draggableItems, function (ditem, dindex) {
                    if ($filter('lowercase')(inputValue) == $filter('lowercase')(ditem.title)) {
                        ditem.title = "__________";
                    }
                });
            }
            else if (option.interaction == 'container') {
                var input = !!inputValue ? inputValue.map(function (x) { return x.title; }) : [];
                angular.forEach(question.draggableItems, function (ditem, dindex) {
                    if (input.indexOf(ditem.title) != -1) {
                        ditem.title = "__________";
                    }
                });
            }

            if (question.question_layout.text != '' && question.question_layout.text != "dragdrop_parallel") {
                htmlText = "<span class='option drop " + answerClass + "'><span class='option drag'>" + inputValue + "</span><span class='sign'><span class='fa'></span></span></span>";
                if (question.question_layout.text === "right" || question.question_layout.text === "left" || question.question_layout.text === "vertical") {
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].leftvertical = "col-md-10";
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].rightvertical = "col-md-2 right";
                    question.leftvertical = "col-md-10";
                    question.rightvertical = "col-md-2 right";
                }
                else if (question.question_layout.text === "row") {
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].leftvertical = "";
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].rightvertical = "text-center";
                    question.leftvertical = "";
                    question.rightvertical = "text-center";
                }

                question.choices[key].isCorrectAnswer = answerClass;
                question.choices[key].selectedAnswer = inputValue;
            }
            else {
                question.choices[key].isCorrectAnswer = answerClass;
                question.choices[key].selectedAnswer = inputValue;
            }
            questiontext = questiontext.replace(textToReplace, htmlText);

        });
        question.FIBText = questiontext;
    };

    $scope.FillFeedbackCompositeDroppableItems = function (question) {
        angular.forEach(question.choices, function (option, key) {
            angular.forEach(option.distractorforeachblank, function (titem, tindex) {
                question.draggableItems.push({ 'title': titem.text, 'drag': false });
            });

            angular.forEach(option.textforeachblank, function (titem, tindex) {
                question.draggableItems.push({ 'title': titem.text, 'drag': false });
            });
        });
    };

    //************************************************** End of Composite Question feedback***************************************

    //*********************************Save Answer for composite type **********************************

    $scope.GetUserSelectedOptionForCompositeType = function (questions)
    {
        var selectedItemsForComposite = [];
        $(questions).each(function (index, question) {
            selectedItemsForComposite.push({'Index':index,'UserSelectedOptions': $scope.FindUserSelectedOptions(question)});
        });
        return selectedItemsForComposite;
    }

    //*********************************End of save answer for composite type ***************************

    //*********************************Check Answer for Composite type question*************************

    $scope.CheckCompositeAnswers=function(questions)
    {
        $(questions).each(function (index, question) {
            if (question.question_type.text == Enum.QuestionType.MCSS || question.question_type.text == Enum.QuestionType.MCMS) {
                $scope.CheckMCSSAndMCMSAnswer(question);
            }
            else if (question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
                $scope.CheckDNDAnswers(question);
            }
            else if (question.question_type.text == Enum.QuestionType.Order) {
                $scope.CheckOrderTypeAnswer(question);
            }
            else if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
                $scope.CheckChoiceMatrixTypeAnswer(question);
            }
        });
    }

    $scope.CheckChoiceMatrixTypeAnswer = function (question) {
        var selectedItems = $scope.FindUserSelectedOptions(question);
        var selectedoptions = angular.fromJson(selectedItems);
        var currentAccess;
        if (parseInt(selectedoptions) == 1) {
            currentAccess = true;
        }
        else {
            currentAccess = false;
        }
        $(question.choices).each(function (index, value) {
            var optionindex = index + 1;
            $(value.suboption).each(function (sindex, soption) {
                if (value.suboptionSelected == soption.identifier) {
                    if (value.isAnswered.toString() == soption.isAnswered.toString()) {
                        question.choices[index].suboption[sindex].currentClass = "correct";
                    } else {
                        question.choices[index].suboption[sindex].currentClass = "incorrect";
                    }
                }

            });
        });
    }

    $scope.CheckOrderTypeAnswer = function (question)
    {
        if (question.userHasSelectedOptions) {
            $(question.choices).each(function (index, value) {
                var correctoptions = question.correct_answer.split(",");
                var optionindex = value.choiceid;
                if (parseInt(optionindex) == correctoptions[index]) {
                    value.isCorrect = true;
                    value.currentClass = "correct";
                }
                else {
                    value.isCorrect = false;
                    value.currentClass = "incorrect";
                }
            });
        }
    }

    $scope.CheckDNDAnswers = function (question)
    {
        var selectedItems = $scope.FindUserSelectedOptions(question);
        var selectedoptions = angular.fromJson(selectedItems);

        $(question.choices).each(function (index, value) {
            var inputValue = value.UserInput;
            var textFound = false;
            if (value.interaction == 'dragdrop' && value.Canvas.length > 0) {
                inputValue = value.Canvas[0].title;
            }

            if (inputValue.length > 0) {
                $(value.textforeachblank).each(function (textindex, tvalue) {
                    if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                        if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(inputValue)) {
                            textFound = true;
                        }
                    }
                    else {
                        if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(inputValue)) {
                            textFound = true;
                        }
                    }
                });
                if (textFound) {
                    value.AnswerClass = "correct";                    
                }
                else {
                    value.AnswerClass = "incorrect";
                }

            }
            if (value.interaction == 'container' && value.Canvas.length > 0) {
                var textArray = value.textforeachblank.map(function (x) {
                    return $('<span>' + x.text + '</span>').text();
                });

                for (var i = 0; i < value.Canvas.length; i++) {
                    if ($.inArray($(value.Canvas[i].title).text(), textArray) > -1) {
                        value.Canvas[i].AnswerClass = "correct";                       
                    }
                    else {
                        value.Canvas[i].AnswerClass = "incorrect";                                           
                    }
                }                
            }
        });        
    }

    $scope.CheckMCSSAndMCMSAnswer=function(question)
    {
        var selectedItems = $scope.FindUserSelectedOptions(question);
        var correctoptions = question.correct_answer.split(",");
        var selectedoptions = angular.fromJson(selectedItems).map(function (x) { return x.choiceid.toString(); });

        $(question.choices).each(function (index, value) {
            var optionindex = index + 1;
            var answerFound = false;

            if (this.isAnswered == true) {
                $(correctoptions).each(function (correctindex) {
                    if (parseInt(value.choiceid) == parseInt(correctoptions[correctindex])) {
                        answerFound = true;
                    }
                });
                if (answerFound) {
                    value.isCorrect = true;
                    value.currentClass = "correct";                    
                }
                else {
                    value.isCorrect = false;
                    value.currentClass = "incorrect";                   
                }
            }
            else {
                value.currentClass = "";
            }

        });
    }

    $scope.FindUserSelectedOptions=function(question)
    {
        var params = [];       
        if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
            $(question.choices).each(function (index, value) {                
                if (value.isChoiceAnswered) {
                    var name = value.choiceid;
                    params.push({ "inputvalue": value.choiceid + '-' + $('input[name=' + name + ']:checked').val(), "isCorrectAnswer": true, "questionTypeId": question.question_type.text });
                }
            });
            return JSON.stringify(params);
        }
        else if (question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
            $(question.choices).each(function (index, value) {
                var userInputValue = value.UserInput;
                var correctAnswer = false;
                if (value.interaction == 'dragdrop') {
                    userInputValue = (value.Canvas.length > 0) ? value.Canvas[0].title : "";
                }

                if (!!userInputValue) {
                    $(value.textforeachblank).each(function (textindex, tvalue) {
                        if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                            if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                        else {
                            if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                    });
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    var textArray = value.textforeachblank.map(function (x) { return $('<span>' + x.text + '</span>').text(); });

                    for (var i = 0; i < value.Canvas.length; i++) {
                        if ($.inArray($(value.Canvas[i].title).text(), textArray) > -1) {
                            value.Canvas[i].AnswerClass = "correct";
                        }
                        else {
                            value.Canvas[i].AnswerClass = "incorrect";
                        }
                    }
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": value.Canvas, "choiceid": value.choiceid, "questionTypeId": question.question_type.text });
                }
                else if (!!userInputValue) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": userInputValue, "isCorrectAnswer": correctAnswer, "choiceid": value.choiceid, "questionTypeId": question.question_type.text });
                }

            });
            return JSON.stringify(params);
        }
        else if (question.question_type.text == Enum.QuestionType.Essay) {
            var str = "";           
            if (question.toolbar.type == "math_3-5_palette" || question.toolbar.type == "math_6-8_palette") {
                // removing the unwanted duplicate cursor from matheditor
                if ($('#mathQuill .cursor').length > 1) {
                    $('#mathQuill .cursor:first').remove();
                }

                if ($('#mathQuill').mathquill('latex') == "{ }") {
                    $('#mathQuill').mathquill('8', 'keyStroke');
                }
                str = $('#mathQuill').mathquill('latex');
            } else {
                str = $scope.essayTemplateText;//$("#txtEssayContent").text();
            }
            //var html = $(str).text();
            params.push(str);
            return params;
        }
        else if (question.question_type.text == Enum.QuestionType.Order) {
            var selectedIds = '';
            $(question.choices).each(function (index) {
                if (selectedIds == '') {
                    selectedIds = question.choices[index].choiceid;
                }
                else {
                    selectedIds = selectedIds + "," + question.choices[index].choiceid;
                }
            });
            params.push({ "selectedOrder": selectedIds, "questionTypeId": question.question_type.text });
            return JSON.stringify(params);
        }
        else {
            $(question.choices).each(function (index) {
                if (this.isAnswered == true) {
                    params.push({ "choiceid": this.choiceid, "questionTypeId": question.question_type.text });
                }
            });
        }
        return JSON.stringify(params);
    }

    //*******************End of Composite question answer**************************

    $scope.checkCorrectTextEntered = function () {
        $scope.selectedoptions = angular.fromJson($scope.selectedItems);

        $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
            var inputValue = value.UserInput;
            var textFound = false;
            if (value.interaction == 'dragdrop' && value.Canvas.length > 0) {
                inputValue = value.Canvas[0].title;
            }

            if (inputValue.length > 0) {
                $(value.textforeachblank).each(function (textindex, tvalue) {
                    if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                        if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(inputValue)) {
                            textFound = true;
                        }
                    }
                    else {
                        if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(inputValue)) {
                            textFound = true;
                        }
                    }
                });
                if (textFound) {
                    $scope.QuestionOptions[index].AnswerClass = "correct";
                    // $scope.ShowGlobalFeebackData($scope.QuestionOptions[index].AnswerClass);
                }
                else {
                    $scope.QuestionOptions[index].AnswerClass = "incorrect";
                    $scope.ShowGlobalIncorrectFeedback = true;
                    // $scope.ShowGlobalFeebackData($scope.QuestionOptions[index].AnswerClass);
                }

            }
            if (value.interaction == 'container' && value.Canvas.length > 0) {
                var textArray = value.textforeachblank.map(function (x) {
                    return $('<span>' + x.text + '</span>').text();
                });

                for (var i = 0; i < value.Canvas.length; i++) {
                    if ($.inArray($(value.Canvas[i].title).text(), textArray) > -1) {
                        value.Canvas[i].AnswerClass = "correct";
                        // $scope.ShowGlobalFeebackData(value.Canvas[i].AnswerClass);
                    }
                    else {
                        value.Canvas[i].AnswerClass = "incorrect";
                        $scope.ShowGlobalIncorrectFeedback = true;
                        // $scope.ShowGlobalFeebackData(value.Canvas[i].AnswerClass);
                    }
                }

                if ($scope.selectedoptions.length > 0) {
                    $($scope.selectedoptions).each(function (sindex, svalue) {
                        if (svalue.choiceid == value.choiceid) {
                            if (svalue.inputvalue.length != value.textforeachblank.length) { $scope.ShowGlobalIncorrectFeedback = true; }
                        }
                    });
                }
            }
        });
        if (!$scope.ShowGlobalIncorrectFeedback && ($scope.selectedoptions.length == $scope.CurrentQuestion.QuestionData.choices.length)) {
            $scope.ShowGlobalFeebackData("correct");
        }
        else {
            $scope.ShowGlobalFeebackData("incorrect");
        }

    };

    function checkEqualArray(array1, array2) {
        if (array1.length != array2.length) {
            return false;
        }
        for (var i = 0; i < array1.length; i++) {
            if (array2.indexOf(array1[i]) == -1) {
                return false;
            }
        }
        return true;
    }

    $scope.getValue = function (text) {
        $scope.essayTemplateText = text;
    }
    //$scope.generateFeedback = function () {        
    //    $location.url(routeUrl + '/FeedBack');                
    //};
    //$scope.generateFeedback = function () {
    //    var modalInstance = $modal.open({
    //        templateUrl: routeUrl + '/Scripts/app/views/assessment/FeedBack.html',
    //        controller: 'FeedBackCtrl'

    //    });

    //    //$scope.cancel = function () {
    //    //    modalInstance.dismiss('cancel');
    //    //};

    //};
    $scope.userSelectedOptions = function () {
        var params = [];
        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            return $scope.GetUserSelectedOptionForCompositeType($scope.CurrentQuestion.QuestionData.questions);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                // $.each($scope.CurrentQuestion.QuestionData.choices[index].suboption, function (index1, value1) {
                if (value.isChoiceAnswered) {
                    var name = value.choiceid;
                    params.push({ "inputvalue": value.choiceid + '-' + $('input[name=' + name + ']:checked').val(), "isCorrectAnswer": true, "questionTypeId": $scope.questionTypeId });
                }
            });
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.Matching) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                var userInputValue = value.UserInput;
                var correctAnswer = false;
                if (value.interaction == 'dragdrop') {
                    userInputValue = (value.Canvas.length > 0) ? value.Canvas[0].title : "";
                }

                if (!!userInputValue) {
                    $(value.textforeachblank).each(function (textindex, tvalue) {
                        if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                            if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                        else {
                            if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                    });                   
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    var textArray = value.textforeachblank.map(function (x) { return '<span math dynamic>' + x.text +'</span>'; });

                    for (var i = 0; i < value.Canvas.length; i++) {                        
                        if (textArray.indexOf(value.Canvas[i].title) != -1) {
                            value.Canvas[i].AnswerClass = "correct";
                        }
                        else {
                            value.Canvas[i].AnswerClass = "incorrect";
                        }
                    }
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": value.Canvas, "choiceid": value.choiceid, "questionTypeId": $scope.questionTypeId });
                }
                else if (!!userInputValue) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": userInputValue, "isCorrectAnswer": correctAnswer, "choiceid": value.choiceid, "questionTypeId": $scope.questionTypeId });
                }

            });
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.Essay) {
            var str = "";
            //$scope.CurrentQuestion.QuestionData
            if ($scope.CurrentQuestion.QuestionData.toolbar.type == "math_3-5_palette" || $scope.CurrentQuestion.QuestionData.toolbar.type == "math_6-8_palette") {
                // removing the unwanted duplicate cursor from matheditor
                if ($('#mathQuill .cursor').length > 1) {
                    $('#mathQuill .cursor:first').remove();
                }

                if ($('#mathQuill').mathquill('latex') == "{ }") {
                    $('#mathQuill').mathquill('8', 'keyStroke');
                }
                str = $('#mathQuill').mathquill('latex');
            } else {
                str = $scope.essayTemplateText;//$("#txtEssayContent").text();
            }
            //var html = $(str).text();
            params.push(str);
            return params;
        }
        else if ($scope.questionTypeId == Enum.QuestionType.Order) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                params.push({ "choiceid": this.CurrentOptionIndex, "questionTypeId": $scope.questionTypeId });
            });
            return JSON.stringify(params);
        }
        else {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                if (this.isAnswered == true) {
                    params.push({ "choiceid": this.choiceid, "questionTypeId": $scope.questionTypeId });
                }
            });
        }
        return JSON.stringify(params);

    }




    $scope.setOptions = function (option, OptionId) {
        if ($scope.isPaused) return false;
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.ShowGlobalCorrectFeedback = false;
        $(".option").removeClass('correct');
        $(".option").removeClass('incorrect');
        if ($('#checkAnswerbtn').attr('disabled') != 'disabled') {
            $('#checkAnswerbtn').html('Check Answer');
        }
        switch ($scope.questionTypeId) {
            case Enum.QuestionType.MCSS:
            case Enum.QuestionType.GRAPHIC:
            case Enum.QuestionType.Exhibit:
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                    this.currentClass = "";
                    if (this.choiceid == option.choiceid) {
                        this.isAnswered = true;
                        option.currentClass = "selected";
                    }
                    else {
                        this.isAnswered = false;
                    }
                    this.isButtonClicked = true;
                    if (this.isAnswered) {
                        $scope.userHasSelectedOptions = true;
                        this.currentClass = "selected";
                    }
                });

                break;
            case Enum.QuestionType.MCMSVIDEO:
            case Enum.QuestionType.MCMS:
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                    if (this.choiceid == option.choiceid) {
                        if (this.isAnswered == true) {
                            this.isAnswered = false;
                            option.currentClass = "";
                        }
                        else {
                            this.isAnswered = true;
                            option.currentClass = "selected";
                        }
                    }
                    if (this.isAnswered == true) {
                        this.currentClass = "selected";
                        $scope.userHasSelectedOptions = true;
                    }
                });
                break;
            case Enum.QuestionType.ChoiceMatrix:
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                    $scope.selectedOption = OptionId;

                    $(value.suboption).each(function (sindex, soption) {
                        if (value.choiceid == OptionId) {
                            value.isChoiceAnswered = true;
                            if (option.identifier == soption.identifier) {
                                this.isOptionAnswered = true;
                                value.isAnswered = value.assess;
                                value.suboptionSelected = soption.identifier;
                                soption.isAnswered = soption.value;
                                soption.currentClass = "selected";
                            }
                            else {
                                this.isOptionAnswered = false;
                                soption.currentClass = "";
                                soption.isAnswered = !soption.value;
                            }
                        }
                        if (this.isOptionAnswered == true) {
                            this.currentClass = "selected";
                            $scope.userHasSelectedOptions = true;
                        }
                    });
                });
                break;
            case Enum.QuestionType.Order:
                $(option).each(function (index) {
                    this.isAnswered = true;
                    if (this.isAnswered) {
                        $scope.userHasSelectedOptions = true;
                    }
                });
                break;
            case Enum.QuestionType.FIB:
            case Enum.QuestionType.FIBDND:
            case Enum.QuestionType.Matching:
                var totalChoices = $scope.CurrentQuestion.QuestionData.choices.length;
                var objList = $filter('filter')($scope.CurrentQuestion.QuestionData.choices, { "Canvas": [] }, true);
                var objList2 = $filter('filter')($scope.CurrentQuestion.QuestionData.choices, { "UserInput": "" }, true);
                if (objList.length < totalChoices)
                    $scope.userHasSelectedOptions = true;
                if (objList2.length < totalChoices)
                    $scope.userHasSelectedOptions = true;
                break;
        }
    }

    $scope.shuffleArray = function (array) {
        var m = array.length, t, i;

        // While there remain elements to shuffle
        while (m) {
            // Pick a remaining element…
            i = Math.floor(Math.random() * m--);

            // And swap it with the current element.
            t = array[m];
            array[m] = array[i];
            array[i] = t;
        }

        return array;
    };

    $scope.showHideCheckAnswer = function () {
        //if ($scope.CurrentQuestion.QuestionData.global_correct_feedback.text != null && $scope.CurrentQuestion.QuestionData.global_correct_feedback.text != "") {
        //    $scope.showCheckAnswerbtn = true;
        //}
        //else {            
        //    $scope.showCheckAnswerbtn = false;
        //}
        //if ($scope.CurrentQuestion.QuestionData.question_type.text != 'essay') {
        //    $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
        //        if (value.feedback != null && value.feedback != "") {
        //            $scope.showCheckAnswerbtn = true;
        //        }
        //    });
        //}
        if ($scope.CurrentQuestion.QuestionData.question_type.text == 'essay') {
            $scope.showCheckAnswerbtn = false;
        }
    };

    // $scope.showOptionFeedback = function (optionId) {
    // if(optionId != undefined)
    // {
    // var popupValue = '';
    // $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
    // if (value.choiceid == optionId) {
    // if (value.feedback != null && value.feedback != undefined && value.feedback != "") {
    // popupValue = '<span>' + value.feedback + '</span>';
    // popupValue = $compile(popupValue)($scope);

    // $('.container-main').click(function(e){
    // if (e.target.className != null && e.target.className != undefined && e.target.className != "" && e.target.className != 'fa fa-commenting-o')
    // {
    // $('.popover').hide(); 
    // }
    // });
    // }
    // }
    // });

    // if(popupValue != "" && popupValue != undefined && popupValue != null)
    // {
    // popupValue = popupValue[0].innerHTML;
    // }
    // return popupValue;
    // }
    // };


    $scope.initializeQuiz = function (data) {
        $scope.showExhibit = false;
        $scope.userHasSelectedOptions = false;
        $scope.questionTypeId = data.QuestionTypeId;
        if (!$scope.isUndefined(data.QuestionData.question_type)) {
            if ((data.QuestionData.question_type.text.toLowerCase() == 'choicematrix' || data.QuestionData.question_type.text == 'mcss' || data.QuestionData.question_type.text == 'composite' || data.QuestionData.question_type.text == 'mcms' || data.QuestionData.question_type.text == 'order' || data.QuestionData.question_type.text == 'fib' || data.QuestionData.question_type.text == 'fib_dnd' || data.QuestionData.question_type.text == 'matching' || data.QuestionData.question_type.text == 'essay') && data.QuestionData.question_type.text.length > 0) {
                $scope.questionTypeId = data.QuestionData.question_type.text.toLowerCase();
            }
        }
        if ($scope.questionTypeId != 'composite') $scope.setQuestionOptions(data);
        $scope.CurrentQuestion = data;
        $scope.questionName = data.QuestionData.question_stem.text;
        if ($scope.questionTypeId == 'composite') {
            $scope.questionBody = data.QuestionData.question_body.text;
        }

        if (!$scope.isUndefined(data.QuestionData.stem_details) && data.QuestionData.stem_details.type == 'algorithimic')
        {
            $scope.SetAlgorithimicType(data);
        }
        $scope.instructionText = data.QuestionData.instruction_text.text;
        $scope.totalItems = $scope.questions.length;

        $scope.isEssay = false;
        $scope.isComposite = false;
        $scope.compositeQuestions = [];
        $scope.showHint = false;
        $scope.BookMarked = false;
        $scope.NotLearned = false;
        $scope.ReportNote = '';
        $scope.CorrectAnswerCount = 0;
        $scope.HintsUsed = 0;

        $scope.ShowGlobalFeedback = false;
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.ShowGlobalCorrectFeedback = false;
        $scope.showOptionFeedbackModal = false;

        $scope.globalCorrectFeedback = '';
        $scope.globalInCorrectFeedback = '';
        $scope.OptionFeedback = '';
        $scope.showCheckAnswerbtn = true;

        if (data.UserAssessmentActivityDetail) {
            $scope.UserAssessmentActivityDetail = data.UserAssessmentActivityDetail;
            $scope.BookMarked = $scope.UserAssessmentActivityDetail.IsBookMarked;
            $scope.NotLearned = $scope.UserAssessmentActivityDetail.NotLearned;
            $scope.ReportNote = $scope.UserAssessmentActivityDetail.ReportAProblem;
            $scope.CorrectAnswerCount = $scope.UserAssessmentActivityDetail.NoOfRetry;
            $scope.HintsUsed = $scope.UserAssessmentActivityDetail.HintsUsed;
        } else {
            $scope.UserAssessmentActivityDetail = {
                IsBookMarked: false,
                NotLearned: false,
                HintsUsed: 0,
                NoOfRetry: 0,
                TimeSpent: 0,
                ReportAProblem: '',
                QuizQuestID: '',
                UserAssessmentActivityType: 0
            };
        }
       
        if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
            $scope.ShowHintBox = $scope.AssignmentSetting.HintOn;
            $scope.BackwardMovement = $scope.AssignmentSetting.BackwardMovement;
            $scope.SkipQuestion = $scope.AssignmentSetting.SkipQuestion;
            $scope.ShuffleQuestions = $scope.AssignmentSetting.Shuffle;
            $scope.AssessmentAttempt = $scope.AssignmentSetting.Attempts;
            $scope.QuestionAttempt = $scope.AssignmentSetting.QuestionAttempts;
            $scope.ShowFeedBackAt = angular.fromJson($scope.AssignmentSetting.ShowFeedBackAt);
            $scope.PauseTimer = $scope.AssignmentSetting.PauseTimer;

            if ($scope.ShowFeedBackAt.Question == true) {
                $scope.ShowGlobalFeedback = true;
                //$scope.showCheckAnswerbtn = true;
                //$scope.showHideCheckAnswer();
            }
            if ($scope.ShowFeedBackAt.Option == true) {
                $scope.showOptionFeedbackModal = true;
                //$scope.showCheckAnswerbtn = true;
                //$scope.showHideCheckAnswer();
            }
            $scope.showHideCheckAnswer();
            if ($scope.ShuffleQuestions) {
                if (data.QuestionData.choices != null || data.QuestionData.choices != undefined) {
                    $scope.QuestionOptions = $scope.shuffleArray(data.QuestionData.choices);
                }
            }
            else {
                $scope.QuestionOptions = data.QuestionData.choices;
            }

            if ($scope.SkipQuestion == false) {
                $scope.disableNextBtn = true;
            }
            else {
                $scope.disableNextBtn = false;
            }

            $('#checkAnswerbtn').html('Check Answer');

            if ($scope.CorrectAnswerCount > 0) {
                $scope.showAttempt = true;
                if ($scope.QuestionAttempt == $scope.CorrectAnswerCount) {
                    $('#checkAnswerbtn').html('Try Again');
                    $scope.DisableCorrectAnswer = true;
                }
            }
        }
        else {
            $('#checkAnswerbtn').html('Check Answer');
            $scope.AssignmentSetting = {};
            $scope.ShowGlobalFeedback = true;
            $scope.showAttempt = true;
            $('.attempts').hide();
            $scope.showCheckAnswerbtn = true;
            $scope.ShowHintBox = true;
            $scope.showHintCount = true;
            //  $scope.ShowFeedBackAt.Question = true;
            $scope.QuestionOptions = data.QuestionData.choices;
        }
        
        //$scope.PassiveTimer.startTimer(data.TimeSpent);
        $scope.text = '';
        $scope.currentPage = data.CurrentQuestion;
        if ($scope.questionTypeId == Enum.QuestionType.MCMS || $scope.questionTypeId == Enum.QuestionType.MCSS) {
            $scope.questionSubType = data.QuestionData.question_interaction.text;
            $scope.questionLayout = data.QuestionData.question_layout.text;
            //$scope.exhibitType = data.QuestionData.exhibit[0].exhibit_type;
            if (!$scope.isUndefined(data.QuestionData.exhibit) && data.QuestionData.exhibit.length > 0 && data.QuestionData.exhibit[0].exhibit_type != "") {
                $scope.showExhibit = true;
                if (data.QuestionData.exhibit[0].exhibit_type == 'image') {
                    //var newString = String(data.QuestionData.exhibit[0].path).replace("___ASSETINFO", "").replace("___<br>", "").replace(/&#39;/g, '"').replace(/'/g, '"');
                    //var optionImage = JSON.parse(newString);
                    var imagePath = String(data.QuestionData.exhibit[0].path).replace("'\'", "");
                    // var newImage= imagePath[0];
                    //var newImagePath = newImage + '/>';

                    data.QuestionData.exhibit[0].path = imagePath;//"Content/images/" + optionImage.asset_name;
                }
            }
            else {
                $scope.showExhibit = false;
            }
        }
        else {
            $scope.questionSubType = data.QuestionData.settings.question_type;
        }

        if ($scope.questionTypeId == Enum.QuestionType.Order) {
            $scope.orderType = data.QuestionData.question_interaction.text;
            if ($scope.orderType == "media")
                $scope.layoutType = data.QuestionData.question_layout.text;
        }

        if ((!$scope.isUndefined(data.QuestionData.hints) && $scope.AssignmentSetting.HintOn) || (!$scope.isUndefined(data.QuestionData.hints) && $scope.showHintCount)) {

            var hintsUsed = $scope.HintsUsed;
            $scope.hintCount = 0;
            $scope.visibleHints = [];

            for (var i = 0; i < data.QuestionData.hints.length; i++) {
                var value = data.QuestionData.hints[i];
                if (value != "") {
                    if (value.text != null && value.text.trim() != '') {
                        if (hintsUsed != 0) {
                            $scope.showHint = true;
                            $scope.visibleHints.push('<span math dynamic>' + data.QuestionData.hints[i].text + '</span>');
                            hintsUsed = hintsUsed - 1;
                        }
                        else {
                            $scope.hintCount++;
                        }
                    }
                }
            }
        }
        else {
            $scope.hintCount = 0;
            $scope.showHint = false;
        }

        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            $scope.showCheckAnswerbtn = true;
            $scope.isComposite = true;
            var compositeQuestionData = [];
            $('#quizContainer').parent().removeClass('container');
            //$.each(data.QuestionData.questions, function (index, value) {
            //    //debugger;
            //    compositeQuestionData.push({ "QuestionData": value });
            //});
			
            $scope.Questions = null;
            $scope.compositeQuestions = data.QuestionData.questions;
            $scope.CompositeSelectedOption = data.SelectedOptions;
            if ($scope.compositeQuestions.length == 0)
            {
                $scope.showCheckAnswerbtn = false;
            }
			//$scope.$broadcast('nextCompositeQuestion', {myMsg: "hi children"});
			$scope.$broadcast('nextCompositeQuestion');
			
            //assessmentEngineSvc.SetCompositeQuestions(data.QuestionData.questions);
            //$timeout(function () {
            //    if (document.getElementById('compositeIframe') != null) document.getElementById('compositeIframe').src = document.getElementById('compositeIframe').src;
            //});
        }
        else {
            $('#quizContainer').parent().addClass('container');
        }

        if ($scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.Matching) {
            $scope.SetFIBType(data);
        }

        if ($scope.questionTypeId == Enum.QuestionType.Order) {
            $.each(data.QuestionData.choices, function (index, value) {
                data.QuestionData.choices[index].text = '<span math dynamic>' + value.text + '</span>';
            });
            //console.log(data.QuestionData.choices);
        }
        if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
            $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
                $.each($scope.CurrentQuestion.QuestionData.choices[index].suboption, function (index1, value1) {
                    $scope.CurrentQuestion.QuestionData.choices[index].suboption[index1] = value1;
                    //$scope.CurrentQuestion.QuestionData.choices[index].suboption[index1].setIndex = index1 + 1;
                });
            });
        }
        if ($scope.questionTypeId == Enum.QuestionType.Essay) {
            $scope.isEssay = true;
            if (data.QuestionData.max_characters != undefined) {
                $scope.maxCharaterLength = data.QuestionData.max_characters.text;
            }
            $scope.EssayTypeToolbar = data.QuestionData.toolbar.type;
            mathEditorType = data.QuestionData.toolbar.type;
            $('.math-toolbar').hide();
        }
        $scope.getProgressPercentage();
        $('#myTabs').on('click', '.nav-tabs a', function () {
            $(this).closest('.dropdown').addClass('dontClose');
        })

        $('#myDropDown').on('hide.bs.dropdown', function (e) {
            if ($(this).hasClass('dontClose')) {
                e.preventDefault();
            }
            $(this).removeClass('dontClose');
        });

        $('#myDropDown2 .toc-list').click(function () {
            $(this).parent().addClass('open');
        });

        $('#myTabs2 ul.nav li').click(
        function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('.active').removeClass('active').addClass('notactive');
            $(this).addClass('active').removeClass('notactive');
            var ref = $(this).find("a").attr("href");
            $(this).parents("#myTabs2").find(".tab-content .tab-pane").removeClass("active")
            $(this).parents("#myTabs2").find(".tab-content .tab-pane" + ref).addClass("active");
        });

        $('.actions-mobile .dropdown-menu').on('click', function (event) {
            event.stopPropagation();
        });
        //$('#quizContainer').mCustomScrollbar(
        //    {
        //        theme: "dark"
        //    });

        //Added for dynamically loading the Math Editor
        setTimeout(function () {
            $('#mathQuill').mathquill('editable');
            $('#mathQuill').width($('.math-editor').width());
            $('#mathQuill').height('auto');


            if ($.trim(mathEditorPrevLatex).length != 0) {
                mathEditorPrevLatex = mathEditorPrevLatex.replace("?", "");
                $('#mathQuill').mathquill('latex', mathEditorPrevLatex);

                SetEditorView(mathEditorType);
                $('#mathQuill textarea').focus();
            } else {
                $('#mathQuill').text('');
                $('#mathQuill').mathquill('editable');
                $('#mathQuill').width($('.math-editor').width());
                $('#mathQuill').height('auto');
                SetEditorView(mathEditorType);
                $('#mathQuill').mathquill('cmd', 'emptyspace');
                $('#mathQuill textarea').focus();
            }

        }, 1000);
    }

    $scope.SetFIBType = function (data) {
        var questiontext = "";//data.QuestionData.textwithblanks.text;
		if(data.QuestionData.textwithblanks != undefined)
		{
			if(data.QuestionData.textwithblanks.text != undefined && data.QuestionData.textwithblanks.text != "")
				questiontext = data.QuestionData.textwithblanks.text;
		}
        //Array object to hold draggable item values
        $scope.droppableItems = [];
        $scope.FIBText = "";
        $scope.layoutType = ($scope.isUndefined(data.QuestionData.question_layout)) ? "" : data.QuestionData.question_layout.text;
        $scope.FillDroppableItems(data);
        angular.forEach(data.QuestionData.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            $scope.QuestionOptions[key].key = key;       // Index of option
            $scope.QuestionOptions[key].UserInput = "";  //User input value of textbox and dropdown value.         
            $scope.QuestionOptions[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            $scope.QuestionOptions[key].width = 'auto';

            $scope.QuestionOptions[key].ShowGlobalFeedback = $scope.ShowGlobalFeedback;

            questiontext = questiontext.replace(textToReplace, "<fibvariation ng-model='$parent.QuestionOptions[" + key + "]' parent-ctrl='$parent' user-input='QuestionOptions[" + key + "].UserInput'></fibvariation>");

            if (data.SelectedOptions) {
                var selectedOption = angular.fromJson(data.SelectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if ((option.interaction == 'textentry' || option.interaction == 'inline') && value.noforeachblank == option.noforeachblank) {
                        $scope.QuestionOptions[key].UserInput = value.inputvalue;
                    }
                    else if (option.interaction == 'dragdrop' && value.noforeachblank == option.noforeachblank) {
                        angular.forEach($scope.droppableItems, function (ditem, dindex) {
                            if ($filter('lowercase')(ditem.title) == $filter('lowercase')(value.inputvalue)) {
                                //$scope.droppableItems[dindex].title = "";
                                $scope.QuestionOptions[key].Canvas = [{ 'title': value.inputvalue, 'drag': true, selectedClass: '' }];
                                $scope.droppableItems.splice(dindex, 1);
                                dindex = dindex - 1;
                            }
                        });
                    }
                    else if (option.interaction == 'container' && value.noforeachblank == option.noforeachblank) {
                        var input = value.inputvalue.map(function (x) { return x.title; });
                        for (var i = 0; i < $scope.droppableItems.length; i++) {
                            if (input.indexOf($scope.droppableItems[i].title) != -1) {
                                $scope.QuestionOptions[key].Canvas.push({ 'title': $scope.droppableItems[i].title, 'drag': true, selectedClass: '' });
                                $scope.droppableItems.splice(i, 1);
                                i = i - 1;
                            }
                        }
                    }
                });
            }
        });
        $scope.FIBText = questiontext;
        if (!$scope.isUndefined($scope.layoutType)) {
            if ($scope.layoutType == 'right' || $scope.layoutType == 'left' || $scope.layoutType == 'vertical') {
                $scope.droptype = "col-sm-9 col-md-9";
                $scope.dragtype = "dragpanel col-sm-3 col-md-3 right";
            }
            else if ($scope.layoutType == 'row') {
                $scope.droptype = "";
                $scope.dragtype = "dragpanel text-center";
            }
            else {
                $scope.dragtype = "dragpanel";
                $scope.droptype = "";
            }
        }
    };

    $scope.SetAlgorithimicType = function (data) {
        var textReplaced = false;
        if ($scope.isUndefined(data.AlgorithmicVariableArray)) {
            data.AlgorithmicVariableArray = [];
        }
        if (data.QuestionData.stem_details.textforeachvalue.length > 0) {
            angular.forEach(data.QuestionData.stem_details.textforeachvalue, function (textDetail, key) {
                if (textDetail.type == 'random') {
                    if (data.AlgorithmicVariableArray.length == angular.fromJson(data.QuestionData.stem_details.textforeachvalue).length) {
                        angular.forEach(data.AlgorithmicVariableArray, function (arrayVariable, akey) {
                            $scope.questionName = $scope.questionName.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        });
                    }
                    else {
                        var random = Math.floor((Math.random() * textDetail.maxvalue) + textDetail.minvalue);
                        $scope.questionName = $scope.questionName.replace(textDetail.text, random);
                        data.AlgorithmicVariableArray.push({ "TextToReplace": textDetail.text, "TextValue": random });
                    }
                }
            });

            angular.forEach(data.QuestionData.choices, function (option, key) {
                angular.forEach(data.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    if (isNaN(option.text) && option.text.indexOf(arrayVariable.TextToReplace) != -1) {
                        option.text = option.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        textReplaced = true;
                    }
                });
                if (data.QuestionData.stem_details.display == 'result' && textReplaced) {
                    option.text = eval(option.text);
                }
            });
        }
    };

    $scope.FillDroppableItems = function (data) {
		isDistractorsPushDone = false;
        angular.forEach(data.QuestionData.choices, function (option, key) {
            $scope.QuestionOptions[key].arrList = [];
            if (option.interaction == "inline") {
                angular.forEach(option.distractorforeachblank, function (titem, tindex) {
					if(titem.text)
						$scope.QuestionOptions[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                });
            }
			
			if (option.interaction == "dragdrop" || option.interaction == "container") {
				if(isDistractorsPushDone == false)
				{
					angular.forEach(data.QuestionData.distractors, function (titem, tindex) {
						if(titem.text)
							$scope.QuestionOptions[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
					});
				}
				isDistractorsPushDone = true;
            }
			
			angular.forEach(option.textforeachblank, function (titem, tindex) {
				if(titem.text)
					$scope.QuestionOptions[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
            });

			
            if (option.interaction == "dragdrop" || option.interaction == "container") {
                angular.forEach($scope.QuestionOptions[key].arrList, function (item, index) {
                    $scope.droppableItems.push({ 'title': item, 'drag': true, selectedClass: '' });
                });
                $scope.QuestionOptions[key].Canvas = [];
                $scope.QuestionOptions[key].Drop = true;
            }
        });
    };
	
	

    $scope.OnDragStart = function (option) {
        option.AnswerClass = "";
    };

    $scope.AddDropCssOnOver = function (key) {
        $('#divDropOption' + key).addClass('active');
        //$scope.QuestionOptions[key].AnswerClass = '';
        angular.forEach($scope.QuestionOptions[key].Canvas, function (option, key) {
            option.AnswerClass = '';
        });
        return true;
    };

    $scope.RemoveDropCssOnLeave = function () {
        //$('#divDOption' + key).removeClass('active');
        angular.forEach($scope.QuestionOptions, function (option, key) {
            $('#divDropOption' + key).removeClass('active');
        });
    };

    $scope.AddCssOnOver = function (item) {
        item.selectedClass = 'dragging';
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveSelected = function () {
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveCssOnOut = function () {
        angular.forEach($scope.droppableItems, function (item, key) {
            item.selectedClass = '';
        });
    };

    $scope.setQuestionOptions = function (data) {
        $scope.text = '';
        $scope.showOptionFeedbackModal = false;
        $('.fdback').addClass('ng-hide');
        $(data.QuestionData.choices).each(function (index) {
            data.QuestionData.choices[index].currentClass = '';

        });
        if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
            if (data.SelectedOptions) {
                var selectedOpt = angular.fromJson(data.SelectedOptions);
                $(selectedOpt).each(function (selectindex, selectvalue) {
                    var inputValues = selectvalue.inputvalue.split("-");
                    $(data.QuestionData.choices).each(function (index) {
                        var optionindex = index + 1;
                        $(data.QuestionData.choices[index].suboption).each(function (index1, value1) {
                            if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                                data.QuestionData.choices[index].suboption[index1].isAnswered = true;
                                data.QuestionData.choices[index].suboption[index1].currentClass = '';
                            }
                        });

                    });
                });

            }

        }
        else if ($scope.questionTypeId == Enum.QuestionType.Essay) {

            if (data.SelectedOptions) {
                //$scope.essayTemplateText = data.SelectedOptions;
                if (data.QuestionData.toolbar.type == "math_3-5_palette" || data.QuestionData.toolbar.type == "math_6-8_palette") {
                    if ($scope.operationmode == Enum.OperationMode.Offline) {
                        mathEditorPrevLatex = $.trim(data.SelectedOptions[0].replace("?", ""));
                        mathEditorType = data.QuestionData.toolbar.type;
                    }
                    else {
                        mathEditorPrevLatex = $.trim(data.SelectedOptions.replace("?", ""));
                        mathEditorType = data.QuestionData.toolbar.type;
                    }
                } else {
                    $("#txtEssayContent").val(data.SelectedOptions);
                    $scope.text = data.SelectedOptions;
                }
            }
        }
        else {
            $(data.QuestionData.choices).each(function (index) {
                //data.QuestionData.choices[index].isAnswered = false;
                //if (!$scope.isUndefined(data.QuestionData.image) && data.QuestionData.image.image.indexOf('http') === -1 && data.QuestionData.image.image != "") {
                //    data.QuestionData.choices[index].image = "Content/images/" + data.QuestionData.choices[index].image;
                //}
                var optionindex = index + 1;
                data.QuestionData.choices[index].CurrentOptionIndex = optionindex;
                if (data.SelectedOptions) {
                    var selectedoptions = $.parseJSON(data.SelectedOptions);
                    $(selectedoptions).each(function (selectindex) {
                        data.QuestionData.choices[index].SelectedOptions = parseInt(selectedoptions[selectindex].choiceid);
                        if (optionindex == parseInt(selectedoptions[selectindex].choiceid)) {
                            data.QuestionData.choices[index].isAnswered = true;
                        }
                        if (!$scope.isUndefined(data.QuestionData.question_interaction) &&  data.QuestionData.question_interaction.text == 'selectable') {
                            if (data.QuestionData.choices[index].choiceid == parseInt(selectedoptions[selectindex].choiceid))
                                data.QuestionData.choices[index].currentClass = "selected";
                        }
                    });
                }
            });
        }
        return data;
    }

    $scopeserializeData = function (data) {
        // If this is not an object, defer to native stringification.
        if (!angular.isObject(data)) {
            return ((data == null) ? "" : data.toString());
        }
        var buffer = [];

        // Serialize each key in the object.
        for (var name in data) {
            if (!data.hasOwnProperty(name)) {
                continue;
            }

            var value = data[name];
            buffer.push(
                encodeURIComponent(name) +
                "=" +
                encodeURIComponent((value == null) ? "" : value)
            );
        }

        // Serialize the buffer and clean it up for transportation.
        var source = buffer
            .join("&")
            .replace(/%20/g, "+")
        ;
        return (source);
    };

    $scope.getProgressPercentage = function () {
        if ($scope.currentPage > 0) {
            $scope.Progress = [];
            $scope.Progress.ProgressPercentage = (($scope.currentPage * 100) / $scope.totalItems).toFixed(2);
            $scope.Progress.CurrentQuestion = $scope.currentPage;
            if ($scope.operationmode == Enum.OperationMode.Offline) {
                $scope.Progress.ProgressPercentage = (($scope.currentPage * 100) / $scope.offline.quiz.length).toFixed(2);
                $scope.Progress.TotalQuestions = $scope.offline.quiz.length;
            }
            else {
                $scope.Progress.ProgressPercentage = (($scope.currentPage * 100) / $scope.totalItems).toFixed(2);
                $scope.Progress.TotalQuestions = $scope.totalItems;
            }
        }
    }
    $scope.getProgressTotal = function () {
        return $scope.CurrentQuestion.TotalQuestions;
    }

    $scope.PassiveTimer = (function () {
        var time = 0;
        var timerToken;
        var startTimer = function (startTime) {
            time = startTime;
            timerToken = setInterval(function () {
                time = time + 1;
            }, 1000);
        }
        var stopTimer = function () {
            clearInterval(timerToken);
            return time;
        }

        return {
            startTimer: startTimer,
            stopTimer: stopTimer
        };
    })();

    $scope.ActiveTimer = function (TimerSetting, CurrentQuestion) {
        $scope.totaltimemoving = TimerSetting.Minute * 60;
        $scope.totaltime = (TimerSetting.Minute * 60).toHHMMSS();

        $timeout(function () {
            $scope.$broadcast('add-timer');
        });
    }

    $scope.startTimer = function () {
        if (!timeStarted) {
            $scope.$broadcast('timer-start');
            $scope.timerRunning = true;
            timeStarted = true
        } else if ((timeStarted) && (!$scope.timerRunning)) {
            $scope.isPaused = false;

            $scope.$broadcast('timer-resume');
            $scope.timerRunning = true;
        }
    };

    $scope.stopTimer = function () {
        if ((timeStarted) && ($scope.timerRunning)) {
            $scope.isPaused = true;

            $scope.$broadcast('timer-stop');
            $scope.timerRunning = false;
        }
    };

    $scope.$on('timer-stopped', function (event, data) {
        timeStarted = true;
    });

    $scope.callbackTimer.finished = function () {
        SweetAlert.swal("Alert!", "Your time has expired. Your assignment has been submitted. Redirecting you to the feedback page in 5 seconds", "warning");
        $scope.autoSubmit();
    };

    Number.prototype.toHHMMSS = function () {
        var sec_num = parseInt(this, 10); // don't forget the second parm
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) { hours = "0" + hours; }
        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        var time = hours + ':' + minutes + ':' + seconds;
        return time;
    }

    $scope.TimerClass = (function () {
        Number.prototype.toHHMMSS = function () {
            var sec_num = parseInt(this, 10); // don't forget the second parm
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) { hours = "0" + hours; }
            if (minutes < 10) { minutes = "0" + minutes; }
            if (seconds < 10) { seconds = "0" + seconds; }
            var time = hours + ':' + minutes + ':' + seconds;
            return time;
        }
        var secondCounter = 0;
        var startTime = 0, totalSeconds = 0, threshold = 0;
        var timerToken;
        var initializeTimer = function (startTime, totSeconds, callback) {
            secondCounter = totSeconds - startTime;
            startTime = startTime;
            totalSeconds = totSeconds;
            setThreshold();
            var currentDisplayTime = (totalSeconds * 1000) - (startTime * 1000);
            timerToken = setInterval(function () {
                secondCounter = secondCounter - 1;
                if (secondCounter == threshold) {
                    //$("#timer").css('color', 'red');
                }
                $scope.totaltimemoving = secondCounter.toHHMMSS();

                $scope.$apply();
            }, 1000);
            setTimeout(function () { callback.call(totalSeconds); }, currentDisplayTime);
            $scope.totaltime = secondCounter.toHHMMSS();
        }
        var setThreshold = function () {
            threshold = totalSeconds - Math.round((totalSeconds * 80) / 100);
        }
        var stopTime = function (callback) {
            clearInterval(timerToken);
            var pendingTime = totalSeconds - secondCounter;
            callback.call(pendingTime);
        }
        return {
            startTimer: initializeTimer,
            stopTimer: stopTime
        };
    })();

    $scope.mathCommand = function (val, opr) {

        if ($.trim($('#mathQuill').html()).length == 0) {
            $('#mathQuill').mathquill('editable');
            $('#mathQuill').width($('.math-editor').width());
            $('#mathQuill').height('auto');
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').mathquill('showcursor');
        }

        $scope.mathEditorPrevLatex = $('#mathQuill').mathquill('latex');

        $('#mathQuill').mathquill(opr, val);
        $('#mathQuill').focus();

        if (val === '+' || val === '-' || val === '÷' || val === '=' || val === 'times' || val === '<' || val === '>') {
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').focus();
        }

        $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');

        //$('#codecogsimg').attr('src', 'http://latex.codecogs.com/gif.latex?' + $scope.mathEditorLatex);
    };


    $scope.mathKeyCommand = function (val, opr) {

        if ($.trim($('#mathQuill').html()).length == 0) {
            $('#mathQuill').mathquill('editable');
            $('#mathQuill').width($('.math-editor').width());
            $('#mathQuill').height('auto');
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').mathquill('showcursor');
        }

        $scope.mathEditorPrevLatex = $('#mathQuill').mathquill('latex');

        $('#mathQuill').mathquill(opr, val);


        if (val === '+' || val === '-' || val === '÷' || val === '=' || val === 'times' || val === '<' || val === '>') {
            $('#mathQuill').mathquill('cmd', 'emptyspace');
        }

        if (val === '\sqrt[3]') {
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').mathquill('cmd', 'emptyspace');
        }

        $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');
        $('#mathQuill textarea').focus();

    };



    $scope.updateDataMathEditor = function (e) {

        //if (event.which == 42 || event.which == 43 || event.which == 45 || event.which == 61)
        //{
        //    $('#mathQuill').mathquill('cmd', 'emptyspace');
        //    $('#mathQuill').focus();
        //}

        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(37); //left
        specialKeys.push(38); //up
        specialKeys.push(39); //right
        specialKeys.push(40); //down

        // Added for the operators
        specialKeys.push(42); //*
        specialKeys.push(43); //+
        specialKeys.push(47); //÷
        specialKeys.push(45); //-
        specialKeys.push(61); //=

        var keyCode = e.which ? e.which : e.keyCode
        var result = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 97 && keyCode <= 122) || (keyCode >= 64 && keyCode <= 91) || specialKeys.indexOf(keyCode) != -1);
        if (!result) {
            e.preventDefault();
            return false;
        } else {

            if (specialKeys.indexOf(keyCode) != -1) {
                if (keyCode == 42) {
                    $scope.mathCommand('times', 'cmd');
                } else if (keyCode == 43) {
                    $scope.mathCommand('+', 'write');
                } else if (keyCode == 47) {
                    $scope.mathCommand('÷', 'write');
                } else if (keyCode == 45) {
                    $scope.mathCommand('-', 'write');
                } else if (keyCode == 61) {
                    $scope.mathCommand('=', 'write');
                }
                e.preventDefault();
                return false;
            }

            $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');

        }


    };

    $scope.mathRedoCommand = function () {
        $('#mathQuill').mathquill('editable');
        $('#mathQuill').width($('.math-editor').width());
        $('#mathQuill').height('auto');
        $('#mathQuill').mathquill('cmd', 'emptyspace');
        $('#mathQuill').mathquill('latex', $scope.mathEditorLatex);
        $('#mathQuill textarea').focus();
    };

    $scope.mathDeleteCommand = function () {
        $('#mathQuill').mathquill('editable');
        $('#mathQuill').width($('.math-editor').width());
        $('#mathQuill').height('auto');
        $('#mathQuill').mathquill('cmd', 'emptyspace');
        $('#mathQuill').mathquill('latex', "");
        $('#mathQuill textarea').focus();
    };

    $scope.mathUndoCommand = function () {
        $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');
        $('#mathQuill').mathquill('editable');
        $('#mathQuill').width($('.math-editor').width());
        $('#mathQuill').height('auto');
        $('#mathQuill').mathquill('cmd', 'emptyspace');
        $('#mathQuill').mathquill('latex', $scope.mathEditorPrevLatex);
        $('#mathQuill textarea').focus();
    };

    $scope.mathEditorRefactor = function () {
        // removing the unwanted duplicate cursor from matheditor
        if ($('#mathQuill .cursor').length > 1) {
            $('#mathQuill .cursor:first').remove();
        }

        if ($('#mathQuill').mathquill('latex') == "{ }") {
            $('#mathQuill').mathquill('8', 'keyStroke');
        }
    };

    $scope.getQuizDetails();
    $scope.OpenQuestionMap = function () {
        if ($('.main-body-block').hasClass('visible')) {
            closeNav();
        } else {
            $(this).addClass('visible');
            $('.main-nav').addClass('visible');
            $('.main-body-block').addClass('visible');
            $('body').addClass('overflow-hidden');

        }
        if ($('#myDropDown').hasClass('open')) {
            if (!$('#liQuestionMap').hasClass('active')) {
                $('#liQuestionMap').addClass('active');
                $('#liBookMark').removeClass('active');
                $('#Map').addClass('active');
                $('#Bookmarks').removeClass('active');
                $scope.QuestionMapBookMarkClicked = false;
                $scope.searchFilter = { QuestionStatus: '' };
            }
        }
    }
   
    function closeNav() {
        $('.toc-list-dropdown, .navoverlay').removeClass('visible');
        $('.main-nav').removeClass('visible');
        $('.main-body-block').removeClass('visible');
        $('body').removeClass('overflow-hidden');

    }

    //window.loadDone = function () {
    //    console.log('loadDone');
    //    //document.getElementById('compositeIframe').contentWindow.angular.element('#quizContainer').hide();
    //    var data = { "quiz": $scope.compositeQuestions, "settings": $scope.AssignmentSetting };
    //    $timeout(function () {
    //        loadiFrameWithInterval(data);
    //    });
    //}

    //window.loadiFrameWithInterval = function (data) {
    //    console.log('loadiFrameWithInterval');
    //    document.getElementById('compositeIframe').contentWindow.angular.element('.container-main').scope().startQuiz(data, true);
    //    document.getElementById('compositeIframe').contentWindow.angular.element('.container-main').scope().$apply();
    //    //document.getElementById('compositeIframe').contentWindow.angular.element('#quizContainer').show();
    //}
}])
    .directive('bindTimer', function ($compile) {
        return {
            restrict: 'A',
            link: function ($scope, el) {
                $scope.$on('add-timer', function (e) {
                    var timerHtml = '<timer autostart="false" interval="1000" countdown="' + $scope.totaltimemoving + '" finish-callback="callbackTimer.finished()"> {{hours}}:{{minutes}}:{{seconds}}</timer>';
                    var compiledHtml = $compile(timerHtml)($scope);
                    el.replaceWith(compiledHtml);
                    $scope.startTimer();
                })
            }
        }
    });

function resize_fun() {
    var window_height = $(window).innerHeight();
    var header_height = $(".main-header-block").innerHeight();
    var footer_height = $(".main-footer-block").innerHeight();
    var r_header_height = $(".main-nav-container .nav-tabs").innerHeight();
    var r_footer_height = $(".foter-navs").innerHeight();
    var body_height = window_height - (header_height + footer_height);
    var right_tab_height = body_height - (r_header_height + r_footer_height);
    $(".main-body-block").height(body_height - 1);
    $(".main-nav, .main-body-block>.container").height(body_height - 1);
    $(".main-nav-container .tab-content").height(right_tab_height - 2);
}

//$(document).ready(function () {
//    resize_fun();
//});
//$(window).resize(function () {
//    resize_fun();
//})
//window.onorientationchange = function () {
//    resize_fun();
//}




