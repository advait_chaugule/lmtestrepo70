﻿var serializeData = function (data) {
    // If this is not an object, defer to native stringification.
    if (!angular.isObject(data)) {
        return ((data == null) ? "" : data.toString());
    }
    var buffer = [];

    // Serialize each key in the object.
    for (var name in data) {
        if (!data.hasOwnProperty(name)) {
            continue;
        }

        var value = data[name];
        buffer.push(
            encodeURIComponent(name) +
            "=" +
            encodeURIComponent((value == null) ? "" : value)
        );
    }

    // Serialize the buffer and clean it up for transportation.
    var source = buffer
        .join("&")
        .replace(/%20/g, "+")
    ;
    return (source);
}


var format = function (text) {
    var arguments = ['v1'];
    if (!text) return text;
    for (var i = 1; i <= arguments.length; i++) {
        var pattern = new RegExp("\\{" + (i - 1) + "\\}", "g");
        text = text.replace(pattern, arguments[i - 1]);
    }
    return text;
}

function IsNotEmptyValue(value) {
    return !IsEmptyValue(value);
}
function IsEmptyValue(value) {
    return (value === undefined || value === null || value === "");
}
function validateDate(e) {
    var keyCode = e.which; // Capture the event      
    var invalidKey = true;
    //8 (backspace), 9 (tab key), 16 (shiftkey), 35 (end button), 36 (home button), 37 (left arrow key), 39 (right arrow key), 40 (down arrow key), 46 (del button), 191 (back slash), 111 (numeric back slash)
    if (keyCode === 8
        || keyCode === 9
        || keyCode === 16
        || keyCode === 35
        || keyCode === 36
        || keyCode === 37
        || keyCode === 39
        || keyCode === 40
        || keyCode === 46
        || keyCode === 191
        || keyCode === 111)
        invalidKey = false;
    else if (47 < keyCode && keyCode < 58)
        invalidKey = false;
    else if (95 < keyCode && keyCode < 106)
        invalidKey = false;

    if (invalidKey) {
        e.preventDefault();
    }
};
function validateDatePattern(e) {
    if (e != "") {
        var re = /^((\d{2})\/(\d{2})\/(\d{4}))$/;
        return re.test(e);
    }
    else {
        return true;
    }
};

function GetTodaysDate() {
    var dt = new Date();
    return GetDateWithTimeZero(dt);
}
function GetDateWithTimeZero(dateString) {
    if (IsNotEmptyValue(dateString)) {
        if (Date.parse(dateString) > 0) {
            var d = new Date(dateString);
            d.setHours(0, 0, 0, 0);
            return d;
        }
        else {
            if (dateString.split !== undefined) {
                var dateObjects = dateString.split('/');
                if (dateObjects.length === 3) {
                    var year = parseInt(dateObjects[2]);
                    var month = parseInt(dateObjects[0]) - 1;
                    var date = parseInt(dateObjects[1]);
                    var dt = new Date(year, month, date);
                    dt.setHours(0, 0, 0, 0);
                    var dateValue = new Date(dt);
                    return dateValue;
                }
            }
        }
    }
    return null;
};


function SetEditorView(type) {

    if (type === "math_3-5_palette") {
        
        $('.btn-minussign').show();
        $('.btn-plussign').show();
        $('.btn-timessign').show();
        $('.btn-divisionsign').show();
        $('.btn-uniE005').show();
        $('.btn-mixednumber').show();
        $('.btn-equal').show();
        $('.btn-lessthan').show();
        $('.btn-greaterthan').show();
        $('.btn-parenthesis').show();
        $('.btn-bracket').show();
        $('.btn-uniE002').hide();
        $('.btn-uniE003').hide();
        $('.btn-bracket').hide();
        $('.btn-plusminus').hide();
        $('.btn-periodcentered').hide();
        $('.btn-greaterequal').hide();
        $('.btn-lessequal').hide();
        $('.btn-Pi').hide();
        $('.btn-uni2218').hide();

        $('.math-toolbar').show();

    } else if (type === "math_6-8_palette") {
        $('.btn-plussign').show();
        $('.btn-minussign').show();
        $('.btn-timessign').show();
        $('.btn-divisionsign').show();
        $('.btn-uniE005').show();
        $('.btn-mixednumber').show();
        $('.btn-equal').show();
        $('.btn-lessthan').show();
        $('.btn-greaterthan').show();
        $('.btn-parenthesis').show();
        $('.btn-bracket').show();
        $('.btn-uniE002').show();
        $('.btn-uniE003').show();
        $('.btn-bracket').show();
        $('.btn-plusminus').show();
        $('.btn-periodcentered').show();
        $('.btn-greaterequal').show();
        $('.btn-lessequal').show();
        $('.btn-Pi').show();
        $('.btn-uni2218').show();
        $('.math-toolbar').show();
    }
};

var currentEditAssignmentId = 0;
var CurrentClassId = 0;
var CurrentClassName = "";
var IsLTILaunchMode = 0;
var mathEditorType = "";