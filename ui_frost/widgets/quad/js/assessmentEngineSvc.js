﻿app.factory('assessmentEngineSvc', ['$http', '$resource', 'servicehelper',
      function ($http, $resource, servicehelper) {
          var assessment = servicehelper.Assessment;
          var StartQuiz = function (params) {
              //var data = serializeData(loginData);
              return assessment.StartQuiz(params);
          };
          var FetchQuestion = function (params) {
              //var data = serializeData(loginData);
              return assessment.FetchQuestion(params);
          };
          var GenerateFeedback = function (params) {
              //var data = serializeData(loginData);
              return assessment.GenerateFeedback(params);
          };
          var AutoSubmit = function (params) {
              //var data = serializeData(loginData);
              return assessment.AutoSubmit(params);
          };
          var FetchQuizDetails = function (params) {
              //var data = serializeData(loginData);
              return assessment.FetchQuizDetails(params);
          };
          return {
              StartQuiz: StartQuiz,
              FetchQuestion: FetchQuestion,
              GenerateFeedback: GenerateFeedback,
              AutoSubmit: AutoSubmit,
              FetchQuizDetails: FetchQuizDetails
          };
      }]);