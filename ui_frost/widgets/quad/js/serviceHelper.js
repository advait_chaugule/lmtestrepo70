﻿app.factory('servicehelper', ['$http', '$resource', function ($http, $resource) {
    var baseUrl = Urls.restUrl;
    var routeUrl = Urls.routeUrl;
    var buildUrl = function (resourceUrl) {
        return baseUrl + format(resourceUrl);
    };


    var format = function (text) {
        var arguments = ['v1'];
        if (!text) return text;
        for (var i = 1; i <= arguments.length; i++) {
            var pattern = new RegExp("\\{" + (i - 1) + "\\}", "g");
            text = text.replace(pattern, arguments[i - 1]);
        }
        return text;
    };

    return {
        AuthorizationToken: $resource(Urls.restAuthUrl, null,
        {
            login: {
                method: 'post',
                url: Urls.restAuthUrl,
                isArray: false
            }
        }),
        Product: $resource(buildUrl('/{0}/{0}.Product/GetProductList'), null,
            {
                GetProductList: {
                    method: 'post',
                    url: buildUrl('/{0}/{0}.Product/GetProductList'),
                    isArray: true
                }
            }),
        Assessment: $resource(buildUrl('/{0}/{0}.Assessment/'), null,
            {
                StartQuiz: {
                    method: 'post',
                    url: buildUrl('/{0}/{0}.Assessment/StartQuiz')

                },
                FetchQuestion: {
                    method: 'post',
                    url: buildUrl('/{0{0}/}.Assessment/FetchQuestion')

                },
                GenerateFeedback: {
                    method: 'post',
                    url: buildUrl('/{0}/{0}.Assessment/GenerateFeedback')
                },
                AutoSubmit: {
                    method: 'post',
                    url: buildUrl('/{0}/{0}.Assessment/AutoSubmit')
                },
                FetchQuizDetails: {
                    method: 'post',
                    url: buildUrl('/{0}/{0}.Assessment/FetchQuizDetails')
                }

            }),
        Account: $resource(buildUrl('/{0}/{0}.Account'), null, {
            CreateAccount: {
                method: 'post',
                url: buildUrl('/{0}/{0}.Account/CreateAccount')
            }
        })

    };
}]);