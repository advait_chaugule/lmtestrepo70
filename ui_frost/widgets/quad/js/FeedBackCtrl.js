﻿app.controller('FeedBackCtrl', ['$scope', '$location', '$routeParams', '$window', '$http', 'assessmentEngineSvc', 'Enum',
function ($scope, $location, $routeParams, $window, $http, assessmentEngineSvc, Enum) {
    $scope.offline = {};
    $scope.operationmode = 0;
    $scope.$on('$viewContentLoaded', function () {
        $scope.quizID = 56525;//assessmentEngineSvc.GetQuizID();
        $scope.generateFeedback();
    });

    $scope.generateFeedback = function () {
        var params = { quizID: 56525 };
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            $scope.loadjsonfile();
            $scope.FeedBack = $scope.offline.quiz;
        }
        else {
            assessmentEngineSvc.GenerateFeedback($scopeserializeData(params)).$promise
              .then(function (response) {                  
                  $scope.FeedBack = response.AssesmentQuestions;                 
              }, function (err) {
                  //console.log("Err" + err);
              });
        }

    };

    $scope.loadjsonfile = function () {
        var file = "js/offlinejson.js";
        $http.get(file)
         .then(function (res) {
             var data = jQuery.grep(res.data.quiz, function (n, i) {
                 return (parseInt(n.ItemID) == parseInt($scope.quizID));
             });;
             $scope.offline.quizInfo = data[0].quizName;
             $scope.offline.quiz = data;
             $scope.quizName = data[0].quizName;
             $scope.startQuiz();
         });
    }

}]);