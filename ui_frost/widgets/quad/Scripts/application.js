;var serializeData = function (data) {
    // If this is not an object, defer to native stringification.
    if (!angular.isObject(data)) {
        return ((data == null) ? "" : data.toString());
    }
    var buffer = [];

    // Serialize each key in the object.
    for (var name in data) {
        if (!data.hasOwnProperty(name)) {
            continue;
        }

        var value = data[name];
        buffer.push(
            encodeURIComponent(name) +
            "=" +
            encodeURIComponent((value == null) ? "" : value)
        );
    }

    // Serialize the buffer and clean it up for transportation.
    var source = buffer
        .join("&")
        .replace(/%20/g, "+")
    ;
    return (source);
}


var format = function (text) {
    var arguments = ['v1'];
    if (!text) return text;
    for (var i = 1; i <= arguments.length; i++) {
        var pattern = new RegExp("\\{" + (i - 1) + "\\}", "g");
        text = text.replace(pattern, arguments[i - 1]);
    }
    return text;
}

function IsNotEmptyValue(value) {
    return !IsEmptyValue(value);
}
function IsEmptyValue(value) {
    return (value === undefined || value === null || value === "");
}
function validateDate(e) {
    var keyCode = e.which; // Capture the event      
    var invalidKey = true;
    //8 (backspace), 9 (tab key), 16 (shiftkey), 35 (end button), 36 (home button), 37 (left arrow key), 39 (right arrow key), 40 (down arrow key), 46 (del button), 191 (back slash), 111 (numeric back slash)
    if (keyCode === 8
        || keyCode === 9
        || keyCode === 16
        || keyCode === 35
        || keyCode === 36
        || keyCode === 37
        || keyCode === 39
        || keyCode === 40
        || keyCode === 46
        || keyCode === 191
        || keyCode === 111)
        invalidKey = false;
    else if (47 < keyCode && keyCode < 58)
        invalidKey = false;
    else if (95 < keyCode && keyCode < 106)
        invalidKey = false;

    if (invalidKey) {
        e.preventDefault();
    }
};
function validateDatePattern(e) {
    if (e != "") {
        var re = /^((\d{2})\/(\d{2})\/(\d{4}))$/;
        return re.test(e);
    }
    else {
        return true;
    }
};

function GetTodaysDate() {
    var dt = new Date();
    return GetDateWithTimeZero(dt);
}
function GetDateWithTimeZero(dateString) {
    if (IsNotEmptyValue(dateString)) {
        if (Date.parse(dateString) > 0) {
            var d = new Date(dateString);
            d.setHours(0, 0, 0, 0);
            return d;
        }
        else {
            if (dateString.split !== undefined) {
                var dateObjects = dateString.split('/');
                if (dateObjects.length === 3) {
                    var year = parseInt(dateObjects[2]);
                    var month = parseInt(dateObjects[0]) - 1;
                    var date = parseInt(dateObjects[1]);
                    var dt = new Date(year, month, date);
                    dt.setHours(0, 0, 0, 0);
                    var dateValue = new Date(dt);
                    return dateValue;
                }
            }
        }
    }
    return null;
};


function SetEditorView(type) {

    if (type === "math_3-5_palette") {
        
        $('.btn-minussign').show();
        $('.btn-plussign').show();
        $('.btn-timessign').show();
        $('.btn-divisionsign').show();
        $('.btn-uniE005').show();
        $('.btn-mixednumber').show();
        $('.btn-equal').show();
        $('.btn-lessthan').show();
        $('.btn-greaterthan').show();
        $('.btn-parenthesis').show();
        $('.btn-bracket').show();
        $('.btn-uniE002').hide();
        $('.btn-uniE003').hide();
        $('.btn-bracket').hide();
        $('.btn-plusminus').hide();
        $('.btn-periodcentered').hide();
        $('.btn-greaterequal').hide();
        $('.btn-lessequal').hide();
        $('.btn-Pi').hide();
        $('.btn-uni2218').hide();

        $('.math-toolbar').show();

    } else if (type === "math_6-8_palette") {
        $('.btn-plussign').show();
        $('.btn-minussign').show();
        $('.btn-timessign').show();
        $('.btn-divisionsign').show();
        $('.btn-uniE005').show();
        $('.btn-mixednumber').show();
        $('.btn-equal').show();
        $('.btn-lessthan').show();
        $('.btn-greaterthan').show();
        $('.btn-parenthesis').show();
        $('.btn-bracket').show();
        $('.btn-uniE002').show();
        $('.btn-uniE003').show();
        $('.btn-bracket').show();
        $('.btn-plusminus').show();
        $('.btn-periodcentered').show();
        $('.btn-greaterequal').show();
        $('.btn-lessequal').show();
        $('.btn-Pi').show();
        $('.btn-uni2218').show();
        $('.math-toolbar').show();
    }
};

var currentEditAssignmentId = 0;
var CurrentClassId = 0;
var CurrentClassName = "";
var IsLTILaunchMode = 0;
var mathEditorType = "";
;window.app = angular.module('ACEApp', ['ngResource', 'ng-sortable', 'ngTouch', 'ui.bootstrap', 'ui.sortable', 'textAngular', 'ngCookies', 'dndLists', 'timer', 'oitozero.ngSweetAlert', 'am.multiselect'])
.constant('ngSortableConfig', {
    onEnd: function () {
        console.log('default onEnd()');
    }
});

var Urls = {
    restAuthUrl: '/ACEAuth/oauth2/token',
    restUrl: '/ACEApi/Api',
    routeUrl: '/NewACE'
};
app.controller('AssessmentEngineCtrl', ['$scope', '$location', '$window', '$http', 'assessmentEngineSvc', 'Enum', '$modal', '$sce', '$filter', '$compile', '$timeout', '$modal', 'SweetAlert',
function ($scope, $location, $window, $http, assessmentEngineSvc, Enum, $modal, $sce, $filter, $compile, $timeout, $modal, SweetAlert) {

    $scope.droppableConfig = {
        group: 'grswip',
        animation: 150,
        onAdd: function (evt) {
            //alert(evt.item.parent.child.count);
            //$scope.maxOneConfig.disabled = true;
        }
        , onSort: function (evt) {
            if (evt.action === 'add') {
            }
        }
        , onMove: function (evt) {
            //if (evt.action === 'add') {
            //}
            if (evt.to.childElementCount > 0) {
                return false;
            }
        }
    };

    $scope.SortableFuncConfig = {
        group: 'grswip',
        animation: 150,
        onSort: function (evt) {
            $scope.userHasSelectedOptions = true;
        },
        onMove: function (evt) {
            if ($scope.questionTypeId == Enum.QuestionType.LTDDND && evt.to.childElementCount == 1) {
                return false;
            }
            if ($scope.questionTypeId == Enum.QuestionType.Matching
    && ($scope.layoutType == 'dragdrop_parallel' || $scope.layoutType == 'horizontal' || $scope.layoutType == 'vertical' || $scope.layoutType == 'image')
    && evt.to.childElementCount == 1) {
                return false;
            }

        }
    };

    $scope.draggableConfig = {
        group: 'grswip',
        animation: 150,
        onAdd: function (evt) {
            //alert(evt.item.parent.child.count);
            //$scope.maxOneConfig.disabled = true;
        }
        , onSort: function (evt) {
            if (evt.action === 'add') {
            }
        }
        , onStart: function (/**Event*/evt) {
            //evt.oldIndex;  // element index within parent
            return;
        }
        , onMove: function (evt) {
            //evt.to.childElementCount
            //}
            if (evt.to.childElementCount > 0) {
                return false;
            }
            //evt.preventDefault();
        }
    };

    $scope.SingledroppableConfig = {
        group: 'grswip',
        animation: 150,
        onMove: function (evt) {
            //if (evt.to.childElementCount == 1) {
            //    return false;
            //}
            //alert(111);
            if ($scope.questionTypeId == Enum.QuestionType.Matching
                && ($scope.layoutType == 'dragdrop_parallel' || $scope.layoutType == 'horizontal' || $scope.layoutType == 'vertical' || $scope.layoutType == 'image')
                && evt.to.childElementCount == 1) {
                return false;
            }
        }
    };

    $scope.SingledraggableConfig = {
        group: 'grswip',
        animation: 150,
        onMove: function (evt) {
            if (evt.to.childElementCount > 0 && evt.to.childElementCount < 2) {
                return false;
            }
        }
    };

    $scope.$on('textEntered', function (event, args) {
        $scope.userHasSelectedOptions = true;
    });
    $scope.ChangeSelectedValue = function (value) {
        var selectedValues = '';
        if (value.length > 0) {
            //option.Canvas = [];
            angular.forEach(value, function (item, key) {
                selectedValues = (selectedValues == '') ? item : selectedValues + "," + item;
                //if (option.Canvas.indexOf("<span math dynamic>" + item + "</span>") == -1) {
                //    option.Canvas.push("<span math dynamic>" + item + "</span>");
                //}
            });
        }
        return selectedValues;
    };
    $scope.SetCanvasValue = function (option) {
        if (option.multiSelectItem.length > 0) {
            option.Canvas = [];
            //var selectedDrpItems = $scope.SelectedValues.split(',');
            angular.forEach(option.multiSelectItem, function (item, key) {
                option.Canvas.push({ "AnswerClass": "", "drag": "true", "selectedClass": "", "title": "<span math dynamic>" + item + "</span>" });
            });
        }
    };
    $scope.totalAtemptedQuestions = 1;
    $scope.UserSelectedArea = [];
    $scope.showModal = false;
    if ($location.absUrl().toLowerCase().indexOf("index.html") > -1) { $scope.showModal = true; }
    var routeUrl = Urls.routeUrl;
    var AssignmentID = 54;
    $scope.currentYear = new Date();
    var questionArray = [];
    $scope.mode = 'quiz';
    $scope.operationmode = 1;//0-online,1-offline
    $scope.offline = {};
    $scope.Enum = Enum;
    $scope.showFooter = false;
    $scope.ShowQueFeedback = false;
    $scope.showFeedback = false;
    $scope.showHint = false;
    $scope.visibleHints = [];
    $scope.isLastquestion = true;
    $scope.isBackButtonOn = false;
    $scope.hintCount = 0;
    $scope.isUndefined = function (thing) {
        return (typeof thing === "undefined");
    };
    $scope.AssignmentQuestions = [];
    $scope.questions = [];
    $scope.questiontosave = {};
    $scope.showExhibit = false;
    $scope.isComposite = false;
    $scope.isEssay = false;
    $scope.ShowHintBox = false;
    $scope.BackwardMovement = true;
    $scope.SkipQuestion = true;
    $scope.disableBackBtn = false;
    $scope.disableNextBtn = false;
    $scope.userHasSelectedOptions = false;
    $scope.CorrectAnswerCount = 0;
    $scope.DisableCorrectAnswer = false;

    $scope.ShowGlobalFeedback = false;
    $scope.ShowGlobalIncorrectFeedback = false;
    $scope.ShowGlobalCorrectFeedback = false;
    $scope.showOptionFeedbackModal = false;
    $scope.showOptionFeedbackIcon = false;
    $scope.showCheckAnswerbtn = false;

    $scope.isPaused = false;
    $scope.showTimer = false;
    $scope.callbackTimer = {};
    $scope.timerRunning = true;
    var timeStarted = false;

    //UserAssessmentActivity related variables
    $scope.BookMarked = false;
    $scope.showAttempt = false;
    $scope.NoLearned = true;
    $scope.QuestionMapBookMarkClicked = false;
    $scope.QuestionMapViewAllClicked = false;
    $scope.UserAssessmentActivityDetail = {
        QuestionStatus: 'not-visited',
        IsBookMarked: '',
        NotLearned: '',
        HintsUsed: 0,
        NoOfRetry: 0,
        TimeSpent: 0,
        ReportAProblem: '',
        QuizQuestID: '',
        UserAssessmentActivityType: 0,
        QuestionNo: 0
    };

    $scope.mathEditorLatex = "";
    $scope.mathEditorPrevLatex = "";
    $scope.mathEditorCurrLatex = "";
    var mathEditorPrevLatex = "";

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.trustAsHtml = function (string) {
        return $sce.trustAsHtml(string);
    };

    $scope.getQuizDetails = function (quizId) {
        $scope.quizID = quizId;
        //assessmentEngineSvc.SetQuizID(quizID);
        var params = { 'quizId': quizId };
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            //if ($location.absUrl().indexOf("index.html") > -1) {
            $scope.loadjsonfile();
            //}
        }
        else {
            assessmentEngineSvc.FetchQuizDetails(params).$promise
                   .then(function (response) {
                       $scope.quizName = response.AssignmentName;
                       $scope.CourseID = response.ClassId;
                       // var questions = '';
                       if (!$scope.isUndefined(response.QuestionIds) && response.QuestionIds != null && response.QuestionIds != "") {
                           questionsList = angular.fromJson(response.QuestionIds);
                           angular.forEach(questionsList, function (question, key) {
                               $scope.questions.push(question.QuestionId);
                           });
                           $scope.startQuiz($scope.questions);
                       }

                   }).catch(function (er) {
                       $scope.Message = er.error_description;
                   });
        }
    };

    $scope.startQuiz = function (questions, isCompositeQuestion) {
        //debugger;
        //  $scope.quizID = quizID;
        //var params = { quizID: quizID, currentQuestion: 0 };
        //var params = { 'questionId': questions[0], 'assignmentId': AssignmentID };
        $scope.showFooter = true;
        $scope.AssignmentQuestions = [];
        var getQuestion;
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            if (questions != undefined) {
                $scope.offline.quiz = questions.quiz;

                $scope.AssignmentSetting = questions.settings;
                if (questions.quiz.length > 1) {
                    $scope.isLastquestion = false;
                    if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
                        $scope.showModal = true;
                    }
                    else {
                        $scope.showModal = false;
                    }
                }
                else {
                    $scope.isBackButtonOn = false;
                    $scope.isLastquestion = true;
                    if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
                        $scope.showModal = true;
                    }
                    else {
                        $scope.showModal = false;
                    }
                }

            }

            if ($scope.isLastquestion == true && $scope.showModal == false) {
                $scope.offline.quiz[0].CurrentQuestion = 1;
                $scope.initializeQuiz($scope.offline.quiz[0]);
            }
            else if ($scope.isLastquestion == false && $scope.showModal == false) {
                $scope.offline.quiz[0].CurrentQuestion = 1;
                $scope.initializeQuiz($scope.offline.quiz[0]);
            }
            else if (isCompositeQuestion || $scope.isComposite || $location.absUrl().indexOf("CompositeIndex.html") > -1) {
                $scope.offline.quiz[0].CurrentQuestion = 1;
                $scope.initializeQuiz($scope.offline.quiz[0]);
            }
            else {
                var modalInstance = $modal.open({
                    templateUrl: 'SettingDetails.html',
                    scope: $scope,
                    backdrop: 'static',
                    keyboard: false
                });

                $scope.Ok = function () {
                    modalInstance.close('cancel');
                    $('#quizContainer').show();
                    if ($scope.offline.quiz.length == 1)
                        $scope.isLastquestion = true;
                };

                modalInstance.result.then(function (result) {
                    var index = 0;
                    angular.forEach($scope.offline.quiz, function (question, key) {
                        var UserAssessmentActivityDetail = {
                            QuestionStatus: 'not-visited',
                            IsBookMarked: '',
                            NotLearned: '',
                            HintsUsed: 0,
                            NoOfRetry: 0,
                            TimeSpent: 0,
                            ReportAProblem: '',
                            QuizQuestID: '',
                            UserAssessmentActivityType: 0,
                            QuestionNo: 0
                        };
                        index = index + 1;
                        UserAssessmentActivityDetail.QuizQuestID = question.QuestionId;
                        UserAssessmentActivityDetail.QuestionNo = index;
                        $scope.AssignmentQuestions.push(UserAssessmentActivityDetail);
                    });
                    $scope.searchFilter = { QuestionStatus: '' };
                    $scope.offline.quiz[0].CurrentQuestion = 1;
                    $scope.initializeQuiz($scope.offline.quiz[0]);

                    $scope.Timer = angular.fromJson($scope.AssignmentSetting.Timer);

                    if ($scope.Timer != undefined) {
                        if ($scope.Timer.Start == true && $scope.Timer.Minute != 0) {
                            $scope.showTimer = true;
                            $scope.ActiveTimer($scope.Timer, $scope.CurrentQuestion);
                        }
                        else {
                            $scope.showTimer = false;
                        }
                    }
                }, function () { });
            }

        }
        else {
            assessmentEngineSvc.StartQuiz(JSON.stringify(params)).$promise
                .then(function (response) {
                    response[0].QuestionData = $.parseJSON(response[0].QuestionData)
                    response = response[0];
                    response.CurrentQuestion = 1;
                    $scope.initializeQuiz(response);
                    $scope.ActiveTimer($scope.CurrentQuestion);
                }).catch(function (er) {
                    $scope.Message = er.error_description;
                });
        }
        $('.btn-arrow-left').prop('disabled', true);
        resize_fun();
        if ($scope.offline.quiz.length == 1) {
            $('.btn-arrow-right').html('Submit');
            $('.btn-arrow-right').addClass('dummyClass');
            $('.btn-arrow-right').removeClass('btn-arrow-right');
            $scope.isBackButtonOn = true;
            $scope.isLastquestion = true;
        }
        else {
            $('.dummyClass').html(' Next <i class="fa fa-arrow-circle-right fa-fw"></i>');
            $('.dummyClass').addClass('btn-arrow-right');
            $('.dummyClass').removeClass('dummyClass');
            $scope.isLastquestion = false;
        }
    };
    $scope.goTo = function (index, Next) {
        $scope.mathEditorRefactor();
        $("#loader").show();
        $("#loader").removeClass('hide');
        $scope.showHint = false;
        $scope.showExhibit = false;
        //$scope.userHasSelectedOptions = false;

        $scope.ShowGlobalFeedback = false;
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.ShowGlobalCorrectFeedback = false;
        $scope.showOptionFeedbackModal = false;

        $scope.DisableCorrectAnswer = false;
        $scope.showAttempt = false;
        $scope.QuestionAttempt = 0;
        $scope.SingledroppableConfig.disabled = false;
        $scope.SingledraggableConfig.disabled = false;
        $scope.SortableFuncConfig.disabled = false;
        $scope.CurrentQuestion.SelectedOptions = $scope.userSelectedOptions();
        $scope.CurrentQuestion.UserAssessmentActivityDetail = $scope.UserAssessmentActivityDetail;
        $scope.CurrentQuestion.CorrectAnswerCount = $scope.CorrectAnswerCount;
        //$scope.CurrentQuestion.isNext = Next;

        $scope.MapQuestionToSave(Next, index);

        if ($scope.userHasSelectedOptions == true) {
            $scope.SetQuestionMapSetting(true, Enum.AssessmentActivityType.SubmissionStatus);
        } else {
            if ($scope.AssignmentQuestions.length > 0) {
                $scope.AssignmentQuestions[$scope.currentPage - 1].QuestionStatus = 'answer-visited';
            }
        }
        $scope.CurrentQuestion.TimeSpent = $scope.PassiveTimer.stopTimer();
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            if (index <= $scope.offline.quiz.length) {
                if (index == 1) {
                    $('.btn-arrow-left').prop('disabled', true);
                    $scope.isLastquestion = false;
                    $scope.isBackButtonOn = false;
                }
                else {
                    $('.btn-arrow-left').prop('disabled', false);
                    $scope.isBackButtonOn = true;
                }
                if ($scope.offline.quiz.length == index) {
                    $('.btn-arrow-right').html('Submit');
                    $('.btn-arrow-right').addClass('dummyClass');
                    $('.btn-arrow-right').removeClass('btn-arrow-right');
                    $scope.isBackButtonOn = true;
                    $scope.isLastquestion = true;
                }
                else {
                    $('.dummyClass').html(' Next <i class="fa fa-arrow-circle-right fa-fw"></i>');
                    $('.dummyClass').addClass('btn-arrow-right');
                    $('.dummyClass').removeClass('dummyClass');
                    $scope.isLastquestion = false;
                }

                $scope.offline.quiz[index - 1].CurrentQuestion = index;
                $scope.initializeQuiz($scope.offline.quiz[index - 1]);
                //  if (document.getElementById('compositeIframe') != null) document.getElementById('compositeIframe').contentWindow.location.reload();
            }
            else {
                $scope.stopTimer();
                $scope.showFeedback = true;
                $scope.generateFeedback();
            }
            $("#loader").hide();
            $("#loader").addClass('hide');
        }
        else {
            if (index == 1) {
                $('.btn-arrow-left').prop('disabled', true);
            }
            else {
                $('.btn-arrow-left').prop('disabled', false);
            }

            if ($scope.totalItems == index) {
                $('.btn-arrow-right').html('Submit');
                $('.btn-arrow-right').removeClass('btn-arrow-right');
            }
            assessmentEngineSvc.FetchQuestion(serializeData(params)).$promise
                .then(function (response) {

                    if ($scope.questiontosave.NextQuestionId !== undefined) {//index <= $scope.totalItems
                        response[0].QuestionData = $.parseJSON(response[0].QuestionData)
                        response = response[0];
                        response.CurrentQuestion = index;
                        $scope.initializeQuiz(response);
                    }
                    else {
                        $scope.TimerClass.stopTimer(function () { });
                        $scope.generateFeedback();
                        $scope.showFeedback = true;
                    }
                }).catch(function (er) {
                    $scope.Message = er.error_description;
                });
        }


    };

    $scope.MapQuestionToSave = function (Next, index) {
        $scope.questiontosave.ApplicationID = 0,
        $scope.questiontosave.ProductID = 9209,
        $scope.questiontosave.CourseID = $scope.CourseID,
        $scope.questiontosave.ItemGUID = AssignmentID,
        $scope.questiontosave.UserID = 0,
        $scope.questiontosave.QuestionGUID = $scope.CurrentQuestion.QuestionId,
        $scope.questiontosave.SelectedOptions = $scope.userSelectedOptions(),
        $scope.questiontosave.MaxScore = 0,
        $scope.questiontosave.UserScore = 0,
        $scope.questiontosave.UserAnswerText = "",
        $scope.questiontosave.UserAudioFile = "",
        $scope.questiontosave.TimeSpent = 0,
        $scope.questiontosave.isNext = Next,
        $scope.questiontosave.CurrentQuestion = index,
        $scope.questiontosave.QuestionTypeID = $scope.CurrentQuestion.QuestionTypeId,
        $scope.questiontosave.NextQuestionId = $scope.questions[index - 1],
        $scope.questiontosave.AssignmentID = AssignmentID
    };

    $scope.generateFeedback = function () {
        $scope.showHint = false;
        $('#spnMathQuillCreateMathEquation').mathquill();
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            //$scope.loadjsonfile();
            $scope.FeedBack = $scope.offline.quiz;
            $.each($scope.FeedBack, function (index, value) {
                $scope.FeedBack[index].QuestionData = value.QuestionData;
                $scope.FeedBack[index].QuestionCount = index + 1;
                $scope.FeedBack[index].QuestionOptions = value.QuestionData.choices;
                //$scope.FeedBack[index].layoutType = value.QuestionData.question_layout.text;;
                $scope.FeedBack[index].draggableItems = [];
                $scope.FeedBack[index].leftvertical = "";
                $scope.FeedBack[index].rightvertical = "";
                $scope.FeedBack[index].ImageSrc = '';
                var questionType = value.QuestionTypeId;

                if (!$scope.isUndefined(value.QuestionData.stem_details) && value.QuestionData.stem_details.type == 'algorithimic') {
                    $scope.SetFeedbackAlgorithimicType(value);
                }

                if (!$scope.isUndefined(value.QuestionData.question_type)) {
                    if ((value.QuestionData.question_type.text == 'ltd_dnd' || value.QuestionData.question_type.text == 'ltd' || value.QuestionData.question_type.text == 'hotspot' || value.QuestionData.question_type.text == 'composite' || value.QuestionData.question_type.text == 'choicematrix' || value.QuestionData.question_type.text == 'mcss' || value.QuestionData.question_type.text == 'mcms' || value.QuestionData.question_type.text == 'order' || value.QuestionData.question_type.text == 'fib' || value.QuestionData.question_type.text == 'fib_dnd' || value.QuestionData.question_type.text == 'matching' || value.QuestionData.question_type.text == 'essay' || value.QuestionData.question_type.text == 'textselection') && value.QuestionData.question_type.text.length > 0) {
                        questionType = value.QuestionData.question_type.text;
                    }
                }
                $scope.FeedBack[index].questionTypeId = questionType;
                if (questionType == Enum.QuestionType.Composite) {
                    $scope.SetCompositeType($scope.FeedBack[index], index);
                }
                else if (questionType == Enum.QuestionType.TextSelection) {
                    if ($scope.FeedBack[index].QuestionData.textwithblanks != undefined) {
                        if ($scope.FeedBack[index].QuestionData.textwithblanks.text != undefined && $scope.FeedBack[index].QuestionData.textwithblanks.text != "")
                            questiontext = $scope.FeedBack[index].QuestionData.textwithblanks.text;
                    }

                    if (!$scope.isUndefined(value.QuestionData.exhibit) && value.QuestionData.exhibit.length > 0 && value.QuestionData.exhibit[0].exhibit_type != "") {
                        value.showExhibit = true;
                        if (value.QuestionData.exhibit[0].exhibit_type == 'image') {
                            var imagePath = String(value.QuestionData.exhibit[0].path);
                            value.QuestionData.exhibit[0].path = imagePath;
                        }
                    }
                    else {
                        value.showExhibit = false;
                    }

                    $scope.FeedBack[index].PassageText = "";
                    angular.forEach($scope.FeedBack[index].QuestionData.choices, function (option, key) {
                        var startTagToReplace = option.text;
                        var endTagToReplace = option.text.substring(0, 2) + "/" + option.text.substring(2);
                        $scope.FeedBack[index].QuestionData.choices[key].key = key;       // Index of option            
                        $scope.FeedBack[index].QuestionData.choices[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect            
                        var answerClass = '';
                        //ar correctAnswers= $scope.FeedBack[index].QuestionData.correct_answer.split(',');
                        if ($scope.FeedBack[index].SelectedOptions) {
                            var selectedOption = angular.fromJson($scope.FeedBack[index].SelectedOptions);
                            $(selectedOption).each(function (textindex, value) {
                                if (value.choiceid == option.choiceid) {
                                    if ($scope.FeedBack[index].QuestionData.correct_answer.indexOf(value.choiceid) != -1) {
                                        answerClass = 'selectedC correct';
                                    }
                                    else {
                                        answerClass = 'selectedC incorrect';
                                    }
                                }
                            });
                        }

                        questiontext = questiontext.replace(startTagToReplace, "&nbsp;&nbsp;<span class='option selectable " + answerClass + "' style='cursor: default;'> <span class='sign'><span class='sr-only'></span><span class='fa'></span></span>");
                        questiontext = questiontext.replace(endTagToReplace, "</span>&nbsp;&nbsp;");
                    });
                    $scope.FeedBack[index].PassageText = questiontext;
                }
                else if (questionType == Enum.QuestionType.HotSpot) {
                    //var regex = /<img.*?src='(.*?)'/;
                    if (!$scope.isUndefined($scope.FeedBack[index].QuestionData.question_media.media) && $scope.FeedBack[index].QuestionData.question_media.media.length > 0 && $scope.FeedBack[index].QuestionData.question_media.media != "") {
                        //$scope.FeedBack[index].ImageSrc = regex.exec($scope.FeedBack[index].QuestionData.question_media.media)[1];
                        var imgObject = document.createElement('div');
                        imgObject.innerHTML = value.QuestionData.question_media.media;
                        $scope.FeedBack[index].ImageSrc = $(imgObject).find('img').attr('src');
                    }
                    if ($scope.FeedBack[index].SelectedOptions) {
                        var correctoptions = $scope.FeedBack[index].QuestionData.correct_answer.split(",");
                        var selectedoptions = $.parseJSON($scope.FeedBack[index].SelectedOptions);
                        $scope.FeedBack[index].SelectedOptions = selectedoptions.map(function (x) { return x.choiceid; }).toString();

                        $($scope.FeedBack[index].QuestionData.choices).each(function (cindex, value) {
                            $(selectedoptions).each(function (selectindex, svalue) {
                                if (value.choiceid == parseInt(svalue.choiceid)) {
                                    if (correctoptions.indexOf(value.choiceid.toString()) != -1) {
                                        value.isCorrect = true;
                                        value.currentClass = "correct";
                                    }
                                    else {
                                        value.isCorrect = false;
                                        value.currentClass = "incorrect";
                                    }
                                }
                            });

                        });
                    }
                }
                else if (questionType == Enum.QuestionType.LTD || questionType == Enum.QuestionType.LTDDND) {
                    //var regex = /<img.*?src='(.*?)'/;                   
                    if (!$scope.isUndefined($scope.FeedBack[index].QuestionData.question_media.media) && $scope.FeedBack[index].QuestionData.question_media.media.length > 0 && $scope.FeedBack[index].QuestionData.question_media.media != "") {
                        //$scope.FeedBack[index].ImageSrc = regex.exec($scope.FeedBack[index].QuestionData.question_media.media)[1];
                        var imgObject = document.createElement('div');
                        imgObject.innerHTML = value.QuestionData.question_media.media;
                        $scope.FeedBack[index].ImageSrc = $(imgObject).find('img').attr('src');
                    }
                    $scope.FillFeedbackDroppableItems($scope.FeedBack[index]);
                    angular.forEach($scope.FeedBack[index].QuestionData.choices, function (option, key) {
                        var inputValue = "";
                        if ($scope.FeedBack[index].SelectedOptions) {
                            var selectedOption = angular.fromJson($scope.FeedBack[index].SelectedOptions);
                            $(selectedOption).each(function (fibindex, value) {
                                if (value.noforeachblank == option.noforeachblank && value.inputvalue != "") {
                                    option.LTDText = value.inputvalue;
                                    inputValue = value.inputvalue;
                                    option.AnswerClass = (value.isCorrectAnswer) ? "correct" : "incorrect";
                                }
                            });
                        }

                        if (option.interaction == 'dragdrop') {
                            $scope.FeedBack[index].LayoutType = value.QuestionData.question_layout.text;

                            if (option.interaction == 'dragdrop') {
                                angular.forEach($scope.FeedBack[index].draggableItems, function (ditem, dindex) {
                                    if ($filter('lowercase')(inputValue) == $filter('lowercase')('<span math dynamic>' + ditem.title + '</span>')) {
                                        ditem.title = "__________";
                                    }
                                });
                            }


                            //htmlText = "<span class='option drop " + answerClass + "'><span class='option drag'>" + inputValue + "</span><span class='sign'><span class='fa'></span></span></span>";
                            if ($scope.FeedBack[index].QuestionData.question_layout.text === "right" || $scope.FeedBack[index].QuestionData.question_layout.text === "left" || $scope.FeedBack[index].QuestionData.question_layout.text === "vertical") {
                                $scope.FeedBack[index].leftvertical = "col-md-10";
                                $scope.FeedBack[index].rightvertical = "col-md-2 right";
                            }
                            else if ($scope.FeedBack[index].QuestionData.question_layout.text === "row") {
                                $scope.FeedBack[index].leftvertical = "";
                                $scope.FeedBack[index].rightvertical = "text-center";
                            }

                            //$scope.FeedBack[index].QuestionOptions[key].isCorrectAnswer = answerClass;
                            //$scope.FeedBack[index].QuestionOptions[key].selectedAnswer = inputValue;
                        }

                        //questiontext = questiontext.replace(textToReplace, htmlText);

                    });
                    //$scope.FeedBack[index].LTDText = questiontext;
                }
                else if (questionType == Enum.QuestionType.FIB || questionType == Enum.QuestionType.FIBDND || questionType == Enum.QuestionType.Matching) {
                    var questiontext = "";
                    if (value.QuestionData.textwithblanks != undefined) {
                        if (value.QuestionData.textwithblanks.text != undefined && value.QuestionData.textwithblanks.text != "")
                            questiontext = $scope.FeedBack[index].QuestionData.textwithblanks.text;
                    }

                    $scope.FillFeedbackDroppableItems($scope.FeedBack[index]);
                    angular.forEach($scope.FeedBack[index].QuestionData.choices, function (option, key) {
                        var textToReplace = option.noforeachblank;
                        var htmlText = "";
                        var answerClass = "";
                        var inputValue = "";

                        if ($scope.FeedBack[index].SelectedOptions) {
                            var selectedOption = angular.fromJson($scope.FeedBack[index].SelectedOptions);
                            $(selectedOption).each(function (fibindex, value) {
                                if (value.noforeachblank == option.noforeachblank && value.inputvalue != "") {
                                    inputValue = value.inputvalue;
                                    answerClass = (value.isCorrectAnswer) ? "correct" : "incorrect";
                                }
                            });
                        }
                        if (option.interaction == 'textentry') {
                            htmlText = "<span class='option " + answerClass + "'><input type='text' class='form-control' disabled value='" + inputValue + "'><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
                        }
                        else if (option.interaction == 'inline') {
                            htmlText = "<span class='option " + answerClass + "'><select class='form-control' disabled><option>" + inputValue + "</option></select><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
                        }
                        else if (option.interaction == 'dragdrop' || option.interaction == 'container') {
                            $scope.FeedBack[index].LayoutType = value.QuestionData.question_layout.text;

                            if (option.interaction == 'dragdrop') {
                                angular.forEach($scope.FeedBack[index].draggableItems, function (ditem, dindex) {
                                    if ($filter('lowercase')(inputValue) == $filter('lowercase')(ditem.title)) {
                                        ditem.title = "__________";
                                    }
                                });
                            }
                            else if (option.interaction == 'container') {
                                var input = !!inputValue ? inputValue.map(function (x) { return x.title; }) : [];
                                angular.forEach($scope.FeedBack[index].draggableItems, function (ditem, dindex) {
                                    if (input.indexOf(ditem.title) != -1) {
                                        ditem.title = "__________";
                                    }
                                });
                                if (option.Canvas != undefined) {
                                    var isOptionIncorrect = false;
                                    var isOptionAnswered = false;
                                    for (var i = 0; i < option.Canvas.length; i++) {
                                        isOptionAnswered = true;
                                        if (option.Canvas[i].AnswerClass == "incorrect") {
                                            isOptionIncorrect = true;
                                        }

                                    }
                                    if (isOptionAnswered) {
                                        option.AnswerClass = isOptionIncorrect ? "incorrect" : "correct";
                                    }
                                }
                            }

                            if ($scope.FeedBack[index].QuestionData.question_layout.text != "dragdrop_parallel") {
                                htmlText = "<span class='option drop " + answerClass + "'><span class='option drag'>" + inputValue + "</span><span class='sign'><span class='fa'></span></span></span>";
                                if ($scope.FeedBack[index].QuestionData.question_layout.text === "right" || $scope.FeedBack[index].QuestionData.question_layout.text === "left" || $scope.FeedBack[index].QuestionData.question_layout.text === "vertical") {
                                    $scope.FeedBack[index].leftvertical = "col-md-10";
                                    $scope.FeedBack[index].rightvertical = "col-md-2 right";
                                }
                                else if ($scope.FeedBack[index].QuestionData.question_layout.text === "row") {
                                    $scope.FeedBack[index].leftvertical = "";
                                    $scope.FeedBack[index].rightvertical = "text-center";
                                }

                                $scope.FeedBack[index].QuestionOptions[key].isCorrectAnswer = answerClass;
                                $scope.FeedBack[index].QuestionOptions[key].selectedAnswer = inputValue;
                            }
                            else {
                                $scope.FeedBack[index].QuestionOptions[key].isCorrectAnswer = answerClass;
                                $scope.FeedBack[index].QuestionOptions[key].selectedAnswer = inputValue;
                            }
                        }

                        questiontext = questiontext.replace(textToReplace, htmlText);

                    });
                    $scope.FeedBack[index].FIBText = questiontext;

                }

                else if (questionType == Enum.QuestionType.MCSS || questionType == Enum.QuestionType.MCMS) {
                    value.questionSubType = value.QuestionData.question_interaction.text;
                    value.LayoutType = value.QuestionData.question_layout.text;
                    if (!$scope.isUndefined(value.QuestionData.exhibit) && value.QuestionData.exhibit.length > 0 && value.QuestionData.exhibit[0].exhibit_type != "") {
                        value.showExhibit = true;
                        if (value.QuestionData.exhibit[0].exhibit_type == 'image') {
                            var imagePath = String(value.QuestionData.exhibit[0].path);
                            value.QuestionData.exhibit[0].path = imagePath;
                        }
                    }
                    else {
                        value.showExhibit = false;
                    }
                }

                $($scope.FeedBack[index].QuestionData.choices).each(function (indexOpt, valueOpt) {
                    //if (valueOpt.assess == true) {
                    if (questionType == Enum.QuestionType.MCSS || questionType == Enum.QuestionType.MCMS) {
                        var correctoptions = $scope.FeedBack[index].QuestionData.correct_answer.split(",");
                        $scope.FeedBack[index].CorrectOptions = correctoptions.toString();
                        var sSrchTxt = String(valueOpt.text);
                        if (sSrchTxt.indexOf('<img') != -1) {
                            var options = String(valueOpt.text).split('<img');
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].text = options[0];
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].MCSSImage = "<img" + options[1];
                        }
                        else {
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].text = sSrchTxt;
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].MCSSImage = null;
                        }

                        if (this.isAnswered == true) {
                            $(correctoptions).each(function (correctindex) {
                                if (parseInt(valueOpt.choiceid) != parseInt(correctoptions[correctindex])) {
                                    valueOpt.isCorrect = false;
                                    valueOpt.currentClass = "incorrect";
                                }
                            });
                        }

                        $(correctoptions).each(function (correctindex) {
                            if (parseInt(valueOpt.choiceid) == parseInt(correctoptions[correctindex])) {
                                valueOpt.isCorrect = true;
                                valueOpt.currentClass = "correct";
                            }
                        });
                    }

                    if (questionType == Enum.QuestionType.ChoiceMatrix) {
                        if (value.SelectedOptions) {
                            var selectedOpt = angular.fromJson(value.SelectedOptions);
                            $(selectedOpt).each(function (selectindex, selectvalue) {
                                var inputValues = selectvalue.inputvalue.split("-");
                                var optionindex = indexOpt + 1;
                                $($scope.FeedBack[index].QuestionData.choices[indexOpt].suboption).each(function (index1, value1) {
                                    if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                                        $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isAnswered = true;
                                        var assess;
                                        if (parseInt(inputValues[1]) == 1) {
                                            assess = true;
                                        }
                                        else {
                                            assess = false;
                                        }
                                        if ($scope.FeedBack[index].QuestionData.choices[indexOpt].assess === assess) {
                                            $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isCorrect = true;
                                        }
                                        else {
                                            $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isCorrect = false;
                                        }
                                    }
                                    else {
                                        var val = (value1.value === "true");
                                        if ($scope.FeedBack[index].QuestionData.choices[indexOpt].assess === val) {
                                            $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isCorrect = true;
                                        }
                                    }
                                });
                            });

                        }
                    }

                    else if (questionType == Enum.QuestionType.Order) {
                        $scope.FeedBack[index].OrderSetting = $scope.FeedBack[index].QuestionData.question_interaction.text;

                        var correctoptions = $scope.FeedBack[index].QuestionData.correct_answer.split(",");
                        var optionindex = valueOpt.choiceid;
                        if (parseInt(optionindex) == correctoptions[indexOpt]) {
                            valueOpt.isCorrect = true;
                        }
                        else {
                            valueOpt.isCorrect = false;
                        }

                        if ($scope.orderType == "media" && $scope.FeedBack[index].QuestionData.question_layout != null)
                            $scope.FeedBack[index].LayoutType = $scope.FeedBack[index].QuestionData.question_layout.text;
                    }
                });
                if (questionType == Enum.QuestionType.Essay) {
                    $scope.text = '';
                    $scope.FeedBack[index].EssayText = '';
                    if (value.SelectedOptions) {
                        if ($scope.FeedBack[index].QuestionData.toolbar.type == "math_3-5_palette" || $scope.FeedBack[index].QuestionData.toolbar.type == "math_6-8_palette") {
                            $scope.FeedBack[index].IsFeedbackMathType = true;
                            $('#spnMathQuillCreateMathEquation').mathquill('latex', value.SelectedOptions[0]);

                            // Added the logic as Bootstrap is conflicting with the MATH font.
                            $('#spnMathQuillCreateMathEquation span.sqrt-prefix[style^="transform"]').attr("style", "transform: scale(1,1);");
                            $('#spnMathQuillCreateMathEquation span.scaled[style^="transform"]').attr("style", "transform: scale(1,1);");

                            $('#divMathQuillCreateMathEquation span').each(function () {
                                if ($(this).html() == '?') {
                                    $(this).html('');
                                }
                            });

                            $scope.FeedBack[index].feedbackMathType = $sce.trustAsHtml($('#divMathQuillCreateMathEquation').html());
                        } else {
                            $scope.FeedBack[index].IsFeedbackMathType = false;
                            $scope.FeedBack[index].EssayText = value.SelectedOptions; //$sce.trustAsHtml(value.SelectedOptions)
                        }
                    }

                }
                else if (questionType == Enum.QuestionType.Order || questionType == Enum.QuestionType.TextSelection) {
                    if (value.SelectedOptions) {
                        var userSelectedOptions = "";
                        var selectedOpt = angular.fromJson(value.SelectedOptions);
                        $(selectedOpt).each(function (sindex, svalue) {
                            if (userSelectedOptions == "") {
                                userSelectedOptions = svalue.choiceid;
                            }
                            else {
                                userSelectedOptions = userSelectedOptions + "," + svalue.choiceid;
                            }
                        });
                        value.SelectedOptions = userSelectedOptions;
                    }
                }

            });
            $scope.getFeedbackOptions($scope.FeedBack);
        }


    };

    $scope.StopClickEvent = function (event) {
        return false;
    };

    $scope.SetFeedbackAlgorithimicType = function (data) {
        if (!$scope.isUndefined(data.AlgorithmicVariableArray) && data.QuestionData.stem_details.textforeachvalue.length > 0) {
            angular.forEach(data.QuestionData.stem_details.textforeachvalue, function (textDetail, key) {
                angular.forEach(data.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    data.QuestionData.question_stem.text = data.QuestionData.question_stem.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                });
            });
        }
    }

    $scope.FillFeedbackDroppableItems = function (data) {
        isDistractorsPushDone = false;
        angular.forEach(data.QuestionData.choices, function (option, key) {
            if (isDistractorsPushDone == false) {
                angular.forEach(data.QuestionData.distractors, function (titem, tindex) {
                    data.draggableItems.push({ 'title': titem.text, 'drag': false });
                });
                isDistractorsPushDone = true;
            }

            angular.forEach(option.textforeachblank, function (titem, tindex) {
                data.draggableItems.push({ 'title': titem.text, 'drag': false });
            });
        });
    }

    $scope.getFeedbackOptions = function (data) {
        $(data).each(function (index, value) {
            var questionType = value.QuestionTypeId;
            if (!$scope.isUndefined(value.QuestionData.question_type)) {
                if ((value.QuestionData.question_type.text == 'hotspot' || value.QuestionData.question_type.text == 'mcss' || value.QuestionData.question_type.text == 'mcms' || value.QuestionData.question_type.text == 'order' || value.QuestionData.question_type.text == 'fib' || value.QuestionData.question_type.text == 'fib_dnd' || value.QuestionData.question_type.text == 'matching' || value.QuestionData.question_type.text == 'essay' || value.QuestionData.question_type.text == 'textselection') && value.QuestionData.question_type.text.length > 0) {
                    questionType = value.QuestionData.question_type.text;
                }
            }
            if (questionType != Enum.QuestionType.HotSpot && questionType != Enum.QuestionType.TextSelection && questionType != Enum.QuestionType.Order && questionType != Enum.QuestionType.FIB && questionType != Enum.QuestionType.FIBDND && questionType != Enum.QuestionType.Matching && questionType != Enum.QuestionType.Essay && questionType != Enum.QuestionType.ChoiceMatrix) {
                if (value.SelectedOptions != "" && value.SelectedOptions != null && value.SelectedOptions != "[]") {
                    var selectedOptions = angular.fromJson(value.SelectedOptions);
                }
                if (value.CorrectOptions != null) {
                    var correctoptions = value.CorrectOptions.split(",");
                }
                $(data[index].QuestionData.choices).each(function (index1, value1) {
                    var optionindex = index1 + 1;
                    if (selectedOptions) {
                        $(selectedOptions).each(function (selectindex, selectvalue) {
                            var selectedindex = parseInt(selectvalue.choiceid) - 1;
                            $(correctoptions).each(function (correctindex, correctvalue) {
                                var correctindex = correctvalue - 1;
                                if (correctvalue === parseInt(selectvalue)) {
                                    data[index].QuestionData.choices[selectedindex].isCorrect = true;
                                }
                                else {

                                    data[index].QuestionData.choices[selectedindex].isCorrect = false;
                                    data[index].QuestionData.choices[correctindex].isCorrect = true;
                                }
                                data[index].QuestionData.choices[selectedindex].isSelected = true;
                            });
                        });
                    }
                });
            }

        });

    };

    $scope.loadjsonfile = function () {
        $("#loader").show();
        $("#loader").removeClass('hide');
        var file = "Scripts/js/offlinejson.js";
        $http.get(file)
         .then(function (res) {
             var data = res.data.quiz;
             var settings = res.data.settings;
             //var data = jQuery.grep(res.data.quiz, function (n, i) {
             //    return (parseInt(n.ItemID) == parseInt($scope.quizID));
             //});;
             $scope.offline.quizInfo = "Demo Quiz";//data[0].quizName;
             $scope.offline.quiz = data;
             $scope.AssignmentSetting = settings;

             if ($scope.showFeedback == true) {
                 $scope.quizName = "Feedback - Demo";
             }
             else {
                 $scope.quizName = "Demo";
             }
             if (data.length > 0) {
                 $scope.startQuiz();
             }
             else {
                 $scope.BackwardMovement = false;
             }
             $("#loader").hide();
             $("#loader").addClass('hide');
         });
    }

    $scope.autoSubmit = function () {
        $scope.showFeedback = true;
        $scope.generateFeedback();
        $scope.$apply();
    };

    $scope.ShowGlobalFeebackDataForComposite = function (data, question) {
        if (data == 'correct') {
            if (question.global_correct_feedback.text != null && question.global_correct_feedback.text != "") {
                question.globalCorrectFeedback = '<span math dynamic>' + question.global_correct_feedback.text + '</span>';
                question.ShowGlobalCorrectFeedback = true;
            }
            else {
                question.ShowGlobalCorrectFeedback = false;
            }
        }
        else if (data == 'incorrect') {
            if (question.global_incorrect_feedback.text != null && question.global_incorrect_feedback.text != "") {
                question.globalInCorrectFeedback = '<span math dynamic>' + question.global_incorrect_feedback.text + '</span>';
                question.ShowGlobalIncorrectFeedback = true;
            }
            else {
                question.ShowGlobalIncorrectFeedback = false;
            }
        }

        //$(question.choices).each(function (opIndex, value) {
        //    if (value.correct_feedback != "" && value.incorrect_feedback != "" && jQuery.isEmptyObject($scope.AssignmentSetting)) {
        //        value.showOptionFeedbackModal = true;
        //        value.showAttempt = true;
        //        $('.fdback').removeClass('ng-hide');
        //    }
        //    else {
        //        value.showOptionFeedbackModal = false;
        //        value.showAttempt = false;
        //        $('.fdback').addClass('ng-hide');
        //    }
        //});
    };

    $scope.ShowGlobalFeebackData = function (data) {
        if ($scope.isLastquestion && jQuery.isEmptyObject($scope.AssignmentSetting)) {
            $scope.ShowGlobalFeedback = true;
        }
        if (data == 'correct') {
            if ($scope.CurrentQuestion.QuestionData.global_correct_feedback.text != null && $scope.CurrentQuestion.QuestionData.global_correct_feedback.text != "") {
                $scope.globalCorrectFeedback = '<span math dynamic>' + $scope.CurrentQuestion.QuestionData.global_correct_feedback.text + '</span>';
                $scope.ShowGlobalCorrectFeedback = true;
                //$scope.ShowGlobalIncorrectFeedback = false;
            }
            else {
                $scope.ShowGlobalCorrectFeedback = false;
            }
        }
        else if (data == 'incorrect') {
            if ($scope.CurrentQuestion.QuestionData.global_incorrect_feedback.text != null && $scope.CurrentQuestion.QuestionData.global_incorrect_feedback.text != "") {
                $scope.globalInCorrectFeedback = '<span math dynamic>' + $scope.CurrentQuestion.QuestionData.global_incorrect_feedback.text + '</span>';
                $scope.ShowGlobalIncorrectFeedback = true;
                //$scope.ShowGlobalCorrectFeedback = false;
            }
            else {
                $scope.ShowGlobalIncorrectFeedback = false;
            }
        }
    };

    $scope.showHintData = function () {
        $scope.showHint = true;
        $scope.hintCount = $scope.hintCount - 1;
        $scope.UpdateUserAssessmentActivity($scope.UserAssessmentActivityDetail.HintsUsed, Enum.AssessmentActivityType.ShowHint);
        $scope.visibleHints.push('<span math dynamic>' + $scope.CurrentQuestion.QuestionData.hints[$scope.UserAssessmentActivityDetail.HintsUsed - 1].text + '</span>');
    };

    $scope.BookMarkQuestion = function () {
        if ($scope.BookMarked) {
            $scope.BookMarked = false;
        }
        else {
            $scope.BookMarked = true;
        }
        $scope.UpdateUserAssessmentActivity($scope.BookMarked, Enum.AssessmentActivityType.BookMark);
    };

    $scope.NotLearnedQuestion = function () {
        if ($scope.NotLearned) { return false; }
        $scope.NotLearned = true;
        $scope.disableNextBtn = false;
        $scope.UpdateUserAssessmentActivity($scope.NotLearned, Enum.AssessmentActivityType.NotLearned);
    };

    $scope.questionQuizPreviewCacncel = function () {
        angular.element('#quizContainer').hide();
    }

    $scope.UpdateUserAssessmentActivity = function (updateValue, activityType) {
        switch (activityType) {
            case Enum.AssessmentActivityType.BookMark:
                $scope.UserAssessmentActivityDetail.IsBookMarked = updateValue;
                break;
            case Enum.AssessmentActivityType.ShowHint:
                $scope.UserAssessmentActivityDetail.HintsUsed = $scope.UserAssessmentActivityDetail.HintsUsed + 1;
                break;
            case Enum.AssessmentActivityType.NotLearned:
                $scope.UserAssessmentActivityDetail.NotLearned = updateValue;
                break;
            case Enum.AssessmentActivityType.NoOfTry:
                $scope.UserAssessmentActivityDetail.NoOfRetry = $scope.UserAssessmentActivityDetail.NoOfRetry + 1;
                break;
            case Enum.AssessmentActivityType.ReportAProblem:
                $scope.UserAssessmentActivityDetail.ReportAProblem = updateValue;
                break;
            default:
                break;
        }
        $scope.UserAssessmentActivityDetail.QuizQuestID = $scope.CurrentQuestion.QuestionId;
        $scope.UserAssessmentActivityDetail.UserAssessmentActivityType = activityType;

        $scope.SetQuestionMapSetting(updateValue, activityType);
    };

    $scope.SetQuestionMapSetting = function (updateValue, activityType) {
        angular.forEach($scope.AssignmentQuestions, function (question, key) {
            if ($scope.CurrentQuestion.QuestionId == question.QuizQuestID) {
                switch (activityType) {
                    case Enum.AssessmentActivityType.BookMark:
                        question.IsBookMarked = updateValue;
                        if (question.QuestionStatus == 'not-visited')
                            question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.ShowHint:
                        question.HintsUsed = $scope.UserAssessmentActivityDetail.HintsUsed + 1;
                        if (question.QuestionStatus == 'not-visited')
                            question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.NotLearned:
                        question.NotLearned = updateValue;
                        if (question.QuestionStatus == 'not-visited')
                            question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.NoOfTry:
                        question.NoOfRetry = $scope.UserAssessmentActivityDetail.NoOfRetry + 1;
                        if (question.QuestionStatus == 'not-visited')
                            question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.SubmissionStatus:
                        question.QuestionAttempted = true;
                        question.QuestionStatus = 'answer-right';
                        break;
                    default:
                        break;
                }
                return;
            }
        });
    };

    $scope.ShowFilterQuestions = function (filterType) {
        switch (filterType) {
            case Enum.QuestionFilterType.NotVisited:
                $scope.searchFilter = { QuestionStatus: 'not-visited' };
                break;
            case Enum.QuestionFilterType.VisitedNotAnswered:
                $scope.searchFilter = { QuestionStatus: 'answer-visited' };
                break;
            case Enum.QuestionFilterType.Completed:
                $scope.searchFilter = { QuestionStatus: 'answer-right' };
                break;
            case Enum.QuestionFilterType.IsBookMarked:
                $scope.searchFilter = { IsBookMarked: true };
                break;
            case Enum.QuestionFilterType.ViewAll:
                if ($scope.searchFilter.QuestionStatus != "") {
                    $scope.searchFilter = { QuestionStatus: '' };
                    $('#toggleTest').trigger('click');
                }
                break;
            case Enum.QuestionFilterType.ViewAttended:
                if ($scope.searchFilter.QuestionStatus != "not-visited") {
                    $scope.searchFilter = { QuestionStatus: 'not-visited' };
                    $('#toggleTest').trigger('click');
                }
                break;
            default:
                $scope.searchFilter = { QuestionStatus: '' };
                break;
        }

        if (filterType == Enum.QuestionFilterType.IsBookMarked) {
            $scope.QuestionMapBookMarkClicked = true;
        }
        else {
            $scope.QuestionMapBookMarkClicked = false;
        }
    };

    $(window).resize(function () {
        if ($(window).width() <= 767)
            $('.popover').hide();
        else
            $('.popover').show(); resize_fun();
    });

    var popovermodel;
    $scope.GetFeedback = function (model) {
        var feedbackText = '';
        var currentClass = '';
        popovermodel = '';
        var questTypeId = $scope.questionTypeId;
        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            questTypeId = $scope.CurrentQuestion.QuestionData.questions[model.QuestionIndex].question_type.text;
        }
        $('body').popover("destroy");
        popovermodel = model;
        if (questTypeId == Enum.QuestionType.TextSelection || questTypeId == Enum.QuestionType.Matching || questTypeId == Enum.QuestionType.LTDDND || questTypeId == Enum.QuestionType.LTD || questTypeId == Enum.QuestionType.FIBDND) {
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {

                    if (questTypeId == Enum.QuestionType.Matching && popovermodel.interaction.indexOf("container") > -1) {
                        if (popovermodel.AnswerClass.indexOf("incorrect") > -1) {
                            feedbackText = popovermodel.incorrect_feedback;
                        }
                        else {
                            feedbackText = popovermodel.correct_feedback;
                        }
                    }
                    else {
                        if (popovermodel.AnswerClass.indexOf("incorrect") > -1) {
                            feedbackText = popovermodel.incorrect_feedback;
                        }
                        else {
                            feedbackText = popovermodel.correct_feedback;
                        }
                    }
                    if ($('.fdback-mobile').is(':visible')) {
                        if (questTypeId == Enum.QuestionType.LTDDND || questTypeId == Enum.QuestionType.Matching) {
                            if ($scope.questionTypeId == Enum.QuestionType.Composite) {
                                $('#divltd' + model.Index + model.key).html('<span>' + (model.key + 1) + ". " + feedbackText + '</span>');
                                $('#divltd' + model.Index + model.key).show();
                            }
                            else {
                                $('#divltd' + model.key).html('<span>' + (model.key + 1) + ". " + feedbackText + '</span>');
                                $('#divltd' + model.key).show();
                            }
                        }
                        else {
                            if ($scope.questionTypeId == Enum.QuestionType.Composite) {
                                $('#divltd' + model.Index + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                                $('#divltd' + model.Index + model.choiceid).show();
                            }
                            else {
                                $('#divltd' + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                                $('#divltd' + model.choiceid).show();
                            }

                        }
                    }
                    else {
                        return feedbackText;
                    }
                },//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                placement: "top"
            });
        }
        else if (questTypeId == Enum.QuestionType.ChoiceMatrix) {
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {
                    if (popovermodel.suboption[popovermodel.suboptionSelected - 1].currentClass.indexOf("incorrect") > -1) {
                        feedbackText = popovermodel.incorrect_feedback;
                    }
                    else {
                        feedbackText = popovermodel.correct_feedback;
                    }
                    return feedbackText;
                },
                placement: "top"
            });
        }
        else if (questTypeId == Enum.QuestionType.MCSS || questTypeId == Enum.QuestionType.MCMS || questTypeId == Enum.QuestionType.Order || questTypeId == Enum.QuestionType.HotSpot) {
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {

                    if (popovermodel.currentClass.indexOf("incorrect") > -1) {
                        feedbackText = popovermodel.incorrect_feedback;
                    }
                    else {
                        feedbackText = popovermodel.correct_feedback;
                    }
                    if ($('.fdback-mobile').is(':visible')) {
                        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
                            $('#divltd' + model.Index + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                            $('#divltd' + model.Index + model.choiceid).show();
                        }
                        else {
                            $('#divltd' + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                            $('#divltd' + model.choiceid).show();
                        }
                    }
                    else {
                        return feedbackText;
                    }
                },//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                placement: "top"
            });

        }

    }


    $scope.showHideOptionFeedbackIcon = function () {
        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            $.each($scope.CurrentQuestion.QuestionData.questions, function (index, question) {
                var selectedOptions = angular.fromJson($scope.FindUserSelectedOptions(question));
                $.each(question.choices, function (index, value) {
                    $(selectedOptions).each(function (sindex, soption) {
                        if (soption["choiceid"] != undefined && soption["choiceid"] != "") {
                            if (value.choiceid == soption["choiceid"]) {
                                $scope.showHideOptionFeedbackIconInternal(value, question);
                            }
                        } else if (soption["inputvalue"] != undefined && soption["inputvalue"] != "") {
                            if (value.choiceid == soption["inputvalue"].split('-')[0]) {
                                $scope.showHideOptionFeedbackIconInternal(value, question);
                            }
                        }
                    });
                });
            });
        } else if ($scope.questionTypeId != Enum.QuestionType.Composite) {
            var selectedOptions = angular.fromJson($scope.userSelectedOptions());
            $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
                $(selectedOptions).each(function (sindex, soption) {
                    //if (value != null && value != "") {
                    if (soption["choiceid"] != undefined && soption["choiceid"] != "") {
                        if (value.choiceid == soption["choiceid"]) {
                            $scope.showHideOptionFeedbackIconInternal(value, $scope.CurrentQuestion.QuestionData);
                        }
                    } else if (soption["inputvalue"] != undefined && soption["inputvalue"] != "") {
                        if (value.choiceid == soption["inputvalue"].split('-')[0]) {
                            $scope.showHideOptionFeedbackIconInternal(value, $scope.CurrentQuestion.QuestionData);
                        }
                    }
                });
            });
        }
    };

    $scope.showHideOptionFeedbackIconInternal = function (value, question) {
        if (!$scope.questionTypeId == Enum.QuestionType.Composite) {
            if ((question.question_type.text == Enum.QuestionType.HotSpot || question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND || question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND) &&
                value.correct_feedback != "" && value.incorrect_feedback != "") {
                value.showOptionFeedbackModal = true;
                value.showAttempt = true;
                $('.fdback').removeClass('ng-hide');
            }
            else if (value.correct_feedback != "" && value.incorrect_feedback != "") {
                value.showOptionFeedbackModal = true;
                value.showAttempt = true;
                $('.fdback').removeClass('ng-hide');
            }
            else {
                value.showOptionFeedbackModal = false;
                value.showAttempt = false;
                $('.fdback').addClass('ng-hide');
            }
        }

            //else if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            //    value.showOptionFeedbackModal = true;
            //    if (value.correct_feedback != "" && value.incorrect_feedback != "") {
            //        value.displayfeedback = true;
            //    }
            //    else
            //        value.displayfeedback = true;
            //    $('.fdback').removeClass('ng-hide');
            //}


        else if (!$scope.showOptionFeedbackModal) {
            if (($scope.questionTypeId == Enum.QuestionType.TextSelection || $scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND || $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND) &&
                value.correct_feedback != "" && value.incorrect_feedback != "" && $scope.isLastquestion && jQuery.isEmptyObject($scope.AssignmentSetting)) {
                $scope.showOptionFeedbackModal = true;
                $('.fdback').removeClass('ng-hide');
            }
            else if (value.correct_feedback != "" && value.incorrect_feedback != "" && $scope.isLastquestion && jQuery.isEmptyObject($scope.AssignmentSetting)) {
                $scope.showOptionFeedbackModal = true;
                $('.fdback').removeClass('ng-hide');
            }

            else {
                $scope.showOptionFeedbackModal = false;
                $('.fdback').addClass('ng-hide');
            }

        }
    };

    $scope.showCorrectAnswer = function () {
        popovermodel = '';
        $scope.CheckAnswer = true;
        $scope.ShowGlobalCorrectFeedback = false;
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.selectedItems = [];
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            if ($scope.userSelectedOptions() != "" && $scope.userSelectedOptions() != "[]") {
                $scope.selectedItems = $scope.userSelectedOptions();
                if ($('#checkAnswerbtn').html() == 'Check Answer') {
                    $('input').attr("disabled", true)
                    $('select').attr("disabled", true)
                    $('#checkAnswerbtn').html('Try Again');
                    $('#checkAnswerbtnmobile').html('Try Again');
                    if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
                        $scope.UpdateUserAssessmentActivity($scope.UserAssessmentActivityDetail.NoOfRetry, Enum.AssessmentActivityType.NoOfTry);
                        $scope.showAttempt = true;
                        if ($scope.questionTypeId == Enum.QuestionType.TextSelection || $scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND && $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND) {
                            $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                                value.showAttempt = true;
                            });
                        }
                        $scope.CorrectAnswerCount = $scope.CorrectAnswerCount + 1;
                        //if ($scope.QuestionAttempt > 1) $('#checkAnswerbtn').html('Try Again');
                        if ($scope.QuestionAttempt == $scope.CorrectAnswerCount) {
                            $scope.DisableCorrectAnswer = true;
                        }
                    }
                    $scope.SingledroppableConfig.disabled = true;
                    $scope.SingledraggableConfig.disabled = true;
                    $scope.SortableFuncConfig.disabled = true;
                }
                else {
                    //Added by Khadir
                    $('input').removeAttr("disabled")
                    $('select').removeAttr("disabled")
                    $scope.SingledroppableConfig.disabled = false;
                    $scope.SingledraggableConfig.disabled = false;
                    $scope.SortableFuncConfig.disabled = false;
                    $scope.userHasSelectedOptions = false;
                    $(".alert").addClass('ng-hide');
                    $('#checkAnswerbtn').html('Check Answer');
                    $('#checkAnswerbtnmobile').html('Check Answer');
                    $scope.CheckAnswer = false;
                    if ($scope.questionTypeId == Enum.QuestionType.Composite) {
                        //$scope.ResetCompositeQuestions($scope.CurrentQuestion.QuestionData.questions);
                        $scope.$broadcast('resetCompositeQuestion');
                        $scope.$apply();
                    }
                    else {
                        $(".option").removeClass('correct incorrect selected');
                        $(".hotspot").removeClass('correct incorrect selected');

                        //$(".option").removeClass('incorrect');
                        //$(".option").removeClass('selected');
                        $(".option input:checked").prop('checked', false);
                        $scope.CurrentQuestion.SelectedOptions = [];
                        $scope.selectedoptions = [];
                        //$scope.UserSelectedArea = [];
                        $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                            if ($scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND || $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || ($scope.questionTypeId == Enum.QuestionType.Matching && $scope.layoutType == 'dragdrop_parallel')) {
                                this.isAnswered = false;
                                this.AnswerClass = "";
                                if (!!value.UserInput) value.UserInput = "";
                                if (!!value.Canvas && value.Canvas.length > 0) {
                                    $(value.Canvas).each(function (cindex, cvalue) {
                                        cvalue.AnswerClass = "";
                                        cvalue.selectedClass = "";
                                        $scope.droppableItems.push(cvalue);
                                    });
                                    value.Canvas = [];
                                }
                                if ($scope.questionTypeId == Enum.QuestionType.FIBDND || ($scope.questionTypeId == Enum.QuestionType.Matching && $scope.layoutType == 'dragdrop_parallel')) {
                                    var selectedValue = $scope.SelectedTemplateArray.filter(function (x) { return x.dropId == index; });
                                    if (selectedValue.length > 0) {
                                        $(selectedValue).each(function (sindex, svalue) {
                                            if (svalue != "") {
                                                if ($scope.droppableItems.filter(function (x) { return $filter('lowercase')(x.title) == $filter('lowercase')('<span math dynamic>' + svalue.title + '</span>'); }).length == 0) {
                                                    $scope.droppableItems.push({ 'title': svalue.title, 'drag': true, selectedClass: '' });
                                                }
                                                svalue.dropId = "initial";
                                            }
                                        });
                                    }
                                }
                            }
                            else if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
                                value.isChoiceAnswered = false;
                                $(value.suboption).each(function (sindex, svalue) {
                                    svalue.currentClass = "";
                                    svalue.isAnswered = "";
                                    svalue.isOptionAnswered = false;
                                    value.suboptionSelected = "";
                                });
                            }
                            else if ($scope.questionTypeId == Enum.QuestionType.TextSelection) {
                                this.AnswerClass = "";
                                $scope.NoOfSelection = 0;
                                $('#divltd' + value.choiceid).html('');
                                $('#divltd' + value.choiceid).hide();
                            }
                            else if ($scope.questionTypeId == Enum.QuestionType.HotSpot || $scope.questionTypeId == Enum.QuestionType.Order) {
                                $scope.UserSelectedArea = [];
                                //$('#mediaImage').mapster("set_options", { fillOpacity: 0.4, fillColor: "d42e16", stroke: true, strokeColor: "3320FF", strokeOpacity: 0.8, strokeWidth: 4, clickNavigate: true, highlight: true, isSelectable: true, singleSelect: false, mapKey: 'data-state' });
                                $('#divltd' + value.choiceid).html('');
                                $('#divltd' + value.choiceid).hide();
                                this.currentClass = "";
                                $scope.NoOfSelection = 0;
                                value.isAnswered = false;
                            }
                            else {
                                this.isAnswered = false;
                                this.currentClass = "";
                            }
                            if ($scope.questionTypeId == Enum.QuestionType.FIBDND || ($scope.questionTypeId == Enum.QuestionType.Matching && $scope.layoutType == 'dragdrop_parallel')) {
                                $("div[data-drop-id='" + index + "']").empty();
                                value.accept = true;
                            }
                            if ($scope.questionTypeId == Enum.QuestionType.Matching || $scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.TextSelection || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.LTDDND) {
                                $('#divltd' + value.key).html('');
                                $('#divltd' + value.key).hide();
                                this.currentClass = "";
                                value.isAnswered = false;
                            }
                        });
                        if ($scope.questionTypeId == Enum.QuestionType.FIBDND || ($scope.questionTypeId == Enum.QuestionType.Matching && $scope.layoutType == 'dragdrop_parallel')) {
                            $scope.droppableItems = [];
                            angular.forEach($scope.SelectedTemplateArray, function (option, key) {
                                $scope.droppableItems.push({ 'title': option.title, 'drag': true, selectedClass: '' });
                            });
                        }

                        if ($scope.questionTypeId == Enum.QuestionType.Order) {
                            $scope.CurrentQuestion.QuestionData.choices.sort(function (a, b) {
                                if (a.CurrentOptionIndex < b.CurrentOptionIndex) return -1;
                                if (a.CurrentOptionIndex > b.CurrentOptionIndex) return 1;
                                return 0;
                            })
                        }

                        //$scope.showHint = false;
                        //$scope.UserAssessmentActivityDetail.HintsUsed = 0;
                        //$scope.hintCount = $scope.CurrentQuestion.QuestionData.hints.length;
                        //$scope.visibleHints = [];

                        //angular.forEach($scope.AssignmentQuestions, function (question, key) {
                        //    if ($scope.CurrentQuestion.QuestionId == question.QuizQuestID) {
                        //        question.HintsUsed = 0;
                        //    }
                        //});
                        $('span[id^="spnltd"]').each(function () {
                            $('#' + this.id).popover("destroy");
                        });

                        return;
                    }
                }
            }
            if ($scope.questionTypeId == Enum.QuestionType.Composite) {
                $scope.CheckCompositeAnswers($scope.CurrentQuestion.QuestionData.questions);
                //return;
            }
            $scope.showHideOptionFeedbackIcon();
            if ($scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND || $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.Matching) {
                $scope.checkCorrectTextEntered();
               // $scope.userHasSelectedOptions = true;
            }
            else if ($scope.questionTypeId == Enum.QuestionType.HotSpot) {
                var correctoptions = $scope.CurrentQuestion.QuestionData.correct_answer.split(",");
                $scope.selectedoptions = angular.fromJson($scope.selectedItems);
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                    var answerFound = false;

                    if (this.isAnswered == true) {
                        $(correctoptions).each(function (index, cvalue) {
                            if (cvalue == value.choiceid) {
                                answerFound = true;
                            }
                        });

                        if (answerFound) {
                            value.isCorrect = true;
                            value.currentClass = "correct";
                            if (value.correct_feedback != '') {
                                value.displayfeedback = true;
                            }
                        }
                        else {
                            value.isCorrect = false;
                            value.currentClass = "incorrect";
                            $scope.ShowGlobalIncorrectFeedback = true;
                            if (value.incorrect_feedback != '') {
                                value.displayfeedback = true;
                            }
                        }
                    }
                    else {
                        value.currentClass = "";
                    }
                });
                if ($scope.ShowGlobalIncorrectFeedback) {
                    $scope.ShowGlobalFeebackData("incorrect");
                }
                else {
                    $scope.ShowGlobalFeebackData("correct");
                }
            }
            else if ($scope.questionTypeId == Enum.QuestionType.Order) {
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                    var correctoptions = $scope.CurrentQuestion.QuestionData.correct_answer.split(",");
                    var optionindex = value.choiceid;
                    if (parseInt(optionindex) == correctoptions[index]) {
                        value.isCorrect = true;
                        value.currentClass = "correct";
                        if (value.correct_feedback != '') {
                            value.displayfeedback = true;
                        }
                        // $scope.ShowGlobalFeebackData(value.currentClass);
                    }
                    else {
                        value.isCorrect = false;
                        value.currentClass = "incorrect";
                        if (value.incorrect_feedback != '') {
                            value.displayfeedback = true;
                        }
                        $scope.ShowGlobalIncorrectFeedback = true;
                        //$scope.ShowGlobalFeebackData(value.currentClass);
                    }
                });

                if ($scope.ShowGlobalIncorrectFeedback) {
                    $scope.ShowGlobalFeebackData("incorrect");
                }
                else {
                    $scope.ShowGlobalFeebackData("correct");
                }
            }
            else if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
                $scope.selectedoptions = angular.fromJson($scope.selectedItems);

                var currentAccess;
                if (parseInt($scope.selectedOption) == 1) {
                    currentAccess = true;
                }
                else {
                    currentAccess = false;
                }

                $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                    var optionindex = index + 1;
                    $(value.suboption).each(function (sindex, soption) {
                        if (value.suboptionSelected == soption.identifier) {
                            if (value.isAnswered.toString() == soption.isAnswered.toString()) {
                                $scope.CurrentQuestion.QuestionData.choices[index].suboption[sindex].currentClass = "correct";
                                if (value.correct_feedback != '') {
                                    value.displayfeedback = true;
                                }
                            } else {
                                $scope.CurrentQuestion.QuestionData.choices[index].suboption[sindex].currentClass = "incorrect";
                                if (value.incorrect_feedback != '') {
                                    value.displayfeedback = true;
                                }
                                $scope.ShowGlobalIncorrectFeedback = true;
                            }
                        }

                    });
                });

                if ($scope.selectedoptions.length > 0) {
                    if (!$scope.ShowGlobalIncorrectFeedback && ($scope.selectedoptions.length == $scope.CurrentQuestion.QuestionData.choices.length)) {
                        $scope.ShowGlobalFeebackData("correct");
                    }
                    else {
                        $scope.ShowGlobalFeebackData("incorrect");
                    }
                }
            }
            else if ($scope.questionTypeId == Enum.QuestionType.TextSelection) {
                $scope.CheckTextSelectionTypeAnswer($scope.CurrentQuestion);
                if ($scope.ShowGlobalIncorrectFeedback) {
                    $scope.ShowGlobalFeebackData("incorrect");
                }
                else {
                    $scope.ShowGlobalFeebackData("correct");
                }
            }
            else {
                if ($scope.questionTypeId != Enum.QuestionType.Composite) {
                    $scope.correctoptions = $scope.CurrentQuestion.QuestionData.correct_answer.split(",");
                    $scope.selectedoptions = angular.fromJson($scope.selectedItems).map(function (x) { return x.choiceid.toString(); });

                    $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                        var optionindex = index + 1;
                        var answerFound = false;

                        if (this.isAnswered == true) {
                            //if (value.assess == true) {

                            $($scope.correctoptions).each(function (correctindex) {
                                if (parseInt(value.choiceid) == parseInt($scope.correctoptions[correctindex])) {
                                    answerFound = true;
                                }
                            });
                            if (answerFound) {
                                value.isCorrect = true;
                                value.currentClass = "correct";
                                if (value.correct_feedback != '') {
                                    value.displayfeedback = true;
                                }
                                //$scope.ShowGlobalFeebackData(value.currentClass);
                            }
                            else {
                                value.isCorrect = false;
                                value.currentClass = "incorrect";
                                if (value.incorrect_feedback != '') {
                                    value.displayfeedback = true;
                                }
                                // $scope.ShowGlobalFeebackData(value.currentClass);
                                // $scope.ShowGlobalIncorrectFeedback = true;
                            }
                        }
                        else {
                            value.currentClass = "";
                        }

                    });
                    if ($scope.selectedoptions.length > 0) {
                        var globalCorrectFeedback = checkEqualArray($scope.selectedoptions, $scope.correctoptions);
                        if (globalCorrectFeedback) {
                            $scope.ShowGlobalFeebackData("correct");
                        }
                        else {
                            $scope.ShowGlobalFeebackData("incorrect");
                        }
                    }
                }

            }

        }
    };

    $scope.CheckTextSelectionTypeAnswer = function (question) {
        $scope.CorrectAnswer = question.QuestionData.correct_answer.split(',');
        var count = 0;
        $(question.QuestionData.choices).each(function (key, choice) {
            var choiceFound = false;

            if (choice.AnswerClass.indexOf("selectedC") != -1) {
                count++;
                $($scope.CorrectAnswer).each(function (index, anserid) {
                    if (choice.choiceid == anserid) {
                        choiceFound = true;
                    }
                });
                if (choiceFound) {
                    choice.AnswerClass = "selectedC correct";
                    if (choice.correct_feedback != '') {
                        choice.displayfeedback = true;
                    }

                }
                else {
                    choice.AnswerClass = "selectedC incorrect";
                    if (choice.incorrect_feedback != '') {
                        choice.displayfeedback = true;
                    }
                    $scope.ShowGlobalIncorrectFeedback = true;
                }
            }

        });
        // show global feebbackCorrect only when all correct is options are selected
        if ($scope.CorrectAnswer.length == count && $scope.ShowGlobalIncorrectFeedback != true)
            $scope.ShowGlobalIncorrectFeedback = false;
        else
        { $scope.ShowGlobalIncorrectFeedback = true; }
    };
    //************************************Composite question code starts****************************/
    //*****************Reset Try Again *******************//
    $scope.ResetCompositeQuestions = function (questions) {
        $.each(questions, function (index, question) {

            question.globalCorrectFeedback = "";
            question.ShowGlobalCorrectFeedback = false;
            question.globalInCorrectFeedback = "";
            question.ShowGlobalIncorrectFeedback = false;

            $('.fdback').addClass('ng-hide');

            $(question.choices).each(function (opIndex, value) {
                value.showOptionFeedbackModal = false;
                value.showAttempt = false;

                if (question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
                    this.isAnswered = false;
                    this.AnswerClass = "";
                    if (!!value.UserInput) value.UserInput = "";
                    if (!!value.Canvas && value.Canvas.length > 0) {
                        $(value.Canvas).each(function (cindex, cvalue) {
                            cvalue.AnswerClass = "";
                            cvalue.selectedClass = "";
                            question.droppableItems.push(cvalue);
                        });
                        value.Canvas = [];
                    }
                }
                else if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
                    value.isChoiceAnswered = false;
                    $(value.suboption).each(function (sindex, svalue) {
                        svalue.currentClass = "";
                        svalue.isAnswered = "";
                        svalue.isOptionAnswered = false;
                        value.suboptionSelected = "";
                    });
                } else if (question.question_type.text == Enum.QuestionType.Order) {
                    value.isCorrect = false;
                    value.currentClass = "";
                } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
                    value.AnswerClass = "";

                } else {
                    this.isAnswered = false;
                    this.currentClass = "";
                }
            });
            if (question.question_type.text == Enum.QuestionType.Order) {
                question.choices.sort(function (a, b) {
                    if (a.CurrentOptionIndex < b.CurrentOptionIndex) return -1;
                    if (a.CurrentOptionIndex > b.CurrentOptionIndex) return 1;
                    return 0;
                })
                $(".odering-panel").removeClass("incorrect correct");
            } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
                question.NoOfSelection = 0;
            }
        });
    };
    //****************End of Try again *******************//

    //****************************************Composite question Feedback***********************
    $scope.SetCompositeType = function (compositeQuestion, compositeIndex) {
        $.each(compositeQuestion.QuestionData.questions, function (qindex, question) {
            question.Index = qindex;
            var selectedOptions = '';
            if (compositeQuestion.SelectedOptions != null && !$scope.isUndefined(compositeQuestion.SelectedOptions)) {
                selectedOptions = compositeQuestion.SelectedOptions[qindex].UserSelectedOptions;
            }

            if (!$scope.isUndefined(question.stem_details) && question.stem_details.type == 'algorithimic') {
                $scope.SetCompositeFeedbackAlgorithimicType(question);
            }

            if (question.question_type.text == Enum.QuestionType.MCSS || question.question_type.text == Enum.QuestionType.MCMS) {
                $scope.SetFeedbackForCompositeMCSSMCMS(question, selectedOptions);
            }
            else if (question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
                $scope.SetFeedbackForCompositeFIBDNDMatching(question, selectedOptions);
            }
            else if (question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND) {
                $scope.SetFeedbackForCompositeLTDDND(question, selectedOptions);
            }
            else if (question.question_type.text == Enum.QuestionType.Order) {
                $scope.SetFeedbackForCompositeOrder(question, selectedOptions);
            }
            else if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
                $scope.SetFeedbackForCompositeChoiceMatrix(question, selectedOptions);
            }
                //ACECBE-1029
            else if (question.question_type.text == Enum.QuestionType.HotSpot) {
                $scope.SetCompositeFeedbackHotSpot(question, selectedOptions);
            }

        });
    }

    $scope.SetCompositeFeedbackHotSpot = function (question, selectedOptions) {
        if (!$scope.isUndefined(question.question_media.media) && question.question_media.media.length > 0 && question.question_media.media != "") {
            //$scope.FeedBack[index].ImageSrc = regex.exec($scope.FeedBack[index].QuestionData.question_media.media)[1];
            var imgObject = document.createElement('div');
            imgObject.innerHTML = question.question_media.media;
            question.ImageSrc = $(imgObject).find('img').attr('src');
        }

        angular.forEach(question.choices, function (option, key) {
            option.XPosition = '';
            option.YPosition = '';
            option.isAnswered = false;
            // $scope.QuestionOptions[key].opacity = 0;

            var positions = option.text.split(',');
            if (positions.length == 2) {
                option.XPosition = positions[0];
                option.YPosition = positions[1];
            }
        });

        if (selectedOptions) {
            var correctoptions = question.correct_answer.split(",");
            var selectedoptions = $.parseJSON(selectedOptions);
            question.SelectedOptions = selectedoptions.map(function (x) { return x.choiceid; }).toString();
            //var abc = selectedoptions.map(function (x) { return x.choiceid; }).toString();



            $(question.choices).each(function (cindex, value) {
                $(selectedoptions).each(function (selectindex, svalue) {
                    if (value.choiceid == parseInt(svalue.choiceid)) {
                        if (correctoptions.indexOf(value.choiceid.toString()) != -1) {
                            value.isCorrect = true;
                            value.currentClass = "correct";
                        }
                        else {
                            value.isCorrect = false;
                            value.currentClass = "incorrect";
                        }
                    }
                });

            });
        }
    }

    $scope.SetCompositeFeedbackAlgorithimicType = function (question) {
        if (!$scope.isUndefined(question.AlgorithmicVariableArray) && question.stem_details.textforeachvalue.length > 0) {
            angular.forEach(question.stem_details.textforeachvalue, function (textDetail, key) {
                angular.forEach(question.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    question.question_stem.text = question.question_stem.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                });
            });
        }
    }

    $scope.SetFeedbackForCompositeMCSSMCMS = function (question, selectedOptions) {
        if (!$scope.isUndefined(question.exhibit) && question.exhibit.length > 0 && question.exhibit[0].exhibit_type != "") {
            question.showExhibit = true;
            if (question.exhibit[0].exhibit_type == 'image') {
                var imagePath = String(question.exhibit[0].path);
                question.exhibit[0].path = imagePath;
            }
        }
        else {
            question.showExhibit = false;
        }
        $(question.choices).each(function (indexOpt, valueOpt) {
            var correctoptions = question.correct_answer.split(",");
            //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].CorrectOptions = correctoptions.toString();
            question.CorrectOptions = correctoptions.toString();
            var sSrchTxt = String(valueOpt.text);
            if (sSrchTxt.indexOf('<img') != -1) {
                var options = String(valueOpt.text).split('<img');
                question.choices[indexOpt].text = options[0];
                question.choices[indexOpt].MCSSImage = "<img" + options[1];
                question.choices[indexOpt].text = options[0];
                question.choices[indexOpt].MCSSImage = "<img" + options[1];
            }
            else {
                //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].choices[indexOpt].text = sSrchTxt;
                //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].choices[indexOpt].MCSSImage = null;
                question.choices[indexOpt].text = sSrchTxt;
                question.choices[indexOpt].MCSSImage = null;
            }

            if (this.isAnswered == true) {
                $(correctoptions).each(function (correctindex) {
                    if (parseInt(valueOpt.choiceid) != parseInt(correctoptions[correctindex])) {
                        valueOpt.isCorrect = false;
                        valueOpt.currentClass = "incorrect";
                        if (valueOpt.incorrect_feedback != '') {
                            valueOpt.displayfeedback = true;
                        }
                    }
                });
            }

            $(correctoptions).each(function (correctindex) {
                if (parseInt(valueOpt.choiceid) == parseInt(correctoptions[correctindex])) {
                    valueOpt.isCorrect = true;
                    valueOpt.currentClass = "correct";
                    if (valueOpt.correct_feedback != '') {
                        valueOpt.displayfeedback = true;
                    }

                }
            });
        });
    }

    $scope.SetFeedbackForCompositeOrder = function (question, selectedOptions) {
        $(question.choices).each(function (indexOpt, valueOpt) {
            var correctoptions = question.correct_answer.split(",");
            var optionindex = valueOpt.choiceid;
            if (parseInt(optionindex) == correctoptions[indexOpt]) {
                valueOpt.isCorrect = true;
            }
            else {
                valueOpt.isCorrect = false;
            }
            if (selectedOptions) {
                var userSelected = $.parseJSON(selectedOptions);
                //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].SelectedOptions = userSelected[0].selectedOrder;
                question.SelectedOptions = userSelected[0].selectedOrder;
            }
            //if (question.question_interaction.text == "media" && question.question_layout != null)
            //    $scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].LayoutType = question.question_layout.text;
        });
    }

    $scope.SetFeedbackForCompositeChoiceMatrix = function (question, selectedOptions) {
        $(question.choices).each(function (indexOpt, valueOpt) {
            if (selectedOptions) {
                var selectedOpt = angular.fromJson(selectedOptions);
                $(selectedOpt).each(function (selectindex, selectvalue) {
                    var inputValues = selectvalue.inputvalue.split("-");
                    var optionindex = indexOpt + 1;
                    $(valueOpt.suboption).each(function (index1, value1) {
                        if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                            valueOpt.suboption[index1].isAnswered = true;
                            var assess;
                            if (parseInt(inputValues[1]) == 1) {
                                assess = true;
                            }
                            else {
                                assess = false;
                            }
                            if (valueOpt.assess === assess) {
                                valueOpt.suboption[index1].isCorrect = true;
                            }
                            else {
                                valueOpt.suboption[index1].isCorrect = false;
                            }
                        }
                        else {
                            var val = (value1.value === "true");
                            if (valueOpt.assess === val) {
                                valueOpt.suboption[index1].isCorrect = true;
                            }
                        }
                    });
                });

            }

        });
    }

    $scope.SetFeedbackForCompositeFIBDNDMatching = function (question, selectedOptions) {
        question.leftvertical = "";
        question.rightvertical = "";
        question.draggableItems = [];
        var questiontext = '';
        if (!$scope.isUndefined(question.textwithblanks)) {
            questiontext = question.textwithblanks.text;
        }

        $scope.FillFeedbackCompositeDroppableItems(question);

        angular.forEach(question.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            var htmlText = "";
            var answerClass = "";
            var inputValue = "";

            if (selectedOptions) {
                var selectedOption = angular.fromJson(selectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if (value.noforeachblank == option.noforeachblank && value.inputvalue != "") {
                        inputValue = value.inputvalue;
                        answerClass = (value.isCorrectAnswer) ? "correct" : "incorrect";
                    }
                });
            }
            if (option.interaction == 'textentry') {
                htmlText = "<span class='option " + answerClass + "'><input type='text' class='form-control' disabled value='" + inputValue + "'><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
            }
            else if (option.interaction == 'inline') {
                htmlText = "<span class='option " + answerClass + "'><select class='form-control' disabled><option>" + inputValue + "</option></select><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
            }
            else if (option.interaction == 'dragdrop' || option.interaction == 'container') {
                question.LayoutType = question.question_layout.text;
            }

            if (option.interaction == 'dragdrop') {
                angular.forEach(question.draggableItems, function (ditem, dindex) {
                    if ($filter('lowercase')(inputValue) == $filter('lowercase')(ditem.title)) {
                        ditem.title = "__________";
                    }
                });
            }
            else if (option.interaction == 'container') {
                var input = !!inputValue ? inputValue.map(function (x) { return x.title; }) : [];
                angular.forEach(question.draggableItems, function (ditem, dindex) {
                    if (input.indexOf(ditem.title) != -1) {
                        ditem.title = "__________";
                    }
                });

                var isOptionIncorrect = false;
                var isOptionAnswered = false;
                if (option.Canvas != undefined && option.Canvas != null) {
                    for (var i = 0; i < option.Canvas.length; i++) {
                        isOptionAnswered = true;
                        if (option.Canvas[i].AnswerClass == "incorrect") {
                            isOptionIncorrect = true;
                        }
                    }
                }
                if (isOptionAnswered) {
                    option.AnswerClass = isOptionIncorrect ? "incorrect" : "correct";
                }
            }

            if (question.question_type.text != Enum.QuestionType.LTD && question.question_type.text != Enum.QuestionType.LTDDND && question.question_layout.text != '' && question.question_layout.text != "dragdrop_parallel") {
                htmlText = "<span class='option drop " + answerClass + "'><span class='option drag'>" + inputValue + "</span><span class='sign'><span class='fa'></span></span></span>";
                if (question.question_layout.text === "right" || question.question_layout.text === "left" || question.question_layout.text === "vertical") {
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].leftvertical = "col-md-10";
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].rightvertical = "col-md-2 right";
                    question.leftvertical = "col-md-10";
                    question.rightvertical = "col-md-2 right";
                }
                else if (question.question_layout.text === "row") {
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].leftvertical = "";
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].rightvertical = "text-center";
                    question.leftvertical = "";
                    question.rightvertical = "text-center";
                }

                question.choices[key].isCorrectAnswer = answerClass;
                question.choices[key].selectedAnswer = inputValue;
            }
            else {
                question.choices[key].isCorrectAnswer = answerClass;
                question.choices[key].selectedAnswer = inputValue;
            }
            questiontext = questiontext.replace(textToReplace, htmlText);

        });
        question.FIBText = questiontext;
    };

    $scope.SetFeedbackForCompositeLTDDND = function (question, selectedOptions) {

        question.leftvertical = "";
        question.rightvertical = "";
        question.draggableItems = [];

        if (question.question_media != undefined && question.question_media.media.length > 0 && question.question_media.media != "") {
            var imgObject = document.createElement('div');
            imgObject.innerHTML = question.question_media.media;
            question.ImageSrc = $(imgObject).find('img').attr('src');
        }

        $scope.FillFeedbackCompositeDroppableItems(question);

        angular.forEach(question.choices, function (option, key) {
            var inputValue = "";
            if (selectedOptions) {
                var selectedOption = angular.fromJson(selectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if (value.noforeachblank == option.noforeachblank && value.inputvalue != "") {
                        option.LTDText = value.inputvalue;
                        inputValue = value.inputvalue;
                        option.AnswerClass = (value.isCorrectAnswer) ? "correct" : "incorrect";
                    }
                });
            }

            if (option.interaction == 'dragdrop') {
                question.LayoutType = question.question_layout.text;

                if (option.interaction == 'dragdrop') {
                    angular.forEach(question.draggableItems, function (ditem, dindex) {
                        if ($filter('lowercase')(inputValue) == $filter('lowercase')('<span math dynamic>' + ditem.title + '</span>')) {
                            ditem.title = "__________";
                        }
                    });
                }

                if (question.question_layout.text === "right" || question.question_layout.text === "left" || question.question_layout.text === "vertical") {
                    question.leftvertical = "col-md-10";
                    question.rightvertical = "col-md-2 right";
                }
                else if (question.question_layout.text === "row") {
                    question.leftvertical = "";
                    question.rightvertical = "text-center";
                }
            }
        });
    }

    $scope.FillFeedbackCompositeDroppableItems = function (question) {
        var isDistractorsPushDone = false;
        angular.forEach(question.choices, function (option, key) {
            if (isDistractorsPushDone == false) {
                angular.forEach(option.distractorforeachblank, function (titem, tindex) {
                    question.draggableItems.push({ 'title': titem.text, 'drag': false });
                });
                isDistractorsPushDone = true;
            }

            angular.forEach(option.textforeachblank, function (titem, tindex) {
                question.draggableItems.push({ 'title': titem.text, 'drag': false });
            });
        });
    };

    //************************************************** End of Composite Question feedback***************************************

    //*********************************Save Answer for composite type **********************************

    $scope.GetUserSelectedOptionForCompositeType = function (questions) {
        var selectedItemsForComposite = [];
        $(questions).each(function (index, question) {
            selectedItemsForComposite.push({ 'Index': index, 'UserSelectedOptions': $scope.FindUserSelectedOptions(question) });
        });
        return selectedItemsForComposite;
    }

    //*********************************End of save answer for composite type ***************************

    //*********************************Check Answer for Composite type question*************************

    $scope.CheckCompositeAnswers = function (questions) {
        $scope.$broadcast('enableDisableDragDropComposite', true);
        $(questions).each(function (index, question) {
            question.ShowGlobalFeedback = true;
            question.ShowGlobalCorrectFeedback = false;
            question.ShowGlobalIncorrectFeedback = false;
            question.globalCorrectFeedback = '';
            question.globalInCorrectFeedback = '';
            if (question.question_type.text == Enum.QuestionType.MCSS || question.question_type.text == Enum.QuestionType.MCMS) {
                $scope.CheckMCSSAndMCMSAnswer(question);
            }
            else if (question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND || question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
                $scope.CheckDNDAnswers(question);
            }
            else if (question.question_type.text == Enum.QuestionType.Order) {
                $scope.CheckOrderTypeAnswer(question);
            }
            else if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
                $scope.CheckChoiceMatrixTypeAnswer(question);
            } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
                $scope.CheckCompositeTextSelectionTypeAnswer(question);
            } else if (question.question_type.text == Enum.QuestionType.HotSpot) {
                $scope.checkCompositeHotspotAnswer(question);
            }
        });
    };

    $scope.checkCompositeHotspotAnswer = function (question) {

        var selectedItems = $scope.FindUserSelectedOptions(question);
        question.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
        var correctoptions = question.correct_answer.split(",");
        var selectedoptions = angular.fromJson(selectedItems).map(function (x) { return x.choiceid.toString(); });
        var ansCount = 0;
        $(question.choices).each(function (index, value) {
            var answerFound = false;

            if (this.isAnswered == true) {
                ansCount += 1;
                $(correctoptions).each(function (index, cvalue) {
                    if (cvalue == value.choiceid) {
                        answerFound = true;
                    }
                });

                if (answerFound) {
                    value.isCorrect = true;
                    value.currentClass = "correct";
                    value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                    if (value.correct_feedback != '') {
                        value.displayfeedback = true;
                    }
                }
                else {
                    value.isCorrect = false;
                    value.currentClass = "incorrect";
                    value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                    question.ShowGlobalIncorrectFeedback = true;
                    if (value.incorrect_feedback != '') {
                        value.displayfeedback = true;
                    }
                }
            }
            else {
                value.currentClass = "";
            }
        });
        if (question.ShowGlobalIncorrectFeedback) {
            $scope.ShowGlobalFeebackDataForComposite("incorrect", question);
        }
        else if (ansCount > 0) {
            $scope.ShowGlobalFeebackDataForComposite("correct", question);
        }

    }

    $scope.CheckCompositeTextSelectionTypeAnswer = function (question) {
        var selectedItems = $scope.FindUserSelectedOptions(question);
        var selectedoptions = angular.fromJson(selectedItems);
        if (selectedoptions != undefined && selectedoptions.length > 0) {
            $scope.CorrectAnswer = question.correct_answer.split(',');
            var isincorrect = false;
            $(question.choices).each(function (key, choice) {
                var choiceFound = false;
                if (choice.AnswerClass.indexOf("selectedC") != -1) {
                    $($scope.CorrectAnswer).each(function (index, anserid) {
                        if (choice.choiceid == anserid) {
                            choiceFound = true;
                        }
                    });
                    if (choiceFound) {
                        choice.AnswerClass = "selectedC correct";
                        if (choice.correct_feedback != '') {
                            choice.displayfeedback = true;
                        }
                    }
                    else {
                        choice.AnswerClass = "selectedC incorrect";
                        if (choice.incorrect_feedback != '') {
                            choice.displayfeedback = true;
                        }
                        isincorrect = true;
                    }
                }
            });
            if (isincorrect) {
                $scope.ShowGlobalFeebackDataForComposite('incorrect', question);
            } else {
                $scope.ShowGlobalFeebackDataForComposite('correct', question);
            }
        }
    };

    $scope.CheckChoiceMatrixTypeAnswer = function (question) {
        var selectedItems = $scope.FindUserSelectedOptions(question);
        var selectedoptions = angular.fromJson(selectedItems);
        if (selectedoptions != undefined && selectedoptions.length > 0) {
            var currentAccess;
            var isincorrect = false;
            if (parseInt(selectedoptions) == 1) {
                currentAccess = true;
            }
            else {
                currentAccess = false;
            }
            $(question.choices).each(function (index, value) {
                var optionindex = index + 1;
                value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                $(value.suboption).each(function (sindex, soption) {
                    if (value.suboptionSelected == soption.identifier) {
                        if (value.isAnswered.toString() == soption.isAnswered.toString()) {
                            question.choices[index].suboption[sindex].currentClass = "correct";
                            if (value.correct_feedback != "") {
                                value.displayfeedback = true;
                            }
                            else {
                                value.displayfeedback = false;
                            }
                        } else {
                            question.choices[index].suboption[sindex].currentClass = "incorrect";
                            isincorrect = true;
                            if (value.incorrect_feedback != "") {
                                value.displayfeedback = true;
                            }
                            else {
                                value.displayfeedback = false;
                            }
                        }
                    }

                });
            });
            if (isincorrect) {
                $scope.ShowGlobalFeebackDataForComposite('incorrect', question);
            } else {
                $scope.ShowGlobalFeebackDataForComposite('correct', question);
            }
        }
    }

    $scope.CheckOrderTypeAnswer = function (question) {
        var isincorrect = false;
        var isselected = false;
        $(question.choices).each(function (index, value) {
            if (parseInt(value.choiceid) != index + 1) {
                isselected = true;
            }
        });
        if (isselected) {
            //if (question.userHasSelectedOptions) {
            $(question.choices).each(function (index, value) {
                var correctoptions = question.correct_answer.split(",");
                var optionindex = value.choiceid;
                if (parseInt(optionindex) == correctoptions[index]) {
                    value.isCorrect = true;
                    value.currentClass = "correct";
                    value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                    if (value.correct_feedback != "") {
                        value.displayfeedback = true
                    }
                    else { value.displayfeedback = false; }
                }
                else {
                    value.isCorrect = false;
                    value.currentClass = "incorrect";
                    isincorrect = true;
                    value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                    if (value.incorrect_feedback != "") {
                        value.displayfeedback = true
                    }
                    else { value.displayfeedback = false; }
                }
            });
            //}
            if (isincorrect) {
                $scope.ShowGlobalFeebackDataForComposite('incorrect', question);
            } else {
                $scope.ShowGlobalFeebackDataForComposite('correct', question);
            }
        }
    }

    $scope.CheckDNDAnswers = function (question) {
        var selectedItems = $scope.FindUserSelectedOptions(question);
        var selectedoptions = angular.fromJson(selectedItems);
        var isincorrect = false;
        var isblankoption = false;
        if (selectedoptions != undefined && selectedoptions.length > 0) {
            $(question.choices).each(function (index, value) {
                value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                var inputValue = value.UserInput;
                var textFound = false;
                if (question.question_type.text == Enum.QuestionType.FIBDND) {
                    $(selectedoptions).each(function (sindex, svalue) {
                        if (svalue.noforeachblank == value.noforeachblank) {
                            inputValue = svalue.inputvalue;
                        }
                    });
                }
                if (value.interaction == 'dragdrop' && value.Canvas.length > 0) {
                    inputValue = value.Canvas[0].title;
                }

                if (inputValue.length > 0) {
                    $(value.textforeachblank).each(function (textindex, tvalue) {
                        if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                            if (inputValue.indexOf('<span math dynamic>') == -1) {
                                inputValue = '<span math dynamic>' + inputValue + '</span>';
                            }
                            if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(inputValue)) {
                                textFound = true;
                            }
                        }
                        else {
                            if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(inputValue)) {
                                textFound = true;
                            }
                        }
                    });

                    if (textFound) {
                        value.AnswerClass = "correct";
                        if (value.correct_feedback != '') {
                            value.displayfeedback = true;
                        }
                        else { value.displayfeedback = false; }
                    }
                    else {
                        value.AnswerClass = "incorrect";
                        if (value.incorrect_feedback != "")
                        { value.displayfeedback = true; }
                        else { value.displayfeedback = false; }
                        isincorrect = true;
                    }
                }
                else {
                    if (value.interaction != 'container')
                        isblankoption = true;
                }

                if (value.interaction == 'container') {
                    if (value.Canvas.length > 0) {
                        var textArray = value.textforeachblank.map(function (x) {
                            return $('<span>' + x.text + '</span>').text();
                        });
                        var isOptionIncorrect = false;
                        for (var i = 0; i < value.Canvas.length; i++) {
                            if ($.inArray(value.Canvas[i].title, textArray) > -1) {
                                value.Canvas[i].AnswerClass = "correct";
                                if (value.correct_feedback != '') {
                                    value.displayfeedback = true;
                                }
                                else { value.displayfeedback = false; }
                            }
                            else {
                                value.Canvas[i].AnswerClass = "incorrect";
                                if (value.incorrect_feedback != "")
                                { value.displayfeedback = true; }
                                else { value.displayfeedback = false; }
                                isOptionIncorrect = true;
                                isincorrect = true;
                            }
                        }
                        value.AnswerClass = isOptionIncorrect ? 'incorrect' : 'correct';
                    }
                    else {
                        isblankoption = true;
                    }
                    if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
                        $scope.$apply();
                    }

                    var drpItems = $('#divDropOption' + index + ' li a');
                    $(value.multiSelectItem).each(function (eindex, item) {
                        var itemFound = false;
                        if ($.inArray(item, textArray) > -1) {
                            itemFound = true;
                        }
                        $(drpItems).each(function (eindex, element) {
                            if (item == $(element).text().trim()) {
                                var className = itemFound ? 'correct' : 'incorrect';
                                $(element).parent().addClass(className);
                            }
                        });
                    });
                }
            });
            if (!isincorrect && !isblankoption) {
                $scope.ShowGlobalFeebackDataForComposite('correct', question);
            } else {
                $scope.ShowGlobalFeebackDataForComposite('incorrect', question);
            }
        }
    }

    $scope.CheckMCSSAndMCMSAnswer = function (question) {
        var selectedItems = $scope.FindUserSelectedOptions(question);
        var correctoptions = question.correct_answer.split(",");
        var selectedoptions = angular.fromJson(selectedItems).map(function (x) { return x.choiceid.toString(); });
        if (selectedoptions != undefined && selectedoptions.length > 0) {
            var isincorrect = false;
            $(question.choices).each(function (index, value) {
                var optionindex = index + 1;
                var answerFound = false;

                if (this.isAnswered == true) {
                    $(correctoptions).each(function (correctindex) {
                        if (parseInt(value.choiceid) == parseInt(correctoptions[correctindex])) {
                            answerFound = true;
                        }
                    });
                    if (answerFound) {
                        value.isCorrect = true;
                        value.currentClass = "correct";
                        value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                        if (value.correct_feedback != "") {
                            value.displayfeedback = true;
                        }
                        else {
                            value.displayfeedback = false;
                        }

                    }
                    else {
                        value.isCorrect = false;
                        value.currentClass = "incorrect";
                        isincorrect = true;
                        value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                        if (value.incorrect_feedback != "") {
                            value.displayfeedback = true;
                        }
                        else {
                            value.displayfeedback = false;
                        }
                    }
                }
                else {
                    value.currentClass = "";
                }
            });
            if (isincorrect) {
                $scope.ShowGlobalFeebackDataForComposite('incorrect', question);
            } else {
                $scope.ShowGlobalFeebackDataForComposite('correct', question);
            }
        }
    }

    $scope.FindUserSelectedOptions = function (question) {
        var params = [];
        var str = "";
        if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
            $(question.choices).each(function (index, value) {
                if (value.isChoiceAnswered) {
                    var name = value.choiceid;
                    params.push({ "inputvalue": value.choiceid + '-' + $('input[name=' + name + ']:checked').val(), "isCorrectAnswer": true, "questionTypeId": question.question_type.text });
                }
            });
            return JSON.stringify(params);
        }
        else if (question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND || question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
            $(question.choices).each(function (index, value) {
                var userInputValue = value.UserInput;
                var correctAnswer = false;

                $(value.Canvas).each(function (cindex, cvalue) {
                    cvalue.title = cvalue.title.match(/<span/) ? $(cvalue.title).text() : cvalue.title;
                    userInputValue = cvalue.title;
                });

                if (value.interaction == 'dragdrop' && userInputValue.length == 0) {
                    if (question.question_type.text == Enum.QuestionType.FIBDND) {
                        var selectedValue = question.SelectedTemplateArray.filter(function (x) { return x.dropId == index; }).map(function (x) { return x.title; });
                        userInputValue = selectedValue.length > 0 ? selectedValue[0] : '';
                    }
                    else {
                        userInputValue = (!$scope.isUndefined(value.Canvas) && value.Canvas.length > 0) ? value.Canvas[0].title : "";
                    }
                }

                if (!!userInputValue) {
                    $(value.textforeachblank).each(function (textindex, tvalue) {
                        if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                            if (userInputValue.indexOf('<span math dynamic>') == -1) {
                                userInputValue = '<span math dynamic>' + userInputValue + '</span>';
                            }
                            if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                        else {
                            if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                    });
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    var textArray = value.textforeachblank.map(function (x) { return $('<span>' + x.text + '</span>').text(); });

                    for (var i = 0; i < value.Canvas.length; i++) {
                        if ($.inArray(value.Canvas[i].title, textArray) > -1) {
                            value.Canvas[i].AnswerClass = "correct";
                        }
                        else {
                            value.Canvas[i].AnswerClass = "incorrect";
                        }
                    }
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": value.Canvas, "choiceid": value.choiceid, "questionTypeId": question.question_type.text });
                }
                else if (!!userInputValue) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": userInputValue, "isCorrectAnswer": correctAnswer, "choiceid": value.choiceid, "questionTypeId": question.question_type.text });
                }

            });
            return JSON.stringify(params);
        } else if (question.question_type.text == Enum.QuestionType.Essay) {

            if (question.toolbar.type == "math_3-5_palette" || question.toolbar.type == "math_6-8_palette") {
                // removing the unwanted duplicate cursor from matheditor
                if ($('#mathQuill .cursor').length > 1) {
                    $('#mathQuill .cursor:first').remove();
                }

                if ($('#mathQuill').mathquill('latex') == "{ }") {
                    $('#mathQuill').mathquill('8', 'keyStroke');
                }
                str = $('#mathQuill').mathquill('latex');
            } else {
                // $(question.choices).each(function (index) {
                str = question.EssayText;
                //$scope.essayTemplateText;//$("#txtEssayContent").text();
                //});
            }
            //var html = $(str).text();
            params.push(str);
            return params;
        } else if (question.question_type.text == Enum.QuestionType.Order) {
            var selectedIds = '';
            $(question.choices).each(function (index) {
                if (selectedIds == '') {
                    selectedIds = question.choices[index].choiceid;
                }
                else {
                    selectedIds = selectedIds + "," + question.choices[index].choiceid;
                }
            });
            params.push({ "selectedOrder": selectedIds, "questionTypeId": question.question_type.text });
            return JSON.stringify(params);
        } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
            angular.forEach(question.choices, function (option, key) {
                if (option.AnswerClass != undefined && option.AnswerClass == 'selectedC') {
                    params.push({ "choiceid": option.choiceid });
                }
            });
            return JSON.stringify(params);
        }
        else {
            $(question.choices).each(function (index) {
                if (this.isAnswered == true) {
                    params.push({ "choiceid": this.choiceid, "questionTypeId": question.question_type.text });
                }
            });
        }
        return JSON.stringify(params);
    }

    //*******************End of Composite question answer**************************

    $scope.checkCorrectTextEntered = function () {
        $scope.selectedoptions = angular.fromJson($scope.selectedItems);
        var selectedText = '';
        $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
            var inputValue = value.UserInput;
            var textFound = false;
            if (value.interaction == 'dragdrop') {
                if (value.Canvas.length > 0) {
                    inputValue = value.Canvas[0].title;
                }
                var selectedValue = $scope.SelectedTemplateArray.filter(function (x) { return x.dropId == index; }).map(function (x) { return x.title; });
                if (selectedValue.length > 0) {
                    inputValue = selectedValue[0];
                }
            }

            if (inputValue.length > 0) {
                if (inputValue.indexOf("<span math dynamic>") != -1) {
                    selectedText = inputValue;
                }
                else {
                    selectedText = '<span math dynamic>' + inputValue + '</span>'
                }
                $(value.textforeachblank).each(function (textindex, tvalue) {
                    if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                        if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(selectedText)) {
                            textFound = true;
                        }
                    }
                    else {
                        if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(inputValue)) {
                            textFound = true;
                        }
                    }
                });
                if (textFound) {
                    $scope.QuestionOptions[index].AnswerClass = "correct";
                    if ($scope.QuestionOptions[index].correct_feedback != '') {
                        $scope.QuestionOptions[index].displayfeedback = true;
                        value.isCorrect = true;
                        value.currentClass = "correct";
                    }
                    // $scope.ShowGlobalFeebackData($scope.QuestionOptions[index].AnswerClass);
                }
                else {
                    $scope.QuestionOptions[index].AnswerClass = "incorrect";
                    if ($scope.QuestionOptions[index].incorrect_feedback != '') {
                        $scope.QuestionOptions[index].displayfeedback = true;
                        value.isCorrect = true;
                        value.currentClass = "incorrect";
                    }
                    $scope.ShowGlobalIncorrectFeedback = true;
                    // $scope.ShowGlobalFeebackData($scope.QuestionOptions[index].AnswerClass);
                }

            }
            if ($scope.CurrentQuestion.QuestionData.question_type.text != 'ltd' && $scope.CurrentQuestion.QuestionData.question_type.text != 'ltd_dnd' && value.interaction == 'dragdrop' || ($scope.CurrentQuestion.QuestionData.question_type.text == 'matching' && $scope.CurrentQuestion.QuestionData.question_layout.text != "dragdrop_parallel")) {
                //if ($scope.CurrentQuestion.QuestionData.question_type.text !='ltd' && $scope.CurrentQuestion.QuestionData.question_type.text !='ltd_dnd' &&  $scope.CurrentQuestion.QuestionData.question_layout.text != "dragdrop_parallel" && value.interaction == 'dragdrop') {
                var drpItems = $('#divDropOption' + index + ' li a');
                $($scope.QuestionOptions[index].multiSelectItem).each(function (eindex, item) {
                    var itemFound = false;
                    if ($.inArray(item, textArray) > -1) {
                        itemFound = true;
                    }
                    $(drpItems).each(function (eindex, element) {
                        if (item == $(element).text().trim()) {
                            var className = itemFound ? 'correct' : 'incorrect';
                            $(element).parent().addClass(className);
                        }
                    });
                });
            }
            if (value.interaction == 'container' && value.Canvas.length > 0) {
                var textArray = value.textforeachblank.map(function (x) {
                    return $('<span>' + x.text + '</span>').text();
                });
                var isOptionIncorrect = false;
                for (var i = 0; i < value.Canvas.length; i++) {
                    if ($.inArray($(value.Canvas[i].title).text(), textArray) > -1) {
                        value.Canvas[i].AnswerClass = "correct";
                        if (value.correct_feedback != '') {
                            value.displayfeedback = true;
                        }
                        // $scope.ShowGlobalFeebackData(value.Canvas[i].AnswerClass);
                    }
                    else {
                        value.Canvas[i].AnswerClass = "incorrect";
                        $scope.ShowGlobalIncorrectFeedback = true;
                        if (value.incorrect_feedback != '') {
                            value.displayfeedback = true;
                            isOptionIncorrect = true;
                        }
                        // $scope.ShowGlobalFeebackData(value.Canvas[i].AnswerClass);
                    }
                }
                value.AnswerClass = isOptionIncorrect ? 'incorrect' : 'correct';
                var drpItems = $('#divDropOption' + index + ' li a');
                $($scope.QuestionOptions[index].multiSelectItem).each(function (eindex, item) {
                    var itemFound = false;
                    if ($.inArray(item, textArray) > -1) {
                        itemFound = true;
                    }
                    $(drpItems).each(function (eindex, element) {
                        if (item == $(element).text().trim()) {
                            var className = itemFound ? 'correct' : 'incorrect';
                            $(element).parent().addClass(className);
                        }
                    });
                });

                //if ($scope.selectedoptions.length > 0) {
                //    $($scope.selectedoptions).each(function (sindex, svalue) {
                //        if (svalue.choiceid == value.choiceid) {
                //            if (svalue.inputvalue.length != value.textforeachblank.length) { $scope.ShowGlobalIncorrectFeedback = true; }
                //        }
                //    });
                //}
            }
        });
        if (!$scope.ShowGlobalIncorrectFeedback && ($scope.selectedoptions.length == $scope.CurrentQuestion.QuestionData.choices.length)) {
            $scope.ShowGlobalFeebackData("correct");
        }
        else {
            $scope.ShowGlobalFeebackData("incorrect");
        }

    };

    function checkEqualArray(array1, array2) {
        if (array1.length != array2.length) {
            return false;
        }
        for (var i = 0; i < array1.length; i++) {
            if (array2.indexOf(array1[i]) == -1) {
                return false;
            }
        }
        return true;
    }


    $scope.getValue = function (text) {
        $scope.essayTemplateText = text;
    }
    //$scope.generateFeedback = function () {        
    //    $state.go('FeedBack');                
    //};
    //$scope.generateFeedback = function () {
    //    var modalInstance = $modal.open({
    //        templateUrl: routeUrl + 'Scripts/app/views/assessment/FeedBack.html',
    //        controller: 'FeedBackCtrl'

    //    });

    //    //$scope.cancel = function () {
    //    //    modalInstance.dismiss('cancel');
    //    //};

    //};
    $scope.userSelectedOptions = function () {
        var params = [];
        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            return $scope.GetUserSelectedOptionForCompositeType($scope.CurrentQuestion.QuestionData.questions);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.Hotspot) {
            if ($scope.UserAnswerText) {
                params.push({ "inputvalue": $scope.UserAnswerText, "isCorrectAnswer": $scope.selectedHotspot, "questionTypeId": $scope.questionTypeId });
            }
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                // $.each($scope.CurrentQuestion.QuestionData.choices[index].suboption, function (index1, value1) {
                if (value.isChoiceAnswered) {
                    var name = value.choiceid;
                    params.push({ "inputvalue": value.choiceid + '-' + $('input[name=' + name + ']:checked').val(), "isCorrectAnswer": true, "questionTypeId": $scope.questionTypeId });
                }
            });
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND || $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.Matching) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                var userInputValue = value.UserInput;
                var correctAnswer = false;
                if (value.interaction == 'dragdrop') {
                    userInputValue = (value.Canvas.length > 0) ? value.Canvas[0].title : "";
                    var selectedValue = $scope.SelectedTemplateArray.filter(function (x) { return x.dropId == index; }).map(function (x) { return x.title; });
                    if (selectedValue.length > 0) {
                        userInputValue = selectedValue[0];
                    }
                }

                if (!!userInputValue) {
                    $(value.textforeachblank).each(function (textindex, tvalue) {
                        if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                            if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                        else {
                            if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                    });
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    var textArray = value.textforeachblank.map(function (x) { return '<span math dynamic>' + x.text + '</span>'; });

                    for (var i = 0; i < value.Canvas.length; i++) {
                        if (textArray.indexOf(value.Canvas[i].title) != -1) {
                            value.Canvas[i].AnswerClass = "correct";
                        }
                        else {
                            value.Canvas[i].AnswerClass = "incorrect";
                        }
                    }
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": value.Canvas, "choiceid": value.choiceid, "questionTypeId": $scope.questionTypeId });
                }
                else if (!!userInputValue) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": userInputValue, "isCorrectAnswer": correctAnswer, "choiceid": value.choiceid, "questionTypeId": $scope.questionTypeId });
                }

            });
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.Essay) {
            var str = "";
            //$scope.CurrentQuestion.QuestionData
            if ($scope.CurrentQuestion.QuestionData.toolbar.type == "math_3-5_palette" || $scope.CurrentQuestion.QuestionData.toolbar.type == "math_6-8_palette") {
                // removing the unwanted duplicate cursor from matheditor
                if ($('#mathQuill .cursor').length > 1) {
                    $('#mathQuill .cursor:first').remove();
                }

                if ($('#mathQuill').mathquill('latex') == "{ }") {
                    $('#mathQuill').mathquill('8', 'keyStroke');
                }
                str = $('#mathQuill').mathquill('latex');
            } else {
                str = $scope.essayTemplateText;//$("#txtEssayContent").text();
            }
            //var html = $(str).text();
            params.push(str);
            return params;
        }
        else if ($scope.questionTypeId == Enum.QuestionType.Order) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                params.push({ "choiceid": this.CurrentOptionIndex, "questionTypeId": $scope.questionTypeId });
            });
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.TextSelection) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                if ($scope.CurrentQuestion.QuestionData.choices[index].AnswerClass.indexOf("selectedC") != -1) {
                    params.push({ "choiceid": this.choiceid, "questionTypeId": $scope.questionTypeId });
                }
            });
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.HotSpot) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                if ($scope.UserSelectedArea.indexOf(this.choiceid) != -1) {
                    params.push({ "choiceid": this.choiceid, "questionTypeId": $scope.questionTypeId });
                }
            });
            return JSON.stringify(params);
        }
        else {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                if (this.isAnswered == true) {
                    params.push({ "choiceid": this.choiceid, "questionTypeId": $scope.questionTypeId });
                }
            });
        }
        return JSON.stringify(params);

    }




    $scope.setOptions = function (option, OptionId) {
        if ($scope.isPaused) return false;
        if ($scope.questionTypeId == Enum.QuestionType.MCSS || $scope.questionTypeId == Enum.QuestionType.MCMS) {
            if ($('#checkAnswerbtn').html() == 'Try Again') {
                if (option.currentClass == "") {
                    $('#radio' + option.choiceid).prop('checked', '');
                    $('#chkBox' + option.choiceid).prop('checked', '');
                }

                $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                    if (this.currentClass != "") {
                        $('#radio' + this.choiceid).prop('checked', 'checked');
                        $('#chkBox' + this.choiceid).prop('checked', 'checked');
                    }
                });
                return false;
            }
        }
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.ShowGlobalCorrectFeedback = false;
        $(".option").removeClass('correct');
        $(".option").removeClass('incorrect');
        if ($('#checkAnswerbtn').attr('disabled') != 'disabled') {
            $('#checkAnswerbtn').html('Check Answer');
            $('#checkAnswerbtnmobile').html('Check Answer');
        }
        switch ($scope.questionTypeId) {
            case Enum.QuestionType.MCSS:
            case Enum.QuestionType.GRAPHIC:
            case Enum.QuestionType.Exhibit:
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                    this.currentClass = "";
                    if (this.choiceid == option.choiceid) {
                        this.isAnswered = true;
                        option.currentClass = "selected";
                    }
                    else {
                        this.isAnswered = false;
                    }
                    this.isButtonClicked = true;
                    if (this.isAnswered) {
                        $scope.userHasSelectedOptions = true;
                        this.currentClass = "selected";
                    }
                });

                break;
            case Enum.QuestionType.MCMSVIDEO:
            case Enum.QuestionType.MCMS:
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                    if (this.choiceid == option.choiceid) {
                        if (this.isAnswered == true) {
                            this.isAnswered = false;
                            option.currentClass = "";
                        }
                        else {
                            this.isAnswered = true;
                            option.currentClass = "selected";
                        }
                    }
                    if (this.isAnswered == true) {
                        this.currentClass = "selected";
                        $scope.userHasSelectedOptions = true;
                    }
                });
                break;
            case Enum.QuestionType.ChoiceMatrix:
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                    $scope.selectedOption = OptionId;

                    $(value.suboption).each(function (sindex, soption) {
                        if (value.choiceid == OptionId) {
                            value.isChoiceAnswered = true;
                            if (option.identifier == soption.identifier) {
                                this.isOptionAnswered = true;
                                value.isAnswered = value.assess;
                                value.suboptionSelected = soption.identifier;
                                soption.isAnswered = soption.value;
                                soption.currentClass = "selected";
                            }
                            else {
                                this.isOptionAnswered = false;
                                soption.currentClass = "";
                                soption.isAnswered = !soption.value;
                            }
                        }
                        if (this.isOptionAnswered == true) {
                            this.currentClass = "selected";
                            $scope.userHasSelectedOptions = true;
                        }
                    });
                });
                break;
            case Enum.QuestionType.Order:
                $(option).each(function (index) {
                    this.isAnswered = true;
                    if (this.isAnswered) {
                        $scope.userHasSelectedOptions = true;
                    }
                });
                break;
            case Enum.QuestionType.FIB:
            case Enum.QuestionType.FIBDND:
            case Enum.QuestionType.Matching:
            case Enum.QuestionType.LTD:
            case Enum.QuestionType.LTDDND:
                var totalChoices = $scope.CurrentQuestion.QuestionData.choices.length;
                var objList = $filter('filter')($scope.CurrentQuestion.QuestionData.choices, { "Canvas": [] }, true);
                var objList2 = $filter('filter')($scope.CurrentQuestion.QuestionData.choices, { "UserInput": "" }, true);
                if (objList.length < totalChoices && objList.length > 0)
                    $scope.userHasSelectedOptions = true;
                if (objList2.length < totalChoices && objList.length > 0)
                    $scope.userHasSelectedOptions = true;
                //if (objList.length == 0) mehul
                //    $scope.userHasSelectedOptions = false;
                break;
        }
    }

    $scope.enableDisableCheckAnswerButton = function (flagValue) {
        $scope.userHasSelectedOptions = flagValue;
    }

    $scope.shuffleArray = function (array) {
        var m = array.length, t, i;

        // While there remain elements to shuffle
        while (m) {
            // Pick a remaining element…
            i = Math.floor(Math.random() * m--);

            // And swap it with the current element.
            t = array[m];
            array[m] = array[i];
            array[i] = t;
        }

        return array;
    };

    $scope.showHideCheckAnswer = function () {
        //if ($scope.CurrentQuestion.QuestionData.global_correct_feedback.text != null && $scope.CurrentQuestion.QuestionData.global_correct_feedback.text != "") {
        //    $scope.showCheckAnswerbtn = true;
        //}
        //else {            
        //    $scope.showCheckAnswerbtn = false;
        //}
        //if ($scope.CurrentQuestion.QuestionData.question_type.text != 'essay') {
        //    $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
        //        if (value.feedback != null && value.feedback != "") {
        //            $scope.showCheckAnswerbtn = true;
        //        }
        //    });
        //}
        if ($scope.CurrentQuestion.QuestionData.question_type.text == 'essay') {
            $scope.showCheckAnswerbtn = false;
        }
    };

    // $scope.showOptionFeedback = function (optionId) {
    // if(optionId != undefined)
    // {
    // var popupValue = '';
    // $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
    // if (value.choiceid == optionId) {
    // if (value.feedback != null && value.feedback != undefined && value.feedback != "") {
    // popupValue = '<span>' + value.feedback + '</span>';
    // popupValue = $compile(popupValue)($scope);

    // $('.container-main').click(function(e){
    // if (e.target.className != null && e.target.className != undefined && e.target.className != "" && e.target.className != 'fa fa-commenting-o')
    // {
    // $('.popover').hide(); 
    // }
    // });
    // }
    // }
    // });

    // if(popupValue != "" && popupValue != undefined && popupValue != null)
    // {
    // popupValue = popupValue[0].innerHTML;
    // }
    // return popupValue;
    // }
    // };


    $scope.initializeQuiz = function (data) {
        popovermodel = '';
        $scope.showExhibit = false;
        $scope.CheckAnswer = false;
        $scope.userHasSelectedOptions = false;
        $scope.questionTypeId = data.QuestionTypeId;

        if (!$scope.isUndefined(data.QuestionData.question_type)) {
            if ((data.QuestionData.question_type.text.toLowerCase() == 'ltd' || data.QuestionData.question_type.text.toLowerCase() == 'ltd_dnd' || data.QuestionData.question_type.text.toLowerCase() == 'hotspot' || data.QuestionData.question_type.text.toLowerCase() == 'textselection' || data.QuestionData.question_type.text.toLowerCase() == 'choicematrix' || data.QuestionData.question_type.text == 'mcss' || data.QuestionData.question_type.text == 'composite' || data.QuestionData.question_type.text == 'mcms' || data.QuestionData.question_type.text == 'order' || data.QuestionData.question_type.text == 'fib' || data.QuestionData.question_type.text == 'fib_dnd' || data.QuestionData.question_type.text == 'matching' || data.QuestionData.question_type.text == 'essay') && data.QuestionData.question_type.text.length > 0) {
                $scope.questionTypeId = data.QuestionData.question_type.text.toLowerCase();
            }
        }
        if ($scope.questionTypeId != 'composite') $scope.setQuestionOptions(data);
        $scope.CurrentQuestion = data;
        $scope.questionName = data.QuestionData.question_stem.text;
        if ($scope.questionTypeId == 'composite') {
            $scope.questionBody = data.QuestionData.question_body.text;
        }

        if (!$scope.isUndefined(data.QuestionData.stem_details) && data.QuestionData.stem_details.type == 'algorithimic') {
            $scope.SetAlgorithimicType(data);
        }
        $scope.instructionText = data.QuestionData.instruction_text.text;
        $scope.totalItems = $scope.questions.length;

        $scope.isEssay = false;
        $scope.isComposite = false;
        $scope.compositeQuestions = [];
        $scope.showHint = false;
        $scope.BookMarked = false;
        $scope.NotLearned = false;
        $scope.ReportNote = '';
        $scope.CorrectAnswerCount = 0;
        $scope.HintsUsed = 0;

        $scope.ShowGlobalFeedback = false;
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.ShowGlobalCorrectFeedback = false;
        $scope.showOptionFeedbackModal = false;

        $scope.globalCorrectFeedback = '';
        $scope.globalInCorrectFeedback = '';
        $scope.OptionFeedback = '';
        $scope.showCheckAnswerbtn = true;
        $scope.ShowQueFeedback = true;
        $scope.ShowFeedBackAt = { 'Question': true, 'Option': true };

        $scope.NoOfSelection = 0;
        if (data.UserAssessmentActivityDetail) {
            $scope.UserAssessmentActivityDetail = data.UserAssessmentActivityDetail;
            $scope.BookMarked = $scope.UserAssessmentActivityDetail.IsBookMarked;
            $scope.NotLearned = $scope.UserAssessmentActivityDetail.NotLearned;
            $scope.ReportNote = $scope.UserAssessmentActivityDetail.ReportAProblem;
            $scope.CorrectAnswerCount = $scope.UserAssessmentActivityDetail.NoOfRetry;
            $scope.HintsUsed = $scope.UserAssessmentActivityDetail.HintsUsed;
        } else {
            $scope.UserAssessmentActivityDetail = {
                IsBookMarked: false,
                NotLearned: false,
                HintsUsed: 0,
                NoOfRetry: 0,
                TimeSpent: 0,
                ReportAProblem: '',
                QuizQuestID: '',
                UserAssessmentActivityType: 0
            };
        }

        if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
            $scope.ShowHintBox = $scope.AssignmentSetting.HintOn;
            $scope.BackwardMovement = $scope.AssignmentSetting.BackwardMovement;
            $scope.SkipQuestion = $scope.AssignmentSetting.SkipQuestion;
            $scope.ShuffleQuestions = $scope.AssignmentSetting.Shuffle;
            $scope.AssessmentAttempt = $scope.AssignmentSetting.Attempts;
            $scope.QuestionAttempt = $scope.AssignmentSetting.QuestionAttempts;

            if (!$scope.isUndefined($scope.AssignmentSetting.ShowQueFeedback))
                $scope.ShowQueFeedback = $scope.AssignmentSetting.ShowQueFeedback;

            $scope.PauseTimer = $scope.AssignmentSetting.PauseTimer;

            if (!$scope.isUndefined($scope.AssignmentSetting.ShowFeedBackAt) && !$scope.isUndefined($scope.AssignmentSetting.ShowFeedBackAt.Option)) {
                $scope.ShowFeedBackAt = angular.fromJson($scope.AssignmentSetting.ShowFeedBackAt);
            }

            if ($scope.ShowFeedBackAt.Question == true) {
                $scope.ShowGlobalFeedback = true;
                //$scope.showCheckAnswerbtn = true;
                //$scope.showHideCheckAnswer();
            }
            if ($scope.ShowFeedBackAt.Option == true) {
                $scope.showOptionFeedbackModal = true;
                //$scope.showCheckAnswerbtn = true;
                //$scope.showHideCheckAnswer();
            }

            $scope.showHideCheckAnswer();
            if ($scope.ShuffleQuestions) {
                if (data.QuestionData.choices != null || data.QuestionData.choices != undefined) {
                    $scope.QuestionOptions = $scope.shuffleArray(data.QuestionData.choices);
                }
            }
            else {
                $scope.QuestionOptions = data.QuestionData.choices;
            }

            if ($scope.SkipQuestion == false) {
                $scope.disableNextBtn = true;
            }
            else {
                $scope.disableNextBtn = false;
            }

            $('#checkAnswerbtn').html('Check Answer');

            if ($scope.CorrectAnswerCount > 0) {
                $scope.showAttempt = true;
                if ($scope.QuestionAttempt == $scope.CorrectAnswerCount) {
                    $('#checkAnswerbtn').html('Try Again');
                    $('#checkAnswerbtnmobile').html('Try Again');
                    $scope.DisableCorrectAnswer = true;
                }
            }
        }
        else {
            $('#checkAnswerbtn').html('Check Answer');
            $('#checkAnswerbtnmobile').html('Check Answer');
            $scope.AssignmentSetting = {};
            $scope.ShowGlobalFeedback = true;
            $scope.showOptionFeedbackModal = true;
            $scope.showAttempt = true;
            $('.attempts').hide();
            $scope.showCheckAnswerbtn = true;
            $scope.ShowHintBox = true;
            $scope.showHintCount = true;
            //  $scope.ShowFeedBackAt.Question = true;
            $scope.QuestionOptions = data.QuestionData.choices;
        }

        angular.forEach($scope.QuestionOptions, function (option, key) {
            option.displayfeedback = false;
            if ($scope.questionTypeId == Enum.QuestionType.FIBDND) {
                $("div[data-drop-id='" + key + "']").empty();
            }
        });
        //$scope.PassiveTimer.startTimer(data.TimeSpent);
        //$scope.text = '';
        $scope.currentPage = data.CurrentQuestion;
        if ($scope.questionTypeId == Enum.QuestionType.MCMS || $scope.questionTypeId == Enum.QuestionType.MCSS) {
            $scope.questionSubType = data.QuestionData.question_interaction.text;
            $scope.questionLayout = data.QuestionData.question_layout.text;
            //$scope.exhibitType = data.QuestionData.exhibit[0].exhibit_type;
            if (!$scope.isUndefined(data.QuestionData.exhibit) && data.QuestionData.exhibit.length > 0 && data.QuestionData.exhibit[0].exhibit_type != "") {
                $scope.showExhibit = true;
                if (data.QuestionData.exhibit[0].exhibit_type == 'image') {
                    //var newString = String(data.QuestionData.exhibit[0].path).replace("___ASSETINFO", "").replace("___<br>", "").replace(/&#39;/g, '"').replace(/'/g, '"');
                    //var optionImage = JSON.parse(newString);
                    var imagePath = String(data.QuestionData.exhibit[0].path);
                    // var newImage= imagePath[0];
                    //var newImagePath = newImage + '/>';

                    data.QuestionData.exhibit[0].path = imagePath;//"Content/images/" + optionImage.asset_name;
                }
            }
            else {
                $scope.showExhibit = false;
            }
        }
        else {
            $scope.questionSubType = data.QuestionData.settings.question_type;
        }

        if ($scope.questionTypeId == Enum.QuestionType.Order) {
            $scope.orderType = data.QuestionData.question_interaction.text;
            if ($scope.orderType == "media")
                $scope.layoutType = data.QuestionData.question_layout.text;
        }

        if ((!$scope.isUndefined(data.QuestionData.hints) && $scope.AssignmentSetting.HintOn) || (!$scope.isUndefined(data.QuestionData.hints) && $scope.showHintCount)) {

            var hintsUsed = $scope.HintsUsed;
            $scope.hintCount = 0;
            $scope.visibleHints = [];

            for (var i = 0; i < data.QuestionData.hints.length; i++) {
                var value = data.QuestionData.hints[i];
                if (value != null || value!=undefined) {
                    if (value.text != null) {
                        if (hintsUsed != 0) {
                            $scope.showHint = true;
                            $scope.visibleHints.push('<span math dynamic>' + data.QuestionData.hints[i].text + '</span>');
                            hintsUsed = hintsUsed - 1;
                        }
                        else {
                            $scope.hintCount++;
                        }
                    }
                }
            }
        }
        else {
            $scope.hintCount = 0;
            $scope.showHint = false;
        }

        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            $scope.showCheckAnswerbtn = true;
            $scope.isComposite = true;
            var compositeQuestionData = [];
            $('#quizContainer').parent().removeClass('container');
            $('#quizContainer').parent().addClass('composite_container');
            //$.each(data.QuestionData.questions, function (index, value) {
            //    //debugger;
            //    compositeQuestionData.push({ "QuestionData": value });
            //});

            $scope.Questions = null;
            $scope.compositeQuestions = data.QuestionData.questions;
            $scope.CompositeSelectedOption = data.SelectedOptions;
            if ($scope.compositeQuestions.length == 0) {
                $scope.showCheckAnswerbtn = false;
            }
            //$scope.$broadcast('nextCompositeQuestion', {myMsg: "hi children"});
            $scope.$broadcast('nextCompositeQuestion');
            //$scope.$broadcast('resetCompositeQuestion');
            //$scope.ResetCompositeQuestions(data.QuestionData.questions);

            //$scope.$broadcast('resetCompositeQuestion');
            //assessmentEngineSvc.SetCompositeQuestions(data.QuestionData.questions);
            //$timeout(function () {
            //    if (document.getElementById('compositeIframe') != null) document.getElementById('compositeIframe').src = document.getElementById('compositeIframe').src;
            //});
        }
        else {
            $('#quizContainer').parent().addClass('container');
        }

        if ($scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.Matching) {
            $scope.SetFIBType(data);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND) {
            $scope.SetLTDType(data);
        } else if ($scope.questionTypeId == Enum.QuestionType.TextSelection) {
            $scope.SetTextSelectionType(data);
            if (data.SelectedOptions) {
                var selectedoptions = $.parseJSON(data.SelectedOptions);
                $(selectedoptions).each(function (selectindex) {
                    $($scope.QuestionOptions).each(function (index, value) {
                        if ($scope.QuestionOptions[index].choiceid == parseInt(selectedoptions[selectindex].choiceid)) {
                            $scope.QuestionOptions[index].AnswerClass = "selectedC";
                            $scope.NoOfSelection = $scope.NoOfSelection + 1;
                        }
                    });
                });
            }
            console.log($scope.ShowGlobalCorrectFeedback);
        }
        if ($scope.questionTypeId == Enum.QuestionType.HotSpot) {
            $scope.SetHotSpotType(data);
        }

        if ($scope.questionTypeId == Enum.QuestionType.Order) {
            if ($scope.ShuffleQuestions) {
                data.QuestionData.choices = $scope.Shuffle(data.QuestionData.choices);
            }
            $.each(data.QuestionData.choices, function (index, value) {
                data.QuestionData.choices[index].text = '<span math dynamic>' + value.text + '</span>';
                data.QuestionData.choices[index].OptionNo = index + 1;
            });
            //console.log(data.QuestionData.choices);
        }
        if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
            $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
                $.each($scope.CurrentQuestion.QuestionData.choices[index].suboption, function (index1, value1) {
                    $scope.CurrentQuestion.QuestionData.choices[index].suboption[index1] = value1;
                    //$scope.CurrentQuestion.QuestionData.choices[index].suboption[index1].setIndex = index1 + 1;
                });
            });
        }
        if ($scope.questionTypeId == Enum.QuestionType.Essay) {
            $scope.isEssay = true;
            if (data.QuestionData.max_characters != undefined) {
                $scope.maxCharaterLength = data.QuestionData.max_characters.text;
            }
            $scope.EssayTypeToolbar = data.QuestionData.toolbar.type;
            mathEditorType = data.QuestionData.toolbar.type;
            $('.math-toolbar').hide();
        }
        if ($scope.AssignmentQuestions.length > 0 && !$scope.isUndefined($scope.AssignmentQuestions[$scope.currentPage - 1])) {
            if ($scope.AssignmentQuestions[$scope.currentPage - 1].QuestionStatus != 'answer-visited' && $scope.AssignmentQuestions[$scope.currentPage - 1].QuestionStatus != 'answer-right') {
                $scope.getProgressPercentage();
            }
        }
        $('#myTabs').on('click', '.nav-tabs a', function () {
            $(this).closest('.dropdown').addClass('dontClose');
        })

        $('#myDropDown').on('hide.bs.dropdown', function (e) {
            if ($(this).hasClass('dontClose')) {
                e.preventDefault();
            }
            $(this).removeClass('dontClose');
        });

        $('#myDropDown2 .toc-list').click(function () {
            $(this).parent().addClass('open');
        });

        $('#myTabs2 ul.nav li').click(
        function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('.active').removeClass('active').addClass('notactive');
            $(this).addClass('active').removeClass('notactive');
            var ref = $(this).find("a").attr("href");
            $(this).parents("#myTabs2").find(".tab-content .tab-pane").removeClass("active")
            $(this).parents("#myTabs2").find(".tab-content .tab-pane" + ref).addClass("active");
        });

        $('.actions-mobile .dropdown-menu').on('click', function (event) {
            event.stopPropagation();
        });
        //$('#quizContainer').mCustomScrollbar(
        //    {
        //        theme: "dark"
        //    });

        //Added for dynamically loading the Math Editor
        setTimeout(function () {
            resize_fun();
            $('#mathQuill').mathquill('editable');
            $('#mathQuill').width($('.math-editor').width());
            $('#mathQuill').height('auto');
            SetEditorView(mathEditorType);

            if ($.trim(mathEditorPrevLatex).length != 0) {
                mathEditorPrevLatex = mathEditorPrevLatex.replace("?", "");
                $('#mathQuill').mathquill('latex', mathEditorPrevLatex);
                $('#mathQuill textarea').focus();
            } else {
                $('#mathQuill').text('');
                $('#mathQuill').mathquill('editable');
                $('#mathQuill').width($('.math-editor').width());
                $('#mathQuill').height('auto');
                $('#mathQuill').mathquill('cmd', 'emptyspace');
                $('#mathQuill textarea').focus();
            }
        }, 2000);
    }

    $scope.hoverIn = function (event, option) {
        if (!option.stopHover) {
            event.currentTarget.style.opacity = 1;
        }
    };

    $scope.hoverOut = function (event, option) {
        if (!option.stopHover) {
            event.currentTarget.style.opacity = 0;
        }
    };

    $scope.clickHotspot = function (option) {
        if (!$scope.CheckAnswer) {
            if ($scope.NoOfSelection < $scope.MaxSelection || ($scope.NoOfSelection == $scope.MaxSelection && option.currentClass == 'selected')) {
                option.isAnswered = !option.isAnswered;
                var index = $scope.UserSelectedArea.indexOf(option.choiceid);
                if (index > -1) {
                    $scope.UserSelectedArea.splice(index, 1);
                    option.currentClass = '';
                    $scope.NoOfSelection = $scope.NoOfSelection - 1;
                }
                else {
                    $scope.UserSelectedArea.push(option.choiceid);
                    option.currentClass = 'selected';
                    $scope.NoOfSelection = $scope.NoOfSelection + 1;
                }
                if ($scope.NoOfSelection > 0) {
                    $scope.userHasSelectedOptions = true;
                }
                else if ($scope.NoOfSelection == 0) {
                    $scope.userHasSelectedOptions = false;
                }
            }
        }
    };

    $scope.SetHotSpotType = function (data) {
        if (data.QuestionData.max_choice_selection.text == '0') {
            $scope.MaxSelection = data.QuestionData.choices.length;
        }
        else if (data.QuestionData.max_choice_selection.text == '') {
            $scope.MaxSelection = 1;
        }
        else {
            $scope.MaxSelection = parseInt(data.QuestionData.max_choice_selection.text);
        }
        $scope.NoOfSelection = 0;

        var imgObject = document.createElement('div');
        imgObject.innerHTML = data.QuestionData.question_media.media;
        $scope.imageSrc = $(imgObject).find('img').attr('src');
        if ($scope.isUndefined($scope.imageSrc)) {
            $scope.imageSrc = '';
        }

        angular.forEach(data.QuestionData.choices, function (option, key) {
            $scope.QuestionOptions[key].XPosition = '';
            $scope.QuestionOptions[key].YPosition = '';
            $scope.QuestionOptions[key].isAnswered = false;
            // $scope.QuestionOptions[key].opacity = 0;

            var positions = option.text.split(',');
            if (positions.length == 2) {
                $scope.QuestionOptions[key].XPosition = positions[0];
                $scope.QuestionOptions[key].YPosition = positions[1];
            }
        });


        if (!$scope.isUndefined(data.QuestionData.question_media.media) && data.QuestionData.question_media.media.length > 0 && data.QuestionData.question_media.media != "") {
            if (data.SelectedOptions) {
                var selectedoptions = $.parseJSON(data.SelectedOptions);
                $(selectedoptions).each(function (selectindex) {
                    $($scope.QuestionOptions).each(function (index, value) {

                        if ($scope.QuestionOptions[index].choiceid == parseInt(selectedoptions[selectindex].choiceid)) {
                            $scope.QuestionOptions[index].isAnswered = true;
                            $scope.QuestionOptions[index].currentClass = 'selected';
                            $scope.NoOfSelection = $scope.NoOfSelection + 1;
                        }
                    });
                });
            }
        }
    };

    $scope.SetTextSelectionType = function (data) {
        var questiontext = "";//data.QuestionData.textwithblanks.text;
        $scope.PassageText = "";
        if (data.QuestionData.max_choice_selection.text == '0') {
            $scope.MaxSelection = data.QuestionData.choices.length;
        }
        else if (data.QuestionData.max_choice_selection.text == '') {
            $scope.MaxSelection = 1;
        }
        else {
            $scope.MaxSelection = parseInt(data.QuestionData.max_choice_selection.text);
        }
        $scope.NoOfSelection = 0;
        if (data.QuestionData.textwithblanks != undefined) {
            if (data.QuestionData.textwithblanks.text != undefined && data.QuestionData.textwithblanks.text != "")
                questiontext = data.QuestionData.textwithblanks.text;
        }
        if (!$scope.isUndefined(data.QuestionData.exhibit) && data.QuestionData.exhibit.length > 0 && data.QuestionData.exhibit[0].exhibit_type != "") {
            $scope.showExhibit = true;
            if (data.QuestionData.exhibit[0].exhibit_type == 'image') {
                var imagePath = String(data.QuestionData.exhibit[0].path);
                data.QuestionData.exhibit[0].path = imagePath;
            }
        }


        angular.forEach(data.QuestionData.choices, function (option, key) {
            $scope.QuestionOptions[key].key = key;       // Index of option            
            $scope.QuestionOptions[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect            
            $scope.QuestionOptions[key].showOptionFeedbackModal = $scope.showOptionFeedbackModal;
            $scope.QuestionOptions[key].showAttempt = $scope.showAttempt;
            $scope.QuestionOptions[key].ShowGlobalFeedback = $scope.ShowGlobalFeedback;
            if (!$scope.isUndefined(option.text) && option.text != "" && option.text.match("^\[\[[0-9]*\]\]")) {
                var startTagToReplace = option.text;
                var endTagToReplace = option.text.substring(0, 2) + "/" + option.text.substring(2);
                questiontext = questiontext.replace(startTagToReplace, "&nbsp;&nbsp;<span class='option selectable' style='cursor: pointer;' ng-click='$parent.SetSelectedClass(" + key + ")' ng-class='$parent.QuestionOptions[" + key + "].AnswerClass'> <span class='sign'><span class='sr-only'></span><span class='fa'></span></span><span class='fdback fdback-desktop' ng-show='$parent.QuestionOptions[" + key + "].showOptionFeedbackModal && $parent.QuestionOptions[" + key + "].displayfeedback'><span class='sr-only'></span><span id='spnltd" + key + "'class='fa fa-commenting-o' popover popover-html='" + $scope.QuestionOptions[key].choiceid + "' ng-click='$parent.GetFeedback($parent.QuestionOptions[" + key + "])'></span></span>");
                //questiontext = questiontext.replace(startTagToReplace, "&nbsp;&nbsp;<span id='spnltd" + key + "' class='option selectable' style='cursor: pointer;' ng-click='$parent.SetSelectedClass(" + key + ")' ng-class='$parent.QuestionOptions[" + key + "].AnswerClass'> <span class='sign'><span class='sr-only'></span><span class='fa'></span></span><span class='fdback' ng-show='$parent.QuestionOptions[" + key + "].showOptionFeedbackModal && $parent.QuestionOptions[" + key + "].showAttempt'><span class='sr-only'></span><span class='fa fa-commenting-o' popover popover-html='" + $scope.QuestionOptions[key].choiceid + "' ng-click='$parent.GetFeedback($parent.QuestionOptions[" + key + "])'></span></span>");
                questiontext = questiontext.replace(endTagToReplace, "</span>&nbsp;&nbsp;");
            }
        });
        $scope.PassageText = questiontext;
    };

    $scope.SetSelectedClass = function (key) {
        if (!$scope.CheckAnswer) {
            if ($scope.NoOfSelection < $scope.MaxSelection || ($scope.NoOfSelection == $scope.MaxSelection && $scope.QuestionOptions[key].AnswerClass == 'selectedC')) {
                if ($scope.QuestionOptions[key].AnswerClass == 'selectedC') {
                    $scope.QuestionOptions[key].AnswerClass = "";
                    $scope.NoOfSelection = $scope.NoOfSelection - 1;
                }
                else {
                    $scope.QuestionOptions[key].AnswerClass = "selectedC";
                    $scope.NoOfSelection = $scope.NoOfSelection + 1;
                }
            }
            if ($scope.NoOfSelection > 0) {
                $scope.userHasSelectedOptions = true;
            }
            else if ($scope.NoOfSelection == 0) {
                $scope.userHasSelectedOptions = false;
            }
        }
    };

    $scope.$on('elementDropped', function (event, args) {
        //console.log('nextCompositeQuestion');
        //alert(args.dragId);

        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            var option = ($scope.isUndefined(args.option)) ? null : $.parseJSON(args.option);
            $scope.DragDropInComposite(option, args.dropId, args.title, args.questionIndex);
        }
        else {
            $scope.dropFunction(args.dropId, args.title);
            $scope.setOptions(null, null);
        }
    })

    $scope.DragDropInComposite = function (choice, dropId, dropItemText, questionIndex) {
        var compoQuestionIndex = (choice == null) ? questionIndex : choice.QuestionIndex;
        $scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].userHasSelectedOptions = true;
        $scope.enableDisableCheckAnswerButton(true);
        angular.forEach($scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].SelectedTemplateArray, function (option, key) {
            if (option.title == dropItemText) {
                option.dropId = dropId;
            }
        });
        if (dropId != 'initial') {
            var input = $scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].SelectedTemplateArray.filter(function (x) { return x.dropId == dropId; });
            if (input.length == 1) {
                $scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].choices[dropId].accept = false;
                $scope.$apply();
            }
        }
        else {
            //$('[id^=divDrpItems]').removeClass('drop-class-2');
            var items = $('.drag-container').children().removeClass('drop-class-2');
        }

        angular.forEach($scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].droppableItems, function (option, key) {
            var otitle = option.title;
            if (option.title.indexOf('<span math dynamic>') == -1) {
                otitle = '<span math dynamic>' + option.title + '</span>';
            }
            if ($filter('lowercase')(otitle) == $filter('lowercase')('<span math dynamic>' + dropItemText + '</span>')) {
                option.dropId = dropId;
                if (dropId != 'initial') {
                    option.model = $scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].choices[dropId];
                }
                else {
                    option.model = '';
                }
            }
        });
        var itemCount = 0;
        angular.forEach($scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].choices, function (model, index) {
            itemCount = 0;
            angular.forEach($scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].SelectedTemplateArray, function (option, key) {
                if (option.dropId == index) {
                    itemCount++;
                }
            });
            if (itemCount == 0) {
                model.accept = true;
                $scope.$apply();
            }
        });
    };

    $scope.dropFunction = function (dropId, dropItemText) {
        angular.forEach($scope.SelectedTemplateArray, function (option, key) {
            if (option.title == dropItemText) {
                option.dropId = dropId;
            }
        });
        $scope.userHasSelectedOptions = true;
        if ($scope.questionTypeId == Enum.QuestionType.FIBDND || ($scope.questionTypeId == Enum.QuestionType.Matching && $scope.layoutType == 'dragdrop_parallel')) {
            $scope.DragDropOnFIB(dropId, dropItemText);
        }
        else if (($scope.questionTypeId == Enum.QuestionType.Matching && $scope.layoutType != 'dragdrop_parallel') || $scope.questionTypeId == Enum.QuestionType.LTDDND) {
            $scope.DragDropOnMatching(dropId);
        }
    };

    $scope.DragDropOnFIB = function (dropId, dropItemText) {
        if (dropId != 'initial') {
            var input = $scope.SelectedTemplateArray.filter(function (x) { return x.dropId == dropId; });
            if (input.length == 1) {
                $scope.QuestionOptions[dropId].accept = false;
                $scope.userHasSelectedOptions = true;
                // $scope.QuestionOptions[dropId].Canvas = [{ 'title': dropItemText, 'drag': true, selectedClass: '' }]; mehul

                $scope.$apply();
            }
        }
        else {
            var items = $('.drag-container').children().removeClass('drop-class-2');
        }

        angular.forEach($scope.droppableItems, function (option, key) {
            var otitle = option.title;
            if (option.title.indexOf('<span math dynamic>') == -1) {
                otitle = '<span math dynamic>' + option.title + '</span>';
            }
            if ($filter('lowercase')(otitle) == $filter('lowercase')('<span math dynamic>' + dropItemText + '</span>')) {
                option.dropId = dropId;
                if (dropId != 'initial') {
                    option.model = $scope.QuestionOptions[dropId];
                }
                else {
                    option.model = '';
                }
            }
        });
        var itemCount = 0;
        angular.forEach($scope.QuestionOptions, function (model, index) {
            itemCount = 0;
            angular.forEach($scope.SelectedTemplateArray, function (option, key) {
                if (option.dropId == index) {
                    itemCount++;
                }
            });
            if (itemCount == 0) {
                model.accept = true;
                $scope.$apply();
            }
        });
    }
    $scope.DragDropOnMatching = function (dropId) {
        if (dropId != 'initial') {
            var input = $scope.SelectedTemplateArray.filter(function (x) { return x.dropId == dropId; });
            if (input.length == 1) {
                $scope.QuestionOptions[dropId].accept = false;
            }
        }
        var itemCount = 0;
        angular.forEach($scope.QuestionOptions, function (model, index) {
            if (model.interaction == 'dragdrop') {
                itemCount = 0;
                angular.forEach($scope.SelectedTemplateArray, function (option, key) {
                    if (option.dropId == index) {
                        itemCount++;
                    }
                });
                if (itemCount == 0) {
                    model.accept = true;
                    //$scope.$apply();
                }
            }
            else {
                model.accept = true;
            }
        });
        $scope.$apply();
    }

    $scope.SetFIBType = function (data) {
        var questiontext = "";//data.QuestionData.textwithblanks.text;
        if (data.QuestionData.textwithblanks != undefined) {
            if (data.QuestionData.textwithblanks.text != undefined && data.QuestionData.textwithblanks.text != "")
                questiontext = data.QuestionData.textwithblanks.text;
        }
        //Array object to hold draggable item values
        $scope.droppableItems = [];
        $scope.FIBText = "";
        var selectedText = '';
        $scope.layoutType = ($scope.isUndefined(data.QuestionData.question_layout)) ? "" : data.QuestionData.question_layout.text;
        $scope.FillDroppableItems(data);
        angular.forEach(data.QuestionData.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            $scope.QuestionOptions[key].key = key;       // Index of option
            $scope.QuestionOptions[key].UserInput = "";  //User input value of textbox and dropdown value.         
            $scope.QuestionOptions[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            $scope.QuestionOptions[key].width = 'auto';
            $scope.QuestionOptions[key].showOptionFeedbackModal = $scope.showOptionFeedbackModal;
            $scope.QuestionOptions[key].showAttempt = $scope.showAttempt;
            $scope.QuestionOptions[key].ShowGlobalFeedback = $scope.ShowGlobalFeedback;
            $scope.QuestionOptions[key].multiSelectItem = [];
            $scope.interaction = option.interaction;
            $scope.QuestionOptions[key].accept = true;
            $scope.QuestionOptions[key].FibClass = "";

            if ($scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND) {
                $scope.QuestionOptions[key].XPosition = '';
                $scope.QuestionOptions[key].YPosition = '';
                var positions = option.noforeachblank.split(',');
                if (positions.length == 2) {
                    $scope.QuestionOptions[key].XPosition = positions[0];
                    $scope.QuestionOptions[key].YPosition = positions[1];
                }
            }

            questiontext = questiontext.replace(textToReplace, "<fibvariation ng-model='$parent.QuestionOptions[" + key + "]' parent-ctrl='$parent' user-input='QuestionOptions[" + key + "].UserInput'></fibvariation>");

            $scope.$watch("QuestionOptions[" + key + "].Canvas.length", function (newValue, oldValue) {
                //console.log(newValue);
                $scope.setOptions(null, null);
            });
            //$scope.$watch("QuestionOptions[" + key + "].UserInput", function (newValue, oldValue) {
            //    //console.log(newValue);
            //    $scope.setOptions(null, null);
            //});

            if (data.SelectedOptions) {
                var selectedOption = angular.fromJson(data.SelectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if ((option.interaction == 'textentry' || option.interaction == 'inline') && value.noforeachblank == option.noforeachblank) {
                        $scope.QuestionOptions[key].UserInput = value.inputvalue;
                    }
                    else if (option.interaction == 'dragdrop' && value.noforeachblank == option.noforeachblank) {
                        if (value.inputvalue.indexOf("<span math dynamic>") != -1) {
                            selectedText = value.inputvalue;
                        }
                        else {
                            selectedText = '<span math dynamic>' + value.inputvalue + '</span>'
                        }
                        angular.forEach($scope.droppableItems, function (ditem, dindex) {
                            if ($filter('lowercase')(ditem.title) == $filter('lowercase')(selectedText)) {
                                $scope.droppableItems.splice(dindex, 1);
                                dindex = dindex - 1;
                                $scope.QuestionOptions[key].Canvas = [{ 'title': value.inputvalue, 'drag': true, selectedClass: '' }];
                                $scope.QuestionOptions[key].accept = false;
                            }
                        });
                        angular.forEach($scope.SelectedTemplateArray, function (item, indx) {
                            if (item.title == value.inputvalue) {
                                item.dropId = key;
                            }
                        });
                    }
                    else if (option.interaction == 'container' && value.noforeachblank == option.noforeachblank) {
                        var input = value.inputvalue.map(function (x) { return x.title; });
                        $scope.SelectedValues = '';
                        for (var i = 0; i < $scope.droppableItems.length; i++) {
                            if (input.indexOf($scope.droppableItems[i].title) != -1) {
                                $scope.QuestionOptions[key].Canvas.push({ 'title': $scope.droppableItems[i].title, 'drag': true, selectedClass: '' });
                                $scope.QuestionOptions[key].multiSelectItem.push($($scope.droppableItems[i].title).text());
                                $scope.droppableItems.splice(i, 1);
                                i = i - 1;
                            }
                        }
                        angular.forEach($scope.SelectedTemplateArray, function (item, indx) {
                            angular.forEach(input, function (initem, inndx) {
                                if ($filter('lowercase')(initem) == $filter('lowercase')('<span math dynamic>' + item.title + '</span>')) {
                                    item.dropId = key;
                                }
                            });
                        });
                    }
                });
            }
            if ((option.interaction == 'textentry' || option.interaction == 'inline')) {
                $scope.QuestionOptions[key].FibClass = "fibclass";
            }
        });

        $scope.FIBText = questiontext;
        if (!$scope.isUndefined($scope.layoutType)) {
            if ($scope.layoutType == 'right' || $scope.layoutType == 'vertical') {
                $scope.droptype = "col-sm-9 col-md-9";
                $scope.dragtype = "dragpanel col-sm-3 col-md-3 right-matching";
            }
            else if ($scope.layoutType == 'left') {
                $scope.droptype = "col-sm-9 col-md-9";
                $scope.dragtype = "dragpanel col-sm-3 col-md-3 left-matching";
            }
            else if ($scope.layoutType == 'top') {
                $scope.droptype = "col-sm-12 col-md-12";
                $scope.dragtype = "dragpanel col-sm-12 col-md-12 top-matching";
            }
            else if ($scope.layoutType == 'row') {
                $scope.droptype = "";
                $scope.dragtype = "dragpanel text-center";
            }
            else {
                $scope.dragtype = "dragpanel";
                $scope.droptype = "";
            }
        }
    };

    $scope.SetLTDType = function (data) {
        $scope.droppableItems = [];
        $scope.layoutType = ($scope.isUndefined(data.QuestionData.question_layout)) ? "" : data.QuestionData.question_layout.text;
        //var imagePath = String(data.QuestionData.question_media.media).replace("'\'", "");
        //var regex = /<img.*?src='(.*?)'/;
        //$scope.imageSrc = regex.exec(imagePath)[1];   
        var imgObject = document.createElement('div');
        imgObject.innerHTML = data.QuestionData.question_media.media;
        $scope.imageSrc = $(imgObject).find('img').attr('src');

        $scope.FillDroppableItems(data);
        angular.forEach(data.QuestionData.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            $scope.QuestionOptions[key].key = key;       // Index of option
            $scope.QuestionOptions[key].UserInput = "";  //User input value of textbox and dropdown value.         
            $scope.QuestionOptions[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            $scope.QuestionOptions[key].width = 'auto';
            $scope.QuestionOptions[key].showOptionFeedbackModal = $scope.showOptionFeedbackModal;
            $scope.QuestionOptions[key].showAttempt = $scope.showAttempt;
            $scope.QuestionOptions[key].ShowGlobalFeedback = $scope.ShowGlobalFeedback;
            $scope.QuestionOptions[key].XPosition = '';
            $scope.QuestionOptions[key].YPosition = '';
            $scope.QuestionOptions[key].LTDText = '';

            var positions = option.noforeachblank.split(',');
            if (positions.length == 2) {
                $scope.QuestionOptions[key].XPosition = positions[0];
                $scope.QuestionOptions[key].YPosition = positions[1];
            }
            if (data.SelectedOptions) {
                var selectedOption = angular.fromJson(data.SelectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if ((option.interaction == 'textentry' || option.interaction == 'inline') && value.noforeachblank == option.noforeachblank) {
                        $scope.QuestionOptions[key].UserInput = value.inputvalue;
                    }
                    else if (option.interaction == 'dragdrop' && value.noforeachblank == option.noforeachblank) {
                        angular.forEach($scope.droppableItems, function (ditem, dindex) {
                            if ($filter('lowercase')(ditem.title) == $filter('lowercase')(value.inputvalue)) {
                                //$scope.droppableItems[dindex].title = "";
                                $scope.QuestionOptions[key].Canvas = [{ 'title': value.inputvalue, 'drag': true, selectedClass: '' }];
                                $scope.droppableItems.splice(dindex, 1);
                                dindex = dindex - 1;
                            }
                        });
                    }
                });
            }
        });

        //$scope.Text = questiontext;
        if (!$scope.isUndefined($scope.layoutType)) {
            if ($scope.layoutType == 'right' || $scope.layoutType == 'left' || $scope.layoutType == 'vertical') {
                $scope.droptype = "col-sm-9 col-md-9";
                $scope.dragtype = "dragpanel col-sm-3 col-md-3 right";
            }
            else if ($scope.layoutType == 'row') {
                $scope.droptype = "";
                $scope.dragtype = "dragpanel text-center";
            }
            else {
                $scope.dragtype = "dragpanel";
                $scope.droptype = "";
            }
        }
    };

    $scope.SetAlgorithimicType = function (data) {
        var textReplaced = false;
        if ($scope.isUndefined(data.AlgorithmicVariableArray)) {
            data.AlgorithmicVariableArray = [];
        }
        if (data.QuestionData.stem_details.textforeachvalue.length > 0) {
            angular.forEach(data.QuestionData.stem_details.textforeachvalue, function (textDetail, key) {
                if (textDetail.type == 'random') {
                    if (data.AlgorithmicVariableArray.length == angular.fromJson(data.QuestionData.stem_details.textforeachvalue).length) {
                        angular.forEach(data.AlgorithmicVariableArray, function (arrayVariable, akey) {
                            $scope.questionName = $scope.questionName.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        });
                    }
                    else {
                        var random = Math.floor((Math.random() * textDetail.maxvalue) + textDetail.minvalue);
                        $scope.questionName = $scope.questionName.replace(textDetail.text, random);
                        data.AlgorithmicVariableArray.push({ "TextToReplace": textDetail.text, "TextValue": random });
                    }
                }
            });

            angular.forEach(data.QuestionData.choices, function (option, key) {
                angular.forEach(data.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    if (isNaN(option.text) && option.text.indexOf(arrayVariable.TextToReplace) != -1) {
                        option.text = option.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        textReplaced = true;
                    }
                });
                if (data.QuestionData.stem_details.display == 'result' && textReplaced) {
                    option.text = eval(option.text);
                }
            });
        }
    };

    $scope.Shuffle = function (array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    };

    $scope.FillDroppableItems = function (data) {
        isDistractorsPushDone = false;
        $scope.DistractorItems = [];
        $scope.SelectedTemplateArray = [];
        //data.QuestionData.choices = $scope.Shuffle(data.QuestionData.choices);
        angular.forEach(data.QuestionData.choices, function (option, key) {
            $scope.QuestionOptions[key].arrList = [];
            if (option.interaction == "inline") {
                angular.forEach(option.distractorforeachblank, function (titem, tindex) {
                    if (titem.text && titem.text.trim().length > 0)
                        $scope.QuestionOptions[key].arrList.push('<span math dynamic>' + $('<div/>').html(titem.text).text() + '</span>');
                });
            }

            if (option.interaction == "dragdrop" || option.interaction == "container") {
                if (isDistractorsPushDone == false) {
                    angular.forEach(data.QuestionData.distractors, function (titem, tindex) {
                        if (titem.text && titem.text.trim().length > 0) {
                            $scope.QuestionOptions[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                            $scope.DistractorItems.push({ id: tindex + 1, title: titem.text });
                            $scope.SelectedTemplateArray.push({ dropId: 'initial', title: $('<div/>').html(titem.text).text() });
                            //$scope.SelectedTemplateArray.push({ dropId: 'initial', title: titem.text });
                        }
                    });
                }
                isDistractorsPushDone = true;
            }

            angular.forEach(option.textforeachblank, function (titem, tindex) {
                if (titem.text) {
                    $scope.QuestionOptions[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                    $scope.DistractorItems.push({ id: tindex + 1, title: titem.text });
                    $scope.SelectedTemplateArray.push({ dropId: 'initial', title: $('<div/>').html(titem.text).text() });
                    //$scope.SelectedTemplateArray.push({ dropId: 'initial', title: titem.text });
                }
            });


            if (option.interaction == "dragdrop" || option.interaction == "container") {
                angular.forEach($scope.QuestionOptions[key].arrList, function (item, index) {
                    $scope.droppableItems.push({ 'title': item, 'drag': true, selectedClass: '' });
                });
                $scope.QuestionOptions[key].Canvas = [];
                $scope.QuestionOptions[key].Drop = true;
            }
        });
    };

    //$scope.SetItemToDropCanvasList = function (index) {
    //    if ($window.innerWidth <= 767) {
    //        if ($scope.QuestionOptions[index].Canvas.length == 0) {
    //            $scope.DropCanvasListIndex = index;
    //        }
    //    }
    //};

    /*Changes for Composite Question Type*/
    $scope.SetItemToDropCanvasListComposite = function (index, dropItems) {
        $scope.DropCanvasListIndex = index;
        $scope.droppableItems = dropItems;
    }

    $scope.AddItemToDropCanvasListComposite = function (item, index) {
        if ($scope.DropCanvasListIndex != null) {
            if ($scope.CurrentQuestion.QuestionData.questions[item.QuestionIndex].choices[$scope.DropCanvasListIndex].Canvas.length == 0) {
                $scope.CurrentQuestion.QuestionData.questions[item.QuestionIndex].choices[$scope.DropCanvasListIndex].Canvas.push({ 'title': item.title, 'drag': true, selectedClass: 'header' });
                $scope.droppableItems.splice(index, 1);
                $scope.DropCanvasListIndex = null;
                $scope.userHasSelectedOptions = true;
            }
        }
    };
    /*Changes for Composite Question Type*/

    $scope.SetItemToDropCanvasList = function (option) {
        if ($window.innerWidth <= 767) {
            if ($scope.QuestionOptions[option.key].Canvas.length == 0) {
                $scope.DropCanvasListIndex = option.key;
            }
        }
    };

    $scope.AddItemToDropCanvasList = function (item, index) {
        if ($scope.DropCanvasListIndex != null) {
            if ($scope.QuestionOptions[$scope.DropCanvasListIndex].Canvas.length == 0) {
                $scope.QuestionOptions[$scope.DropCanvasListIndex].Canvas.push({ 'title': item.title, 'drag': true, selectedClass: 'header' });
                $scope.droppableItems.splice(index, 1);
                $scope.DropCanvasListIndex = null;
                $scope.userHasSelectedOptions = true;
                //if ($window.innerWidth <= 767) {
                //    $scope.dropFunction(index, $('<div/>').html(item.title).text());
                //}
            }
        }
    };

    $scope.AddItemToDropItemList = function (item, key, index) {
        if ($window.innerWidth <= 767 && $('#checkAnswerbtn').html() == 'Check Answer') {
            if ($scope.QuestionOptions[key].Canvas.length == 1) {
                $scope.droppableItems.push({ 'title': item.title, 'drag': true, selectedClass: '' });
                $scope.QuestionOptions[key].Canvas.splice(index, 1);
                $scope.DropCanvasListIndex = key;
                $scope.userHasSelectedOptions = true;
                //if ($window.innerWidth <= 767) {
                //    $scope.dropFunction('initial', $('<div/>').html(item.title).text());
                //}
            }
        }
    };

    $scope.OnDragStart = function (option) {
        option.AnswerClass = "";
    };

    $scope.AddDropCssOnOver = function (key) {
        $('#divDropOption' + key).addClass('active');
        //$scope.QuestionOptions[key].AnswerClass = '';
        angular.forEach($scope.QuestionOptions[key].Canvas, function (option, key) {
            option.AnswerClass = '';
        });
        return true;
    };

    $scope.RemoveDropCssOnLeave = function () {
        //$('#divDOption' + key).removeClass('active');
        angular.forEach($scope.QuestionOptions, function (option, key) {
            $('#divDropOption' + key).removeClass('active');
        });
    };

    $scope.AddCssOnOver = function (item) {
        item.selectedClass = 'dragging';
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveSelected = function () {
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveCssOnOut = function () {
        angular.forEach($scope.droppableItems, function (item, key) {
            item.selectedClass = '';
        });
    };

    $scope.setQuestionOptions = function (data) {
        $scope.text = '';
        $scope.showOptionFeedbackModal = false;
        $('.fdback').addClass('ng-hide');
        $(data.QuestionData.choices).each(function (index) {
            data.QuestionData.choices[index].currentClass = '';

        });
        if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
            var selectedOpt = angular.fromJson(data.SelectedOptions);
            if (data.SelectedOptions) {
                
                $(selectedOpt).each(function (selectindex, selectvalue) {
                    var inputValues = selectvalue.inputvalue.split("-");
                    $(data.QuestionData.choices).each(function (index) {
                        var optionindex = index + 1;
                        $(data.QuestionData.choices[index].suboption).each(function (index1, value1) {
                            if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                                data.QuestionData.choices[index].suboption[index1].isAnswered = true;
                                data.QuestionData.choices[index].suboption[index1].currentClass = '';
                            }
                        });

                    });
                });

            }

        }
        else if ($scope.questionTypeId == Enum.QuestionType.HotSpot) {
            if (data.SelectedOptions != "[]" && data.SelectedOptions != "" && data.SelectedOptions != null) {
                var selectedOpt = angular.fromJson(data.SelectedOptions);
                $scope.UserAnswerText = selectedOpt[0].inputvalue;
            }
        }
        else if ($scope.questionTypeId == Enum.QuestionType.Essay) {

            if (data.SelectedOptions) {
                //$scope.essayTemplateText = data.SelectedOptions;
                if (data.QuestionData.toolbar.type == "math_3-5_palette" || data.QuestionData.toolbar.type == "math_6-8_palette") {
                    if ($scope.operationmode == Enum.OperationMode.Offline) {
                        mathEditorPrevLatex = $.trim(data.SelectedOptions[0].replace("?", ""));
                        mathEditorType = data.QuestionData.toolbar.type;
                    }
                    else {
                        mathEditorPrevLatex = $.trim(data.SelectedOptions.replace("?", ""));
                        mathEditorType = data.QuestionData.toolbar.type;
                    }
                } else {
                    $("#txtEssayContent").val(data.SelectedOptions);
                    $scope.text = data.SelectedOptions;
                }
            }
        }
        else {
            $(data.QuestionData.choices).each(function (index) {
                //data.QuestionData.choices[index].isAnswered = false;
                //if (!$scope.isUndefined(data.QuestionData.image) && data.QuestionData.image.image.indexOf('http') === -1 && data.QuestionData.image.image != "") {
                //    data.QuestionData.choices[index].image = "Content/images/" + data.QuestionData.choices[index].image;
                //}
                var optionindex = index + 1;
                data.QuestionData.choices[index].CurrentOptionIndex = optionindex;
                var selectedoptions = $.parseJSON(data.SelectedOptions);
                if (selectedoptions != null && selectedoptions.length > 0) {
                    var selectedoptions = $.parseJSON(data.SelectedOptions);
                    $(selectedoptions).each(function (selectindex) {
                        data.QuestionData.choices[index].SelectedOptions = parseInt(selectedoptions[selectindex].choiceid);
                        if (optionindex == parseInt(selectedoptions[selectindex].choiceid)) {
                            data.QuestionData.choices[index].isAnswered = true;
                        }
                        if (!$scope.isUndefined(data.QuestionData.question_interaction) && data.QuestionData.question_interaction.text == 'selectable') {
                            if (data.QuestionData.choices[index].choiceid == parseInt(selectedoptions[selectindex].choiceid))
                                data.QuestionData.choices[index].currentClass = "selected";
                        }
                    });
                }
                else
                    $scope.userHasSelectedOptions = false;
            });
        }
        return data;
    }

    $scope.DrawHotspot = function (event) {
        var option = $(this).attr("option");
        // $('area').mapster('deselect');       

        //var key='TX3';
        //$('area[data-State=' + key + ']').mapster({ strokeColor: '0C3BAD' });
        //$('area').mapster('deselect');        
        //$('.hotspotStamp').remove();
        ////var box = $("<div>").addClass('hotspotStamp').addClass('hotspotBorder').css({ 'background-color': 'red', 'opacity': '0.5' });
        //var imageWidth = parseInt($($("#mediaImage")).css('width'));
        //var imageHeight = parseInt($($("#mediaImage")).css('height'));
        //var posX = event.pageX - $($("#mediaImage")).offset().left;
        //var posY = event.pageY - $($("#mediaImage")).offset().top;
        //var left = (posX / imageWidth) * 100;
        //var top = (posY / imageHeight) * 100;
        //var coords = { left: left, top: top };
        //$(box).css({ 'left': (coords.left - 2) + "%", 'top': (coords.top - 2) + "%", 'position': 'absolute' }).appendTo('#hotspotContainer');
        //if ($(event.target).hasClass('hotspotTag')) {
        //    $scope.selectedHotspot = true;
        //    //$scope.UserAnswerText = $(box).get(0).outerHTML;
        //    $scope.UserSelectedArea.push(option.choiceid);
        //}
        //else {
        //    $scope.selectedHotspot = false;
        //    var index = $scope.UserSelectedArea.indexOf('TX' + option.choiceid);
        //    if (index != -1) {
        //        $scope.UserSelectedArea.splice(index, 1);
        //    }
        //    //$scope.UserAnswerText = $(box).get(0).outerHTML;
        //}
        if (!$scope.isUndefined(option)) {
            var index = $scope.UserSelectedArea.indexOf(option.choiceid);
            if (index > -1) {
                $scope.UserSelectedArea.splice(index, 1);
            }
            else {
                $scope.UserSelectedArea.push(option.choiceid);
            }
        }
        //    if ($(event.target).hasClass('hotspotTag')) {
        //        $scope.selectedHotspot = true;
        //        //$scope.UserAnswerText = $(box).get(0).outerHTML;
        //        $scope.UserSelectedArea.push(option.choiceid);
        //    }
        //else {
        //        $scope.selectedHotspot = false;
        //    var index = $scope.UserSelectedArea.indexOf('TX' + option.choiceid);
        //    if (index != -1) {
        //        $scope.UserSelectedArea.splice(index, 1);
        //    }
        //    //$scope.UserAnswerText = $(box).get(0).outerHTML;
        //}
    }


    $scopeserializeData = function (data) {
        // If this is not an object, defer to native stringification.
        if (!angular.isObject(data)) {
            return ((data == null) ? "" : data.toString());
        }
        var buffer = [];

        // Serialize each key in the object.
        for (var name in data) {
            if (!data.hasOwnProperty(name)) {
                continue;
            }

            var value = data[name];
            buffer.push(
                encodeURIComponent(name) +
                "=" +
                encodeURIComponent((value == null) ? "" : value)
            );
        }

        // Serialize the buffer and clean it up for transportation.
        var source = buffer
            .join("&")
            .replace(/%20/g, "+")
        ;
        return (source);
    };

    $scope.getProgressPercentage = function () {
        if ($scope.currentPage > 0) {
            var totalQuestions = $scope.AssignmentQuestions.filter(function (x) { return (x.QuestionStatus == 'answer-visited' || x.QuestionStatus == 'answer-right'); });
            $scope.totalAtemptedQuestions = totalQuestions.length + 1;
            $scope.Progress = [];
            $scope.Progress.ProgressPercentage = (($scope.currentPage * 100) / $scope.totalItems).toFixed(2);
            $scope.Progress.CurrentQuestion = $scope.currentPage;
            if ($scope.operationmode == Enum.OperationMode.Offline) {
                $scope.Progress.ProgressPercentage = (($scope.totalAtemptedQuestions * 100) / $scope.offline.quiz.length).toFixed(2);
                $scope.Progress.TotalQuestions = $scope.offline.quiz.length;
            }
            else {
                $scope.Progress.ProgressPercentage = (($scope.currentPage * 100) / $scope.totalItems).toFixed(2);
                $scope.Progress.TotalQuestions = $scope.totalItems;
            }
        }
    }
    $scope.getProgressTotal = function () {
        return $scope.CurrentQuestion.TotalQuestions;
    }

    $scope.PassiveTimer = (function () {
        var time = 0;
        var timerToken;
        var startTimer = function (startTime) {
            time = startTime;
            timerToken = setInterval(function () {
                time = time + 1;
            }, 1000);
        }
        var stopTimer = function () {
            clearInterval(timerToken);
            return time;
        }

        return {
            startTimer: startTimer,
            stopTimer: stopTimer
        };
    })();

    $scope.ActiveTimer = function (TimerSetting, CurrentQuestion) {
        $scope.totaltimemoving = TimerSetting.Minute * 60;
        $scope.totaltime = (TimerSetting.Minute * 60).toHHMMSS();

        $timeout(function () {
            $scope.$broadcast('add-timer');
        });
    }

    $scope.startTimer = function () {
        if (!timeStarted) {
            $scope.$broadcast('timer-start');
            $scope.timerRunning = true;
            timeStarted = true
        } else if ((timeStarted) && (!$scope.timerRunning)) {
            $scope.isPaused = false;

            $scope.$broadcast('timer-resume');
            $scope.timerRunning = true;
        }
    };

    $scope.stopTimer = function () {
        if ((timeStarted) && ($scope.timerRunning)) {
            $scope.isPaused = true;

            $scope.$broadcast('timer-stop');
            $scope.timerRunning = false;
        }
    };

    $scope.$on('timer-stopped', function (event, data) {
        timeStarted = true;
    });

    $scope.callbackTimer.finished = function () {
        SweetAlert.swal("Alert!", Messages.TimeExpiredAssignSubmit, "warning");
        $scope.autoSubmit();
    };

    Number.prototype.toHHMMSS = function () {
        var sec_num = parseInt(this, 10); // don't forget the second parm
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) { hours = "0" + hours; }
        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        var time = hours + ':' + minutes + ':' + seconds;
        return time;
    }

    $scope.TimerClass = (function () {
        Number.prototype.toHHMMSS = function () {
            var sec_num = parseInt(this, 10); // don't forget the second parm
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) { hours = "0" + hours; }
            if (minutes < 10) { minutes = "0" + minutes; }
            if (seconds < 10) { seconds = "0" + seconds; }
            var time = hours + ':' + minutes + ':' + seconds;
            return time;
        }
        var secondCounter = 0;
        var startTime = 0, totalSeconds = 0, threshold = 0;
        var timerToken;
        var initializeTimer = function (startTime, totSeconds, callback) {
            secondCounter = totSeconds - startTime;
            startTime = startTime;
            totalSeconds = totSeconds;
            setThreshold();
            var currentDisplayTime = (totalSeconds * 1000) - (startTime * 1000);
            timerToken = setInterval(function () {
                secondCounter = secondCounter - 1;
                if (secondCounter == threshold) {
                    //$("#timer").css('color', 'red');
                }
                $scope.totaltimemoving = secondCounter.toHHMMSS();

                $scope.$apply();
            }, 1000);
            setTimeout(function () { callback.call(totalSeconds); }, currentDisplayTime);
            $scope.totaltime = secondCounter.toHHMMSS();
        }
        var setThreshold = function () {
            threshold = totalSeconds - Math.round((totalSeconds * 80) / 100);
        }
        var stopTime = function (callback) {
            clearInterval(timerToken);
            var pendingTime = totalSeconds - secondCounter;
            callback.call(pendingTime);
        }
        return {
            startTimer: initializeTimer,
            stopTimer: stopTime
        };
    })();

    $scope.mathCommand = function (val, opr) {

        if ($.trim($('#mathQuill').html()).length == 0) {
            $('#mathQuill').mathquill('editable');
            $('#mathQuill').width($('.math-editor').width());
            $('#mathQuill').height('auto');
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').mathquill('showcursor');
        }

        $scope.mathEditorPrevLatex = $('#mathQuill').mathquill('latex');

        $('#mathQuill').mathquill(opr, val);
        $('#mathQuill').focus();

        if (val === '+' || val === '-' || val === '÷' || val === '=' || val === 'times' || val === '<' || val === '>' || val === ',' || val === '.') {
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').focus();
        }

        $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');

        //$('#codecogsimg').attr('src', 'http://latex.codecogs.com/gif.latex?' + $scope.mathEditorLatex);
    };


    $scope.mathKeyCommand = function (val, opr) {

        if ($.trim($('#mathQuill').html()).length == 0) {
            $('#mathQuill').mathquill('editable');
            $('#mathQuill').width($('.math-editor').width());
            $('#mathQuill').height('auto');
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').mathquill('showcursor');
        }

        $scope.mathEditorPrevLatex = $('#mathQuill').mathquill('latex');

        $('#mathQuill').mathquill(opr, val);


        if (val === '+' || val === '-' || val === '÷' || val === '=' || val === 'times' || val === '<' || val === '>' || val === '.' || val === ',') {
            $('#mathQuill').mathquill('cmd', 'emptyspace');
        }

        if (val === '\sqrt[3]') {
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').mathquill('cmd', 'emptyspace');
        }

        $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');
        $('#mathQuill textarea').focus();

    };



    $scope.updateDataMathEditor = function (e) {

        //if (event.which == 42 || event.which == 43 || event.which == 45 || event.which == 61)
        //{
        //    $('#mathQuill').mathquill('cmd', 'emptyspace');
        //    $('#mathQuill').focus();
        //}

        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(37); //left
        specialKeys.push(38); //up
        specialKeys.push(39); //right
        specialKeys.push(40); //down

        // Added for the operators
        specialKeys.push(42); //*
        specialKeys.push(43); //+
        specialKeys.push(47); //÷
        specialKeys.push(45); //-
        specialKeys.push(61); //=

        var keyCode = e.which ? e.which : e.keyCode
        var result = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 97 && keyCode <= 122) || (keyCode >= 64 && keyCode <= 91) || specialKeys.indexOf(keyCode) != -1);
        if (!result) {
            e.preventDefault();
            return false;
        } else {

            if (specialKeys.indexOf(keyCode) != -1) {
                if (keyCode == 42) {
                    $scope.mathCommand('times', 'cmd');
                } else if (keyCode == 43) {
                    $scope.mathCommand('+', 'write');
                } else if (keyCode == 47) {
                    $scope.mathCommand('÷', 'write');
                } else if (keyCode == 45) {
                    $scope.mathCommand('-', 'write');
                } else if (keyCode == 61) {
                    $scope.mathCommand('=', 'write');
                }
                e.preventDefault();
                return false;
            }

            $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');

        }


    };

    $scope.mathRedoCommand = function () {
        $('#mathQuill').mathquill('editable');
        $('#mathQuill').width($('.math-editor').width());
        $('#mathQuill').height('auto');
        $('#mathQuill').mathquill('cmd', 'emptyspace');
        $('#mathQuill').mathquill('latex', $scope.mathEditorLatex);
        $('#mathQuill textarea').focus();
    };

    $scope.mathDeleteCommand = function () {
        $('#mathQuill').mathquill('editable');
        $('#mathQuill').width($('.math-editor').width());
        $('#mathQuill').height('auto');
        $('#mathQuill').mathquill('cmd', 'emptyspace');
        $('#mathQuill').mathquill('latex', "");
        $('#mathQuill textarea').focus();
    };

    $scope.mathUndoCommand = function () {
        $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');
        $('#mathQuill').mathquill('editable');
        $('#mathQuill').width($('.math-editor').width());
        $('#mathQuill').height('auto');
        $('#mathQuill').mathquill('cmd', 'emptyspace');
        $('#mathQuill').mathquill('latex', $scope.mathEditorPrevLatex);
        $('#mathQuill textarea').focus();
    };

    $scope.mathEditorRefactor = function () {
        // removing the unwanted duplicate cursor from matheditor
        if ($('#mathQuill .cursor').length > 1) {
            $('#mathQuill .cursor:first').remove();
        }

        if ($('#mathQuill').mathquill('latex') == "{ }") {
            $('#mathQuill').mathquill('8', 'keyStroke');
        }
    };

    $scope.getQuizDetails();
    $scope.OpenQuestionMap = function () {
        if ($('.main-body-block').hasClass('visible')) {
            closeNav();
        } else {
            $(this).addClass('visible');
            $('.main-nav').addClass('visible');
            $('.main-body-block').addClass('visible');
            $('body').addClass('overflow-hidden');

        }
        if ($('#myDropDown').hasClass('open')) {
            if (!$('#liQuestionMap').hasClass('active')) {
                $('#liQuestionMap').addClass('active');
                $('#liBookMark').removeClass('active');
                $('#Map').addClass('active');
                $('#Bookmarks').removeClass('active');
                $scope.QuestionMapBookMarkClicked = false;
                $scope.searchFilter = { QuestionStatus: '' };
            }
        }
    }

    function closeNav() {
        $('.toc-list-dropdown, .navoverlay').removeClass('visible');
        $('.main-nav').removeClass('visible');
        $('.main-body-block').removeClass('visible');
        $('body').removeClass('overflow-hidden');

    }

    //window.loadDone = function () {
    //    console.log('loadDone');
    //    //document.getElementById('compositeIframe').contentWindow.angular.element('#quizContainer').hide();
    //    var data = { "quiz": $scope.compositeQuestions, "settings": $scope.AssignmentSetting };
    //    $timeout(function () {
    //        loadiFrameWithInterval(data);
    //    });
    //}

    //window.loadiFrameWithInterval = function (data) {
    //    console.log('loadiFrameWithInterval');
    //    document.getElementById('compositeIframe').contentWindow.angular.element('.container-main').scope().startQuiz(data, true);
    //    document.getElementById('compositeIframe').contentWindow.angular.element('.container-main').scope().$apply();
    //    //document.getElementById('compositeIframe').contentWindow.angular.element('#quizContainer').show();
    //}
}])
    .directive('bindTimer', function ($compile) {
        return {
            restrict: 'A',
            link: function ($scope, el) {
                $scope.$on('add-timer', function (e) {
                    var timerHtml = '<timer autostart="false" interval="1000" countdown="' + $scope.totaltimemoving + '" finish-callback="callbackTimer.finished()"> {{hours}}:{{minutes}}:{{seconds}}</timer>';
                    var compiledHtml = $compile(timerHtml)($scope);
                    el.replaceWith(compiledHtml);
                    $scope.startTimer();
                })
            }
        }
    });


function resize_fun() {
    var window_height = $(window).innerHeight();
    var header_height = $(".main-header-block").innerHeight();
    var footer_height = $(".main-footer-block").innerHeight();
    var r_header_height = $(".main-nav-container .nav-tabs").innerHeight();
    var r_footer_height = $(".foter-navs").innerHeight();
    var body_height = window_height - (header_height + footer_height);
    var right_tab_height = body_height - (r_header_height + r_footer_height);
    $(".main-body-block").height(body_height - 1);
    $(".main-nav, .main-body-block>.container").height(body_height - 1);
    $(".main-nav, .main-body-block>.composite_container").height(body_height - 1);

    $(".main-nav-container .tab-content").height(right_tab_height - 2);

    $("#CompositeQuestionBody").css({ "max-height": (body_height - 140), "overflow-y": "auto" });
    //$("#CompositeQuestionBodyContent").css({ "max-height": (body_height - 2), "overflow-y": "auto" });
    $("#CompositeQuestionBodyContent").css({ "overflow-y": "auto" });
}


//$(document).ready(function () {
//    resize_fun();
//});
//$(window).resize(function () {
//    resize_fun();
//})
//window.onorientationchange = function () {
//    resize_fun();
//}





;app.controller('CompositeEngineCtrl', function ($scope, $window, $http, assessmentEngineSvc, Enum, $modal, $sce, $filter, $compile, $timeout, $modal, SweetAlert) {
    //$scope.template = '';

    $scope.SortableFuncConfig = {
        group: 'grswip',
        animation: 150,
        disabled:false,
        onSort: function (evt) {            
            $scope.$parent.enableDisableCheckAnswerButton(true);
        }
    };

    $scope.SingledroppableConfig = {
        group: 'grswip',
        animation: 150,
        disabled:false
        //onMove: function (evt) {
        //    //if (evt.to.childElementCount == 1) {
        //    //    return false;
        //    //}
        //    //alert(111);
        //    if ($scope.questionTypeId == Enum.QuestionType.Matching
        //        && ($scope.layoutType == 'dragdrop_parallel' || $scope.layoutType == 'horizontal' || $scope.layoutType == 'vertical' || $scope.layoutType == 'image')
        //        && evt.to.childElementCount == 1) {
        //        return false;
        //    }
        //}
    };

    $scope.SingledraggableConfig = {
        group: 'grswip',
        animation: 150,
        disabled: false
        //onMove: function (evt) {
        //    if (evt.to.childElementCount > 0 && evt.to.childElementCount < 2) {
        //        return false;
        //    }
        //}
    };

    $scope.clickHotspotForComposite = function (index,option) {
        
        if (!$scope.CheckAnswer) {
            if ($scope.Questions[index].NoOfSelection < $scope.Questions[index].MaxSelection || ($scope.Questions[index].NoOfSelection == $scope.Questions[index].MaxSelection && option.currentClass == 'selected')) {
                option.isAnswered = !option.isAnswered;
                var optindex = $scope.Questions[index].UserSelectedArea.indexOf(option.choiceid);
                if (optindex > -1) {
                    $scope.Questions[index].UserSelectedArea.splice(optindex, 1);
                    option.currentClass = '';
                    $scope.Questions[index].NoOfSelection = $scope.Questions[index].NoOfSelection - 1;
                }
                else {
                    $scope.Questions[index].UserSelectedArea.push(option.choiceid);
                    option.currentClass = 'selected';
                    $scope.Questions[index].NoOfSelection = $scope.Questions[index].NoOfSelection + 1;
                    $scope.$parent.enableDisableCheckAnswerButton(true);
                }
                if ($scope.Questions[index].NoOfSelection > 0) {
                    $scope.Questions[index].userHasSelectedOptions = true;
                    
                }
                else if ($scope.NoOfSelection == 0) {
                    $scope.Questions[index].userHasSelectedOptions = false;
                }
            }
        }
    };

    $scope.SetHotSpotTypeForComposite = function (data, index) {
        //data.globalCorrectFeedback = "";
        //data.ShowGlobalCorrectFeedback = false;
        //data.globalInCorrectFeedback = "";
        //data.ShowGlobalIncorrectFeedback = false;
        if (data.max_choice_selection.text == '0') {
            data.MaxSelection = data.choices.length;
        }
        else if (data.max_choice_selection.text == '') {
            data.MaxSelection = 1;
        }
        else {
            data.MaxSelection = parseInt(data.max_choice_selection.text);
        }
        //mehul
        if ($scope.$parent.isUndefined(data.NoOfSelection))
        {
            data.NoOfSelection = 0;
        }
        if ($scope.$parent.isUndefined(data.UserSelectedArea))
        {
            data.UserSelectedArea = [];
        }
        var imgObject = document.createElement('div');
        imgObject.innerHTML = data.question_media.media;
        data.imageSrc = $(imgObject).find('img').attr('src');
        if ($scope.isUndefined(data.imageSrc)) {
            data.imageSrc = '';
        }

        angular.forEach(data.choices, function (option, key) {
            option.XPosition = '';
            option.YPosition = '';
            option.isAnswered = false;
            option.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
            option.displayfeedback = false;
            // $scope.QuestionOptions[key].opacity = 0;

            var positions = option.text.split(',');
            if (positions.length == 2) {
                option.XPosition = positions[0];
                option.YPosition = positions[1];
            }
        });

    };


    $scope.init = function () {
        //$scope.Questions = assessmentEngineSvc.GetCompositeQuestions();
        //$scope.template = 'Composite.html';
        $scope.SingledraggableConfig.disabled = false;
        $scope.Questions = $scope.$parent.CurrentQuestion.QuestionData.questions;
        $scope.AllSelectedOptions = $scope.$parent.CompositeSelectedOption;
        $scope.enableDisableDragDropComposite(false);
        $.each($scope.Questions, function (index, value) {
            $scope.Questions[index].Index = index;
            $scope.Questions[index].userHasSelectedOptions = false;
            $scope.Questions[index].DistractorItems = [];
            $scope.Questions[index].SelectedTemplateArray = [];
            $scope.Questions[index].globalCorrectFeedback = "";
            $scope.Questions[index].ShowGlobalCorrectFeedback = false;
            $scope.Questions[index].globalInCorrectFeedback = "";
            $scope.Questions[index].ShowGlobalIncorrectFeedback = false;
            if (!$scope.isUndefined(value.stem_details) && value.stem_details.type == 'algorithimic') {
                $scope.SetAlgorithimicType(value);
            }
            if (value.question_type.text == Enum.QuestionType.FIB || value.question_type.text == Enum.QuestionType.FIBDND || value.question_type.text == Enum.QuestionType.Matching) {
                $scope.SetFIBType(value, index);
            } else if (value.question_type.text == Enum.QuestionType.LTD || value.question_type.text == Enum.QuestionType.LTDDND) {
                $scope.SetLTDType(value, index);
            }
            else if (value.question_type.text == Enum.QuestionType.Essay) {
                $scope.Questions[index].EssayText = '';
            } else if (value.question_type.text == Enum.QuestionType.TextSelection) {
                $scope.SetTextSelectionType(value, index);
            }
            else if (value.question_type.text == Enum.QuestionType.HotSpot)
            {
                $scope.SetHotSpotTypeForComposite(value,index);
            }
            angular.forEach(value.choices, function (option, key) {
                $scope.Questions[index].choices[key].currentClass = "";
                $scope.Questions[index].choices[key].QuestionIndex = index;
                $scope.Questions[index].choices[key].CurrentOptionIndex = key;
            });

            if ($scope.AllSelectedOptions != null && !$scope.isUndefined($scope.AllSelectedOptions) && $scope.AllSelectedOptions.length >= index) {
                $scope.Questions[index].EssayText = '';
                $scope.setCompositeQuestionOptions(value, $scope.AllSelectedOptions[index].UserSelectedOptions, index);
            }
        });
        $scope.$parent.compositeQuestions = $scope.Questions;

    };

    $scope.CheckInputValue = function (e, value) {
        var keyCode = e.which ? e.which : e.keyCode;
        value.AnswerClass = "";
        var decimalplaces = (value.decimalplace == "") ? 0 : value.decimalplace;
        if (!$scope.ValidateUserInput(keyCode, value.inputtype, decimalplaces, value)) {
            e.preventDefault();
            return false;
        }
    };
    $scope.ValidateUserInput = function (keyCode, inputtype, maxDecimals, value) {
        var resultType = false;
        var regexp = "";
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(37); //left
        specialKeys.push(38); //up
        specialKeys.push(39); //right
        specialKeys.push(40); //down
        specialKeys.push(9); //Tab key
        specialKeys.push(32);//space bar
        switch (inputtype) {
            case 'alphanumeric':
                if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                    resultType = true;
                break;
            case 'numeric':
                if (maxDecimals > 0) {
                    specialKeys.push(46); //decimal
                    regexp = "^-?\\d*\\.?\\d{0," + maxDecimals + "}$";
                } else {
                    regexp = "^-?\\d*$";
                }
                if ((keyCode >= 48 && keyCode <= 57) || (specialKeys.indexOf(keyCode) != -1)) {
                    var validRegex = new RegExp(regexp);
                    resultType = validRegex.test(value.UserInput);
                }
                break;
            case 'alphabet':
                if ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                    resultType = true;
                break;
            default:
                break;
        }
        return resultType;
    };

    $scope.$on('nextCompositeQuestion', function () {
        //console.log('nextCompositeQuestion');
        $scope.init();
    })
    $scope.$on('resetCompositeQuestion', function () {
        //console.log('nextCompositeQuestion');
        $scope.ResetCompositeQuestions();
    })

    $scope.$on('enableDisableDragDropComposite', function (value) {
        //console.log('nextCompositeQuestion');
        $scope.enableDisableDragDropComposite(value);
    })

    $scope.enableDisableDragDropComposite = function (value) {
        $scope.SortableFuncConfig.disabled = value;
        $scope.SingledraggableConfig.disabled = value;
        $scope.SingledroppableConfig.disabled = value;
    }

    $scope.ResetCompositeQuestions = function () {
        $scope.enableDisableDragDropComposite(false);
        $.each($scope.Questions, function (index, question) {
            question.globalCorrectFeedback = "";
            question.ShowGlobalCorrectFeedback = false;
            question.globalInCorrectFeedback = "";
            question.ShowGlobalIncorrectFeedback = false;
            $('.fdback').addClass('ng-hide');
            $('.fdback - mobile').addClass('ng-hide');
           
            if (question.question_type.text == Enum.QuestionType.FIBDND) {
                $scope.SetFIBType(question, index);
            }
            $(question.choices).each(function (opIndex, value) {
                value.showOptionFeedbackModal = false;
                value.showAttempt = false;
                if (question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND || question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
                    this.isAnswered = false;
                    this.AnswerClass = "";
                    if (!!value.UserInput) value.UserInput = "";
                    if (!!value.Canvas && value.Canvas.length > 0) {
                        $(value.Canvas).each(function (cindex, cvalue) {
                            cvalue.AnswerClass = "";
                            cvalue.selectedClass = "";
                            question.droppableItems.push(cvalue);
                        });
                        value.Canvas = [];

                    }
                    value.multiSelectItem = [];
                    $('li').removeClass('incorrect');
                    $('li').removeClass('correct');
                    $('#divltd' + question.Index + value.key).html('');
                    $('#divltd' + question.Index + value.key).hide();
                }
                else if (question.question_type.text == Enum.QuestionType.FIBDND) {
                    $("div[data-question-id='" + value.questionIndex + "']").empty();
                    $('#divltd' + question.Index + value.key).html('');
                    $('#divltd' + question.Index + value.key).hide();
                    //var selectedValue = question.SelectedTemplateArray.filter(function (x) { return x.dropId == opIndex; });
                    //if (selectedValue.length > 0) {
                    //    $(selectedValue).each(function (sindex, svalue) {
                    //        if (svalue != "") {
                    //            if (question.droppableItems.filter(function (x) { return $filter('lowercase')(x.title) == $filter('lowercase')('<span math dynamic>' + svalue.title + '</span>'); }).length == 0) {
                    //                question.droppableItems.push({ 'title': svalue.title, 'drag': true, selectedClass: '' });
                    //            }
                    //            svalue.dropId = "initial";
                    //        }
                    //    });
                    //}
                }
                else if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
                    value.isChoiceAnswered = false;
                    $(value.suboption).each(function (sindex, svalue) {
                        svalue.currentClass = "";
                        svalue.isAnswered = "";
                        svalue.isOptionAnswered = false;
                        value.suboptionSelected = "";
                    });
                    $('#divltd' + value.QuestionIndex + value.choiceid).html('');
                    $('#divltd' + value.QuestionIndex + value.choiceid).hide();
                } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
                    value.AnswerClass = "";
                }
                else if (question.question_type.text == Enum.QuestionType.Order) {
                    value.isCorrect = false;
                    value.currentClass = "";
                }
                else if (question.question_type.text == Enum.QuestionType.HotSpot) {
                    $('#divltd' + value.QuestionIndex + value.choiceid).html('');
                    $('#divltd' + value.QuestionIndex + value.choiceid).hide();
                    value.isAnswered = false;
                    this.isAnswered = false;
                    this.currentClass = "";
                }
                else {
                    this.isAnswered = false;
                    this.currentClass = "";
                }
            });
            if (question.question_type.text == Enum.QuestionType.Order) {
                question.choices.sort(function (a, b) {
                    if (a.CurrentOptionIndex < b.CurrentOptionIndex) return -1;
                    if (a.CurrentOptionIndex > b.CurrentOptionIndex) return 1;
                    return 0;
                })
            } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
                question.NoOfSelection = 0;
                question.userHasSelectedOptions = false;
            }
            else if (question.question_type.text == Enum.QuestionType.HotSpot) {
                question.NoOfSelection = 0;
                question.userHasSelectedOptions = false;
                question.UserSelectedArea = [];
            }
        });
    };
    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.trustAsHtml = function (string) {
        return $sce.trustAsHtml(string);
    };

    $scope.setOptions = function (option, OptionId, questionIndex) {
        $(".option").removeClass('correct');
        $(".option").removeClass('incorrect');
        var questionType = '';
        if (!$scope.isUndefined(option.QuestionIndex)) {
            questionType = $scope.Questions[option.QuestionIndex].question_type.text;
        }
        else {
            questionType = $scope.Questions[questionIndex].question_type.text;
        }
        switch (questionType) {
            case Enum.QuestionType.MCSS:
            case Enum.QuestionType.GRAPHIC:
            case Enum.QuestionType.Exhibit:
                $($scope.Questions[option.QuestionIndex].choices).each(function (index) {
                    this.currentClass = "";
                    if (this.choiceid == option.choiceid) {
                        this.isAnswered = true;
                        option.currentClass = "selected";
                    }
                    else {
                        this.isAnswered = false;
                    }
                    this.isButtonClicked = true;
                    if (this.isAnswered) {
                        $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                        $scope.$parent.enableDisableCheckAnswerButton(true);
                        this.currentClass = "selected";
                    }
                });

                break;
            case Enum.QuestionType.MCMSVIDEO:
            case Enum.QuestionType.MCMS:
                $($scope.Questions[option.QuestionIndex].choices).each(function (index) {
                    if (this.choiceid == option.choiceid) {
                        if (this.isAnswered == true) {
                            this.isAnswered = false;
                            option.currentClass = "";
                        }
                        else {
                            this.isAnswered = true;
                            option.currentClass = "selected";
                        }
                    }
                    if (this.isAnswered == true) {
                        this.currentClass = "selected";
                        $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                        $scope.$parent.enableDisableCheckAnswerButton(true);
                    }
                });
                break;
            case Enum.QuestionType.ChoiceMatrix:
                $($scope.Questions[questionIndex].choices).each(function (index, value) {
                    $scope.Questions[questionIndex].selectedOption = OptionId;

                    $(value.suboption).each(function (sindex, soption) {
                        if (value.choiceid == OptionId) {
                            value.isChoiceAnswered = true;
                            if (option.identifier == soption.identifier) {
                                this.isOptionAnswered = true;
                                value.isAnswered = value.assess;
                                value.suboptionSelected = soption.identifier;
                                soption.isAnswered = soption.value;
                                soption.currentClass = "selected";
                            }
                            else {
                                this.isOptionAnswered = false;
                                soption.currentClass = "";
                                soption.isAnswered = !soption.value;
                            }
                        }
                        if (this.isOptionAnswered == true) {
                            this.currentClass = "selected";
                            $scope.Questions[questionIndex].userHasSelectedOptions = true;
                            $scope.$parent.enableDisableCheckAnswerButton(true);
                        }
                    });
                });
                break;
            case Enum.QuestionType.Order:
                $(option).each(function (index) {
                    this.isAnswered = true;
                    if (this.isAnswered) {
                        $scope.Questions[questionIndex].userHasSelectedOptions = true;
                        $scope.$parent.enableDisableCheckAnswerButton(true);
                    }
                });
                break;
            case Enum.QuestionType.LTD:
            case Enum.QuestionType.LTDDND:
            case Enum.QuestionType.FIB:
            case Enum.QuestionType.FIBDND:
            case Enum.QuestionType.Matching:
                var totalChoices = $scope.Questions[option.QuestionIndex].choices.length;
                var objList = $filter('filter')($scope.Questions[option.QuestionIndex].choices, { "Canvas": [] }, true);
                var objList2 = $filter('filter')($scope.Questions[option.QuestionIndex].choices, { "UserInput": "" }, true);
                if (objList.length > 0 && objList.length < totalChoices) {
                    $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                    $scope.$parent.enableDisableCheckAnswerButton(true);
                }
                if (objList2.length < totalChoices) {
                    $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                    $scope.$parent.enableDisableCheckAnswerButton(true);
                }
                break;
        }
    }

    $scope.setCompositeQuestionOptions = function (question, selectedOptions, questionIndex) {

        $scope.text = '';
        if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
            if (selectedOptions) {
                var selectedOpt = angular.fromJson(selectedOptions);
                $(selectedOpt).each(function (selectindex, selectvalue) {
                    var inputValues = selectvalue.inputvalue.split("-");
                    $(question.choices).each(function (index) {
                        var optionindex = index + 1;
                        $(question.choices[index].suboption).each(function (index1, value1) {
                            question.choices[index].suboption[index1].currentClass = "";
                            if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                                question.choices[index].suboption[index1].isAnswered = true;
                            }
                        });

                    });
                });

            }

        }
        if (question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND || question.question_type.text == Enum.QuestionType.Matching || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.FIB) {
            if (selectedOptions) {
                var selctdOpt = angular.fromJson(selectedOptions);
                angular.forEach(question.choices, function (option, key) {
                    $(selctdOpt).each(function (fibindex, value) {
                        if ((option.interaction == 'textentry' || option.interaction == 'inline') && value.noforeachblank == option.noforeachblank) {
                            $scope.Questions[questionIndex].choices[key].UserInput = value.inputvalue;
                        }
                        else if (option.interaction == 'dragdrop' && value.noforeachblank == option.noforeachblank) {
                            angular.forEach($scope.Questions[questionIndex].droppableItems, function (ditem, dindex) {
                                if ($filter('lowercase')(ditem.title) == $filter('lowercase')(value.inputvalue)) {
                                    //$scope.droppableItems[dindex].title = "";
                                    $scope.Questions[questionIndex].choices[key].Canvas = [{ 'title': value.inputvalue, 'drag': true, selectedClass: '' }];
                                    $scope.Questions[questionIndex].droppableItems.splice(dindex, 1);
                                    dindex = dindex - 1;
                                }
                            });
                        }
                        else if (option.interaction == 'container' && value.noforeachblank == option.noforeachblank) {
                            var input = value.inputvalue.map(function (x) { return x.title; });
                            for (var i = 0; i < $scope.Questions[questionIndex].droppableItems.length; i++) {
                                if (input.indexOf($scope.Questions[questionIndex].droppableItems[i].title) != -1) {
                                    $scope.Questions[questionIndex].choices[key].Canvas.push({ 'title': $scope.Questions[questionIndex].droppableItems[i].title, 'drag': true, selectedClass: '' });
                                    $scope.Questions[questionIndex].choices[key].multiSelectItem.push($($scope.Questions[questionIndex].droppableItems[i].title).text());
                                    $scope.Questions[questionIndex].droppableItems.splice(i, 1);
                                    i = i - 1;
                                }
                            }
                        }
                    });
                });

            }

        }
        else if (question.question_type.text == Enum.QuestionType.Essay) {

                if (selectedOptions) {
                    //$scope.essayTemplateText = data.SelectedOptions;
                    if (question.toolbar.type == "math_3-5_palette" || question.toolbar.type == "math_6-8_palette") {
                        if ($scope.operationmode == Enum.OperationMode.Offline) {
                            question.mathEditorPrevLatex = $.trim(selectedOptions[0].replace("?", ""));
                            question.mathEditorType = question.toolbar.type;
                        }
                        else {
                            question.mathEditorPrevLatex = $.trim(selectedOptions.replace("?", ""));
                            question.mathEditorType = question.toolbar.type;
                        }
                    } else {
                        $("#txtEssayContent" + questionIndex).val(selectedOptions);
                        question.EssayText = selectedOptions;
                    }
                }
            }
        else if (question.question_type.text == Enum.QuestionType.TextSelection) {
            if (selectedOptions) {
                var userSelectedOptions = "";
                var selctdOpt = angular.fromJson(selectedOptions);
                $(selctdOpt).each(function (sindex, svalue) {
                    if (userSelectedOptions == "") {
                        userSelectedOptions = svalue.choiceid;
                    }
                    else {
                        userSelectedOptions = userSelectedOptions + "," + svalue.choiceid;
                    }
                });
                question.SelectedOptions = userSelectedOptions;
            }
        }
        else if (question.question_type.text == Enum.QuestionType.HotSpot) {
            if (!$scope.$parent.isUndefined(question.question_media.media) && question.question_media.media.length > 0 && question.question_media.media != "") {
                //if (data.userHasSelectedOptions) {
                var selectedoptions = $scope.AllSelectedOptions;
                if (selectedoptions != null) {
                    var usrSelectedObj = JSON.parse(selectedoptions[question.Index].UserSelectedOptions)
                    $(question.choices).each(function (counter, value) {
                        $(usrSelectedObj).each(function (opCounter, opValue)
                            {
                            if (value.choiceid == opValue.choiceid) {
                                value.isAnswered = true;
                                value.currentClass = 'selected';
                            }
                    });

                    });
                }
            }
        }
        else {
            var selctdoptions = '';
            $(question.choices).each(function (index) {
                //data.QuestionData.choices[index].isAnswered = false;
                //if (!$scope.isUndefined(data.QuestionData.image) && data.QuestionData.image.image.indexOf('http') === -1 && data.QuestionData.image.image != "") {
                //    data.QuestionData.choices[index].image = "Content/images/" + data.QuestionData.choices[index].image;
                //}
                question.choices[index].showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                question.choices[index].displayfeedback = false;
                var optionindex = index + 1;
                question.choices[index].CurrentOptionIndex = optionindex;
                if (selectedOptions) {
                    selctdoptions = selectedOptions.split(",");
                    $(selctdoptions).each(function (selectindex) {
                        question.choices[index].SelectedOptions = parseInt(selctdoptions[selectindex]);
                        if (optionindex == parseInt(selctdoptions[selectindex])) {
                            question.choices[index].isAnswered = true;
                            //question.choices[index].currentClass = "selected";

                        }
                    });
                }
            });

        }
        // return data;
    }

    $scope.SetAlgorithimicType = function (question) {
        var textReplaced = false;
        if ($scope.isUndefined(question.AlgorithmicVariableArray)) {
            question.AlgorithmicVariableArray = [];
        }
        if (question.stem_details.textforeachvalue.length > 0) {
            angular.forEach(question.stem_details.textforeachvalue, function (textDetail, key) {
                if (textDetail.type == 'random') {
                    if (question.AlgorithmicVariableArray.length == angular.fromJson(question.stem_details.textforeachvalue).length) {
                        angular.forEach(question.AlgorithmicVariableArray, function (arrayVariable, akey) {
                            question.question_stem.text = question.question_stem.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        });
                    }
                    else {
                        var random = Math.floor((Math.random() * textDetail.maxvalue) + textDetail.minvalue);
                        question.question_stem.text = question.question_stem.text.replace(textDetail.text, random);
                        question.AlgorithmicVariableArray.push({ "TextToReplace": textDetail.text, "TextValue": random });
                    }
                }
            });

            angular.forEach(question.choices, function (option, key) {
                angular.forEach(question.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    if (isNaN(option.text) && option.text.indexOf(arrayVariable.TextToReplace) != -1) {
                        option.text = option.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        textReplaced = true;
                    }
                });
                if (question.stem_details.display == 'result' && textReplaced) {
                    option.text = eval(option.text);
                }
            });
        }
    };

    $scope.SetFIBType = function (data, index) {
        var questiontext = "";//data.QuestionData.textwithblanks.text;
        if (data.textwithblanks != undefined) {
            if (data.textwithblanks.text != undefined && data.textwithblanks.text != "")
                questiontext = data.textwithblanks.text;
        }

        //Array object to hold draggable item values
        $scope.Questions[index].droppableItems = [];
        $scope.Questions[index].FIBText = "";
        $scope.Questions[index].layoutType = ($scope.isUndefined(data.question_layout)) ? "" : data.question_layout.text;
        $scope.FillDroppableItems(data, index);
        angular.forEach(data.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            $scope.Questions[index].choices[key].key = key;       // Index of option
            $scope.Questions[index].choices[key].UserInput = "";  //User input value of textbox and dropdown value.         
            $scope.Questions[index].choices[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            $scope.Questions[index].choices[key].width = 'auto';
            $scope.Questions[index].choices[key].multiSelectItem = [];
            $scope.Questions[index].choices[key].FibClass = "";
            $scope.Questions[index].choices[key].accept = true;
            $scope.Questions[index].choices[key].showOptionFeedbackModal = $scope.$parent.showOptionFeedbackModal;

            questiontext = questiontext.replace(textToReplace, "<fibvariation ng-model='$parent.Questions[" + index + "].choices[" + key + "]' parent-ctrl='$parent' user-input='Questions[" + index + "].choices[" + key + "].UserInput'></fibvariation>");

            $scope.$watch("Questions[" + index + "].choices[" + key + "].Canvas.length", function (newValue, oldValue) {
                if ($scope.Questions.length > 0) {
                    if (index < $scope.Questions.length) {
                        $scope.setOptions($scope.Questions[index].choices[key], null);
                    }
                }
            });

            if ((option.interaction == 'textentry' || option.interaction == 'inline')) {
                $scope.Questions[index].choices[key].FibClass = "fibclass";
            }
        });
        $scope.Questions[index].FIBText = questiontext;
        if (!$scope.isUndefined($scope.Questions[index].layoutType)) {
            if ($scope.Questions[index].layoutType == 'right' || $scope.Questions[index].layoutType == 'left' || $scope.Questions[index].layoutType == 'vertical') {
                $scope.Questions[index].droptype = "";
                $scope.Questions[index].dragtype = "dragpanel col-sm-4 col-md-4 right";
            }
            else if ($scope.Questions[index].layoutType == 'row') {
                $scope.Questions[index].droptype = "";
                $scope.Questions[index].dragtype = "dragpanel text-center";
            }
            else {
                $scope.Questions[index].dragtype = "dragpanel";
                $scope.Questions[index].droptype = "";
            }
        }
    };

    $scope.SetTextSelectionType = function (data, index) {
        var questiontext = "";
        $scope.Questions[index].PassageText = "";
        $scope.Questions[index].showExhibit = false;
        $scope.Questions[index].MaxSelection = 0;
       
        $scope.Questions[index].userHasSelectedOptions = false;

        if (data.max_choice_selection.text == '0') {
            $scope.Questions[index].MaxSelection = data.choices.length;
        }
        else if (data.max_choice_selection.text == '') {
            $scope.Questions[index].MaxSelection = 1;
        }
        else {
            $scope.Questions[index].MaxSelection = parseInt(data.max_choice_selection.text);
        }
        $scope.Questions[index].NoOfSelection = 0;
        if (data.textwithblanks != undefined) {
            if (data.textwithblanks.text != undefined && data.textwithblanks.text != "")
                questiontext = data.textwithblanks.text;
        }
        if (!$scope.isUndefined(data.exhibit) && data.exhibit.length > 0 && data.exhibit[0].exhibit_type != "") {
            $scope.Questions[index].showExhibit = true;
            if (data.exhibit[0].exhibit_type == 'image') {
                var imagePath = String(data.exhibit[0].path);
                data.exhibit[0].path = imagePath;
            }
        }


        angular.forEach(data.choices, function (option, key) {

            $scope.Questions[index].choices[key].showAttempt = false;
            $scope.Questions[index].choices[key].showOptionFeedbackModal = false;

            $scope.Questions[index].choices[key].key = key;       // Index of option            
            $scope.Questions[index].choices[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            if (!$scope.isUndefined(option.text) && option.text != "" && option.text.match("^\[\[[0-9]*\]\]")) {
                var startTagToReplace = option.text;
                var endTagToReplace = option.text.substring(0, 2) + "/" + option.text.substring(2);
                questiontext = questiontext.replace(startTagToReplace, "&nbsp;&nbsp;<span class='option selectable' style='cursor: pointer;' ng-click='$parent.SetSelectedClass(" + key + "," + index + ")' ng-class='$parent.Questions[" + index + "].choices[" + key + "].AnswerClass'> <span class='sign'><span class='sr-only'></span><span class='fa'></span></span><span class='fdback fdback-desktop' ng-show='$parent.Questions[" + index + "].choices[" + key + "].showOptionFeedbackModal && $parent.Questions[" + index + "].choices[" + key + "].displayfeedback'><span class='sr-only'></span><span id='spnltd" + key + "'class='fa fa-commenting-o' popover popover-html='" + $scope.Questions[index].choices[key].choiceid + "' ng-click='$parent.GetFeedbackForComposite($parent.Questions[" + index + "].choices[" + key + "],\"" + $scope.Questions[index].question_type.text + "\")'></span></span>");
                //questiontext = questiontext.replace(startTagToReplace, "&nbsp;&nbsp;<span id='spnltd" + key + "' class='option selectable' style='cursor: pointer;' ng-click='$parent.SetSelectedClass(" + key + ")' ng-class='$parent.QuestionOptions[" + key + "].AnswerClass'> <span class='sign'><span class='sr-only'></span><span class='fa'></span></span><span class='fdback' ng-show='$parent.QuestionOptions[" + key + "].showOptionFeedbackModal && $parent.QuestionOptions[" + key + "].showAttempt'><span class='sr-only'></span><span class='fa fa-commenting-o' popover popover-html='" + $scope.QuestionOptions[key].choiceid + "' ng-click='$parent.GetFeedback($parent.QuestionOptions[" + key + "])'></span></span>");
                questiontext = questiontext.replace(endTagToReplace, "</span>&nbsp;&nbsp;");
            }
        });
        data.PassageText = questiontext;
    };

    $scope.SetSelectedClass = function (key, index) {
        if (!$scope.$parent.CheckAnswer) {
            if ($scope.Questions[index].NoOfSelection < $scope.Questions[index].MaxSelection || ($scope.Questions[index].NoOfSelection == $scope.Questions[index].MaxSelection && $scope.Questions[index].choices[key].AnswerClass == 'selectedC')) {
                if ($scope.Questions[index].choices[key].AnswerClass == 'selectedC') {
                    $scope.Questions[index].choices[key].AnswerClass = "";
                    $scope.Questions[index].NoOfSelection = $scope.Questions[index].NoOfSelection - 1;
                }
                else {
                    $scope.Questions[index].choices[key].AnswerClass = "selectedC";
                    $scope.Questions[index].NoOfSelection = $scope.Questions[index].NoOfSelection + 1;
                }
            }
            if ($scope.Questions[index].NoOfSelection > 0) {
                $scope.Questions[index].userHasSelectedOptions = true;
                $scope.$parent.enableDisableCheckAnswerButton(true);
            }
            else if ($scope.Questions[index].NoOfSelection == 0) {
                $scope.Questions[index].userHasSelectedOptions = false;
            }
        }
    };

    $scope.CheckDropItem = function (event, index, item, type) {
        if (item.QuestionIndex != index) {
            return false;
        }
        else {
            if ($scope.Questions[index].question_type.text == Enum.QuestionType.Order) {
                $scope.setOptions($scope.Questions[index].choices, null, index);
            }
            return item;
        }
    },

    $scope.FillDroppableItems = function (data, index) {
        $scope.Questions[index].SelectedTemplateArray = [];
        isDistractorsPushDone = false;
        angular.forEach(data.choices, function (option, key) {
            $scope.Questions[index].choices[key].arrList = [];
            if (option.interaction == "inline") {
                angular.forEach(option.distractorforeachblank, function (titem, tindex) {
                    if (titem.text && titem.text.trim().length > 0) {
                        $scope.Questions[index].choices[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                    }
                });
            }

            if (option.interaction == "dragdrop" || option.interaction == "container") {
                if (isDistractorsPushDone == false) {
                    angular.forEach($scope.$parent.CurrentQuestion.QuestionData.questions[index].distractors, function (titem, tindex) {
                        if (titem.text && titem.text.trim().length > 0) {
                            $scope.Questions[index].choices[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                            $scope.Questions[index].DistractorItems.push({ id: tindex + 1, title: titem.text });
                            $scope.Questions[index].SelectedTemplateArray.push({ dropId: 'initial', title: titem.text });
                        }
                    });
                }
                isDistractorsPushDone = true;
            }

            angular.forEach(option.textforeachblank, function (titem, tindex) {
                if (titem.text) {
                    $scope.Questions[index].choices[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                    $scope.Questions[index].DistractorItems.push({ id: tindex + 1, title: titem.text });
                    $scope.Questions[index].SelectedTemplateArray.push({ dropId: 'initial', title: titem.text });
                }
            });

            if (option.interaction == "dragdrop" || option.interaction == "container") {
                angular.forEach($scope.Questions[index].choices[key].arrList, function (item, cindex) {
                    $scope.Questions[index].droppableItems.push({ 'title': item, 'drag': true, selectedClass: '', 'QuestionIndex': index });
                });
                $scope.Questions[index].choices[key].Canvas = [];
                $scope.Questions[index].choices[key].Drop = true;
            }
        });
    };

    /*Dev*/
    $scope.SetItemToDropCanvasList = function (option) {
        if ($window.innerWidth <= 767) {
            if ($scope.Questions[option.QuestionIndex].choices[option.CurrentOptionIndex].Canvas.length == 0) {
                $scope.$parent.SetItemToDropCanvasListComposite(option.key, $scope.Questions[option.QuestionIndex].droppableItems);
            }
        }
    };


    //$scope.AddItemToDropItemList = function (item, key, index) {
    //    if ($window.innerWidth <= 767) {
    //        if ($scope.Questions[key].Canvas.length == 1) {
    //            $scope.droppableItems.push({ 'title': item.title, 'drag': true, selectedClass: '' });
    //            $scope.Questions[key].Canvas.splice(index, 1);
    //            $scope.DropCanvasListIndex = key;
    //            $scope.userHasSelectedOptions = true;
    //        }
    //    }
    //};
    /*Dev*/

    $scope.isUndefined = function (thing) {
        return (typeof thing === "undefined");
    };

    $scope.getValue = function (text, index) {
        $scope.Questions[index].essayTemplateText = text;
    }

    $scope.OnDragStart = function (option) {
        option.AnswerClass = "";
    };

    $scope.AddDropCssOnOver = function (key, questionIndex) {
        $('#divDropOption' + key).addClass('active');
        angular.forEach($scope.Questions[questionIndex].choices[key].Canvas, function (option, key) {
            option.AnswerClass = '';
        });
        return true;
    };

    $scope.RemoveDropCssOnLeave = function (questionIndex) {
        angular.forEach($scope.Questions[questionIndex].choices, function (option, key) {
            $('#divDropOption' + key).removeClass('active');
        });
    };

    $scope.AddCssOnOver = function (item) {
        item.selectedClass = 'dragging';
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveSelected = function () {
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveCssOnOut = function (droppableItems) {
        angular.forEach(droppableItems, function (item, key) {
            item.selectedClass = '';
        });
    };

    var popovermodel;
    $scope.GetFeedbackForComposite = function (model, questiotype) {
        var feedbackText = '';
        var currentClass = '';
        popovermodel = '';
        $('body').popover("destroy");
        popovermodel = model;
        if (questiotype == Enum.QuestionType.TextSelection || questiotype == Enum.QuestionType.Matching || questiotype == Enum.QuestionType.LTDDND || questiotype == Enum.QuestionType.FIBDND) {
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {
                    if (questiotype == Enum.QuestionType.Matching && popovermodel.interaction.indexOf("container") > -1) {
                        if (popovermodel.AnswerClass.indexOf("incorrect") > -1) {
                            feedbackText = popovermodel.incorrect_feedback;
                        }
                        else {
                            feedbackText = popovermodel.correct_feedback;
                        }
                    }
                    else {
                        if (popovermodel.AnswerClass.indexOf("incorrect") > -1) {
                            feedbackText = popovermodel.incorrect_feedback;
                        }
                        else {
                            feedbackText = popovermodel.correct_feedback;
                        }
                    }
                    if ($('.fdback-mobile').is(':visible')) {
                        $('#divltd' + model.QuestionIndex + model.choiceid).html('<span>' + (model.choiceid) + ". " + feedbackText + '</span>');
                        $('#divltd' + model.QuestionIndex + model.choiceid).show();
                    }
                    else {
                        return feedbackText;
                    }
                },//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                placement: "top"
            });
        }
        else if (questiotype == Enum.QuestionType.ChoiceMatrix) {
            var cssClass = "";
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {
                    if (popovermodel.suboption[popovermodel.suboptionSelected - 1].currentClass.indexOf("incorrect") > -1) {
                        feedbackText = popovermodel.incorrect_feedback;
                        cssClass = "incorrect";
                    }
                    else {
                        feedbackText = popovermodel.correct_feedback;
                        cssClass = "correct";
                    }
                    
                    if ($('.fdback-mobile').is(':visible')) {
                       
                        $('#divltd' + model.QuestionIndex + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                        $('#divltd' + model.QuestionIndex + model.choiceid).addClass(cssClass);
                        $('#divltd' + model.QuestionIndex + model.choiceid).show();
                    }
                    else {
                        return feedbackText;
                    }
                },
                placement: "top"
            });
        }
        else if (questiotype == Enum.QuestionType.MCSS || questiotype == Enum.QuestionType.MCMS || questiotype == Enum.QuestionType.Order || questiotype == Enum.QuestionType.HotSpot) {
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {
                    if (popovermodel.currentClass.indexOf("incorrect") > -1) {
                        feedbackText = popovermodel.incorrect_feedback;
                    }
                    else {
                        feedbackText = popovermodel.correct_feedback;
                    }
                    if ($('.fdback-mobile').is(':visible')) {
                        $('#divltd' + model.QuestionIndex + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                        $('#divltd' + model.QuestionIndex + model.choiceid).show();
                    }
                    else {
                        return feedbackText;
                    }
                },//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                placement: "top"
            });            
        }

    }

    $scope.SetLTDType = function (data, index) {
        data.QuestionOptionsList = [];
        data.droppableItems = [];
        data.layoutType = ($scope.isUndefined(data.question_layout)) ? "" : data.question_layout.text;

        var imgObject = document.createElement('div');
        imgObject.innerHTML = data.question_media.media;
        data.imageSrc = $(imgObject).find('img').attr('src');

        $scope.FillDroppableItems(data, index);
        data.QuestionOptions = data.choices;

        angular.forEach(data.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            data.QuestionOptions[key].key = key;       // Index of option
            data.QuestionOptions[key].UserInput = "";  //User input value of textbox and dropdown value.         
            data.QuestionOptions[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            data.QuestionOptions[key].width = 'auto';
            data.QuestionOptions[key].showOptionFeedbackModal = data.showOptionFeedbackModal;
            data.QuestionOptions[key].showAttempt = data.showAttempt;
            data.QuestionOptions[key].ShowGlobalFeedback = data.ShowGlobalFeedback;
            data.QuestionOptions[key].XPosition = '';
            data.QuestionOptions[key].YPosition = '';
            data.QuestionOptions[key].LTDText = '';
            data.QuestionOptions[key].accept = true;

            var positions = option.noforeachblank.split(',');
            if (positions.length == 2) {
                data.QuestionOptions[key].XPosition = positions[0];
                data.QuestionOptions[key].YPosition = positions[1];
            }
            if (data.SelectedOptions) {
                var selectedOption = angular.fromJson(data.SelectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if ((option.interaction == 'textentry' || option.interaction == 'inline') && value.noforeachblank == option.noforeachblank) {
                        data.QuestionOptions[key].UserInput = value.inputvalue;
                    }
                    else if (option.interaction == 'dragdrop' && value.noforeachblank == option.noforeachblank) {
                        angular.forEach(data.droppableItems, function (ditem, dindex) {
                            if ($filter('lowercase')(ditem.title) == $filter('lowercase')(value.inputvalue)) {
                                //$scope.droppableItems[dindex].title = "";
                                data.QuestionOptions[key].Canvas = [{ 'title': value.inputvalue, 'drag': true, selectedClass: '' }];
                                data.droppableItems.splice(dindex, 1);
                                dindex = dindex - 1;
                            }
                        });
                    }
                });
            }
            data.QuestionOptionsList.push(option);
        });

        if (!$scope.isUndefined(data.layoutType)) {
            if (data.layoutType == 'right' || data.layoutType == 'left' || data.layoutType == 'vertical') {
                data.droptype = "col-sm-9 col-md-9";
                data.dragtype = "dragpanel col-sm-3 col-md-3 right";
            }
            else if (data.layoutType == 'row') {
                data.droptype = "";
                data.dragtype = "dragpanel text-center";
            }
            else {
                data.dragtype = "dragpanel";
                data.droptype = "";
            }
        }
    };

    $scope.init();

});

;app.factory('servicehelper', ['$http', '$resource', function ($http, $resource) {
    var baseUrl = Urls.restUrl;
    var routeUrl = Urls.routeUrl;
    var buildUrl = function (resourceUrl) {
        return baseUrl + format(resourceUrl);
    };


    var format = function (text) {
        var arguments = ['v1'];
        if (!text) return text;
        for (var i = 1; i <= arguments.length; i++) {
            var pattern = new RegExp("\\{" + (i - 1) + "\\}", "g");
            text = text.replace(pattern, arguments[i - 1]);
        }
        return text;
    };

    return {
        AuthorizationToken: $resource(Urls.restAuthUrl, null,
        {
            login: {
                method: 'post',
                url: Urls.restAuthUrl,
                isArray: false
            }
        }),
        Product: $resource(buildUrl('/{0}/{0}.Product/GetProductList'), null,
            {
                GetProductList: {
                    method: 'post',
                    url: buildUrl('/{0}/{0}.Product/GetProductList'),
                    isArray: true
                }
            }),
        Assessment: $resource(buildUrl('/{0}/{0}.Assessment/'), null,
            {
                StartQuiz: {
                    method: 'post',
                    url: buildUrl('/{0}/{0}.Assessment/StartQuiz')

                },
                FetchQuestion: {
                    method: 'post',
                    url: buildUrl('/{0{0}/}.Assessment/FetchQuestion')

                },
                GenerateFeedback: {
                    method: 'post',
                    url: buildUrl('/{0}/{0}.Assessment/GenerateFeedback')
                },
                AutoSubmit: {
                    method: 'post',
                    url: buildUrl('/{0}/{0}.Assessment/AutoSubmit')
                },
                FetchQuizDetails: {
                    method: 'post',
                    url: buildUrl('/{0}/{0}.Assessment/FetchQuizDetails')
                }

            }),
        Account: $resource(buildUrl('/{0}/{0}.Account'), null, {
            CreateAccount: {
                method: 'post',
                url: buildUrl('/{0}/{0}.Account/CreateAccount')
            }
        })

    };
}]);
;app.factory('assessmentEngineSvc', ['$http', '$resource', 'servicehelper',
      function ($http, $resource, servicehelper) {
          var assessment = servicehelper.Assessment;
          var StartQuiz = function (params) {
              //var data = serializeData(loginData);
              return assessment.StartQuiz(params);
          };
          var FetchQuestion = function (params) {
              //var data = serializeData(loginData);
              return assessment.FetchQuestion(params);
          };
          var GenerateFeedback = function (params) {
              //var data = serializeData(loginData);
              return assessment.GenerateFeedback(params);
          };
          var AutoSubmit = function (params) {
              //var data = serializeData(loginData);
              return assessment.AutoSubmit(params);
          };
          var FetchQuizDetails = function (params) {
              //var data = serializeData(loginData);
              return assessment.FetchQuizDetails(params);
          };
          return {
              StartQuiz: StartQuiz,
              FetchQuestion: FetchQuestion,
              GenerateFeedback: GenerateFeedback,
              AutoSubmit: AutoSubmit,
              FetchQuizDetails: FetchQuizDetails
          };
      }]);
;app.constant('Enum', {
    QuestionType: {
        MCSS: 'mcss',
        MCMS: 'mcms',
        FIB: 'fib',
        FIBDND: 'fib_dnd',
        Matching: 'matching',
        Order: 'order',
        GRAPHIC: 6,
        Exhibit: 7,
        HOTSPOTt: 8,
        ChoiceMatrix: 'choicematrix',
        MCMSVIDEO: 9,
        Essay: 'essay',
        Composite: 'composite',
        TextSelection: 'textselection',
        HotSpot: 'hotspot',
        LTD: 'ltd',
        LTDDND:'ltd_dnd'
    },
    Role: {
        Student: 'Student',
        Instructor: 'Instructor'
    },
    OperationMode: {
        Online: 0,
        Offline: 1
    },
    QuestionSubType: {
        mcss_vertical: "mcss_vertical",
        mcss_horizontal: "mcss_horizontal",
        mcss_image_horizontal: "mcss_image_horizontal",
        mcss_image_vertical: "mcss_image_vertical",
        mcss_graphics: "mcss_graphic",
        mcss_select: "mcss_select",

        mcms_vertical: "mcms_vertical",
        mcms_horizontal: "mcms_horizontal",
        mcms_image_horizontal: "mcms_image_horizontal",
        mcms_image_vertical: "mcms_image_vertical",
        mcms_graphics: "mcms_graphic",
        mcms_select: "mcms_select",
        image_sentence: "image_sentence",
        orderingmedia: "media"
    },
    EssayToolBarType: {
        editorbasic: "editor-basic",
        no_toolbar: "no-toolbar",
        basicmatheditor: "basicmatheditor",
    },
    AssessmentActivityType: {
        ShowHint: 1,
        NotLearned: 2,
        BookMark: 3,
        ReportAProblem: 4,
        NoOfTry: 5,
        SubmissionStatus: 6
    },
    QuestionFilterType: {
        NotVisited: 1,
        VisitedNotAnswered: 2,
        Completed: 3,
        ViewAll: 4,
        IsBookMarked: 5,
        ViewAttended:6
    }

});
;app.directive("math", function () {
    return {
        restrict: 'EA',
        $scope: {
            math: '@'
        },
        controller: ["$scope", "$element", "$attrs", function ($scope, $elem, $attrs) {
            $scope.$watch($attrs.math, function (value) {
                if (!value) return;
                $elem.html(value);
				//MathJax.Hub.Queue(["Typeset", MathJax.Hub, $elem[0]]);
                MathJax.Hub.Queue(["Typeset", MathJax.Hub, value]);
            });
        }]
    };
});
;app.directive('fibvariation', ["$rootScope", function ($rootScope) {
    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            model: "=ngModel",
            UserInput: "=userInput",
            ParentCtrl: "=parentCtrl"
        },
        replace: true,
        templateUrl: 'FIBdirective.html',
        link: function (scope, element, attrs, ngModel) {
            scope.OnDragStart = function (option) {
                option.AnswerClass = "";
            },
            scope.CheckInputValue = function (e) {
                var keyCode = e.which ? e.which : e.keyCode;
                ngModel.$modelValue.AnswerClass = "";
                var decimalplaces = (ngModel.$modelValue.decimalplace == "") ? 0 : ngModel.$modelValue.decimalplace;
                if (!scope.ValidateUserInput(keyCode, ngModel.$modelValue.inputtype, decimalplaces)) {
                    e.preventDefault();
                    return false;
                }else
                {
                    $rootScope.$broadcast("textEntered", {});
                }
                //if (ngModel.$modelValue.UserInput != "") {
                //    $rootScope.$broadcast("textEntered", {});
                //}
            },
            scope.SetItemToDropCanvasList = function (index) {
                scope.ParentCtrl.SetItemToDropCanvasList(index);
            },
            scope.AddItemToDropItemList = function (item, key, index) {
                scope.ParentCtrl.AddItemToDropItemList(item, key, index);
            },
             scope.RemoveSelected = function () {
                 var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
                 if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
             },
            scope.showOptionFeedback = function (ParentCtrl, optionId) {
                ParentCtrl.showOptionFeedback(optionId);
            },
            scope.CheckSelectedValue = function (value) {
                ngModel.$modelValue.UserInput = value;
                ngModel.$modelValue.AnswerClass = "";
                $rootScope.$broadcast("textEntered", {});
            },            
            scope.GetFeedback = function (model) {                
                var feedbackText = '';
                popovermodel = '';
                $('body').popover("destroy");

                popovermodel = model;
                //$('#spnltd' + model.key).popover({
                //    trigger: 'click',
                //    html: true,
                //    content: feedbackText,//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                //    placement: "top"
                //});
                $('body').popover({
                    trigger: 'click',
                    html: true,
                    selector: '.fa-commenting-o',
                    content: function () {
                        if (popovermodel.AnswerClass == "correct") {
                            feedbackText = popovermodel.correct_feedback;
                        }
                        else {
                            feedbackText = popovermodel.incorrect_feedback;
                        }
                        //return feedbackText;
                        if (popovermodel.QuestionIndex > 0) {
                            $('.fdback-mobile #divltd' + popovermodel.QuestionIndex + popovermodel.key).html('<span>' + (popovermodel.key + 1) + ". " + feedbackText + '</span>');
                        }
                        else {
                            $('.fdback-mobile #divltd' + popovermodel.key).html('<span>' + (popovermodel.key + 1) + ". " + feedbackText + '</span>');
                        }
                        if ($('.fdback-mobile').is(':visible')) {
                            //$('.fdback-mobile #divltd' + popovermodel.key).html('<span>' + (popovermodel.key + 1) + ". " + feedbackText + '</span>');
                            if (popovermodel.QuestionIndex > 0) {
                                $('.fdback-mobile #divltd' + popovermodel.QuestionIndex + popovermodel.key).show();
                            }
                            else {
                                $('.fdback-mobile #divltd' + popovermodel.key).show();
                            }
                        }
                        else {
                            return feedbackText;
                        }
                    },//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                    placement: "top"
                });
            },
            scope.CheckDropItem = function (event, index, item, type) {
                if (item.QuestionIndex != index) {
                    return false;
                }
                else {
                    return item;
                }
            },
            scope.ValidateUserInput = function (keyCode, inputtype, maxDecimals) {
                var resultType = false;
                var regexp = "";
                var specialKeys = new Array();
                specialKeys.push(8); //Backspace
                specialKeys.push(37); //left
                specialKeys.push(38); //up
                specialKeys.push(39); //right
                specialKeys.push(40); //down
                specialKeys.push(9); //Tab key
                specialKeys.push(32);//space bar
                specialKeys.push(126);//~
                specialKeys.push(33);//!
                specialKeys.push(64);//@
                specialKeys.push(35);//#
                specialKeys.push(36);//$
                specialKeys.push(37);//%
                specialKeys.push(94);//^
                specialKeys.push(38);//&
                specialKeys.push(42);//*
                specialKeys.push(40);//(
                specialKeys.push(41);//)
                specialKeys.push(45);//-
                specialKeys.push(43);//+
                specialKeys.push(61);//=
                specialKeys.push(60);//<
                specialKeys.push(44);//,
                specialKeys.push(62);//>
                specialKeys.push(46);//.
                specialKeys.push(123);//{
                specialKeys.push(125);//}
                specialKeys.push(91);//[
                specialKeys.push(93);//]
                specialKeys.push(95);//_
                specialKeys.push(47);///
                specialKeys.push(92);//\
                specialKeys.push(59);//;
                specialKeys.push(58);//:
                specialKeys.push(96);//`
                specialKeys.push(63);//?
                specialKeys.push(58);//:
                specialKeys.push(124);//|
                specialKeys.push(34);//"
                switch (inputtype) {
                    case 'alphanumeric':
                        if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                            resultType = true;
                        break;
                    case 'numeric':
                        if (maxDecimals > 0) {
                            specialKeys.push(46); //decimal
                            regexp = "^-?\\d*\\.?\\d{0," + maxDecimals + "}$";
                            if (ngModel.$modelValue.UserInput != "" && decimalPlaces(ngModel.$modelValue.UserInput) > maxDecimals) {
                                return false;
                            }
                        } else {
                            regexp = "^-?\\d*$";
                        }
                        if ((keyCode >= 48 && keyCode <= 57) || (specialKeys.indexOf(keyCode) != -1)) {
                            if (ngModel.$modelValue.UserInput.indexOf('.') > -1 && keyCode == 46) {
                                return false;
                            }
                            else if (maxDecimals > 0 && keyCode >= 48 && keyCode <= 57 && decimalPlaces(ngModel.$modelValue.UserInput) == maxDecimals) {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                            //var validRegex = new RegExp(regexp);
                            //resultType = validRegex.test(ngModel.$modelValue.UserInput);
                        }
                        break;
                    case 'alphabet':
                        if ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                            resultType = true;
                        break;
                    default:
                        break;
                }
                return resultType;
            }
            function decimalPlaces(num) {
                var match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                return Math.max(
                     0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
            }
        }
    }

}]);

;app.directive('dynamic', function ($compile) {
    return {
        restrict: 'A',
        replace: true,
        scope: { dynamic: '=dynamic' },
        link: function (scope, ele, attrs) {
            scope.$watch('dynamic', function (html) {               
                ele.html(scope.dynamic);
                $compile(ele.contents())(scope);
            });
        }
    };
});
;app.directive('loading', ['$http', '$window', '$cookieStore', '$rootScope', function ($http, $window, $cookieStore, $rootScope) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            // if ($rootScope.token != undefined) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;

            };

            scope.$watch(scope.isLoading, function (v) {
                if (v) {
                    elm.show();
                    elm.removeClass('hide');
                } else {
                    elm.hide();
                }
            });
            //}
        }
    };
}]);





;app.directive('popover', function () {
    return {
        restrict: 'A',
		template: '<span math dynamic></span>',
		link: function (scope, el, attrs, compile) {
		    if(attrs.popoverHtml != undefined && attrs.popoverHtml != null && attrs.popoverHtml.trim() != "")
		    {
		        var choiceData;
		        var questionData;
		        if(scope.$parent.CurrentQuestion != undefined)
		        {
		            if (scope.$parent.CurrentQuestion.QuestionData.choices != undefined) {
		                choiceData = scope.$parent.CurrentQuestion.QuestionData.choices;
		                questionData = scope.$parent.CurrentQuestion.QuestionData;
		            }
		        }
		        else if(scope.$parent.ParentCtrl.CurrentQuestion !=undefined)
		        {
		            if (scope.$parent.ParentCtrl.CurrentQuestion.QuestionData.choices != undefined) {
		                choiceData = scope.$parent.ParentCtrl.CurrentQuestion.QuestionData.choices;
		                questionData = scope.$parent.ParentCtrl.CurrentQuestion.QuestionData;
		            }
		        }
				
		        if(choiceData != undefined && choiceData.length > 0)
		        {
		            //if (questionData.question_type.text != 'fib_dnd' && questionData.question_type.text != 'fib' && questionData.question_type.text != 'textselection' && questionData.question_type.text != 'ltd' && questionData.question_type.text != 'ltd_dnd') {
		            //    $(el).popover({
		            //        trigger: 'click',
		            //        html: true,
		            //        content: choiceData[attrs.popoverHtml - 1].feedback,//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
		            //        placement: "top"
		            //    });
		            //}
										
		            $('.container-main').click(function(e){
		                if (e.target.className != null && e.target.className != undefined && e.target.className.trim() != "" && e.target.className.trim() != 'fa fa-commenting-o')
		                {
		                    $('.popover').hide(); 
		                }
		            });
		        }
		    }		   
		}        
    };
});
;app.directive("algorithmic", function () {
    return {
        restrict: 'EA',
        $scope: {
            algorithmic: '@'
        },
        controller: ["$scope", "$element", "$attrs", function ($scope, $elem, $attrs) {
			if($scope.$parent.CurrentQuestion.SelectedOptions == null)
			{
				if($attrs.algorithmic.indexOf('rand') > -1)
				{
					var min = $attrs.algorithmic.split(',')[0].split('(')[1];
					var max = $attrs.algorithmic.split(',')[1].split(')')[0];
					var random = Math.floor((Math.random() * max) + min);
					if($scope.$parent.CurrentQuestion.AlgorithmicVariableArray == undefined)
					{
						$scope.$parent.CurrentQuestion.AlgorithmicVariableArray = {};
						$scope.$parent.CurrentQuestion.AlgorithmicVariableArray[$elem.attr('id')] = random;
					}
					else
					{
						$scope.$parent.CurrentQuestion.AlgorithmicVariableArray[$elem.attr('id')] = random;
					}
					$elem.html(random);
				}
				else
				{
					optionValues($elem);
				}
			}
			else
			{
				if($attrs.algorithmic.indexOf('rand') > -1)
				{
					$elem.html($scope.$parent.CurrentQuestion.AlgorithmicVariableArray[$elem.attr('id')]);
				}
				else
				{
					optionValues($elem);
				}
			}
			
			function optionValues(elem)
			{
				if ($elem.html().indexOf('[[') > -1)
				{
					$.each($elem.children(), function (index, value) {
						var spanId = $(value).html();
						$elem.html($elem.html().replace('[[', ''));
						$elem.html($elem.html().replace(']]', ''));
						$elem.html($elem.html().replace('<span>' + spanId + '</span>', parseInt($(spanId).html())));
					});
					$elem.html(eval($elem.html()));
				}
				else
				{
					$.each($elem.children(), function (index, value) {
						var spanId = $(value).html();
						$elem.html($elem.html().replace(spanId, $(spanId).html()));
					});
				}
			}
        }]
    };
});
;app.directive('imageonload', function ($timeout) {
    return {
        restrict: 'AE',
        link: function ($scope, element, attrs) {
            element.bind('load', function () {
                $('#'+attrs.id).mapster({                   
                    fillOpacity: 0.4,
                    fillColor: "d42e16",
                    stroke: true,
                    strokeColor: "3320FF",
                    strokeOpacity: 0.8,
                    strokeWidth: 4,
                    clickNavigate: true,
                    highlight: true,
                    isSelectable: true,
                    singleSelect:false,
                    mapKey: 'data-state'
                    //render_select: {
                    //    fill: false,
                    //    fillColor: 'ff0000',
                    //    stroke: false,
                    //    strokeColor: '00ff00',
                    //    strokeWidth: 2
                    //},
                    //areas: [{
                    //    key: 'really-cold',
                    //    render_select: {
                    //        fillColor: '0000ff'
                    //    }
                    //},
                    //{
                    //    key: 'new-england',
                    //    render_select: {
                    //        fillColor: '44ff44'
                    //    }
                    //}
                    //]
                });
            });

            //$timeout(function () {
            //    if ($scope.UserAnswerText) {
            //        $('#hotspotContainer').append($scope.UserAnswerText);
            //    }
            //});
        }
       

    };
});
;/**!
 * Sortable
 * @author	RubaXa   <trash@rubaxa.org>
 * @license MIT
 */


(function (factory) {
	"use strict";

	if (typeof define === "function" && define.amd) {
		define(factory);
	}
	else if (typeof module != "undefined" && typeof module.exports != "undefined") {
		module.exports = factory();
	}
	else if (typeof Package !== "undefined") {
		Sortable = factory();  // export for Meteor.js
	}
	else {
		/* jshint sub:true */
		window["Sortable"] = factory();
	}
})(function () {
	"use strict";

	var dragEl,
		parentEl,
		ghostEl,
		cloneEl,
		rootEl,
		nextEl,

		scrollEl,
		scrollParentEl,

		lastEl,
		lastCSS,
		lastParentCSS,

		oldIndex,
		newIndex,

		activeGroup,
		autoScroll = {},

		tapEvt,
		touchEvt,

		moved,

		/** @const */
		RSPACE = /\s+/g,

		expando = 'Sortable' + (new Date).getTime(),

		win = window,
		document = win.document,
		parseInt = win.parseInt,

		supportDraggable = !!('draggable' in document.createElement('div')),
		supportCssPointerEvents = (function (el) {
			el = document.createElement('x');
			el.style.cssText = 'pointer-events:auto';
			return el.style.pointerEvents === 'auto';
		})(),

		_silent = false,

		abs = Math.abs,
		slice = [].slice,

		touchDragOverListeners = [],

		_autoScroll = _throttle(function (/**Event*/evt, /**Object*/options, /**HTMLElement*/rootEl) {
			// Bug: https://bugzilla.mozilla.org/show_bug.cgi?id=505521
			if (rootEl && options.scroll) {
				var el,
					rect,
					sens = options.scrollSensitivity,
					speed = options.scrollSpeed,

					x = evt.clientX,
					y = evt.clientY,

					winWidth = window.innerWidth,
					winHeight = window.innerHeight,

					vx,
					vy
				;

				// Delect scrollEl
				if (scrollParentEl !== rootEl) {
					scrollEl = options.scroll;
					scrollParentEl = rootEl;

					if (scrollEl === true) {
						scrollEl = rootEl;

						do {
							if ((scrollEl.offsetWidth < scrollEl.scrollWidth) ||
								(scrollEl.offsetHeight < scrollEl.scrollHeight)
							) {
								break;
							}
							/* jshint boss:true */
						} while (scrollEl = scrollEl.parentNode);
					}
				}

				if (scrollEl) {
					el = scrollEl;
					rect = scrollEl.getBoundingClientRect();
					vx = (abs(rect.right - x) <= sens) - (abs(rect.left - x) <= sens);
					vy = (abs(rect.bottom - y) <= sens) - (abs(rect.top - y) <= sens);
				}


				if (!(vx || vy)) {
					vx = (winWidth - x <= sens) - (x <= sens);
					vy = (winHeight - y <= sens) - (y <= sens);

					/* jshint expr:true */
					(vx || vy) && (el = win);
				}


				if (autoScroll.vx !== vx || autoScroll.vy !== vy || autoScroll.el !== el) {
					autoScroll.el = el;
					autoScroll.vx = vx;
					autoScroll.vy = vy;

					clearInterval(autoScroll.pid);

					if (el) {
						autoScroll.pid = setInterval(function () {
							if (el === win) {
								win.scrollTo(win.pageXOffset + vx * speed, win.pageYOffset + vy * speed);
							} else {
								vy && (el.scrollTop += vy * speed);
								vx && (el.scrollLeft += vx * speed);
							}
						}, 24);
					}
				}
			}
		}, 30),

		_prepareGroup = function (options) {
			var group = options.group;

			if (!group || typeof group != 'object') {
				group = options.group = {name: group};
			}

			['pull', 'put'].forEach(function (key) {
				if (!(key in group)) {
					group[key] = true;
				}
			});

			options.groups = ' ' + group.name + (group.put.join ? ' ' + group.put.join(' ') : '') + ' ';
		}
	;



	/**
	 * @class  Sortable
	 * @param  {HTMLElement}  el
	 * @param  {Object}       [options]
	 */
	function Sortable(el, options) {
		if (!(el && el.nodeType && el.nodeType === 1)) {
			throw 'Sortable: `el` must be HTMLElement, and not ' + {}.toString.call(el);
		}

		this.el = el; // root element
		this.options = options = _extend({}, options);


		// Export instance
		el[expando] = this;


		// Default options
		var defaults = {
			group: Math.random(),
			sort: true,
			disabled: false,
			store: null,
			handle: null,
			scroll: true,
			scrollSensitivity: 30,
			scrollSpeed: 10,
			draggable: /[uo]l/i.test(el.nodeName) ? 'li' : '>*',
			ghostClass: 'sortable-ghost',
			chosenClass: 'sortable-chosen',
			ignore: 'a, img',
			filter: null,
			animation: 0,
			setData: function (dataTransfer, dragEl) {
				dataTransfer.setData('Text', dragEl.textContent);
			},
			dropBubble: false,
			dragoverBubble: false,
			dataIdAttr: 'data-id',
			delay: 0,
			forceFallback: false,
			fallbackClass: 'sortable-fallback',
			fallbackOnBody: false
		};


		// Set default options
		for (var name in defaults) {
			!(name in options) && (options[name] = defaults[name]);
		}

		_prepareGroup(options);

		// Bind all private methods
		for (var fn in this) {
			if (fn.charAt(0) === '_') {
				this[fn] = this[fn].bind(this);
			}
		}

		// Setup drag mode
		this.nativeDraggable = options.forceFallback ? false : supportDraggable;

		// Bind events
		_on(el, 'mousedown', this._onTapStart);
		_on(el, 'touchstart', this._onTapStart);

		if (this.nativeDraggable) {
			_on(el, 'dragover', this);
			_on(el, 'dragenter', this);
		}

		touchDragOverListeners.push(this._onDragOver);

		// Restore sorting
		options.store && this.sort(options.store.get(this));
	}


	Sortable.prototype = /** @lends Sortable.prototype */ {
		constructor: Sortable,

		_onTapStart: function (/** Event|TouchEvent */evt) {
			var _this = this,
				el = this.el,
				options = this.options,
				type = evt.type,
				touch = evt.touches && evt.touches[0],
				target = (touch || evt).target,
				originalTarget = target,
				filter = options.filter;


			if (type === 'mousedown' && evt.button !== 0 || options.disabled) {
				return; // only left button or enabled
			}

			target = _closest(target, options.draggable, el);

			if (!target) {
				return;
			}

			// get the index of the dragged element within its parent
			oldIndex = _index(target, options.draggable);

			// Check filter
			if (typeof filter === 'function') {
				if (filter.call(this, evt, target, this)) {
					_dispatchEvent(_this, originalTarget, 'filter', target, el, oldIndex);
					evt.preventDefault();
					return; // cancel dnd
				}
			}
			else if (filter) {
				filter = filter.split(',').some(function (criteria) {
					criteria = _closest(originalTarget, criteria.trim(), el);

					if (criteria) {
						_dispatchEvent(_this, criteria, 'filter', target, el, oldIndex);
						return true;
					}
				});

				if (filter) {
					evt.preventDefault();
					return; // cancel dnd
				}
			}


			if (options.handle && !_closest(originalTarget, options.handle, el)) {
				return;
			}


			// Prepare `dragstart`
			this._prepareDragStart(evt, touch, target);
		},

		_prepareDragStart: function (/** Event */evt, /** Touch */touch, /** HTMLElement */target) {
			var _this = this,
				el = _this.el,
				options = _this.options,
				ownerDocument = el.ownerDocument,
				dragStartFn;

			if (target && !dragEl && (target.parentNode === el)) {
				tapEvt = evt;

				rootEl = el;
				dragEl = target;
				parentEl = dragEl.parentNode;
				nextEl = dragEl.nextSibling;
				activeGroup = options.group;

				dragStartFn = function () {
					// Delayed drag has been triggered
					// we can re-enable the events: touchmove/mousemove
					_this._disableDelayedDrag();

					// Make the element draggable
					dragEl.draggable = true;

					// Chosen item
					_toggleClass(dragEl, _this.options.chosenClass, true);

					// Bind the events: dragstart/dragend
					_this._triggerDragStart(touch);
				};

				// Disable "draggable"
				options.ignore.split(',').forEach(function (criteria) {
					_find(dragEl, criteria.trim(), _disableDraggable);
				});

				_on(ownerDocument, 'mouseup', _this._onDrop);
				_on(ownerDocument, 'touchend', _this._onDrop);
				_on(ownerDocument, 'touchcancel', _this._onDrop);

				if (options.delay) {
					// If the user moves the pointer or let go the click or touch
					// before the delay has been reached:
					// disable the delayed drag
					_on(ownerDocument, 'mouseup', _this._disableDelayedDrag);
					_on(ownerDocument, 'touchend', _this._disableDelayedDrag);
					_on(ownerDocument, 'touchcancel', _this._disableDelayedDrag);
					_on(ownerDocument, 'mousemove', _this._disableDelayedDrag);
					_on(ownerDocument, 'touchmove', _this._disableDelayedDrag);

					_this._dragStartTimer = setTimeout(dragStartFn, options.delay);
				} else {
					dragStartFn();
				}
			}
		},

		_disableDelayedDrag: function () {
			var ownerDocument = this.el.ownerDocument;

			clearTimeout(this._dragStartTimer);
			_off(ownerDocument, 'mouseup', this._disableDelayedDrag);
			_off(ownerDocument, 'touchend', this._disableDelayedDrag);
			_off(ownerDocument, 'touchcancel', this._disableDelayedDrag);
			_off(ownerDocument, 'mousemove', this._disableDelayedDrag);
			_off(ownerDocument, 'touchmove', this._disableDelayedDrag);
		},

		_triggerDragStart: function (/** Touch */touch) {
			if (touch) {
				// Touch device support
				tapEvt = {
					target: dragEl,
					clientX: touch.clientX,
					clientY: touch.clientY
				};

				this._onDragStart(tapEvt, 'touch');
			}
			else if (!this.nativeDraggable) {
				this._onDragStart(tapEvt, true);
			}
			else {
				_on(dragEl, 'dragend', this);
				_on(rootEl, 'dragstart', this._onDragStart);
			}

			try {
				if (document.selection) {
					document.selection.empty();
				} else {
					window.getSelection().removeAllRanges();
				}
			} catch (err) {
			}
		},

		_dragStarted: function () {
			if (rootEl && dragEl) {
				// Apply effect
				_toggleClass(dragEl, this.options.ghostClass, true);

				Sortable.active = this;

				// Drag start event
				_dispatchEvent(this, rootEl, 'start', dragEl, rootEl, oldIndex);
			}
		},

		_emulateDragOver: function () {
			if (touchEvt) {
				if (this._lastX === touchEvt.clientX && this._lastY === touchEvt.clientY) {
					return;
				}

				this._lastX = touchEvt.clientX;
				this._lastY = touchEvt.clientY;

				if (!supportCssPointerEvents) {
					_css(ghostEl, 'display', 'none');
				}

				var target = document.elementFromPoint(touchEvt.clientX, touchEvt.clientY),
					parent = target,
					groupName = ' ' + this.options.group.name + '',
					i = touchDragOverListeners.length;

				if (parent) {
					do {
						if (parent[expando] && parent[expando].options.groups.indexOf(groupName) > -1) {
							while (i--) {
								touchDragOverListeners[i]({
									clientX: touchEvt.clientX,
									clientY: touchEvt.clientY,
									target: target,
									rootEl: parent
								});
							}

							break;
						}

						target = parent; // store last element
					}
					/* jshint boss:true */
					while (parent = parent.parentNode);
				}

				if (!supportCssPointerEvents) {
					_css(ghostEl, 'display', '');
				}
			}
		},


		_onTouchMove: function (/**TouchEvent*/evt) {
			if (tapEvt) {
				// only set the status to dragging, when we are actually dragging
				if (!Sortable.active) {
					this._dragStarted();
				}

				// as well as creating the ghost element on the document body
				this._appendGhost();

				var touch = evt.touches ? evt.touches[0] : evt,
					dx = touch.clientX - tapEvt.clientX,
					dy = touch.clientY - tapEvt.clientY,
					translate3d = evt.touches ? 'translate3d(' + dx + 'px,' + dy + 'px,0)' : 'translate(' + dx + 'px,' + dy + 'px)';

				moved = true;
				touchEvt = touch;

				_css(ghostEl, 'webkitTransform', translate3d);
				_css(ghostEl, 'mozTransform', translate3d);
				_css(ghostEl, 'msTransform', translate3d);
				_css(ghostEl, 'transform', translate3d);

				evt.preventDefault();
			}
		},

		_appendGhost: function () {
			if (!ghostEl) {
				var rect = dragEl.getBoundingClientRect(),
					css = _css(dragEl),
					options = this.options,
					ghostRect;

				ghostEl = dragEl.cloneNode(true);

				_toggleClass(ghostEl, options.ghostClass, false);
				_toggleClass(ghostEl, options.fallbackClass, true);

				_css(ghostEl, 'top', rect.top - parseInt(css.marginTop, 10));
				_css(ghostEl, 'left', rect.left - parseInt(css.marginLeft, 10));
				_css(ghostEl, 'width', rect.width);
				_css(ghostEl, 'height', rect.height);
				_css(ghostEl, 'opacity', '0.8');
				_css(ghostEl, 'position', 'fixed');
				_css(ghostEl, 'zIndex', '100000');
				_css(ghostEl, 'pointerEvents', 'none');

				options.fallbackOnBody && document.body.appendChild(ghostEl) || rootEl.appendChild(ghostEl);

				// Fixing dimensions.
				ghostRect = ghostEl.getBoundingClientRect();
				_css(ghostEl, 'width', rect.width * 2 - ghostRect.width);
				_css(ghostEl, 'height', rect.height * 2 - ghostRect.height);
			}
		},

		_onDragStart: function (/**Event*/evt, /**boolean*/useFallback) {
			var dataTransfer = evt.dataTransfer,
				options = this.options;

			this._offUpEvents();

			if (activeGroup.pull == 'clone') {
				cloneEl = dragEl.cloneNode(true);
				_css(cloneEl, 'display', 'none');
				rootEl.insertBefore(cloneEl, dragEl);
			}

			if (useFallback) {

				if (useFallback === 'touch') {
					// Bind touch events
					_on(document, 'touchmove', this._onTouchMove);
					_on(document, 'touchend', this._onDrop);
					_on(document, 'touchcancel', this._onDrop);
				} else {
					// Old brwoser
					_on(document, 'mousemove', this._onTouchMove);
					_on(document, 'mouseup', this._onDrop);
				}

				this._loopId = setInterval(this._emulateDragOver, 50);
			}
			else {
				if (dataTransfer) {
					dataTransfer.effectAllowed = 'move';
					options.setData && options.setData.call(this, dataTransfer, dragEl);
				}

				_on(document, 'drop', this);
				setTimeout(this._dragStarted, 0);
			}
		},

		_onDragOver: function (/**Event*/evt) {
			var el = this.el,
				target,
				dragRect,
				revert,
				options = this.options,
				group = options.group,
				groupPut = group.put,
				isOwner = (activeGroup === group),
				canSort = options.sort;

			if (evt.preventDefault !== void 0) {
				evt.preventDefault();
				!options.dragoverBubble && evt.stopPropagation();
			}

			moved = true;

			if (activeGroup && !options.disabled &&
				(isOwner
					? canSort || (revert = !rootEl.contains(dragEl)) // Reverting item into the original list
					: activeGroup.pull && groupPut && (
						(activeGroup.name === group.name) || // by Name
						(groupPut.indexOf && ~groupPut.indexOf(activeGroup.name)) // by Array
					)
				) &&
				(evt.rootEl === void 0 || evt.rootEl === this.el) // touch fallback
			) {
				// Smart auto-scrolling
				_autoScroll(evt, options, this.el);

				if (_silent) {
					return;
				}

				target = _closest(evt.target, options.draggable, el);
				dragRect = dragEl.getBoundingClientRect();

				if (revert) {
					_cloneHide(true);

					if (cloneEl || nextEl) {
						rootEl.insertBefore(dragEl, cloneEl || nextEl);
					}
					else if (!canSort) {
						rootEl.appendChild(dragEl);
					}

					return;
				}


				if ((el.children.length === 0) || (el.children[0] === ghostEl) ||
					(el === evt.target) && (target = _ghostIsLast(el, evt))
				) {

					if (target) {
						if (target.animated) {
							return;
						}

						targetRect = target.getBoundingClientRect();
					}

					_cloneHide(isOwner);

					if (_onMove(rootEl, el, dragEl, dragRect, target, targetRect) !== false) {
						if (!dragEl.contains(el)) {
							el.appendChild(dragEl);
							parentEl = el; // actualization
						}

						this._animate(dragRect, dragEl);
						target && this._animate(targetRect, target);
					}
				}
				else if (target && !target.animated && target !== dragEl && (target.parentNode[expando] !== void 0)) {
					if (lastEl !== target) {
						lastEl = target;
						lastCSS = _css(target);
						lastParentCSS = _css(target.parentNode);
					}


					var targetRect = target.getBoundingClientRect(),
						width = targetRect.right - targetRect.left,
						height = targetRect.bottom - targetRect.top,
						floating = /left|right|inline/.test(lastCSS.cssFloat + lastCSS.display)
							|| (lastParentCSS.display == 'flex' && lastParentCSS['flex-direction'].indexOf('row') === 0),
						isWide = (target.offsetWidth > dragEl.offsetWidth),
						isLong = (target.offsetHeight > dragEl.offsetHeight),
						halfway = (floating ? (evt.clientX - targetRect.left) / width : (evt.clientY - targetRect.top) / height) > 0.5,
						nextSibling = target.nextElementSibling,
						moveVector = _onMove(rootEl, el, dragEl, dragRect, target, targetRect),
						after
					;

					if (moveVector !== false) {
						_silent = true;
						setTimeout(_unsilent, 30);

						_cloneHide(isOwner);

						if (moveVector === 1 || moveVector === -1) {
							after = (moveVector === 1);
						}
						else if (floating) {
							var elTop = dragEl.offsetTop,
								tgTop = target.offsetTop;

							if (elTop === tgTop) {
								after = (target.previousElementSibling === dragEl) && !isWide || halfway && isWide;
							} else {
								after = tgTop > elTop;
							}
						} else {
							after = (nextSibling !== dragEl) && !isLong || halfway && isLong;
						}

						if (!dragEl.contains(el)) {
							if (after && !nextSibling) {
								el.appendChild(dragEl);
							} else {
								target.parentNode.insertBefore(dragEl, after ? nextSibling : target);
							}
						}

						parentEl = dragEl.parentNode; // actualization

						this._animate(dragRect, dragEl);
						this._animate(targetRect, target);
					}
				}
			}
		},

		_animate: function (prevRect, target) {
			var ms = this.options.animation;

			if (ms) {
				var currentRect = target.getBoundingClientRect();

				_css(target, 'transition', 'none');
				_css(target, 'transform', 'translate3d('
					+ (prevRect.left - currentRect.left) + 'px,'
					+ (prevRect.top - currentRect.top) + 'px,0)'
				);

				target.offsetWidth; // repaint

				_css(target, 'transition', 'all ' + ms + 'ms');
				_css(target, 'transform', 'translate3d(0,0,0)');

				clearTimeout(target.animated);
				target.animated = setTimeout(function () {
					_css(target, 'transition', '');
					_css(target, 'transform', '');
					target.animated = false;
				}, ms);
			}
		},

		_offUpEvents: function () {
			var ownerDocument = this.el.ownerDocument;

			_off(document, 'touchmove', this._onTouchMove);
			_off(ownerDocument, 'mouseup', this._onDrop);
			_off(ownerDocument, 'touchend', this._onDrop);
			_off(ownerDocument, 'touchcancel', this._onDrop);
		},

		_onDrop: function (/**Event*/evt) {
			var el = this.el,
				options = this.options;

			clearInterval(this._loopId);
			clearInterval(autoScroll.pid);
			clearTimeout(this._dragStartTimer);

			// Unbind events
			_off(document, 'mousemove', this._onTouchMove);

			if (this.nativeDraggable) {
				_off(document, 'drop', this);
				_off(el, 'dragstart', this._onDragStart);
			}

			this._offUpEvents();

			if (evt) {
				if (moved) {
					evt.preventDefault();
					!options.dropBubble && evt.stopPropagation();
				}

				ghostEl && ghostEl.parentNode.removeChild(ghostEl);

				if (dragEl) {
					if (this.nativeDraggable) {
						_off(dragEl, 'dragend', this);
					}

					_disableDraggable(dragEl);

					// Remove class's
					_toggleClass(dragEl, this.options.ghostClass, false);
					_toggleClass(dragEl, this.options.chosenClass, false);

					if (rootEl !== parentEl) {
						newIndex = _index(dragEl, options.draggable);

						if (newIndex >= 0) {
							// drag from one list and drop into another
							_dispatchEvent(null, parentEl, 'sort', dragEl, rootEl, oldIndex, newIndex);
							_dispatchEvent(this, rootEl, 'sort', dragEl, rootEl, oldIndex, newIndex);

							// Add event
							_dispatchEvent(null, parentEl, 'add', dragEl, rootEl, oldIndex, newIndex);

							// Remove event
							_dispatchEvent(this, rootEl, 'remove', dragEl, rootEl, oldIndex, newIndex);
						}
					}
					else {
						// Remove clone
						cloneEl && cloneEl.parentNode.removeChild(cloneEl);

						if (dragEl.nextSibling !== nextEl) {
							// Get the index of the dragged element within its parent
							newIndex = _index(dragEl, options.draggable);

							if (newIndex >= 0) {
								// drag & drop within the same list
								_dispatchEvent(this, rootEl, 'update', dragEl, rootEl, oldIndex, newIndex);
								_dispatchEvent(this, rootEl, 'sort', dragEl, rootEl, oldIndex, newIndex);
							}
						}
					}

					if (Sortable.active) {
						if (newIndex === null || newIndex === -1) {
							newIndex = oldIndex;
						}

						_dispatchEvent(this, rootEl, 'end', dragEl, rootEl, oldIndex, newIndex);

						// Save sorting
						this.save();
					}
				}

				// Nulling
				rootEl =
				dragEl =
				parentEl =
				ghostEl =
				nextEl =
				cloneEl =

				scrollEl =
				scrollParentEl =

				tapEvt =
				touchEvt =

				moved =
				newIndex =

				lastEl =
				lastCSS =

				activeGroup =
				Sortable.active = null;
			}
		},


		handleEvent: function (/**Event*/evt) {
			var type = evt.type;

			if (type === 'dragover' || type === 'dragenter') {
				if (dragEl) {
					this._onDragOver(evt);
					_globalDragOver(evt);
				}
			}
			else if (type === 'drop' || type === 'dragend') {
				this._onDrop(evt);
			}
		},


		/**
		 * Serializes the item into an array of string.
		 * @returns {String[]}
		 */
		toArray: function () {
			var order = [],
				el,
				children = this.el.children,
				i = 0,
				n = children.length,
				options = this.options;

			for (; i < n; i++) {
				el = children[i];
				if (_closest(el, options.draggable, this.el)) {
					order.push(el.getAttribute(options.dataIdAttr) || _generateId(el));
				}
			}

			return order;
		},


		/**
		 * Sorts the elements according to the array.
		 * @param  {String[]}  order  order of the items
		 */
		sort: function (order) {
			var items = {}, rootEl = this.el;

			this.toArray().forEach(function (id, i) {
				var el = rootEl.children[i];

				if (_closest(el, this.options.draggable, rootEl)) {
					items[id] = el;
				}
			}, this);

			order.forEach(function (id) {
				if (items[id]) {
					rootEl.removeChild(items[id]);
					rootEl.appendChild(items[id]);
				}
			});
		},


		/**
		 * Save the current sorting
		 */
		save: function () {
			var store = this.options.store;
			store && store.set(this);
		},


		/**
		 * For each element in the set, get the first element that matches the selector by testing the element itself and traversing up through its ancestors in the DOM tree.
		 * @param   {HTMLElement}  el
		 * @param   {String}       [selector]  default: `options.draggable`
		 * @returns {HTMLElement|null}
		 */
		closest: function (el, selector) {
			return _closest(el, selector || this.options.draggable, this.el);
		},


		/**
		 * Set/get option
		 * @param   {string} name
		 * @param   {*}      [value]
		 * @returns {*}
		 */
		option: function (name, value) {
			var options = this.options;

			if (value === void 0) {
				return options[name];
			} else {
				options[name] = value;

				if (name === 'group') {
					_prepareGroup(options);
				}
			}
		},


		/**
		 * Destroy
		 */
		destroy: function () {
			var el = this.el;

			el[expando] = null;

			_off(el, 'mousedown', this._onTapStart);
			_off(el, 'touchstart', this._onTapStart);

			if (this.nativeDraggable) {
				_off(el, 'dragover', this);
				_off(el, 'dragenter', this);
			}

			// Remove draggable attributes
			Array.prototype.forEach.call(el.querySelectorAll('[draggable]'), function (el) {
				el.removeAttribute('draggable');
			});

			touchDragOverListeners.splice(touchDragOverListeners.indexOf(this._onDragOver), 1);

			this._onDrop();

			this.el = el = null;
		}
	};


	function _cloneHide(state) {
		if (cloneEl && (cloneEl.state !== state)) {
			_css(cloneEl, 'display', state ? 'none' : '');
			!state && cloneEl.state && rootEl.insertBefore(cloneEl, dragEl);
			cloneEl.state = state;
		}
	}


	function _closest(/**HTMLElement*/el, /**String*/selector, /**HTMLElement*/ctx) {
		if (el) {
			ctx = ctx || document;

			do {
				if (
					(selector === '>*' && el.parentNode === ctx)
					|| _matches(el, selector)
				) {
					return el;
				}
			}
			while (el !== ctx && (el = el.parentNode));
		}

		return null;
	}


	function _globalDragOver(/**Event*/evt) {
		if (evt.dataTransfer) {
			evt.dataTransfer.dropEffect = 'move';
		}
		evt.preventDefault();
	}


	function _on(el, event, fn) {
		el.addEventListener(event, fn, false);
	}


	function _off(el, event, fn) {
		el.removeEventListener(event, fn, false);
	}


	function _toggleClass(el, name, state) {
		if (el) {
			if (el.classList) {
				el.classList[state ? 'add' : 'remove'](name);
			}
			else {
				var className = (' ' + el.className + ' ').replace(RSPACE, ' ').replace(' ' + name + ' ', ' ');
				el.className = (className + (state ? ' ' + name : '')).replace(RSPACE, ' ');
			}
		}
	}


	function _css(el, prop, val) {
		var style = el && el.style;

		if (style) {
			if (val === void 0) {
				if (document.defaultView && document.defaultView.getComputedStyle) {
					val = document.defaultView.getComputedStyle(el, '');
				}
				else if (el.currentStyle) {
					val = el.currentStyle;
				}

				return prop === void 0 ? val : val[prop];
			}
			else {
				if (!(prop in style)) {
					prop = '-webkit-' + prop;
				}

				style[prop] = val + (typeof val === 'string' ? '' : 'px');
			}
		}
	}


	function _find(ctx, tagName, iterator) {
		if (ctx) {
			var list = ctx.getElementsByTagName(tagName), i = 0, n = list.length;

			if (iterator) {
				for (; i < n; i++) {
					iterator(list[i], i);
				}
			}

			return list;
		}

		return [];
	}



	function _dispatchEvent(sortable, rootEl, name, targetEl, fromEl, startIndex, newIndex) {
		var evt = document.createEvent('Event'),
			options = (sortable || rootEl[expando]).options,
			onName = 'on' + name.charAt(0).toUpperCase() + name.substr(1);

		evt.initEvent(name, true, true);

		evt.to = rootEl;
		evt.from = fromEl || rootEl;
		evt.item = targetEl || rootEl;
		evt.clone = cloneEl;

		evt.oldIndex = startIndex;
		evt.newIndex = newIndex;

		rootEl.dispatchEvent(evt);

		if (options[onName]) {
			options[onName].call(sortable, evt);
		}
	}


	function _onMove(fromEl, toEl, dragEl, dragRect, targetEl, targetRect) {
		var evt,
			sortable = fromEl[expando],
			onMoveFn = sortable.options.onMove,
			retVal;

		evt = document.createEvent('Event');
		evt.initEvent('move', true, true);

		evt.to = toEl;
		evt.from = fromEl;
		evt.dragged = dragEl;
		evt.draggedRect = dragRect;
		evt.related = targetEl || toEl;
		evt.relatedRect = targetRect || toEl.getBoundingClientRect();

		fromEl.dispatchEvent(evt);

		if (onMoveFn) {
			retVal = onMoveFn.call(sortable, evt);
		}

		return retVal;
	}


	function _disableDraggable(el) {
		el.draggable = false;
	}


	function _unsilent() {
		_silent = false;
	}


	/** @returns {HTMLElement|false} */
	function _ghostIsLast(el, evt) {
		var lastEl = el.lastElementChild,
				rect = lastEl.getBoundingClientRect();

		return ((evt.clientY - (rect.top + rect.height) > 5) || (evt.clientX - (rect.right + rect.width) > 5)) && lastEl; // min delta
	}


	/**
	 * Generate id
	 * @param   {HTMLElement} el
	 * @returns {String}
	 * @private
	 */
	function _generateId(el) {
		var str = el.tagName + el.className + el.src + el.href + el.textContent,
			i = str.length,
			sum = 0;

		while (i--) {
			sum += str.charCodeAt(i);
		}

		return sum.toString(36);
	}

	/**
	 * Returns the index of an element within its parent for a selected set of
	 * elements
	 * @param  {HTMLElement} el
	 * @param  {selector} selector
	 * @return {number}
	 */
	function _index(el, selector) {
		var index = 0;

		if (!el || !el.parentNode) {
			return -1;
		}

		while (el && (el = el.previousElementSibling)) {
			if (el.nodeName.toUpperCase() !== 'TEMPLATE'
					&& _matches(el, selector)) {
				index++;
			}
		}

		return index;
	}

	function _matches(/**HTMLElement*/el, /**String*/selector) {
		if (el) {
			selector = selector.split('.');

			var tag = selector.shift().toUpperCase(),
				re = new RegExp('\\s(' + selector.join('|') + ')(?=\\s)', 'g');

			return (
				(tag === '' || el.nodeName.toUpperCase() == tag) &&
				(!selector.length || ((' ' + el.className + ' ').match(re) || []).length == selector.length)
			);
		}

		return false;
	}

	function _throttle(callback, ms) {
		var args, _this;

		return function () {
			if (args === void 0) {
				args = arguments;
				_this = this;

				setTimeout(function () {
					if (args.length === 1) {
						callback.call(_this, args[0]);
					} else {
						callback.apply(_this, args);
					}

					args = void 0;
				}, ms);
			}
		};
	}

	function _extend(dst, src) {
		if (dst && src) {
			for (var key in src) {
				if (src.hasOwnProperty(key)) {
					dst[key] = src[key];
				}
			}
		}

		return dst;
	}


	// Export utils
	Sortable.utils = {
		on: _on,
		off: _off,
		css: _css,
		find: _find,
		is: function (el, selector) {
			return !!_closest(el, selector, el);
		},
		extend: _extend,
		throttle: _throttle,
		closest: _closest,
		toggleClass: _toggleClass,
		index: _index
	};


	/**
	 * Create sortable instance
	 * @param {HTMLElement}  el
	 * @param {Object}      [options]
	 */
	Sortable.create = function (el, options) {
		return new Sortable(el, options);
	};


	// Export
	Sortable.version = '1.4.2';
	return Sortable;
});

;/**
 * @author RubaXa <trash@rubaxa.org>
 * @licence MIT
 */
(function (factory) {
	'use strict';

	if (typeof define === 'function' && define.amd) {
		define(['angular', './Sortable'], factory);
	}
	else if (typeof require === 'function' && typeof exports === 'object' && typeof module === 'object') {
		require('angular');
		factory(angular, require('./Sortable'));
		module.exports = 'ng-sortable';
	}
	else if (window.angular && window.Sortable) {
		factory(angular, Sortable);
	}
})(function (angular, Sortable) {
	'use strict';


	/**
	 * @typedef   {Object}        ngSortEvent
	 * @property  {*}             model      List item
	 * @property  {Object|Array}  models     List of items
	 * @property  {number}        oldIndex   before sort
	 * @property  {number}        newIndex   after sort
	 */

	var expando = 'Sortable:ng-sortable';

	angular.module('ng-sortable', [])
		.constant('ngSortableVersion', '0.4.0')
		.constant('ngSortableConfig', {})
		.directive('ngSortable', ['$parse', 'ngSortableConfig', function ($parse, ngSortableConfig) {
			var removed,
				nextSibling,
				getSourceFactory = function getSourceFactory(el, scope) {
					var ngRepeat = [].filter.call(el.childNodes, function (node) {
						return (
								(node.nodeType === 8) &&
								(node.nodeValue.indexOf('ngRepeat:') !== -1)
							);
					})[0];

					if (!ngRepeat) {
						// Without ng-repeat
						return function () {
							return null;
						};
					}

					// tests: http://jsbin.com/kosubutilo/1/edit?js,output
					ngRepeat = ngRepeat.nodeValue.match(/ngRepeat:\s*(?:\(.*?,\s*)?([^\s)]+)[\s)]+in\s+([^\s|]+)/);

					var itemsExpr = $parse(ngRepeat[2]);

					return function () {
						return itemsExpr(scope.$parent) || [];
					};
				};


			// Export
			return {
				restrict: 'AC',
				scope: { ngSortable: "=?" },
				link: function (scope, $el) {
					var el = $el[0],
						options = angular.extend(scope.ngSortable || {}, ngSortableConfig),
						watchers = [],
						getSource = getSourceFactory(el, scope),
						sortable
					;

					el[expando] = getSource;

					function _emitEvent(/**Event*/evt, /*Mixed*/item) {
						var name = 'on' + evt.type.charAt(0).toUpperCase() + evt.type.substr(1);
						var source = getSource();

						/* jshint expr:true */
						options[name] && options[name]({
							model: item || source[evt.newIndex],
							models: source,
							oldIndex: evt.oldIndex,
							newIndex: evt.newIndex
						});
					}


					function _sync(/**Event*/evt) {
						var items = getSource();

						if (!items) {
							// Without ng-repeat
							return;
						}

						var oldIndex = evt.oldIndex,
							newIndex = evt.newIndex;

						if (el !== evt.from) {
							var prevItems = evt.from[expando]();

							removed = prevItems[oldIndex];

							if (evt.clone) {
								removed = angular.copy(removed);
								prevItems.splice(Sortable.utils.index(evt.clone), 0, prevItems.splice(oldIndex, 1)[0]);
								evt.from.removeChild(evt.clone);
							}
							else {
								prevItems.splice(oldIndex, 1);
							}

							items.splice(newIndex, 0, removed);

							evt.from.insertBefore(evt.item, nextSibling); // revert element
						}
						else {
							items.splice(newIndex, 0, items.splice(oldIndex, 1)[0]);
						}

						scope.$apply();
					}


					sortable = Sortable.create(el, Object.keys(options).reduce(function (opts, name) {
						opts[name] = opts[name] || options[name];
						return opts;
					}, {
						onStart: function (/**Event*/evt) {
							nextSibling = evt.item.nextSibling;
							_emitEvent(evt);
							scope.$apply();
						},
						onEnd: function (/**Event*/evt) {
							_emitEvent(evt, removed);
							scope.$apply();
						},
						onAdd: function (/**Event*/evt) {
							_sync(evt);
							_emitEvent(evt, removed);
							scope.$apply();
						},
						onUpdate: function (/**Event*/evt) {
							_sync(evt);
							_emitEvent(evt);
						},
						onRemove: function (/**Event*/evt) {
							_emitEvent(evt, removed);
						},
						onSort: function (/**Event*/evt) {
							_emitEvent(evt);
						}
					}));

					$el.on('$destroy', function () {
						angular.forEach(watchers, function (/** Function */unwatch) {
							unwatch();
						});

						sortable.destroy();

						el[expando] = null;
						el = null;
						watchers = null;
						sortable = null;
						nextSibling = null;
					});

					angular.forEach([
						'sort', 'disabled', 'draggable', 'handle', 'animation', 'group', 'ghostClass', 'filter',
						'onStart', 'onEnd', 'onAdd', 'onUpdate', 'onRemove', 'onSort'
					], function (name) {
						watchers.push(scope.$watch('ngSortable.' + name, function (value) {
							if (value !== void 0) {
								options[name] = value;

								if (!/^on[A-Z]/.test(name)) {
									sortable.option(name, value);
								}
							}
						}));
					});
				}
			};
		}]);
});

;/**
 * Created by debayan.das on 1/18/2016.
 */
/**
 * <b>Description<b>: Provides "data-droppable" directive to create droppable area.
 * <b>Usage<b>: add "data-droppable" to any container element to make it droppable area.
 * ************************************************************************************
 * It has the following configurable attributes:
 * data-drop-id : <String> : required : Unique identifier for the drop area. Duplicate is not allowed.
 * data-hover-class : <String> : optional : the class will be added to the droppable while an acceptable
 *                     draggable is being hovered over the droppable. Defaults to "".
 * data-drop-function : <Function> : optional : Additional drop validation. Can be any function on scope
 * ******************************************************************************************
 */
//(function (module) {
app.directive("droppable", [
        "$rootScope",
        "$parse",
        "$compile",
        function ($rootScope, $parse, $compile) {
            return {
                restrict: "A",
                replace: "false",
                scope: {
                    dropFunction: "="
                },
                link: function (scope, elm, attr) {
                    scope.init = function () {
                        scope.dropId = attr["dropId"];
                        scope.questionIndex = attr["questionId"];
                        if (scope.dropId === undefined || scope.dropId === null) {
                            throw new Error("required field 'data-drop-id' not found");
                        }
                        scope._createDroppable();
                    };

                    scope._createDroppable = function () {
                        elm.droppable({
                            accept: scope._getAcceptedElement,
                            hoverClass: scope._getHoverClass(),
                            drop: scope._dropActions
                        });
                    };

                    scope._getAcceptedElement = function (droppedElm) {
                        var dropId = droppedElm.attr("data-drop-col-id");
                        return !(dropId && dropId.split(",").indexOf(scope.dropId) === -1);
                    };
                    scope._dropActions = function (event, ui) {
                        scope.Acceptable = attr["accept"];
                        if (scope.Acceptable != undefined && scope.Acceptable == "false") {
                            return;
                        }
                        var dropElm = scope._getDropElem(ui);
                        elm.append(dropElm);
                        $rootScope.$broadcast("elementDropped", { dragId: ui.draggable.attr("data-drag-id"), dropId: scope.dropId, title: dropElm[0].innerText , questionIndex: scope.questionIndex});
                        $parse(scope.dropFunction)();

                    };

                    scope._getDropElem = function (origDrop) {
                        var dropElm,
                            originalDraggableElm = origDrop.draggable;
                        if (parseInt(originalDraggableElm.attr("data-clone-drop"), 10) !== 1) {
                            dropElm = originalDraggableElm.clone()
                                .css("opacity", "1")
                                .removeAttr("data-clone-drop")
                                .attr("data-drag-id", scope._getCloneDragId(originalDraggableElm));
                            $compile(dropElm)($rootScope);
                        } else {
                            dropElm = originalDraggableElm;
                        }
                        return dropElm.addClass(dropElm.attr("data-drop-class") || "");
                    };
                    scope._getCloneDragId = function (originalDraggableElm) {
                        return originalDraggableElm.attr("data-drag-id")
                            + "_"
                            + (parseInt(originalDraggableElm.attr("data-max-clone-drop"), 10)
                            - parseInt(originalDraggableElm.attr("data-clone-drop"), 10) + 1)
                    };
                    scope._getHoverClass = function () {
                        return attr["hoverClass"] || "";
                    };

                    scope.init();
                }
            }
        }]);
//}(angular.module("angularDragDrop")));
;/**
 * Created by debayan.das on 1/18/2016.
 */

//(function (module) {
app.directive("draggable", [
        function () {
            return {
                restrict: "A",
                replace: false,
                scope: {},
                link: function (scope, elm, attr) {
                    scope.init = function () {
                        scope.dragId = attr["dragId"];
                        scope.isDropped = false;
                        scope.cloneDrop = parseInt(attr["cloneDrop"] || "1", 10);
                        elm.attr("data-clone-drop", scope.cloneDrop);
                        elm.attr("data-max-clone-drop", scope.cloneDrop);
                        //@todo: validate duplicate data id
                        if (scope.dragId === undefined || scope.dragId === null) {
                            throw new Error("required field 'data-drag-id' not found");
                        }
                        scope.$on("elementDropped", scope._dropListener);
                        scope._createDraggable();
                    };
                    scope._createDraggable = function () {
                        if (attr["dragActive"] === "false") {
                            elm.addClass("drag-disable");
                            return;
                        }
                        elm.draggable({
                            revert: scope._isRevert,
                            containment: scope._getContainment(),
                            cursor: "move",
                            helper: "clone",
                            start: scope._dragStart,
                            stop: scope._dragStop
                        });
                    };
                    scope._dragStart = function (e, ui) {
                        scope.isDropped = false;
                        angular.element(ui.helper).addClass(attr["dragClass"] || "");
                        elm.css("opacity", "0.5");
                    };
                    scope._dragStop = function (e, ui) {
                        elm.css("opacity", "1");
                    };

                    scope._getContainment = function () {
                        return attr["containment"] || "body";
                    };

                    scope._dropListener = function (e, data) {
                        if (data.dragId === scope.dragId) {
                            scope._performAfterDrop();
                        }
                    };
                    scope._performAfterDrop = function () {
                        scope.isDropped = true;
                        elm.attr("data-clone-drop", scope.cloneDrop === 1 ? scope.cloneDrop : --scope.cloneDrop);
                    };

                    scope.getCloneDrop = function () {
                        return scope.cloneDrop;
                    };

                    scope._isRevert = function () {
                        return !scope.isDropped;
                    };
                    scope.init();
                }
            };
        }]);
//}(angular.module("angularDragDrop")));
