﻿{
    "quiz": [],
    "settings": {
        "Shuffle": false,
            "BackwardMovement": true,
            "QuestionMap": true,
            "AllowFlagging": false,
            "Attempts": 10,
            "QuestionAttempts": 3,
            "HintOn": true,
            "Timer": {
                "Start": true,
                "Minute": 0
            },
      "PauseTimer": true,
      "SkipQuestion": true,
      "ShowQueFeedback": true,
      "ShowFeedBackAt": {
          "Question": true,
          "Option": true
      },
      "RandomizeQuestion": 0
    }
}