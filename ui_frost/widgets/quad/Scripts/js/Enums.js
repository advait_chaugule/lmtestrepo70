﻿app.constant('Enum', {
    QuestionType: {
        MCSS: 'mcss',
        MCMS: 'mcms',
        FIB: 'fib',
        FIBDND: 'fib_dnd',
        Matching: 'matching',
        Order: 'order',
        GRAPHIC: 6,
        Exhibit: 7,
        HOTSPOTt: 8,
        ChoiceMatrix: 'choicematrix',
        MCMSVIDEO: 9,
        Essay: 'essay',
        Composite: 'composite',
        TextSelection: 'textselection',
        HotSpot: 'hotspot',
        LTD: 'ltd',
        LTDDND:'ltd_dnd'
    },
    Role: {
        Student: 'Student',
        Instructor: 'Instructor'
    },
    OperationMode: {
        Online: 0,
        Offline: 1
    },
    QuestionSubType: {
        mcss_vertical: "mcss_vertical",
        mcss_horizontal: "mcss_horizontal",
        mcss_image_horizontal: "mcss_image_horizontal",
        mcss_image_vertical: "mcss_image_vertical",
        mcss_graphics: "mcss_graphic",
        mcss_select: "mcss_select",

        mcms_vertical: "mcms_vertical",
        mcms_horizontal: "mcms_horizontal",
        mcms_image_horizontal: "mcms_image_horizontal",
        mcms_image_vertical: "mcms_image_vertical",
        mcms_graphics: "mcms_graphic",
        mcms_select: "mcms_select",
        image_sentence: "image_sentence",
        orderingmedia: "media"
    },
    EssayToolBarType: {
        editorbasic: "editor-basic",
        no_toolbar: "no-toolbar",
        basicmatheditor: "basicmatheditor",
    },
    AssessmentActivityType: {
        ShowHint: 1,
        NotLearned: 2,
        BookMark: 3,
        ReportAProblem: 4,
        NoOfTry: 5,
        SubmissionStatus: 6
    },
    QuestionFilterType: {
        NotVisited: 1,
        VisitedNotAnswered: 2,
        Completed: 3,
        ViewAll: 4,
        IsBookMarked: 5,
        ViewAttended:6
    }

});