function getFrameElement() {
    //debugger;
    var iframes = parent.document.getElementsByTagName('object');
    for (var i = iframes.length; i-- > 0; ) {
        var iframe = iframes[i];
        try {
            var idoc = iframe.contentDocument;
        } catch (exc) {
            continue;
        }
        if (idoc === document)
            return iframe;
    }
    return null;
}

var frame = $(getFrameElement()),
    frameEl = frame.context,
    frameDataPath = $(frameEl).attr('data-path');

if (frameDataPath != undefined) {
    var aScript;
    aScript = frameEl.contentDocument.createElement('script');
    aScript.type = "text/javascript";

    var isOffline = (parent.location.href).substring(0, 4) === 'file',
        winLocation = (parent.location.href).split('/'),
        urlHref = '';

    if (isOffline) {
        var getRelPath = (function () {
            for (var i = 0; i < winLocation.length; i++) {
                if (winLocation[i].slice(-5) != 'xhtml')
                    urlHref += winLocation[i] + '/';
            }
        })();

        var isPathOfServer = (function(){
            var isUrlLocal = frameDataPath.substring(0, 4) === 'file';
            return !isUrlLocal;
        })();

        if(isPathOfServer){
            aScript.src = 'http://'+frameDataPath;
        }
        else{
            aScript.src = urlHref + frameDataPath;
        }
    }
    else {
        aScript.src = frameDataPath;
    }

    frameEl.contentDocument.head.appendChild(aScript);

    aScript.onload = function () {
        setTimeout(function () {
            var frameData = advJsonData;
            window.angular.element('.container-main').scope().startQuiz(frameData);
            window.angular.element('.container-main').scope().$apply();
            //frameEl.contentWindow.angular.element('#quizContainer').show();
            if (jQuery.isEmptyObject(frameData.settings)) {
                window.angular.element('#quizContainer').show();
                setTimeout(function(){
                    var assignObjHt = window.angular.element('.container-main').height();
                    frame[0].height = assignObjHt;
                },1000);
            }
            
        }, 500);
    }
}
