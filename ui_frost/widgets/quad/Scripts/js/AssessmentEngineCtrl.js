﻿window.app = angular.module('ACEApp', ['ngResource', 'ng-sortable', 'ngTouch', 'ui.bootstrap', 'ui.sortable', 'textAngular', 'ngCookies', 'dndLists', 'timer', 'oitozero.ngSweetAlert', 'am.multiselect'])
.constant('ngSortableConfig', {
    onEnd: function () {
        console.log('default onEnd()');
    }
});

var Urls = {
    restAuthUrl: '/ACEAuth/oauth2/token',
    restUrl: '/ACEApi/Api',
    routeUrl: '/NewACE'
};
app.controller('AssessmentEngineCtrl', ['$scope', '$location', '$window', '$http', 'assessmentEngineSvc', 'Enum', '$modal', '$sce', '$filter', '$compile', '$timeout', '$modal', 'SweetAlert',
function ($scope, $location, $window, $http, assessmentEngineSvc, Enum, $modal, $sce, $filter, $compile, $timeout, $modal, SweetAlert) {

    $scope.droppableConfig = {
        group: 'grswip',
        animation: 150,
        onAdd: function (evt) {
            //alert(evt.item.parent.child.count);
            //$scope.maxOneConfig.disabled = true;
        }
        , onSort: function (evt) {
            if (evt.action === 'add') {
            }
        }
        , onMove: function (evt) {
            //if (evt.action === 'add') {
            //}
            if (evt.to.childElementCount > 0) {
                return false;
            }
        }
    };

    $scope.SortableFuncConfig = {
        group: 'grswip',
        animation: 150,
        onSort: function (evt) {
            $scope.userHasSelectedOptions = true;
        },
        onMove: function (evt) {
            if ($scope.questionTypeId == Enum.QuestionType.LTDDND && evt.to.childElementCount == 1) {
                return false;
            }
            if ($scope.questionTypeId == Enum.QuestionType.Matching
    && ($scope.layoutType == 'dragdrop_parallel' || $scope.layoutType == 'horizontal' || $scope.layoutType == 'vertical' || $scope.layoutType == 'image')
    && evt.to.childElementCount == 1) {
                return false;
            }

        }
    };

    $scope.draggableConfig = {
        group: 'grswip',
        animation: 150,
        onAdd: function (evt) {
            //alert(evt.item.parent.child.count);
            //$scope.maxOneConfig.disabled = true;
        }
        , onSort: function (evt) {
            if (evt.action === 'add') {
            }
        }
        , onStart: function (/**Event*/evt) {
            //evt.oldIndex;  // element index within parent
            return;
        }
        , onMove: function (evt) {
            //evt.to.childElementCount
            //}
            if (evt.to.childElementCount > 0) {
                return false;
            }
            //evt.preventDefault();
        }
    };

    $scope.SingledroppableConfig = {
        group: 'grswip',
        animation: 150,
        onMove: function (evt) {
            //if (evt.to.childElementCount == 1) {
            //    return false;
            //}
            //alert(111);
            if ($scope.questionTypeId == Enum.QuestionType.Matching
                && ($scope.layoutType == 'dragdrop_parallel' || $scope.layoutType == 'horizontal' || $scope.layoutType == 'vertical' || $scope.layoutType == 'image')
                && evt.to.childElementCount == 1) {
                return false;
            }
        }
    };

    $scope.SingledraggableConfig = {
        group: 'grswip',
        animation: 150,
        onMove: function (evt) {
            if (evt.to.childElementCount > 0 && evt.to.childElementCount < 2) {
                return false;
            }
        }
    };

    $scope.$on('textEntered', function (event, args) {
        $scope.userHasSelectedOptions = true;
    });
    $scope.ChangeSelectedValue = function (value) {
        var selectedValues = '';
        if (value.length > 0) {
            //option.Canvas = [];
            angular.forEach(value, function (item, key) {
                selectedValues = (selectedValues == '') ? item : selectedValues + "," + item;
                //if (option.Canvas.indexOf("<span math dynamic>" + item + "</span>") == -1) {
                //    option.Canvas.push("<span math dynamic>" + item + "</span>");
                //}
            });
        }
        return selectedValues;
    };
    $scope.SetCanvasValue = function (option) {
        if (option.multiSelectItem.length > 0) {
            option.Canvas = [];
            //var selectedDrpItems = $scope.SelectedValues.split(',');
            angular.forEach(option.multiSelectItem, function (item, key) {
                option.Canvas.push({ "AnswerClass": "", "drag": "true", "selectedClass": "", "title": "<span math dynamic>" + item + "</span>" });
            });
        }
    };
    $scope.totalAtemptedQuestions = 1;
    $scope.UserSelectedArea = [];
    $scope.showModal = false;
    if ($location.absUrl().toLowerCase().indexOf("index.html") > -1) { $scope.showModal = true; }
    var routeUrl = Urls.routeUrl;
    var AssignmentID = 54;
    $scope.currentYear = new Date();
    var questionArray = [];
    $scope.mode = 'quiz';
    $scope.operationmode = 1;//0-online,1-offline
    $scope.offline = {};
    $scope.Enum = Enum;
    $scope.showFooter = false;
    $scope.ShowQueFeedback = false;
    $scope.showFeedback = false;
    $scope.showHint = false;
    $scope.visibleHints = [];
    $scope.isLastquestion = true;
    $scope.isBackButtonOn = false;
    $scope.hintCount = 0;
    $scope.isUndefined = function (thing) {
        return (typeof thing === "undefined");
    };
    $scope.AssignmentQuestions = [];
    $scope.questions = [];
    $scope.questiontosave = {};
    $scope.showExhibit = false;
    $scope.isComposite = false;
    $scope.isEssay = false;
    $scope.ShowHintBox = false;
    $scope.BackwardMovement = true;
    $scope.SkipQuestion = true;
    $scope.disableBackBtn = false;
    $scope.disableNextBtn = false;
    $scope.userHasSelectedOptions = false;
    $scope.CorrectAnswerCount = 0;
    $scope.DisableCorrectAnswer = false;

    $scope.ShowGlobalFeedback = false;
    $scope.ShowGlobalIncorrectFeedback = false;
    $scope.ShowGlobalCorrectFeedback = false;
    $scope.showOptionFeedbackModal = false;
    $scope.showOptionFeedbackIcon = false;
    $scope.showCheckAnswerbtn = false;

    $scope.isPaused = false;
    $scope.showTimer = false;
    $scope.callbackTimer = {};
    $scope.timerRunning = true;
    var timeStarted = false;

    //UserAssessmentActivity related variables
    $scope.BookMarked = false;
    $scope.showAttempt = false;
    $scope.NoLearned = true;
    $scope.QuestionMapBookMarkClicked = false;
    $scope.QuestionMapViewAllClicked = false;
    $scope.UserAssessmentActivityDetail = {
        QuestionStatus: 'not-visited',
        IsBookMarked: '',
        NotLearned: '',
        HintsUsed: 0,
        NoOfRetry: 0,
        TimeSpent: 0,
        ReportAProblem: '',
        QuizQuestID: '',
        UserAssessmentActivityType: 0,
        QuestionNo: 0
    };

    $scope.mathEditorLatex = "";
    $scope.mathEditorPrevLatex = "";
    $scope.mathEditorCurrLatex = "";
    var mathEditorPrevLatex = "";

    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.trustAsHtml = function (string) {
        return $sce.trustAsHtml(string);
    };

    $scope.getQuizDetails = function (quizId) {
        $scope.quizID = quizId;
        //assessmentEngineSvc.SetQuizID(quizID);
        var params = { 'quizId': quizId };
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            //if ($location.absUrl().indexOf("index.html") > -1) {
            $scope.loadjsonfile();
            //}
        }
        else {
            assessmentEngineSvc.FetchQuizDetails(params).$promise
                   .then(function (response) {
                       $scope.quizName = response.AssignmentName;
                       $scope.CourseID = response.ClassId;
                       // var questions = '';
                       if (!$scope.isUndefined(response.QuestionIds) && response.QuestionIds != null && response.QuestionIds != "") {
                           questionsList = angular.fromJson(response.QuestionIds);
                           angular.forEach(questionsList, function (question, key) {
                               $scope.questions.push(question.QuestionId);
                           });
                           $scope.startQuiz($scope.questions);
                       }

                   }).catch(function (er) {
                       $scope.Message = er.error_description;
                   });
        }
    };

    $scope.startQuiz = function (questions, isCompositeQuestion) {
        //debugger;
        //  $scope.quizID = quizID;
        //var params = { quizID: quizID, currentQuestion: 0 };
        //var params = { 'questionId': questions[0], 'assignmentId': AssignmentID };
        $scope.showFooter = true;
        $scope.AssignmentQuestions = [];
        var getQuestion;
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            if (questions != undefined) {
                $scope.offline.quiz = questions.quiz;

                $scope.AssignmentSetting = questions.settings;
                if (questions.quiz.length > 1) {
                    $scope.isLastquestion = false;
                    if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
                        $scope.showModal = true;
                    }
                    else {
                        $scope.showModal = false;
                    }
                }
                else {
                    $scope.isBackButtonOn = false;
                    $scope.isLastquestion = true;
                    if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
                        $scope.showModal = true;
                    }
                    else {
                        $scope.showModal = false;
                    }
                }

            }

            if ($scope.isLastquestion == true && $scope.showModal == false) {
                $scope.offline.quiz[0].CurrentQuestion = 1;
                $scope.initializeQuiz($scope.offline.quiz[0]);
            }
            else if ($scope.isLastquestion == false && $scope.showModal == false) {
                $scope.offline.quiz[0].CurrentQuestion = 1;
                $scope.initializeQuiz($scope.offline.quiz[0]);
            }
            else if (isCompositeQuestion || $scope.isComposite || $location.absUrl().indexOf("CompositeIndex.html") > -1) {
                $scope.offline.quiz[0].CurrentQuestion = 1;
                $scope.initializeQuiz($scope.offline.quiz[0]);
            }
            else {
                var modalInstance = $modal.open({
                    templateUrl: 'SettingDetails.html',
                    scope: $scope,
                    backdrop: 'static',
                    keyboard: false
                });

                $scope.Ok = function () {
                    modalInstance.close('cancel');
                    $('#quizContainer').show();
                    if ($scope.offline.quiz.length == 1)
                        $scope.isLastquestion = true;
                };

                modalInstance.result.then(function (result) {
                    var index = 0;
                    angular.forEach($scope.offline.quiz, function (question, key) {
                        var UserAssessmentActivityDetail = {
                            QuestionStatus: 'not-visited',
                            IsBookMarked: '',
                            NotLearned: '',
                            HintsUsed: 0,
                            NoOfRetry: 0,
                            TimeSpent: 0,
                            ReportAProblem: '',
                            QuizQuestID: '',
                            UserAssessmentActivityType: 0,
                            QuestionNo: 0
                        };
                        index = index + 1;
                        UserAssessmentActivityDetail.QuizQuestID = question.QuestionId;
                        UserAssessmentActivityDetail.QuestionNo = index;
                        $scope.AssignmentQuestions.push(UserAssessmentActivityDetail);
                    });
                    $scope.searchFilter = { QuestionStatus: '' };
                    $scope.offline.quiz[0].CurrentQuestion = 1;
                    $scope.initializeQuiz($scope.offline.quiz[0]);

                    $scope.Timer = angular.fromJson($scope.AssignmentSetting.Timer);

                    if ($scope.Timer != undefined) {
                        if ($scope.Timer.Start == true && $scope.Timer.Minute != 0) {
                            $scope.showTimer = true;
                            $scope.ActiveTimer($scope.Timer, $scope.CurrentQuestion);
                        }
                        else {
                            $scope.showTimer = false;
                        }
                    }
                }, function () { });
            }

        }
        else {
            assessmentEngineSvc.StartQuiz(JSON.stringify(params)).$promise
                .then(function (response) {
                    response[0].QuestionData = $.parseJSON(response[0].QuestionData)
                    response = response[0];
                    response.CurrentQuestion = 1;
                    $scope.initializeQuiz(response);
                    $scope.ActiveTimer($scope.CurrentQuestion);
                }).catch(function (er) {
                    $scope.Message = er.error_description;
                });
        }
        $('.btn-arrow-left').prop('disabled', true);
        resize_fun();
        if ($scope.offline.quiz.length == 1) {
            $('.btn-arrow-right').html('Submit');
            $('.btn-arrow-right').addClass('dummyClass');
            $('.btn-arrow-right').removeClass('btn-arrow-right');
            $scope.isBackButtonOn = true;
            $scope.isLastquestion = true;
        }
        else {
            $('.dummyClass').html(' Next <i class="fa fa-arrow-circle-right fa-fw"></i>');
            $('.dummyClass').addClass('btn-arrow-right');
            $('.dummyClass').removeClass('dummyClass');
            $scope.isLastquestion = false;
        }
    };
    $scope.goTo = function (index, Next) {
        $scope.mathEditorRefactor();
        $("#loader").show();
        $("#loader").removeClass('hide');
        $scope.showHint = false;
        $scope.showExhibit = false;
        //$scope.userHasSelectedOptions = false;

        $scope.ShowGlobalFeedback = false;
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.ShowGlobalCorrectFeedback = false;
        $scope.showOptionFeedbackModal = false;

        $scope.DisableCorrectAnswer = false;
        $scope.showAttempt = false;
        $scope.QuestionAttempt = 0;
        $scope.SingledroppableConfig.disabled = false;
        $scope.SingledraggableConfig.disabled = false;
        $scope.SortableFuncConfig.disabled = false;
        $scope.CurrentQuestion.SelectedOptions = $scope.userSelectedOptions();
        $scope.CurrentQuestion.UserAssessmentActivityDetail = $scope.UserAssessmentActivityDetail;
        $scope.CurrentQuestion.CorrectAnswerCount = $scope.CorrectAnswerCount;
        //$scope.CurrentQuestion.isNext = Next;

        $scope.MapQuestionToSave(Next, index);

        if ($scope.userHasSelectedOptions == true) {
            $scope.SetQuestionMapSetting(true, Enum.AssessmentActivityType.SubmissionStatus);
        } else {
            if ($scope.AssignmentQuestions.length > 0) {
                $scope.AssignmentQuestions[$scope.currentPage - 1].QuestionStatus = 'answer-visited';
            }
        }
        $scope.CurrentQuestion.TimeSpent = $scope.PassiveTimer.stopTimer();
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            if (index <= $scope.offline.quiz.length) {
                if (index == 1) {
                    $('.btn-arrow-left').prop('disabled', true);
                    $scope.isLastquestion = false;
                    $scope.isBackButtonOn = false;
                }
                else {
                    $('.btn-arrow-left').prop('disabled', false);
                    $scope.isBackButtonOn = true;
                }
                if ($scope.offline.quiz.length == index) {
                    $('.btn-arrow-right').html('Submit');
                    $('.btn-arrow-right').addClass('dummyClass');
                    $('.btn-arrow-right').removeClass('btn-arrow-right');
                    $scope.isBackButtonOn = true;
                    $scope.isLastquestion = true;
                }
                else {
                    $('.dummyClass').html(' Next <i class="fa fa-arrow-circle-right fa-fw"></i>');
                    $('.dummyClass').addClass('btn-arrow-right');
                    $('.dummyClass').removeClass('dummyClass');
                    $scope.isLastquestion = false;
                }

                $scope.offline.quiz[index - 1].CurrentQuestion = index;
                $scope.initializeQuiz($scope.offline.quiz[index - 1]);
                //  if (document.getElementById('compositeIframe') != null) document.getElementById('compositeIframe').contentWindow.location.reload();
            }
            else {
                $scope.stopTimer();
                $scope.showFeedback = true;
                $scope.generateFeedback();
            }
            $("#loader").hide();
            $("#loader").addClass('hide');
        }
        else {
            if (index == 1) {
                $('.btn-arrow-left').prop('disabled', true);
            }
            else {
                $('.btn-arrow-left').prop('disabled', false);
            }

            if ($scope.totalItems == index) {
                $('.btn-arrow-right').html('Submit');
                $('.btn-arrow-right').removeClass('btn-arrow-right');
            }
            assessmentEngineSvc.FetchQuestion(serializeData(params)).$promise
                .then(function (response) {

                    if ($scope.questiontosave.NextQuestionId !== undefined) {//index <= $scope.totalItems
                        response[0].QuestionData = $.parseJSON(response[0].QuestionData)
                        response = response[0];
                        response.CurrentQuestion = index;
                        $scope.initializeQuiz(response);
                    }
                    else {
                        $scope.TimerClass.stopTimer(function () { });
                        $scope.generateFeedback();
                        $scope.showFeedback = true;
                    }
                }).catch(function (er) {
                    $scope.Message = er.error_description;
                });
        }


    };

    $scope.MapQuestionToSave = function (Next, index) {
        $scope.questiontosave.ApplicationID = 0,
        $scope.questiontosave.ProductID = 9209,
        $scope.questiontosave.CourseID = $scope.CourseID,
        $scope.questiontosave.ItemGUID = AssignmentID,
        $scope.questiontosave.UserID = 0,
        $scope.questiontosave.QuestionGUID = $scope.CurrentQuestion.QuestionId,
        $scope.questiontosave.SelectedOptions = $scope.userSelectedOptions(),
        $scope.questiontosave.MaxScore = 0,
        $scope.questiontosave.UserScore = 0,
        $scope.questiontosave.UserAnswerText = "",
        $scope.questiontosave.UserAudioFile = "",
        $scope.questiontosave.TimeSpent = 0,
        $scope.questiontosave.isNext = Next,
        $scope.questiontosave.CurrentQuestion = index,
        $scope.questiontosave.QuestionTypeID = $scope.CurrentQuestion.QuestionTypeId,
        $scope.questiontosave.NextQuestionId = $scope.questions[index - 1],
        $scope.questiontosave.AssignmentID = AssignmentID
    };

    $scope.generateFeedback = function () {
        $scope.showHint = false;
        $('#spnMathQuillCreateMathEquation').mathquill();
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            //$scope.loadjsonfile();
            $scope.FeedBack = $scope.offline.quiz;
            $.each($scope.FeedBack, function (index, value) {
                $scope.FeedBack[index].QuestionData = value.QuestionData;
                $scope.FeedBack[index].QuestionCount = index + 1;
                $scope.FeedBack[index].QuestionOptions = value.QuestionData.choices;
                //$scope.FeedBack[index].layoutType = value.QuestionData.question_layout.text;;
                $scope.FeedBack[index].draggableItems = [];
                $scope.FeedBack[index].leftvertical = "";
                $scope.FeedBack[index].rightvertical = "";
                $scope.FeedBack[index].ImageSrc = '';
                var questionType = value.QuestionTypeId;

                if (!$scope.isUndefined(value.QuestionData.stem_details) && value.QuestionData.stem_details.type == 'algorithimic') {
                    $scope.SetFeedbackAlgorithimicType(value);
                }

                if (!$scope.isUndefined(value.QuestionData.question_type)) {
                    if ((value.QuestionData.question_type.text == 'ltd_dnd' || value.QuestionData.question_type.text == 'ltd' || value.QuestionData.question_type.text == 'hotspot' || value.QuestionData.question_type.text == 'composite' || value.QuestionData.question_type.text == 'choicematrix' || value.QuestionData.question_type.text == 'mcss' || value.QuestionData.question_type.text == 'mcms' || value.QuestionData.question_type.text == 'order' || value.QuestionData.question_type.text == 'fib' || value.QuestionData.question_type.text == 'fib_dnd' || value.QuestionData.question_type.text == 'matching' || value.QuestionData.question_type.text == 'essay' || value.QuestionData.question_type.text == 'textselection') && value.QuestionData.question_type.text.length > 0) {
                        questionType = value.QuestionData.question_type.text;
                    }
                }
                $scope.FeedBack[index].questionTypeId = questionType;
                if (questionType == Enum.QuestionType.Composite) {
                    $scope.SetCompositeType($scope.FeedBack[index], index);
                }
                else if (questionType == Enum.QuestionType.TextSelection) {
                    if ($scope.FeedBack[index].QuestionData.textwithblanks != undefined) {
                        if ($scope.FeedBack[index].QuestionData.textwithblanks.text != undefined && $scope.FeedBack[index].QuestionData.textwithblanks.text != "")
                            questiontext = $scope.FeedBack[index].QuestionData.textwithblanks.text;
                    }

                    if (!$scope.isUndefined(value.QuestionData.exhibit) && value.QuestionData.exhibit.length > 0 && value.QuestionData.exhibit[0].exhibit_type != "") {
                        value.showExhibit = true;
                        if (value.QuestionData.exhibit[0].exhibit_type == 'image') {
                            var imagePath = String(value.QuestionData.exhibit[0].path);
                            value.QuestionData.exhibit[0].path = imagePath;
                        }
                    }
                    else {
                        value.showExhibit = false;
                    }

                    $scope.FeedBack[index].PassageText = "";
                    angular.forEach($scope.FeedBack[index].QuestionData.choices, function (option, key) {
                        var startTagToReplace = option.text;
                        var endTagToReplace = option.text.substring(0, 2) + "/" + option.text.substring(2);
                        $scope.FeedBack[index].QuestionData.choices[key].key = key;       // Index of option            
                        $scope.FeedBack[index].QuestionData.choices[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect            
                        var answerClass = '';
                        //ar correctAnswers= $scope.FeedBack[index].QuestionData.correct_answer.split(',');
                        if ($scope.FeedBack[index].SelectedOptions) {
                            var selectedOption = angular.fromJson($scope.FeedBack[index].SelectedOptions);
                            $(selectedOption).each(function (textindex, value) {
                                if (value.choiceid == option.choiceid) {
                                    if ($scope.FeedBack[index].QuestionData.correct_answer.indexOf(value.choiceid) != -1) {
                                        answerClass = 'selectedC correct';
                                    }
                                    else {
                                        answerClass = 'selectedC incorrect';
                                    }
                                }
                            });
                        }

                        questiontext = questiontext.replace(startTagToReplace, "&nbsp;&nbsp;<span class='option selectable " + answerClass + "' style='cursor: default;'> <span class='sign'><span class='sr-only'></span><span class='fa'></span></span>");
                        questiontext = questiontext.replace(endTagToReplace, "</span>&nbsp;&nbsp;");
                    });
                    $scope.FeedBack[index].PassageText = questiontext;
                }
                else if (questionType == Enum.QuestionType.HotSpot) {
                    //var regex = /<img.*?src='(.*?)'/;
                    if (!$scope.isUndefined($scope.FeedBack[index].QuestionData.question_media.media) && $scope.FeedBack[index].QuestionData.question_media.media.length > 0 && $scope.FeedBack[index].QuestionData.question_media.media != "") {
                        //$scope.FeedBack[index].ImageSrc = regex.exec($scope.FeedBack[index].QuestionData.question_media.media)[1];
                        var imgObject = document.createElement('div');
                        imgObject.innerHTML = value.QuestionData.question_media.media;
                        $scope.FeedBack[index].ImageSrc = $(imgObject).find('img').attr('src');
                    }
                    if ($scope.FeedBack[index].SelectedOptions) {
                        var correctoptions = $scope.FeedBack[index].QuestionData.correct_answer.split(",");
                        var selectedoptions = $.parseJSON($scope.FeedBack[index].SelectedOptions);
                        $scope.FeedBack[index].SelectedOptions = selectedoptions.map(function (x) { return x.choiceid; }).toString();

                        $($scope.FeedBack[index].QuestionData.choices).each(function (cindex, value) {
                            $(selectedoptions).each(function (selectindex, svalue) {
                                if (value.choiceid == parseInt(svalue.choiceid)) {
                                    if (correctoptions.indexOf(value.choiceid.toString()) != -1) {
                                        value.isCorrect = true;
                                        value.currentClass = "correct";
                                    }
                                    else {
                                        value.isCorrect = false;
                                        value.currentClass = "incorrect";
                                    }
                                }
                            });

                        });
                    }
                }
                else if (questionType == Enum.QuestionType.LTD || questionType == Enum.QuestionType.LTDDND) {
                    //var regex = /<img.*?src='(.*?)'/;                   
                    if (!$scope.isUndefined($scope.FeedBack[index].QuestionData.question_media.media) && $scope.FeedBack[index].QuestionData.question_media.media.length > 0 && $scope.FeedBack[index].QuestionData.question_media.media != "") {
                        //$scope.FeedBack[index].ImageSrc = regex.exec($scope.FeedBack[index].QuestionData.question_media.media)[1];
                        var imgObject = document.createElement('div');
                        imgObject.innerHTML = value.QuestionData.question_media.media;
                        $scope.FeedBack[index].ImageSrc = $(imgObject).find('img').attr('src');
                    }
                    $scope.FillFeedbackDroppableItems($scope.FeedBack[index]);
                    angular.forEach($scope.FeedBack[index].QuestionData.choices, function (option, key) {
                        var inputValue = "";
                        if ($scope.FeedBack[index].SelectedOptions) {
                            var selectedOption = angular.fromJson($scope.FeedBack[index].SelectedOptions);
                            $(selectedOption).each(function (fibindex, value) {
                                if (value.noforeachblank == option.noforeachblank && value.inputvalue != "") {
                                    option.LTDText = value.inputvalue;
                                    inputValue = value.inputvalue;
                                    option.AnswerClass = (value.isCorrectAnswer) ? "correct" : "incorrect";
                                }
                            });
                        }

                        if (option.interaction == 'dragdrop') {
                            $scope.FeedBack[index].LayoutType = value.QuestionData.question_layout.text;

                            if (option.interaction == 'dragdrop') {
                                angular.forEach($scope.FeedBack[index].draggableItems, function (ditem, dindex) {
                                    if ($filter('lowercase')(inputValue) == $filter('lowercase')('<span math dynamic>' + ditem.title + '</span>')) {
                                        ditem.title = "__________";
                                    }
                                });
                            }


                            //htmlText = "<span class='option drop " + answerClass + "'><span class='option drag'>" + inputValue + "</span><span class='sign'><span class='fa'></span></span></span>";
                            if ($scope.FeedBack[index].QuestionData.question_layout.text === "right" || $scope.FeedBack[index].QuestionData.question_layout.text === "left" || $scope.FeedBack[index].QuestionData.question_layout.text === "vertical") {
                                $scope.FeedBack[index].leftvertical = "col-md-10";
                                $scope.FeedBack[index].rightvertical = "col-md-2 right";
                            }
                            else if ($scope.FeedBack[index].QuestionData.question_layout.text === "row") {
                                $scope.FeedBack[index].leftvertical = "";
                                $scope.FeedBack[index].rightvertical = "text-center";
                            }

                            //$scope.FeedBack[index].QuestionOptions[key].isCorrectAnswer = answerClass;
                            //$scope.FeedBack[index].QuestionOptions[key].selectedAnswer = inputValue;
                        }

                        //questiontext = questiontext.replace(textToReplace, htmlText);

                    });
                    //$scope.FeedBack[index].LTDText = questiontext;
                }
                else if (questionType == Enum.QuestionType.FIB || questionType == Enum.QuestionType.FIBDND || questionType == Enum.QuestionType.Matching) {
                    var questiontext = "";
                    if (value.QuestionData.textwithblanks != undefined) {
                        if (value.QuestionData.textwithblanks.text != undefined && value.QuestionData.textwithblanks.text != "")
                            questiontext = $scope.FeedBack[index].QuestionData.textwithblanks.text;
                    }

                    $scope.FillFeedbackDroppableItems($scope.FeedBack[index]);
                    angular.forEach($scope.FeedBack[index].QuestionData.choices, function (option, key) {
                        var textToReplace = option.noforeachblank;
                        var htmlText = "";
                        var answerClass = "";
                        var inputValue = "";

                        if ($scope.FeedBack[index].SelectedOptions) {
                            var selectedOption = angular.fromJson($scope.FeedBack[index].SelectedOptions);
                            $(selectedOption).each(function (fibindex, value) {
                                if (value.noforeachblank == option.noforeachblank && value.inputvalue != "") {
                                    inputValue = value.inputvalue;
                                    answerClass = (value.isCorrectAnswer) ? "correct" : "incorrect";
                                }
                            });
                        }
                        if (option.interaction == 'textentry') {
                            htmlText = "<span class='option " + answerClass + "'><input type='text' class='form-control' disabled value='" + inputValue + "'><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
                        }
                        else if (option.interaction == 'inline') {
                            htmlText = "<span class='option " + answerClass + "'><select class='form-control' disabled><option>" + inputValue + "</option></select><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
                        }
                        else if (option.interaction == 'dragdrop' || option.interaction == 'container') {
                            $scope.FeedBack[index].LayoutType = value.QuestionData.question_layout.text;

                            if (option.interaction == 'dragdrop') {
                                angular.forEach($scope.FeedBack[index].draggableItems, function (ditem, dindex) {
                                    if ($filter('lowercase')(inputValue) == $filter('lowercase')(ditem.title)) {
                                        ditem.title = "__________";
                                    }
                                });
                            }
                            else if (option.interaction == 'container') {
                                var input = !!inputValue ? inputValue.map(function (x) { return x.title; }) : [];
                                angular.forEach($scope.FeedBack[index].draggableItems, function (ditem, dindex) {
                                    if (input.indexOf(ditem.title) != -1) {
                                        ditem.title = "__________";
                                    }
                                });
                                if (option.Canvas != undefined) {
                                    var isOptionIncorrect = false;
                                    var isOptionAnswered = false;
                                    for (var i = 0; i < option.Canvas.length; i++) {
                                        isOptionAnswered = true;
                                        if (option.Canvas[i].AnswerClass == "incorrect") {
                                            isOptionIncorrect = true;
                                        }

                                    }
                                    if (isOptionAnswered) {
                                        option.AnswerClass = isOptionIncorrect ? "incorrect" : "correct";
                                    }
                                }
                            }

                            if ($scope.FeedBack[index].QuestionData.question_layout.text != "dragdrop_parallel") {
                                htmlText = "<span class='option drop " + answerClass + "'><span class='option drag'>" + inputValue + "</span><span class='sign'><span class='fa'></span></span></span>";
                                if ($scope.FeedBack[index].QuestionData.question_layout.text === "right" || $scope.FeedBack[index].QuestionData.question_layout.text === "left" || $scope.FeedBack[index].QuestionData.question_layout.text === "vertical") {
                                    $scope.FeedBack[index].leftvertical = "col-md-10";
                                    $scope.FeedBack[index].rightvertical = "col-md-2 right";
                                }
                                else if ($scope.FeedBack[index].QuestionData.question_layout.text === "row") {
                                    $scope.FeedBack[index].leftvertical = "";
                                    $scope.FeedBack[index].rightvertical = "text-center";
                                }

                                $scope.FeedBack[index].QuestionOptions[key].isCorrectAnswer = answerClass;
                                $scope.FeedBack[index].QuestionOptions[key].selectedAnswer = inputValue;
                            }
                            else {
                                $scope.FeedBack[index].QuestionOptions[key].isCorrectAnswer = answerClass;
                                $scope.FeedBack[index].QuestionOptions[key].selectedAnswer = inputValue;
                            }
                        }

                        questiontext = questiontext.replace(textToReplace, htmlText);

                    });
                    $scope.FeedBack[index].FIBText = questiontext;

                }

                else if (questionType == Enum.QuestionType.MCSS || questionType == Enum.QuestionType.MCMS) {
                    value.questionSubType = value.QuestionData.question_interaction.text;
                    value.LayoutType = value.QuestionData.question_layout.text;
                    if (!$scope.isUndefined(value.QuestionData.exhibit) && value.QuestionData.exhibit.length > 0 && value.QuestionData.exhibit[0].exhibit_type != "") {
                        value.showExhibit = true;
                        if (value.QuestionData.exhibit[0].exhibit_type == 'image') {
                            var imagePath = String(value.QuestionData.exhibit[0].path);
                            value.QuestionData.exhibit[0].path = imagePath;
                        }
                    }
                    else {
                        value.showExhibit = false;
                    }
                }

                $($scope.FeedBack[index].QuestionData.choices).each(function (indexOpt, valueOpt) {
                    //if (valueOpt.assess == true) {
                    if (questionType == Enum.QuestionType.MCSS || questionType == Enum.QuestionType.MCMS) {
                        var correctoptions = $scope.FeedBack[index].QuestionData.correct_answer.split(",");
                        $scope.FeedBack[index].CorrectOptions = correctoptions.toString();
                        var sSrchTxt = String(valueOpt.text);
                        if (sSrchTxt.indexOf('<img') != -1) {
                            var options = String(valueOpt.text).split('<img');
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].text = options[0];
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].MCSSImage = "<img" + options[1];
                        }
                        else {
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].text = sSrchTxt;
                            $scope.FeedBack[index].QuestionData.choices[indexOpt].MCSSImage = null;
                        }

                        if (this.isAnswered == true) {
                            $(correctoptions).each(function (correctindex) {
                                if (parseInt(valueOpt.choiceid) != parseInt(correctoptions[correctindex])) {
                                    valueOpt.isCorrect = false;
                                    valueOpt.currentClass = "incorrect";
                                }
                            });
                        }

                        $(correctoptions).each(function (correctindex) {
                            if (parseInt(valueOpt.choiceid) == parseInt(correctoptions[correctindex])) {
                                valueOpt.isCorrect = true;
                                valueOpt.currentClass = "correct";
                            }
                        });
                    }

                    if (questionType == Enum.QuestionType.ChoiceMatrix) {
                        if (value.SelectedOptions) {
                            var selectedOpt = angular.fromJson(value.SelectedOptions);
                            $(selectedOpt).each(function (selectindex, selectvalue) {
                                var inputValues = selectvalue.inputvalue.split("-");
                                var optionindex = indexOpt + 1;
                                $($scope.FeedBack[index].QuestionData.choices[indexOpt].suboption).each(function (index1, value1) {
                                    if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                                        $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isAnswered = true;
                                        var assess;
                                        if (parseInt(inputValues[1]) == 1) {
                                            assess = true;
                                        }
                                        else {
                                            assess = false;
                                        }
                                        if ($scope.FeedBack[index].QuestionData.choices[indexOpt].assess === assess) {
                                            $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isCorrect = true;
                                        }
                                        else {
                                            $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isCorrect = false;
                                        }
                                    }
                                    else {
                                        var val = (value1.value === "true");
                                        if ($scope.FeedBack[index].QuestionData.choices[indexOpt].assess === val) {
                                            $scope.FeedBack[index].QuestionData.choices[indexOpt].suboption[index1].isCorrect = true;
                                        }
                                    }
                                });
                            });

                        }
                    }

                    else if (questionType == Enum.QuestionType.Order) {
                        $scope.FeedBack[index].OrderSetting = $scope.FeedBack[index].QuestionData.question_interaction.text;

                        var correctoptions = $scope.FeedBack[index].QuestionData.correct_answer.split(",");
                        var optionindex = valueOpt.choiceid;
                        if (parseInt(optionindex) == correctoptions[indexOpt]) {
                            valueOpt.isCorrect = true;
                        }
                        else {
                            valueOpt.isCorrect = false;
                        }

                        if ($scope.orderType == "media" && $scope.FeedBack[index].QuestionData.question_layout != null)
                            $scope.FeedBack[index].LayoutType = $scope.FeedBack[index].QuestionData.question_layout.text;
                    }
                });
                if (questionType == Enum.QuestionType.Essay) {
                    $scope.text = '';
                    $scope.FeedBack[index].EssayText = '';
                    if (value.SelectedOptions) {
                        if ($scope.FeedBack[index].QuestionData.toolbar.type == "math_3-5_palette" || $scope.FeedBack[index].QuestionData.toolbar.type == "math_6-8_palette") {
                            $scope.FeedBack[index].IsFeedbackMathType = true;
                            $('#spnMathQuillCreateMathEquation').mathquill('latex', value.SelectedOptions[0]);

                            // Added the logic as Bootstrap is conflicting with the MATH font.
                            $('#spnMathQuillCreateMathEquation span.sqrt-prefix[style^="transform"]').attr("style", "transform: scale(1,1);");
                            $('#spnMathQuillCreateMathEquation span.scaled[style^="transform"]').attr("style", "transform: scale(1,1);");

                            $('#divMathQuillCreateMathEquation span').each(function () {
                                if ($(this).html() == '?') {
                                    $(this).html('');
                                }
                            });

                            $scope.FeedBack[index].feedbackMathType = $sce.trustAsHtml($('#divMathQuillCreateMathEquation').html());
                        } else {
                            $scope.FeedBack[index].IsFeedbackMathType = false;
                            $scope.FeedBack[index].EssayText = value.SelectedOptions; //$sce.trustAsHtml(value.SelectedOptions)
                        }
                    }

                }
                else if (questionType == Enum.QuestionType.Order || questionType == Enum.QuestionType.TextSelection) {
                    if (value.SelectedOptions) {
                        var userSelectedOptions = "";
                        var selectedOpt = angular.fromJson(value.SelectedOptions);
                        $(selectedOpt).each(function (sindex, svalue) {
                            if (userSelectedOptions == "") {
                                userSelectedOptions = svalue.choiceid;
                            }
                            else {
                                userSelectedOptions = userSelectedOptions + "," + svalue.choiceid;
                            }
                        });
                        value.SelectedOptions = userSelectedOptions;
                    }
                }

            });
            $scope.getFeedbackOptions($scope.FeedBack);
        }


    };

    $scope.StopClickEvent = function (event) {
        return false;
    };

    $scope.SetFeedbackAlgorithimicType = function (data) {
        if (!$scope.isUndefined(data.AlgorithmicVariableArray) && data.QuestionData.stem_details.textforeachvalue.length > 0) {
            angular.forEach(data.QuestionData.stem_details.textforeachvalue, function (textDetail, key) {
                angular.forEach(data.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    data.QuestionData.question_stem.text = data.QuestionData.question_stem.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                });
            });
        }
    }

    $scope.FillFeedbackDroppableItems = function (data) {
        isDistractorsPushDone = false;
        angular.forEach(data.QuestionData.choices, function (option, key) {
            if (isDistractorsPushDone == false) {
                angular.forEach(data.QuestionData.distractors, function (titem, tindex) {
                    data.draggableItems.push({ 'title': titem.text, 'drag': false });
                });
                isDistractorsPushDone = true;
            }

            angular.forEach(option.textforeachblank, function (titem, tindex) {
                data.draggableItems.push({ 'title': titem.text, 'drag': false });
            });
        });
    }

    $scope.getFeedbackOptions = function (data) {
        $(data).each(function (index, value) {
            var questionType = value.QuestionTypeId;
            if (!$scope.isUndefined(value.QuestionData.question_type)) {
                if ((value.QuestionData.question_type.text == 'hotspot' || value.QuestionData.question_type.text == 'mcss' || value.QuestionData.question_type.text == 'mcms' || value.QuestionData.question_type.text == 'order' || value.QuestionData.question_type.text == 'fib' || value.QuestionData.question_type.text == 'fib_dnd' || value.QuestionData.question_type.text == 'matching' || value.QuestionData.question_type.text == 'essay' || value.QuestionData.question_type.text == 'textselection') && value.QuestionData.question_type.text.length > 0) {
                    questionType = value.QuestionData.question_type.text;
                }
            }
            if (questionType != Enum.QuestionType.HotSpot && questionType != Enum.QuestionType.TextSelection && questionType != Enum.QuestionType.Order && questionType != Enum.QuestionType.FIB && questionType != Enum.QuestionType.FIBDND && questionType != Enum.QuestionType.Matching && questionType != Enum.QuestionType.Essay && questionType != Enum.QuestionType.ChoiceMatrix) {
                if (value.SelectedOptions != "" && value.SelectedOptions != null && value.SelectedOptions != "[]") {
                    var selectedOptions = angular.fromJson(value.SelectedOptions);
                }
                if (value.CorrectOptions != null) {
                    var correctoptions = value.CorrectOptions.split(",");
                }
                $(data[index].QuestionData.choices).each(function (index1, value1) {
                    var optionindex = index1 + 1;
                    if (selectedOptions) {
                        $(selectedOptions).each(function (selectindex, selectvalue) {
                            var selectedindex = parseInt(selectvalue.choiceid) - 1;
                            $(correctoptions).each(function (correctindex, correctvalue) {
                                var correctindex = correctvalue - 1;
                                if (correctvalue === parseInt(selectvalue)) {
                                    data[index].QuestionData.choices[selectedindex].isCorrect = true;
                                }
                                else {

                                    data[index].QuestionData.choices[selectedindex].isCorrect = false;
                                    data[index].QuestionData.choices[correctindex].isCorrect = true;
                                }
                                data[index].QuestionData.choices[selectedindex].isSelected = true;
                            });
                        });
                    }
                });
            }

        });

    };

    $scope.loadjsonfile = function () {
        $("#loader").show();
        $("#loader").removeClass('hide');
        var file = "Scripts/js/offlinejson.js";
        $http.get(file)
         .then(function (res) {
             var data = res.data.quiz;
             var settings = res.data.settings;
             //var data = jQuery.grep(res.data.quiz, function (n, i) {
             //    return (parseInt(n.ItemID) == parseInt($scope.quizID));
             //});;
             $scope.offline.quizInfo = "Demo Quiz";//data[0].quizName;
             $scope.offline.quiz = data;
             $scope.AssignmentSetting = settings;

             if ($scope.showFeedback == true) {
                 $scope.quizName = "Feedback - Demo";
             }
             else {
                 $scope.quizName = "Demo";
             }
             if (data.length > 0) {
                 $scope.startQuiz();
             }
             else {
                 $scope.BackwardMovement = false;
             }
             $("#loader").hide();
             $("#loader").addClass('hide');
         });
    }

    $scope.autoSubmit = function () {
        $scope.showFeedback = true;
        $scope.generateFeedback();
        $scope.$apply();
    };

    $scope.ShowGlobalFeebackDataForComposite = function (data, question) {
        if (data == 'correct') {
            if (question.global_correct_feedback.text != null && question.global_correct_feedback.text != "") {
                question.globalCorrectFeedback = '<span math dynamic>' + question.global_correct_feedback.text + '</span>';
                question.ShowGlobalCorrectFeedback = true;
            }
            else {
                question.ShowGlobalCorrectFeedback = false;
            }
        }
        else if (data == 'incorrect') {
            if (question.global_incorrect_feedback.text != null && question.global_incorrect_feedback.text != "") {
                question.globalInCorrectFeedback = '<span math dynamic>' + question.global_incorrect_feedback.text + '</span>';
                question.ShowGlobalIncorrectFeedback = true;
            }
            else {
                question.ShowGlobalIncorrectFeedback = false;
            }
        }

        //$(question.choices).each(function (opIndex, value) {
        //    if (value.correct_feedback != "" && value.incorrect_feedback != "" && jQuery.isEmptyObject($scope.AssignmentSetting)) {
        //        value.showOptionFeedbackModal = true;
        //        value.showAttempt = true;
        //        $('.fdback').removeClass('ng-hide');
        //    }
        //    else {
        //        value.showOptionFeedbackModal = false;
        //        value.showAttempt = false;
        //        $('.fdback').addClass('ng-hide');
        //    }
        //});
    };

    $scope.ShowGlobalFeebackData = function (data) {
        if ($scope.isLastquestion && jQuery.isEmptyObject($scope.AssignmentSetting)) {
            $scope.ShowGlobalFeedback = true;
        }
        if (data == 'correct') {
            if ($scope.CurrentQuestion.QuestionData.global_correct_feedback.text != null && $scope.CurrentQuestion.QuestionData.global_correct_feedback.text != "") {
                $scope.globalCorrectFeedback = '<span math dynamic>' + $scope.CurrentQuestion.QuestionData.global_correct_feedback.text + '</span>';
                $scope.ShowGlobalCorrectFeedback = true;
                //$scope.ShowGlobalIncorrectFeedback = false;
            }
            else {
                $scope.ShowGlobalCorrectFeedback = false;
            }
        }
        else if (data == 'incorrect') {
            if ($scope.CurrentQuestion.QuestionData.global_incorrect_feedback.text != null && $scope.CurrentQuestion.QuestionData.global_incorrect_feedback.text != "") {
                $scope.globalInCorrectFeedback = '<span math dynamic>' + $scope.CurrentQuestion.QuestionData.global_incorrect_feedback.text + '</span>';
                $scope.ShowGlobalIncorrectFeedback = true;
                //$scope.ShowGlobalCorrectFeedback = false;
            }
            else {
                $scope.ShowGlobalIncorrectFeedback = false;
            }
        }
    };

    $scope.showHintData = function () {
        $scope.showHint = true;
        $scope.hintCount = $scope.hintCount - 1;
        $scope.UpdateUserAssessmentActivity($scope.UserAssessmentActivityDetail.HintsUsed, Enum.AssessmentActivityType.ShowHint);
        $scope.visibleHints.push('<span math dynamic>' + $scope.CurrentQuestion.QuestionData.hints[$scope.UserAssessmentActivityDetail.HintsUsed - 1].text + '</span>');
    };

    $scope.BookMarkQuestion = function () {
        if ($scope.BookMarked) {
            $scope.BookMarked = false;
        }
        else {
            $scope.BookMarked = true;
        }
        $scope.UpdateUserAssessmentActivity($scope.BookMarked, Enum.AssessmentActivityType.BookMark);
    };

    $scope.NotLearnedQuestion = function () {
        if ($scope.NotLearned) { return false; }
        $scope.NotLearned = true;
        $scope.disableNextBtn = false;
        $scope.UpdateUserAssessmentActivity($scope.NotLearned, Enum.AssessmentActivityType.NotLearned);
    };

    $scope.questionQuizPreviewCacncel = function () {
        angular.element('#quizContainer').hide();
    }

    $scope.UpdateUserAssessmentActivity = function (updateValue, activityType) {
        switch (activityType) {
            case Enum.AssessmentActivityType.BookMark:
                $scope.UserAssessmentActivityDetail.IsBookMarked = updateValue;
                break;
            case Enum.AssessmentActivityType.ShowHint:
                $scope.UserAssessmentActivityDetail.HintsUsed = $scope.UserAssessmentActivityDetail.HintsUsed + 1;
                break;
            case Enum.AssessmentActivityType.NotLearned:
                $scope.UserAssessmentActivityDetail.NotLearned = updateValue;
                break;
            case Enum.AssessmentActivityType.NoOfTry:
                $scope.UserAssessmentActivityDetail.NoOfRetry = $scope.UserAssessmentActivityDetail.NoOfRetry + 1;
                break;
            case Enum.AssessmentActivityType.ReportAProblem:
                $scope.UserAssessmentActivityDetail.ReportAProblem = updateValue;
                break;
            default:
                break;
        }
        $scope.UserAssessmentActivityDetail.QuizQuestID = $scope.CurrentQuestion.QuestionId;
        $scope.UserAssessmentActivityDetail.UserAssessmentActivityType = activityType;

        $scope.SetQuestionMapSetting(updateValue, activityType);
    };

    $scope.SetQuestionMapSetting = function (updateValue, activityType) {
        angular.forEach($scope.AssignmentQuestions, function (question, key) {
            if ($scope.CurrentQuestion.QuestionId == question.QuizQuestID) {
                switch (activityType) {
                    case Enum.AssessmentActivityType.BookMark:
                        question.IsBookMarked = updateValue;
                        if (question.QuestionStatus == 'not-visited')
                            question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.ShowHint:
                        question.HintsUsed = $scope.UserAssessmentActivityDetail.HintsUsed + 1;
                        if (question.QuestionStatus == 'not-visited')
                            question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.NotLearned:
                        question.NotLearned = updateValue;
                        if (question.QuestionStatus == 'not-visited')
                            question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.NoOfTry:
                        question.NoOfRetry = $scope.UserAssessmentActivityDetail.NoOfRetry + 1;
                        if (question.QuestionStatus == 'not-visited')
                            question.QuestionStatus = 'answer-visited';
                        break;
                    case Enum.AssessmentActivityType.SubmissionStatus:
                        question.QuestionAttempted = true;
                        question.QuestionStatus = 'answer-right';
                        break;
                    default:
                        break;
                }
                return;
            }
        });
    };

    $scope.ShowFilterQuestions = function (filterType) {
        switch (filterType) {
            case Enum.QuestionFilterType.NotVisited:
                $scope.searchFilter = { QuestionStatus: 'not-visited' };
                break;
            case Enum.QuestionFilterType.VisitedNotAnswered:
                $scope.searchFilter = { QuestionStatus: 'answer-visited' };
                break;
            case Enum.QuestionFilterType.Completed:
                $scope.searchFilter = { QuestionStatus: 'answer-right' };
                break;
            case Enum.QuestionFilterType.IsBookMarked:
                $scope.searchFilter = { IsBookMarked: true };
                break;
            case Enum.QuestionFilterType.ViewAll:
                if ($scope.searchFilter.QuestionStatus != "") {
                    $scope.searchFilter = { QuestionStatus: '' };
                    $('#toggleTest').trigger('click');
                }
                break;
            case Enum.QuestionFilterType.ViewAttended:
                if ($scope.searchFilter.QuestionStatus != "not-visited") {
                    $scope.searchFilter = { QuestionStatus: 'not-visited' };
                    $('#toggleTest').trigger('click');
                }
                break;
            default:
                $scope.searchFilter = { QuestionStatus: '' };
                break;
        }

        if (filterType == Enum.QuestionFilterType.IsBookMarked) {
            $scope.QuestionMapBookMarkClicked = true;
        }
        else {
            $scope.QuestionMapBookMarkClicked = false;
        }
    };

    $(window).resize(function () {
        if ($(window).width() <= 767)
            $('.popover').hide();
        else
            $('.popover').show(); resize_fun();
    });

    var popovermodel;
    $scope.GetFeedback = function (model) {
        var feedbackText = '';
        var currentClass = '';
        popovermodel = '';
        var questTypeId = $scope.questionTypeId;
        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            questTypeId = $scope.CurrentQuestion.QuestionData.questions[model.QuestionIndex].question_type.text;
        }
        $('body').popover("destroy");
        popovermodel = model;
        if (questTypeId == Enum.QuestionType.TextSelection || questTypeId == Enum.QuestionType.Matching || questTypeId == Enum.QuestionType.LTDDND || questTypeId == Enum.QuestionType.LTD || questTypeId == Enum.QuestionType.FIBDND) {
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {

                    if (questTypeId == Enum.QuestionType.Matching && popovermodel.interaction.indexOf("container") > -1) {
                        if (popovermodel.AnswerClass.indexOf("incorrect") > -1) {
                            feedbackText = popovermodel.incorrect_feedback;
                        }
                        else {
                            feedbackText = popovermodel.correct_feedback;
                        }
                    }
                    else {
                        if (popovermodel.AnswerClass.indexOf("incorrect") > -1) {
                            feedbackText = popovermodel.incorrect_feedback;
                        }
                        else {
                            feedbackText = popovermodel.correct_feedback;
                        }
                    }
                    if ($('.fdback-mobile').is(':visible')) {
                        if (questTypeId == Enum.QuestionType.LTDDND || questTypeId == Enum.QuestionType.Matching) {
                            if ($scope.questionTypeId == Enum.QuestionType.Composite) {
                                $('#divltd' + model.Index + model.key).html('<span>' + (model.key + 1) + ". " + feedbackText + '</span>');
                                $('#divltd' + model.Index + model.key).show();
                            }
                            else {
                                $('#divltd' + model.key).html('<span>' + (model.key + 1) + ". " + feedbackText + '</span>');
                                $('#divltd' + model.key).show();
                            }
                        }
                        else {
                            if ($scope.questionTypeId == Enum.QuestionType.Composite) {
                                $('#divltd' + model.Index + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                                $('#divltd' + model.Index + model.choiceid).show();
                            }
                            else {
                                $('#divltd' + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                                $('#divltd' + model.choiceid).show();
                            }

                        }
                    }
                    else {
                        return feedbackText;
                    }
                },//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                placement: "top"
            });
        }
        else if (questTypeId == Enum.QuestionType.ChoiceMatrix) {
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {
                    if (popovermodel.suboption[popovermodel.suboptionSelected - 1].currentClass.indexOf("incorrect") > -1) {
                        feedbackText = popovermodel.incorrect_feedback;
                    }
                    else {
                        feedbackText = popovermodel.correct_feedback;
                    }
                    return feedbackText;
                },
                placement: "top"
            });
        }
        else if (questTypeId == Enum.QuestionType.MCSS || questTypeId == Enum.QuestionType.MCMS || questTypeId == Enum.QuestionType.Order || questTypeId == Enum.QuestionType.HotSpot) {
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {

                    if (popovermodel.currentClass.indexOf("incorrect") > -1) {
                        feedbackText = popovermodel.incorrect_feedback;
                    }
                    else {
                        feedbackText = popovermodel.correct_feedback;
                    }
                    if ($('.fdback-mobile').is(':visible')) {
                        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
                            $('#divltd' + model.Index + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                            $('#divltd' + model.Index + model.choiceid).show();
                        }
                        else {
                            $('#divltd' + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                            $('#divltd' + model.choiceid).show();
                        }
                    }
                    else {
                        return feedbackText;
                    }
                },//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                placement: "top"
            });

        }

    }


    $scope.showHideOptionFeedbackIcon = function () {
        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            $.each($scope.CurrentQuestion.QuestionData.questions, function (index, question) {
                var selectedOptions = angular.fromJson($scope.FindUserSelectedOptions(question));
                $.each(question.choices, function (index, value) {
                    $(selectedOptions).each(function (sindex, soption) {
                        if (soption["choiceid"] != undefined && soption["choiceid"] != "") {
                            if (value.choiceid == soption["choiceid"]) {
                                $scope.showHideOptionFeedbackIconInternal(value, question);
                            }
                        } else if (soption["inputvalue"] != undefined && soption["inputvalue"] != "") {
                            if (value.choiceid == soption["inputvalue"].split('-')[0]) {
                                $scope.showHideOptionFeedbackIconInternal(value, question);
                            }
                        }
                    });
                });
            });
        } else if ($scope.questionTypeId != Enum.QuestionType.Composite) {
            var selectedOptions = angular.fromJson($scope.userSelectedOptions());
            $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
                $(selectedOptions).each(function (sindex, soption) {
                    //if (value != null && value != "") {
                    if (soption["choiceid"] != undefined && soption["choiceid"] != "") {
                        if (value.choiceid == soption["choiceid"]) {
                            $scope.showHideOptionFeedbackIconInternal(value, $scope.CurrentQuestion.QuestionData);
                        }
                    } else if (soption["inputvalue"] != undefined && soption["inputvalue"] != "") {
                        if (value.choiceid == soption["inputvalue"].split('-')[0]) {
                            $scope.showHideOptionFeedbackIconInternal(value, $scope.CurrentQuestion.QuestionData);
                        }
                    }
                });
            });
        }
    };

    $scope.showHideOptionFeedbackIconInternal = function (value, question) {
        if (!$scope.questionTypeId == Enum.QuestionType.Composite) {
            if ((question.question_type.text == Enum.QuestionType.HotSpot || question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND || question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND) &&
                value.correct_feedback != "" && value.incorrect_feedback != "") {
                value.showOptionFeedbackModal = true;
                value.showAttempt = true;
                $('.fdback').removeClass('ng-hide');
            }
            else if (value.correct_feedback != "" && value.incorrect_feedback != "") {
                value.showOptionFeedbackModal = true;
                value.showAttempt = true;
                $('.fdback').removeClass('ng-hide');
            }
            else {
                value.showOptionFeedbackModal = false;
                value.showAttempt = false;
                $('.fdback').addClass('ng-hide');
            }
        }

            //else if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            //    value.showOptionFeedbackModal = true;
            //    if (value.correct_feedback != "" && value.incorrect_feedback != "") {
            //        value.displayfeedback = true;
            //    }
            //    else
            //        value.displayfeedback = true;
            //    $('.fdback').removeClass('ng-hide');
            //}


        else if (!$scope.showOptionFeedbackModal) {
            if (($scope.questionTypeId == Enum.QuestionType.TextSelection || $scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND || $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND) &&
                value.correct_feedback != "" && value.incorrect_feedback != "" && $scope.isLastquestion && jQuery.isEmptyObject($scope.AssignmentSetting)) {
                $scope.showOptionFeedbackModal = true;
                $('.fdback').removeClass('ng-hide');
            }
            else if (value.correct_feedback != "" && value.incorrect_feedback != "" && $scope.isLastquestion && jQuery.isEmptyObject($scope.AssignmentSetting)) {
                $scope.showOptionFeedbackModal = true;
                $('.fdback').removeClass('ng-hide');
            }

            else {
                $scope.showOptionFeedbackModal = false;
                $('.fdback').addClass('ng-hide');
            }

        }
    };

    $scope.showCorrectAnswer = function () {
        popovermodel = '';
        $scope.CheckAnswer = true;
        $scope.ShowGlobalCorrectFeedback = false;
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.selectedItems = [];
        if ($scope.operationmode == Enum.OperationMode.Offline) {
            if ($scope.userSelectedOptions() != "" && $scope.userSelectedOptions() != "[]") {
                $scope.selectedItems = $scope.userSelectedOptions();
                if ($('#checkAnswerbtn').html() == 'Check Answer') {
                    $('input').attr("disabled", true)
                    $('select').attr("disabled", true)
                    $('#checkAnswerbtn').html('Try Again');
                    $('#checkAnswerbtnmobile').html('Try Again');
                    if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
                        $scope.UpdateUserAssessmentActivity($scope.UserAssessmentActivityDetail.NoOfRetry, Enum.AssessmentActivityType.NoOfTry);
                        $scope.showAttempt = true;
                        if ($scope.questionTypeId == Enum.QuestionType.TextSelection || $scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND && $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND) {
                            $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                                value.showAttempt = true;
                            });
                        }
                        $scope.CorrectAnswerCount = $scope.CorrectAnswerCount + 1;
                        //if ($scope.QuestionAttempt > 1) $('#checkAnswerbtn').html('Try Again');
                        if ($scope.QuestionAttempt == $scope.CorrectAnswerCount) {
                            $scope.DisableCorrectAnswer = true;
                        }
                    }
                    $scope.SingledroppableConfig.disabled = true;
                    $scope.SingledraggableConfig.disabled = true;
                    $scope.SortableFuncConfig.disabled = true;
                }
                else {
                    //Added by Khadir
                    $('input').removeAttr("disabled")
                    $('select').removeAttr("disabled")
                    $scope.SingledroppableConfig.disabled = false;
                    $scope.SingledraggableConfig.disabled = false;
                    $scope.SortableFuncConfig.disabled = false;
                    $scope.userHasSelectedOptions = false;
                    $(".alert").addClass('ng-hide');
                    $('#checkAnswerbtn').html('Check Answer');
                    $('#checkAnswerbtnmobile').html('Check Answer');
                    $scope.CheckAnswer = false;
                    if ($scope.questionTypeId == Enum.QuestionType.Composite) {
                        //$scope.ResetCompositeQuestions($scope.CurrentQuestion.QuestionData.questions);
                        $scope.$broadcast('resetCompositeQuestion');
                        $scope.$apply();
                    }
                    else {
                        $(".option").removeClass('correct incorrect selected');
                        $(".hotspot").removeClass('correct incorrect selected');

                        //$(".option").removeClass('incorrect');
                        //$(".option").removeClass('selected');
                        $(".option input:checked").prop('checked', false);
                        $scope.CurrentQuestion.SelectedOptions = [];
                        $scope.selectedoptions = [];
                        //$scope.UserSelectedArea = [];
                        $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                            if ($scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND || $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.Matching) {
                                this.isAnswered = false;
                                this.AnswerClass = "";
                                if (!!value.UserInput) value.UserInput = "";
                                if (!!value.Canvas && value.Canvas.length > 0) {
                                    $(value.Canvas).each(function (cindex, cvalue) {
                                        cvalue.AnswerClass = "";
                                        cvalue.selectedClass = "";
                                        $scope.droppableItems.push(cvalue);
                                    });
                                    value.Canvas = [];
                                }
                                if ($scope.questionTypeId == Enum.QuestionType.FIBDND) {
                                    var selectedValue = $scope.SelectedTemplateArray.filter(function (x) { return x.dropId == index; });
                                    if (selectedValue.length > 0) {
                                        $(selectedValue).each(function (sindex, svalue) {
                                            if (svalue != "") {
                                                if ($scope.droppableItems.filter(function (x) { return $filter('lowercase')(x.title) == $filter('lowercase')('<span math dynamic>' + svalue.title + '</span>'); }).length == 0) {
                                                    $scope.droppableItems.push({ 'title': svalue.title, 'drag': true, selectedClass: '' });
                                                }
                                                svalue.dropId = "initial";
                                            }
                                        });
                                    }
                                }
                            }
                            else if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
                                value.isChoiceAnswered = false;
                                $(value.suboption).each(function (sindex, svalue) {
                                    svalue.currentClass = "";
                                    svalue.isAnswered = "";
                                    svalue.isOptionAnswered = false;
                                    value.suboptionSelected = "";
                                });
                            }
                            else if ($scope.questionTypeId == Enum.QuestionType.TextSelection) {
                                this.AnswerClass = "";
                                $scope.NoOfSelection = 0;
                                $('#divltd' + value.choiceid).html('');
                                $('#divltd' + value.choiceid).hide();
                            }
                            else if ($scope.questionTypeId == Enum.QuestionType.HotSpot || $scope.questionTypeId == Enum.QuestionType.Order) {
                                $scope.UserSelectedArea = [];
                                //$('#mediaImage').mapster("set_options", { fillOpacity: 0.4, fillColor: "d42e16", stroke: true, strokeColor: "3320FF", strokeOpacity: 0.8, strokeWidth: 4, clickNavigate: true, highlight: true, isSelectable: true, singleSelect: false, mapKey: 'data-state' });
                                $('#divltd' + value.choiceid).html('');
                                $('#divltd' + value.choiceid).hide();
                                this.currentClass = "";
                                $scope.NoOfSelection = 0;
                                value.isAnswered = false;
                            }
                            else {
                                this.isAnswered = false;
                                this.currentClass = "";
                            }
                            if ($scope.questionTypeId == Enum.QuestionType.FIBDND) {
                                $("div[data-drop-id='" + index + "']").empty();
                                value.accept = true;
                            }
                            if ($scope.questionTypeId == Enum.QuestionType.Matching || $scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.TextSelection || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.LTDDND) {
                                $('#divltd' + value.key).html('');
                                $('#divltd' + value.key).hide();
                                this.currentClass = "";
                                value.isAnswered = false;
                            }
                        });
                        if ($scope.questionTypeId == Enum.QuestionType.FIBDND) {
                            $scope.droppableItems = [];
                            angular.forEach($scope.SelectedTemplateArray, function (option, key) {
                                $scope.droppableItems.push({ 'title': option.title, 'drag': true, selectedClass: '' });
                            });
                        }

                        if ($scope.questionTypeId == Enum.QuestionType.Order) {
                            $scope.CurrentQuestion.QuestionData.choices.sort(function (a, b) {
                                if (a.CurrentOptionIndex < b.CurrentOptionIndex) return -1;
                                if (a.CurrentOptionIndex > b.CurrentOptionIndex) return 1;
                                return 0;
                            })
                        }

                        //$scope.showHint = false;
                        //$scope.UserAssessmentActivityDetail.HintsUsed = 0;
                        //$scope.hintCount = $scope.CurrentQuestion.QuestionData.hints.length;
                        //$scope.visibleHints = [];

                        //angular.forEach($scope.AssignmentQuestions, function (question, key) {
                        //    if ($scope.CurrentQuestion.QuestionId == question.QuizQuestID) {
                        //        question.HintsUsed = 0;
                        //    }
                        //});
                        $('span[id^="spnltd"]').each(function () {
                            $('#' + this.id).popover("destroy");
                        });

                        return;
                    }
                }
            }
            if ($scope.questionTypeId == Enum.QuestionType.Composite) {
                $scope.CheckCompositeAnswers($scope.CurrentQuestion.QuestionData.questions);
                //return;
            }
            $scope.showHideOptionFeedbackIcon();
            if ($scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND || $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.Matching) {
                $scope.checkCorrectTextEntered();
               // $scope.userHasSelectedOptions = true;
            }
            else if ($scope.questionTypeId == Enum.QuestionType.HotSpot) {
                var correctoptions = $scope.CurrentQuestion.QuestionData.correct_answer.split(",");
                $scope.selectedoptions = angular.fromJson($scope.selectedItems);
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                    var answerFound = false;

                    if (this.isAnswered == true) {
                        $(correctoptions).each(function (index, cvalue) {
                            if (cvalue == value.choiceid) {
                                answerFound = true;
                            }
                        });

                        if (answerFound) {
                            value.isCorrect = true;
                            value.currentClass = "correct";
                            if (value.correct_feedback != '') {
                                value.displayfeedback = true;
                            }
                        }
                        else {
                            value.isCorrect = false;
                            value.currentClass = "incorrect";
                            $scope.ShowGlobalIncorrectFeedback = true;
                            if (value.incorrect_feedback != '') {
                                value.displayfeedback = true;
                            }
                        }
                    }
                    else {
                        value.currentClass = "";
                    }
                });
                if ($scope.ShowGlobalIncorrectFeedback) {
                    $scope.ShowGlobalFeebackData("incorrect");
                }
                else {
                    $scope.ShowGlobalFeebackData("correct");
                }
            }
            else if ($scope.questionTypeId == Enum.QuestionType.Order) {
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                    var correctoptions = $scope.CurrentQuestion.QuestionData.correct_answer.split(",");
                    var optionindex = value.choiceid;
                    if (parseInt(optionindex) == correctoptions[index]) {
                        value.isCorrect = true;
                        value.currentClass = "correct";
                        if (value.correct_feedback != '') {
                            value.displayfeedback = true;
                        }
                        // $scope.ShowGlobalFeebackData(value.currentClass);
                    }
                    else {
                        value.isCorrect = false;
                        value.currentClass = "incorrect";
                        if (value.incorrect_feedback != '') {
                            value.displayfeedback = true;
                        }
                        $scope.ShowGlobalIncorrectFeedback = true;
                        //$scope.ShowGlobalFeebackData(value.currentClass);
                    }
                });

                if ($scope.ShowGlobalIncorrectFeedback) {
                    $scope.ShowGlobalFeebackData("incorrect");
                }
                else {
                    $scope.ShowGlobalFeebackData("correct");
                }
            }
            else if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
                $scope.selectedoptions = angular.fromJson($scope.selectedItems);

                var currentAccess;
                if (parseInt($scope.selectedOption) == 1) {
                    currentAccess = true;
                }
                else {
                    currentAccess = false;
                }

                $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                    var optionindex = index + 1;
                    $(value.suboption).each(function (sindex, soption) {
                        if (value.suboptionSelected == soption.identifier) {
                            if (value.isAnswered.toString() == soption.isAnswered.toString()) {
                                $scope.CurrentQuestion.QuestionData.choices[index].suboption[sindex].currentClass = "correct";
                                if (value.correct_feedback != '') {
                                    value.displayfeedback = true;
                                }
                            } else {
                                $scope.CurrentQuestion.QuestionData.choices[index].suboption[sindex].currentClass = "incorrect";
                                if (value.incorrect_feedback != '') {
                                    value.displayfeedback = true;
                                }
                                $scope.ShowGlobalIncorrectFeedback = true;
                            }
                        }

                    });
                });

                if ($scope.selectedoptions.length > 0) {
                    if (!$scope.ShowGlobalIncorrectFeedback && ($scope.selectedoptions.length == $scope.CurrentQuestion.QuestionData.choices.length)) {
                        $scope.ShowGlobalFeebackData("correct");
                    }
                    else {
                        $scope.ShowGlobalFeebackData("incorrect");
                    }
                }
            }
            else if ($scope.questionTypeId == Enum.QuestionType.TextSelection) {
                $scope.CheckTextSelectionTypeAnswer($scope.CurrentQuestion);
                if ($scope.ShowGlobalIncorrectFeedback) {
                    $scope.ShowGlobalFeebackData("incorrect");
                }
                else {
                    $scope.ShowGlobalFeebackData("correct");
                }
            }
            else {
                if ($scope.questionTypeId != Enum.QuestionType.Composite) {
                    $scope.correctoptions = $scope.CurrentQuestion.QuestionData.correct_answer.split(",");
                    $scope.selectedoptions = angular.fromJson($scope.selectedItems).map(function (x) { return x.choiceid.toString(); });

                    $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                        var optionindex = index + 1;
                        var answerFound = false;

                        if (this.isAnswered == true) {
                            //if (value.assess == true) {

                            $($scope.correctoptions).each(function (correctindex) {
                                if (parseInt(value.choiceid) == parseInt($scope.correctoptions[correctindex])) {
                                    answerFound = true;
                                }
                            });
                            if (answerFound) {
                                value.isCorrect = true;
                                value.currentClass = "correct";
                                if (value.correct_feedback != '') {
                                    value.displayfeedback = true;
                                }
                                //$scope.ShowGlobalFeebackData(value.currentClass);
                            }
                            else {
                                value.isCorrect = false;
                                value.currentClass = "incorrect";
                                if (value.incorrect_feedback != '') {
                                    value.displayfeedback = true;
                                }
                                // $scope.ShowGlobalFeebackData(value.currentClass);
                                // $scope.ShowGlobalIncorrectFeedback = true;
                            }
                        }
                        else {
                            value.currentClass = "";
                        }

                    });
                    if ($scope.selectedoptions.length > 0) {
                        var globalCorrectFeedback = checkEqualArray($scope.selectedoptions, $scope.correctoptions);
                        if (globalCorrectFeedback) {
                            $scope.ShowGlobalFeebackData("correct");
                        }
                        else {
                            $scope.ShowGlobalFeebackData("incorrect");
                        }
                    }
                }

            }

        }
    };

    $scope.CheckTextSelectionTypeAnswer = function (question) {
        $scope.CorrectAnswer = question.QuestionData.correct_answer.split(',');
        var count = 0;
        $(question.QuestionData.choices).each(function (key, choice) {
            var choiceFound = false;

            if (choice.AnswerClass.indexOf("selectedC") != -1) {
                count++;
                $($scope.CorrectAnswer).each(function (index, anserid) {
                    if (choice.choiceid == anserid) {
                        choiceFound = true;
                    }
                });
                if (choiceFound) {
                    choice.AnswerClass = "selectedC correct";
                    if (choice.correct_feedback != '') {
                        choice.displayfeedback = true;
                    }

                }
                else {
                    choice.AnswerClass = "selectedC incorrect";
                    if (choice.incorrect_feedback != '') {
                        choice.displayfeedback = true;
                    }
                    $scope.ShowGlobalIncorrectFeedback = true;
                }
            }

        });
        // show global feebbackCorrect only when all correct is options are selected
        if ($scope.CorrectAnswer.length == count && $scope.ShowGlobalIncorrectFeedback != true)
            $scope.ShowGlobalIncorrectFeedback = false;
        else
        { $scope.ShowGlobalIncorrectFeedback = true; }
    };
    //************************************Composite question code starts****************************/
    //*****************Reset Try Again *******************//
    $scope.ResetCompositeQuestions = function (questions) {
        $.each(questions, function (index, question) {

            question.globalCorrectFeedback = "";
            question.ShowGlobalCorrectFeedback = false;
            question.globalInCorrectFeedback = "";
            question.ShowGlobalIncorrectFeedback = false;

            $('.fdback').addClass('ng-hide');

            $(question.choices).each(function (opIndex, value) {
                value.showOptionFeedbackModal = false;
                value.showAttempt = false;

                if (question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
                    this.isAnswered = false;
                    this.AnswerClass = "";
                    if (!!value.UserInput) value.UserInput = "";
                    if (!!value.Canvas && value.Canvas.length > 0) {
                        $(value.Canvas).each(function (cindex, cvalue) {
                            cvalue.AnswerClass = "";
                            cvalue.selectedClass = "";
                            question.droppableItems.push(cvalue);
                        });
                        value.Canvas = [];
                    }
                }
                else if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
                    value.isChoiceAnswered = false;
                    $(value.suboption).each(function (sindex, svalue) {
                        svalue.currentClass = "";
                        svalue.isAnswered = "";
                        svalue.isOptionAnswered = false;
                        value.suboptionSelected = "";
                    });
                } else if (question.question_type.text == Enum.QuestionType.Order) {
                    value.isCorrect = false;
                    value.currentClass = "";
                } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
                    value.AnswerClass = "";

                } else {
                    this.isAnswered = false;
                    this.currentClass = "";
                }
            });
            if (question.question_type.text == Enum.QuestionType.Order) {
                question.choices.sort(function (a, b) {
                    if (a.CurrentOptionIndex < b.CurrentOptionIndex) return -1;
                    if (a.CurrentOptionIndex > b.CurrentOptionIndex) return 1;
                    return 0;
                })
                $(".odering-panel").removeClass("incorrect correct");
            } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
                question.NoOfSelection = 0;
            }
        });
    };
    //****************End of Try again *******************//

    //****************************************Composite question Feedback***********************
    $scope.SetCompositeType = function (compositeQuestion, compositeIndex) {
        $.each(compositeQuestion.QuestionData.questions, function (qindex, question) {
            question.Index = qindex;
            var selectedOptions = '';
            if (compositeQuestion.SelectedOptions != null && !$scope.isUndefined(compositeQuestion.SelectedOptions)) {
                selectedOptions = compositeQuestion.SelectedOptions[qindex].UserSelectedOptions;
            }

            if (!$scope.isUndefined(question.stem_details) && question.stem_details.type == 'algorithimic') {
                $scope.SetCompositeFeedbackAlgorithimicType(question);
            }

            if (question.question_type.text == Enum.QuestionType.MCSS || question.question_type.text == Enum.QuestionType.MCMS) {
                $scope.SetFeedbackForCompositeMCSSMCMS(question, selectedOptions);
            }
            else if (question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
                $scope.SetFeedbackForCompositeFIBDNDMatching(question, selectedOptions);
            }
            else if (question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND) {
                $scope.SetFeedbackForCompositeLTDDND(question, selectedOptions);
            }
            else if (question.question_type.text == Enum.QuestionType.Order) {
                $scope.SetFeedbackForCompositeOrder(question, selectedOptions);
            }
            else if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
                $scope.SetFeedbackForCompositeChoiceMatrix(question, selectedOptions);
            }
                //ACECBE-1029
            else if (question.question_type.text == Enum.QuestionType.HotSpot) {
                $scope.SetCompositeFeedbackHotSpot(question, selectedOptions);
            }

        });
    }

    $scope.SetCompositeFeedbackHotSpot = function (question, selectedOptions) {
        if (!$scope.isUndefined(question.question_media.media) && question.question_media.media.length > 0 && question.question_media.media != "") {
            //$scope.FeedBack[index].ImageSrc = regex.exec($scope.FeedBack[index].QuestionData.question_media.media)[1];
            var imgObject = document.createElement('div');
            imgObject.innerHTML = question.question_media.media;
            question.ImageSrc = $(imgObject).find('img').attr('src');
        }

        angular.forEach(question.choices, function (option, key) {
            option.XPosition = '';
            option.YPosition = '';
            option.isAnswered = false;
            // $scope.QuestionOptions[key].opacity = 0;

            var positions = option.text.split(',');
            if (positions.length == 2) {
                option.XPosition = positions[0];
                option.YPosition = positions[1];
            }
        });

        if (selectedOptions) {
            var correctoptions = question.correct_answer.split(",");
            var selectedoptions = $.parseJSON(selectedOptions);
            question.SelectedOptions = selectedoptions.map(function (x) { return x.choiceid; }).toString();
            //var abc = selectedoptions.map(function (x) { return x.choiceid; }).toString();



            $(question.choices).each(function (cindex, value) {
                $(selectedoptions).each(function (selectindex, svalue) {
                    if (value.choiceid == parseInt(svalue.choiceid)) {
                        if (correctoptions.indexOf(value.choiceid.toString()) != -1) {
                            value.isCorrect = true;
                            value.currentClass = "correct";
                        }
                        else {
                            value.isCorrect = false;
                            value.currentClass = "incorrect";
                        }
                    }
                });

            });
        }
    }

    $scope.SetCompositeFeedbackAlgorithimicType = function (question) {
        if (!$scope.isUndefined(question.AlgorithmicVariableArray) && question.stem_details.textforeachvalue.length > 0) {
            angular.forEach(question.stem_details.textforeachvalue, function (textDetail, key) {
                angular.forEach(question.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    question.question_stem.text = question.question_stem.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                });
            });
        }
    }

    $scope.SetFeedbackForCompositeMCSSMCMS = function (question, selectedOptions) {
        if (!$scope.isUndefined(question.exhibit) && question.exhibit.length > 0 && question.exhibit[0].exhibit_type != "") {
            question.showExhibit = true;
            if (question.exhibit[0].exhibit_type == 'image') {
                var imagePath = String(question.exhibit[0].path);
                question.exhibit[0].path = imagePath;
            }
        }
        else {
            question.showExhibit = false;
        }
        $(question.choices).each(function (indexOpt, valueOpt) {
            var correctoptions = question.correct_answer.split(",");
            //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].CorrectOptions = correctoptions.toString();
            question.CorrectOptions = correctoptions.toString();
            var sSrchTxt = String(valueOpt.text);
            if (sSrchTxt.indexOf('<img') != -1) {
                var options = String(valueOpt.text).split('<img');
                question.choices[indexOpt].text = options[0];
                question.choices[indexOpt].MCSSImage = "<img" + options[1];
                question.choices[indexOpt].text = options[0];
                question.choices[indexOpt].MCSSImage = "<img" + options[1];
            }
            else {
                //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].choices[indexOpt].text = sSrchTxt;
                //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].choices[indexOpt].MCSSImage = null;
                question.choices[indexOpt].text = sSrchTxt;
                question.choices[indexOpt].MCSSImage = null;
            }

            if (this.isAnswered == true) {
                $(correctoptions).each(function (correctindex) {
                    if (parseInt(valueOpt.choiceid) != parseInt(correctoptions[correctindex])) {
                        valueOpt.isCorrect = false;
                        valueOpt.currentClass = "incorrect";
                        if (valueOpt.incorrect_feedback != '') {
                            valueOpt.displayfeedback = true;
                        }
                    }
                });
            }

            $(correctoptions).each(function (correctindex) {
                if (parseInt(valueOpt.choiceid) == parseInt(correctoptions[correctindex])) {
                    valueOpt.isCorrect = true;
                    valueOpt.currentClass = "correct";
                    if (valueOpt.correct_feedback != '') {
                        valueOpt.displayfeedback = true;
                    }

                }
            });
        });
    }

    $scope.SetFeedbackForCompositeOrder = function (question, selectedOptions) {
        $(question.choices).each(function (indexOpt, valueOpt) {
            var correctoptions = question.correct_answer.split(",");
            var optionindex = valueOpt.choiceid;
            if (parseInt(optionindex) == correctoptions[indexOpt]) {
                valueOpt.isCorrect = true;
            }
            else {
                valueOpt.isCorrect = false;
            }
            if (selectedOptions) {
                var userSelected = $.parseJSON(selectedOptions);
                //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].SelectedOptions = userSelected[0].selectedOrder;
                question.SelectedOptions = userSelected[0].selectedOrder;
            }
            //if (question.question_interaction.text == "media" && question.question_layout != null)
            //    $scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].LayoutType = question.question_layout.text;
        });
    }

    $scope.SetFeedbackForCompositeChoiceMatrix = function (question, selectedOptions) {
        $(question.choices).each(function (indexOpt, valueOpt) {
            if (selectedOptions) {
                var selectedOpt = angular.fromJson(selectedOptions);
                $(selectedOpt).each(function (selectindex, selectvalue) {
                    var inputValues = selectvalue.inputvalue.split("-");
                    var optionindex = indexOpt + 1;
                    $(valueOpt.suboption).each(function (index1, value1) {
                        if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                            valueOpt.suboption[index1].isAnswered = true;
                            var assess;
                            if (parseInt(inputValues[1]) == 1) {
                                assess = true;
                            }
                            else {
                                assess = false;
                            }
                            if (valueOpt.assess === assess) {
                                valueOpt.suboption[index1].isCorrect = true;
                            }
                            else {
                                valueOpt.suboption[index1].isCorrect = false;
                            }
                        }
                        else {
                            var val = (value1.value === "true");
                            if (valueOpt.assess === val) {
                                valueOpt.suboption[index1].isCorrect = true;
                            }
                        }
                    });
                });

            }

        });
    }

    $scope.SetFeedbackForCompositeFIBDNDMatching = function (question, selectedOptions) {
        question.leftvertical = "";
        question.rightvertical = "";
        question.draggableItems = [];
        var questiontext = '';
        if (!$scope.isUndefined(question.textwithblanks)) {
            questiontext = question.textwithblanks.text;
        }

        $scope.FillFeedbackCompositeDroppableItems(question);

        angular.forEach(question.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            var htmlText = "";
            var answerClass = "";
            var inputValue = "";

            if (selectedOptions) {
                var selectedOption = angular.fromJson(selectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if (value.noforeachblank == option.noforeachblank && value.inputvalue != "") {
                        inputValue = value.inputvalue;
                        answerClass = (value.isCorrectAnswer) ? "correct" : "incorrect";
                    }
                });
            }
            if (option.interaction == 'textentry') {
                htmlText = "<span class='option " + answerClass + "'><input type='text' class='form-control' disabled value='" + inputValue + "'><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
            }
            else if (option.interaction == 'inline') {
                htmlText = "<span class='option " + answerClass + "'><select class='form-control' disabled><option>" + inputValue + "</option></select><span class='sign'><span class='sr-only'></span><span class='fa'></span></span></span>";
            }
            else if (option.interaction == 'dragdrop' || option.interaction == 'container') {
                question.LayoutType = question.question_layout.text;
            }

            if (option.interaction == 'dragdrop') {
                angular.forEach(question.draggableItems, function (ditem, dindex) {
                    if ($filter('lowercase')(inputValue) == $filter('lowercase')(ditem.title)) {
                        ditem.title = "__________";
                    }
                });
            }
            else if (option.interaction == 'container') {
                var input = !!inputValue ? inputValue.map(function (x) { return x.title; }) : [];
                angular.forEach(question.draggableItems, function (ditem, dindex) {
                    if (input.indexOf(ditem.title) != -1) {
                        ditem.title = "__________";
                    }
                });

                var isOptionIncorrect = false;
                var isOptionAnswered = false;
                if (option.Canvas != undefined && option.Canvas != null) {
                    for (var i = 0; i < option.Canvas.length; i++) {
                        isOptionAnswered = true;
                        if (option.Canvas[i].AnswerClass == "incorrect") {
                            isOptionIncorrect = true;
                        }
                    }
                }
                if (isOptionAnswered) {
                    option.AnswerClass = isOptionIncorrect ? "incorrect" : "correct";
                }
            }

            if (question.question_type.text != Enum.QuestionType.LTD && question.question_type.text != Enum.QuestionType.LTDDND && question.question_layout.text != '' && question.question_layout.text != "dragdrop_parallel") {
                htmlText = "<span class='option drop " + answerClass + "'><span class='option drag'>" + inputValue + "</span><span class='sign'><span class='fa'></span></span></span>";
                if (question.question_layout.text === "right" || question.question_layout.text === "left" || question.question_layout.text === "vertical") {
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].leftvertical = "col-md-10";
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].rightvertical = "col-md-2 right";
                    question.leftvertical = "col-md-10";
                    question.rightvertical = "col-md-2 right";
                }
                else if (question.question_layout.text === "row") {
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].leftvertical = "";
                    //$scope.FeedBack[compositeIndex].QuestionData.questions[question.Index].rightvertical = "text-center";
                    question.leftvertical = "";
                    question.rightvertical = "text-center";
                }

                question.choices[key].isCorrectAnswer = answerClass;
                question.choices[key].selectedAnswer = inputValue;
            }
            else {
                question.choices[key].isCorrectAnswer = answerClass;
                question.choices[key].selectedAnswer = inputValue;
            }
            questiontext = questiontext.replace(textToReplace, htmlText);

        });
        question.FIBText = questiontext;
    };

    $scope.SetFeedbackForCompositeLTDDND = function (question, selectedOptions) {

        question.leftvertical = "";
        question.rightvertical = "";
        question.draggableItems = [];

        if (question.question_media != undefined && question.question_media.media.length > 0 && question.question_media.media != "") {
            var imgObject = document.createElement('div');
            imgObject.innerHTML = question.question_media.media;
            question.ImageSrc = $(imgObject).find('img').attr('src');
        }

        $scope.FillFeedbackCompositeDroppableItems(question);

        angular.forEach(question.choices, function (option, key) {
            var inputValue = "";
            if (selectedOptions) {
                var selectedOption = angular.fromJson(selectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if (value.noforeachblank == option.noforeachblank && value.inputvalue != "") {
                        option.LTDText = value.inputvalue;
                        inputValue = value.inputvalue;
                        option.AnswerClass = (value.isCorrectAnswer) ? "correct" : "incorrect";
                    }
                });
            }

            if (option.interaction == 'dragdrop') {
                question.LayoutType = question.question_layout.text;

                if (option.interaction == 'dragdrop') {
                    angular.forEach(question.draggableItems, function (ditem, dindex) {
                        if ($filter('lowercase')(inputValue) == $filter('lowercase')('<span math dynamic>' + ditem.title + '</span>')) {
                            ditem.title = "__________";
                        }
                    });
                }

                if (question.question_layout.text === "right" || question.question_layout.text === "left" || question.question_layout.text === "vertical") {
                    question.leftvertical = "col-md-10";
                    question.rightvertical = "col-md-2 right";
                }
                else if (question.question_layout.text === "row") {
                    question.leftvertical = "";
                    question.rightvertical = "text-center";
                }
            }
        });
    }

    $scope.FillFeedbackCompositeDroppableItems = function (question) {
        var isDistractorsPushDone = false;
        angular.forEach(question.choices, function (option, key) {
            if (isDistractorsPushDone == false) {
                angular.forEach(option.distractorforeachblank, function (titem, tindex) {
                    question.draggableItems.push({ 'title': titem.text, 'drag': false });
                });
                isDistractorsPushDone = true;
            }

            angular.forEach(option.textforeachblank, function (titem, tindex) {
                question.draggableItems.push({ 'title': titem.text, 'drag': false });
            });
        });
    };

    //************************************************** End of Composite Question feedback***************************************

    //*********************************Save Answer for composite type **********************************

    $scope.GetUserSelectedOptionForCompositeType = function (questions) {
        var selectedItemsForComposite = [];
        $(questions).each(function (index, question) {
            selectedItemsForComposite.push({ 'Index': index, 'UserSelectedOptions': $scope.FindUserSelectedOptions(question) });
        });
        return selectedItemsForComposite;
    }

    //*********************************End of save answer for composite type ***************************

    //*********************************Check Answer for Composite type question*************************

    $scope.CheckCompositeAnswers = function (questions) {
        $scope.$broadcast('enableDisableDragDropComposite', true);
        $(questions).each(function (index, question) {
            question.ShowGlobalFeedback = true;
            question.ShowGlobalCorrectFeedback = false;
            question.ShowGlobalIncorrectFeedback = false;
            question.globalCorrectFeedback = '';
            question.globalInCorrectFeedback = '';
            if (question.question_type.text == Enum.QuestionType.MCSS || question.question_type.text == Enum.QuestionType.MCMS) {
                $scope.CheckMCSSAndMCMSAnswer(question);
            }
            else if (question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND || question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
                $scope.CheckDNDAnswers(question);
            }
            else if (question.question_type.text == Enum.QuestionType.Order) {
                $scope.CheckOrderTypeAnswer(question);
            }
            else if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
                $scope.CheckChoiceMatrixTypeAnswer(question);
            } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
                $scope.CheckCompositeTextSelectionTypeAnswer(question);
            } else if (question.question_type.text == Enum.QuestionType.HotSpot) {
                $scope.checkCompositeHotspotAnswer(question);
            }
        });
    };

    $scope.checkCompositeHotspotAnswer = function (question) {

        var selectedItems = $scope.FindUserSelectedOptions(question);
        question.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
        var correctoptions = question.correct_answer.split(",");
        var selectedoptions = angular.fromJson(selectedItems).map(function (x) { return x.choiceid.toString(); });
        var ansCount = 0;
        $(question.choices).each(function (index, value) {
            var answerFound = false;

            if (this.isAnswered == true) {
                ansCount += 1;
                $(correctoptions).each(function (index, cvalue) {
                    if (cvalue == value.choiceid) {
                        answerFound = true;
                    }
                });

                if (answerFound) {
                    value.isCorrect = true;
                    value.currentClass = "correct";
                    value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                    if (value.correct_feedback != '') {
                        value.displayfeedback = true;
                    }
                }
                else {
                    value.isCorrect = false;
                    value.currentClass = "incorrect";
                    value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                    question.ShowGlobalIncorrectFeedback = true;
                    if (value.incorrect_feedback != '') {
                        value.displayfeedback = true;
                    }
                }
            }
            else {
                value.currentClass = "";
            }
        });
        if (question.ShowGlobalIncorrectFeedback) {
            $scope.ShowGlobalFeebackDataForComposite("incorrect", question);
        }
        else if (ansCount > 0) {
            $scope.ShowGlobalFeebackDataForComposite("correct", question);
        }

    }

    $scope.CheckCompositeTextSelectionTypeAnswer = function (question) {
        var selectedItems = $scope.FindUserSelectedOptions(question);
        var selectedoptions = angular.fromJson(selectedItems);
        if (selectedoptions != undefined && selectedoptions.length > 0) {
            $scope.CorrectAnswer = question.correct_answer.split(',');
            var isincorrect = false;
            $(question.choices).each(function (key, choice) {
                var choiceFound = false;
                if (choice.AnswerClass.indexOf("selectedC") != -1) {
                    $($scope.CorrectAnswer).each(function (index, anserid) {
                        if (choice.choiceid == anserid) {
                            choiceFound = true;
                        }
                    });
                    if (choiceFound) {
                        choice.AnswerClass = "selectedC correct";
                        if (choice.correct_feedback != '') {
                            choice.displayfeedback = true;
                        }
                    }
                    else {
                        choice.AnswerClass = "selectedC incorrect";
                        if (choice.incorrect_feedback != '') {
                            choice.displayfeedback = true;
                        }
                        isincorrect = true;
                    }
                }
            });
            if (isincorrect) {
                $scope.ShowGlobalFeebackDataForComposite('incorrect', question);
            } else {
                $scope.ShowGlobalFeebackDataForComposite('correct', question);
            }
        }
    };

    $scope.CheckChoiceMatrixTypeAnswer = function (question) {
        var selectedItems = $scope.FindUserSelectedOptions(question);
        var selectedoptions = angular.fromJson(selectedItems);
        if (selectedoptions != undefined && selectedoptions.length > 0) {
            var currentAccess;
            var isincorrect = false;
            if (parseInt(selectedoptions) == 1) {
                currentAccess = true;
            }
            else {
                currentAccess = false;
            }
            $(question.choices).each(function (index, value) {
                var optionindex = index + 1;
                value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                $(value.suboption).each(function (sindex, soption) {
                    if (value.suboptionSelected == soption.identifier) {
                        if (value.isAnswered.toString() == soption.isAnswered.toString()) {
                            question.choices[index].suboption[sindex].currentClass = "correct";
                            if (value.correct_feedback != "") {
                                value.displayfeedback = true;
                            }
                            else {
                                value.displayfeedback = false;
                            }
                        } else {
                            question.choices[index].suboption[sindex].currentClass = "incorrect";
                            isincorrect = true;
                            if (value.incorrect_feedback != "") {
                                value.displayfeedback = true;
                            }
                            else {
                                value.displayfeedback = false;
                            }
                        }
                    }

                });
            });
            if (isincorrect) {
                $scope.ShowGlobalFeebackDataForComposite('incorrect', question);
            } else {
                $scope.ShowGlobalFeebackDataForComposite('correct', question);
            }
        }
    }

    $scope.CheckOrderTypeAnswer = function (question) {
        var isincorrect = false;
        var isselected = false;
        $(question.choices).each(function (index, value) {
            if (parseInt(value.choiceid) != index + 1) {
                isselected = true;
            }
        });
        if (isselected) {
            //if (question.userHasSelectedOptions) {
            $(question.choices).each(function (index, value) {
                var correctoptions = question.correct_answer.split(",");
                var optionindex = value.choiceid;
                if (parseInt(optionindex) == correctoptions[index]) {
                    value.isCorrect = true;
                    value.currentClass = "correct";
                    value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                    if (value.correct_feedback != "") {
                        value.displayfeedback = true
                    }
                    else { value.displayfeedback = false; }
                }
                else {
                    value.isCorrect = false;
                    value.currentClass = "incorrect";
                    isincorrect = true;
                    value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                    if (value.incorrect_feedback != "") {
                        value.displayfeedback = true
                    }
                    else { value.displayfeedback = false; }
                }
            });
            //}
            if (isincorrect) {
                $scope.ShowGlobalFeebackDataForComposite('incorrect', question);
            } else {
                $scope.ShowGlobalFeebackDataForComposite('correct', question);
            }
        }
    }

    $scope.CheckDNDAnswers = function (question) {
        var selectedItems = $scope.FindUserSelectedOptions(question);
        var selectedoptions = angular.fromJson(selectedItems);
        var isincorrect = false;
        var isblankoption = false;
        if (selectedoptions != undefined && selectedoptions.length > 0) {
            $(question.choices).each(function (index, value) {
                value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                var inputValue = value.UserInput;
                var textFound = false;
                if (question.question_type.text == Enum.QuestionType.FIBDND) {
                    $(selectedoptions).each(function (sindex, svalue) {
                        if (svalue.noforeachblank == value.noforeachblank) {
                            inputValue = svalue.inputvalue;
                        }
                    });
                }
                if (value.interaction == 'dragdrop' && value.Canvas.length > 0) {
                    inputValue = value.Canvas[0].title;
                }

                if (inputValue.length > 0) {
                    $(value.textforeachblank).each(function (textindex, tvalue) {
                        if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                            if (inputValue.indexOf('<span math dynamic>') == -1) {
                                inputValue = '<span math dynamic>' + inputValue + '</span>';
                            }
                            if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(inputValue)) {
                                textFound = true;
                            }
                        }
                        else {
                            if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(inputValue)) {
                                textFound = true;
                            }
                        }
                    });

                    if (textFound) {
                        value.AnswerClass = "correct";
                        if (value.correct_feedback != '') {
                            value.displayfeedback = true;
                        }
                        else { value.displayfeedback = false; }
                    }
                    else {
                        value.AnswerClass = "incorrect";
                        if (value.incorrect_feedback != "")
                        { value.displayfeedback = true; }
                        else { value.displayfeedback = false; }
                        isincorrect = true;
                    }
                }
                else {
                    if (value.interaction != 'container')
                        isblankoption = true;
                }

                if (value.interaction == 'container') {
                    if (value.Canvas.length > 0) {
                        var textArray = value.textforeachblank.map(function (x) {
                            return $('<span>' + x.text + '</span>').text();
                        });
                        var isOptionIncorrect = false;
                        for (var i = 0; i < value.Canvas.length; i++) {
                            if ($.inArray(value.Canvas[i].title, textArray) > -1) {
                                value.Canvas[i].AnswerClass = "correct";
                                if (value.correct_feedback != '') {
                                    value.displayfeedback = true;
                                }
                                else { value.displayfeedback = false; }
                            }
                            else {
                                value.Canvas[i].AnswerClass = "incorrect";
                                if (value.incorrect_feedback != "")
                                { value.displayfeedback = true; }
                                else { value.displayfeedback = false; }
                                isOptionIncorrect = true;
                                isincorrect = true;
                            }
                        }
                        value.AnswerClass = isOptionIncorrect ? 'incorrect' : 'correct';
                    }
                    else {
                        isblankoption = true;
                    }
                    if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
                        $scope.$apply();
                    }

                    var drpItems = $('#divDropOption' + index + ' li a');
                    $(value.multiSelectItem).each(function (eindex, item) {
                        var itemFound = false;
                        if ($.inArray(item, textArray) > -1) {
                            itemFound = true;
                        }
                        $(drpItems).each(function (eindex, element) {
                            if (item == $(element).text().trim()) {
                                var className = itemFound ? 'correct' : 'incorrect';
                                $(element).parent().addClass(className);
                            }
                        });
                    });
                }
            });
            if (!isincorrect && !isblankoption) {
                $scope.ShowGlobalFeebackDataForComposite('correct', question);
            } else {
                $scope.ShowGlobalFeebackDataForComposite('incorrect', question);
            }
        }
    }

    $scope.CheckMCSSAndMCMSAnswer = function (question) {
        var selectedItems = $scope.FindUserSelectedOptions(question);
        var correctoptions = question.correct_answer.split(",");
        var selectedoptions = angular.fromJson(selectedItems).map(function (x) { return x.choiceid.toString(); });
        if (selectedoptions != undefined && selectedoptions.length > 0) {
            var isincorrect = false;
            $(question.choices).each(function (index, value) {
                var optionindex = index + 1;
                var answerFound = false;

                if (this.isAnswered == true) {
                    $(correctoptions).each(function (correctindex) {
                        if (parseInt(value.choiceid) == parseInt(correctoptions[correctindex])) {
                            answerFound = true;
                        }
                    });
                    if (answerFound) {
                        value.isCorrect = true;
                        value.currentClass = "correct";
                        value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                        if (value.correct_feedback != "") {
                            value.displayfeedback = true;
                        }
                        else {
                            value.displayfeedback = false;
                        }

                    }
                    else {
                        value.isCorrect = false;
                        value.currentClass = "incorrect";
                        isincorrect = true;
                        value.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                        if (value.incorrect_feedback != "") {
                            value.displayfeedback = true;
                        }
                        else {
                            value.displayfeedback = false;
                        }
                    }
                }
                else {
                    value.currentClass = "";
                }
            });
            if (isincorrect) {
                $scope.ShowGlobalFeebackDataForComposite('incorrect', question);
            } else {
                $scope.ShowGlobalFeebackDataForComposite('correct', question);
            }
        }
    }

    $scope.FindUserSelectedOptions = function (question) {
        var params = [];
        var str = "";
        if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
            $(question.choices).each(function (index, value) {
                if (value.isChoiceAnswered) {
                    var name = value.choiceid;
                    params.push({ "inputvalue": value.choiceid + '-' + $('input[name=' + name + ']:checked').val(), "isCorrectAnswer": true, "questionTypeId": question.question_type.text });
                }
            });
            return JSON.stringify(params);
        }
        else if (question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND || question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
            $(question.choices).each(function (index, value) {
                var userInputValue = value.UserInput;
                var correctAnswer = false;

                $(value.Canvas).each(function (cindex, cvalue) {
                    cvalue.title = cvalue.title.match(/<span/) ? $(cvalue.title).text() : cvalue.title;
                    userInputValue = cvalue.title;
                });

                if (value.interaction == 'dragdrop' && userInputValue.length == 0) {
                    if (question.question_type.text == Enum.QuestionType.FIBDND) {
                        var selectedValue = question.SelectedTemplateArray.filter(function (x) { return x.dropId == index; }).map(function (x) { return x.title; });
                        userInputValue = selectedValue.length > 0 ? selectedValue[0] : '';
                    }
                    else {
                        userInputValue = (!$scope.isUndefined(value.Canvas) && value.Canvas.length > 0) ? value.Canvas[0].title : "";
                    }
                }

                if (!!userInputValue) {
                    $(value.textforeachblank).each(function (textindex, tvalue) {
                        if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                            if (userInputValue.indexOf('<span math dynamic>') == -1) {
                                userInputValue = '<span math dynamic>' + userInputValue + '</span>';
                            }
                            if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                        else {
                            if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                    });
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    var textArray = value.textforeachblank.map(function (x) { return $('<span>' + x.text + '</span>').text(); });

                    for (var i = 0; i < value.Canvas.length; i++) {
                        if ($.inArray(value.Canvas[i].title, textArray) > -1) {
                            value.Canvas[i].AnswerClass = "correct";
                        }
                        else {
                            value.Canvas[i].AnswerClass = "incorrect";
                        }
                    }
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": value.Canvas, "choiceid": value.choiceid, "questionTypeId": question.question_type.text });
                }
                else if (!!userInputValue) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": userInputValue, "isCorrectAnswer": correctAnswer, "choiceid": value.choiceid, "questionTypeId": question.question_type.text });
                }

            });
            return JSON.stringify(params);
        } else if (question.question_type.text == Enum.QuestionType.Essay) {

            if (question.toolbar.type == "math_3-5_palette" || question.toolbar.type == "math_6-8_palette") {
                // removing the unwanted duplicate cursor from matheditor
                if ($('#mathQuill .cursor').length > 1) {
                    $('#mathQuill .cursor:first').remove();
                }

                if ($('#mathQuill').mathquill('latex') == "{ }") {
                    $('#mathQuill').mathquill('8', 'keyStroke');
                }
                str = $('#mathQuill').mathquill('latex');
            } else {
                // $(question.choices).each(function (index) {
                str = question.EssayText;
                //$scope.essayTemplateText;//$("#txtEssayContent").text();
                //});
            }
            //var html = $(str).text();
            params.push(str);
            return params;
        } else if (question.question_type.text == Enum.QuestionType.Order) {
            var selectedIds = '';
            $(question.choices).each(function (index) {
                if (selectedIds == '') {
                    selectedIds = question.choices[index].choiceid;
                }
                else {
                    selectedIds = selectedIds + "," + question.choices[index].choiceid;
                }
            });
            params.push({ "selectedOrder": selectedIds, "questionTypeId": question.question_type.text });
            return JSON.stringify(params);
        } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
            angular.forEach(question.choices, function (option, key) {
                if (option.AnswerClass != undefined && option.AnswerClass == 'selectedC') {
                    params.push({ "choiceid": option.choiceid });
                }
            });
            return JSON.stringify(params);
        }
        else {
            $(question.choices).each(function (index) {
                if (this.isAnswered == true) {
                    params.push({ "choiceid": this.choiceid, "questionTypeId": question.question_type.text });
                }
            });
        }
        return JSON.stringify(params);
    }

    //*******************End of Composite question answer**************************

    $scope.checkCorrectTextEntered = function () {
        $scope.selectedoptions = angular.fromJson($scope.selectedItems);
        var selectedText = '';
        $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
            var inputValue = value.UserInput;
            var textFound = false;
            if (value.interaction == 'dragdrop') {
                if (value.Canvas.length > 0) {
                    inputValue = value.Canvas[0].title;
                }
                var selectedValue = $scope.SelectedTemplateArray.filter(function (x) { return x.dropId == index; }).map(function (x) { return x.title; });
                if (selectedValue.length > 0) {
                    inputValue = selectedValue[0];
                }
            }

            if (inputValue.length > 0) {
                if (inputValue.indexOf("<span math dynamic>") != -1) {
                    selectedText = inputValue;
                }
                else {
                    selectedText = '<span math dynamic>' + inputValue + '</span>'
                }
                $(value.textforeachblank).each(function (textindex, tvalue) {
                    if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                        if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(selectedText)) {
                            textFound = true;
                        }
                    }
                    else {
                        if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(inputValue)) {
                            textFound = true;
                        }
                    }
                });
                if (textFound) {
                    $scope.QuestionOptions[index].AnswerClass = "correct";
                    if ($scope.QuestionOptions[index].correct_feedback != '') {
                        $scope.QuestionOptions[index].displayfeedback = true;
                        value.isCorrect = true;
                        value.currentClass = "correct";
                    }
                    // $scope.ShowGlobalFeebackData($scope.QuestionOptions[index].AnswerClass);
                }
                else {
                    $scope.QuestionOptions[index].AnswerClass = "incorrect";
                    if ($scope.QuestionOptions[index].incorrect_feedback != '') {
                        $scope.QuestionOptions[index].displayfeedback = true;
                        value.isCorrect = true;
                        value.currentClass = "incorrect";
                    }
                    $scope.ShowGlobalIncorrectFeedback = true;
                    // $scope.ShowGlobalFeebackData($scope.QuestionOptions[index].AnswerClass);
                }

            }
            if ($scope.CurrentQuestion.QuestionData.question_type.text != 'ltd' && $scope.CurrentQuestion.QuestionData.question_type.text != 'ltd_dnd' && value.interaction == 'dragdrop' || ($scope.CurrentQuestion.QuestionData.question_type.text == 'matching' && $scope.CurrentQuestion.QuestionData.question_layout.text != "dragdrop_parallel")) {
                //if ($scope.CurrentQuestion.QuestionData.question_type.text !='ltd' && $scope.CurrentQuestion.QuestionData.question_type.text !='ltd_dnd' &&  $scope.CurrentQuestion.QuestionData.question_layout.text != "dragdrop_parallel" && value.interaction == 'dragdrop') {
                var drpItems = $('#divDropOption' + index + ' li a');
                $($scope.QuestionOptions[index].multiSelectItem).each(function (eindex, item) {
                    var itemFound = false;
                    if ($.inArray(item, textArray) > -1) {
                        itemFound = true;
                    }
                    $(drpItems).each(function (eindex, element) {
                        if (item == $(element).text().trim()) {
                            var className = itemFound ? 'correct' : 'incorrect';
                            $(element).parent().addClass(className);
                        }
                    });
                });
            }
            if (value.interaction == 'container' && value.Canvas.length > 0) {
                var textArray = value.textforeachblank.map(function (x) {
                    return $('<span>' + x.text + '</span>').text();
                });
                var isOptionIncorrect = false;
                for (var i = 0; i < value.Canvas.length; i++) {
                    if ($.inArray($(value.Canvas[i].title).text(), textArray) > -1) {
                        value.Canvas[i].AnswerClass = "correct";
                        if (value.correct_feedback != '') {
                            value.displayfeedback = true;
                        }
                        // $scope.ShowGlobalFeebackData(value.Canvas[i].AnswerClass);
                    }
                    else {
                        value.Canvas[i].AnswerClass = "incorrect";
                        $scope.ShowGlobalIncorrectFeedback = true;
                        if (value.incorrect_feedback != '') {
                            value.displayfeedback = true;
                            isOptionIncorrect = true;
                        }
                        // $scope.ShowGlobalFeebackData(value.Canvas[i].AnswerClass);
                    }
                }
                value.AnswerClass = isOptionIncorrect ? 'incorrect' : 'correct';
                var drpItems = $('#divDropOption' + index + ' li a');
                $($scope.QuestionOptions[index].multiSelectItem).each(function (eindex, item) {
                    var itemFound = false;
                    if ($.inArray(item, textArray) > -1) {
                        itemFound = true;
                    }
                    $(drpItems).each(function (eindex, element) {
                        if (item == $(element).text().trim()) {
                            var className = itemFound ? 'correct' : 'incorrect';
                            $(element).parent().addClass(className);
                        }
                    });
                });

                //if ($scope.selectedoptions.length > 0) {
                //    $($scope.selectedoptions).each(function (sindex, svalue) {
                //        if (svalue.choiceid == value.choiceid) {
                //            if (svalue.inputvalue.length != value.textforeachblank.length) { $scope.ShowGlobalIncorrectFeedback = true; }
                //        }
                //    });
                //}
            }
        });
        if (!$scope.ShowGlobalIncorrectFeedback && ($scope.selectedoptions.length == $scope.CurrentQuestion.QuestionData.choices.length)) {
            $scope.ShowGlobalFeebackData("correct");
        }
        else {
            $scope.ShowGlobalFeebackData("incorrect");
        }

    };

    function checkEqualArray(array1, array2) {
        if (array1.length != array2.length) {
            return false;
        }
        for (var i = 0; i < array1.length; i++) {
            if (array2.indexOf(array1[i]) == -1) {
                return false;
            }
        }
        return true;
    }


    $scope.getValue = function (text) {
        $scope.essayTemplateText = text;
    }
    //$scope.generateFeedback = function () {        
    //    $state.go('FeedBack');                
    //};
    //$scope.generateFeedback = function () {
    //    var modalInstance = $modal.open({
    //        templateUrl: routeUrl + 'Scripts/app/views/assessment/FeedBack.html',
    //        controller: 'FeedBackCtrl'

    //    });

    //    //$scope.cancel = function () {
    //    //    modalInstance.dismiss('cancel');
    //    //};

    //};
    $scope.userSelectedOptions = function () {
        var params = [];
        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            return $scope.GetUserSelectedOptionForCompositeType($scope.CurrentQuestion.QuestionData.questions);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.Hotspot) {
            if ($scope.UserAnswerText) {
                params.push({ "inputvalue": $scope.UserAnswerText, "isCorrectAnswer": $scope.selectedHotspot, "questionTypeId": $scope.questionTypeId });
            }
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                // $.each($scope.CurrentQuestion.QuestionData.choices[index].suboption, function (index1, value1) {
                if (value.isChoiceAnswered) {
                    var name = value.choiceid;
                    params.push({ "inputvalue": value.choiceid + '-' + $('input[name=' + name + ']:checked').val(), "isCorrectAnswer": true, "questionTypeId": $scope.questionTypeId });
                }
            });
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND || $scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.Matching) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                var userInputValue = value.UserInput;
                var correctAnswer = false;
                if (value.interaction == 'dragdrop') {
                    userInputValue = (value.Canvas.length > 0) ? value.Canvas[0].title : "";
                    var selectedValue = $scope.SelectedTemplateArray.filter(function (x) { return x.dropId == index; }).map(function (x) { return x.title; });
                    if (selectedValue.length > 0) {
                        userInputValue = selectedValue[0];
                    }
                }

                if (!!userInputValue) {
                    $(value.textforeachblank).each(function (textindex, tvalue) {
                        if (value.interaction == 'dragdrop' || value.interaction == 'inline') {
                            if ($filter('lowercase')('<span math dynamic>' + tvalue.text + '</span>') == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                        else {
                            if ($filter('lowercase')(tvalue.text) == $filter('lowercase')(userInputValue)) {
                                correctAnswer = true;
                            }
                        }
                    });
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    var textArray = value.textforeachblank.map(function (x) { return '<span math dynamic>' + x.text + '</span>'; });

                    for (var i = 0; i < value.Canvas.length; i++) {
                        if (textArray.indexOf(value.Canvas[i].title) != -1) {
                            value.Canvas[i].AnswerClass = "correct";
                        }
                        else {
                            value.Canvas[i].AnswerClass = "incorrect";
                        }
                    }
                }

                if (value.interaction == 'container' && value.Canvas.length > 0) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": value.Canvas, "choiceid": value.choiceid, "questionTypeId": $scope.questionTypeId });
                }
                else if (!!userInputValue) {
                    params.push({ "noforeachblank": value.noforeachblank, "inputvalue": userInputValue, "isCorrectAnswer": correctAnswer, "choiceid": value.choiceid, "questionTypeId": $scope.questionTypeId });
                }

            });
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.Essay) {
            var str = "";
            //$scope.CurrentQuestion.QuestionData
            if ($scope.CurrentQuestion.QuestionData.toolbar.type == "math_3-5_palette" || $scope.CurrentQuestion.QuestionData.toolbar.type == "math_6-8_palette") {
                // removing the unwanted duplicate cursor from matheditor
                if ($('#mathQuill .cursor').length > 1) {
                    $('#mathQuill .cursor:first').remove();
                }

                if ($('#mathQuill').mathquill('latex') == "{ }") {
                    $('#mathQuill').mathquill('8', 'keyStroke');
                }
                str = $('#mathQuill').mathquill('latex');
            } else {
                str = $scope.essayTemplateText;//$("#txtEssayContent").text();
            }
            //var html = $(str).text();
            params.push(str);
            return params;
        }
        else if ($scope.questionTypeId == Enum.QuestionType.Order) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                params.push({ "choiceid": this.CurrentOptionIndex, "questionTypeId": $scope.questionTypeId });
            });
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.TextSelection) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                if ($scope.CurrentQuestion.QuestionData.choices[index].AnswerClass.indexOf("selectedC") != -1) {
                    params.push({ "choiceid": this.choiceid, "questionTypeId": $scope.questionTypeId });
                }
            });
            return JSON.stringify(params);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.HotSpot) {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                if ($scope.UserSelectedArea.indexOf(this.choiceid) != -1) {
                    params.push({ "choiceid": this.choiceid, "questionTypeId": $scope.questionTypeId });
                }
            });
            return JSON.stringify(params);
        }
        else {
            $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                if (this.isAnswered == true) {
                    params.push({ "choiceid": this.choiceid, "questionTypeId": $scope.questionTypeId });
                }
            });
        }
        return JSON.stringify(params);

    }




    $scope.setOptions = function (option, OptionId) {
        if ($scope.isPaused) return false;
        if ($scope.questionTypeId == Enum.QuestionType.MCSS || $scope.questionTypeId == Enum.QuestionType.MCMS) {
            if ($('#checkAnswerbtn').html() == 'Try Again') {
                if (option.currentClass == "") {
                    $('#radio' + option.choiceid).prop('checked', '');
                    $('#chkBox' + option.choiceid).prop('checked', '');
                }

                $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                    if (this.currentClass != "") {
                        $('#radio' + this.choiceid).prop('checked', 'checked');
                        $('#chkBox' + this.choiceid).prop('checked', 'checked');
                    }
                });
                return false;
            }
        }
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.ShowGlobalCorrectFeedback = false;
        $(".option").removeClass('correct');
        $(".option").removeClass('incorrect');
        if ($('#checkAnswerbtn').attr('disabled') != 'disabled') {
            $('#checkAnswerbtn').html('Check Answer');
            $('#checkAnswerbtnmobile').html('Check Answer');
        }
        switch ($scope.questionTypeId) {
            case Enum.QuestionType.MCSS:
            case Enum.QuestionType.GRAPHIC:
            case Enum.QuestionType.Exhibit:
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                    this.currentClass = "";
                    if (this.choiceid == option.choiceid) {
                        this.isAnswered = true;
                        option.currentClass = "selected";
                    }
                    else {
                        this.isAnswered = false;
                    }
                    this.isButtonClicked = true;
                    if (this.isAnswered) {
                        $scope.userHasSelectedOptions = true;
                        this.currentClass = "selected";
                    }
                });

                break;
            case Enum.QuestionType.MCMSVIDEO:
            case Enum.QuestionType.MCMS:
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index) {
                    if (this.choiceid == option.choiceid) {
                        if (this.isAnswered == true) {
                            this.isAnswered = false;
                            option.currentClass = "";
                        }
                        else {
                            this.isAnswered = true;
                            option.currentClass = "selected";
                        }
                    }
                    if (this.isAnswered == true) {
                        this.currentClass = "selected";
                        $scope.userHasSelectedOptions = true;
                    }
                });
                break;
            case Enum.QuestionType.ChoiceMatrix:
                $($scope.CurrentQuestion.QuestionData.choices).each(function (index, value) {
                    $scope.selectedOption = OptionId;

                    $(value.suboption).each(function (sindex, soption) {
                        if (value.choiceid == OptionId) {
                            value.isChoiceAnswered = true;
                            if (option.identifier == soption.identifier) {
                                this.isOptionAnswered = true;
                                value.isAnswered = value.assess;
                                value.suboptionSelected = soption.identifier;
                                soption.isAnswered = soption.value;
                                soption.currentClass = "selected";
                            }
                            else {
                                this.isOptionAnswered = false;
                                soption.currentClass = "";
                                soption.isAnswered = !soption.value;
                            }
                        }
                        if (this.isOptionAnswered == true) {
                            this.currentClass = "selected";
                            $scope.userHasSelectedOptions = true;
                        }
                    });
                });
                break;
            case Enum.QuestionType.Order:
                $(option).each(function (index) {
                    this.isAnswered = true;
                    if (this.isAnswered) {
                        $scope.userHasSelectedOptions = true;
                    }
                });
                break;
            case Enum.QuestionType.FIB:
            case Enum.QuestionType.FIBDND:
            case Enum.QuestionType.Matching:
            case Enum.QuestionType.LTD:
            case Enum.QuestionType.LTDDND:
                var totalChoices = $scope.CurrentQuestion.QuestionData.choices.length;
                var objList = $filter('filter')($scope.CurrentQuestion.QuestionData.choices, { "Canvas": [] }, true);
                var objList2 = $filter('filter')($scope.CurrentQuestion.QuestionData.choices, { "UserInput": "" }, true);
                if (objList.length < totalChoices && objList.length > 0)
                    $scope.userHasSelectedOptions = true;
                if (objList2.length < totalChoices && objList.length > 0)
                    $scope.userHasSelectedOptions = true;
                //if (objList.length == 0) mehul
                //    $scope.userHasSelectedOptions = false;
                break;
        }
    }

    $scope.enableDisableCheckAnswerButton = function (flagValue) {
        $scope.userHasSelectedOptions = flagValue;
    }

    $scope.shuffleArray = function (array) {
        var m = array.length, t, i;

        // While there remain elements to shuffle
        while (m) {
            // Pick a remaining element…
            i = Math.floor(Math.random() * m--);

            // And swap it with the current element.
            t = array[m];
            array[m] = array[i];
            array[i] = t;
        }

        return array;
    };

    $scope.showHideCheckAnswer = function () {
        //if ($scope.CurrentQuestion.QuestionData.global_correct_feedback.text != null && $scope.CurrentQuestion.QuestionData.global_correct_feedback.text != "") {
        //    $scope.showCheckAnswerbtn = true;
        //}
        //else {            
        //    $scope.showCheckAnswerbtn = false;
        //}
        //if ($scope.CurrentQuestion.QuestionData.question_type.text != 'essay') {
        //    $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
        //        if (value.feedback != null && value.feedback != "") {
        //            $scope.showCheckAnswerbtn = true;
        //        }
        //    });
        //}
        if ($scope.CurrentQuestion.QuestionData.question_type.text == 'essay') {
            $scope.showCheckAnswerbtn = false;
        }
    };

    // $scope.showOptionFeedback = function (optionId) {
    // if(optionId != undefined)
    // {
    // var popupValue = '';
    // $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
    // if (value.choiceid == optionId) {
    // if (value.feedback != null && value.feedback != undefined && value.feedback != "") {
    // popupValue = '<span>' + value.feedback + '</span>';
    // popupValue = $compile(popupValue)($scope);

    // $('.container-main').click(function(e){
    // if (e.target.className != null && e.target.className != undefined && e.target.className != "" && e.target.className != 'fa fa-commenting-o')
    // {
    // $('.popover').hide(); 
    // }
    // });
    // }
    // }
    // });

    // if(popupValue != "" && popupValue != undefined && popupValue != null)
    // {
    // popupValue = popupValue[0].innerHTML;
    // }
    // return popupValue;
    // }
    // };


    $scope.initializeQuiz = function (data) {
        popovermodel = '';
        $scope.showExhibit = false;
        $scope.CheckAnswer = false;
        $scope.userHasSelectedOptions = false;
        $scope.questionTypeId = data.QuestionTypeId;

        if (!$scope.isUndefined(data.QuestionData.question_type)) {
            if ((data.QuestionData.question_type.text.toLowerCase() == 'ltd' || data.QuestionData.question_type.text.toLowerCase() == 'ltd_dnd' || data.QuestionData.question_type.text.toLowerCase() == 'hotspot' || data.QuestionData.question_type.text.toLowerCase() == 'textselection' || data.QuestionData.question_type.text.toLowerCase() == 'choicematrix' || data.QuestionData.question_type.text == 'mcss' || data.QuestionData.question_type.text == 'composite' || data.QuestionData.question_type.text == 'mcms' || data.QuestionData.question_type.text == 'order' || data.QuestionData.question_type.text == 'fib' || data.QuestionData.question_type.text == 'fib_dnd' || data.QuestionData.question_type.text == 'matching' || data.QuestionData.question_type.text == 'essay') && data.QuestionData.question_type.text.length > 0) {
                $scope.questionTypeId = data.QuestionData.question_type.text.toLowerCase();
            }
        }
        if ($scope.questionTypeId != 'composite') $scope.setQuestionOptions(data);
        $scope.CurrentQuestion = data;
        $scope.questionName = data.QuestionData.question_stem.text;
        if ($scope.questionTypeId == 'composite') {
            $scope.questionBody = data.QuestionData.question_body.text;
        }

        if (!$scope.isUndefined(data.QuestionData.stem_details) && data.QuestionData.stem_details.type == 'algorithimic') {
            $scope.SetAlgorithimicType(data);
        }
        $scope.instructionText = data.QuestionData.instruction_text.text;
        $scope.totalItems = $scope.questions.length;

        $scope.isEssay = false;
        $scope.isComposite = false;
        $scope.compositeQuestions = [];
        $scope.showHint = false;
        $scope.BookMarked = false;
        $scope.NotLearned = false;
        $scope.ReportNote = '';
        $scope.CorrectAnswerCount = 0;
        $scope.HintsUsed = 0;

        $scope.ShowGlobalFeedback = false;
        $scope.ShowGlobalIncorrectFeedback = false;
        $scope.ShowGlobalCorrectFeedback = false;
        $scope.showOptionFeedbackModal = false;

        $scope.globalCorrectFeedback = '';
        $scope.globalInCorrectFeedback = '';
        $scope.OptionFeedback = '';
        $scope.showCheckAnswerbtn = true;
        $scope.ShowQueFeedback = true;
        $scope.ShowFeedBackAt = { 'Question': true, 'Option': true };

        $scope.NoOfSelection = 0;
        if (data.UserAssessmentActivityDetail) {
            $scope.UserAssessmentActivityDetail = data.UserAssessmentActivityDetail;
            $scope.BookMarked = $scope.UserAssessmentActivityDetail.IsBookMarked;
            $scope.NotLearned = $scope.UserAssessmentActivityDetail.NotLearned;
            $scope.ReportNote = $scope.UserAssessmentActivityDetail.ReportAProblem;
            $scope.CorrectAnswerCount = $scope.UserAssessmentActivityDetail.NoOfRetry;
            $scope.HintsUsed = $scope.UserAssessmentActivityDetail.HintsUsed;
        } else {
            $scope.UserAssessmentActivityDetail = {
                IsBookMarked: false,
                NotLearned: false,
                HintsUsed: 0,
                NoOfRetry: 0,
                TimeSpent: 0,
                ReportAProblem: '',
                QuizQuestID: '',
                UserAssessmentActivityType: 0
            };
        }

        if (!jQuery.isEmptyObject($scope.AssignmentSetting)) {
            $scope.ShowHintBox = $scope.AssignmentSetting.HintOn;
            $scope.BackwardMovement = $scope.AssignmentSetting.BackwardMovement;
            $scope.SkipQuestion = $scope.AssignmentSetting.SkipQuestion;
            $scope.ShuffleQuestions = $scope.AssignmentSetting.Shuffle;
            $scope.AssessmentAttempt = $scope.AssignmentSetting.Attempts;
            $scope.QuestionAttempt = $scope.AssignmentSetting.QuestionAttempts;

            if (!$scope.isUndefined($scope.AssignmentSetting.ShowQueFeedback))
                $scope.ShowQueFeedback = $scope.AssignmentSetting.ShowQueFeedback;

            $scope.PauseTimer = $scope.AssignmentSetting.PauseTimer;

            if (!$scope.isUndefined($scope.AssignmentSetting.ShowFeedBackAt) && !$scope.isUndefined($scope.AssignmentSetting.ShowFeedBackAt.Option)) {
                $scope.ShowFeedBackAt = angular.fromJson($scope.AssignmentSetting.ShowFeedBackAt);
            }

            if ($scope.ShowFeedBackAt.Question == true) {
                $scope.ShowGlobalFeedback = true;
                //$scope.showCheckAnswerbtn = true;
                //$scope.showHideCheckAnswer();
            }
            if ($scope.ShowFeedBackAt.Option == true) {
                $scope.showOptionFeedbackModal = true;
                //$scope.showCheckAnswerbtn = true;
                //$scope.showHideCheckAnswer();
            }

            $scope.showHideCheckAnswer();
            if ($scope.ShuffleQuestions) {
                if (data.QuestionData.choices != null || data.QuestionData.choices != undefined) {
                    $scope.QuestionOptions = $scope.shuffleArray(data.QuestionData.choices);
                }
            }
            else {
                $scope.QuestionOptions = data.QuestionData.choices;
            }

            if ($scope.SkipQuestion == false) {
                $scope.disableNextBtn = true;
            }
            else {
                $scope.disableNextBtn = false;
            }

            $('#checkAnswerbtn').html('Check Answer');

            if ($scope.CorrectAnswerCount > 0) {
                $scope.showAttempt = true;
                if ($scope.QuestionAttempt == $scope.CorrectAnswerCount) {
                    $('#checkAnswerbtn').html('Try Again');
                    $('#checkAnswerbtnmobile').html('Try Again');
                    $scope.DisableCorrectAnswer = true;
                }
            }
        }
        else {
            $('#checkAnswerbtn').html('Check Answer');
            $('#checkAnswerbtnmobile').html('Check Answer');
            $scope.AssignmentSetting = {};
            $scope.ShowGlobalFeedback = true;
            $scope.showOptionFeedbackModal = true;
            $scope.showAttempt = true;
            $('.attempts').hide();
            $scope.showCheckAnswerbtn = true;
            $scope.ShowHintBox = true;
            $scope.showHintCount = true;
            //  $scope.ShowFeedBackAt.Question = true;
            $scope.QuestionOptions = data.QuestionData.choices;
        }

        angular.forEach($scope.QuestionOptions, function (option, key) {
            option.displayfeedback = false;
            if ($scope.questionTypeId == Enum.QuestionType.FIBDND) {
                $("div[data-drop-id='" + key + "']").empty();
            }
        });
        //$scope.PassiveTimer.startTimer(data.TimeSpent);
        //$scope.text = '';
        $scope.currentPage = data.CurrentQuestion;
        if ($scope.questionTypeId == Enum.QuestionType.MCMS || $scope.questionTypeId == Enum.QuestionType.MCSS) {
            $scope.questionSubType = data.QuestionData.question_interaction.text;
            $scope.questionLayout = data.QuestionData.question_layout.text;
            //$scope.exhibitType = data.QuestionData.exhibit[0].exhibit_type;
            if (!$scope.isUndefined(data.QuestionData.exhibit) && data.QuestionData.exhibit.length > 0 && data.QuestionData.exhibit[0].exhibit_type != "") {
                $scope.showExhibit = true;
                if (data.QuestionData.exhibit[0].exhibit_type == 'image') {
                    //var newString = String(data.QuestionData.exhibit[0].path).replace("___ASSETINFO", "").replace("___<br>", "").replace(/&#39;/g, '"').replace(/'/g, '"');
                    //var optionImage = JSON.parse(newString);
                    var imagePath = String(data.QuestionData.exhibit[0].path);
                    // var newImage= imagePath[0];
                    //var newImagePath = newImage + '/>';

                    data.QuestionData.exhibit[0].path = imagePath;//"Content/images/" + optionImage.asset_name;
                }
            }
            else {
                $scope.showExhibit = false;
            }
        }
        else {
            $scope.questionSubType = data.QuestionData.settings.question_type;
        }

        if ($scope.questionTypeId == Enum.QuestionType.Order) {
            $scope.orderType = data.QuestionData.question_interaction.text;
            if ($scope.orderType == "media")
                $scope.layoutType = data.QuestionData.question_layout.text;
        }

        if ((!$scope.isUndefined(data.QuestionData.hints) && $scope.AssignmentSetting.HintOn) || (!$scope.isUndefined(data.QuestionData.hints) && $scope.showHintCount)) {

            var hintsUsed = $scope.HintsUsed;
            $scope.hintCount = 0;
            $scope.visibleHints = [];

            for (var i = 0; i < data.QuestionData.hints.length; i++) {
                var value = data.QuestionData.hints[i];
                if (value != null || value!=undefined) {
                    if (value.text != null) {
                        if (hintsUsed != 0) {
                            $scope.showHint = true;
                            $scope.visibleHints.push('<span math dynamic>' + data.QuestionData.hints[i].text + '</span>');
                            hintsUsed = hintsUsed - 1;
                        }
                        else {
                            $scope.hintCount++;
                        }
                    }
                }
            }
        }
        else {
            $scope.hintCount = 0;
            $scope.showHint = false;
        }

        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            $scope.showCheckAnswerbtn = true;
            $scope.isComposite = true;
            var compositeQuestionData = [];
            $('#quizContainer').parent().removeClass('container');
            $('#quizContainer').parent().addClass('composite_container');
            //$.each(data.QuestionData.questions, function (index, value) {
            //    //debugger;
            //    compositeQuestionData.push({ "QuestionData": value });
            //});

            $scope.Questions = null;
            $scope.compositeQuestions = data.QuestionData.questions;
            $scope.CompositeSelectedOption = data.SelectedOptions;
            if ($scope.compositeQuestions.length == 0) {
                $scope.showCheckAnswerbtn = false;
            }
            //$scope.$broadcast('nextCompositeQuestion', {myMsg: "hi children"});
            $scope.$broadcast('nextCompositeQuestion');
            //$scope.$broadcast('resetCompositeQuestion');
            //$scope.ResetCompositeQuestions(data.QuestionData.questions);

            //$scope.$broadcast('resetCompositeQuestion');
            //assessmentEngineSvc.SetCompositeQuestions(data.QuestionData.questions);
            //$timeout(function () {
            //    if (document.getElementById('compositeIframe') != null) document.getElementById('compositeIframe').src = document.getElementById('compositeIframe').src;
            //});
        }
        else {
            $('#quizContainer').parent().addClass('container');
        }

        if ($scope.questionTypeId == Enum.QuestionType.FIB || $scope.questionTypeId == Enum.QuestionType.FIBDND || $scope.questionTypeId == Enum.QuestionType.Matching) {
            $scope.SetFIBType(data);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND) {
            $scope.SetLTDType(data);
        } else if ($scope.questionTypeId == Enum.QuestionType.TextSelection) {
            $scope.SetTextSelectionType(data);
            if (data.SelectedOptions) {
                var selectedoptions = $.parseJSON(data.SelectedOptions);
                $(selectedoptions).each(function (selectindex) {
                    $($scope.QuestionOptions).each(function (index, value) {
                        if ($scope.QuestionOptions[index].choiceid == parseInt(selectedoptions[selectindex].choiceid)) {
                            $scope.QuestionOptions[index].AnswerClass = "selectedC";
                            $scope.NoOfSelection = $scope.NoOfSelection + 1;
                        }
                    });
                });
            }
            console.log($scope.ShowGlobalCorrectFeedback);
        }
        if ($scope.questionTypeId == Enum.QuestionType.HotSpot) {
            $scope.SetHotSpotType(data);
        }

        if ($scope.questionTypeId == Enum.QuestionType.Order) {
            if ($scope.ShuffleQuestions) {
                data.QuestionData.choices = $scope.Shuffle(data.QuestionData.choices);
            }
            $.each(data.QuestionData.choices, function (index, value) {
                data.QuestionData.choices[index].text = '<span math dynamic>' + value.text + '</span>';
                data.QuestionData.choices[index].OptionNo = index + 1;
            });
            //console.log(data.QuestionData.choices);
        }
        if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
            $.each($scope.CurrentQuestion.QuestionData.choices, function (index, value) {
                $.each($scope.CurrentQuestion.QuestionData.choices[index].suboption, function (index1, value1) {
                    $scope.CurrentQuestion.QuestionData.choices[index].suboption[index1] = value1;
                    //$scope.CurrentQuestion.QuestionData.choices[index].suboption[index1].setIndex = index1 + 1;
                });
            });
        }
        if ($scope.questionTypeId == Enum.QuestionType.Essay) {
            $scope.isEssay = true;
            if (data.QuestionData.max_characters != undefined) {
                $scope.maxCharaterLength = data.QuestionData.max_characters.text;
            }
            $scope.EssayTypeToolbar = data.QuestionData.toolbar.type;
            mathEditorType = data.QuestionData.toolbar.type;
            $('.math-toolbar').hide();
        }
        if ($scope.AssignmentQuestions.length > 0 && !$scope.isUndefined($scope.AssignmentQuestions[$scope.currentPage - 1])) {
            if ($scope.AssignmentQuestions[$scope.currentPage - 1].QuestionStatus != 'answer-visited' && $scope.AssignmentQuestions[$scope.currentPage - 1].QuestionStatus != 'answer-right') {
                $scope.getProgressPercentage();
            }
        }
        $('#myTabs').on('click', '.nav-tabs a', function () {
            $(this).closest('.dropdown').addClass('dontClose');
        })

        $('#myDropDown').on('hide.bs.dropdown', function (e) {
            if ($(this).hasClass('dontClose')) {
                e.preventDefault();
            }
            $(this).removeClass('dontClose');
        });

        $('#myDropDown2 .toc-list').click(function () {
            $(this).parent().addClass('open');
        });

        $('#myTabs2 ul.nav li').click(
        function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('.active').removeClass('active').addClass('notactive');
            $(this).addClass('active').removeClass('notactive');
            var ref = $(this).find("a").attr("href");
            $(this).parents("#myTabs2").find(".tab-content .tab-pane").removeClass("active")
            $(this).parents("#myTabs2").find(".tab-content .tab-pane" + ref).addClass("active");
        });

        $('.actions-mobile .dropdown-menu').on('click', function (event) {
            event.stopPropagation();
        });
        //$('#quizContainer').mCustomScrollbar(
        //    {
        //        theme: "dark"
        //    });

        //Added for dynamically loading the Math Editor
        setTimeout(function () {
            resize_fun();
            $('#mathQuill').mathquill('editable');
            $('#mathQuill').width($('.math-editor').width());
            $('#mathQuill').height('auto');
            SetEditorView(mathEditorType);

            if ($.trim(mathEditorPrevLatex).length != 0) {
                mathEditorPrevLatex = mathEditorPrevLatex.replace("?", "");
                $('#mathQuill').mathquill('latex', mathEditorPrevLatex);
                $('#mathQuill textarea').focus();
            } else {
                $('#mathQuill').text('');
                $('#mathQuill').mathquill('editable');
                $('#mathQuill').width($('.math-editor').width());
                $('#mathQuill').height('auto');
                $('#mathQuill').mathquill('cmd', 'emptyspace');
                $('#mathQuill textarea').focus();
            }
        }, 2000);
    }

    $scope.hoverIn = function (event, option) {
        if (!option.stopHover) {
            event.currentTarget.style.opacity = 1;
        }
    };

    $scope.hoverOut = function (event, option) {
        if (!option.stopHover) {
            event.currentTarget.style.opacity = 0;
        }
    };

    $scope.clickHotspot = function (option) {
        if (!$scope.CheckAnswer) {
            if ($scope.NoOfSelection < $scope.MaxSelection || ($scope.NoOfSelection == $scope.MaxSelection && option.currentClass == 'selected')) {
                option.isAnswered = !option.isAnswered;
                var index = $scope.UserSelectedArea.indexOf(option.choiceid);
                if (index > -1) {
                    $scope.UserSelectedArea.splice(index, 1);
                    option.currentClass = '';
                    $scope.NoOfSelection = $scope.NoOfSelection - 1;
                }
                else {
                    $scope.UserSelectedArea.push(option.choiceid);
                    option.currentClass = 'selected';
                    $scope.NoOfSelection = $scope.NoOfSelection + 1;
                }
                if ($scope.NoOfSelection > 0) {
                    $scope.userHasSelectedOptions = true;
                }
                else if ($scope.NoOfSelection == 0) {
                    $scope.userHasSelectedOptions = false;
                }
            }
        }
    };

    $scope.SetHotSpotType = function (data) {
        if (data.QuestionData.max_choice_selection.text == '0') {
            $scope.MaxSelection = data.QuestionData.choices.length;
        }
        else if (data.QuestionData.max_choice_selection.text == '') {
            $scope.MaxSelection = 1;
        }
        else {
            $scope.MaxSelection = parseInt(data.QuestionData.max_choice_selection.text);
        }
        $scope.NoOfSelection = 0;

        var imgObject = document.createElement('div');
        imgObject.innerHTML = data.QuestionData.question_media.media;
        $scope.imageSrc = $(imgObject).find('img').attr('src');
        if ($scope.isUndefined($scope.imageSrc)) {
            $scope.imageSrc = '';
        }

        angular.forEach(data.QuestionData.choices, function (option, key) {
            $scope.QuestionOptions[key].XPosition = '';
            $scope.QuestionOptions[key].YPosition = '';
            $scope.QuestionOptions[key].isAnswered = false;
            // $scope.QuestionOptions[key].opacity = 0;

            var positions = option.text.split(',');
            if (positions.length == 2) {
                $scope.QuestionOptions[key].XPosition = positions[0];
                $scope.QuestionOptions[key].YPosition = positions[1];
            }
        });


        if (!$scope.isUndefined(data.QuestionData.question_media.media) && data.QuestionData.question_media.media.length > 0 && data.QuestionData.question_media.media != "") {
            if (data.SelectedOptions) {
                var selectedoptions = $.parseJSON(data.SelectedOptions);
                $(selectedoptions).each(function (selectindex) {
                    $($scope.QuestionOptions).each(function (index, value) {

                        if ($scope.QuestionOptions[index].choiceid == parseInt(selectedoptions[selectindex].choiceid)) {
                            $scope.QuestionOptions[index].isAnswered = true;
                            $scope.QuestionOptions[index].currentClass = 'selected';
                            $scope.NoOfSelection = $scope.NoOfSelection + 1;
                        }
                    });
                });
            }
        }
    };

    $scope.SetTextSelectionType = function (data) {
        var questiontext = "";//data.QuestionData.textwithblanks.text;
        $scope.PassageText = "";
        if (data.QuestionData.max_choice_selection.text == '0') {
            $scope.MaxSelection = data.QuestionData.choices.length;
        }
        else if (data.QuestionData.max_choice_selection.text == '') {
            $scope.MaxSelection = 1;
        }
        else {
            $scope.MaxSelection = parseInt(data.QuestionData.max_choice_selection.text);
        }
        $scope.NoOfSelection = 0;
        if (data.QuestionData.textwithblanks != undefined) {
            if (data.QuestionData.textwithblanks.text != undefined && data.QuestionData.textwithblanks.text != "")
                questiontext = data.QuestionData.textwithblanks.text;
        }
        if (!$scope.isUndefined(data.QuestionData.exhibit) && data.QuestionData.exhibit.length > 0 && data.QuestionData.exhibit[0].exhibit_type != "") {
            $scope.showExhibit = true;
            if (data.QuestionData.exhibit[0].exhibit_type == 'image') {
                var imagePath = String(data.QuestionData.exhibit[0].path);
                data.QuestionData.exhibit[0].path = imagePath;
            }
        }


        angular.forEach(data.QuestionData.choices, function (option, key) {
            $scope.QuestionOptions[key].key = key;       // Index of option            
            $scope.QuestionOptions[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect            
            $scope.QuestionOptions[key].showOptionFeedbackModal = $scope.showOptionFeedbackModal;
            $scope.QuestionOptions[key].showAttempt = $scope.showAttempt;
            $scope.QuestionOptions[key].ShowGlobalFeedback = $scope.ShowGlobalFeedback;
            if (!$scope.isUndefined(option.text) && option.text != "" && option.text.match("^\[\[[0-9]*\]\]")) {
                var startTagToReplace = option.text;
                var endTagToReplace = option.text.substring(0, 2) + "/" + option.text.substring(2);
                questiontext = questiontext.replace(startTagToReplace, "&nbsp;&nbsp;<span class='option selectable' style='cursor: pointer;' ng-click='$parent.SetSelectedClass(" + key + ")' ng-class='$parent.QuestionOptions[" + key + "].AnswerClass'> <span class='sign'><span class='sr-only'></span><span class='fa'></span></span><span class='fdback fdback-desktop' ng-show='$parent.QuestionOptions[" + key + "].showOptionFeedbackModal && $parent.QuestionOptions[" + key + "].displayfeedback'><span class='sr-only'></span><span id='spnltd" + key + "'class='fa fa-commenting-o' popover popover-html='" + $scope.QuestionOptions[key].choiceid + "' ng-click='$parent.GetFeedback($parent.QuestionOptions[" + key + "])'></span></span>");
                //questiontext = questiontext.replace(startTagToReplace, "&nbsp;&nbsp;<span id='spnltd" + key + "' class='option selectable' style='cursor: pointer;' ng-click='$parent.SetSelectedClass(" + key + ")' ng-class='$parent.QuestionOptions[" + key + "].AnswerClass'> <span class='sign'><span class='sr-only'></span><span class='fa'></span></span><span class='fdback' ng-show='$parent.QuestionOptions[" + key + "].showOptionFeedbackModal && $parent.QuestionOptions[" + key + "].showAttempt'><span class='sr-only'></span><span class='fa fa-commenting-o' popover popover-html='" + $scope.QuestionOptions[key].choiceid + "' ng-click='$parent.GetFeedback($parent.QuestionOptions[" + key + "])'></span></span>");
                questiontext = questiontext.replace(endTagToReplace, "</span>&nbsp;&nbsp;");
            }
        });
        $scope.PassageText = questiontext;
    };

    $scope.SetSelectedClass = function (key) {
        if (!$scope.CheckAnswer) {
            if ($scope.NoOfSelection < $scope.MaxSelection || ($scope.NoOfSelection == $scope.MaxSelection && $scope.QuestionOptions[key].AnswerClass == 'selectedC')) {
                if ($scope.QuestionOptions[key].AnswerClass == 'selectedC') {
                    $scope.QuestionOptions[key].AnswerClass = "";
                    $scope.NoOfSelection = $scope.NoOfSelection - 1;
                }
                else {
                    $scope.QuestionOptions[key].AnswerClass = "selectedC";
                    $scope.NoOfSelection = $scope.NoOfSelection + 1;
                }
            }
            if ($scope.NoOfSelection > 0) {
                $scope.userHasSelectedOptions = true;
            }
            else if ($scope.NoOfSelection == 0) {
                $scope.userHasSelectedOptions = false;
            }
        }
    };

    $scope.$on('elementDropped', function (event, args) {
        //console.log('nextCompositeQuestion');
        //alert(args.dragId);

        if ($scope.questionTypeId == Enum.QuestionType.Composite) {
            var option = ($scope.isUndefined(args.option)) ? null : $.parseJSON(args.option);
            $scope.DragDropInComposite(option, args.dropId, args.title, args.questionIndex);
        }
        else {
            $scope.dropFunction(args.dropId, args.title);
            $scope.setOptions(null, null);
        }
    })

    $scope.DragDropInComposite = function (choice, dropId, dropItemText, questionIndex) {
        var compoQuestionIndex = (choice == null) ? questionIndex : choice.QuestionIndex;
        $scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].userHasSelectedOptions = true;
        $scope.enableDisableCheckAnswerButton(true);
        angular.forEach($scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].SelectedTemplateArray, function (option, key) {
            if (option.title == dropItemText) {
                option.dropId = dropId;
            }
        });
        if (dropId != 'initial') {
            var input = $scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].SelectedTemplateArray.filter(function (x) { return x.dropId == dropId; });
            if (input.length == 1) {
                $scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].choices[dropId].accept = false;
                $scope.$apply();
            }
        }
        else {
            //$('[id^=divDrpItems]').removeClass('drop-class-2');
            var items = $('.drag-container').children().removeClass('drop-class-2');
        }

        angular.forEach($scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].droppableItems, function (option, key) {
            var otitle = option.title;
            if (option.title.indexOf('<span math dynamic>') == -1) {
                otitle = '<span math dynamic>' + option.title + '</span>';
            }
            if ($filter('lowercase')(otitle) == $filter('lowercase')('<span math dynamic>' + dropItemText + '</span>')) {
                option.dropId = dropId;
                if (dropId != 'initial') {
                    option.model = $scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].choices[dropId];
                }
                else {
                    option.model = '';
                }
            }
        });
        var itemCount = 0;
        angular.forEach($scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].choices, function (model, index) {
            itemCount = 0;
            angular.forEach($scope.CurrentQuestion.QuestionData.questions[compoQuestionIndex].SelectedTemplateArray, function (option, key) {
                if (option.dropId == index) {
                    itemCount++;
                }
            });
            if (itemCount == 0) {
                model.accept = true;
                $scope.$apply();
            }
        });
    };

    $scope.dropFunction = function (dropId, dropItemText) {
        angular.forEach($scope.SelectedTemplateArray, function (option, key) {
            if (option.title == dropItemText) {
                option.dropId = dropId;
            }
        });
        $scope.userHasSelectedOptions = true;
        if ($scope.questionTypeId == Enum.QuestionType.FIBDND) {
            $scope.DragDropOnFIB(dropId, dropItemText);
        }
        else if ($scope.questionTypeId == Enum.QuestionType.Matching || $scope.questionTypeId == Enum.QuestionType.LTDDND) {
            $scope.DragDropOnMatching(dropId);
        }
    };

    $scope.DragDropOnFIB = function (dropId, dropItemText) {
        if (dropId != 'initial') {
            var input = $scope.SelectedTemplateArray.filter(function (x) { return x.dropId == dropId; });
            if (input.length == 1) {
                $scope.QuestionOptions[dropId].accept = false;
                $scope.userHasSelectedOptions = true;
                // $scope.QuestionOptions[dropId].Canvas = [{ 'title': dropItemText, 'drag': true, selectedClass: '' }]; mehul

                $scope.$apply();
            }
        }
        else {
            var items = $('.drag-container').children().removeClass('drop-class-2');
        }

        angular.forEach($scope.droppableItems, function (option, key) {
            var otitle = option.title;
            if (option.title.indexOf('<span math dynamic>') == -1) {
                otitle = '<span math dynamic>' + option.title + '</span>';
            }
            if ($filter('lowercase')(otitle) == $filter('lowercase')('<span math dynamic>' + dropItemText + '</span>')) {
                option.dropId = dropId;
                if (dropId != 'initial') {
                    option.model = $scope.QuestionOptions[dropId];
                }
                else {
                    option.model = '';
                }
            }
        });
        var itemCount = 0;
        angular.forEach($scope.QuestionOptions, function (model, index) {
            itemCount = 0;
            angular.forEach($scope.SelectedTemplateArray, function (option, key) {
                if (option.dropId == index) {
                    itemCount++;
                }
            });
            if (itemCount == 0) {
                model.accept = true;
                $scope.$apply();
            }
        });
    }
    $scope.DragDropOnMatching = function (dropId) {
        if (dropId != 'initial') {
            var input = $scope.SelectedTemplateArray.filter(function (x) { return x.dropId == dropId; });
            if (input.length == 1) {
                $scope.QuestionOptions[dropId].accept = false;
            }
        }
        var itemCount = 0;
        angular.forEach($scope.QuestionOptions, function (model, index) {
            if (model.interaction == 'dragdrop') {
                itemCount = 0;
                angular.forEach($scope.SelectedTemplateArray, function (option, key) {
                    if (option.dropId == index) {
                        itemCount++;
                    }
                });
                if (itemCount == 0) {
                    model.accept = true;
                    //$scope.$apply();
                }
            }
            else {
                model.accept = true;
            }
        });
        $scope.$apply();
    }

    $scope.SetFIBType = function (data) {
        var questiontext = "";//data.QuestionData.textwithblanks.text;
        if (data.QuestionData.textwithblanks != undefined) {
            if (data.QuestionData.textwithblanks.text != undefined && data.QuestionData.textwithblanks.text != "")
                questiontext = data.QuestionData.textwithblanks.text;
        }
        //Array object to hold draggable item values
        $scope.droppableItems = [];
        $scope.FIBText = "";
        var selectedText = '';
        $scope.layoutType = ($scope.isUndefined(data.QuestionData.question_layout)) ? "" : data.QuestionData.question_layout.text;
        $scope.FillDroppableItems(data);
        angular.forEach(data.QuestionData.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            $scope.QuestionOptions[key].key = key;       // Index of option
            $scope.QuestionOptions[key].UserInput = "";  //User input value of textbox and dropdown value.         
            $scope.QuestionOptions[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            $scope.QuestionOptions[key].width = 'auto';
            $scope.QuestionOptions[key].showOptionFeedbackModal = $scope.showOptionFeedbackModal;
            $scope.QuestionOptions[key].showAttempt = $scope.showAttempt;
            $scope.QuestionOptions[key].ShowGlobalFeedback = $scope.ShowGlobalFeedback;
            $scope.QuestionOptions[key].multiSelectItem = [];
            $scope.interaction = option.interaction;
            $scope.QuestionOptions[key].accept = true;
            $scope.QuestionOptions[key].FibClass = "";

            if ($scope.questionTypeId == Enum.QuestionType.LTD || $scope.questionTypeId == Enum.QuestionType.LTDDND) {
                $scope.QuestionOptions[key].XPosition = '';
                $scope.QuestionOptions[key].YPosition = '';
                var positions = option.noforeachblank.split(',');
                if (positions.length == 2) {
                    $scope.QuestionOptions[key].XPosition = positions[0];
                    $scope.QuestionOptions[key].YPosition = positions[1];
                }
            }

            questiontext = questiontext.replace(textToReplace, "<fibvariation ng-model='$parent.QuestionOptions[" + key + "]' parent-ctrl='$parent' user-input='QuestionOptions[" + key + "].UserInput'></fibvariation>");

            $scope.$watch("QuestionOptions[" + key + "].Canvas.length", function (newValue, oldValue) {
                //console.log(newValue);
                $scope.setOptions(null, null);
            });
            //$scope.$watch("QuestionOptions[" + key + "].UserInput", function (newValue, oldValue) {
            //    //console.log(newValue);
            //    $scope.setOptions(null, null);
            //});

            if (data.SelectedOptions) {
                var selectedOption = angular.fromJson(data.SelectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if ((option.interaction == 'textentry' || option.interaction == 'inline') && value.noforeachblank == option.noforeachblank) {
                        $scope.QuestionOptions[key].UserInput = value.inputvalue;
                    }
                    else if (option.interaction == 'dragdrop' && value.noforeachblank == option.noforeachblank) {
                        if (value.inputvalue.indexOf("<span math dynamic>") != -1) {
                            selectedText = value.inputvalue;
                        }
                        else {
                            selectedText = '<span math dynamic>' + value.inputvalue + '</span>'
                        }
                        angular.forEach($scope.droppableItems, function (ditem, dindex) {
                            if ($filter('lowercase')(ditem.title) == $filter('lowercase')(selectedText)) {
                                $scope.droppableItems.splice(dindex, 1);
                                dindex = dindex - 1;
                                $scope.QuestionOptions[key].Canvas = [{ 'title': value.inputvalue, 'drag': true, selectedClass: '' }];
                                $scope.QuestionOptions[key].accept = false;
                            }
                        });
                        angular.forEach($scope.SelectedTemplateArray, function (item, indx) {
                            if (item.title == value.inputvalue) {
                                item.dropId = key;
                            }
                        });
                    }
                    else if (option.interaction == 'container' && value.noforeachblank == option.noforeachblank) {
                        var input = value.inputvalue.map(function (x) { return x.title; });
                        $scope.SelectedValues = '';
                        for (var i = 0; i < $scope.droppableItems.length; i++) {
                            if (input.indexOf($scope.droppableItems[i].title) != -1) {
                                $scope.QuestionOptions[key].Canvas.push({ 'title': $scope.droppableItems[i].title, 'drag': true, selectedClass: '' });
                                $scope.QuestionOptions[key].multiSelectItem.push($($scope.droppableItems[i].title).text());
                                $scope.droppableItems.splice(i, 1);
                                i = i - 1;
                            }
                        }
                        angular.forEach($scope.SelectedTemplateArray, function (item, indx) {
                            angular.forEach(input, function (initem, inndx) {
                                if ($filter('lowercase')(initem) == $filter('lowercase')('<span math dynamic>' + item.title + '</span>')) {
                                    item.dropId = key;
                                }
                            });
                        });
                    }
                });
            }
            if ((option.interaction == 'textentry' || option.interaction == 'inline')) {
                $scope.QuestionOptions[key].FibClass = "fibclass";
            }
        });

        $scope.FIBText = questiontext;
        if (!$scope.isUndefined($scope.layoutType)) {
            if ($scope.layoutType == 'right' || $scope.layoutType == 'vertical') {
                $scope.droptype = "col-sm-9 col-md-9";
                $scope.dragtype = "dragpanel col-sm-3 col-md-3 right-matching";
            }
            else if ($scope.layoutType == 'left') {
                $scope.droptype = "col-sm-9 col-md-9";
                $scope.dragtype = "dragpanel col-sm-3 col-md-3 left-matching";
            }
            else if ($scope.layoutType == 'top') {
                $scope.droptype = "col-sm-12 col-md-12";
                $scope.dragtype = "dragpanel col-sm-12 col-md-12 top-matching";
            }
            else if ($scope.layoutType == 'row') {
                $scope.droptype = "";
                $scope.dragtype = "dragpanel text-center";
            }
            else {
                $scope.dragtype = "dragpanel";
                $scope.droptype = "";
            }
        }
    };

    $scope.SetLTDType = function (data) {
        $scope.droppableItems = [];
        $scope.layoutType = ($scope.isUndefined(data.QuestionData.question_layout)) ? "" : data.QuestionData.question_layout.text;
        //var imagePath = String(data.QuestionData.question_media.media).replace("'\'", "");
        //var regex = /<img.*?src='(.*?)'/;
        //$scope.imageSrc = regex.exec(imagePath)[1];   
        var imgObject = document.createElement('div');
        imgObject.innerHTML = data.QuestionData.question_media.media;
        $scope.imageSrc = $(imgObject).find('img').attr('src');

        $scope.FillDroppableItems(data);
        angular.forEach(data.QuestionData.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            $scope.QuestionOptions[key].key = key;       // Index of option
            $scope.QuestionOptions[key].UserInput = "";  //User input value of textbox and dropdown value.         
            $scope.QuestionOptions[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            $scope.QuestionOptions[key].width = 'auto';
            $scope.QuestionOptions[key].showOptionFeedbackModal = $scope.showOptionFeedbackModal;
            $scope.QuestionOptions[key].showAttempt = $scope.showAttempt;
            $scope.QuestionOptions[key].ShowGlobalFeedback = $scope.ShowGlobalFeedback;
            $scope.QuestionOptions[key].XPosition = '';
            $scope.QuestionOptions[key].YPosition = '';
            $scope.QuestionOptions[key].LTDText = '';

            var positions = option.noforeachblank.split(',');
            if (positions.length == 2) {
                $scope.QuestionOptions[key].XPosition = positions[0];
                $scope.QuestionOptions[key].YPosition = positions[1];
            }
            if (data.SelectedOptions) {
                var selectedOption = angular.fromJson(data.SelectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if ((option.interaction == 'textentry' || option.interaction == 'inline') && value.noforeachblank == option.noforeachblank) {
                        $scope.QuestionOptions[key].UserInput = value.inputvalue;
                    }
                    else if (option.interaction == 'dragdrop' && value.noforeachblank == option.noforeachblank) {
                        angular.forEach($scope.droppableItems, function (ditem, dindex) {
                            if ($filter('lowercase')(ditem.title) == $filter('lowercase')(value.inputvalue)) {
                                //$scope.droppableItems[dindex].title = "";
                                $scope.QuestionOptions[key].Canvas = [{ 'title': value.inputvalue, 'drag': true, selectedClass: '' }];
                                $scope.droppableItems.splice(dindex, 1);
                                dindex = dindex - 1;
                            }
                        });
                    }
                });
            }
        });

        //$scope.Text = questiontext;
        if (!$scope.isUndefined($scope.layoutType)) {
            if ($scope.layoutType == 'right' || $scope.layoutType == 'left' || $scope.layoutType == 'vertical') {
                $scope.droptype = "col-sm-9 col-md-9";
                $scope.dragtype = "dragpanel col-sm-3 col-md-3 right";
            }
            else if ($scope.layoutType == 'row') {
                $scope.droptype = "";
                $scope.dragtype = "dragpanel text-center";
            }
            else {
                $scope.dragtype = "dragpanel";
                $scope.droptype = "";
            }
        }
    };

    $scope.SetAlgorithimicType = function (data) {
        var textReplaced = false;
        if ($scope.isUndefined(data.AlgorithmicVariableArray)) {
            data.AlgorithmicVariableArray = [];
        }
        if (data.QuestionData.stem_details.textforeachvalue.length > 0) {
            angular.forEach(data.QuestionData.stem_details.textforeachvalue, function (textDetail, key) {
                if (textDetail.type == 'random') {
                    if (data.AlgorithmicVariableArray.length == angular.fromJson(data.QuestionData.stem_details.textforeachvalue).length) {
                        angular.forEach(data.AlgorithmicVariableArray, function (arrayVariable, akey) {
                            $scope.questionName = $scope.questionName.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        });
                    }
                    else {
                        var random = Math.floor((Math.random() * textDetail.maxvalue) + textDetail.minvalue);
                        $scope.questionName = $scope.questionName.replace(textDetail.text, random);
                        data.AlgorithmicVariableArray.push({ "TextToReplace": textDetail.text, "TextValue": random });
                    }
                }
            });

            angular.forEach(data.QuestionData.choices, function (option, key) {
                angular.forEach(data.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    if (isNaN(option.text) && option.text.indexOf(arrayVariable.TextToReplace) != -1) {
                        option.text = option.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        textReplaced = true;
                    }
                });
                if (data.QuestionData.stem_details.display == 'result' && textReplaced) {
                    option.text = eval(option.text);
                }
            });
        }
    };

    $scope.Shuffle = function (array) {
        var currentIndex = array.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }
        return array;
    };

    $scope.FillDroppableItems = function (data) {
        isDistractorsPushDone = false;
        $scope.DistractorItems = [];
        $scope.SelectedTemplateArray = [];
        //data.QuestionData.choices = $scope.Shuffle(data.QuestionData.choices);
        angular.forEach(data.QuestionData.choices, function (option, key) {
            $scope.QuestionOptions[key].arrList = [];
            if (option.interaction == "inline") {
                angular.forEach(option.distractorforeachblank, function (titem, tindex) {
                    if (titem.text && titem.text.trim().length > 0)
                        $scope.QuestionOptions[key].arrList.push('<span math dynamic>' + $('<div/>').html(titem.text).text() + '</span>');
                });
            }

            if (option.interaction == "dragdrop" || option.interaction == "container") {
                if (isDistractorsPushDone == false) {
                    angular.forEach(data.QuestionData.distractors, function (titem, tindex) {
                        if (titem.text && titem.text.trim().length > 0) {
                            $scope.QuestionOptions[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                            $scope.DistractorItems.push({ id: tindex + 1, title: titem.text });
                            $scope.SelectedTemplateArray.push({ dropId: 'initial', title: $('<div/>').html(titem.text).text() });
                            //$scope.SelectedTemplateArray.push({ dropId: 'initial', title: titem.text });
                        }
                    });
                }
                isDistractorsPushDone = true;
            }

            angular.forEach(option.textforeachblank, function (titem, tindex) {
                if (titem.text) {
                    $scope.QuestionOptions[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                    $scope.DistractorItems.push({ id: tindex + 1, title: titem.text });
                    $scope.SelectedTemplateArray.push({ dropId: 'initial', title: $('<div/>').html(titem.text).text() });
                    //$scope.SelectedTemplateArray.push({ dropId: 'initial', title: titem.text });
                }
            });


            if (option.interaction == "dragdrop" || option.interaction == "container") {
                angular.forEach($scope.QuestionOptions[key].arrList, function (item, index) {
                    $scope.droppableItems.push({ 'title': item, 'drag': true, selectedClass: '' });
                });
                $scope.QuestionOptions[key].Canvas = [];
                $scope.QuestionOptions[key].Drop = true;
            }
        });
    };

    //$scope.SetItemToDropCanvasList = function (index) {
    //    if ($window.innerWidth <= 767) {
    //        if ($scope.QuestionOptions[index].Canvas.length == 0) {
    //            $scope.DropCanvasListIndex = index;
    //        }
    //    }
    //};

    /*Changes for Composite Question Type*/
    $scope.SetItemToDropCanvasListComposite = function (index, dropItems) {
        $scope.DropCanvasListIndex = index;
        $scope.droppableItems = dropItems;
    }

    $scope.AddItemToDropCanvasListComposite = function (item, index) {
        if ($scope.DropCanvasListIndex != null) {
            if ($scope.CurrentQuestion.QuestionData.questions[item.QuestionIndex].choices[$scope.DropCanvasListIndex].Canvas.length == 0) {
                $scope.CurrentQuestion.QuestionData.questions[item.QuestionIndex].choices[$scope.DropCanvasListIndex].Canvas.push({ 'title': item.title, 'drag': true, selectedClass: 'header' });
                $scope.droppableItems.splice(index, 1);
                $scope.DropCanvasListIndex = null;
                $scope.userHasSelectedOptions = true;
            }
        }
    };
    /*Changes for Composite Question Type*/

    $scope.SetItemToDropCanvasList = function (option) {
        if ($window.innerWidth <= 767) {
            if ($scope.QuestionOptions[option.key].Canvas.length == 0) {
                $scope.DropCanvasListIndex = option.key;
            }
        }
    };

    $scope.AddItemToDropCanvasList = function (item, index) {
        if ($scope.DropCanvasListIndex != null) {
            if ($scope.QuestionOptions[$scope.DropCanvasListIndex].Canvas.length == 0) {
                $scope.QuestionOptions[$scope.DropCanvasListIndex].Canvas.push({ 'title': item.title, 'drag': true, selectedClass: 'header' });
                $scope.droppableItems.splice(index, 1);
                $scope.DropCanvasListIndex = null;
                $scope.userHasSelectedOptions = true;
                //if ($window.innerWidth <= 767) {
                //    $scope.dropFunction(index, $('<div/>').html(item.title).text());
                //}
            }
        }
    };

    $scope.AddItemToDropItemList = function (item, key, index) {
        if ($window.innerWidth <= 767 && $('#checkAnswerbtn').html() == 'Check Answer') {
            if ($scope.QuestionOptions[key].Canvas.length == 1) {
                $scope.droppableItems.push({ 'title': item.title, 'drag': true, selectedClass: '' });
                $scope.QuestionOptions[key].Canvas.splice(index, 1);
                $scope.DropCanvasListIndex = key;
                $scope.userHasSelectedOptions = true;
                //if ($window.innerWidth <= 767) {
                //    $scope.dropFunction('initial', $('<div/>').html(item.title).text());
                //}
            }
        }
    };

    $scope.OnDragStart = function (option) {
        option.AnswerClass = "";
    };

    $scope.AddDropCssOnOver = function (key) {
        $('#divDropOption' + key).addClass('active');
        //$scope.QuestionOptions[key].AnswerClass = '';
        angular.forEach($scope.QuestionOptions[key].Canvas, function (option, key) {
            option.AnswerClass = '';
        });
        return true;
    };

    $scope.RemoveDropCssOnLeave = function () {
        //$('#divDOption' + key).removeClass('active');
        angular.forEach($scope.QuestionOptions, function (option, key) {
            $('#divDropOption' + key).removeClass('active');
        });
    };

    $scope.AddCssOnOver = function (item) {
        item.selectedClass = 'dragging';
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveSelected = function () {
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveCssOnOut = function () {
        angular.forEach($scope.droppableItems, function (item, key) {
            item.selectedClass = '';
        });
    };

    $scope.setQuestionOptions = function (data) {
        $scope.text = '';
        $scope.showOptionFeedbackModal = false;
        $('.fdback').addClass('ng-hide');
        $(data.QuestionData.choices).each(function (index) {
            data.QuestionData.choices[index].currentClass = '';

        });
        if ($scope.questionTypeId == Enum.QuestionType.ChoiceMatrix) {
            var selectedOpt = angular.fromJson(data.SelectedOptions);
            if (data.SelectedOptions) {
                
                $(selectedOpt).each(function (selectindex, selectvalue) {
                    var inputValues = selectvalue.inputvalue.split("-");
                    $(data.QuestionData.choices).each(function (index) {
                        var optionindex = index + 1;
                        $(data.QuestionData.choices[index].suboption).each(function (index1, value1) {
                            if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                                data.QuestionData.choices[index].suboption[index1].isAnswered = true;
                                data.QuestionData.choices[index].suboption[index1].currentClass = '';
                            }
                        });

                    });
                });

            }

        }
        else if ($scope.questionTypeId == Enum.QuestionType.HotSpot) {
            if (data.SelectedOptions != "[]" && data.SelectedOptions != "" && data.SelectedOptions != null) {
                var selectedOpt = angular.fromJson(data.SelectedOptions);
                $scope.UserAnswerText = selectedOpt[0].inputvalue;
            }
        }
        else if ($scope.questionTypeId == Enum.QuestionType.Essay) {

            if (data.SelectedOptions) {
                //$scope.essayTemplateText = data.SelectedOptions;
                if (data.QuestionData.toolbar.type == "math_3-5_palette" || data.QuestionData.toolbar.type == "math_6-8_palette") {
                    if ($scope.operationmode == Enum.OperationMode.Offline) {
                        mathEditorPrevLatex = $.trim(data.SelectedOptions[0].replace("?", ""));
                        mathEditorType = data.QuestionData.toolbar.type;
                    }
                    else {
                        mathEditorPrevLatex = $.trim(data.SelectedOptions.replace("?", ""));
                        mathEditorType = data.QuestionData.toolbar.type;
                    }
                } else {
                    $("#txtEssayContent").val(data.SelectedOptions);
                    $scope.text = data.SelectedOptions;
                }
            }
        }
        else {
            $(data.QuestionData.choices).each(function (index) {
                //data.QuestionData.choices[index].isAnswered = false;
                //if (!$scope.isUndefined(data.QuestionData.image) && data.QuestionData.image.image.indexOf('http') === -1 && data.QuestionData.image.image != "") {
                //    data.QuestionData.choices[index].image = "Content/images/" + data.QuestionData.choices[index].image;
                //}
                var optionindex = index + 1;
                data.QuestionData.choices[index].CurrentOptionIndex = optionindex;
                var selectedoptions = $.parseJSON(data.SelectedOptions);
                if (selectedoptions != null && selectedoptions.length > 0) {
                    var selectedoptions = $.parseJSON(data.SelectedOptions);
                    $(selectedoptions).each(function (selectindex) {
                        data.QuestionData.choices[index].SelectedOptions = parseInt(selectedoptions[selectindex].choiceid);
                        if (optionindex == parseInt(selectedoptions[selectindex].choiceid)) {
                            data.QuestionData.choices[index].isAnswered = true;
                        }
                        if (!$scope.isUndefined(data.QuestionData.question_interaction) && data.QuestionData.question_interaction.text == 'selectable') {
                            if (data.QuestionData.choices[index].choiceid == parseInt(selectedoptions[selectindex].choiceid))
                                data.QuestionData.choices[index].currentClass = "selected";
                        }
                    });
                }
                else
                    $scope.userHasSelectedOptions = false;
            });
        }
        return data;
    }

    $scope.DrawHotspot = function (event) {
        var option = $(this).attr("option");
        // $('area').mapster('deselect');       

        //var key='TX3';
        //$('area[data-State=' + key + ']').mapster({ strokeColor: '0C3BAD' });
        //$('area').mapster('deselect');        
        //$('.hotspotStamp').remove();
        ////var box = $("<div>").addClass('hotspotStamp').addClass('hotspotBorder').css({ 'background-color': 'red', 'opacity': '0.5' });
        //var imageWidth = parseInt($($("#mediaImage")).css('width'));
        //var imageHeight = parseInt($($("#mediaImage")).css('height'));
        //var posX = event.pageX - $($("#mediaImage")).offset().left;
        //var posY = event.pageY - $($("#mediaImage")).offset().top;
        //var left = (posX / imageWidth) * 100;
        //var top = (posY / imageHeight) * 100;
        //var coords = { left: left, top: top };
        //$(box).css({ 'left': (coords.left - 2) + "%", 'top': (coords.top - 2) + "%", 'position': 'absolute' }).appendTo('#hotspotContainer');
        //if ($(event.target).hasClass('hotspotTag')) {
        //    $scope.selectedHotspot = true;
        //    //$scope.UserAnswerText = $(box).get(0).outerHTML;
        //    $scope.UserSelectedArea.push(option.choiceid);
        //}
        //else {
        //    $scope.selectedHotspot = false;
        //    var index = $scope.UserSelectedArea.indexOf('TX' + option.choiceid);
        //    if (index != -1) {
        //        $scope.UserSelectedArea.splice(index, 1);
        //    }
        //    //$scope.UserAnswerText = $(box).get(0).outerHTML;
        //}
        if (!$scope.isUndefined(option)) {
            var index = $scope.UserSelectedArea.indexOf(option.choiceid);
            if (index > -1) {
                $scope.UserSelectedArea.splice(index, 1);
            }
            else {
                $scope.UserSelectedArea.push(option.choiceid);
            }
        }
        //    if ($(event.target).hasClass('hotspotTag')) {
        //        $scope.selectedHotspot = true;
        //        //$scope.UserAnswerText = $(box).get(0).outerHTML;
        //        $scope.UserSelectedArea.push(option.choiceid);
        //    }
        //else {
        //        $scope.selectedHotspot = false;
        //    var index = $scope.UserSelectedArea.indexOf('TX' + option.choiceid);
        //    if (index != -1) {
        //        $scope.UserSelectedArea.splice(index, 1);
        //    }
        //    //$scope.UserAnswerText = $(box).get(0).outerHTML;
        //}
    }


    $scopeserializeData = function (data) {
        // If this is not an object, defer to native stringification.
        if (!angular.isObject(data)) {
            return ((data == null) ? "" : data.toString());
        }
        var buffer = [];

        // Serialize each key in the object.
        for (var name in data) {
            if (!data.hasOwnProperty(name)) {
                continue;
            }

            var value = data[name];
            buffer.push(
                encodeURIComponent(name) +
                "=" +
                encodeURIComponent((value == null) ? "" : value)
            );
        }

        // Serialize the buffer and clean it up for transportation.
        var source = buffer
            .join("&")
            .replace(/%20/g, "+")
        ;
        return (source);
    };

    $scope.getProgressPercentage = function () {
        if ($scope.currentPage > 0) {
            var totalQuestions = $scope.AssignmentQuestions.filter(function (x) { return (x.QuestionStatus == 'answer-visited' || x.QuestionStatus == 'answer-right'); });
            $scope.totalAtemptedQuestions = totalQuestions.length + 1;
            $scope.Progress = [];
            $scope.Progress.ProgressPercentage = (($scope.currentPage * 100) / $scope.totalItems).toFixed(2);
            $scope.Progress.CurrentQuestion = $scope.currentPage;
            if ($scope.operationmode == Enum.OperationMode.Offline) {
                $scope.Progress.ProgressPercentage = (($scope.totalAtemptedQuestions * 100) / $scope.offline.quiz.length).toFixed(2);
                $scope.Progress.TotalQuestions = $scope.offline.quiz.length;
            }
            else {
                $scope.Progress.ProgressPercentage = (($scope.currentPage * 100) / $scope.totalItems).toFixed(2);
                $scope.Progress.TotalQuestions = $scope.totalItems;
            }
        }
    }
    $scope.getProgressTotal = function () {
        return $scope.CurrentQuestion.TotalQuestions;
    }

    $scope.PassiveTimer = (function () {
        var time = 0;
        var timerToken;
        var startTimer = function (startTime) {
            time = startTime;
            timerToken = setInterval(function () {
                time = time + 1;
            }, 1000);
        }
        var stopTimer = function () {
            clearInterval(timerToken);
            return time;
        }

        return {
            startTimer: startTimer,
            stopTimer: stopTimer
        };
    })();

    $scope.ActiveTimer = function (TimerSetting, CurrentQuestion) {
        $scope.totaltimemoving = TimerSetting.Minute * 60;
        $scope.totaltime = (TimerSetting.Minute * 60).toHHMMSS();

        $timeout(function () {
            $scope.$broadcast('add-timer');
        });
    }

    $scope.startTimer = function () {
        if (!timeStarted) {
            $scope.$broadcast('timer-start');
            $scope.timerRunning = true;
            timeStarted = true
        } else if ((timeStarted) && (!$scope.timerRunning)) {
            $scope.isPaused = false;

            $scope.$broadcast('timer-resume');
            $scope.timerRunning = true;
        }
    };

    $scope.stopTimer = function () {
        if ((timeStarted) && ($scope.timerRunning)) {
            $scope.isPaused = true;

            $scope.$broadcast('timer-stop');
            $scope.timerRunning = false;
        }
    };

    $scope.$on('timer-stopped', function (event, data) {
        timeStarted = true;
    });

    $scope.callbackTimer.finished = function () {
        SweetAlert.swal("Alert!", Messages.TimeExpiredAssignSubmit, "warning");
        $scope.autoSubmit();
    };

    Number.prototype.toHHMMSS = function () {
        var sec_num = parseInt(this, 10); // don't forget the second parm
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        if (hours < 10) { hours = "0" + hours; }
        if (minutes < 10) { minutes = "0" + minutes; }
        if (seconds < 10) { seconds = "0" + seconds; }
        var time = hours + ':' + minutes + ':' + seconds;
        return time;
    }

    $scope.TimerClass = (function () {
        Number.prototype.toHHMMSS = function () {
            var sec_num = parseInt(this, 10); // don't forget the second parm
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) { hours = "0" + hours; }
            if (minutes < 10) { minutes = "0" + minutes; }
            if (seconds < 10) { seconds = "0" + seconds; }
            var time = hours + ':' + minutes + ':' + seconds;
            return time;
        }
        var secondCounter = 0;
        var startTime = 0, totalSeconds = 0, threshold = 0;
        var timerToken;
        var initializeTimer = function (startTime, totSeconds, callback) {
            secondCounter = totSeconds - startTime;
            startTime = startTime;
            totalSeconds = totSeconds;
            setThreshold();
            var currentDisplayTime = (totalSeconds * 1000) - (startTime * 1000);
            timerToken = setInterval(function () {
                secondCounter = secondCounter - 1;
                if (secondCounter == threshold) {
                    //$("#timer").css('color', 'red');
                }
                $scope.totaltimemoving = secondCounter.toHHMMSS();

                $scope.$apply();
            }, 1000);
            setTimeout(function () { callback.call(totalSeconds); }, currentDisplayTime);
            $scope.totaltime = secondCounter.toHHMMSS();
        }
        var setThreshold = function () {
            threshold = totalSeconds - Math.round((totalSeconds * 80) / 100);
        }
        var stopTime = function (callback) {
            clearInterval(timerToken);
            var pendingTime = totalSeconds - secondCounter;
            callback.call(pendingTime);
        }
        return {
            startTimer: initializeTimer,
            stopTimer: stopTime
        };
    })();

    $scope.mathCommand = function (val, opr) {

        if ($.trim($('#mathQuill').html()).length == 0) {
            $('#mathQuill').mathquill('editable');
            $('#mathQuill').width($('.math-editor').width());
            $('#mathQuill').height('auto');
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').mathquill('showcursor');
        }

        $scope.mathEditorPrevLatex = $('#mathQuill').mathquill('latex');

        $('#mathQuill').mathquill(opr, val);
        $('#mathQuill').focus();

        if (val === '+' || val === '-' || val === '÷' || val === '=' || val === 'times' || val === '<' || val === '>') {
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').focus();
        }

        $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');

        //$('#codecogsimg').attr('src', 'http://latex.codecogs.com/gif.latex?' + $scope.mathEditorLatex);
    };


    $scope.mathKeyCommand = function (val, opr) {

        if ($.trim($('#mathQuill').html()).length == 0) {
            $('#mathQuill').mathquill('editable');
            $('#mathQuill').width($('.math-editor').width());
            $('#mathQuill').height('auto');
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').mathquill('showcursor');
        }

        $scope.mathEditorPrevLatex = $('#mathQuill').mathquill('latex');

        $('#mathQuill').mathquill(opr, val);


        if (val === '+' || val === '-' || val === '÷' || val === '=' || val === 'times' || val === '<' || val === '>') {
            $('#mathQuill').mathquill('cmd', 'emptyspace');
        }

        if (val === '\sqrt[3]') {
            $('#mathQuill').mathquill('cmd', 'emptyspace');
            $('#mathQuill').mathquill('cmd', 'emptyspace');
        }

        $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');
        $('#mathQuill textarea').focus();

    };



    $scope.updateDataMathEditor = function (e) {

        //if (event.which == 42 || event.which == 43 || event.which == 45 || event.which == 61)
        //{
        //    $('#mathQuill').mathquill('cmd', 'emptyspace');
        //    $('#mathQuill').focus();
        //}

        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(37); //left
        specialKeys.push(38); //up
        specialKeys.push(39); //right
        specialKeys.push(40); //down

        // Added for the operators
        specialKeys.push(42); //*
        specialKeys.push(43); //+
        specialKeys.push(47); //÷
        specialKeys.push(45); //-
        specialKeys.push(61); //=

        var keyCode = e.which ? e.which : e.keyCode
        var result = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 97 && keyCode <= 122) || (keyCode >= 64 && keyCode <= 91) || specialKeys.indexOf(keyCode) != -1);
        if (!result) {
            e.preventDefault();
            return false;
        } else {

            if (specialKeys.indexOf(keyCode) != -1) {
                if (keyCode == 42) {
                    $scope.mathCommand('times', 'cmd');
                } else if (keyCode == 43) {
                    $scope.mathCommand('+', 'write');
                } else if (keyCode == 47) {
                    $scope.mathCommand('÷', 'write');
                } else if (keyCode == 45) {
                    $scope.mathCommand('-', 'write');
                } else if (keyCode == 61) {
                    $scope.mathCommand('=', 'write');
                }
                e.preventDefault();
                return false;
            }

            $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');

        }


    };

    $scope.mathRedoCommand = function () {
        $('#mathQuill').mathquill('editable');
        $('#mathQuill').width($('.math-editor').width());
        $('#mathQuill').height('auto');
        $('#mathQuill').mathquill('cmd', 'emptyspace');
        $('#mathQuill').mathquill('latex', $scope.mathEditorLatex);
        $('#mathQuill textarea').focus();
    };

    $scope.mathDeleteCommand = function () {
        $('#mathQuill').mathquill('editable');
        $('#mathQuill').width($('.math-editor').width());
        $('#mathQuill').height('auto');
        $('#mathQuill').mathquill('cmd', 'emptyspace');
        $('#mathQuill').mathquill('latex', "");
        $('#mathQuill textarea').focus();
    };

    $scope.mathUndoCommand = function () {
        $scope.mathEditorLatex = $('#mathQuill').mathquill('latex');
        $('#mathQuill').mathquill('editable');
        $('#mathQuill').width($('.math-editor').width());
        $('#mathQuill').height('auto');
        $('#mathQuill').mathquill('cmd', 'emptyspace');
        $('#mathQuill').mathquill('latex', $scope.mathEditorPrevLatex);
        $('#mathQuill textarea').focus();
    };

    $scope.mathEditorRefactor = function () {
        // removing the unwanted duplicate cursor from matheditor
        if ($('#mathQuill .cursor').length > 1) {
            $('#mathQuill .cursor:first').remove();
        }

        if ($('#mathQuill').mathquill('latex') == "{ }") {
            $('#mathQuill').mathquill('8', 'keyStroke');
        }
    };

    $scope.getQuizDetails();
    $scope.OpenQuestionMap = function () {
        if ($('.main-body-block').hasClass('visible')) {
            closeNav();
        } else {
            $(this).addClass('visible');
            $('.main-nav').addClass('visible');
            $('.main-body-block').addClass('visible');
            $('body').addClass('overflow-hidden');

        }
        if ($('#myDropDown').hasClass('open')) {
            if (!$('#liQuestionMap').hasClass('active')) {
                $('#liQuestionMap').addClass('active');
                $('#liBookMark').removeClass('active');
                $('#Map').addClass('active');
                $('#Bookmarks').removeClass('active');
                $scope.QuestionMapBookMarkClicked = false;
                $scope.searchFilter = { QuestionStatus: '' };
            }
        }
    }

    function closeNav() {
        $('.toc-list-dropdown, .navoverlay').removeClass('visible');
        $('.main-nav').removeClass('visible');
        $('.main-body-block').removeClass('visible');
        $('body').removeClass('overflow-hidden');

    }

    //window.loadDone = function () {
    //    console.log('loadDone');
    //    //document.getElementById('compositeIframe').contentWindow.angular.element('#quizContainer').hide();
    //    var data = { "quiz": $scope.compositeQuestions, "settings": $scope.AssignmentSetting };
    //    $timeout(function () {
    //        loadiFrameWithInterval(data);
    //    });
    //}

    //window.loadiFrameWithInterval = function (data) {
    //    console.log('loadiFrameWithInterval');
    //    document.getElementById('compositeIframe').contentWindow.angular.element('.container-main').scope().startQuiz(data, true);
    //    document.getElementById('compositeIframe').contentWindow.angular.element('.container-main').scope().$apply();
    //    //document.getElementById('compositeIframe').contentWindow.angular.element('#quizContainer').show();
    //}
}])
    .directive('bindTimer', function ($compile) {
        return {
            restrict: 'A',
            link: function ($scope, el) {
                $scope.$on('add-timer', function (e) {
                    var timerHtml = '<timer autostart="false" interval="1000" countdown="' + $scope.totaltimemoving + '" finish-callback="callbackTimer.finished()"> {{hours}}:{{minutes}}:{{seconds}}</timer>';
                    var compiledHtml = $compile(timerHtml)($scope);
                    el.replaceWith(compiledHtml);
                    $scope.startTimer();
                })
            }
        }
    });


function resize_fun() {
    var window_height = $(window).innerHeight();
    var header_height = $(".main-header-block").innerHeight();
    var footer_height = $(".main-footer-block").innerHeight();
    var r_header_height = $(".main-nav-container .nav-tabs").innerHeight();
    var r_footer_height = $(".foter-navs").innerHeight();
    var body_height = window_height - (header_height + footer_height);
    var right_tab_height = body_height - (r_header_height + r_footer_height);
    $(".main-body-block").height(body_height - 1);
    $(".main-nav, .main-body-block>.container").height(body_height - 1);
    $(".main-nav, .main-body-block>.composite_container").height(body_height - 1);

    $(".main-nav-container .tab-content").height(right_tab_height - 2);

    $("#CompositeQuestionBody").css({ "max-height": (body_height - 140), "overflow-y": "auto" });
    $("#CompositeQuestionBodyContent").css({ "max-height": (body_height - 2), "overflow-y": "auto" });
}


//$(document).ready(function () {
//    resize_fun();
//});
//$(window).resize(function () {
//    resize_fun();
//})
//window.onorientationchange = function () {
//    resize_fun();
//}




