app.directive("algorithmic", function () {
    return {
        restrict: 'EA',
        $scope: {
            algorithmic: '@'
        },
        controller: ["$scope", "$element", "$attrs", function ($scope, $elem, $attrs) {
			if($scope.$parent.CurrentQuestion.SelectedOptions == null)
			{
				if($attrs.algorithmic.indexOf('rand') > -1)
				{
					var min = $attrs.algorithmic.split(',')[0].split('(')[1];
					var max = $attrs.algorithmic.split(',')[1].split(')')[0];
					var random = Math.floor((Math.random() * max) + min);
					if($scope.$parent.CurrentQuestion.AlgorithmicVariableArray == undefined)
					{
						$scope.$parent.CurrentQuestion.AlgorithmicVariableArray = {};
						$scope.$parent.CurrentQuestion.AlgorithmicVariableArray[$elem.attr('id')] = random;
					}
					else
					{
						$scope.$parent.CurrentQuestion.AlgorithmicVariableArray[$elem.attr('id')] = random;
					}
					$elem.html(random);
				}
				else
				{
					optionValues($elem);
				}
			}
			else
			{
				if($attrs.algorithmic.indexOf('rand') > -1)
				{
					$elem.html($scope.$parent.CurrentQuestion.AlgorithmicVariableArray[$elem.attr('id')]);
				}
				else
				{
					optionValues($elem);
				}
			}
			
			function optionValues(elem)
			{
				if ($elem.html().indexOf('[[') > -1)
				{
					$.each($elem.children(), function (index, value) {
						var spanId = $(value).html();
						$elem.html($elem.html().replace('[[', ''));
						$elem.html($elem.html().replace(']]', ''));
						$elem.html($elem.html().replace('<span>' + spanId + '</span>', parseInt($(spanId).html())));
					});
					$elem.html(eval($elem.html()));
				}
				else
				{
					$.each($elem.children(), function (index, value) {
						var spanId = $(value).html();
						$elem.html($elem.html().replace(spanId, $(spanId).html()));
					});
				}
			}
        }]
    };
});