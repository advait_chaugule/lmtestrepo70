﻿app.directive('imageonload', function ($timeout) {
    return {
        restrict: 'AE',
        link: function ($scope, element, attrs) {
            element.bind('load', function () {
                $('#'+attrs.id).mapster({                   
                    fillOpacity: 0.4,
                    fillColor: "d42e16",
                    stroke: true,
                    strokeColor: "3320FF",
                    strokeOpacity: 0.8,
                    strokeWidth: 4,
                    clickNavigate: true,
                    highlight: true,
                    isSelectable: true,
                    singleSelect:false,
                    mapKey: 'data-state'
                    //render_select: {
                    //    fill: false,
                    //    fillColor: 'ff0000',
                    //    stroke: false,
                    //    strokeColor: '00ff00',
                    //    strokeWidth: 2
                    //},
                    //areas: [{
                    //    key: 'really-cold',
                    //    render_select: {
                    //        fillColor: '0000ff'
                    //    }
                    //},
                    //{
                    //    key: 'new-england',
                    //    render_select: {
                    //        fillColor: '44ff44'
                    //    }
                    //}
                    //]
                });
            });

            //$timeout(function () {
            //    if ($scope.UserAnswerText) {
            //        $('#hotspotContainer').append($scope.UserAnswerText);
            //    }
            //});
        }
       

    };
});