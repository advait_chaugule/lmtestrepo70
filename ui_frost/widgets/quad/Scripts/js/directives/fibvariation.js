﻿app.directive('fibvariation', ["$rootScope", function ($rootScope) {
    return {
        restrict: 'E',
        require: 'ngModel',
        scope: {
            model: "=ngModel",
            UserInput: "=userInput",
            ParentCtrl: "=parentCtrl"
        },
        replace: true,
        templateUrl: 'FIBdirective.html',
        link: function (scope, element, attrs, ngModel) {
            scope.OnDragStart = function (option) {
                option.AnswerClass = "";
            },
            scope.CheckInputValue = function (e) {
                var keyCode = e.which ? e.which : e.keyCode;
                ngModel.$modelValue.AnswerClass = "";
                var decimalplaces = (ngModel.$modelValue.decimalplace == "") ? 0 : ngModel.$modelValue.decimalplace;
                if (!scope.ValidateUserInput(keyCode, ngModel.$modelValue.inputtype, decimalplaces)) {
                    e.preventDefault();
                    return false;
                }else
                {
                    $rootScope.$broadcast("textEntered", {});
                }
                //if (ngModel.$modelValue.UserInput != "") {
                //    $rootScope.$broadcast("textEntered", {});
                //}
            },
            scope.SetItemToDropCanvasList = function (index) {
                scope.ParentCtrl.SetItemToDropCanvasList(index);
            },
            scope.AddItemToDropItemList = function (item, key, index) {
                scope.ParentCtrl.AddItemToDropItemList(item, key, index);
            },
             scope.RemoveSelected = function () {
                 var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
                 if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
             },
            scope.showOptionFeedback = function (ParentCtrl, optionId) {
                ParentCtrl.showOptionFeedback(optionId);
            },
            scope.CheckSelectedValue = function (value) {
                ngModel.$modelValue.UserInput = value;
                ngModel.$modelValue.AnswerClass = "";
                $rootScope.$broadcast("textEntered", {});
            },            
            scope.GetFeedback = function (model) {                
                var feedbackText = '';
                popovermodel = '';
                $('body').popover("destroy");

                popovermodel = model;
                //$('#spnltd' + model.key).popover({
                //    trigger: 'click',
                //    html: true,
                //    content: feedbackText,//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                //    placement: "top"
                //});
                $('body').popover({
                    trigger: 'click',
                    html: true,
                    selector: '.fa-commenting-o',
                    content: function () {
                        if (popovermodel.AnswerClass == "correct") {
                            feedbackText = popovermodel.correct_feedback;
                        }
                        else {
                            feedbackText = popovermodel.incorrect_feedback;
                        }
                        //return feedbackText;
                        if (popovermodel.QuestionIndex > 0) {
                            $('.fdback-mobile #divltd' + popovermodel.QuestionIndex + popovermodel.key).html('<span>' + (popovermodel.key + 1) + ". " + feedbackText + '</span>');
                        }
                        else {
                            $('.fdback-mobile #divltd' + popovermodel.key).html('<span>' + (popovermodel.key + 1) + ". " + feedbackText + '</span>');
                        }
                        if ($('.fdback-mobile').is(':visible')) {
                            //$('.fdback-mobile #divltd' + popovermodel.key).html('<span>' + (popovermodel.key + 1) + ". " + feedbackText + '</span>');
                            if (popovermodel.QuestionIndex > 0) {
                                $('.fdback-mobile #divltd' + popovermodel.QuestionIndex + popovermodel.key).show();
                            }
                            else {
                                $('.fdback-mobile #divltd' + popovermodel.key).show();
                            }
                        }
                        else {
                            return feedbackText;
                        }
                    },//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                    placement: "top"
                });
            },
            scope.CheckDropItem = function (event, index, item, type) {
                if (item.QuestionIndex != index) {
                    return false;
                }
                else {
                    return item;
                }
            },
            scope.ValidateUserInput = function (keyCode, inputtype, maxDecimals) {
                var resultType = false;
                var regexp = "";
                var specialKeys = new Array();
                specialKeys.push(8); //Backspace
                specialKeys.push(37); //left
                specialKeys.push(38); //up
                specialKeys.push(39); //right
                specialKeys.push(40); //down
                specialKeys.push(9); //Tab key
                specialKeys.push(32);//space bar
                specialKeys.push(126);//~
                specialKeys.push(33);//!
                specialKeys.push(64);//@
                specialKeys.push(35);//#
                specialKeys.push(36);//$
                specialKeys.push(37);//%
                specialKeys.push(94);//^
                specialKeys.push(38);//&
                specialKeys.push(42);//*
                specialKeys.push(40);//(
                specialKeys.push(41);//)
                specialKeys.push(45);//-
                specialKeys.push(43);//+
                specialKeys.push(61);//=
                specialKeys.push(60);//<
                specialKeys.push(44);//,
                specialKeys.push(62);//>
                specialKeys.push(46);//.
                specialKeys.push(123);//{
                specialKeys.push(125);//}
                specialKeys.push(91);//[
                specialKeys.push(93);//]
                specialKeys.push(95);//_
                specialKeys.push(47);///
                specialKeys.push(92);//\
                specialKeys.push(59);//;
                specialKeys.push(58);//:
                specialKeys.push(96);//`
                specialKeys.push(63);//?
                specialKeys.push(58);//:
                specialKeys.push(124);//|
                specialKeys.push(34);//"
                switch (inputtype) {
                    case 'alphanumeric':
                        if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                            resultType = true;
                        break;
                    case 'numeric':
                        if (maxDecimals > 0) {
                            specialKeys.push(46); //decimal
                            regexp = "^-?\\d*\\.?\\d{0," + maxDecimals + "}$";
                            if (ngModel.$modelValue.UserInput != "" && decimalPlaces(ngModel.$modelValue.UserInput) > maxDecimals) {
                                return false;
                            }
                        } else {
                            regexp = "^-?\\d*$";
                        }
                        if ((keyCode >= 48 && keyCode <= 57) || (specialKeys.indexOf(keyCode) != -1)) {
                            if (ngModel.$modelValue.UserInput.indexOf('.') > -1 && keyCode == 46) {
                                return false;
                            }
                            else if (maxDecimals > 0 && keyCode >= 48 && keyCode <= 57 && decimalPlaces(ngModel.$modelValue.UserInput) == maxDecimals) {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                            //var validRegex = new RegExp(regexp);
                            //resultType = validRegex.test(ngModel.$modelValue.UserInput);
                        }
                        break;
                    case 'alphabet':
                        if ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                            resultType = true;
                        break;
                    default:
                        break;
                }
                return resultType;
            }
            function decimalPlaces(num) {
                var match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                if (!match) { return 0; }
                return Math.max(
                     0,
                     // Number of digits right of decimal point.
                     (match[1] ? match[1].length : 0)
                     // Adjust for scientific notation.
                     - (match[2] ? +match[2] : 0));
            }
        }
    }

}]);
