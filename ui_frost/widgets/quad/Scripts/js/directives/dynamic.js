﻿app.directive('dynamic', function ($compile) {
    return {
        restrict: 'A',
        replace: true,
        scope: { dynamic: '=dynamic' },
        link: function (scope, ele, attrs) {
            scope.$watch('dynamic', function (html) {               
                ele.html(scope.dynamic);
                $compile(ele.contents())(scope);
            });
        }
    };
});