﻿app.directive("math", function () {
    return {
        restrict: 'EA',
        $scope: {
            math: '@'
        },
        controller: ["$scope", "$element", "$attrs", function ($scope, $elem, $attrs) {
            $scope.$watch($attrs.math, function (value) {
                if (!value) return;
                $elem.html(value);
				//MathJax.Hub.Queue(["Typeset", MathJax.Hub, $elem[0]]);
                MathJax.Hub.Queue(["Typeset", MathJax.Hub, value]);
            });
        }]
    };
});