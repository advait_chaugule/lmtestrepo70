/**
 * Created by debayan.das on 1/18/2016.
 */

//(function (module) {
app.directive("draggable", [
        function () {
            return {
                restrict: "A",
                replace: false,
                scope: {},
                link: function (scope, elm, attr) {
                    scope.init = function () {
                        scope.dragId = attr["dragId"];
                        scope.isDropped = false;
                        scope.cloneDrop = parseInt(attr["cloneDrop"] || "1", 10);
                        elm.attr("data-clone-drop", scope.cloneDrop);
                        elm.attr("data-max-clone-drop", scope.cloneDrop);
                        //@todo: validate duplicate data id
                        if (scope.dragId === undefined || scope.dragId === null) {
                            throw new Error("required field 'data-drag-id' not found");
                        }
                        scope.$on("elementDropped", scope._dropListener);
                        scope._createDraggable();
                    };
                    scope._createDraggable = function () {
                        if (attr["dragActive"] === "false") {
                            elm.addClass("drag-disable");
                            return;
                        }
                        elm.draggable({
                            revert: scope._isRevert,
                            containment: scope._getContainment(),
                            cursor: "move",
                            helper: "clone",
                            start: scope._dragStart,
                            stop: scope._dragStop
                        });
                    };
                    scope._dragStart = function (e, ui) {
                        scope.isDropped = false;
                        angular.element(ui.helper).addClass(attr["dragClass"] || "");
                        elm.css("opacity", "0.5");
                    };
                    scope._dragStop = function (e, ui) {
                        elm.css("opacity", "1");
                    };

                    scope._getContainment = function () {
                        return attr["containment"] || "body";
                    };

                    scope._dropListener = function (e, data) {
                        if (data.dragId === scope.dragId) {
                            scope._performAfterDrop();
                        }
                    };
                    scope._performAfterDrop = function () {
                        scope.isDropped = true;
                        elm.attr("data-clone-drop", scope.cloneDrop === 1 ? scope.cloneDrop : --scope.cloneDrop);
                    };

                    scope.getCloneDrop = function () {
                        return scope.cloneDrop;
                    };

                    scope._isRevert = function () {
                        return !scope.isDropped;
                    };
                    scope.init();
                }
            };
        }]);
//}(angular.module("angularDragDrop")));