﻿app.controller('CompositeEngineCtrl', function ($scope, $window, $http, assessmentEngineSvc, Enum, $modal, $sce, $filter, $compile, $timeout, $modal, SweetAlert) {
    //$scope.template = '';

    $scope.SortableFuncConfig = {
        group: 'grswip',
        animation: 150,
        disabled:false,
        onSort: function (evt) {            
            $scope.$parent.enableDisableCheckAnswerButton(true);
        }
    };

    $scope.SingledroppableConfig = {
        group: 'grswip',
        animation: 150,
        disabled:false
        //onMove: function (evt) {
        //    //if (evt.to.childElementCount == 1) {
        //    //    return false;
        //    //}
        //    //alert(111);
        //    if ($scope.questionTypeId == Enum.QuestionType.Matching
        //        && ($scope.layoutType == 'dragdrop_parallel' || $scope.layoutType == 'horizontal' || $scope.layoutType == 'vertical' || $scope.layoutType == 'image')
        //        && evt.to.childElementCount == 1) {
        //        return false;
        //    }
        //}
    };

    $scope.SingledraggableConfig = {
        group: 'grswip',
        animation: 150,
        disabled: false
        //onMove: function (evt) {
        //    if (evt.to.childElementCount > 0 && evt.to.childElementCount < 2) {
        //        return false;
        //    }
        //}
    };

    $scope.clickHotspotForComposite = function (index,option) {
        
        if (!$scope.CheckAnswer) {
            if ($scope.Questions[index].NoOfSelection < $scope.Questions[index].MaxSelection || ($scope.Questions[index].NoOfSelection == $scope.Questions[index].MaxSelection && option.currentClass == 'selected')) {
                option.isAnswered = !option.isAnswered;
                var optindex = $scope.Questions[index].UserSelectedArea.indexOf(option.choiceid);
                if (optindex > -1) {
                    $scope.Questions[index].UserSelectedArea.splice(optindex, 1);
                    option.currentClass = '';
                    $scope.Questions[index].NoOfSelection = $scope.Questions[index].NoOfSelection - 1;
                }
                else {
                    $scope.Questions[index].UserSelectedArea.push(option.choiceid);
                    option.currentClass = 'selected';
                    $scope.Questions[index].NoOfSelection = $scope.Questions[index].NoOfSelection + 1;
                    $scope.$parent.enableDisableCheckAnswerButton(true);
                }
                if ($scope.Questions[index].NoOfSelection > 0) {
                    $scope.Questions[index].userHasSelectedOptions = true;
                    
                }
                else if ($scope.NoOfSelection == 0) {
                    $scope.Questions[index].userHasSelectedOptions = false;
                }
            }
        }
    };

    $scope.SetHotSpotTypeForComposite = function (data, index) {
        //data.globalCorrectFeedback = "";
        //data.ShowGlobalCorrectFeedback = false;
        //data.globalInCorrectFeedback = "";
        //data.ShowGlobalIncorrectFeedback = false;
        if (data.max_choice_selection.text == '0') {
            data.MaxSelection = data.choices.length;
        }
        else if (data.max_choice_selection.text == '') {
            data.MaxSelection = 1;
        }
        else {
            data.MaxSelection = parseInt(data.max_choice_selection.text);
        }
        //mehul
        if ($scope.$parent.isUndefined(data.NoOfSelection))
        {
            data.NoOfSelection = 0;
        }
        if ($scope.$parent.isUndefined(data.UserSelectedArea))
        {
            data.UserSelectedArea = [];
        }
        var imgObject = document.createElement('div');
        imgObject.innerHTML = data.question_media.media;
        data.imageSrc = $(imgObject).find('img').attr('src');
        if ($scope.isUndefined(data.imageSrc)) {
            data.imageSrc = '';
        }

        angular.forEach(data.choices, function (option, key) {
            option.XPosition = '';
            option.YPosition = '';
            option.isAnswered = false;
            option.showOptionFeedbackModal = $scope.showOptionFeedbackModal;
            option.displayfeedback = false;
            // $scope.QuestionOptions[key].opacity = 0;

            var positions = option.text.split(',');
            if (positions.length == 2) {
                option.XPosition = positions[0];
                option.YPosition = positions[1];
            }
        });

    };


    $scope.init = function () {
        //$scope.Questions = assessmentEngineSvc.GetCompositeQuestions();
        //$scope.template = 'Composite.html';
        $scope.SingledraggableConfig.disabled = false;
        $scope.Questions = $scope.$parent.CurrentQuestion.QuestionData.questions;
        $scope.AllSelectedOptions = $scope.$parent.CompositeSelectedOption;
        $scope.enableDisableDragDropComposite(false);
        $.each($scope.Questions, function (index, value) {
            $scope.Questions[index].Index = index;
            $scope.Questions[index].userHasSelectedOptions = false;
            $scope.Questions[index].DistractorItems = [];
            $scope.Questions[index].SelectedTemplateArray = [];
            $scope.Questions[index].globalCorrectFeedback = "";
            $scope.Questions[index].ShowGlobalCorrectFeedback = false;
            $scope.Questions[index].globalInCorrectFeedback = "";
            $scope.Questions[index].ShowGlobalIncorrectFeedback = false;
            if (!$scope.isUndefined(value.stem_details) && value.stem_details.type == 'algorithimic') {
                $scope.SetAlgorithimicType(value);
            }
            if (value.question_type.text == Enum.QuestionType.FIB || value.question_type.text == Enum.QuestionType.FIBDND || value.question_type.text == Enum.QuestionType.Matching) {
                $scope.SetFIBType(value, index);
            } else if (value.question_type.text == Enum.QuestionType.LTD || value.question_type.text == Enum.QuestionType.LTDDND) {
                $scope.SetLTDType(value, index);
            }
            else if (value.question_type.text == Enum.QuestionType.Essay) {
                $scope.Questions[index].EssayText = '';
            } else if (value.question_type.text == Enum.QuestionType.TextSelection) {
                $scope.SetTextSelectionType(value, index);
            }
            else if (value.question_type.text == Enum.QuestionType.HotSpot)
            {
                $scope.SetHotSpotTypeForComposite(value,index);
            }
            angular.forEach(value.choices, function (option, key) {
                $scope.Questions[index].choices[key].currentClass = "";
                $scope.Questions[index].choices[key].QuestionIndex = index;
                $scope.Questions[index].choices[key].CurrentOptionIndex = key;
            });

            if ($scope.AllSelectedOptions != null && !$scope.isUndefined($scope.AllSelectedOptions) && $scope.AllSelectedOptions.length >= index) {
                $scope.Questions[index].EssayText = '';
                $scope.setCompositeQuestionOptions(value, $scope.AllSelectedOptions[index].UserSelectedOptions, index);
            }
        });
        $scope.$parent.compositeQuestions = $scope.Questions;

    };

    $scope.CheckInputValue = function (e, value) {
        var keyCode = e.which ? e.which : e.keyCode;
        value.AnswerClass = "";
        var decimalplaces = (value.decimalplace == "") ? 0 : value.decimalplace;
        if (!$scope.ValidateUserInput(keyCode, value.inputtype, decimalplaces, value)) {
            e.preventDefault();
            return false;
        }
    };
    $scope.ValidateUserInput = function (keyCode, inputtype, maxDecimals, value) {
        var resultType = false;
        var regexp = "";
        var specialKeys = new Array();
        specialKeys.push(8); //Backspace
        specialKeys.push(37); //left
        specialKeys.push(38); //up
        specialKeys.push(39); //right
        specialKeys.push(40); //down
        specialKeys.push(9); //Tab key
        specialKeys.push(32);//space bar
        switch (inputtype) {
            case 'alphanumeric':
                if ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                    resultType = true;
                break;
            case 'numeric':
                if (maxDecimals > 0) {
                    specialKeys.push(46); //decimal
                    regexp = "^-?\\d*\\.?\\d{0," + maxDecimals + "}$";
                } else {
                    regexp = "^-?\\d*$";
                }
                if ((keyCode >= 48 && keyCode <= 57) || (specialKeys.indexOf(keyCode) != -1)) {
                    var validRegex = new RegExp(regexp);
                    resultType = validRegex.test(value.UserInput);
                }
                break;
            case 'alphabet':
                if ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(keyCode) != -1))
                    resultType = true;
                break;
            default:
                break;
        }
        return resultType;
    };

    $scope.$on('nextCompositeQuestion', function () {
        //console.log('nextCompositeQuestion');
        $scope.init();
    })
    $scope.$on('resetCompositeQuestion', function () {
        //console.log('nextCompositeQuestion');
        $scope.ResetCompositeQuestions();
    })

    $scope.$on('enableDisableDragDropComposite', function (value) {
        //console.log('nextCompositeQuestion');
        $scope.enableDisableDragDropComposite(value);
    })

    $scope.enableDisableDragDropComposite = function (value) {
        $scope.SortableFuncConfig.disabled = value;
        $scope.SingledraggableConfig.disabled = value;
        $scope.SingledroppableConfig.disabled = value;
    }

    $scope.ResetCompositeQuestions = function () {
        $scope.enableDisableDragDropComposite(false);
        $.each($scope.Questions, function (index, question) {
            question.globalCorrectFeedback = "";
            question.ShowGlobalCorrectFeedback = false;
            question.globalInCorrectFeedback = "";
            question.ShowGlobalIncorrectFeedback = false;
            $('.fdback').addClass('ng-hide');
            $('.fdback - mobile').addClass('ng-hide');
           
            if (question.question_type.text == Enum.QuestionType.FIBDND) {
                $scope.SetFIBType(question, index);
            }
            $(question.choices).each(function (opIndex, value) {
                value.showOptionFeedbackModal = false;
                value.showAttempt = false;
                if (question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND || question.question_type.text == Enum.QuestionType.FIB || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.Matching) {
                    this.isAnswered = false;
                    this.AnswerClass = "";
                    if (!!value.UserInput) value.UserInput = "";
                    if (!!value.Canvas && value.Canvas.length > 0) {
                        $(value.Canvas).each(function (cindex, cvalue) {
                            cvalue.AnswerClass = "";
                            cvalue.selectedClass = "";
                            question.droppableItems.push(cvalue);
                        });
                        value.Canvas = [];

                    }
                    value.multiSelectItem = [];
                    $('li').removeClass('incorrect');
                    $('li').removeClass('correct');
                    $('#divltd' + question.Index + value.key).html('');
                    $('#divltd' + question.Index + value.key).hide();
                }
                else if (question.question_type.text == Enum.QuestionType.FIBDND) {
                    $("div[data-question-id='" + value.questionIndex + "']").empty();
                    $('#divltd' + question.Index + value.key).html('');
                    $('#divltd' + question.Index + value.key).hide();
                    //var selectedValue = question.SelectedTemplateArray.filter(function (x) { return x.dropId == opIndex; });
                    //if (selectedValue.length > 0) {
                    //    $(selectedValue).each(function (sindex, svalue) {
                    //        if (svalue != "") {
                    //            if (question.droppableItems.filter(function (x) { return $filter('lowercase')(x.title) == $filter('lowercase')('<span math dynamic>' + svalue.title + '</span>'); }).length == 0) {
                    //                question.droppableItems.push({ 'title': svalue.title, 'drag': true, selectedClass: '' });
                    //            }
                    //            svalue.dropId = "initial";
                    //        }
                    //    });
                    //}
                }
                else if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
                    value.isChoiceAnswered = false;
                    $(value.suboption).each(function (sindex, svalue) {
                        svalue.currentClass = "";
                        svalue.isAnswered = "";
                        svalue.isOptionAnswered = false;
                        value.suboptionSelected = "";
                    });
                    $('#divltd' + value.QuestionIndex + value.choiceid).html('');
                    $('#divltd' + value.QuestionIndex + value.choiceid).hide();
                } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
                    value.AnswerClass = "";
                }
                else if (question.question_type.text == Enum.QuestionType.Order) {
                    value.isCorrect = false;
                    value.currentClass = "";
                }
                else if (question.question_type.text == Enum.QuestionType.HotSpot) {
                    $('#divltd' + value.QuestionIndex + value.choiceid).html('');
                    $('#divltd' + value.QuestionIndex + value.choiceid).hide();
                    value.isAnswered = false;
                    this.isAnswered = false;
                    this.currentClass = "";
                }
                else {
                    this.isAnswered = false;
                    this.currentClass = "";
                }
            });
            if (question.question_type.text == Enum.QuestionType.Order) {
                question.choices.sort(function (a, b) {
                    if (a.CurrentOptionIndex < b.CurrentOptionIndex) return -1;
                    if (a.CurrentOptionIndex > b.CurrentOptionIndex) return 1;
                    return 0;
                })
            } else if (question.question_type.text == Enum.QuestionType.TextSelection) {
                question.NoOfSelection = 0;
                question.userHasSelectedOptions = false;
            }
            else if (question.question_type.text == Enum.QuestionType.HotSpot) {
                question.NoOfSelection = 0;
                question.userHasSelectedOptions = false;
                question.UserSelectedArea = [];
            }
        });
    };
    $scope.trustSrc = function (src) {
        return $sce.trustAsResourceUrl(src);
    }

    $scope.trustAsHtml = function (string) {
        return $sce.trustAsHtml(string);
    };

    $scope.setOptions = function (option, OptionId, questionIndex) {
        $(".option").removeClass('correct');
        $(".option").removeClass('incorrect');
        var questionType = '';
        if (!$scope.isUndefined(option.QuestionIndex)) {
            questionType = $scope.Questions[option.QuestionIndex].question_type.text;
        }
        else {
            questionType = $scope.Questions[questionIndex].question_type.text;
        }
        switch (questionType) {
            case Enum.QuestionType.MCSS:
            case Enum.QuestionType.GRAPHIC:
            case Enum.QuestionType.Exhibit:
                $($scope.Questions[option.QuestionIndex].choices).each(function (index) {
                    this.currentClass = "";
                    if (this.choiceid == option.choiceid) {
                        this.isAnswered = true;
                        option.currentClass = "selected";
                    }
                    else {
                        this.isAnswered = false;
                    }
                    this.isButtonClicked = true;
                    if (this.isAnswered) {
                        $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                        $scope.$parent.enableDisableCheckAnswerButton(true);
                        this.currentClass = "selected";
                    }
                });

                break;
            case Enum.QuestionType.MCMSVIDEO:
            case Enum.QuestionType.MCMS:
                $($scope.Questions[option.QuestionIndex].choices).each(function (index) {
                    if (this.choiceid == option.choiceid) {
                        if (this.isAnswered == true) {
                            this.isAnswered = false;
                            option.currentClass = "";
                        }
                        else {
                            this.isAnswered = true;
                            option.currentClass = "selected";
                        }
                    }
                    if (this.isAnswered == true) {
                        this.currentClass = "selected";
                        $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                        $scope.$parent.enableDisableCheckAnswerButton(true);
                    }
                });
                break;
            case Enum.QuestionType.ChoiceMatrix:
                $($scope.Questions[questionIndex].choices).each(function (index, value) {
                    $scope.Questions[questionIndex].selectedOption = OptionId;

                    $(value.suboption).each(function (sindex, soption) {
                        if (value.choiceid == OptionId) {
                            value.isChoiceAnswered = true;
                            if (option.identifier == soption.identifier) {
                                this.isOptionAnswered = true;
                                value.isAnswered = value.assess;
                                value.suboptionSelected = soption.identifier;
                                soption.isAnswered = soption.value;
                                soption.currentClass = "selected";
                            }
                            else {
                                this.isOptionAnswered = false;
                                soption.currentClass = "";
                                soption.isAnswered = !soption.value;
                            }
                        }
                        if (this.isOptionAnswered == true) {
                            this.currentClass = "selected";
                            $scope.Questions[questionIndex].userHasSelectedOptions = true;
                            $scope.$parent.enableDisableCheckAnswerButton(true);
                        }
                    });
                });
                break;
            case Enum.QuestionType.Order:
                $(option).each(function (index) {
                    this.isAnswered = true;
                    if (this.isAnswered) {
                        $scope.Questions[questionIndex].userHasSelectedOptions = true;
                        $scope.$parent.enableDisableCheckAnswerButton(true);
                    }
                });
                break;
            case Enum.QuestionType.LTD:
            case Enum.QuestionType.LTDDND:
            case Enum.QuestionType.FIB:
            case Enum.QuestionType.FIBDND:
            case Enum.QuestionType.Matching:
                var totalChoices = $scope.Questions[option.QuestionIndex].choices.length;
                var objList = $filter('filter')($scope.Questions[option.QuestionIndex].choices, { "Canvas": [] }, true);
                var objList2 = $filter('filter')($scope.Questions[option.QuestionIndex].choices, { "UserInput": "" }, true);
                if (objList.length > 0 && objList.length < totalChoices) {
                    $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                    $scope.$parent.enableDisableCheckAnswerButton(true);
                }
                if (objList2.length < totalChoices) {
                    $scope.Questions[option.QuestionIndex].userHasSelectedOptions = true;
                    $scope.$parent.enableDisableCheckAnswerButton(true);
                }
                break;
        }
    }

    $scope.setCompositeQuestionOptions = function (question, selectedOptions, questionIndex) {

        $scope.text = '';
        if (question.question_type.text == Enum.QuestionType.ChoiceMatrix) {
            if (selectedOptions) {
                var selectedOpt = angular.fromJson(selectedOptions);
                $(selectedOpt).each(function (selectindex, selectvalue) {
                    var inputValues = selectvalue.inputvalue.split("-");
                    $(question.choices).each(function (index) {
                        var optionindex = index + 1;
                        $(question.choices[index].suboption).each(function (index1, value1) {
                            question.choices[index].suboption[index1].currentClass = "";
                            if (optionindex == parseInt(inputValues[0]) && value1.identifier == parseInt(inputValues[1])) {
                                question.choices[index].suboption[index1].isAnswered = true;
                            }
                        });

                    });
                });

            }

        }
        if (question.question_type.text == Enum.QuestionType.LTD || question.question_type.text == Enum.QuestionType.LTDDND || question.question_type.text == Enum.QuestionType.Matching || question.question_type.text == Enum.QuestionType.FIBDND || question.question_type.text == Enum.QuestionType.FIB) {
            if (selectedOptions) {
                var selctdOpt = angular.fromJson(selectedOptions);
                angular.forEach(question.choices, function (option, key) {
                    $(selctdOpt).each(function (fibindex, value) {
                        if ((option.interaction == 'textentry' || option.interaction == 'inline') && value.noforeachblank == option.noforeachblank) {
                            $scope.Questions[questionIndex].choices[key].UserInput = value.inputvalue;
                        }
                        else if (option.interaction == 'dragdrop' && value.noforeachblank == option.noforeachblank) {
                            angular.forEach($scope.Questions[questionIndex].droppableItems, function (ditem, dindex) {
                                if ($filter('lowercase')(ditem.title) == $filter('lowercase')(value.inputvalue)) {
                                    //$scope.droppableItems[dindex].title = "";
                                    $scope.Questions[questionIndex].choices[key].Canvas = [{ 'title': value.inputvalue, 'drag': true, selectedClass: '' }];
                                    $scope.Questions[questionIndex].droppableItems.splice(dindex, 1);
                                    dindex = dindex - 1;
                                }
                            });
                        }
                        else if (option.interaction == 'container' && value.noforeachblank == option.noforeachblank) {
                            var input = value.inputvalue.map(function (x) { return x.title; });
                            for (var i = 0; i < $scope.Questions[questionIndex].droppableItems.length; i++) {
                                if (input.indexOf($scope.Questions[questionIndex].droppableItems[i].title) != -1) {
                                    $scope.Questions[questionIndex].choices[key].Canvas.push({ 'title': $scope.Questions[questionIndex].droppableItems[i].title, 'drag': true, selectedClass: '' });
                                    $scope.Questions[questionIndex].choices[key].multiSelectItem.push($($scope.Questions[questionIndex].droppableItems[i].title).text());
                                    $scope.Questions[questionIndex].droppableItems.splice(i, 1);
                                    i = i - 1;
                                }
                            }
                        }
                    });
                });

            }

        }
        else if (question.question_type.text == Enum.QuestionType.Essay) {

                if (selectedOptions) {
                    //$scope.essayTemplateText = data.SelectedOptions;
                    if (question.toolbar.type == "math_3-5_palette" || question.toolbar.type == "math_6-8_palette") {
                        if ($scope.operationmode == Enum.OperationMode.Offline) {
                            question.mathEditorPrevLatex = $.trim(selectedOptions[0].replace("?", ""));
                            question.mathEditorType = question.toolbar.type;
                        }
                        else {
                            question.mathEditorPrevLatex = $.trim(selectedOptions.replace("?", ""));
                            question.mathEditorType = question.toolbar.type;
                        }
                    } else {
                        $("#txtEssayContent" + questionIndex).val(selectedOptions);
                        question.EssayText = selectedOptions;
                    }
                }
            }
        else if (question.question_type.text == Enum.QuestionType.TextSelection) {
            if (selectedOptions) {
                var userSelectedOptions = "";
                var selctdOpt = angular.fromJson(selectedOptions);
                $(selctdOpt).each(function (sindex, svalue) {
                    if (userSelectedOptions == "") {
                        userSelectedOptions = svalue.choiceid;
                    }
                    else {
                        userSelectedOptions = userSelectedOptions + "," + svalue.choiceid;
                    }
                });
                question.SelectedOptions = userSelectedOptions;
            }
        }
        else if (question.question_type.text == Enum.QuestionType.HotSpot) {
            if (!$scope.$parent.isUndefined(question.question_media.media) && question.question_media.media.length > 0 && question.question_media.media != "") {
                //if (data.userHasSelectedOptions) {
                var selectedoptions = $scope.AllSelectedOptions;
                if (selectedoptions != null) {
                    var usrSelectedObj = JSON.parse(selectedoptions[question.Index].UserSelectedOptions)
                    $(question.choices).each(function (counter, value) {
                        $(usrSelectedObj).each(function (opCounter, opValue)
                            {
                            if (value.choiceid == opValue.choiceid) {
                                value.isAnswered = true;
                                value.currentClass = 'selected';
                            }
                    });

                    });
                }
            }
        }
        else {
            var selctdoptions = '';
            $(question.choices).each(function (index) {
                //data.QuestionData.choices[index].isAnswered = false;
                //if (!$scope.isUndefined(data.QuestionData.image) && data.QuestionData.image.image.indexOf('http') === -1 && data.QuestionData.image.image != "") {
                //    data.QuestionData.choices[index].image = "Content/images/" + data.QuestionData.choices[index].image;
                //}
                question.choices[index].showOptionFeedbackModal = $scope.showOptionFeedbackModal;
                question.choices[index].displayfeedback = false;
                var optionindex = index + 1;
                question.choices[index].CurrentOptionIndex = optionindex;
                if (selectedOptions) {
                    selctdoptions = selectedOptions.split(",");
                    $(selctdoptions).each(function (selectindex) {
                        question.choices[index].SelectedOptions = parseInt(selctdoptions[selectindex]);
                        if (optionindex == parseInt(selctdoptions[selectindex])) {
                            question.choices[index].isAnswered = true;
                            //question.choices[index].currentClass = "selected";

                        }
                    });
                }
            });

        }
        // return data;
    }

    $scope.SetAlgorithimicType = function (question) {
        var textReplaced = false;
        if ($scope.isUndefined(question.AlgorithmicVariableArray)) {
            question.AlgorithmicVariableArray = [];
        }
        if (question.stem_details.textforeachvalue.length > 0) {
            angular.forEach(question.stem_details.textforeachvalue, function (textDetail, key) {
                if (textDetail.type == 'random') {
                    if (question.AlgorithmicVariableArray.length == angular.fromJson(question.stem_details.textforeachvalue).length) {
                        angular.forEach(question.AlgorithmicVariableArray, function (arrayVariable, akey) {
                            question.question_stem.text = question.question_stem.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        });
                    }
                    else {
                        var random = Math.floor((Math.random() * textDetail.maxvalue) + textDetail.minvalue);
                        question.question_stem.text = question.question_stem.text.replace(textDetail.text, random);
                        question.AlgorithmicVariableArray.push({ "TextToReplace": textDetail.text, "TextValue": random });
                    }
                }
            });

            angular.forEach(question.choices, function (option, key) {
                angular.forEach(question.AlgorithmicVariableArray, function (arrayVariable, akey) {
                    if (isNaN(option.text) && option.text.indexOf(arrayVariable.TextToReplace) != -1) {
                        option.text = option.text.replace(arrayVariable.TextToReplace, arrayVariable.TextValue);
                        textReplaced = true;
                    }
                });
                if (question.stem_details.display == 'result' && textReplaced) {
                    option.text = eval(option.text);
                }
            });
        }
    };

    $scope.SetFIBType = function (data, index) {
        var questiontext = "";//data.QuestionData.textwithblanks.text;
        if (data.textwithblanks != undefined) {
            if (data.textwithblanks.text != undefined && data.textwithblanks.text != "")
                questiontext = data.textwithblanks.text;
        }

        //Array object to hold draggable item values
        $scope.Questions[index].droppableItems = [];
        $scope.Questions[index].FIBText = "";
        $scope.Questions[index].layoutType = ($scope.isUndefined(data.question_layout)) ? "" : data.question_layout.text;
        $scope.FillDroppableItems(data, index);
        angular.forEach(data.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            $scope.Questions[index].choices[key].key = key;       // Index of option
            $scope.Questions[index].choices[key].UserInput = "";  //User input value of textbox and dropdown value.         
            $scope.Questions[index].choices[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            $scope.Questions[index].choices[key].width = 'auto';
            $scope.Questions[index].choices[key].multiSelectItem = [];
            $scope.Questions[index].choices[key].FibClass = "";
            $scope.Questions[index].choices[key].accept = true;
            $scope.Questions[index].choices[key].showOptionFeedbackModal = $scope.$parent.showOptionFeedbackModal;

            questiontext = questiontext.replace(textToReplace, "<fibvariation ng-model='$parent.Questions[" + index + "].choices[" + key + "]' parent-ctrl='$parent' user-input='Questions[" + index + "].choices[" + key + "].UserInput'></fibvariation>");

            $scope.$watch("Questions[" + index + "].choices[" + key + "].Canvas.length", function (newValue, oldValue) {
                if ($scope.Questions.length > 0) {
                    if (index < $scope.Questions.length) {
                        $scope.setOptions($scope.Questions[index].choices[key], null);
                    }
                }
            });

            if ((option.interaction == 'textentry' || option.interaction == 'inline')) {
                $scope.Questions[index].choices[key].FibClass = "fibclass";
            }
        });
        $scope.Questions[index].FIBText = questiontext;
        if (!$scope.isUndefined($scope.Questions[index].layoutType)) {
            if ($scope.Questions[index].layoutType == 'right' || $scope.Questions[index].layoutType == 'left' || $scope.Questions[index].layoutType == 'vertical') {
                $scope.Questions[index].droptype = "";
                $scope.Questions[index].dragtype = "dragpanel col-sm-4 col-md-4 right";
            }
            else if ($scope.Questions[index].layoutType == 'row') {
                $scope.Questions[index].droptype = "";
                $scope.Questions[index].dragtype = "dragpanel text-center";
            }
            else {
                $scope.Questions[index].dragtype = "dragpanel";
                $scope.Questions[index].droptype = "";
            }
        }
    };

    $scope.SetTextSelectionType = function (data, index) {
        var questiontext = "";
        $scope.Questions[index].PassageText = "";
        $scope.Questions[index].showExhibit = false;
        $scope.Questions[index].MaxSelection = 0;
       
        $scope.Questions[index].userHasSelectedOptions = false;

        if (data.max_choice_selection.text == '0') {
            $scope.Questions[index].MaxSelection = data.choices.length;
        }
        else if (data.max_choice_selection.text == '') {
            $scope.Questions[index].MaxSelection = 1;
        }
        else {
            $scope.Questions[index].MaxSelection = parseInt(data.max_choice_selection.text);
        }
        $scope.Questions[index].NoOfSelection = 0;
        if (data.textwithblanks != undefined) {
            if (data.textwithblanks.text != undefined && data.textwithblanks.text != "")
                questiontext = data.textwithblanks.text;
        }
        if (!$scope.isUndefined(data.exhibit) && data.exhibit.length > 0 && data.exhibit[0].exhibit_type != "") {
            $scope.Questions[index].showExhibit = true;
            if (data.exhibit[0].exhibit_type == 'image') {
                var imagePath = String(data.exhibit[0].path);
                data.exhibit[0].path = imagePath;
            }
        }


        angular.forEach(data.choices, function (option, key) {

            $scope.Questions[index].choices[key].showAttempt = false;
            $scope.Questions[index].choices[key].showOptionFeedbackModal = false;

            $scope.Questions[index].choices[key].key = key;       // Index of option            
            $scope.Questions[index].choices[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            if (!$scope.isUndefined(option.text) && option.text != "" && option.text.match("^\[\[[0-9]*\]\]")) {
                var startTagToReplace = option.text;
                var endTagToReplace = option.text.substring(0, 2) + "/" + option.text.substring(2);
                questiontext = questiontext.replace(startTagToReplace, "&nbsp;&nbsp;<span class='option selectable' style='cursor: pointer;' ng-click='$parent.SetSelectedClass(" + key + "," + index + ")' ng-class='$parent.Questions[" + index + "].choices[" + key + "].AnswerClass'> <span class='sign'><span class='sr-only'></span><span class='fa'></span></span><span class='fdback fdback-desktop' ng-show='$parent.Questions[" + index + "].choices[" + key + "].showOptionFeedbackModal && $parent.Questions[" + index + "].choices[" + key + "].displayfeedback'><span class='sr-only'></span><span id='spnltd" + key + "'class='fa fa-commenting-o' popover popover-html='" + $scope.Questions[index].choices[key].choiceid + "' ng-click='$parent.GetFeedbackForComposite($parent.Questions[" + index + "].choices[" + key + "],\"" + $scope.Questions[index].question_type.text + "\")'></span></span>");
                //questiontext = questiontext.replace(startTagToReplace, "&nbsp;&nbsp;<span id='spnltd" + key + "' class='option selectable' style='cursor: pointer;' ng-click='$parent.SetSelectedClass(" + key + ")' ng-class='$parent.QuestionOptions[" + key + "].AnswerClass'> <span class='sign'><span class='sr-only'></span><span class='fa'></span></span><span class='fdback' ng-show='$parent.QuestionOptions[" + key + "].showOptionFeedbackModal && $parent.QuestionOptions[" + key + "].showAttempt'><span class='sr-only'></span><span class='fa fa-commenting-o' popover popover-html='" + $scope.QuestionOptions[key].choiceid + "' ng-click='$parent.GetFeedback($parent.QuestionOptions[" + key + "])'></span></span>");
                questiontext = questiontext.replace(endTagToReplace, "</span>&nbsp;&nbsp;");
            }
        });
        data.PassageText = questiontext;
    };

    $scope.SetSelectedClass = function (key, index) {
        if (!$scope.$parent.CheckAnswer) {
            if ($scope.Questions[index].NoOfSelection < $scope.Questions[index].MaxSelection || ($scope.Questions[index].NoOfSelection == $scope.Questions[index].MaxSelection && $scope.Questions[index].choices[key].AnswerClass == 'selectedC')) {
                if ($scope.Questions[index].choices[key].AnswerClass == 'selectedC') {
                    $scope.Questions[index].choices[key].AnswerClass = "";
                    $scope.Questions[index].NoOfSelection = $scope.Questions[index].NoOfSelection - 1;
                }
                else {
                    $scope.Questions[index].choices[key].AnswerClass = "selectedC";
                    $scope.Questions[index].NoOfSelection = $scope.Questions[index].NoOfSelection + 1;
                }
            }
            if ($scope.Questions[index].NoOfSelection > 0) {
                $scope.Questions[index].userHasSelectedOptions = true;
                $scope.$parent.enableDisableCheckAnswerButton(true);
            }
            else if ($scope.Questions[index].NoOfSelection == 0) {
                $scope.Questions[index].userHasSelectedOptions = false;
            }
        }
    };

    $scope.CheckDropItem = function (event, index, item, type) {
        if (item.QuestionIndex != index) {
            return false;
        }
        else {
            if ($scope.Questions[index].question_type.text == Enum.QuestionType.Order) {
                $scope.setOptions($scope.Questions[index].choices, null, index);
            }
            return item;
        }
    },

    $scope.FillDroppableItems = function (data, index) {
        $scope.Questions[index].SelectedTemplateArray = [];
        isDistractorsPushDone = false;
        angular.forEach(data.choices, function (option, key) {
            $scope.Questions[index].choices[key].arrList = [];
            if (option.interaction == "inline") {
                angular.forEach(option.distractorforeachblank, function (titem, tindex) {
                    if (titem.text && titem.text.trim().length > 0) {
                        $scope.Questions[index].choices[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                    }
                });
            }

            if (option.interaction == "dragdrop" || option.interaction == "container") {
                if (isDistractorsPushDone == false) {
                    angular.forEach($scope.$parent.CurrentQuestion.QuestionData.questions[index].distractors, function (titem, tindex) {
                        if (titem.text && titem.text.trim().length > 0) {
                            $scope.Questions[index].choices[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                            $scope.Questions[index].DistractorItems.push({ id: tindex + 1, title: titem.text });
                            $scope.Questions[index].SelectedTemplateArray.push({ dropId: 'initial', title: titem.text });
                        }
                    });
                }
                isDistractorsPushDone = true;
            }

            angular.forEach(option.textforeachblank, function (titem, tindex) {
                if (titem.text) {
                    $scope.Questions[index].choices[key].arrList.push('<span math dynamic>' + titem.text + '</span>');
                    $scope.Questions[index].DistractorItems.push({ id: tindex + 1, title: titem.text });
                    $scope.Questions[index].SelectedTemplateArray.push({ dropId: 'initial', title: titem.text });
                }
            });

            if (option.interaction == "dragdrop" || option.interaction == "container") {
                angular.forEach($scope.Questions[index].choices[key].arrList, function (item, cindex) {
                    $scope.Questions[index].droppableItems.push({ 'title': item, 'drag': true, selectedClass: '', 'QuestionIndex': index });
                });
                $scope.Questions[index].choices[key].Canvas = [];
                $scope.Questions[index].choices[key].Drop = true;
            }
        });
    };

    /*Dev*/
    $scope.SetItemToDropCanvasList = function (option) {
        if ($window.innerWidth <= 767) {
            if ($scope.Questions[option.QuestionIndex].choices[option.CurrentOptionIndex].Canvas.length == 0) {
                $scope.$parent.SetItemToDropCanvasListComposite(option.key, $scope.Questions[option.QuestionIndex].droppableItems);
            }
        }
    };


    //$scope.AddItemToDropItemList = function (item, key, index) {
    //    if ($window.innerWidth <= 767) {
    //        if ($scope.Questions[key].Canvas.length == 1) {
    //            $scope.droppableItems.push({ 'title': item.title, 'drag': true, selectedClass: '' });
    //            $scope.Questions[key].Canvas.splice(index, 1);
    //            $scope.DropCanvasListIndex = key;
    //            $scope.userHasSelectedOptions = true;
    //        }
    //    }
    //};
    /*Dev*/

    $scope.isUndefined = function (thing) {
        return (typeof thing === "undefined");
    };

    $scope.getValue = function (text, index) {
        $scope.Questions[index].essayTemplateText = text;
    }

    $scope.OnDragStart = function (option) {
        option.AnswerClass = "";
    };

    $scope.AddDropCssOnOver = function (key, questionIndex) {
        $('#divDropOption' + key).addClass('active');
        angular.forEach($scope.Questions[questionIndex].choices[key].Canvas, function (option, key) {
            option.AnswerClass = '';
        });
        return true;
    };

    $scope.RemoveDropCssOnLeave = function (questionIndex) {
        angular.forEach($scope.Questions[questionIndex].choices, function (option, key) {
            $('#divDropOption' + key).removeClass('active');
        });
    };

    $scope.AddCssOnOver = function (item) {
        item.selectedClass = 'dragging';
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveSelected = function () {
        var selection = window.getSelection ? window.getSelection() : document.selection ? document.selection : null;
        if (!!selection) selection.empty ? selection.empty() : selection.removeAllRanges();
    };

    $scope.RemoveCssOnOut = function (droppableItems) {
        angular.forEach(droppableItems, function (item, key) {
            item.selectedClass = '';
        });
    };

    var popovermodel;
    $scope.GetFeedbackForComposite = function (model, questiotype) {
        var feedbackText = '';
        var currentClass = '';
        popovermodel = '';
        $('body').popover("destroy");
        popovermodel = model;
        if (questiotype == Enum.QuestionType.TextSelection || questiotype == Enum.QuestionType.Matching || questiotype == Enum.QuestionType.LTDDND || questiotype == Enum.QuestionType.FIBDND) {
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {
                    if (questiotype == Enum.QuestionType.Matching && popovermodel.interaction.indexOf("container") > -1) {
                        if (popovermodel.AnswerClass.indexOf("incorrect") > -1) {
                            feedbackText = popovermodel.incorrect_feedback;
                        }
                        else {
                            feedbackText = popovermodel.correct_feedback;
                        }
                    }
                    else {
                        if (popovermodel.AnswerClass.indexOf("incorrect") > -1) {
                            feedbackText = popovermodel.incorrect_feedback;
                        }
                        else {
                            feedbackText = popovermodel.correct_feedback;
                        }
                    }
                    if ($('.fdback-mobile').is(':visible')) {
                        $('#divltd' + model.QuestionIndex + model.choiceid).html('<span>' + (model.choiceid) + ". " + feedbackText + '</span>');
                        $('#divltd' + model.QuestionIndex + model.choiceid).show();
                    }
                    else {
                        return feedbackText;
                    }
                },//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                placement: "top"
            });
        }
        else if (questiotype == Enum.QuestionType.ChoiceMatrix) {
            var cssClass = "";
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {
                    if (popovermodel.suboption[popovermodel.suboptionSelected - 1].currentClass.indexOf("incorrect") > -1) {
                        feedbackText = popovermodel.incorrect_feedback;
                        cssClass = "incorrect";
                    }
                    else {
                        feedbackText = popovermodel.correct_feedback;
                        cssClass = "correct";
                    }
                    
                    if ($('.fdback-mobile').is(':visible')) {
                       
                        $('#divltd' + model.QuestionIndex + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                        $('#divltd' + model.QuestionIndex + model.choiceid).addClass(cssClass);
                        $('#divltd' + model.QuestionIndex + model.choiceid).show();
                    }
                    else {
                        return feedbackText;
                    }
                },
                placement: "top"
            });
        }
        else if (questiotype == Enum.QuestionType.MCSS || questiotype == Enum.QuestionType.MCMS || questiotype == Enum.QuestionType.Order || questiotype == Enum.QuestionType.HotSpot) {
            $('body').popover({
                trigger: 'click',
                html: true,
                selector: '.fa-commenting-o',
                content: function () {
                    if (popovermodel.currentClass.indexOf("incorrect") > -1) {
                        feedbackText = popovermodel.incorrect_feedback;
                    }
                    else {
                        feedbackText = popovermodel.correct_feedback;
                    }
                    if ($('.fdback-mobile').is(':visible')) {
                        $('#divltd' + model.QuestionIndex + model.choiceid).html('<span>' + model.choiceid + ". " + feedbackText + '</span>');
                        $('#divltd' + model.QuestionIndex + model.choiceid).show();
                    }
                    else {
                        return feedbackText;
                    }
                },//scope.$eval("<span math dynamic>&Ntilde; \\((a+b)^2\\)</span>"),
                placement: "top"
            });            
        }

    }

    $scope.SetLTDType = function (data, index) {
        data.QuestionOptionsList = [];
        data.droppableItems = [];
        data.layoutType = ($scope.isUndefined(data.question_layout)) ? "" : data.question_layout.text;

        var imgObject = document.createElement('div');
        imgObject.innerHTML = data.question_media.media;
        data.imageSrc = $(imgObject).find('img').attr('src');

        $scope.FillDroppableItems(data, index);
        data.QuestionOptions = data.choices;

        angular.forEach(data.choices, function (option, key) {
            var textToReplace = option.noforeachblank;
            data.QuestionOptions[key].key = key;       // Index of option
            data.QuestionOptions[key].UserInput = "";  //User input value of textbox and dropdown value.         
            data.QuestionOptions[key].AnswerClass = "";//for applying ng-class in case of correct or incorrect
            data.QuestionOptions[key].width = 'auto';
            data.QuestionOptions[key].showOptionFeedbackModal = data.showOptionFeedbackModal;
            data.QuestionOptions[key].showAttempt = data.showAttempt;
            data.QuestionOptions[key].ShowGlobalFeedback = data.ShowGlobalFeedback;
            data.QuestionOptions[key].XPosition = '';
            data.QuestionOptions[key].YPosition = '';
            data.QuestionOptions[key].LTDText = '';
            data.QuestionOptions[key].accept = true;

            var positions = option.noforeachblank.split(',');
            if (positions.length == 2) {
                data.QuestionOptions[key].XPosition = positions[0];
                data.QuestionOptions[key].YPosition = positions[1];
            }
            if (data.SelectedOptions) {
                var selectedOption = angular.fromJson(data.SelectedOptions);
                $(selectedOption).each(function (fibindex, value) {
                    if ((option.interaction == 'textentry' || option.interaction == 'inline') && value.noforeachblank == option.noforeachblank) {
                        data.QuestionOptions[key].UserInput = value.inputvalue;
                    }
                    else if (option.interaction == 'dragdrop' && value.noforeachblank == option.noforeachblank) {
                        angular.forEach(data.droppableItems, function (ditem, dindex) {
                            if ($filter('lowercase')(ditem.title) == $filter('lowercase')(value.inputvalue)) {
                                //$scope.droppableItems[dindex].title = "";
                                data.QuestionOptions[key].Canvas = [{ 'title': value.inputvalue, 'drag': true, selectedClass: '' }];
                                data.droppableItems.splice(dindex, 1);
                                dindex = dindex - 1;
                            }
                        });
                    }
                });
            }
            data.QuestionOptionsList.push(option);
        });

        if (!$scope.isUndefined(data.layoutType)) {
            if (data.layoutType == 'right' || data.layoutType == 'left' || data.layoutType == 'vertical') {
                data.droptype = "col-sm-9 col-md-9";
                data.dragtype = "dragpanel col-sm-3 col-md-3 right";
            }
            else if (data.layoutType == 'row') {
                data.droptype = "";
                data.dragtype = "dragpanel text-center";
            }
            else {
                data.dragtype = "dragpanel";
                data.droptype = "";
            }
        }
    };

    $scope.init();

});
