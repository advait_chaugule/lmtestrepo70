﻿var Hotspot = Hotspot || {}
Hotspot.ImageHotspot = (function () {
    var self = this;
    var image = $("#mediaImage");
    var newImageDim = {};
    var imageDim = {};
    self.settings = [];
    self.hotspotTag = "<div class='hotspotTag'></div>"; //<div class='hotspotContent'></div>
    self.Init = function (arrHotspots, Debug) {
        //var originalImageDim = GetOriginalDimensions($("#" + arrHotspots[0].image));
        //imageDim = { width: $("#mediaImage").width(), height: $("#mediaImage").height() };
        self.settings = arrHotspots;
        self.DrawHotspots(arrHotspots);
        self.InitEvents(Debug);

        //imageDim.width = originalImageDim.width, imageDim.height = originalImageDim.height;
        self.ResizeHotspots();
    }
    self.GetOriginalDimensions = function (id) {
        return {
            width: $(id).width(),
            height: $(id).height()
        }
    }
    self.CalculateImagePosition = function (element, e) {
        var imageWidth = parseInt($(element).css('width'));
        var imageHeight = parseInt($(element).css('height'));
        var posX = e.pageX - $(element).offset().left;
        var posY = e.pageY - $(element).offset().top;
        var left = (posX / imageWidth) * 100;
        var top = (posY / imageHeight) * 100;
        return {
            left: left,
            top: top
        };
    }
    self.InitEvents = function (Debug) {
        $(window).resize(function () {
            self.ResizeHotspots();
        });

        $(".hotspotTag").click(function (e) {
            $("#btnSubmit").prop("disabled", false);
            CreateCircle($("#mediaImage"), e);
            //$(this).addClass('hotspotBorder').css({ 'background-color': 'transparent', 'opacity': '1' }).show();
            //$(this).find('.hotspotContent').fadeIn();
            //$(this).find('.hotspotIcon').hide();
        });

        $("#mediaImage").click(function (e) {
            //DE.QuestionLoader.SetUserOption("");
            $("#btnSubmit").prop("disabled", false);
            CreateCircle(this, e);
        });

        var CreateCircle = function (image, event) {
            $('.hotspotStamp').remove();
            var box = $("<div>").addClass('hotspotStamp').addClass('hotspotBorder').css({ 'background-color': 'red', 'opacity': '0.5' });
            var coords = CalculateImagePosition(image, event);
            $(box).css({ 'left': (coords.left - 2) + "%", 'top': (coords.top - 2) + "%", 'position': 'absolute' }).appendTo('#hotspotContainer');
        }
    }
    self.DrawHotspots = function (arrHotspots) {
        $.each(arrHotspots, function (index, value) {
            imageDim.width = value.imageWidth; imageDim.height = value.imageHeight; // Assign origincal image dimensions
            var image = $("#" + value.image);
            var imageWidth = parseInt($(image).css('width'));
            var imageHeight = parseInt($(image).css('height'));
            //alert("Width:" + imageWidth + " Left%:" + value.leftPercentage + " Top%:" + value.topPercentage);
            var leftPos = (imageWidth * value.leftPercentage) / 100;
            var topPos = (imageHeight * value.topPercentage) / 100;
            if (value.type == 'rectangle') {
                var hotspot = $(self.hotspotTag).css({ "left": value.leftPercentage + "%", "top": value.topPercentage + "%", "position": "absolute", "width": value.hotspotWidth, "height": value.hotspotHeight });
            }
            else {
                var hotspot = $(self.hotspotTag).css({ "left": value.leftPercentage + "%", "top": value.topPercentage + "%", "position": "absolute", width: value.hotspotWidth, height: value.hotspotHeight }).addClass('hotspotCircle');
                if (value.isCorrect != undefined && value.isCorrect == true) {
                    $(hotspot).addClass("correctHotspot");
                }
            }
            //$(hotspot).find('.hotspotContent').html(value.label);
            $(hotspot).attr('data-option', index + 1);
            if (value.showHotspot) { $(hotspot).addClass('hotspotBorder').find('.hotspotIcon').show(); }
            $("#" + value.container).append(hotspot);
        });
    }

    self.ResizeHotspots = function () {
        newImageDim.width = $("#mediaImage").width(); //New width
        newImageDim.height = $("#mediaImage").height(); //New height

        var widthDiff = imageDim.width - newImageDim.width; //Difference in width 
        var percentWidthChange = (widthDiff > 0) ? Math.round((widthDiff / imageDim.width) * 100) : Math.round((widthDiff / newImageDim.width) * 100);

        var heightDiff = imageDim.height - newImageDim.height; //Difference in height
        var percentHeightChange = (heightDiff > 0) ? Math.round((heightDiff / imageDim.height) * 100) : Math.round((heightDiff / newImageDim.height) * 100);

        var hotspots = $(".hotspotTag ");
        $.each(hotspots, function (index, hotspot) {
            var itemWidth = $(this).width();
            var itemHeight = $(this).height();
            var currWidth = itemWidth - Math.round((itemWidth * percentWidthChange) / 100);
            var currHeight = itemHeight - Math.round((itemHeight * percentHeightChange) / 100);
            $(this).css('width', currWidth);
            $(this).css('height', currHeight);
        });
        imageDim.width = newImageDim.width, imageDim.height = newImageDim.height;
    }

    return {
        init: Init
    };
})();