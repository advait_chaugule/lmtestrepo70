{
    "launch_file": "launch.html",
    "config_file": "config.html",
    "widget_data": "js/data/widget_data.js",
    "js_files": ["js/main.js",
        "js/config.js",
        "js/launch.js",
        "js/config.js",
        "js/image-upload.js",
        "js/lib/Widget.js",
        "js/lib/bootstrap.min.js",
        "js/lib/jquery-1.11.1.min.js",
        "js/lib/jquery-ui.js",
        "js/lib/jquery-ui.min.js",
        "js/lib/summernote.min.js"
    ],
    "css_files": ["css/style.css",
        "css/slideLine.css",
        "css/launchcss.css",
        "css/lib/bootstrap.min.css",
        "css/lib/font-awesome.min.css",
        "css/lib/summernote.css"
    ]	
}