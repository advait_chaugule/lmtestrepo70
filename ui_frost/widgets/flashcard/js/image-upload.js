'use strict'
var imageUpload = function(a) {

    var index = 0;
    var preventNext = false;
    var preventPrev = false;

    /*
    * Initialize.
    */
    var init = function() {
        doPagination(0);
        bindEvents();
    };

    /*
    * Pagination of images
    */
    var doPagination = function(start) {

        disableNav();

        var imgRow = $(".img-sel-wrap").find(".image-row");

        //Change this according to requirement
        var cols = 5;
        var rows = 1;

        var totalContent = a.length;//total number of content
        var imgPerPage = cols*rows;//Number of content in one page

        var noOfPage = 0;//Holds number of pages

        if(totalContent%imgPerPage == 0) {
            noOfPage = Math.floor(totalContent/imgPerPage);
        } else {
            noOfPage = Math.floor((totalContent/imgPerPage)+1);
        }

        //if total content is less than number of content in one page
        if(totalContent < imgPerPage) {
            $(".next, .prev").addClass("disable-anchor");
            preventPrev = true;
            preventNext = true;
            noOfPage = 1;
        }

        var whichPage = (start/imgPerPage)+1;//Current page number

        var pagination = 5;//To show page numbers, better to keep odd number like 3,5,7 etc
        var midPagination = Math.floor(pagination / 2);

        //iterate cols
        for(var i=0; i<cols; i++) {
            if(a[start]) {
                imgRow.eq(i).fadeOut("slow", "linear");
                imgRow.eq(i).find("img").attr("src", a[start].path);
                imgRow.eq(i).find(".title").html('<p>' + a[start].name + '</p>');
                imgRow.eq(i).fadeIn("slow");

            } else {
                console.log("No images to display...");
                imgRow.eq(i).fadeOut("slow", "linear");
            }
            start++;
        }
    };

    /*
    * Disable back or previous buttons for first or last page.
    */
    var disableNav = function() {

        //disable previous button on first page
        if(index === 0) {
            last = 5;
            $(".prev").addClass("disable-anchor");
            preventPrev = true;
        } else {
            var last = (index+1)*5;
            $(".prev").removeClass("disable-anchor");
            preventPrev = false;
        }

        //disable next button on last page
        if(last >= a.length) {
            $(".next").addClass("disable-anchor");
            preventNext = true;
        } else {
            $(".next").removeClass("disable-anchor");
            preventNext = false;
        }
    };
    /*
    * Bind click events on images and select button.
    */
    var bindEvents = function() {

        $(".next").off().on("click", function() {
            (!preventNext) ? doPagination(++index*5) : "";
        });

        $(".prev").off().on("click", function() {
            if (!preventPrev) {
                (index > 0) ? doPagination(--index*5) : "";
            }
        });

        $(".image-row").off().on("click", function() {

            $(".image-row").removeClass("selected");
            $(this).addClass("selected");

            $("#img-sel-btn").removeClass("btn-disable").addClass("btn-success");
            // return false;
        });

        $("#img-sel-btn").off().on("click", function() {
            if($(".selected").length > 0) {
                var selectedimg = $(".image-row.selected").find("img").attr("src");
                $(".img-sel-wrap").hide();

                return selectedimg;
            } else {
                console.log("please select any image first");
                return false;
            }
        });
         $("#close").off("click").on("click", function () {
            $(".img-sel-wrap").hide();
             
            //selectImg();
        });
    };

    init();
}
