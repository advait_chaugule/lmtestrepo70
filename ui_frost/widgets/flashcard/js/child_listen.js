/* 
 * Dependency browser should support post-message
 */


(function () {
    /*
     * Function that listen to Parent response
     */

    if (window.addEventListener) {
        window.addEventListener("message", listenParentMessage, false);
    } else {
        window.attachEvent("onmessage", listenParentMessage);
    }

    function listenParentMessage(msg) {
        var response = msg.data;
        try {
            if (response.type == 'GET') {
                widgetDataGetter('POST', response);
            }
        }
        catch (exc) {
            //console.log(exc);
        }
    }
})();

function widgetDataSetter(reqType, data) {
    reqType = reqType.toUpperCase();

    var getDefaultData = {
        'type': reqType,
        'dataset': data
    }

    top.postMessage(getDefaultData, "*"); // Posts message(Request) to parent
}


function widgetDataGetter() {
    if (arguments[0] == 'POST') {
        _widget_.initData(arguments[1]); //Return data to child frame [Widget function that sets data should be called here]
    }
    else {
        var getDefaultData = {
            'type': arguments[0]
        };
        top.postMessage(getDefaultData, "*"); // Posts message(Request) to parent
    }
}