'use strict'
var loadScript = function(path, id) {
    var aScript = document.createElement('script'); //Creates script element
    aScript.setAttribute("type", "text/javascript");
    aScript.setAttribute("src", path);
    aScript.setAttribute("data-guid", id);

    document.head.appendChild(aScript); //Appends the script to head of iframe/object

    // Callback fires after script gets loaded
    aScript.onload = function () {
        console.log(widget_data)
        return widget_data;
    }
    // Callback fires if script fails to load
    aScript.onerror = function(e) {
        return false;
    }
}
