var _widget_ = {};
(function (window, document, $) {

    _widget_ = {
        data: {}, // set a variable to store all required data, like: guid, description, label etc.
        data_path: "",
        guid: "",
        imgArr: [
            {
                name: "image1",
                path: "img/image-1.jpg"
            },
            {
                name: "image2",
                path: "img/image-2.jpg"
            },
            {
                name: "image3",
                path: "img/image-3.jpg"
            },
            {
                name: "image4",
                path: "img/flip-front.png"
            },
            {
                name: "image5",
                path: "img/flip-back.png"
            },
            {
                name: "image6",
                path: "img/image-1.jpg"
            },
            {
                name: "image7",
                path: "img/image-2.jpg"
            },
            {
                name: "image8",
                path: "img/image-3.jpg"
            },
            {
                name: "image9",
                path: "img/flip-front.png"
            },
            {
                name: "image10",
                path: "img/flip-back.png"
            },
            {
                name: "image11",
                path: "img/flip-front.png"
            },
            {
                name: "image12",
                path: "img/flip-back.png"
            }
        ],
        imgPreviewElm: "",
        init: function () {
            arrList = [];
            revisedArrList = [];
            finalDataArray = [];
            id = 0;
            
            widgetDataGetter("PUT"); //Request to get default values of widget
        },
        initData: function () {
            _widget_.guid = arguments[0].frame_id ; // set dynamic guid
            _widget_.data_path = arguments[0].data_path; // set dynamic data path
            _widget_.imgArr = arguments[0].images; // set dynamic image path
            
            imageUpload(_widget_.imgArr);
            _widget_.loadData(_widget_.data_path, _widget_.guid); // call loadData method to get the data from external resource
        },
        loadData: function (path, id) {
            var aScript = document.createElement('script'); //Creates script element
            aScript.setAttribute("type", "text/javascript");
            aScript.setAttribute("src", path);
            aScript.setAttribute("data-guid", id);

            try {
                document.head.appendChild(aScript); //Appends the script to head of iframe/object

                aScript.onload = function (msg) {
                    console.log("success!!");

                    _widget_.bindEvents();
                    getData();
                    showingIntitDivs();
                };
                aScript.onerror = function (e) {
                    console.log("error!");

                    _widget_.bindEvents();
                    getData();
                    showingIntitDivs();
                };
            } catch (e) {// error handler
            }
        },
        bindEvents: function () {

            $("textarea.new").summernote({
                height: 170, // set editor height
                minHeight: null, // set minimum height of editor
                maxHeight: null, // set maximum height of editor
                focus: true, // set focus to editable area after initializing summernote
                styleWithSpan: false,
                toolbar: [// set toobar options
                    ['style', ['bold', 'italic', 'underline']], // show only bold, italic and underline features
                    ['para', ['ul', 'ol']], // set bullet and numbering
                ]
            });

            $("#save-button").off("click").on("click", function () {
                data();
                var jsonResponse = {"widgetData": {"options" : []}};
                jsonResponse.widgetData.options = finalDataArray;
                widgetDataSetter('POST', jsonResponse)
            });
            $("#cancel-btn").off().on("click", function () {
                 widgetDataGetter("CLOSE");
            });
        }
    };

    var initArr = function () {
        arrList = [];
        $(".FC-block").each(function () {
            arrList.push($(this).attr("data-id"));
        });
    };

    var getData = function () {

        var totLength = 0;

        try {
            var totLength = widget_data.widgetData.options.length;
        } catch (e) {
            console.log(e);
        }

        console.log(totLength);
        if (totLength > 0) {
            $(".FC-carasual-slider").empty();
            $(".modal-body").eq(0).find(".FC-container-block").remove();
            var type1 = "";
            var type2 = "";
            var value1 = "";
            var value2 = "";
            var description1 = "";
            var description2 = "";
            for (var i = 0; i < totLength; i++) {
                var html = '<div class="FC-block"><button type="button" class="close panel-del" data-dismiss="modal" aria-hidden="true">&times;</button> <div class="FC-name pull-left"><span>1</span><span><i class="fa fa-th"></i></span></div><div class="FC-num pull-middle"><span class="FC-no"></span><span class="FC-no last"></span></div><div class="clearfix"></div></div>';
                var htmlBlock = '<div class="FC-container-block" data-id="' + (i + 1) + '" style="display:none"> <div class="col-xs-6 front"> <div class="form-group"> <label for="">Front</label> <select class="form-control cursor-pointer"> <option>Term</option> <option>Description</option> <option>Image</option> </select> </div><div class="terms-drop-area"> <span class="terms-text" contenteditable="true">Term Goes Here</span> <textarea name="" type="text" class="form-control terms-drop-area new" placeholder="Description" rows="4" style="display: none"></textarea> <input id="file' + (i + 1) + '" type="file" style="display: none"/><button type="button" class="btn btn-default btn-sm sel-btnf' + (i + 1) + ' select1">Select</button> <img id="loader' + (i + 1) + '" class="loaded-image" style="display: none"/> </div></div><div class="col-xs-6 back"> <div class="form-group"> <label for="">Back</label> <select class="form-control cursor-pointer"> <option>Description</option> <option>Term</option> <option>Image</option> </select> </div><div class="terms-drop-area"> <span class="terms-text" style="display: none" contenteditable="true">Term Goes Here</span> <textarea name="" type="text" class="form-control terms-drop-area new" placeholder="Description" rows="4"></textarea> <input id="f' + (i + 1) + '" type="file" style="display: none"/> <button type="button" class="btn btn-default btn-sm  sel-btnb' + (i + 1) + ' select1">Select</button> <img id="i' + (i + 1) + '" class="loaded-image" style="display: none"/> </div></div><div class="clearfix"></div></div>';
                $(".FC-carasual-slider").append(html);
                $(".modal-body").eq(0).append(htmlBlock);

                _widget_.bindEvents();
                dropDownCntrl(i);
                $(".FC-container-block").eq(i).find("textarea").removeClass("new");

                var json = widget_data.widgetData.options[i];
                type1 = json.front.type;
                value1 = json.front.value;
                description1 = json.front.description;

                type2 = json.back.type;
                value2 = json.back.value;
                description2 = json.back.description;

                if (type1 === "Term") {
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find("select").val(type1).show();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-drop-area").find(".note-editor").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-drop-area").find(".note-editor").find(".note-editable").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-text").html(description1).show();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-drop-area").find("#file" + (i + 1) + ", #loader" + (i + 1) + "").hide();
                    $(".sel-btnf" + (i + 1) + "").hide();

                }

                if (type1 === "Description") {
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find("select").val(type1);
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-drop-area").find(".note-editor").show().find(".note-editable").html(description1);
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-drop-area").find(".note-editor").find(".note-editable").show();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-text").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-drop-area").find("#file" + (i + 1) + ", #loader" + (i + 1) + "").hide();
                    $(".sel-btnf" + (i + 1) + "").hide();
                }
                if (type1 === "Image") {
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find("select").val(type1).show();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-drop-area").find(".note-editor").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-drop-area").find(".note-editor").find(".note-editable").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-text").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".front").find(".terms-drop-area").find("#loader" + (i + 1)).attr("src", value1).show();
                    $(".sel-btnf" + (i + 1) + "").show();
                    dropDownCntrl(i);
                    selectImg();

                }

                if (type2 === "Term") {
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find("select").val(type2).show();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-drop-area").find(".note-editor").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-drop-area").find(".note-editor").find(".note-editable").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-text").html(description2).show();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-drop-area").find("#f" + (i + 1) + ", #i" + (i + 1) + "").hide();
                    $(".sel-btnb" + (i + 1) + "").hide();

                }

                if (type2 === "Description") {
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find("select").val(type2);
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-drop-area").find(".note-editor").show().find(".note-editable").html(description2);
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-drop-area").find(".note-editor").find(".note-editable").show();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-text").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-drop-area").find("#f" + (i + 1) + ", #i" + (i + 1) + "").hide();
                    $(".sel-btnb" + (i + 1) + "").hide();
                }
                if (type2 === "Image") {
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find("select").val(type2).show();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-drop-area").find(".note-editor").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-drop-area").find(".note-editor").find(".note-editable").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-text").hide();
                    $(".modal-body").eq(0).find(".FC-container-block").eq(i).find(".back").find(".terms-drop-area").find("#i" + (i + 1)).attr("src", value2).show();
                    $(".sel-btnb" + (i + 1) + "").show();
                    dropDownCntrl(i);
                }
                selectImg();
                deleteNode();
            }
        } else {
            var i = $(".FC-container-block").length - 1;
            dropDownCntrl(i);
            addCard();
            clickBlock();
            sortBlock();
            imageFitFill();
            uploadImageInit();
            selectImg();
            deleteNode();
        }
        $(".FC-container-block").eq(0).show();
        $(".FC-block").eq(0).addClass("active");
        $(".FC-block").each(function (i) {
            $(this).attr("data-id", i + 1);
        });
        var length = $(".FC-block").length;
        $(".FC-block").eq(length - 1).attr("data-id", length);

        numberSorting();
        addCard();
        clickBlock();
        sortBlock();
    };

    var selectImg = function () {
        $(".select1").off().on("click", function () {
            $(".img-sel-wrap").show();
            _widget_.imgPreviewElm = $(this).parent().find(".loaded-image");

        });
        document.getElementById("img-sel-btn").addEventListener("click", function () {
            var selectedimg = $(".image-row.selected").find("img").attr("src");
            _widget_.imgPreviewElm.attr("src", selectedimg);
        });
    };
    var addCard = function () {
        var i = $(".FC-container-block").length;
        var html = '<div class="FC-block"><button type="button" class="close panel-del" data-dismiss="modal" aria-hidden="true">&times;</button><div class="FC-name pull-left"><span>1</span><span><i class="fa fa-th"></i></span></div><div class="FC-num pull-middle"><span class="FC-no"></span><span class="FC-no last"></span></div><div class="clearfix"></div></div>';
        var htmlBlock = '<div class="FC-container-block" data-id="' + (i + 1) + '" style="display:none"> <div class="col-xs-6 front"> <div class="form-group"> <label for="">Front</label> <select class="form-control cursor-pointer"> <option>Term</option> <option>Description</option> <option>Image</option> </select> </div><div class="terms-drop-area"> <span class="terms-text">Term Goes Here</span> <textarea name="" type="text" class="form-control terms-drop-area new" placeholder="Description" rows="4" style="display: none"></textarea> <input id="file' + (i + 1) + '" type="file" style="display: none"/> <button type="button" class="btn btn-primary sel-btnf' + (i + 1) + ' select1" style="display: none">Select</button><img id="loader' + (i + 1) + '" class="loaded-image" style="display: none"/> </div></div><div class="col-xs-6 back"> <div class="form-group"> <label for="">Back</label> <select class="form-control cursor-pointer"> <option>Description</option> <option>Term</option> <option>Image</option> </select> </div><div class="terms-drop-area"> <span class="terms-text" style="display: none">Term Goes Here</span> <textarea name="" type="text" class="form-control terms-drop-area new" placeholder="Description" rows="4"></textarea> <input id="f' + (i + 1) + '" type="file" style="display: none"/><button type="button" class="btn btn-default btn-sm sel-btnb' + (i + 1) + ' select1" style="display: none">Select</button> <img id="i' + (i + 1) + '" class="loaded-image" style="display: none"/> </div></div><div class="clearfix"></div></div>';
        $(".add").off("click").on("click", function () {
            $(".FC-carasual-slider").append(html);
            var length = $(".FC-block").length;
            $(".FC-block").eq(length - 1).attr("data-id", length);
            $(".modal-body").eq(0).append(htmlBlock);
            initArr();
            numberSorting();
            clickBlock();
            sortBlock();
            _widget_.bindEvents();
            dropDownCntrl(i);
            selectImg();
            deleteNode();
            $("textarea").removeClass("new");
            var length1 = $(".FC-container-block").length;
            $(".FC-container-block").eq(length1 - 1).find(".front").find(".terms-drop-area").find(".note-editor").hide();
            $(".FC-carasual-slider").scrollLeft($('.FC-carasual-slider')[0].scrollWidth);
        });
    };

    var clickBlock = function () {
        $(".FC-block").off("click").on("click", function () {
            $(".FC-block").removeClass("active");
            $(this).addClass("active");
            id = +$(this).attr("data-id");
            console.log(id);
            $(".modal-body").eq(0).find(".FC-container-block").hide();
            $(".modal-body").eq(0).find(".FC-container-block").eq(id - 1).show();
            $(".image-slide-container").hide();
        });
    };
    var sortBlock = function () {
        $('.FC-carasual-slider').sortable({
            containment: ".FC-carasual-container",
            update: function (event, ui) {
                numberSorting();
                arrangeArray();
            }
        });
    };

    var numberSorting = function () {
        $(".FC-block").each(function (i) {
            $(this).find("span").eq(0).html(i + 1);
        });
    };

    var arrangeArray = function () {
        var arr = [];
        revisedArrList = [];
        $(".FC-block").each(function () {
            var id = $(this).attr("data-id");
            arr.push(arrList[id - 1]);
        });
        revisedArrList = arr;
        for (var i = 0; i < revisedArrList.length; i++) {
            $(".FC-container-block").eq(i).attr("data-id", revisedArrList[i]);
        }
    };

    var imageFitFill = function () {
        $(".btn-group").find("label").off("click").on("click", function () {
            $(".btn-group").find("label").removeClass("active");
            $(this).addClass("active");
            if ($(this).find("input").attr("id") === "option1") {
            } else {
            }
        });
    };

    var dropDownCntrl = function (i) {
        $("select").change(function () {
            var val = $(this).val();
            console.log(val);
            if ($(this).closest(".col-xs-6").hasClass("front")) {
                if (val === "Term") {
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find(".note-editor").hide();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-text").show();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find("#file" + (i + 1) + ", #loader" + (i + 1) + "").hide();
                    $(this).closest(".FC-container-block").find(".front").find(".sel-btnf" + (i + 1) + "").hide();
                }
                if (val === "Description") {
                    $(this).closest(".FC-container-block").find(".front").find(".terms-text").hide();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find(".note-editor").show();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find(".note-editor").find(".note-editable").show();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find("#file" + (i + 1) + ", #loader" + (i + 1) + "").hide();
                    $(this).closest(".FC-container-block").find(".front").find(".sel-btnf" + (i + 1) + "").hide();

                }
                if (val === "Image") {
                    $(this).closest(".FC-container-block").find(".front").find(".terms-text").hide();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find(".note-editor").hide();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find("#loader" + (i + 1) + "").show();
                    $(this).closest(".FC-container-block").find(".front").find(".sel-btnf" + (i + 1) + "").show();
                }
            } else {
                if (val === "Term") {
                    $(this).closest(".FC-container-block").find(".back").find(".note-editor").hide();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-text").show();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-drop-area").find("#f" + (i + 1) + ", #i" + (i + 1) + "").hide();
                    $(this).closest(".FC-container-block").find(".back").find(".sel-btnb" + (i + 1) + "").hide();
                }
                if (val === "Description") {
                    $(this).closest(".FC-container-block").find(".back").find(".terms-text").hide();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-drop-area").find(".note-editor").show();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-drop-area").find(".note-editor").find(".note-editable").show();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-drop-area").find("#f" + (i + 1) + ", #i" + (i + 1) + "").hide();
                    $(this).closest(".FC-container-block").find(".back").find(".sel-btnb" + (i + 1) + "").hide();
                }
                if (val === "Image") {
                    $(this).closest(".FC-container-block").find(".back").find(".terms-text").hide();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-drop-area").find(".note-editor").hide();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-drop-area").find("#i" + (i + 1) + "").show();
                    $(this).closest(".FC-container-block").find(".back").find(".sel-btnb" + (i + 1) + "").show();
                }
            }
        });
    };

    var uploadImageInit = function () {
        $("select").change(function () {

            var val = $(this).val();
            if ($(this).closest(".col-xs-6").hasClass("front")) {

                if (val === "Term") {
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find(".note-editor").hide();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-text").show();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find("#file, #loader").hide();
                }
                if (val === "Description") {
                    $(this).closest(".FC-container-block").find(".front").find(".terms-text").hide();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find(".note-editor").show();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find("#file, #loader").hide();

                }
                if (val === "Image") {
                    $(this).closest(".FC-container-block").find(".front").find(".terms-text").hide();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find(".note-editor").hide();
                    $(this).closest(".FC-container-block").find(".front").find(".terms-drop-area").find("#file, #loader").show();

                }
            } else {
                if (val === "Term") {
                    $(this).closest(".FC-container-block").find(".back").find(".note-editor").hide();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-text").show();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-drop-area").find("#f, #i").hide();
                }
                if (val === "Description") {
                    $(this).closest(".FC-container-block").find(".back").find(".terms-text").hide();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-drop-area").find(".note-editor").show();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-drop-area").find("#f, #i").hide();
                }
                if (val === "Image") {
                    $(this).closest(".FC-container-block").find(".back").find(".terms-text").hide();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-drop-area").find(".note-editor").hide();
                    $(this).closest(".FC-container-block").find(".back").find(".terms-drop-area").find("#f, #i").show();

                }
            }

        });
    };

    var deleteNode = function () {
        $(".close").off("click").on("click", function () {

            var fcData = +$(this).parent().attr("data-id");
            if ($(this).parent().hasClass("active")) {
                var index = $(this).parent().index();
                var length = $(".FC-block").length - 1;

                if (index === 0) {
                    $(".FC-block").eq(index + 1).addClass("active");
                    var id = +$(".FC-block").eq(index + 1).attr("data-id");
                    $(".FC-container-block").eq(id - 1).show();
                }
                if (index === length) {
                    $(".FC-block").eq(index - 1).addClass("active");
                    var id = +$(".FC-block").eq(index - 1).attr("data-id");
                    $(".FC-container-block").eq(id - 1).show();
                } else {
                    $(".FC-block").eq(index + 1).addClass("active");
                    var id = +$(".FC-block").eq(index + 1).attr("data-id");
                    $(".FC-container-block").eq(id - 1).show();
                }
            }
            $(".FC-container-block").eq(fcData - 1).remove();
            $(this).parent().remove();
            numberSorting();
            $(".FC-block").each(function () {
                var dataId = $(this).attr("data-id");
                if (dataId > fcData) {
                    dataId = dataId - 1;
                    $(this).attr("data-id", dataId);
                }
            });

            $(".FC-container-block").each(function () {
                var dataId1 = +$(this).attr("data-id");
                if (dataId1 > fcData) {
                    dataId1 = dataId1 - 1;
                    $(this).attr("data-id", dataId1);
                }
            });

        });
    };

    var showingIntitDivs = function () {

        $(".FC-container-block").eq(0).find(".front").find(".terms-drop-area").find("textarea").hide();
        $(".FC-container-block").eq(0).find(".front").find(".terms-drop-area").find(".note-editor").hide();
        $(".FC-container-block").eq(0).find(".front").find(".terms-drop-area").find("#file, #loader").hide();
        $(".FC-container-block").eq(0).find(".front").find(".terms-text").show();

        $(".FC-container-block").eq(0).find(".back").find(".terms-text").hide();
        $(".FC-container-block").eq(0).find(".back").find(".terms-drop-area").find("textarea").hide();
        $(".FC-container-block").eq(0).find(".back").find(".terms-drop-area").find("#f, #i").hide();
        $(".FC-container-block").eq(0).find(".back").find(".terms-drop-area").find(".note-editor").show();
    };

    var data = function () {
        finalDataArray = [];

        var type1 = "";
        var type2 = "";
        var value1 = "";
        var value2 = "";
        var description1 = "";
        var description2 = "";
        $(".FC-block").each(function () {
            var index = +$(this).attr("data-id");

            var data = {
                front: {
                    "type": "",
                    "value": "",
                    "description": ""
                },
                back: {
                    "type": "",
                    "value": "",
                    "description": ""
                }
            };
            type1 = $(".FC-container-block").eq(index - 1).find(".front").find("select").val();

            if (type1 === "Term") {
                description1 = $(".FC-container-block").eq(index - 1).find(".front").find(".terms-text").text();
            }
            if (type1 === "Description") {
                description1 = $(".FC-container-block").eq(index - 1).find(".front").find(".note-editable").html();
            }
            if (type1 === "Image") {
                description1 = "";
                value1 = $(".FC-container-block").eq(index - 1).find(".front").find("img").attr("src");
            }

            type2 = $(".FC-container-block").eq(index - 1).find(".back").find("select").val();

            if (type2 === "Term") {
                description2 = $(".FC-container-block").eq(index - 1).find(".back").find(".terms-text").text();
            }
            if (type2 === "Description") {
                description2 = $(".FC-container-block").eq(index - 1).find(".back").find(".note-editable").html();
            }
            if (type2 === "Image") {
                description2 = "";
                value2 = $(".FC-container-block").eq(index - 1).find(".back").find("img").attr("src");
            }

            data.front.type = type1;
            data.front.value = value1;
            data.front.description = description1;

            data.back.type = type2;
            data.back.value = value2;
            data.back.description = description2;

            finalDataArray.push(data);
        });
    };

    _widget_.init();
})(window, document, jQuery);
