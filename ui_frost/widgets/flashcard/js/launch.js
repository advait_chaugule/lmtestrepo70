var _widget_ = {};
$.mobile.loading().hide();
    (function (window, document, $) {
        _widget_ = {
            data: {}, // set a variable to store all required data, like: guid, description, label etc.
            noOfCards: 0,
            centerPos: 0,
            leftPos: 0,
            rightPos: 0,
            rmDisablPrev:"",
            posArr: [],
            animTime: 300,
            init: function () {
                widgetDataGetter("GET"); //Request to get default values of widget
            },
            initData: function () {
                _widget_.guid = arguments[0].frame_id ; // set dynamic guid
                _widget_.data_path = arguments[0].data_path; // set dynamic data path
                _widget_.imgArr = arguments[0].images; // set dynamic images path
                
                _widget_.loadData(_widget_.data_path, _widget_.guid); // call loadData method to get the data from external resource
            },
            loadData: function (path, id) {
                var aScript = document.createElement('script'); //Creates script element
                aScript.setAttribute("type", "text/javascript");
                aScript.setAttribute("src", path);
                aScript.setAttribute("data-guid", id);

                try {
                    document.head.appendChild(aScript); //Appends the script to head of iframe/object

                    // Callback fires after script gets loaded
                    aScript.onload = function (msg) {
                        $.extend(_widget_.data, widget_data);
                        _widget_.createDOM(_widget_.bindEvents);

                    }
                    // Callback fires if script fails to load
                    aScript.onerror = function (e) {
                        _widget_.createDOM(_widget_.bindEvents);
                    }
                } catch (e) {// error handler
                    // console.log("error received", e);
                }
            },
            createDOM: function (cb) {
                var innerHTML = "";
                $(".slider-container").empty();

                $.each(_widget_.data.widgetData.options, function(i, val) {
                    var cardFront = val.front.type.toLowerCase();
                    var cardBack = val.back.type.toLowerCase();
                    var front = "";
                    var back = "";
                    var sliderCol = "";
                    switch(cardFront) {
                        case "term":
                            front = '<div class="front frontFace"><div class="block-content"><span class="flip-front" ></span><h3>' + val.front.description + '</h3></div></div>';
                            break;
                        case "description":
                            front = '<div class="front frontFace"><div class="block-content scroll"><span class="flip-front" ></span>' + val.front.description +'</div></div>';
                            break;
                        case "image":
                            front = '<div class="front frontFace"><div class="img-content"><span class="flip-front" ></span><img src="' + val.front.value + '" alt="' + val.front.description + '" /></div></div>';
                            break;
                        default:
                            front = '<div class="front frontFace"><div class="block-content"><span class="flip-front" ></span><h3>Term goes Here.</h3></div></div>';
                    }

                    switch(cardBack) {
                        case "term ":
                            back = '<div class="back backFace"><div class="block-content"><span class="flip-back" ></span><h3>' + val.back.description + '</h3></div></div>';
                            break;
                        case "description":
                            back = '<div class="back backFace"><div class="block-content scroll"><span class="flip-back" ></span>' + val.back.description +'</div></div>';
                            break;
                        case "image":
                            back = '<div class="back backFace"><div class="img-content"><span class="flip-back" ></span><img src="' + val.back.value + '" alt="' + val.back.description + '" /></div></div>';
                            break;
                        default:
                            back = '<div class="back backFace"><div class="block-content"><span class="flip-back" ></span><h3>Term goes here</h3></div></div>';
                    }

                    innerHTML += '<div class="slider-col"><div class="col"><div class="desc">' + front + back +'</div></div></div>'
                });
                $(".slider-container").append(innerHTML);

                cb();
            },
            bindEvents: function () {

                // functionality for previous button
                $(".slide-nav").find(".prev").off().on("click", function () {
                    clearTimeout(this.rmDisablPrev);
                    $(this).addClass('disablePrev');
                    _widget_.posArr.slice(0, _widget_.posArr.length).join(",");
                    _widget_.posArr.push(_widget_.posArr.shift());

                    _widget_.updateCounter();
                    _widget_.moveCards();
                    this.rmDisablPrev = setTimeout(function(){
                        $(".slide-nav").find(".prev").removeClass('disablePrev');
                    }, 350);
                });

                // functionality for next button
                $(".slide-nav").find(".next").off().on("click", function () {
                    clearTimeout(this.rmDisablNext);
                    $(this).addClass('disableNext');
                    var lastElm = _widget_.posArr[_widget_.posArr.length - 1];

                    _widget_.posArr.splice(_widget_.posArr.length - 1, 1);
                    _widget_.posArr.splice(0, 0, lastElm);

                    _widget_.updateCounter();
                    _widget_.moveCards();
                    this.rmDisablNext = setTimeout(function(){
                        $(".slide-nav").find(".next").removeClass('disableNext');
                    }, 350);
                });

                // show previous slider on swiping right
                $(".slider-container").swiperight(function() {
                    _widget_.posArr.slice(0, _widget_.posArr.length).join(",")
                    _widget_.posArr.push(_widget_.posArr.shift());

                    _widget_.updateCounter();
                    _widget_.moveCards();
                });

                // show next slider on swiping left
                $(".slider-container").swipeleft(function() {
                    var lastElm = _widget_.posArr[_widget_.posArr.length - 1];

                    _widget_.posArr.splice(_widget_.posArr.length - 1, 1);
                    _widget_.posArr.splice(0, 0, lastElm);

                    _widget_.updateCounter();
                    _widget_.moveCards();
                });

                $(".slider-col-left").off().on("click", function() {alert()
                    $(".slide-nav").find(".prev").trigger("click");
                });
                $(".slider-col-right").off().on("click", function() {alert()
                    $(".slide-nav").find(".next").trigger("click");
                });
                // Prevent images from dragging
                window.ondragstart = function() { return false; }
                _widget_.bindFlip();
                _widget_.bindSliders();
                _widget_.resizeImages();
            },
            bindFlip: function() {
                $('.slider-col-active').find(".flip-front").off().on("click", function () {
                    $('.slider-col-active').find('.front').addClass('frontFaceRotate').removeClass('frontFace');
                    $('.slider-col-active').find('.back').addClass('backFaceRotate').removeClass('backFace');
                });

                $('.slider-col-active').find(".flip-back").off().on("click", function () {
                    $('.slider-col-active').find('.front').removeClass('frontFaceRotate').addClass('frontFace');
                    $('.slider-col-active').find('.back').removeClass('backFaceRotate').addClass('backFace');

                });
            },
            bindSliders: function (obj) {
                this.noOfCards = $(".slider-container").find(".slider-col").length;

                for (var i = 0; i < this.noOfCards; i++) {
                    _widget_.posArr.push(i);
                }

                this.updateCounter();
                this.moveCards();
            },
            updateCounter: function () {
                var count = _widget_.posArr.length + 1 - _widget_.posArr[0];
                (count > _widget_.posArr.length) ? (count = 1) : "";
                var slideCount = "<p>" + count + "<span>of</span>" + _widget_.posArr.length + "</p>";
                $(".slide-count").html(slideCount);
            },
            moveCards: function () {
                // _widget_.animTime = 5000;
                var sliderCol = $(".slider-container").find(".slider-col");
                var frontCard = sliderCol.eq(_widget_.posArr[0]);
                var leftCard = sliderCol.eq(_widget_.posArr[1]);
                var rightCard = sliderCol.eq(_widget_.posArr[_widget_.posArr.length - 1]);

                sliderCol.find('.front').removeClass('frontFaceRotate').addClass('frontFace');
                sliderCol.find('.back').removeClass('backFaceRotate').addClass('backFace');
                sliderCol.removeClass("slider-col-center slider-col-active faces slider-col-left slider-col-right");


                // Front card
                frontCard
                    .addClass("slider-col-center slider-col-active faces")
                    .animate(_widget_.animateCards("front"), _widget_.animTime, "swing");

                // Left card
                leftCard
                    .addClass("faces slider-col-left")
                    .animate(_widget_.animateCards("left"), _widget_.animTime, "swing");

                // Right card
                rightCard
                    .addClass("faces slider-col-right")
                    .animate(_widget_.animateCards("right"), _widget_.animTime, "swing");

                var otherCards = $(".slider-container").find(".slider-col:not(.faces)");
                // default style for other cards.
                otherCards.animate(_widget_.animateCards(), _widget_.animTime, "swing");

                this.bindFlip();
            },
            animateCards: function(str) {
                switch(str) {
                    case "front":
                        return {
                            width: "250px",
                            height: "250px",
                            margin: "0px 0px 0 -114px",
                            top: "0px",
                            left: "50%",
                            zIndex:999,
                            cursor:"default",
                            // perspective: "1200px",
                            visibility: "inherit",
                            opacity: 1
                        };
                        break;
                    case "left":
                        return {
                            width: "200",
                            height: "200",
                            visibility: "visible",
                            left: "0px",
                            margin: "27px 0 0 0",
                            opacity:1,
                            zIndex: 1
                        };
                        break;
                    case "right":
                        return {
                            width: "200",
                            height: "200",
                            visibility: "visible",
                            right:"0px",
                            left: "67%",
                            margin:"27px 0 0 0",
                            opacity:1,
                            zIndex: 1,
                        };
                        break;
                    default:
                        return {
                            width: "200px",
                            height: "200px",
                            top:"0px",
                            margin: "27px 0px 0 -114px",
                            left: "50%",
                            cursor:"move",
                            visibility: "hidden",
                            zIndex:-1,
                            opacity:0,
                        };
                }
            },
            resizeImages: function() {

                $(".slider-col").find(".img-content").find("img").each(function() {
                    $(this).removeClass("fit-width fill fit-height");

                    var pic_real_width, pic_real_height, that;
                    that = $(this);
                    $("<img/>") // Make in memory copy of image to avoid css issues
                    .attr("src", $(that).attr("src"))
                    .load(function() {
                        pic_real_width = this.width;   // Note: $(this).width() will not
                        pic_real_height = this.height; // work for in memory images.

                        if(pic_real_width > pic_real_height) {
                            if(((pic_real_width * 0.7) > pic_real_height) || ((pic_real_width - pic_real_height) > 200)) {
                                $(that).addClass("fit-width");
                            } else {
                                $(that).addClass("fill");
                            }
                        } else if(pic_real_height > pic_real_width) {
                            if(((pic_real_height * 0.7) >  pic_real_width) || ((pic_real_height - pic_real_width) > 200)) {
                                $(that).addClass("fit-height");
                            } else {
                                $(that).addClass("fill");
                            }
                        } else {
                            $(that).addClass("fill");
                        }
                    });
                });
            }
        };

        // call the init function.
        _widget_.init();
    })(window, document, jQuery);
