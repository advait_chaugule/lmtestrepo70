/* 
 * Frost
 * Dependency jQuery
 * Dependency browser should support post-message
 */

(function () {
    var allFrameLoaded = 0,
            parFrameEl = '',
            imgList = [],
            isOffline = (parent.location.href).substring(0, 4) === 'file',
            frameSize = [],
            widgetHeightLimit = 610;

    var modeSetter = (function () {
        return {
            getInstance: function () {
                var parFrameEl = jQuery('#template_preview_iframe').length;
                if (parFrameEl)
                    return jQuery('#template_preview_iframe').contents()
                else
                    return jQuery('body');
            },
            getAbsolutePath: function (href) {
                var link = document.createElement("a");
                link.href = href;
                return (link.protocol + "//" + link.host + link.pathname + link.search + link.hash);
            }
        };
    })();


    function listenChildMessage(msg) {
        parFrameEl = modeSetter.getInstance();

        try {
            getImages();
        }
        catch (exc) {
            //console.log('Window says: '+ exc);
        }

        var childReq = msg.data,
                reqType = (childReq.type).toUpperCase();

        if (reqType === 'GET' && (parFrameEl.find('iframe.exwidget').length >= allFrameLoaded)) {
            allFrameLoaded++;

        }
        else if (reqType === 'PUT') {
            try {
                var dataPath = htmlEditor.thisFrame.attr("data-path"),
                        ref = htmlEditor.thisFrame.attr("id");

                var childRes = {
                    'type': 'GET',
                    'frame_id': ref,
                    'data_path': dataPath,
                    'images': imgList
                };

                sendResponse('POST', 'widFrame', childRes);
            }
            catch (exc) {
                //console.log('Window says: '+ exc);
            }
        }
        else if (reqType === 'CLOSE') {
            closeWidgetModal();
        }
        else if (reqType === 'SIZE') {
            frameSize = [];
            frameSize.push({"height": childReq.height, "width": childReq.width});
            setFrameSize(childReq.frame_id, 0);
        }

        switch (reqType) {
            case "GET":
                var frameHeight = childReq.height,
                        frameWidth = childReq.width;
                
                frameSize.push({"height": frameHeight, "width": frameWidth});

                if (parFrameEl.find('iframe.exwidget').length === allFrameLoaded)
                    getChildDefaultAttributes(frameSize);
                break;

            case "POST":
                setData(htmlEditor.thisFrame.attr('id'), childReq.dataset, htmlEditor.thisFrame.attr('widget-id'));
                break;

            default:
                //console.log('Sorry, something went wrong!!');
                break;
        }
    }

    function closeWidgetModal() {
        jQuery('#exwidgetModal').find('button.close').trigger('click');
    }

    if (window.addEventListener) {
        window.addEventListener("message", listenChildMessage, false);
    } else {
        window.attachEvent("onmessage", listenChildMessage);
    }

    function setData(ref, data, widgetID) {
        var dataPath = setWidgetData(ref, data, widgetID);
        jQuery(".cke_wysiwyg_frame").contents().find('#' + ref).attr('data-path', dataPath);

        var childRes = {
            'frame_id': ref,
            'data_path': dataPath
        };

        sendResponse('POST', 'widFrame', childRes);
        closeWidgetModal();
    }

    function setWidgetData(ref, data, widgetID) {
        var path = "",
                dataToSent = data;

        var reqParam = {"project_id": project_id, "project_type_id": project_type_id, "ex_widgets": dataToSent, "repo_id": ref, "object_id": node_id, "widget_id": widgetID};

        jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.saveWidgetDataAPI,
            async: false,
            method: 'POST',
            data: reqParam,
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function (data) {
                path = data.json_file_path;
            },
            error: function () {
                alert("Error occured..Retry");
            }
        });
        return path;
    }

    var getChildDefaultAttributes = function () {
        var childIdSet = iterateChildFrames('id'),
                childData = iterateChildFrames('data-path');

        if (isOffline) {
            for (var ind = 0; ind < childData.length; ind++) {
                var actPath = modeSetter.getAbsolutePath(childData[ind]);
                childData[ind] = actPath;
            }
        }

        for (var ind = 0; ind < childIdSet.length; ind++) {
            var childRes = {
                'type': 'GET',
                'frame_id': childIdSet[ind],
                'data_path': childData[ind]
            };
            sendResponse('GET', childIdSet[ind], childRes);

            //setFrameSize(childIdSet[ind], ind);
        }
        allFrameLoaded = 0;
        setTimeout(function(){
        scrollToRef();
        },500);
    };

    function setFrameSize(ref, index) {
        var frameId = ref,
                frame = (parFrameEl.find('#' + frameId))[0],
                frameHeight = Number(frameSize[index].height) + 40;
        try {
            if (frameHeight < widgetHeightLimit) {
                frame.height = frameHeight;
                
            }
            else {
                frame.height = widgetHeightLimit;
            }
        }
        catch (exc) {
            //console.log(exc);
        }
    }

    function sendResponse(type, ref, obj) {
        if (type === 'POST') {
            var frameId = ref,
                    frame = (jQuery('body').find('#' + frameId))[0];

            frame.contentWindow.postMessage(obj, "*");
        }
        else if (type === 'GET') {
            var frameId = ref,
                    frame = (parFrameEl.find('#' + frameId))[0];

            frame.contentWindow.postMessage(obj, "*");
        }

    }

    function getImages() {
        var responseObject = "",
                returnVal = [];

        jQuery.ajax({
            url: htmlEditor.PXEmappedAPI.getLocalImagesAPI,
            async: true,
            type: "GET",
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            success: function (response) {
                responseObject = response.data;
                if (responseObject !== null) {
                    for (var idx = 0; idx < responseObject.length; idx++) {
                        returnVal.push({
                            name: responseObject[idx].original_name,
                            path: responseObject[idx].asset_location
                        });
                    }
                }

                imgList = returnVal;
            },
            error: function () {
                swal({title: "Error!", text: "Some error occured while fetching the assets."});
            }
        });
    }

    function iterateChildFrames(atr) {
        var attrSet = [];
        parFrameEl.find('iframe.exwidget').each(function () {
            var elId = jQuery(this).attr(atr);
            attrSet.push(elId);
        });
        return attrSet;
    }
})();