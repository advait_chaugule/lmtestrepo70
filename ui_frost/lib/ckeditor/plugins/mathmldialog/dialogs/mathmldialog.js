/**
 * Custom dialog for mathml insertion
 */

'use strict';

CKEDITOR.dialog.add('mathmlDialog', function (editor) {
  return {
    title: 'Math Equation',
    minWidth: 350,
    minHeight: 100,
    contents: [
      {
        id: 'info',
        elements: [
          {
            id: 'mathmlequation',
            type: 'textarea',
            label: 'Write your MathML here',
            validate: function () {
              if (!this.getValue()) {
                alert('MathML text area cannot be empty.');
                return false;
              }else if(this.getValue().indexOf("<math")){
                if (this.getValue().indexOf("<math") == -1) {
                  alert('Please input a valid MathML.');
                  return false;
                } else {
                  return true;
                }
              }
            }
          },
        ]
      }
    ],
    onShow: function () {
      var dialog = this;
      if (editor.elementPath().contains('math') == null) {
        var mySelection = '';
      } else {
        editor.elementPath().contains('math').addClass('math_edit');
        var mySelection = editor.elementPath().contains('math').getParent().getHtml();
      }
      dialog.setValueOf("info", "mathmlequation", mySelection);
    },
    onOk: function ()
    {
      var dialog = this;
      var mathmlequation = dialog.getValueOf('info', 'mathmlequation');
      if (editor.elementPath().contains('math') == null) {
        var mySelection = '';
      } else {
        var mySelection = editor.elementPath().contains('math').getParent().getName();
        mathmlequation = $(mathmlequation).filter('math').html();
      }
      if (mySelection == 'span') {
        editor.elementPath().contains('math').getParent().remove();
        editor.insertHtml('<span><math>' + mathmlequation + '</math></span>&nbsp;');
      } else {
        editor.insertHtml('<span>' + mathmlequation + '</span>&nbsp;');
      }
    },
    onCancel: function () {
      if (editor.elementPath().contains('math') != null) {
        editor.elementPath().contains('math').removeClass('math_edit');
      }
    }
  };
});