/**
 * ustom plugin for mathml insertion
 */
CKEDITOR.plugins.add('mathmldialog', {
  icons: 'MathML',
  init: function (editor) {
    CKEDITOR.dialog.add("mathmlDialog", this.path + "dialogs/mathmldialog.js");
    editor.addCommand('openMathmlDialog', new CKEDITOR.dialogCommand('mathmlDialog'));
    editor.ui.addButton('MathmlDialog', {
      label: 'Insert Mathml',
      command: 'openMathmlDialog',
      toolbar: 'insert',
      icon: this.path + 'images/MathML.png'
    });

    editor.on('mode', function (evt) {
      if (evt.editor.document == undefined) {
        return false;
      } else {
        var elements = evt.editor.document.getElementsByTag('math');
        for (var i = 0; i < elements.count(); ++i) {
          elements.getItem(i).removeClass('math_edit');
        }
        var focusManager = new CKEDITOR.focusManager(evt.editor);
        focusManager.blur();
      }
    });

    editor.on('doubleclick', function (evt) {
      var element = evt.data.element;
      if (editor.elementPath().contains('math') == null) {
        var tagName = '';
      } else {
        var tagName = editor.elementPath().contains('math').getName();
      }
      if (tagName == 'math') {
        var tagData = editor.elementPath().contains('math').getHtml();
        evt.data.dialog = 'mathmlDialog';
      }
    });

    //Delete Functionality on MathML
    editor.on('focus', function () {
      for (var i in CKEDITOR.instances) {
        if (CKEDITOR.instances[i].document != undefined) {
          var elements = CKEDITOR.instances[i].document.getElementsByTag('math');
          for (var i = 0; i < elements.count(); ++i) {
            elements.getItem(i).removeClass('math_edit');
          }
        }
      }
      var document = this.document;
      document.on('click', function (event) {
        var elements = editor.document.getBody().getElementsByTag('math');
        for (var i = 0; i < elements.count(); ++i) {
          elements.getItem(i).removeClass('math_edit');
        }
        if (editor.elementPath().contains('math') != null) {
          editor.elementPath().contains('math').addClass('math_edit');
          document.on('keydown', function (events) {
            if (events.data.getKey() == 46 || events.data.getKey() == 8) {
              if (editor.elementPath().contains('math') != null) {
                events.data.preventDefault();
                editor.elementPath().contains('math').getParent().remove();
              } else {
                return true;
              }
            }
          });
        }
      });
    });

    editor.on('blur', function () {
      for (var i in CKEDITOR.instances) {
        if (CKEDITOR.instances[i].document != undefined) {
          var elements = CKEDITOR.instances[i].document.getElementsByTag('math');
          for (var i = 0; i < elements.count(); ++i) {
            elements.getItem(i).removeClass('math_edit');
          }
        }
      }
    });
  }
});