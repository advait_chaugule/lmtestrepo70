CKEDITOR.plugins.add('customimage', {
    icons: 'customimage',
    init: function (editor) {
        editor.addCommand('insertImg', {
            exec: function () {
                angular.element(document.getElementById('questionController')).scope().addAssestModal(true);
            }
        });
        editor.ui.addButton('insertImage', {
            label: 'Image',
            command: 'insertImg',
            toolbar: 'insert'
        });
    }
});