/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
  // Define changes to default configuration here.
  // For complete reference see:
  // http://docs.ckeditor.com/#!/api/CKEDITOR.config
  config.plugins = "dialogui,dialog,about,basicstyles,clipboard,button,toolbar,enterkey,entities,floatingspace,wysiwygarea,indent,indentlist,fakeobjects,link,list,undo,sourcearea,mathmldialog,tab,table,tabletools,resize,customimage";
  config.mathJaxLib = '//cdn.mathjax.org/mathjax/2.6-latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML-full';
  // The toolbar groups arrangement, optimized for a single toolbar row.
  config.toolbarGroups = [
    {name: 'document', groups: ['mode', 'document', 'doctools']},
    {name: 'clipboard', groups: ['clipboard', 'undo']},
    {name: 'editing', groups: ['find', 'selection', 'spellchecker']},
    {name: 'forms'},
    {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi']},
    {name: 'links'},
    {name: 'insert'},
    {name: 'styles'},
    {name: 'colors'},
    {name: 'tools'},
    {name: 'others'},
    {name: 'about'}
  ];
  config.allowedContent = true;
  config.editingBlock = false;
  // The default plugins included in the basic setup define some buttons that
  // are not needed in a basic editor. They are removed here.
  config.removeButtons = 'Cut,Copy,Paste,Undo,Redo,Anchor,Strike,Subscript,Superscript,About,Indent,Outdent,Underline';

  // Dialog windows are also simplified.
  config.removeDialogTabs = 'image:advanced;link:advanced';

  //Adjust height
  config.height = '70';
  //Resize Plugin Intriduced, 
  //To disable, remove it from config.plugins[] also
  config.resize_enabled = true;
  config.resize_dir = 'vertical';

  //config.disallowedContent = 'table[border]';
};

CKEDITOR.on('dialogDefinition', function (ev) {
  var dialogName = ev.data.name;
  var dialogDefinition = ev.data.definition;
  if (dialogName == 'table' || dialogName == 'tableProperties') {
    var infoTab = dialogDefinition.getContents('info');
    var txtWidth = infoTab.get('txtWidth');
    txtWidth['default'] = '100%';
    infoTab.remove('txtCellSpace');
    infoTab.remove('txtCellPad');
    infoTab.remove('txtBorder');
    infoTab.remove('txtSummary');
    infoTab.remove('cmbAlign');
  }
  if (dialogName == 'link') {
    dialogDefinition.getContents('target').get('linkTargetType')['default'] = '_blank';

    //Keep Only URL on LinkType
    var infoTab = dialogDefinition.getContents('info');
    var linktypeField = infoTab.get('linkType');
    linktypeField['items'].splice(1, 2);
    //==//

    //Keep only New Window on link>target
    var target = dialogDefinition.getContents('target');
    var target_options = target.get('linkTargetType').items;
    for (var i = target_options.length - 1; i >= 0; i--) {
      var label = target_options[i][0];
      if (!label.match(/new window/i)) {
        target_options.splice(i, 1);
      }
    }
    var targetField = target.get('linkTargetType');
    targetField['default'] = '_blank';
    //=====//

    //Keep only http and https on link>protocol
    dialogDefinition.getContents('info').get('protocol')['items'].splice(2, 3);
    //
  }
});

CKEDITOR.on("instanceReady", function (event) {
  $('.cke_button__insertimage_icon').css({
    'background-image': 'url(../../lib/ckeditor/plugins/customimage/icons/icons.png)',
    'background-position': '-68px -254px'
  });
  $('.cke_button__mathmldialog_icon').css({
    'background-size': 'inherit',
    'width': '18px'
  });
});


